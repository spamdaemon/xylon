#include <xylon/xsdom/dom/util.h>
#include <iostream>

using namespace ::std;
using namespace ::xylon;
using namespace ::xylon::xsdom;
using namespace ::xylon::xsdom::dom;

void utest_1()
{
  String list("1 2 3 4 5");
  vector<String> values = parseListValue(list);
  assert(values.size()==5);
}
