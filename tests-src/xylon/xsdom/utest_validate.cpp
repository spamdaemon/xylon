#include <canopy/Process.h>
#include <canopy/mt.h>
#include <iostream>
#include <cassert>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::mt;

struct Output {
  Output(istream& in) : _stream(in) {}
  
  string _data;
  ostringstream _ostream;
  istream& _stream;
};

static void consumeStream (void* in)
{
  Output& out = *reinterpret_cast<Output*>(in);

  char buf[1024];

  while(true) {
    out._stream.read(buf,sizeof(buf));
    if (out._stream.gcount()>0) {
      out._ostream.write(buf,out._stream.gcount());
    }
    else {
      break;
    }
  }
  out._data = out._ostream.str();
  out._ostream.str("");
}


static bool validateSchema(string file, const string& data)
{

  Thread cerrReader,coutReader;
  Process proc;
  try {
    if (data.empty()) {
      ::std::cerr << "bin/xylon/svalidate " << file << ::std::endl;
      proc = Process::execute(Process::Command("bin/xylon/svalidate",{file}));
    }
    else {
      ::std::cerr << "bin/xylon/svalidate " << file << ' '  << data << ::std::endl;
      proc = Process::execute(Process::Command("bin/xylon/svalidate", { file,data}));
    }
    Output cerrOut(proc.cerr());
    Output coutOut(proc.cout());
    cerrReader=Thread("cerr-reader",consumeStream,&cerrOut);
    coutReader=Thread("cout-reader",consumeStream,&coutOut);

    while(Process::RUNNING == proc.state()) {
      // consume a little form the stderr
      char buf[128];
      proc.cout().read(buf,sizeof(buf));
      if(proc.cout().gcount()>0) {
	::std::cout << "[COUT] ";
	::std::cout.write(buf,proc.cout().gcount());
      }
      proc.cerr().read(buf,sizeof(buf));
      if(proc.cerr().gcount()>0) {
	::std::cerr << "[CERR] ";
	::std::cerr.write(buf,proc.cerr().gcount());
	::std::cerr << ::std::flush;
      }
    }
    proc.waitFor();
    Thread::join(cerrReader);
    Thread::join(coutReader);
    return proc.state()==Process::EXITED && proc.exitCode()==0;
  }
  catch (const ::std::exception& e) {
    Thread::join(cerrReader);
    Thread::join(coutReader);
    ::std::cerr << "Exception : " << e.what() << ::std::endl;
    return false;
  }
}

// validate a schema all on its own
static bool selfValidateSchema(const string& file)
{ return validateSchema(file,""); }

// validate a schema against XML xsd
static bool validateSchema(const string& file)
{
  // now, validate it against the xml schema 
  return validateSchema("test-data/w3c/XMLSchema.xsd",file);
}


void utest_validateSchema()
{
  string dir("test-data/unit/");

  assert(validateSchema("test-data/w3c/XMLSchema.xsd"));
  assert(validateSchema("test-data/w3c/wsdl.xsd"));
  assert(validateSchema("test-data/w3c/purchaseOrder.xsd"));
  assert(validateSchema("test-data/w3c/soap-envelope.xsd"));

  // my own schema
  assert(validateSchema("schemas/uuid.xsd"));

  assert(validateSchema("test-data/kml/kml21.xsd"));
  assert(validateSchema(dir+"test1.xsd"));
  assert(validateSchema(dir+"test2.xsd"));
  assert(validateSchema(dir+"test3.xsd"));
  assert(validateSchema(dir+"test4.xsd"));
  assert(validateSchema(dir+"test5.xsd"));
  assert(validateSchema(dir+"test6.xsd"));
  assert(validateSchema(dir+"test7.xsd"));
}

void utest_selfValidateSchema()
{
  string dir("test-data/unit/");

  assert(selfValidateSchema("test-data/w3c/XMLSchema.xsd"));
  // my own schema
  assert(selfValidateSchema("schemas/uuid.xsd"));

  assert(selfValidateSchema("test-data/kml/kml21.xsd"));
  assert(selfValidateSchema(dir+"test1.xsd"));
  assert(selfValidateSchema(dir+"test2.xsd"));
  assert(!selfValidateSchema(dir+"test3.xsd"));
  assert(selfValidateSchema(dir+"test4.xsd"));
  assert(selfValidateSchema(dir+"test5.xsd"));
  assert(!selfValidateSchema(dir+"test6.xsd"));
  assert(!selfValidateSchema(dir+"test7.xsd"));
}

void utest_validatKMLSchema()
{
  assert(!validateSchema("test-data/kml/kml21.xsd","test-data/kml/rides/KML_Samples.kml")); // cannot validate this, it's kml2.2
  assert(validateSchema("test-data/kml/kml21.xsd","test-data/kml/doc.kml"));

  assert(validateSchema("schemas/uuid.xsd"));

  assert(validateSchema("http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd","test-data/kml/rides/KML_Samples.kml")); 
}

void utest_unitTests()
{
  string dir("test-data/unit/");
  assert(validateSchema(dir+"test1.xsd",dir+"test1.1.xml"));
  assert(validateSchema(dir+"test1.xsd",dir+"test1.2.xml"));
  assert(validateSchema(dir+"test1.xsd",dir+"test1.xml"));

  assert(!validateSchema(dir+"test2.xsd",dir+"test2.xml")); // totally the wrong data for the schema
  assert(!validateSchema(dir+"test2.xsd",dir+"test2.1.xml")); // cannot use an abstract element

  assert(!validateSchema(dir+"test3.xsd",dir+"test3.1.xml")); // hmmm, schema is not valid

  assert(validateSchema(dir+"test4.xsd",dir+"test4.1.xml"));
  assert(validateSchema(dir+"test5.xsd",dir+"test5.1.xml"));
}


void utest_invalidSchema()
{
}
