#include <xylon/xsdom/Schema.h>
#include <xylon/xsdom/ComplexType.h>
#include <xylon/xsdom/Element.h>
#include <xylon/xsdom/Sequence.h>
#include <xylon/xsdom/FirstSets.h>
#include <xylon/SchemaBuilder.h>
#include <xylon/xsdom/XmlSchema.h>
#include <timber/logging.h>
#include <timber/w3c/xml/xml.h>
#include <set>
#include <algorithm>
#include <iterator>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::timber::w3c;
using namespace ::xylon;
using namespace ::xylon::xsdom;

static Reference< ::xylon::XsdSchema> loadSchema(::std::string url)
{
   try {
      unique_ptr< SchemaBuilder> b = SchemaBuilder::create();
      b->importSchema(::timber::w3c::xml::XML_NAMESPACE, ::timber::w3c::URI::create("file:./test-data/w3c/xml.xsd"));
      b->importSchema(::timber::w3c::URI::create(url));
      b->importSchema(::timber::w3c::URI::create("test-data/w3c/XMLSchema.xsd"));
      b->importSchema(XmlSchema::instance());
      Pointer< ::xylon::XsdSchema> s = b->getSchema();
      assert(s && "Could not load schema");
      return s;
   }
   catch (const ::std::exception& e) {
      cerr << "Exception loading : " << e.what() << endl;
      throw;
   }
}

static Reference< ::xylon::XsdSchema> loadSchemaNoXSD(::std::string url)
{
   try {
      unique_ptr< SchemaBuilder> b = SchemaBuilder::create();
      b->importSchema(::timber::w3c::xml::XML_NAMESPACE, ::timber::w3c::URI::create("file:./test-data/w3c/xml.xsd"));
      b->importSchema(::timber::w3c::URI::create(url));
      b->importSchema(XmlSchema::instance());
      Pointer< ::xylon::XsdSchema> s = b->getSchema();
      assert(s && "Could not load schema");
      return s;
   }
   catch (const ::std::exception& e) {
      cerr << "Exception loading : " << e.what() << endl;
      throw;
   }
}

void utest_1()
{
   Reference< ::xylon::XsdSchema> schemaA = loadSchema("test-data/unit/test_recursiveA.xsd");
   const FirstSets fA(schemaA);
}

void utest_2()
{
   // the two schemas are structurally the same, but the order of declarations are different
   // this causes problems for one variant
   Pointer< ::xylon::XsdSchema> schemaA = loadSchema("test-data/unit/test_recursiveA.xsd");
   Pointer< ::xylon::XsdSchema> schemaB = loadSchema("test-data/unit/test_recursiveB.xsd");

   // we don't have any way to verify that the first sets are equal, at least for now
   const vector< XRef< Type> > tA = schemaA->types();
   const vector< XRef< Type> > tB = schemaB->types();
   assert(tA.size() == tB.size());
   try {

      const FirstSets fA(schemaA);
      const FirstSets fB(schemaB);

      for (size_t i = 0; i < tA.size(); ++i) {
         bool found = false;
         if (tA[i].name().namespaceURI() != "test") {
            continue;
         }
         Reference< ComplexType> cA = schemaA->derefType(tA[i]);
         set< QName> namesA = fA.firstSetOf(cA);
         for (size_t j = 0; j < tB.size(); ++j) {
            if (tB[j].name().namespaceURI() != "test") {
               continue;
            }
            Reference< ComplexType> cB = schemaB->derefType(tB[j]);
            set< QName> namesB = fB.firstSetOf(cB);

            if (cA->qname() == cB->qname()) {
               found = true;

               cerr << "cA,CB: " << cA->qname() << ::std::endl;

               set< QName> intersection;
               set_intersection(namesA.begin(), namesA.end(), namesB.begin(), namesB.end(), insert_iterator<
                     set< QName> > (intersection, intersection.begin()));
               assert(intersection.size() == max(namesA.size(), namesB.size()));
            }
         }
         assert(found);
      }
   }
   catch (const ::std::exception& e) {
      cerr << "1. Exception : " << e.what() << endl;
      throw;
   }
}

void utest_3()
{
   Reference< ::xylon::XsdSchema> schemaA = loadSchema("test-data/unit/test_infinite_recursion.xsd");
   Log("xylon.xs.FirstSets").setLevel(Level::DEBUGGING);
   // schema is ambiguous and so it must fail
   try {
      const FirstSets fA(schemaA);
      assert("Unexpected success" == 0);
   }
   catch (...) {
   }
}

void utest_4()
{
   Reference< ::xylon::XsdSchema> schemaA = loadSchemaNoXSD("test-data/unit/recursiveGroup.xsd");
   Log("xylon.xs.FirstSets").setLevel(Level::INFO);
   // schema is ambiguous and so it must fail
   try {
      const FirstSets fA(schemaA);
      Reference<Element> A = schemaA->findElement(QName("test","A"));
      Reference<Element> B = schemaA->findElement(QName("test","B"));

      Reference<ComplexType> t = A->elementType();
      assert(fA.allowsEmpty(t->enumerable()));
      t = B->elementType();
      assert(fA.allowsEmpty(t->enumerable()));
   }
   catch (...) {
      assert("Unexpected failure" == 0);
   }

}

void utest_5()
{
   Reference< ::xylon::XsdSchema> schemaA = loadSchemaNoXSD("test-data/unit/firstSetTest.xsd");
   Log("xylon.xs.FirstSets").setLevel(Level::INFO);
   // schema is ambiguous and so it must fail
   try {
      const FirstSets fA(schemaA);
      Reference<Element> e = schemaA->findElement(QName("test","Envelope"));
      Reference<ComplexType> t = e->elementType();
      Reference<Sequence> s = t->enumerable();
      assert(!fA.allowsEmpty(s));
   }
   catch (...) {
      assert("Unexpected failure" == 0);
   }

}
