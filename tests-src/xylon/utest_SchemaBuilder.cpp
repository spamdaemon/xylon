#include <timber/w3c/URI.h>
#include <timber/w3c/xml/xml.h>
#include <timber/logging.h>
#include <xylon/SchemaBuilder.h>
#include <xylon/xsdom/nodes.h>
#include <xylon/xsdom/XmlSchema.h>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::xylon;
using namespace ::xylon::xsdom;

static QName makeXSDName(string elmt)
{
   return QName("http://www.w3.org/2001/XMLSchema", elmt);
}

static QName makeNamespaceName(string elmt)
{
   return QName("http://www.w3.org/XML/1998/namespace", elmt);
}

static Reference< ::xylon::XsdSchema> loadSchema(string schema)
{
   unique_ptr< SchemaBuilder> b = SchemaBuilder::create();
   b->importSchema(::timber::w3c::xml::XML_NAMESPACE, ::timber::w3c::URI::create("file:./test-data/w3c/xml.xsd"));
   b->importSchema(::timber::w3c::URI::create("test-data/w3c/XMLSchema.xsd"));
   b->importSchema(XmlSchema::instance());
   Pointer< ::xylon::XsdSchema> s = b->getSchema();
   return s;
}

void utest_test1()
{
   unique_ptr< SchemaBuilder> b = SchemaBuilder::create();
   b->importSchema(::timber::w3c::xml::XML_NAMESPACE, ::timber::w3c::URI::create("file:./test-data/w3c/xml.xsd"));
   b->importSchema(::timber::w3c::URI::create("test-data/w3c/XMLSchema.xsd"));
   ::std::cerr << "Importing XML schema" << ::std::endl;
   b->importSchema(XmlSchema::instance());

   Pointer< ::xylon::XsdSchema> s = b->getSchema();

   // these are from the xsd namespace
   assert(s->findElement(makeXSDName("complexType")) && "complexType not found");
   assert(s->findElement(makeXSDName("element")) && "element not found");
   assert(s->findElement(makeXSDName("annotation")) && "annotation not found");

   assert(s->findType(makeXSDName("openAttrs")) && "complexType not found");
   assert(s->findGroup(makeXSDName("schemaTop")) && "group not found");
   assert(s->findKey(makeXSDName("element")) && "key not found");
   assert(s->findAttributeGroup(makeXSDName("defRef")) && "attributeGroup not found");

   // these are from the namespace
   assert(s->findAttribute(makeNamespaceName("lang")) && "attribute lang not found");
   assert(s->findAttribute(makeNamespaceName("base")) && "attribute base not found");
   assert(s->findAttribute(makeNamespaceName("id")) && "attribute id not found");
   assert(s->findAttributeGroup(makeNamespaceName("specialAttrs")) && "attributeGroup specialAttrs not found");

}

void utest_loadKML()
{
   //::timber::logging::Log("timber.net.SocketStreamBuffer").setLevel(::timber::logging::Level::ALL);
   //::timber::logging::Log("timber.net.SocketStreamBuffer.detail").setLevel(::timber::logging::Level::ALL);
   //::timber::logging::Log("timber.net.http.client.DefaultHttpClient").setLevel(::timber::logging::Level::ALL);

   unique_ptr< SchemaBuilder> b = SchemaBuilder::create();
   b->importSchema(::timber::w3c::xml::XML_NAMESPACE, ::timber::w3c::URI::create("file:./test-data/w3c/xml.xsd"));
   b->importSchema(::timber::w3c::URI::create("test-data/w3c/XMLSchema.xsd"));
   b->importSchema(::timber::w3c::URI::create("http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd"));
   b->importSchema(::timber::w3c::URI::create("test-data/w3c/XMLSchema.xsd"));
   ::std::cerr << "Importing XML schema" << ::std::endl;
   b->importSchema(XmlSchema::instance());
   Pointer< ::xylon::XsdSchema> s = b->getSchema();
}

void utest_loadIncremental()
{
   unique_ptr< SchemaBuilder> b = SchemaBuilder::create();
   ::std::cerr << "Loading xml schema" << ::std::endl;
   b->importSchema(::timber::w3c::xml::XML_NAMESPACE, ::timber::w3c::URI::create("file:./test-data/w3c/xml.xsd"));
   b->importSchema(::timber::w3c::URI::create("test-data/w3c/XMLSchema.xsd"));
   ::std::cerr << "Loading first schema" << ::std::endl;
   b->importSchema(::timber::w3c::URI::create("test-data/unit/incomplete-schema-1.xsd"));
   ::std::cerr << "Loading second schema" << ::std::endl;
   b->importSchema(::timber::w3c::URI::create("test-data/unit/incomplete-schema-2.xsd"));
   ::std::cerr << "Importing XML schema" << ::std::endl;
   b->importSchema(XmlSchema::instance());
   ::std::cerr << "Getting element schema" << ::std::endl;

   Pointer< ::xylon::XsdSchema> s = b->getSchema();
   Pointer< Type> t = s->findType(QName("tns2", "xType"));
   assert(t);
   Pointer< Element> e = s->findElement(QName("tns1", "xElement"));
   assert(e);
   ::std::cerr << "Accessing the type" << ::std::endl;
   assert(e->type().isBound());

}
