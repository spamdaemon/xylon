#include <xylon/rt/StreamWriter.h>
#include <iostream>
#include <sstream>

using namespace std;
using namespace xylon::rt;

void utest_simple()
{
   StreamWriter writer(cout);
   writer.setIndent("  ");
   writer.addNamespace("A");
   writer.beginDocument();
   writer.addNamespace("B");
   writer.beginElement(QName("C","c"));
   writer.addNamespace("D");
   writer.beginElement(QName("E","e"));
   writer.addNamespace("D");
   writer.writeAttribute(QName("F","f"),"FOO");
   writer.beginAttribute(QName("F","g"));
   writer.writeText("BAR");
   writer.writeQNameAttribute(QName("F","f"),QName("G","x"));
   writer.writeText("Hello,world!");
   writer.beginElement(QName("E","FOOBAR"));
   writer.endDocument();
   cout << flush;
}
