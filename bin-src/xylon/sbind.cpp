#include <xylon/xsdom/xsdom.h>
#include <xylon/xsdom/Import.h>
#include <xylon/xsdom/Schema.h>
#include <xylon/xsdom/XmlSchema.h>

#include <xylon/XsdSchema.h>
#include <xylon/SchemaBuilder.h>
#include <xylon/XsdSchemaModel.h>
#include <xylon/ModelFactory.h>

#include <xylon/binding/DefaultBindingProvider.h>
#include <xylon/binding/XmlSchemaBindingProvider.h>
#include <xylon/model/models.h>
#include <xylon/model/PrintModelVisitor.h>
#include <xylon/model/DOMValidator.h>

#include <xylon/code/cpp/SimpleBindingManager.h>
#include <xylon/code/cpp/DefaultBindingFactory.h>

#include <timber/config/XMLConfiguration.h>
#include <timber/config/Configuration.h>
#include <canopy/fs/FileSystem.h>

#include <timber/w3c/URI.h>

#include <sstream>
#include <fstream>
#include <set>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <cctype>

using namespace ::std;
using namespace ::xylon;
using namespace ::xylon::binding;
using namespace ::xylon::model;
using namespace ::xylon::xsdom;
using namespace ::timber;
using namespace ::timber::config;
using namespace ::timber::w3c;
using namespace ::canopy::fs;

static void usage()
{
   cerr << "sbind [-b bindings] <xsdfile> [-o <outdir>]" << endl;
   exit(1);
}

struct ArgumentNotFound : public ::std::exception
{
      ArgumentNotFound(const char* argName) :
         _message("Argument not found : " + string(argName))
      {
      }

      ArgumentNotFound(const ::std::string& argName) :
         _message("Argument not found : " + argName)
      {
      }

      ~ArgumentNotFound() throw()
      {
      }

      const char* what() const throw()
      {
         return _message.c_str();
      }

   private:
      const ::std::string _message;

};

struct ArgParser
{
      typedef ::std::vector< const char*> Arguments;

      ArgParser(size_t argc, const char** argv)
      {
         for (size_t i = 1; i < argc; ++i) {
            _args.push_back(argv[i]);
         }
      }

      bool consumeSwitch(const string& option)
      {
         for (Arguments::iterator i = _args.begin(); i != _args.end(); ++i) {
            if ((*i) == option) {
               _args.erase(i);
               return true;
            }
         }
         return false;
      }

      bool consumeBoolArg(const string& option)
      {
         for (size_t i = 0, sz = _args.size(); i < sz; ++i) {
            if (_args[i] == option) {
               if (i + 1 == sz) {
                  throw ::std::runtime_error("Missing argument for option " + option);
               }
               _args.erase(_args.begin() + i);

               if (_args[i] == string("false")) {
                  _args.erase(_args.begin() + i);
                  return false;
               }
               if (_args[i] == string("true")) {
                  _args.erase(_args.begin() + i);
                  return true;
               }
               throw ::std::runtime_error("Expected 'false' or 'true' for option " + option);
            }
         }
         throw ArgumentNotFound(option);
      }

      ::canopy::Int64 consumeIntArg(const string& option)
      {
         for (size_t i = 0, sz = _args.size(); i < sz; ++i) {
            if (_args[i] == option) {
               if (i + 1 == sz) {
                  throw ::std::runtime_error("Missing argument for option" + option);
               }
               istringstream in(_args[i]);
               _args.erase(_args.begin() + i);
               ::canopy::Int64 res;
               in >> res;
               if (in.fail()) {
                  throw ::std::runtime_error("Expected 'false' or 'true' for option " + option);
               }
               _args.erase(_args.begin() + i);
               return res;
            }
         }
         throw ArgumentNotFound(option);
      }

      ::canopy::Int64 consumeIntArg(const char* option, ::canopy::Int64 def)
      {
         try {
            return consumeIntArg(option);
         }
         catch (const ArgumentNotFound&) {
            return def;
         }
      }

      const char* consumeStringArg(const string& option, bool optional = false)
      {
         for (size_t i = 0, sz = _args.size(); i < sz; ++i) {
            if (_args[i] == option) {
               if (i + 1 == sz) {
                  throw ::std::runtime_error("Missing argument for option" + option);
               }
               _args.erase(_args.begin() + i);
               const char* res = _args[i];
               _args.erase(_args.begin() + i);
               return res;
            }
         }
         if (optional) {
            return 0;
         }
         throw ArgumentNotFound(option);
      }

      const char* consumeStringArg(const char* option, const char* def)
      {
         const char* arg = consumeStringArg(option, true);
         if (arg) {
            return arg;
         }
         else {
            return def;
         }
      }

      void checkUnknownOptions()
      {
         for (size_t i = 0; i < _args.size(); ++i) {
            if (_args[i][0] == '-') {
               throw ::std::runtime_error("Unknown option " + string(_args[i]));
            }
         }
      }

      ::std::vector< const char*> consumeArgs(size_t expected)
      {
         checkUnknownOptions();
         ::std::vector< const char*> res;
         for (size_t i = 0; i < _args.size(); ++i) {
            res.push_back(_args[i]);
         }
         _args.clear();
         if (res.size() < expected) {
            throw ::std::runtime_error("Too few arguments");
         }

         return res;
      }


   private:
      Arguments _args;
};

int main(int argc, const char** argv)
{
   const char* outdir;
   vector< const char*> infiles;
   const char* bindingFile;
   if (argc < 2) {
      usage();
   }
   try {
      ArgParser args(argc, argv);
      if (args.consumeSwitch("-h")) {
         usage();
      }

      bindingFile = args.consumeStringArg("-b", true);
      outdir = args.consumeStringArg("-o", true);

      infiles = args.consumeArgs(1);


      FileSystem fs;


      auto cfg = Configuration::create();
      if (bindingFile) {
         cfg = XMLConfiguration::loadXML(bindingFile);
      }
      if (outdir) {
         cfg->set("destination-dir",outdir);
      }
      auto builder = SchemaBuilder::create();
      try {
         for (size_t i = 0; i < infiles.size(); ++i) {
            try {
               ::std::cerr << "Importing schema " << infiles[i] << ::std::endl;
               auto uri =  ::timber::w3c::URI::create(infiles[i]);
               if (!uri->scheme()) {
                  // assume it's a file
                  string absfile = fs.getAbsolutePath(infiles[i]);
                  uri = ::timber::w3c::URI::create(string("file:")+absfile);
               }
               builder->importSchema(uri);
            }
            catch (const exception& e) {
               ::std::cerr << "Caught exception while building schema : " << infiles[i] << endl << "Exception was: "
                     << e.what() << ::std::endl;
               return 1;
            }
         }
         //         builder->importSchema(XSD_NAMESPACE_URI, ::timber::w3c::URI::create("http://www.w3.org/2001/XMLSchema.xsd"));
         builder->importSchema(XmlSchema::instance());
      }
      catch (const exception& e) {
         ::std::cerr << "Caught exception while building schema : " << e.what() << ::std::endl;
         return 1;
      }
      Pointer< ::xylon::XsdSchema> schema = builder->getSchema();
      if (!schema) {
         ::std::cerr << "No schema built" << ::std::endl;
         return 2;
      }

      // make sure we have an XSD
      unique_ptr< ModelFactory> bindings = ModelFactory::create();

      vector< String> namespaces = schema->schemas();

      bindings->registerBindingProvider(XmlSchemaBindingProvider::create());
      for (size_t k = 0; k < namespaces.size(); ++k) {
         bindings->registerBindingProvider(unique_ptr< BindingProvider> (new DefaultBindingProvider(namespaces[k])));
      }

      ::timber::Reference< XsdSchemaModel> schemaModel = bindings->createModel(schema);

      unique_ptr< xylon::code::cpp::BindingFactory> bf = ::xylon::code::cpp::DefaultBindingFactory::create(cfg);
      Reference< xylon::code::cpp::BindingManager> codeGen = xylon::code::cpp::SimpleBindingManager::create(move(bf), cfg);
      codeGen->bindModel(schemaModel);
      codeGen->emitCode();
      return 0;
   }
   catch (const ArgumentNotFound& arg) {
      cerr << arg.what() << endl;
      return 1;
   }
   catch (const exception& e) {
      cerr << "Exception caught : " << e.what() << endl;
      return 2;
   }

}
