#include <xylon/xsdom/Schema.h>
#include <xylon/xsdom/nodes.h>
#include <xylon/xsdom/XmlSchema.h>

#include <xylon/XsdSchema.h>
#include <xylon/SchemaBuilder.h>
#include <xylon/XsdSchemaModel.h>
#include <xylon/ModelFactory.h>

#include <xylon/model/DOMValidator.h>
#include <xylon/model/models.h>
#include <xylon/binding/XmlSchemaBindingProvider.h>
#include <xylon/binding/DefaultBindingProvider.h>

#include <timber/w3c/URI.h>
#include <timber/w3c/xml/xml.h>
#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/dom/Document.h>
#include <timber/w3c/xml/dom/DocumentParser.h>
#include <timber/ios/URIInputStream.h>

#include <timber/w3c/xml/dom/dom.h>
#include <timber/logging.h>

#include <sstream>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <cctype>

using namespace ::std;
using namespace ::xylon;
using namespace ::xylon::binding;
using namespace ::xylon::xsdom;
using namespace ::xylon::model;
using namespace ::xylon::xsdom;
using namespace ::timber;
using namespace ::timber::w3c;
using namespace ::timber::ios;
using namespace ::timber::logging;

static void usage()
{
   cerr << "validate <xsdfile> <file1> <file2> ...." << endl;
   exit(1);
}

struct ArgumentNotFound : public ::std::exception
{
      ArgumentNotFound(const char* argName) :
         _message("Argument not found : " + string(argName))
      {
      }

      ArgumentNotFound(const ::std::string& argName) :
         _message("Argument not found : " + argName)
      {
      }

      ~ArgumentNotFound() throw()
      {
      }

      const char* what() const throw()
      {
         return _message.c_str();
      }

   private:
      const ::std::string _message;

};

struct ArgParser
{
      typedef ::std::vector< const char*> Arguments;

      ArgParser(size_t argc, const char** argv)
      {
         for (size_t i = 1; i < argc; ++i) {
            _args.push_back(argv[i]);
         }
      }

      bool consumeSwitch(const string& option)
      {
         for (Arguments::iterator i = _args.begin(); i != _args.end(); ++i) {
            if ((*i) == option) {
               _args.erase(i);
               return true;
            }
         }
         return false;
      }

      bool consumeBoolArg(const string& option)
      {
         for (size_t i = 0, sz = _args.size(); i < sz; ++i) {
            if (_args[i] == option) {
               if (i + 1 == sz) {
                  throw ::std::runtime_error("Missing argument for option" + option);
               }
               _args.erase(_args.begin() + i);

               if (_args[i] == string("false")) {
                  _args.erase(_args.begin() + i);
                  return false;
               }
               if (_args[i] == string("true")) {
                  _args.erase(_args.begin() + i);
                  return true;
               }
               throw ::std::runtime_error("Expected 'false' or 'true' for option " + option);
            }
         }
         throw ArgumentNotFound(option);
      }

      ::canopy::Int64 consumeIntArg(const string& option)
      {
         for (size_t i = 0, sz = _args.size(); i < sz; ++i) {
            if (_args[i] == option) {
               if (i + 1 == sz) {
                  throw ::std::runtime_error("Missing argument for option" + option);
               }
               istringstream in(_args[i]);
               _args.erase(_args.begin() + i);
               ::canopy::Int64 res;
               in >> res;
               if (in.fail()) {
                  throw ::std::runtime_error("Expected 'false' or 'true' for option " + option);
               }
               _args.erase(_args.begin() + i);
               return res;
            }
         }
         throw ArgumentNotFound(option);
      }

      ::canopy::Int64 consumeIntArg(const char* option, ::canopy::Int64 def)
      {
         try {
            return consumeIntArg(option);
         }
         catch (const ArgumentNotFound&) {
            return def;
         }
      }

      const char* consumeStringArg(const string& option)
      {
         for (size_t i = 0, sz = _args.size(); i < sz; ++i) {
            if (_args[i] == option) {
               if (i + 1 == sz) {
                  throw ::std::runtime_error("Missing argument for option" + option);
               }
               _args.erase(_args.begin() + i);
               const char* res = _args[i];
               _args.erase(_args.begin() + i);
               return res;
            }
         }
         throw ArgumentNotFound(option);
      }

      const char* consumeStringArg(const char* option, const char* def)
      {
         try {
            return consumeStringArg(option);
         }
         catch (const ArgumentNotFound&) {
            return def;
         }
      }

      void checkUnknownOptions()
      {
         for (size_t i = 0; i < _args.size(); ++i) {
            if (_args[i][0] == '-') {
               throw ::std::runtime_error("Unknown option " + string(_args[i]));
            }
         }
      }

      ::std::vector< string> consumeArgs(size_t expected)
      {
         checkUnknownOptions();
         ::std::vector< string> res;
         for (size_t i = 0; i < _args.size(); ++i) {
            res.push_back(_args[i]);
         }
         _args.clear();
         if (res.size() < expected) {
            throw ::std::runtime_error("Too few arguments");
         }

         return res;
      }

   private:
      Arguments _args;
};

typedef ::timber::w3c::xml::dom::String DOMString;
typedef ::timber::w3c::xml::dom::Document Document;

::timber::Reference< Document> readFile(const ::timber::SharedRef< URI>& src) throws (::std::exception)
{
   URIInputStream in(src);

   auto parser = ::timber::w3c::xml::Parser::create();
   ::timber::w3c::xml::dom::DocumentParser docBuilder(false, true);
   parser->parseStream(in, docBuilder);

   return docBuilder.getDocument();
}

int main(int argc, const char** argv)
{
   vector< string> files;
   Log("xylon.xs.dom.Validator").setLevel(Level::INFO);
   Log("xylon.xs.FirstSets").setLevel(Level::INFO);

   if (argc < 2) {
      usage();
   }
   Pointer< ::xylon::XsdSchema> schema;

   try {
      ArgParser args(argc, argv);
      if (args.consumeSwitch("-h")) {
         usage();
      }

      files = args.consumeArgs(1);

      {
         const string infile(files[0]);
         unique_ptr< SchemaBuilder> builder = SchemaBuilder::create();
         try {
            builder->importSchema(::timber::w3c::xml::XML_NAMESPACE, ::timber::w3c::URI::create("file:./test-data/w3c/xml.xsd"));
            builder->importSchema(::timber::w3c::URI::create(infile));
            builder->importSchema(XSD_NAMESPACE_URI, ::timber::w3c::URI::create("http://www.w3.org/2001/XMLSchema.xsd"));
            builder->importSchema(XmlSchema::instance());
         }
         catch (const exception& e) {
            ::std::cerr << "Caught exception while building schema : " << infile << endl << "Exception was: "
                  << e.what() << ::std::endl;
            return 1;
         }
         schema = builder->getSchema();
         if (!schema) {
            ::std::cerr << "No schema built" << ::std::endl;
            return 2;
         }
      }
      unique_ptr< ModelFactory> bindings = ModelFactory::create();

      vector< String> namespaces = schema->schemas();
      for (size_t k = 0; k < namespaces.size(); ++k) {
         bindings->registerBindingProvider(unique_ptr< BindingProvider> (new DefaultBindingProvider(namespaces[k])));
      }
      bindings->registerBindingProvider(XmlSchemaBindingProvider::create());

      ::std::unique_ptr< DOMValidator> v = DOMValidator::create(bindings->createModel(
            schema));

      int res = 0;
      for (size_t i = 1; i < files.size(); ++i) {
         auto doc = readFile(URI::create(files[i]));
         try {
            v->validate(doc->documentElement());
            ::std::cerr << "OK    " << ' ' << files[i] << ::std::endl;
         }
         catch (const exception& e) {
            ::std::cerr << "FAILED" << ' ' << files[i] << ::std::endl;
            ::std::cerr << e.what() << ::std::endl;
            res = 1;
         }
      }
      return res;
   }
   catch (const ArgumentNotFound& arg) {
      cerr << arg.what() << endl;
      return 1;
   }
   catch (const exception& e) {
      cerr << "Exception caught : " << e.what() << endl;
      return 2;
   }

}
