TEST_INCLUDES=-I../../lib-src/ -I../../gen-lib-src/ -I../../../timber/lib-src/ -I../../../canopy/lib-src/
TEST_LIBRARIES=../../lib/libxylon.so ../../../timber/lib/libtimber.so ../../../canopy/lib/libcanopy.so

# a local target
.PHONY: kml list purchaseOrder

kml: $(BINARIES) demos-src/kml/driver.cpp 
	mkdir -p demos/$@;
	compile-schema -o demos/$@/schemas test-data/kml/kml21.xsd test-data/kml/kml22beta.xsd test-data/kml/ogckml22.xsd;
	(cd demos/$@ && gcc -g3 -c ../../demos-src/$@/driver.cpp -I schemas $(TEST_INCLUDES)  -I../../demos/$@)
	(cd demos/$@ && gcc -g3 -o $@ driver.o schemas/*.o $(TEST_LIBRARIES) )
	(cd demos/$@ && $@ ../../test-data/kml/doc.kml)
	(cd demos/$@ && $@ ../../test-data/kml/rides/KML_Samples.kml)

purchaseOrder: $(BINARIES) test-data/w3c/purchaseOrder.xsd demos-src/purchaseOrder/driver.cpp test-data/w3c/purchase1.xml
	mkdir -p demos/$@;
	(cd demos/$@ && ../../bin/xylon/gen/xs/cpp/schemagen  ../../test-data/w3c/purchaseOrder.xsd)
	(cd demos/$@ && gcc -g3 -c ../../demos-src/$@/driver.cpp schema.cpp parser.cpp $(TEST_INCLUDES)  -I../../demos/$@)
	(cd demos/$@ && gcc -g3 -o $@ driver.o schema.o parser.o $(TEST_LIBRARIES) )
	(cd demos/$@ && $@ ../../test-data/w3c/purchase1.xml)

list: $(BINARIES)
	mkdir -p demos/$@;
	(cd demos/$@ && ../../bin/xylon/gen/xs/cpp/schemagen  ../../demos-src/$@/schema.xsd)
	(cd demos/$@ && gcc -g3 -c ../../demos-src/$@/main.cpp schema.cpp parser.cpp $(TEST_INCLUDES)  -I../../demos/$@)
	(cd demos/$@ && gcc -g3 -o $@ main.o schema.o parser.o $(TEST_LIBRARIES) )
	(cd demos/$@ && $@ ../../demos-src/$@/data.xml)

any: $(BINARIES)
	mkdir -p demos/$@;
	(cd demos/$@ && ../../bin/xylon/gen/xs/cpp/schemagen  ../../demos-src/$@/schema.xsd)
	(cd demos/$@ && gcc -g3 -c ../../demos-src/$@/main.cpp schema.cpp parser.cpp $(TEST_INCLUDES)  -I../../demos/$@)
	(cd demos/$@ && gcc -g3 -o $@ main.o schema.o parser.o $(TEST_LIBRARIES) )
	(cd demos/$@ && $@ ../../demos-src/$@/data.xml)






