#ifndef _XSD_H
#define _XSD_H

#ifndef _XSD_TYPES_H
#include <xsd/types.h>
#endif

#ifndef _XYLON_RT_ANYSIMPLETYPE_H
#include <xylon/rt/AnySimpleType.h>
#endif

#ifndef _XYLON_RT_ANYELEMENT_H
#include <xylon/rt/AnyElement.h>
#endif

#ifndef _XYLON_RT_LIST_H
#include <xylon/rt/List.h>
#endif

namespace xsd {

}
#endif
