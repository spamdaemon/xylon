#ifndef XSD_TYPEIO_H
#define XSD_TYPEIO_H

#include <iosfwd>

#ifndef _XYLON_RT_PARSEERROR
#include <xylon/rt/ParseError.h>
#endif

namespace xsd {

  class String;
  void printXSDType(const String& value, ::std::ostream& out);
  void readXSDType(String& value, ::std::istream& in) ;

  class Boolean;
  void printXSDType(const Boolean& value, ::std::ostream& out);
  void readXSDType(Boolean& value, ::std::istream& in) ;

  class Float;
  void printXSDType(const Float& value, ::std::ostream& out);
  void readXSDType(Float& value, ::std::istream& in) ;

  class Double;
  void printXSDType(const Double& value, ::std::ostream& out);
  void readXSDType(Double& value, ::std::istream& in) ;

  class Decimal;
  void printXSDType(const Decimal& value, ::std::ostream& out);
  void readXSDType(Decimal& value, ::std::istream& in) ;

  class Duration;
  void printXSDType(const Duration& value, ::std::ostream& out);
  void readXSDType(Duration& value, ::std::istream& in) ;

  class DateTime;
  void printXSDType(const DateTime& value, ::std::ostream& out);
  void readXSDType(DateTime& value, ::std::istream& in) ;

  class Time;
  void printXSDType(const Time& value, ::std::ostream& out);
  void readXSDType(Time& value, ::std::istream& in) ;

  class Date;
  void printXSDType(const Date& value, ::std::ostream& out);
  void readXSDType(Date& value, ::std::istream& in) ;

  class GYearMonth;
  void printXSDType(const GYearMonth& value, ::std::ostream& out);
  void readXSDType(GYearMonth& value, ::std::istream& in) ;

  class GYear;
  void printXSDType(const GYear& value, ::std::ostream& out);
  void readXSDType(GYear& value, ::std::istream& in) ;

  class GMonthDay;
  void printXSDType(const GMonthDay& value, ::std::ostream& out);
  void readXSDType(GMonthDay& value, ::std::istream& in) ;

  class GDay;
  void printXSDType(const GDay& value, ::std::ostream& out);
  void readXSDType(GDay& value, ::std::istream& in) ;

  class GMonth;
  void printXSDType(const GMonth& value, ::std::ostream& out);
  void readXSDType(GMonth& value, ::std::istream& in) ;

  class HexBinary;
  void printXSDType(const HexBinary& value, ::std::ostream& out);
  void readXSDType(HexBinary& value, ::std::istream& in) ;

  class Base64Binary;
  void printXSDType(const Base64Binary& value, ::std::ostream& out);
  void readXSDType(Base64Binary& value, ::std::istream& in) ;

  class AnyURI;
  void printXSDType(const AnyURI& value, ::std::ostream& out);
  void readXSDType(AnyURI& value, ::std::istream& in) ;

  class QName;
  void printXSDType(const QName& value, ::std::ostream& out);
  void readXSDType(QName& value, ::std::istream& in) ;

  class NOTATION;
  void printXSDType(const NOTATION& value, ::std::ostream& out);
  void readXSDType(NOTATION& value, ::std::istream& in) ;

  class NormalizedString;
  void printXSDType(const NormalizedString& value, ::std::ostream& out);
  void readXSDType(NormalizedString& value, ::std::istream& in) ;

  class Token;
  void printXSDType(const Token& value, ::std::ostream& out);
  void readXSDType(Token& value, ::std::istream& in) ;

  class Language;
  void printXSDType(const Language& value, ::std::ostream& out);
  void readXSDType(Language& value, ::std::istream& in) ;

  class IDREFS;
  void printXSDType(const IDREFS& value, ::std::ostream& out);
  void readXSDType(IDREFS& value, ::std::istream& in) ;

  class ENTITIES;
  void printXSDType(const ENTITIES& value, ::std::ostream& out);
  void readXSDType(ENTITIES& value, ::std::istream& in) ;

  class NMTOKEN;
  void printXSDType(const NMTOKEN& value, ::std::ostream& out);
  void readXSDType(NMTOKEN& value, ::std::istream& in) ;

  class NMTOKENS;
  void printXSDType(const NMTOKENS& value, ::std::ostream& out);
  void readXSDType(NMTOKENS& value, ::std::istream& in) ;

  class Name;
  void printXSDType(const Name& value, ::std::ostream& out);
  void readXSDType(Name& value, ::std::istream& in) ;

  class NCName;
  void printXSDType(const NCName& value, ::std::ostream& out);
  void readXSDType(NCName& value, ::std::istream& in) ;

  class ID;
  void printXSDType(const ID& value, ::std::ostream& out);
  void readXSDType(ID& value, ::std::istream& in) ;

  class IDREF;
  void printXSDType(const IDREF& value, ::std::ostream& out);
  void readXSDType(IDREF& value, ::std::istream& in) ;

  class ENTITY;
  void printXSDType(const ENTITY& value, ::std::ostream& out);
  void readXSDType(ENTITY& value, ::std::istream& in) ;

  class Integer;
  void printXSDType(const Integer& value, ::std::ostream& out);
  void readXSDType(Integer& value, ::std::istream& in) ;

  class NonPositiveInteger;
  void printXSDType(const NonPositiveInteger& value, ::std::ostream& out);
  void readXSDType(NonPositiveInteger& value, ::std::istream& in) ;

  class NegativeInteger;
  void printXSDType(const NegativeInteger& value, ::std::ostream& out);
  void readXSDType(NegativeInteger& value, ::std::istream& in) ;

  class Long;
  void printXSDType(const Long& value, ::std::ostream& out);
  void readXSDType(Long& value, ::std::istream& in) ;

  class Int;
  void printXSDType(const Int& value, ::std::ostream& out);
  void readXSDType(Int& value, ::std::istream& in) ;

  class Short;
  void printXSDType(const Short& value, ::std::ostream& out);
  void readXSDType(Short& value, ::std::istream& in) ;

  class Byte;
  void printXSDType(const Byte& value, ::std::ostream& out);
  void readXSDType(Byte& value, ::std::istream& in) ;

  class NonNegativeInteger;
  void printXSDType(const NonNegativeInteger& value, ::std::ostream& out);
  void readXSDType(NonNegativeInteger& value, ::std::istream& in) ;

  class UnsignedLong;
  void printXSDType(const UnsignedLong& value, ::std::ostream& out);
  void readXSDType(UnsignedLong& value, ::std::istream& in) ;

  class UnsignedInt;
  void printXSDType(const UnsignedInt& value, ::std::ostream& out);
  void readXSDType(UnsignedInt& value, ::std::istream& in) ;

  class UnsignedShort;
  void printXSDType(const UnsignedShort& value, ::std::ostream& out);
  void readXSDType(UnsignedShort& value, ::std::istream& in) ;

  class UnsignedByte;
  void printXSDType(const UnsignedByte& value, ::std::ostream& out);
  void readXSDType(UnsignedByte& value, ::std::istream& in) ;

  class PositiveInteger;
  void printXSDType(const PositiveInteger& value, ::std::ostream& out);
  void readXSDType(PositiveInteger& value, ::std::istream& in) ;

}
#endif
