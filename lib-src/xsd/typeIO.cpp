#include <xsd/typeIO.h>
#include <xsd/types.h>
#include <iostream>
#include <cassert>

using namespace ::std;

namespace xsd {
   void printXSDType(const String& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(String& value, istream& in)
   {
      String::NativeType tmp;
      getline(in, tmp);
      in.clear();
      value.move(tmp);
   }
   void printXSDType(const Boolean& value, ostream& out)
   {
      if (value.value()) {
         out << "true";
      }
      else {
         out << "false";
      }
   }
   void readXSDType(Boolean& value, istream& in)
   {
      ::std::string tmp;
      in >> tmp;
      if (tmp == "false" || tmp == "0") {
         value = false;
      }
      else if (tmp == "true" || tmp == "1") {
         value = true;
      }
      else {
         throw ::xylon::rt::ParseError("Invalid boolean value " + tmp);
      }
   }
   void printXSDType(const Float& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Float& value, istream& in)
   {
      Float::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Double& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Double& value, istream& in)
   {
      Double::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Decimal& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Decimal& value, istream& in)
   {
      Decimal::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Duration& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Duration& value, istream& in)
   {
      Duration::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const DateTime& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(DateTime& value, istream& in)
   {
      DateTime::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Time& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Time& value, istream& in)
   {
      Time::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Date& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Date& value, istream& in)
   {
      Date::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const GYearMonth& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(GYearMonth& value, istream& in)
   {
      GYearMonth::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const GYear& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(GYear& value, istream& in)
   {
      GYear::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const GMonthDay& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(GMonthDay& value, istream& in)
   {
      GMonthDay::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const GDay& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(GDay& value, istream& in)
   {
      GDay::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const GMonth& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(GMonth& value, istream& in)
   {
      GMonth::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const HexBinary& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(HexBinary& value, istream& in)
   {
      HexBinary::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Base64Binary& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Base64Binary& value, istream& in)
   {
      Base64Binary::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const AnyURI& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(AnyURI& value, istream& in)
   {
      AnyURI::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const QName& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(QName& value, istream& in)
   {
      QName::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const NOTATION& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(NOTATION& value, istream& in)
   {
      NOTATION::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const NormalizedString& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(NormalizedString& value, istream& in)
   {
      NormalizedString::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Token& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Token& value, istream& in)
   {
      Token::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Language& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Language& value, istream& in)
   {
      Language::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const IDREFS& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(IDREFS& value, istream& in)
   {
      IDREFS::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const ENTITIES& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(ENTITIES& value, istream& in)
   {
      ENTITIES::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const NMTOKEN& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(NMTOKEN& value, istream& in)
   {
      NMTOKEN::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const NMTOKENS& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(NMTOKENS& value, istream& in)
   {
      NMTOKENS::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Name& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Name& value, istream& in)
   {
      Name::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const NCName& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(NCName& value, istream& in)
   {
      NCName::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const ID& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(ID& value, istream& in)
   {
      ID::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const IDREF& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(IDREF& value, istream& in)
   {
      IDREF::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const ENTITY& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(ENTITY& value, istream& in)
   {
      ENTITY::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Integer& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Integer& value, istream& in)
   {
      Integer::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const NonPositiveInteger& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(NonPositiveInteger& value, istream& in)
   {
      NonPositiveInteger::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const NegativeInteger& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(NegativeInteger& value, istream& in)
   {
      NegativeInteger::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Long& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Long& value, istream& in)
   {
      Long::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Int& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Int& value, istream& in)
   {
      Int::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Short& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Short& value, istream& in)
   {
      Short::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const Byte& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(Byte& value, istream& in)
   {
      Byte::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const NonNegativeInteger& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(NonNegativeInteger& value, istream& in)
   {
      NonNegativeInteger::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const UnsignedLong& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(UnsignedLong& value, istream& in)
   {
      UnsignedLong::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const UnsignedInt& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(UnsignedInt& value, istream& in)
   {
      UnsignedInt::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const UnsignedShort& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(UnsignedShort& value, istream& in)
   {
      UnsignedShort::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const UnsignedByte& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(UnsignedByte& value, istream& in)
   {
      UnsignedByte::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
   void printXSDType(const PositiveInteger& value, ostream& out)
   {
      out << value.value();
   }
   void readXSDType(PositiveInteger& value, istream& in)
   {
      PositiveInteger::NativeType tmp;
      in >> tmp;
      value.move(tmp);
   }
}
