#ifndef _XYLON_MODEL_INSTRUCTIONVISITOR_H
#define _XYLON_MODEL_INSTRUCTIONVISITOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {
         // parse instructions
         class AbstractInstruction;
         class Bind;
         class BindAttribute;
         class BindElement;
         class Comment;
         class CreateAndParse;
         class MoveCursor;
         class Error;
         class ForeachAttribute;
         class Message;
         class Loop;
         class IterateList;
         class NewInstance;
         class OnAll;
         class OnAttribute;
         class OnAttributeNamespace;
         class OnElement;
         class OnElementNamespace;
         class OnValidElement;
         class OnXsiType;
         class ParseInstance;
         class ParseNewInstance;
         class ActivateValue;
         class ActivateAttribute;
         class ActivateElement;
         class Sequence;
         class BuiltinInstruction;

         // write instructions
         class StartWriteObject;
         class EndWriteObject;
         class WriteValue;
         class WriteAttribute;
         class WriteBaseType;
      }

      /**
       * A visitor for different xs nodes
       */
      class InstructionVisitor
      {

            /** Default constructor */
         public:
            InstructionVisitor() throws();

            /** Destructor */
         public:
            virtual ~InstructionVisitor() throws() = 0;

            virtual void
            visitBuiltin(const ::timber::Reference< ::xylon::model::instructions::BuiltinInstruction>& obj);
            virtual void visitComment(const ::timber::Reference< ::xylon::model::instructions::Comment>& obj);
            virtual void visitAbstractInstruction(const ::timber::Reference<
                  ::xylon::model::instructions::AbstractInstruction>& obj);
            virtual void visitBind(const ::timber::Reference< ::xylon::model::instructions::Bind>& obj);
            virtual void visitBindAttribute(
                  const ::timber::Reference< ::xylon::model::instructions::BindAttribute>& obj);
            virtual void visitBindElement(const ::timber::Reference< ::xylon::model::instructions::BindElement>& obj);
            virtual void visitNextElement(const ::timber::Reference< ::xylon::model::instructions::MoveCursor>& obj);
            virtual void visitError(const ::timber::Reference< ::xylon::model::instructions::Error>& obj);
            virtual void visitMessage(const ::timber::Reference< ::xylon::model::instructions::Message>& obj);
            virtual void visitLoop(const ::timber::Reference< ::xylon::model::instructions::Loop>& obj);
            virtual void visitForeachAttribute(const ::timber::Reference<
                  ::xylon::model::instructions::ForeachAttribute>& obj);
            virtual void visitNewInstance(const ::timber::Reference< ::xylon::model::instructions::NewInstance>& obj);
            virtual void visitOnAll(const ::timber::Reference< ::xylon::model::instructions::OnAll>& obj);
            virtual void visitOnAttribute(const ::timber::Reference< ::xylon::model::instructions::OnAttribute>& obj);
            virtual void visitOnAttributeNamespace(const ::timber::Reference<
                  ::xylon::model::instructions::OnAttributeNamespace>& obj);
            virtual void visitOnElement(const ::timber::Reference< ::xylon::model::instructions::OnElement>& obj);
            virtual void visitOnElementNamespace(const ::timber::Reference<
                  ::xylon::model::instructions::OnElementNamespace>& obj);
            virtual void visitOnXsiType(const ::timber::Reference< ::xylon::model::instructions::OnXsiType>& obj);
            virtual void visitOnValidElement(
                  const ::timber::Reference< ::xylon::model::instructions::OnValidElement>& obj);
            virtual void visitActivateValue(
                  const ::timber::Reference< ::xylon::model::instructions::ActivateValue>& obj);
            virtual void visitActivateAttribute(const ::timber::Reference<
                  ::xylon::model::instructions::ActivateAttribute>& obj);
            virtual void visitActivateElement(
                  const ::timber::Reference< ::xylon::model::instructions::ActivateElement>& obj);
            virtual void visitIterateList(const ::timber::Reference< ::xylon::model::instructions::IterateList>& obj);
            virtual void visitParseInstance(
                  const ::timber::Reference< ::xylon::model::instructions::ParseInstance>& obj);
            virtual void visitParseNewInstance(const ::timber::Reference<
                  ::xylon::model::instructions::ParseNewInstance>& obj);
            virtual void visitSequence(const ::timber::Reference< ::xylon::model::instructions::Sequence>& obj);
            virtual void visitCreateAndParse(
                  const ::timber::Reference< ::xylon::model::instructions::CreateAndParse>& obj);

            virtual void visitStartWriteObject(const ::timber::Reference<
                  ::xylon::model::instructions::StartWriteObject>& obj);
            virtual void visitEndWriteObject(
                  const ::timber::Reference< ::xylon::model::instructions::EndWriteObject>& obj);
            virtual void visitWriteValue(const ::timber::Reference< ::xylon::model::instructions::WriteValue>& obj);
            virtual void visitWriteAttribute(
                  const ::timber::Reference< ::xylon::model::instructions::WriteAttribute>& obj);
            virtual void visitWriteBaseType(
                  const ::timber::Reference< ::xylon::model::instructions::WriteBaseType>& obj);
      };
   }
}

#endif

