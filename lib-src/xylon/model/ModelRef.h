#ifndef _XYLON_MODEL_MODELREF_H
#define _XYLON_MODEL_MODELREF_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#include <ostream>

namespace xylon {
   namespace model {
      /**
       * This type of XRef, called a ModelRef, is used to represent indirect references
       * to elements. The reason for not using an XRef is that a ModelRef does not have a name
       * by which to refer to the reference.
       */
      template<class T> struct ModelRef
      {
            /** The reference implementation */
         private:
            struct Impl : public ::timber::Counted
            {
                  Impl()
                  {
                  }

                  ~Impl() throws()
                  {
                  }

                  /**
                   * Access the real object
                   * @return a reference to the real object
                   */
               public:
                  inline const T& ref() const
                  {
                     if (_object) {
                        return *_object;
                     }
                     throw ::std::runtime_error("ModelRef was not bound");
                  }

                  /** The pointer to the real object */
               public:
                  ::timber::Pointer< T> _object;
            };

            /**
             * Create a new ModelRef whose reference cannot be modified.
             */
         public:
            ModelRef() throws() :
               _modifiable(false)
            {
            }

            /**
             * Create a ModelRef that is already bound and cannot be modified.
             * @param obj the object to which this reference is bound
             */
         public:
            ModelRef(::timber::Reference< T> obj) throws() :
               _target(new Impl()), _modifiable(false)
            {
               _target->_object = obj;
               assert(isBound());
            }

            /**
             * Create a ModelRef that is already bound
             * @param obj the object to which this reference is bound
             * @param modifiable true if the created ModelRef can be modified
             */
         public:
            ModelRef(::timber::Pointer< T> obj, bool modifiable) throws() :
               _target(new Impl()), _modifiable(modifiable)
            {
              ::timber::Pointer< T>::swap(obj, _target->_object);
            }

            /**
             * Create a copy of a ModelRef, possibly changing  its modifiability. Modifiability
             * cannot be changed from false to true. Therefore, the only value for
             * modifiable, which makes any sense is <em>false</em>. </br>
             * This constructor is typically used to pass the reference back to a client, but
             * retain the original reference as a modifiable object.
             *
             * @param orig the original ModelRef
             * @param modifiable true if the ModelRef should be modifiable, false otherwise.
             */
         public:
            ModelRef(const ModelRef& orig, bool modifiable) throws() :
               _target(orig._target), _modifiable(orig._modifiable && modifiable)
            {
            }

            /** Copy constructor */
         public:
            ModelRef(const ModelRef& orig) throws() :
               _target(orig._target), _modifiable(orig._modifiable)
            {
            }

            /** The constructor with a name and a namespace */
         public:
            ModelRef& operator=(const ModelRef& orig) throws()
            {
               _modifiable = orig._modifiable;
               _target = orig._target;
               return *this;
            }

            /**
             * Bind this reference to the specified object.
             * @param obj an object or 0 to clear the reference
             * @throws exception if this ModelRef is not modifiable
             */
         public:
            ::timber::Pointer< T> bind(::timber::Pointer< T> obj)
            {
               if (!_modifiable) {
                  assert("ModelRef cannot be modified"==0);
                  throw ::std::runtime_error("ModelRef cannot be modified");
               }
               ::timber::Pointer< T>::swap(obj, _target->_object);
               return obj;
            }

            /**
             * Unbind this ModelRef.
             * @throws exception if this ModelRef is not modifiable
             */
         public:
            ::timber::Pointer< T> unbind()
            {
               return bind(::timber::Pointer< T>());
            }

            /**
             * Determine if this non-const methods can be invoked on this target
             * of this reference.
             * @return true if this reference can be bound or cleared
             */
         public:
            inline bool isModifiable() const throws()
            {
               return _modifiable;
            }

            /**
             * Determine if this reference has been bound.
             * @return true if this reference has been resolved and this ModelRef is valid.
             */
         public:
            inline bool isBound() const throws()
            {
               return _target && _target->_object;
            }

            /**
             * Determine if this reference is defined
             * @return true if this ModelRef is a valid ModelRef.
             */
         public:
            inline operator bool() const throw ()
            {
               return _target;
            }

            /**
             * Get the target of this reference.
             * @return the target or 0 if not bound
             */
         public:
            ::timber::Pointer< T> target() const throws()
            {
               ::timber::Pointer< T> tgt;
               if (_target) {
                  tgt = _target->_object;
               }
               return tgt;
            }

            /**
             * Get the target of this reference.
             * @return the target or 0 if not bound
             */
         public:
            template<class U>
            ::timber::Pointer< U> castTarget() const throws()
            {
               ::timber::Pointer< U> tgt;
               if (_target) {
                  tgt = _target->_object;
               }
               return tgt;
            }

            /**
             * Derefence the object
             * @return the object
             * @throws Exception if this ModelRef is null or it's not resolved
             */
         public:
            T* operator->() const throws()
            {
               if (_target && _target->_object) {
                  return &*_target->_object;
               }
               if (!_target) {
                  throw ::std::runtime_error("Reference is undefined");
               }
               else {
                  assert("Reference is not resolved"==0);
                  throw ::std::runtime_error("Reference is not resolved");
               }
            }

            /**
             * Compare two ModelRefs. Two ModelRefs are equal, if their names are the same
             * @param ModelRef another ref
             * @return true if this ModelRef is the same as ModelRef
             */
         public:
            bool operator==(const ModelRef& node) const throws()
            {
               if (_target == node._target) {
                  return true;
               }
               if (!_target || !node._target) {
                  return false;
               }
               // compare by name first, because namespace is longer
               return _target->_name == node._target->_name;
            }

            /**
             * Determine if this ModelRef is lexicographically less than the specified ModelRef
             * @param ModelRef another ModelRef
             * @return true if this ModelRef is lexicographically less than ModelRef
             */
         public:
            bool operator<(const ModelRef& node) const throws()
            {
               if (_target == node._target) {
                  return false;
               }
               if (!_target) {
                  return true;
               }
               if (!node._target) {
                  return false;
               }
               return _target->_object != node._target->_object;
            }

            /** The referenced object */
         private:
            ::timber::Pointer< Impl> _target;

            /** True if this reference is modifiable */
         private:
            bool _modifiable;
      };
   }
}

#endif

