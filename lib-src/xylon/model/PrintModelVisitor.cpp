#include <xylon/model/PrintModelVisitor.h>
#include <xylon/model/models.h>

using namespace ::std;
using namespace ::timber;
using namespace ::xylon::xsdom;

namespace xylon {
   namespace model {
      namespace {

         static void emitScopeName(::std::ostream& out, const Pointer< ScopeModel> m)
         {
            if (m) {
               out << "{ " << m->scopeName() << " }";
               switch (m->scopeName().type()) {
                  case ScopeName::ELEMENT:
                     break;
                  case ScopeName::TYPE:
                     out << "::types";
                     break;
                  case ScopeName::ATTRIBUTE:
                     out << "::attributes";
                     break;
                  case ScopeName::ATTRIBUTE_GROUP:
                     out << "::attributeGroups";
                     break;
                  case ScopeName::KEY:
                     out << "::keys";
                     break;
                  case ScopeName::GROUP:
                     out << "::groups";
                     break;
                  case ScopeName::UNSPECIFIED:
                     out << "::global";
                     break;
                  case ScopeName::SCHEMA:
                     out << "::schema";
                     break;
                  default:
                     throw ::std::logic_error("Hitting default");
               }
            }
         }

         static void startScopeDecls(::std::ostream& out, const Pointer< ScopeModel> m)
         {
            if (m) {
               out << "namespace ";
               emitScopeName(out, m);
               out << endl << "{" << endl;
            }
         }

         static void endScopeDecls(::std::ostream& out, Pointer< ScopeModel> m)
         {
            if (m) {
               out << "} // namespace " << m->scopeName().name() << endl;
            }
         }

         static void emitTypeName(::std::ostream& out, const Pointer< TypeModel>& m)
         {
            if (m->isGlobal()) {
               emitScopeName(out, m->scope());
               out << "::";
            }
            out << m->name();
         }

         template<class T>
         static void visitModels(PrintModelVisitor& visitor, const ::std::vector< T>& models)
         {
            for (size_t i = 0; i < models.size(); ++i) {
               models[i]->accept(visitor);
            }
         }

         template<class T>
         static void visitLocalTypes(PrintModelVisitor& visitor, const ::std::vector< T>& models)
         {
            for (size_t i = 0; i < models.size(); ++i) {
               if (models[i]->isLocal()) {
                  models[i]->accept(visitor);
               }
            }
         }

         static void emitType(PrintModelVisitor& visitor, ::std::ostream& out,
               const ::timber::Reference< TypeModel>& obj)
         {
            out << "class " << obj->name() << ' ';
            if (obj->baseType()) {
               out << " : public ";
               emitTypeName(out, obj->baseType().target());
               out << ' ';
            }
            out << '{' << endl;

            // create each local type
            ::std::vector< Reference< TypeModel> > types = obj->localTypes();
            visitLocalTypes(visitor, types);

            ::std::vector< ::timber::Reference< AttributeModel> > attributes = obj->attributes();
            visitModels(visitor, attributes);

            out << "};" << endl << endl;
         }

      }

      PrintModelVisitor::PrintModelVisitor(::std::ostream& out) throws() :
         _stream(out)
      {
      }

      PrintModelVisitor::~PrintModelVisitor() throws()
      {
         _stream.flush();
      }

      void PrintModelVisitor::visitAttribute(const ::timber::Reference< AttributeModel>& obj)
      {
         if (obj->minOccurs() != 0 && obj->maxOccurs() == 1) {
            _stream << "public:" << endl << " Reference< ";
         }
         else if (obj->minOccurs() == 0 && obj->maxOccurs() == 1) {
            _stream << "public:" << endl << " Pointer< ";
         }
         else {
            _stream << "public:" << endl << " Sequence< ";
         }
         emitTypeName(_stream, obj->type().target());
         _stream << " > " << obj->name() << ';' << endl;
      }

      void PrintModelVisitor::visitType(const ::timber::Reference< TypeModel>& obj)
      {
         if (obj->isGlobal()) {
            startScopeDecls(_stream, obj->scope());
         }
         emitType(*this, _stream, obj);
         if (obj->isGlobal()) {
            endScopeDecls(_stream, obj->scope());
         }
      }

      void PrintModelVisitor::visitElementType(const ::timber::Reference< ElementTypeModel>& obj)
      {
         visitType(obj);
      }

      void PrintModelVisitor::visitUnionType(const ::timber::Reference< UnionTypeModel>& obj)
      {

         if (obj->isGlobal()) {
            startScopeDecls(_stream, obj->scope());
         }
         {
            _stream << "class " << obj->name() << ' ';
            _stream << '{' << endl;

            // create each local type
            ::std::vector< ModelRef< TypeModel> > types = obj->types();
            visitLocalTypes(*this, types);

            for (size_t i = 0; i < types.size(); ++i) {
               emitTypeName(_stream, types[i].target());
               _stream << " >" << " value" << types[i]->name() << ";" << endl;
            }

            _stream << "};" << endl << endl;
         }
         if (obj->isGlobal()) {
            endScopeDecls(_stream, obj->scope());
         }
      }

      void PrintModelVisitor::visitListType(const ::timber::Reference< ListTypeModel>& obj)
      {
         if (obj->isGlobal()) {
            startScopeDecls(_stream, obj->scope());
         }
         {
            _stream << "class " << obj->name() << ' ';
            _stream << '{' << endl;

            // create each local type
            ::std::vector< ModelRef< TypeModel> > types;
            types.push_back(obj->type());
            visitLocalTypes(*this, types);

            _stream << "::std::vector< ";
            emitTypeName(_stream, obj->type().target());
            _stream << " >" << " value;" << endl;

            _stream << "};" << endl << endl;
         }
         if (obj->isGlobal()) {
            endScopeDecls(_stream, obj->scope());
         }
      }

      void PrintModelVisitor::visitMethod(const ::timber::Reference< Method>& obj)
      {
      }

      void PrintModelVisitor::visitNamespace(const ::timber::Reference< NamespaceModel>& obj)
      {
      }
   }
}

