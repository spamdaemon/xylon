#include <xylon/model/NamespaceModel.h>
#include <xylon/model/ModelVisitor.h>

namespace xylon {

   namespace model {

      NamespaceModel::NamespaceModel() throws()
      {
      }

      NamespaceModel::~NamespaceModel() throws()
      {
      }

      void NamespaceModel::accept (ModelVisitor& v) throws()
      { v.visitNamespace(toRef<NamespaceModel>()); }
   }

}
