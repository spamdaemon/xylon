#ifndef _XYLON_MODEL_INSTRUCTIONS_ERROR_H
#define _XYLON_MODEL_INSTRUCTIONS_ERROR_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * The Error instruction terminates processing.
          */
         class Error : public Instruction
         {
               Error(const Error&);
               Error&operator=(const Error&);

               /** Default constructor */
            protected:
               Error() throws();

               /** Destructor */
            public:
               ~Error() throws();

               /**
                * Create an error instruction
                * @return an error instruction
                */
            public:
               static ::timber::Reference< Instruction> create() throws();

               /**
                * Create an error instruction
                * @param errMsg an error message
                * @return an error instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ::std::string& errMsg) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get  the error message.
                * @return an error message
                */
            public:
               virtual const ::std::string& message() const throws() = 0;

         };

      }
   }
}
#endif
