#ifndef _XYLON_MODEL_INSTRUCTIONS_MESSAGE_H
#define _XYLON_MODEL_INSTRUCTIONS_MESSAGE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * The Message instruction terminates processing.
          */
         class Message : public Instruction
         {
               Message(const Message&);
               Message&operator=(const Message&);

               /** Default constructor */
            protected:
               Message() throws();

               /** Destructor */
            public:
               ~Message() throws();

               /**
                * Create an message instruction
                * @param msg the message
                * @return an message instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ::std::string& msg) throws();

               /**
                * Get the message
                * @return the message
                */
            public:
               virtual const ::std::string& message() const throws() = 0;

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}
#endif
