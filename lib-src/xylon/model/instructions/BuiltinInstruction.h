#ifndef _XYLON_MODEL_INSTRUCTIONS_BUILTININSTRUCTION_H
#define _XYLON_MODEL_INSTRUCTIONS_BUILTININSTRUCTION_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * A builtin instruction serves as a hook for allow code generator to introduce built-in instructions
          * and intregrate them into the architecture nonetheless.
          */
         class BuiltinInstruction : public Instruction
         {
               BuiltinInstruction(const BuiltinInstruction&);
               BuiltinInstruction&operator=(const BuiltinInstruction&);

               /** Default constructor */
            protected:
               BuiltinInstruction() throws();

               /** Destructor */
            public:
               ~BuiltinInstruction() throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}
#endif

