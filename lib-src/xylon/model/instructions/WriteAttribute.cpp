#include <xylon/model/instructions/WriteAttribute.h>
#include <xylon/model/AttributeModel.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         WriteAttribute::WriteAttribute() throws()
         {
         }
         WriteAttribute::~WriteAttribute() throws()
         {
         }

         ::timber::Reference< Instruction> WriteAttribute::create(const ::timber::Reference< AttributeModel>& xmodel) throws()
         {
            struct Impl : public WriteAttribute
            {

                  ~Impl() throws()
                  {
                  }

                  ::timber::Reference< AttributeModel> attribute() const throws()
                  {
                     return _model;
                  }
                  ::timber::Pointer< StartWriteObject> before() const throws()
                  {
                     return ::timber::Pointer< StartWriteObject>();
                  }
                  ::timber::Pointer< EndWriteObject> after() const throws()
                  {
                     return ::timber::Pointer< EndWriteObject>();
                  }

                  ::timber::Pointer< AttributeModel> _model;
            };

            Impl* res = new Impl();
            res->_model = xmodel;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Reference< Instruction> WriteAttribute::create(const ::timber::Reference< AttributeModel>& xmodel,
               const ::timber::Reference< StartWriteObject>& xbefore, const ::timber::Reference< EndWriteObject>& xafter) throws()
         {
            struct Impl : public WriteAttribute
            {

                  ~Impl() throws()
                  {
                  }

                  ::timber::Reference< AttributeModel> attribute() const throws()
                  {
                     return _model;
                  }
                  ::timber::Pointer< StartWriteObject> before() const throws()
                  {
                     return _before;
                  }
                  ::timber::Pointer< EndWriteObject> after() const throws()
                  {
                     return _after;
                  }

                  ::timber::Pointer< AttributeModel> _model;
                  ::timber::Pointer< StartWriteObject> _before;
                  ::timber::Pointer< EndWriteObject> _after;
            };

            Impl* res = new Impl();
            res->_model = xmodel;
            res->_before = xbefore;
            res->_after = xafter;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void WriteAttribute::accept(InstructionVisitor& v)
         {
            v.visitWriteAttribute(toRef< WriteAttribute> ());
         }

      }
   }
}
