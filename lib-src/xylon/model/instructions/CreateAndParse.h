#ifndef _XYLON_MODEL_INSTRUCTIONS_CREATEANDPARSE_H
#define _XYLON_MODEL_INSTRUCTIONS_CREATEANDPARSE_H

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      class TypeModel;
      namespace instructions {

         /**
          * This instruction creates and parses a top-level object.
          */
         class CreateAndParse : public Instruction
         {
               CreateAndParse(const CreateAndParse&);
               CreateAndParse&operator=(const CreateAndParse&);

               /** Default constructor */
            protected:
               CreateAndParse() throws();

               /** Destructor */
            public:
               ~CreateAndParse() throws();

               /**
                * Create a new instruction to instantiate the specified type and parse it.
                * @param ref a reference to a global type.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction>
               create(const ModelRef< TypeModel>& model) throws();

               /**
                * Create a new instruction that instantiates a type corresponding to the current
                * element and parses it.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction>
               create() throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Determine if this instruction should instantiate the type(), or instantiate
                * the element on-top of the stack.
                * @return true if type() returns a valid (bound) type, false otherwise
                */
            public:
               virtual bool hasType() const throws() = 0;

               /**
                * Get the the type model for the type to be parsed. This method may return
                * an unbound type. Use to hasType() to see if this method is expected to produce a bound type.
                * @return the type to be instantiated and parsed.
                */
            public:
               virtual ModelRef< TypeModel> type() const throws() = 0;
         };

      }
   }
}

#endif
