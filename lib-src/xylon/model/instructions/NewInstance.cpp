#include <xylon/model/instructions/NewInstance.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/model/TypeModel.h>

namespace xylon {
   namespace model {
      namespace instructions {
         namespace {
            struct Impl : public NewInstance
            {
                  Impl(const ModelRef< TypeModel>& t) :
                     _type(t)
                  {
                  }
                  ~Impl() throws()
                  {
                  }
                  ModelRef< TypeModel> type() const throws()
                  {
                     return _type;
                  }
                  ModelRef< TypeModel> _type;
            };
         }

         NewInstance::NewInstance() throws()
         {
         }
         NewInstance::~NewInstance() throws()
         {
         }

         ::timber::Reference< Instruction> NewInstance::create(const ModelRef< TypeModel>& xmodel) throws()
         {
            assert(xmodel && "Invalid type model");
            return new Impl(xmodel);
         }

         void NewInstance::accept(InstructionVisitor& v)
         {
            v.visitNewInstance(toRef< NewInstance> ());
         }

      }
   }
}
