#include <xylon/model/instructions/OnValidElement.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {
         namespace {
            struct Impl : public OnValidElement
            {
                  Impl() :
                     _requireKnownElement(false)
                  {
                  }
                  ~Impl() throws()
                  {
                  }
                  ::timber::Pointer< Instruction> ifTrue() const throws()
                  {
                     return _true;
                  }
                  ::timber::Pointer< Instruction> ifFalse() const throws()
                  {
                     return _false;
                  }
                  bool requireKnownElement() const throws()
                  {
                     return _requireKnownElement;
                  }

                  ::timber::Pointer< Instruction> _true;
                  ::timber::Pointer< Instruction> _false;
                  bool _requireKnownElement;
            };

         }
         OnValidElement::OnValidElement() throws()
         {
         }
         OnValidElement::~OnValidElement() throws()
         {
         }

         ::timber::Reference< Instruction> OnValidElement::create(const ::timber::Pointer< Instruction>& xtrue,
               const ::timber::Pointer< Instruction>& xfalse) throws()
         {

            Impl* res = new Impl();
            res->_true = xtrue;
            res->_false = xfalse;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Reference< Instruction> OnValidElement::create(bool xRequireKnownElement, const ::timber::Pointer<
               Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse) throws()
         {

            Impl* res = new Impl();
            res->_requireKnownElement = xRequireKnownElement;
            res->_true = xtrue;
            res->_false = xfalse;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Pointer< Instruction> OnValidElement::ifTrue() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }
         ::timber::Pointer< Instruction> OnValidElement::ifFalse() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }

         void OnValidElement::accept(InstructionVisitor& v)
         {
            v.visitOnValidElement(toRef< OnValidElement> ());
         }
      }
   }
}
