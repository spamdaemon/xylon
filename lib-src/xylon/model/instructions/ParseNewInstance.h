#ifndef _XYLON_MODEL_INSTRUCTIONS_PARSENEWINSTANCE_H
#define _XYLON_MODEL_INSTRUCTIONS_PARSENEWINSTANCE_H

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      class TypeModel;
      namespace instructions {

         /**
          * This instruction creates a new instance of a type, and then executes a parse instruction. If the parse instruction
          * fails, the stack is unwound, and the error instruction is invoked. The way to think about this instruction is follows:
          * @code
          *    bool error=false;
          *    parseNewInstance(type);
          *    try {
          *       parseInstance(type);
          *    }
          *    catch (...) {
          *       error = true;
          *       pop stack;
          *    }
          *    if (error) {
          *       ifFalse();
          *    }
          *    else {
          *       ifTrue();
          *    }
          * @endcode
          */
         class ParseNewInstance : public Instruction
         {
               ParseNewInstance(const ParseNewInstance&);
               ParseNewInstance&operator=(const ParseNewInstance&);

               /** Default constructor */
            protected:
               ParseNewInstance() throws();

               /** Destructor */
            public:
               ~ParseNewInstance() throws();

               /**
                * This create an new instance of this instruction.
                * @param ref a reference to a global type.
                * @param ifTrue an instruction to execute if the type was parsed
                * @param ifFalse an instruction to execute if the parse not parsed
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction>
               create(const ModelRef< TypeModel>& model, const ::timber::Reference< Instruction>& ifTrue,
                     const ::timber::Reference< Instruction>& ifFalse) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the the global type being referenced. If this method returns an invalid reference,
                * then type() must return a valid pointer.
                * @return the global type or an invalid reference.
                */
            public:
               virtual ModelRef< TypeModel> type() const throws() = 0;

               /**
                * Get the parse instruction.
                * @return the parse instruction.
                */
            public:
               virtual ::timber::Reference< Instruction> parseInstruction() const throws() = 0;

               /**
                * The instruction to execute if the type was parsed properly.
                * @return the instruction to execute if the type could not be parsed
                */
            public:
               virtual ::timber::Reference< Instruction> ifTrue() const throws() = 0;

               /**
                * The instruction to execute if the type was not parsed properly.
                * @return the instruction to execute if the type could not be parsed
                */
            public:
               virtual ::timber::Reference< Instruction> ifFalse() const throws() = 0;

         };

      }
   }
}

#endif
