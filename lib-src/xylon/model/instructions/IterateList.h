#ifndef _XYLON_MODEL_INSTRUCTIONS_ITERATELIST_H
#define _XYLON_MODEL_INSTRUCTIONS_ITERATELIST_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_XSDOM_OCCURRENCE_H
#include <xylon/xsdom/Occurrence.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * The IterateList function is a model of a iterateList that must be executed a minimum
          * number of times and a maximum of times. The maximum number maybe
          * unlimited.
          */
         class IterateList : public Instruction
         {
               IterateList(const IterateList&);
               IterateList&operator=(const IterateList&);

               /** Default constructor */
            protected:
               IterateList() throws();

               /** Destructor */
            public:
               ~IterateList() throws();

               /**
                * Create a iterateList instruction
                * @param occurs the occurrence value
                * @param body the iterateList body
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ::timber::Reference< Instruction>& body) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the iterateList body. The iterateList body are the instructions that must be executed serially
                * in this iterateList.
                * @return a list of instructions that need to be executed one after the other
                */
            public:
               virtual ::timber::Reference< Instruction> body() const throws() = 0;
         };

      }
   }
}
#endif
