#ifndef _XYLON_MODEL_INSTRUCTIONS_BIND_H
#define _XYLON_MODEL_INSTRUCTIONS_BIND_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * This instruction binds type on top of the type stack to the type below
          * in an unspecified way. This instruction is used for builtin types, such as
          * unions and lists.
          */
         class Bind : public Instruction
         {
               Bind(const Bind&);
               Bind&operator=(const Bind&);

               /** Default constructor */
            protected:
               Bind() throws();

               /** Destructor */
            public:
               ~Bind() throws();

               /**
                * Create a new instruction to bind an attribute model to an attribute.
                * @param attrName the name of the XML attribute
                * @param model an attribute model
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create() throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}

#endif
