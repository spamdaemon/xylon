#ifndef _XYLON_MODEL_INSTRUCTIONS_PARSEINSTANCE_H
#define _XYLON_MODEL_INSTRUCTIONS_PARSEINSTANCE_H

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      class TypeModel;
      namespace instructions {

         /**
          * Parse an instance of a global type. How the contents of the type are
          * parsed depends on the ScopeName of the type. When parsing is complete
          * the cursor will point to the next logical element, which is dependent upon
          * the type being parsed. For elements  the next logical element is the following element.
          * For groups, then next logical element is the element not parsed as part of the group.
          * For complex types, the next logical element is the first child element of the current element
          * that was not parsed.
          * An instanceof of the type being is already at the top of the stack.
          */
         class ParseInstance : public Instruction
         {
               ParseInstance(const ParseInstance&);
               ParseInstance&operator=(const ParseInstance&);

               /** Default constructor */
            protected:
               ParseInstance() throws();

               /** Destructor */
            public:
               ~ParseInstance() throws();

               /**
                * Create a new instruction to instantiate an element whose type is a global type.
                * @param ref a reference to a global type.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ModelRef< TypeModel>& model) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the the global type being referenced. If this method returns an invalid reference,
                * then type() must return a valid pointer.
                * @return the global type or an invalid reference.
                */
            public:
               virtual ModelRef< TypeModel> type() const throws() = 0;
         };

      }
   }
}

#endif
