#ifndef _XYLON_MODEL_INSTRUCTIONS_ONXSITYPE_H
#define _XYLON_MODEL_INSTRUCTIONS_ONXSITYPE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

#include <map>

namespace xylon {
   class QName;
   namespace model {
      class TypeModel;
      namespace instructions {

         /**
          * This instruction can be used to test if the current element cursor points to a valid attribute.
          */
         class OnXsiType : public Instruction
         {
               OnXsiType(const OnXsiType&);
               OnXsiType&operator=(const OnXsiType&);

               /** Default constructor */
            protected:
               OnXsiType() throws();

               /** Destructor */
            public:
               ~OnXsiType() throws();

               /**
                * Instantiate a type. If the current element has an xsi:type attribute then the
                * type of that attribute must be used, otherwise the expected type is used.
                * If the element has an invalid xsi type then an error is to generated.
                * @param expected the expected type for which an xsi type is expected.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction>
               create(const ModelRef< TypeModel>& expected) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the the type model for the type to be parsed.
                * @return the expected type
                */
            public:
               virtual ModelRef< TypeModel> expectedType() const throws() = 0;
         };

      }
   }
}
#endif
