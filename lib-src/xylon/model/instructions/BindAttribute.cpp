#include <xylon/model/instructions/BindAttribute.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/model/AttributeModel.h>

namespace xylon {
   namespace model {
      namespace instructions {
         BindAttribute::BindAttribute() throws()
         {
         }
         BindAttribute::~BindAttribute() throws()
         {
         }

         ::timber::Reference< Instruction> BindAttribute::create(const ::timber::Reference< AttributeModel>& xmodel) throws()
         {
            struct Impl : public BindAttribute
            {
                  ~Impl() throws()
                  {
                  }
                  ::timber::Reference< AttributeModel> boundModel() const throws()
                  {
                     return _model;
                  }
                  ::timber::Pointer< AttributeModel> _model;
                  QName _name;
            };

            Impl* res = new Impl();
            res->_model = xmodel;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void BindAttribute::accept(InstructionVisitor& v)
         {
            v.visitBindAttribute(toRef< BindAttribute> ());
         }

      }
   }
}
