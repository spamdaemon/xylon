#include <xylon/model/instructions/OnXsiType.h>
#include <xylon/model/TypeModel.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/QName.h>

namespace xylon {
   namespace model {
      namespace instructions {

         OnXsiType::OnXsiType() throws()
         {
         }
         OnXsiType::~OnXsiType() throws()
         {
         }


         ::timber::Reference< Instruction> OnXsiType::create(const ModelRef< TypeModel>& expected) throws()
         {
            struct Impl : public OnXsiType
            {
                  ~Impl() throws()
                  {
                  }
                  ModelRef< TypeModel> expectedType() const throws()
                  {
                     return _expected;
                  }

                  ModelRef< TypeModel> _expected;
            };

            Impl* res = new Impl();
            res->_expected = expected;

            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void OnXsiType::accept(InstructionVisitor& v)
         {
            v.visitOnXsiType(toRef< OnXsiType> ());
         }
      }
   }
}
