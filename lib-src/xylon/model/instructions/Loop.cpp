#include <xylon/model/instructions/Loop.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
  namespace model {
    namespace instructions {

      Loop::Loop() throws() {}
      Loop::~Loop() throws() {}

      ::timber::Reference<Instruction> Loop::create (const ::xylon::xsdom::Occurrence& xoccurs, const ::timber::Reference<Instruction>& xbody) throws()
      {
	struct Impl  : public Loop {
	  ~Impl() throws() {}
	  ::xylon::xsdom::Occurrence occurs() const throws() { return _occurs; }
	  ::timber::Reference<Instruction> body() const throws() { return _instruction; }
	  
	  ::xylon::xsdom::Occurrence _occurs;
	  ::timber::Pointer<Instruction> _instruction;
	};
	
	Impl* res = new Impl();
	res->_occurs = xoccurs;
	res->_instruction = xbody;
	return ::timber::Reference<Instruction>(static_cast<Instruction*>(res));
      }

      

      void Loop::accept (InstructionVisitor& v) 
      { v.visitLoop(toRef<Loop>()); }

    }
  }
}
