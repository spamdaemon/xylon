#ifndef _XYLON_MODEL_INSTRUCTIONS_ONELEMENT_H
#define _XYLON_MODEL_INSTRUCTIONS_ONELEMENT_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#include <set>
#include <map>

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * The OnElement instruction checks if the current element is one of the specified elements.
          * If it is, then the body of this element is executed.
          */
         class OnElement : public Instruction
         {
               OnElement(const OnElement&);
               OnElement&operator=(const OnElement&);

               /** This map contains the QNames and the instruction to be executed for each when matched. */
            public:
               typedef ::std::map< QName, ::timber::Reference< Instruction> > Candidates;

               /** Default constructor */
            protected:
               OnElement() throws();

               /** Destructor */
            public:
               ~OnElement() throws();

               /**
                * Create an OnElement instruction
                * @param candidates the candidate elements and their instructions
                * @param noneMatched the statement if the condition is false
                * @return an instruction
                */
            public:
               static ::timber::Reference< OnElement>
               create(const Candidates& match, const ::timber::Pointer< Instruction>& noneMatched, bool ignoreSubstitutionGroups) throws();

               /**
                * Create an OnElement instruction
                * @param condition the element names to check for
                * @param xtrue the statement if the condition is true
                * @param xfalse the statemetn if the condition is false
                * @return an instruction
                */
            public:
               static ::timber::Reference< OnElement>
               create(const ::std::set< QName>& condition, const ::timber::Pointer< Instruction>& xtrue,
                     const ::timber::Pointer< Instruction>& xfalse, bool ignoreSubstitutionGroups) throws();

               /**
                * Create an OnElement instruction
                * @param condition the element name to check for
                * @param xtrue the statement if the condition is true
                * @param xfalse the statemetn if the condition is false
                * @return an instruction
                */
            public:
               static ::timber::Reference< OnElement> create(const QName& condition, const ::timber::Pointer<
                     Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse, bool ignoreSubstitutionGroups) throws();
               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the elements that need to be matched and their instructions.
                * @return the elements and their corresponding instructions
                */
            public:
               virtual Candidates matchedElements() const throw () = 0;

               /**
                * Get the instruction to be executed if the current XML element is not one
                * of the specified elements
                * @return an instruction
                */
            public:
               virtual ::timber::Pointer< Instruction> ifFalse() const throws();

               /**
                * Determine if substitution groups must not be considered.
                * @return true if substitution groups must not be considered
                */
            public:
               virtual bool isIgnoreSubstitutions() const throws() = 0;
         };

      }
   }
}
#endif
