#include <xylon/model/instructions/MoveCursor.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         MoveCursor::MoveCursor() throws()
         {
         }
         MoveCursor::~MoveCursor() throws()
         {
         }

         ::timber::Reference< Instruction> MoveCursor::create(Move dir) throws()
         {
            struct Impl : public MoveCursor
            {
                  Impl(Move xdir) :
                     _dir(xdir)
                  {
                  }

                  ~Impl() throws()
                  {
                  }
                  virtual Move direction() const throws()
                  {
                     return _dir;
                  }

               private:
                  const Move _dir;
            };

            Impl* res = new Impl(dir);
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void MoveCursor::accept(InstructionVisitor& v)
         {
            v.visitNextElement(toRef< MoveCursor> ());
         }
      }
   }
}
