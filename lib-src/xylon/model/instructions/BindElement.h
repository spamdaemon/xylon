#ifndef _XYLON_MODEL_INSTRUCTIONS_BINDELEMENT_H
#define _XYLON_MODEL_INSTRUCTIONS_BINDELEMENT_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      class ElementModel;
      namespace instructions {

         /**
          * This instruction takes the current element and binds it to the type currently on the stack.
          */
         class BindElement : public Instruction
         {
               BindElement(const BindElement&);
               BindElement&operator=(const BindElement&);

               /** Default constructor */
            protected:
               BindElement() throws();

               /** Destructor */
            public:
               ~BindElement() throws();

               /**
                * Create a new instruction to bind an attribute model to an attribute.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create() throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}

#endif
