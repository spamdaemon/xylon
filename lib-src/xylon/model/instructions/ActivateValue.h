#ifndef _XYLON_MODEL_INSTRUCTIONS_ACTIVATEVALUE_H
#define _XYLON_MODEL_INSTRUCTIONS_ACTIVATEVALUE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#include <set>

namespace xylon {
  namespace model {
    namespace instructions {

      /**
       * The ActivateValue instruction checks if the current attribute is one of the specified attributes.
       * If it is, then the body of this attribute is executed.
       */
      class ActivateValue  : public Instruction {
	ActivateValue(const ActivateValue&);
	ActivateValue&operator=(const ActivateValue&);

	/** Default constructor */
      protected:
	ActivateValue() throws();

	/** Destructor */
      public:
	~ActivateValue() throws();

	/**
	 * Create an ActivateValue instruction
	 * @return an instruction
	 */
      public:
	static ::timber::Reference<Instruction> create () throws();

	/** 
	 * Accept the specified visitor.
	 * @param v a visitor
	 */
      public:
	void accept (InstructionVisitor& v);
      };

    }
  }
}
#endif
