#include <xylon/model/instructions/OnAll.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         OnAll::OnAll() throws()
         {
         }
         OnAll::~OnAll() throws()
         {
         }

         ::timber::Reference< Instruction> OnAll::create(const Candidates& xrequired, const Candidates& xoptional) throws()
         {
            struct Impl : public OnAll
            {
                  ~Impl() throws()
                  {
                  }
                  Candidates optional() const throws()
                  {
                     return _optional;
                  }
                  Candidates required() const throws()
                  {
                     return _required;
                  }

                  Candidates _optional, _required;
            };

            Impl* res = new Impl();
            res->_required = xrequired;
            res->_optional = xoptional;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void OnAll::accept(InstructionVisitor& v)
         {
            v.visitOnAll(toRef< OnAll> ());
         }

      }
   }
}
