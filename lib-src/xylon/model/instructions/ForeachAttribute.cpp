#include <xylon/model/instructions/ForeachAttribute.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         ForeachAttribute::ForeachAttribute() throws()
         {
         }
         ForeachAttribute::~ForeachAttribute() throws()
         {
         }

         ::timber::Reference< Instruction> ForeachAttribute::create(const ::timber::Reference< Instruction>& xbody) throws()
         {
            struct Impl : public ForeachAttribute
            {
                  ~Impl() throws()
                  {
                  }
                  ::timber::Reference< Instruction> body() const throws()
                  {
                     return _instruction;
                  }

                  ::timber::Pointer< Instruction> _instruction;
            };

            Impl* res = new Impl();
            res->_instruction = xbody;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void ForeachAttribute::accept(InstructionVisitor& v)
         {
            v.visitForeachAttribute(toRef< ForeachAttribute> ());
         }

      }
   }
}
