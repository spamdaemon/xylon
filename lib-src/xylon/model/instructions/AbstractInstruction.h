#ifndef _XYLON_MODEL_INSTRUCTIONS_ABSTRACTINSTRUCTION_H
#define _XYLON_MODEL_INSTRUCTIONS_ABSTRACTINSTRUCTION_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * The AbstractInstruction function is a model of a sequence that must be executed a minimum
          * number of times and a maximum of times. The maximum number maybe
          * unlimited.
          */
         class AbstractInstruction : public Instruction
         {
               AbstractInstruction(const AbstractInstruction&);
               AbstractInstruction&operator=(const AbstractInstruction&);

               /** Default constructor */
            protected:
               AbstractInstruction() throws();

               /** Destructor */
            public:
               ~AbstractInstruction() throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the concrete instruction.
                * @return the concrete instruction or null if not known
                */
            public:
               virtual ::timber::Pointer< Instruction> instruction() const throws() = 0;
         };

      }
   }
}
#endif
