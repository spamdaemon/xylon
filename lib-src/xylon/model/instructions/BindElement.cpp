#include <xylon/model/instructions/BindElement.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {
         BindElement::BindElement() throws()
         {
         }
         BindElement::~BindElement() throws()
         {
         }

         ::timber::Reference< Instruction> BindElement::create() throws()
         {
            struct Impl : public BindElement
            {
                  ~Impl() throws()
                  {
                  }
            };

            Impl* res = new Impl();
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void BindElement::accept(InstructionVisitor& v)
         {
            v.visitBindElement(toRef< BindElement> ());
         }

      }
   }
}
