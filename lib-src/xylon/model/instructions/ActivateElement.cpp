#include <xylon/model/instructions/ActivateElement.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {
         namespace {
            struct Impl : public ActivateElement
            {
                  Impl() throws ()
                  {
                  }
                  ~Impl() throws()
                  {
                  }
            };
         }

         ActivateElement::ActivateElement() throws()
         {
         }
         ActivateElement::~ActivateElement() throws()
         {
         }

         ::timber::Reference< Instruction> ActivateElement::create() throws()
         {
            return new Impl();
         }

         void ActivateElement::accept(InstructionVisitor& v)
         {
            v.visitActivateElement(toRef< ActivateElement> ());
         }

      }
   }
}
