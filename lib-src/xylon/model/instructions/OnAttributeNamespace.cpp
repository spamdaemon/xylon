#include <xylon/model/instructions/OnAttributeNamespace.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         OnAttributeNamespace::OnAttributeNamespace() throws()
         {
         }

         OnAttributeNamespace::~OnAttributeNamespace() throws()
         {
         }

         ::timber::Reference< Instruction> OnAttributeNamespace::create(const ::std::set< String>& condition,
               const ::timber::Pointer< Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse) throws()
         {
            struct Impl : public OnAttributeNamespace
            {
                  ~Impl() throws()
                  {
                  }

                  ::std::set< String> namespaces() const throws()
                  {
                     return _condition;
                  }
                  ::timber::Pointer< Instruction> ifTrue() const throws()
                  {
                     return _true;
                  }
                  ::timber::Pointer< Instruction> ifFalse() const throws()
                  {
                     return _false;
                  }
                  ::std::set< String> _condition;
                  ::timber::Pointer< Instruction> _true;
                  ::timber::Pointer< Instruction> _false;
            };

            Impl* res = new Impl();
            res->_true = xtrue;
            res->_false = xfalse;
            res->_condition = condition;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Reference< Instruction> OnAttributeNamespace::create(const String& condition,
               const ::timber::Pointer< Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse) throws()
         {
            ::std::set< String> c;
            c.insert(condition);
            return create(c, xtrue, xfalse);
         }

         ::timber::Pointer< Instruction> OnAttributeNamespace::ifTrue() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }

         ::timber::Pointer< Instruction> OnAttributeNamespace::ifFalse() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }
         void OnAttributeNamespace::accept(InstructionVisitor& v)
         {
            v.visitOnAttributeNamespace(toRef< OnAttributeNamespace> ());
         }

      }
   }
}
