#ifndef _XYLON_MODEL_INSTRUCTIONS_ONVALIDELEMENT_H
#define _XYLON_MODEL_INSTRUCTIONS_ONVALIDELEMENT_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * This instruction can be used to test if the current element cursor points to a valid element.
          */
         class OnValidElement : public Instruction
         {
               OnValidElement(const OnValidElement&);
               OnValidElement&operator=(const OnValidElement&);

               /** Default constructor */
            protected:
               OnValidElement() throws();

               /** Destructor */
            public:
               ~OnValidElement() throws();

               /**
                * Create an OnValidElement instruction.
                * @param xtrue the statement if the condition is true
                * @param xfalse the statemetn if the condition is false
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ::timber::Pointer< Instruction>& xtrue,
                     const ::timber::Pointer< Instruction>& xfalse = ::timber::Pointer< Instruction>()) throws();

               /**
                * Create an OnValidElement instruction.
                * @param requireKnownElement if if this instruction checks also for registered element
                * @param xtrue the statement if the condition is true
                * @param xfalse the statemetn if the condition is false
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(bool requireKnownElement, const ::timber::Pointer<
                     Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse = ::timber::Pointer<
                     Instruction>()) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Determine if this instruction needs to check for an known element.
                * @return true if the current element must be known, false otherwise
                */
            public:
               virtual bool requireKnownElement() const throws() = 0;

               /**
                * Get the instruction to be executed if there is another element.
                * @return an instruction
                */
            public:
               virtual ::timber::Pointer< Instruction> ifTrue() const throws();

               /**
                * Get the instruction to be executed if there is no more element.
                * @return an instruction
                */
            public:
               virtual ::timber::Pointer< Instruction> ifFalse() const throws();
         };

      }
   }
}
#endif
