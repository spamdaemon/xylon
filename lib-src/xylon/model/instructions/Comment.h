#ifndef _XYLON_MODEL_INSTRUCTIONS_COMMENT_H
#define _XYLON_MODEL_INSTRUCTIONS_COMMENT_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * This instruction is used to place comments into the
          * instruction stream. It is not of any use for non-code
          * generator.
          */
         class Comment : public Instruction
         {
               Comment(const Comment&);
               Comment&operator=(const Comment&);

               /** Default constructor */
            protected:
               Comment() throws();

               /** Destructor */
            public:
               ~Comment() throws();

               /**
                * Create an message instruction
                * @param msg the message
                * @return an message instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ::std::string& msg) throws();

                /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}
#endif
