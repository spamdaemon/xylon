#include <xylon/model/instructions/StartWriteObject.h>
#include <xylon/QName.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         StartWriteObject::StartWriteObject() throws()
         {
         }
         StartWriteObject::~StartWriteObject() throws()
         {
         }

         ::timber::Reference< Instruction> StartWriteObject::create(const QName& xname, bool isElement) throws()
         {
            struct Impl : public StartWriteObject
            {

                  ~Impl() throws()
                  {
                  }

                  const QName& name() const throws()
                  {
                     return _name;
                  }

                  bool isStartElement() const throws()
                  {
                     return _element;
                  }

                  QName _name;
                  bool _element;
            };

            Impl* res = new Impl();
            res->_name = xname;
            res->_element = isElement;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void StartWriteObject::accept(InstructionVisitor& v)
         {
            v.visitStartWriteObject(toRef< StartWriteObject> ());
         }

      }
   }
}
