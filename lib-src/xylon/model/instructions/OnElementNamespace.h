#ifndef _XYLON_MODEL_INSTRUCTIONS_ONELEMENTNAMESPACE_H
#define _XYLON_MODEL_INSTRUCTIONS_ONELEMENTNAMESPACE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_H
#include <xylon/xylon.h>
#endif

#include <set>

namespace xylon {
  namespace model {
    namespace instructions {
      /**
       * The OnNamespace instruction checks if the current element has namespace defined by
       * this instruction.
       */
      class OnElementNamespace  : public Instruction {
	OnElementNamespace(const OnElementNamespace&);
	OnElementNamespace&operator=(const OnElementNamespace&);

	/** Default constructor */
      protected:
	OnElementNamespace() throws();

	/** Destructor */
      public:
	~OnElementNamespace() throws();

	/**
	 * Create an OnElementNamespace instruction
	 * @param condition the namespaces to check for
	 * @param xtrue the statement if the condition is true
	 * @param xfalse the statemetn if the condition is false
	 * @return an instruction
	 */
      public:
	static ::timber::Reference<Instruction> create (const ::std::set<String>& condition, 
							const ::timber::Pointer<Instruction>& xtrue,
							const ::timber::Pointer<Instruction>& xfalse) throws();

	/**
	 * Create an OnElementNamespace instruction
	 * @param condition the namespaces to check for
	 * @param xtrue the statement if the condition is true
	 * @param xfalse the statemetn if the condition is false
	 * @return an instruction
	 */
      public:
	static ::timber::Reference<Instruction> create (const String& condition, 
							const ::timber::Pointer<Instruction>& xtrue,
							const ::timber::Pointer<Instruction>& xfalse) throws();
	/** 
	 * Accept the specified visitor.
	 * @param v a visitor
	 */
      public:
	void accept (InstructionVisitor& v);

	/**
	 * Get the list of namespaces.
	 * @return a set of namespaces.
	 */
      public:
	virtual ::std::set<String> namespaces() const throws() = 0;
      
	/**
	 * Get the instruction to be executed if the current XML element's namespace is 
	 * one of the namespaces().
	 * @return an instruction
	 */
      public:
	virtual ::timber::Pointer<Instruction> ifTrue() const  throws();

	/**
	 * Get the instruction to be executed if the current XML element's namespace is 
	 * not one of the namespaces().
	 * @return an instruction
	 */
      public:
	virtual ::timber::Pointer<Instruction> ifFalse() const throws();
      };

    }
  }
}
#endif
