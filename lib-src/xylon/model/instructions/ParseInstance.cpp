#include <xylon/model/instructions/ParseInstance.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/model/TypeModel.h>

namespace xylon {
   namespace model {
      namespace instructions {
         namespace {
            struct Impl : public ParseInstance
            {
                  Impl(const ModelRef< TypeModel>& global) :
                     _global(global)
                  {
                  }
                  ~Impl() throws()
                  {
                  }
                  ModelRef< TypeModel> type() const throws()
                  {
                     return _global;
                  }
                  ModelRef< TypeModel> _global;
            };
         }

         ParseInstance::ParseInstance() throws()
         {
         }
         ParseInstance::~ParseInstance() throws()
         {
         }

         ::timber::Reference< Instruction> ParseInstance::create(const ModelRef< TypeModel>& xmodel) throws()
         {
            assert(xmodel && "The type model has not been defined");
            return new Impl(xmodel);
         }

         void ParseInstance::accept(InstructionVisitor& v)
         {
            v.visitParseInstance(toRef< ParseInstance> ());
         }

      }
   }
}
