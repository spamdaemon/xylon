#ifndef _XYLON_MODEL_INSTRUCTIONS_LOOP_H
#define _XYLON_MODEL_INSTRUCTIONS_LOOP_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_XSDOM_OCCURRENCE_H
#include <xylon/xsdom/Occurrence.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * The Loop function is a model of a loop that must be executed a minimum
          * number of times and a maximum of times. The maximum number maybe
          * unlimited.
          */
         class Loop : public Instruction
         {
               Loop(const Loop&);
               Loop&operator=(const Loop&);

               /** Default constructor */
            protected:
               Loop() throws();

               /** Destructor */
            public:
               ~Loop() throws();

               /**
                * Create a loop instruction
                * @param occurs the occurrence value
                * @param body the loop body
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ::xylon::xsdom::Occurrence& occurs,
                     const ::timber::Reference< Instruction>& body) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the number of occurrences of this loop. The loop body must at least be executed the specified minimum number of times.
                * @return the occurrences of this loop
                */
            public:
               virtual ::xylon::xsdom::Occurrence occurs() const throws() = 0;

               /**
                * Get the loop body. The loop body are the instructions that must be executed serially
                * in this loop.
                * @return a list of instructions that need to be executed one after the other
                */
            public:
               virtual ::timber::Reference< Instruction> body() const throws() = 0;
         };

      }
   }
}
#endif
