#ifndef _XYLON_MODEL_INSTRUCTIONS_NEWINSTANCE_H
#define _XYLON_MODEL_INSTRUCTIONS_NEWINSTANCE_H

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      class TypeModel;
      namespace instructions {

         /**
          * This instruction instantiates an instance of a type (global or local)
          * and makes it the active type.
          */
         class NewInstance : public Instruction
         {
               NewInstance(const NewInstance&);
               NewInstance&operator=(const NewInstance&);

               /** Default constructor */
            protected:
               NewInstance() throws();

               /** Destructor */
            public:
               ~NewInstance() throws();

               /**
                * Create a new instruction to instantiate an element whose type is a global type.
                * @param ref a reference to a global type.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ModelRef< TypeModel>& model) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the the global type being referenced. If this method returns an invalid reference,
                * then type() must return a valid pointer.
                * @return the global type or an invalid reference.
                */
            public:
               virtual ModelRef< TypeModel> type() const throws() = 0;
         };

      }
   }
}

#endif
