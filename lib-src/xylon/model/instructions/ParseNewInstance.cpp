#include <xylon/model/instructions/ParseNewInstance.h>
#include <xylon/model/instructions/ParseInstance.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/model/instructions/Sequence.h>
#include <xylon/model/instructions/Error.h>
#include <xylon/model/TypeModel.h>

namespace xylon {
   namespace model {
      namespace instructions {
         namespace {
            struct Impl : public ParseNewInstance
            {
                  Impl(const ModelRef< TypeModel>& xtype, const ::timber::Reference< Instruction>& xparseInstruction,
                        const ::timber::Reference< Instruction>& xifTrue,
                        const ::timber::Reference< Instruction>& xifFalse) :
                     _type(xtype), _parse(xparseInstruction), _ifTrue(xifTrue), _ifFalse(xifFalse)
                  {
                  }
                  ~Impl() throws()
                  {
                  }
                  ModelRef< TypeModel> type() const throws()
                  {
                     return _type;
                  }
                  ::timber::Reference< Instruction> parseInstruction() const throws()
                  {
                     return _parse;
                  }
                  ::timber::Reference< Instruction> ifTrue() const throws()
                  {
                     return _ifTrue;
                  }

                  ::timber::Reference< Instruction> ifFalse() const throws()
                  {
                     return _ifFalse;
                  }

                  ModelRef< TypeModel> _type;

                  ::timber::Reference< Instruction> _parse;
                  ::timber::Reference< Instruction> _ifTrue;
                  ::timber::Reference< Instruction> _ifFalse;
            };
         }

         ParseNewInstance::ParseNewInstance() throws()
         {
         }
         ParseNewInstance::~ParseNewInstance() throws()
         {
         }

          ::timber::Reference< Instruction> ParseNewInstance::create(const ModelRef< TypeModel>& xmodel,
               const ::timber::Reference< Instruction>& xifTrue, const ::timber::Reference< Instruction>& xifFalse) throws()
         {
            return new Impl(xmodel, ParseInstance::create(xmodel), xifTrue, xifFalse);
         }

         void ParseNewInstance::accept(InstructionVisitor& v)
         {
            v.visitParseNewInstance(toRef< ParseNewInstance> ());
         }

      }
   }
}
