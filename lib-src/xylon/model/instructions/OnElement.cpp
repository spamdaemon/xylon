#include <xylon/model/instructions/OnElement.h>
#include <xylon/model/instructions/Sequence.h>
#include <xylon/model/InstructionVisitor.h>
#include <utility>
#include <functional>
#include <iterator>

namespace xylon {
   namespace model {
      namespace instructions {
         namespace {
            struct Impl : public OnElement
            {
                  Impl() throws() :
                     _ignoreSubstitutionGroups(false)
                  {
                  }
                  ~Impl() throws()
                  {
                  }
                  Candidates matchedElements() const throw ()
                  {
                     return _elements;
                  }

                  ::timber::Pointer< Instruction> ifFalse() const throws()
                  {
                     return _false;
                  }
                  bool isIgnoreSubstitutions() const throws()
                  {
                     return _ignoreSubstitutionGroups;
                  }
                  Candidates _elements;
                  ::timber::Pointer< Instruction> _false;
                  bool _ignoreSubstitutionGroups;
            };

         }
         OnElement::OnElement() throws()
         {
         }
         OnElement::~OnElement() throws()
         {
         }

         ::timber::Reference< OnElement> OnElement::create(const ::std::set< QName>& condition,
               const ::timber::Pointer< Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse,
               bool ignoreSubstitutionGroups) throws()
         {
            ::timber::Reference< Instruction> xifTrue(xtrue ? ::timber::Reference< Instruction>(xtrue)
                  : Sequence::create());

            Impl* res = new Impl();
            res->_false = xfalse;
            for (::std::set< QName>::const_iterator i = condition.begin(); i != condition.end(); ++i) {
               res->_elements.insert(::std::make_pair(*i, xifTrue));
            }
            res->_ignoreSubstitutionGroups = ignoreSubstitutionGroups;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Reference< OnElement> OnElement::create(const Candidates& match, const ::timber::Pointer<
               Instruction>& noneMatched, bool ignoreSubstitutionGroups) throws()
         {
            Impl* res = new Impl();
            res->_false = noneMatched;
            res->_elements = match;
            res->_ignoreSubstitutionGroups = ignoreSubstitutionGroups;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Reference< OnElement> OnElement::create(const QName& condition,
               const ::timber::Pointer< Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse,
               bool ignoreSubstitutionGroups) throws()
         {
            ::timber::Reference< Instruction> xifTrue(xtrue ? ::timber::Reference< Instruction>(xtrue)
                  : Sequence::create());

            Impl* res = new Impl();
            res->_elements.insert(::std::make_pair(condition, xifTrue));
            res->_false = xfalse;
            res->_ignoreSubstitutionGroups = ignoreSubstitutionGroups;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Pointer< Instruction> OnElement::ifFalse() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }

         void OnElement::accept(InstructionVisitor& v)
         {
            v.visitOnElement(toRef< OnElement> ());
         }

      }
   }
}
