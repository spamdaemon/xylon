#include <xylon/model/instructions/Message.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         Message::Message() throws()
         {
         }
         Message::~Message() throws()
         {
         }

         ::timber::Reference< Instruction> Message::create(const ::std::string& msg) throws()
         {
            struct Impl : public Message
            {
                  ~Impl() throws()
                  {
                  }

                  const ::std::string& message() const throws()
                  {
                     return _message;
                  }

                  ::std::string _message;
            };

            Impl* res = new Impl();
            res->_message = msg;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void Message::accept(InstructionVisitor& v)
         {
            v.visitMessage(toRef< Message> ());
         }

      }
   }
}
