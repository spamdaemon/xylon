#include <xylon/model/instructions/BuiltinInstruction.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         BuiltinInstruction::BuiltinInstruction() throws()
         {
         }
         BuiltinInstruction::~BuiltinInstruction() throws()
         {
         }

         void BuiltinInstruction::accept(InstructionVisitor& v)
         {
            v.visitBuiltin(toRef< BuiltinInstruction> ());
         }
      }
   }
}
