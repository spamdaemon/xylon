#include <xylon/model/instructions/CreateAndParse.h>
#include <xylon/model/instructions/ParseInstance.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/model/instructions/Sequence.h>
#include <xylon/model/instructions/Error.h>
#include <xylon/model/TypeModel.h>

namespace xylon {
   namespace model {
      namespace instructions {
         namespace {
            struct Impl : public CreateAndParse
            {
                  Impl(const ModelRef< TypeModel>& model) :
                     _model(model), _haveType(true)
                  {
                  }
                  Impl() :
                     _haveType(false)
                  {
                  }
                  ~Impl() throws()
                  {
                  }
                  ModelRef< TypeModel> type() const throws()
                  {
                     return _model;
                  }
                  bool hasType() const throws()
                  {
                     return _haveType;
                  }

               private:
                  const ModelRef< TypeModel> _model;
                  bool _haveType;
            };
         }

         CreateAndParse::CreateAndParse() throws()
         {
         }
         CreateAndParse::~CreateAndParse() throws()
         {
         }

         ::timber::Reference< Instruction> CreateAndParse::create(const ModelRef< TypeModel>& model) throws()
         {
            assert(model && "Not a valid model");
            return new Impl(model);
         }

         ::timber::Reference< Instruction> CreateAndParse::create() throws()
         {
            return new Impl();
         }

         void CreateAndParse::accept(InstructionVisitor& v)
         {
            v.visitCreateAndParse(toRef< CreateAndParse> ());
         }

      }
   }
}
