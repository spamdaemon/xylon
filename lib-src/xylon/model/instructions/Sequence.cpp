#include <xylon/model/instructions/Sequence.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         Sequence::Sequence() throws()
         {
         }
         Sequence::~Sequence() throws()
         {
         }

         ::timber::Reference< Instruction> Sequence::create(const ::std::vector< ::timber::Reference< Instruction> >& v) throws()
         {
            struct Impl : public Sequence
            {
                  ~Impl() throws()
                  {
                  }
                  size_t count() const throws()
                  {
                     return _list.size();
                  }
                  ::timber::Reference< Instruction> instruction(size_t i) const throws()
                  {
                     return _list.at(i);
                  }

                  ::std::vector< ::timber::Reference< Instruction> > _list;
            };

            if (v.size() == 1) {
               return v[0];
            }
            Impl* res = new Impl();
            res->_list = v;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Reference< Instruction> Sequence::create() throws()
         {
            return create(::std::vector< ::timber::Reference< Instruction> >());
         }

         ::timber::Reference< Instruction> Sequence::create(const ::timber::Reference< Instruction>& first,
               const ::timber::Reference< Instruction>& second) throws()
         {
            ::std::vector< ::timber::Reference< Instruction> > v;
            v.push_back(first);
            v.push_back(second);
            return create(v);
         }

         ::timber::Reference< Instruction> Sequence::create(const ::timber::Reference< Instruction>& first,
               const ::timber::Reference< Instruction>& second, const ::timber::Reference< Instruction>& third) throws()
         {
            ::std::vector< ::timber::Reference< Instruction> > v;
            v.push_back(first);
            v.push_back(second);
            v.push_back(third);
            return create(v);
         }

         void Sequence::accept(InstructionVisitor& v)
         {
            v.visitSequence(toRef< Sequence> ());
         }
      }
   }
}
