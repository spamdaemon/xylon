#include <xylon/model/instructions/AbstractInstruction.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         AbstractInstruction::AbstractInstruction() throws()
         {
         }
         AbstractInstruction::~AbstractInstruction() throws()
         {
         }

         void AbstractInstruction::accept(InstructionVisitor& v)
         {
            v.visitAbstractInstruction(toRef< AbstractInstruction> ());
         }
      }
   }
}
