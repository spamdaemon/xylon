#include <xylon/model/instructions/Bind.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/model/AttributeModel.h>

namespace xylon {
   namespace model {
      namespace instructions {
         Bind::Bind() throws()
         {
         }
         Bind::~Bind() throws()
         {
         }

         ::timber::Reference< Instruction> Bind::create() throws()
         {
            struct Impl : public Bind
            {
                  ~Impl() throws()
                  {
                  }
            };

            Impl* res = new Impl();
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void Bind::accept(InstructionVisitor& v)
         {
            v.visitBind(toRef< Bind> ());
         }

      }
   }
}
