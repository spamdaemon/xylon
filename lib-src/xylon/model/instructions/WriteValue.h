#ifndef _XYLON_MODEL_INSTRUCTIONS_WRITEVALUE_H
#define _XYLON_MODEL_INSTRUCTIONS_WRITEVALUE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#include <string>

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * This instruction is used to write start an element.
          */
         class WriteValue : public Instruction
         {
               WriteValue(const WriteValue&);
               WriteValue&operator=(const WriteValue&);

               /** Default constructor */
            protected:
               WriteValue() throws();

               /** Destructor */
            public:
               ~WriteValue() throws();

               /**
                * Create a write value instruction.
                * @param value the value to be written
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ::std::string& value) throws();

               /**
                * Get the literal value to be emitted
                */
            public:
               virtual const ::std::string& value() const throws() = 0;

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}
#endif
