#include <xylon/model/instructions/Comment.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         Comment::Comment() throws()
         {
         }
         Comment::~Comment() throws()
         {
         }

         ::timber::Reference< Instruction> Comment::create(const ::std::string& msg) throws()
         {
            struct Impl : public Comment
            {
                  ~Impl() throws()
                  {
                  }

                  ::std::string comment() const throws()
                  {
                     return _message;
                  }

                  ::std::string _message;
            };

            Impl* res = new Impl();
            res->_message = msg;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void Comment::accept(InstructionVisitor& v)
         {
            v.visitComment(toRef< Comment> ());
         }

      }
   }
}
