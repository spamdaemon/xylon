#include <xylon/model/instructions/WriteValue.h>
#include <xylon/QName.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         WriteValue::WriteValue() throws()
         {
         }
         WriteValue::~WriteValue() throws()
         {
         }

         ::timber::Reference< Instruction> WriteValue::create(const ::std::string& xvalue) throws()
         {
            struct Impl : public WriteValue
            {

                  ~Impl() throws()
                  {
                  }
                  const ::std::string& value() const throws()
                  {
                     return _value;
                  }
                  ::std::string _value;
            };

            Impl* res = new Impl();
            res->_value = xvalue;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void WriteValue::accept(InstructionVisitor& v)
         {
            v.visitWriteValue(toRef< WriteValue> ());
         }

      }
   }
}
