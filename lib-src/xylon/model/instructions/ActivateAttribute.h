#ifndef _XYLON_MODEL_INSTRUCTIONS_ACTIVATEATTRIBUTE_H
#define _XYLON_MODEL_INSTRUCTIONS_ACTIVATEATTRIBUTE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#include <set>

namespace xylon {
  namespace model {
    namespace instructions {

      /**
       * The ActivateAttribute instruction checks if the current attribute is one of the specified attributes.
       * If it is, then the body of this attribute is executed. After the instruction has been completed,
       * the previously active object is restored.
       */
      class ActivateAttribute  : public Instruction {
	ActivateAttribute(const ActivateAttribute&);
	ActivateAttribute&operator=(const ActivateAttribute&);

	/** Default constructor */
      protected:
	ActivateAttribute() throws();

	/** Destructor */
      public:
	~ActivateAttribute() throws();

	/**
	 * Create an ActivateAttribute instruction
	 * @param condition the attribute
	 * @param xtrue the statement if the condition is true
	 * @param xfalse the statemetn if the condition is false
	 * @return an instruction
	 */
      public:
	static ::timber::Reference<Instruction> create (const QName& attr, 
							const ::timber::Pointer<Instruction>& xtrue= ::timber::Pointer<Instruction>(),
							const ::timber::Pointer<Instruction>& xfalse= ::timber::Pointer<Instruction>()) throws();

	/** 
	 * Accept the specified visitor.
	 * @param v a visitor
	 */
      public:
	void accept (InstructionVisitor& v);

	/**
	 * The attribute to test for.
	 * @return the attribute 
	 */
      public:
	virtual QName attribute() const throws() = 0;
	
	/**
	 * Get the instruction to be executed if the current XML attribute is has a name
	 * defined by one of attributes().
	 * @return an instruction
	 */
      public:
	virtual ::timber::Pointer<Instruction> ifTrue() const  throws();

	/**
	 * Get the instruction to be executed if the current XML attribute is not one 
	 * of the specified attributes
	 * @return an instruction
	 */
      public:
	virtual ::timber::Pointer<Instruction> ifFalse() const throws();
      };

    }
  }
}
#endif
