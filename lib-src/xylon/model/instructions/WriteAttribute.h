#ifndef _XYLON_MODEL_INSTRUCTIONS_WRITEATTRIBUTE_H
#define _XYLON_MODEL_INSTRUCTIONS_WRITEATTRIBUTE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_MODEL_INSTRUCTIONS_STARTWRITEOBJECT_H
#include <xylon/model/instructions/StartWriteObject.h>
#endif

#ifndef _XYLON_MODEL_INSTRUCTIONS_ENDWRITEOBJECT_H
#include <xylon/model/instructions/EndWriteObject.h>
#endif

namespace xylon {
   namespace model {
      class AttributeModel;
      namespace instructions {

         /**
          * This instruction is used to write start an element.
          */
         class WriteAttribute : public Instruction
         {
               WriteAttribute(const WriteAttribute&);
               WriteAttribute&operator=(const WriteAttribute&);

               /** Default constructor */
            protected:
               WriteAttribute() throws();

               /** Destructor */
            public:
               ~WriteAttribute() throws();

               /**
                * Create a new instruction to bind an attribute model to an attribute. If checkXsiType
                * is true, then the attribute type must correspond to
                * @param model the name of the XML attribute
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction>
               create(const ::timber::Reference< AttributeModel>& model) throws();

               /**
                * Create a new instruction to bind an attribute model to an attribute. If checkXsiType
                * is true, then the attribute type must correspond to
                * @param model the name of the XML attribute
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction>
               create(const ::timber::Reference< AttributeModel>& model,
                     const ::timber::Reference< StartWriteObject>& before,
                     const ::timber::Reference< EndWriteObject>& after) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the instruction to invoke before writing the attribute.
                */
            public:
               virtual ::timber::Pointer< StartWriteObject> before() const throws() = 0;

               /**
                * The model that is bound to the attribute. This may be null
                * @return an attribute model
                */
            public:
               virtual ::timber::Reference< AttributeModel> attribute() const throws() = 0;
               /**
                * Get the instruction to invoke after writing the attribute.
                */
            public:
               virtual ::timber::Pointer< EndWriteObject> after() const throws() = 0;

         };

      }
   }
}
#endif
