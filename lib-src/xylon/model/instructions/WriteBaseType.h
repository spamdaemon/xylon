#ifndef _XYLON_MODEL_INSTRUCTIONS_WRITEBASETYPE_H
#define _XYLON_MODEL_INSTRUCTIONS_WRITEBASETYPE_H

#ifndef _XYLON_MODEL_MODELREF_H_
#include <xylon/model/ModelRef.h>
#endif

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      class TypeModel;
      namespace instructions {

         /**
          * This instruction is used to indicate that the base-type of the currently written type or element
          * should be emitted.
          */
         class WriteBaseType : public Instruction
         {
               WriteBaseType(const WriteBaseType&);
               WriteBaseType&operator=(const WriteBaseType&);

               /** Default constructor */
            protected:
               WriteBaseType() throws();

               /** Destructor */
            public:
               ~WriteBaseType() throws();

               /**
                * Create a new instruction to bind an attribute model to an attribute.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create() throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}
#endif
