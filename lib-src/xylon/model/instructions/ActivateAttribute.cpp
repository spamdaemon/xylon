#include <xylon/model/instructions/ActivateAttribute.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
  namespace model {
    namespace instructions {
      ActivateAttribute::ActivateAttribute() throws() {}
      ActivateAttribute::~ActivateAttribute() throws() {}

      ::timber::Reference<Instruction> ActivateAttribute::create (const QName& attr, const ::timber::Pointer<Instruction>& xtrue, const ::timber::Pointer<Instruction>& xfalse) throws()
      {
	struct Impl : public ActivateAttribute {
	  QName attribute() const throws() { return _attribute; }
	  ::timber::Pointer<Instruction> ifTrue() const  throws() { return _true; }
	  ::timber::Pointer<Instruction> ifFalse() const  throws() { return _false; }
	  QName _attribute;
	  ::timber::Pointer<Instruction> _true;
	  ::timber::Pointer<Instruction> _false;
	};

	Impl* res = new Impl();
	res->_true = xtrue;
	res->_false = xfalse;
	res->_attribute = attr;
	return ::timber::Reference<Instruction>(static_cast<Instruction*>(res));
      }
      
      ::timber::Pointer<Instruction> ActivateAttribute::ifTrue() const  throws() { return  ::timber::Pointer<Instruction>(); }
      ::timber::Pointer<Instruction> ActivateAttribute::ifFalse() const throws() { return  ::timber::Pointer<Instruction>(); }
      void ActivateAttribute::accept (InstructionVisitor& v) 
      { v.visitActivateAttribute(toRef<ActivateAttribute>()); }

    }
  }
}
