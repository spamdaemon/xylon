#ifndef _XYLON_MODEL_INSTRUCTIONS_SEQUENCE_H
#define _XYLON_MODEL_INSTRUCTIONS_SEQUENCE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#include <vector>

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * The Sequence function is a model of a sequence that must be executed a minimum
          * number of times and a maximum of times. The maximum number maybe
          * unlimited.
          */
         class Sequence : public Instruction
         {
               Sequence(const Sequence&);
               Sequence&operator=(const Sequence&);

               /** Default constructor */
            protected:
               Sequence() throws();

               /** Destructor */
            public:
               ~Sequence() throws();

               /**
                * Create a sequence a no-op instruction.
                * @return a sequence
                */
            public:
               static ::timber::Reference< Instruction> create() throws();

               /**
                * Create a sequence consisting of a vector of instructions.
                * @param v a vector instructions
                * @return a sequence
                */
            public:
               static ::timber::Reference< Instruction> create(
                     const ::std::vector< ::timber::Reference< Instruction> >& v) throws();

               /**
                 * Create a sequence consisting of two instructions.
                 * @param first the first instruction
                 * @param second the second instruction
                 * @return a sequence
                 */
             public:
                static ::timber::Reference< Instruction> create(const ::timber::Reference< Instruction>& first, const ::timber::Reference< Instruction>& second) throws();

                /**
                  * Create a sequence consisting of three instructions.
                  * @param first the first instruction
                  * @param second the second instruction
                  * @param third the second instruction
                  * @return a sequence
                  */
              public:
                 static ::timber::Reference< Instruction> create(const ::timber::Reference< Instruction>& first, const ::timber::Reference< Instruction>& second, const ::timber::Reference< Instruction>& third) throws();

                /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * The number of instructions
                * @return the number of instructions
                */
            public:
               virtual size_t count() const throws() = 0;

               /**
                * Get the specified numbered instruction in this sequence.
                * @param i the instruction number (0 to count()-1)
                * @return an instruction
                */
            public:
               virtual ::timber::Reference< Instruction> instruction(size_t i) const throws() = 0;
         };

      }
   }
}
#endif
