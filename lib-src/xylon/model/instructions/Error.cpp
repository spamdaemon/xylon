#include <xylon/model/instructions/Error.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {
         namespace {
            struct Impl : public Error
            {
                  Impl(const ::std::string& msg) :
                     _message(msg)
                  {
                  }

                  Impl()
                  {
                  }

                  ~Impl() throws()
                  {
                  }

                  const ::std::string& message() const throws()
                  {
                     return _message;
                  }

               private:
                  const ::std::string _message;
            };

         }
         Error::Error() throws()
         {
         }
         Error::~Error() throws()
         {
         }

         ::timber::Reference< Instruction> Error::create() throws()
         {
            return new Impl();
         }

         ::timber::Reference< Instruction> Error::create(const ::std::string& msg) throws()
         {
            return new Impl(msg);
         }

         void Error::accept(InstructionVisitor& v)
         {
            v.visitError(toRef< Error> ());
         }

      }
   }
}
