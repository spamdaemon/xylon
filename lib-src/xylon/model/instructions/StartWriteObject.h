#ifndef _XYLON_MODEL_INSTRUCTIONS_STARTWRITEOBJECT_H
#define _XYLON_MODEL_INSTRUCTIONS_STARTWRITEOBJECT_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   class QName;
   namespace model {
      namespace instructions {

         /**
          * This instruction is used to write start an element.
          */
         class StartWriteObject : public Instruction
         {
               StartWriteObject(const StartWriteObject&);
               StartWriteObject&operator=(const StartWriteObject&);

               /** Default constructor */
            protected:
               StartWriteObject() throws();

               /** Destructor */
            public:
               ~StartWriteObject() throws();

               /**
                * Create an message instruction
                * @param name the name of the element
                * @param isElement true if the we want to end an element
                * @return an start element instruction
                */
            private:
               static ::timber::Reference< Instruction> create(const QName& name, bool isElement) throws();

               /**
                * Create an message instruction
                * @param name the name of the element
                * @return an start element instruction
                */
            public:
               inline static ::timber::Reference< Instruction> createElement(const QName& name) throws()
               {
                  return create(name, true);
               }

               /**
                * Create an message instruction
                * @param name the name of the element
                * @return an start element instruction
                */
            public:
               inline static ::timber::Reference< Instruction> createAttribute(const QName& name) throws()
               {
                  return create(name, false);
               }
               /**
                * Get the name of the element.
                */
            public:
               virtual const QName& name() const throws() = 0;

               /**
                * True if this is an instruction to start an element.
                * @return if this an element should be started
                */
            public:
               virtual bool isStartElement() const throws() = 0;

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}
#endif
