#include <xylon/model/instructions/IterateList.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
  namespace model {
    namespace instructions {

      IterateList::IterateList() throws() {}
      IterateList::~IterateList() throws() {}

      ::timber::Reference<Instruction> IterateList::create (const ::timber::Reference<Instruction>& xbody) throws()
      {
	struct Impl  : public IterateList {
	  ~Impl() throws() {}
	  ::timber::Reference<Instruction> body() const throws() { return _instruction; }
	  
	  ::timber::Pointer<Instruction> _instruction;
	};
	
	Impl* res = new Impl();
	res->_instruction = xbody;
	return ::timber::Reference<Instruction>(static_cast<Instruction*>(res));
      }

      

      void IterateList::accept (InstructionVisitor& v) 
      { v.visitIterateList(toRef<IterateList>()); }

    }
  }
}
