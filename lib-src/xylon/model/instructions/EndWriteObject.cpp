#include <xylon/model/instructions/EndWriteObject.h>
#include <xylon/QName.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         EndWriteObject::EndWriteObject() throws()
         {
         }
         EndWriteObject::~EndWriteObject() throws()
         {
         }

         ::timber::Reference< Instruction> EndWriteObject::create(const QName& xname, bool isElement) throws()
         {
            struct Impl : public EndWriteObject
            {

                  ~Impl() throws()
                  {
                  }

                  const QName& name() const throws()
                  {
                     return _name;
                  }

                  bool isEndElement() const throws()
                  {
                     return _element;
                  }

                  QName _name;
                  bool _element;
            };

            Impl* res = new Impl();
            res->_name = xname;
            res->_element = isElement;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void EndWriteObject::accept(InstructionVisitor& v)
         {
            v.visitEndWriteObject(toRef< EndWriteObject> ());
         }

      }
   }
}
