#include <xylon/model/instructions/OnAttribute.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {
         OnAttribute::OnAttribute() throws()
         {
         }
         OnAttribute::~OnAttribute() throws()
         {
         }

         ::timber::Reference< Instruction> OnAttribute::create(const QName& condition, const ::timber::Pointer<
               Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse) throws()
         {
            struct Impl : public OnAttribute
            {
                  ~Impl() throws()
                  {
                  }

                  const QName& attribute() const throws()
                  {
                     return _attribute;
                  }
                  ::timber::Pointer< Instruction> ifTrue() const throws()
                  {
                     return _true;
                  }
                  ::timber::Pointer< Instruction> ifFalse() const throws()
                  {
                     return _false;
                  }
                  QName _attribute;
                  ::timber::Pointer< Instruction> _true;
                  ::timber::Pointer< Instruction> _false;
            };

            Impl* res = new Impl();
            res->_true = xtrue;
            res->_false = xfalse;
            res->_attribute = condition;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Pointer< Instruction> OnAttribute::ifTrue() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }
         ::timber::Pointer< Instruction> OnAttribute::ifFalse() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }
         void OnAttribute::accept(InstructionVisitor& v)
         {
            v.visitOnAttribute(toRef< OnAttribute> ());
         }

      }
   }
}
