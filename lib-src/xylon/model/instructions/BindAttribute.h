#ifndef _XYLON_MODEL_INSTRUCTIONS_BINDATTRIBUTE_H
#define _XYLON_MODEL_INSTRUCTIONS_BINDATTRIBUTE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      class AttributeModel;
      namespace instructions {

         /**
          * This instruction takes the type instance currently on top of the stack and
          * binds it to the attribute. Depending on the type of attribute, this is a set()
          * or an append() operation, or something different.
          */
         class BindAttribute : public Instruction
         {
               BindAttribute(const BindAttribute&);
               BindAttribute&operator=(const BindAttribute&);

               /** Default constructor */
            protected:
               BindAttribute() throws();

               /** Destructor */
            public:
               ~BindAttribute() throws();

               /**
                * Create a new instruction to bind an attribute model to an attribute.
                * @param attrName the name of the XML attribute
                * @param model an attribute model
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction>
               create(const ::timber::Reference< AttributeModel>& model) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * The model that is bound to the attribute. This may be null
                * @return an attribute model
                */
            public:
               virtual ::timber::Reference< AttributeModel> boundModel() const throws() = 0;
         };

      }
   }
}

#endif
