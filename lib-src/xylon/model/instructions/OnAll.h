#ifndef _XYLON_MODEL_INSTRUCTIONS_ONALL_H
#define _XYLON_MODEL_INSTRUCTIONS_ONALL_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#include <map>
#include <vector>

namespace xylon {
   namespace model {
      namespace instructions {
         class OnElement;

         /**
          * The OnAll instruction is used parse a list of elements. All elements in an order,
          * but may only appear at most once or twice.
          */
         class OnAll : public Instruction
         {
               OnAll(const OnAll&);
               OnAll&operator=(const OnAll&);

               /** This map contains the QNames and the instruction to be executed for each when matched. */
            public:
               typedef ::std::map< QName, ::timber::Reference< Instruction> > Candidate;

               /** This map contains the QNames and the instruction to be executed for each when matched. */
            public:
               typedef ::std::vector<Candidate > Candidates;

               /** Default constructor */
            protected:
               OnAll() throws();

               /** Destructor */
            public:
               ~OnAll() throws();

               /**
                * Create an OnAll instruction. The if-false condition for the specified OnElement
                * instructions are ignored.
                * @param required the elements that are required
                * @param optional the elements that are optional
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(
                     const Candidates& required, const Candidates& optional) throws();
               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * The elements to test for. The ifFalse() branch is always null.
                * @return the elements
                */
            public:
               virtual Candidates optional() const throws() = 0;

               /**
                * The elements to test for.The ifFalse() branch is always null.
                * @return the elements
                */
            public:
               virtual Candidates required() const throws() = 0;
         };

      }
}
}
#endif
