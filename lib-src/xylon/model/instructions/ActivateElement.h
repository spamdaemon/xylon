#ifndef _XYLON_MODEL_INSTRUCTIONS_ACTIVATEELEMENT_H
#define _XYLON_MODEL_INSTRUCTIONS_ACTIVATEELEMENT_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * Push an instance of a global type. How the contents of the type are
          * pushed depends on the ScopeName of the type. When parsing is complete
          * the cursor will point to the next logical element, which is dependent upon
          * the type being pushed. For elements  the next logical element is the following element.
          * For groups, then next logical element is the element not pushed as part of the group.
          * For complex types, the next logical element is the first child element of the current element
          * that was not pushed.
          * An instance of the type being is already at the top of the stack.
          */
         class ActivateElement : public Instruction
         {
               ActivateElement(const ActivateElement&);
               ActivateElement&operator=(const ActivateElement&);

               /** Default constructor */
            protected:
               ActivateElement() throws();

               /** Destructor */
            public:
               ~ActivateElement() throws();

               /**
                * Create a new instruction to instantiate an element whose type is a global type.
                * @param ref a reference to a global type.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create() throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}

#endif
