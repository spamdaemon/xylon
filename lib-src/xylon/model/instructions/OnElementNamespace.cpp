#include <xylon/model/instructions/OnElementNamespace.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         OnElementNamespace::OnElementNamespace() throws()
         {
         }

         OnElementNamespace::~OnElementNamespace() throws()
         {
         }

         ::timber::Reference< Instruction> OnElementNamespace::create(const ::std::set< String>& condition,
               const ::timber::Pointer< Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse) throws()
         {
            struct Impl : public OnElementNamespace
            {
                  ~Impl() throws()
                  {
                  }

                  ::std::set< String> namespaces() const throws()
                  {
                     return _condition;
                  }
                  ::timber::Pointer< Instruction> ifTrue() const throws()
                  {
                     return _true;
                  }
                  ::timber::Pointer< Instruction> ifFalse() const throws()
                  {
                     return _false;
                  }
                  ::std::set< String> _condition;
                  ::timber::Pointer< Instruction> _true;
                  ::timber::Pointer< Instruction> _false;
            };

            Impl* res = new Impl();
            res->_true = xtrue;
            res->_false = xfalse;
            res->_condition = condition;
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         ::timber::Reference< Instruction> OnElementNamespace::create(const String& condition, const ::timber::Pointer<
               Instruction>& xtrue, const ::timber::Pointer< Instruction>& xfalse) throws()
         {
            ::std::set< String> c;
            c.insert(condition);
            return create(c, xtrue, xfalse);
         }

         ::timber::Pointer< Instruction> OnElementNamespace::ifTrue() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }

         ::timber::Pointer< Instruction> OnElementNamespace::ifFalse() const throws()
         {
            return ::timber::Pointer< Instruction>();
         }

         void OnElementNamespace::accept(InstructionVisitor& v)
         {
            v.visitOnElementNamespace(toRef< OnElementNamespace> ());
         }

      }
   }
}
