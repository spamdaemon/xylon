#include <xylon/model/instructions/ActivateValue.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
  namespace model {
    namespace instructions {
      ActivateValue::ActivateValue() throws() {}
      ActivateValue::~ActivateValue() throws() {}

      ::timber::Reference<Instruction> ActivateValue::create () throws()
      {
	struct Impl : public ActivateValue {
	};

	Impl* res = new Impl();
	return ::timber::Reference<Instruction>(static_cast<Instruction*>(res));
      }
      
      void ActivateValue::accept (InstructionVisitor& v) 
      { v.visitActivateValue(toRef<ActivateValue>()); }

    }
  }
}
