#ifndef _XYLON_MODEL_INSTRUCTIONS_FOREACHATTRIBUTE_H
#define _XYLON_MODEL_INSTRUCTIONS_FOREACHATTRIBUTE_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          * The ForeachAttribute instruction is used to loop over all attributes of an XML element. For each
          * attribute, the specified instruction is executed.
          */
         class ForeachAttribute : public Instruction
         {
               ForeachAttribute(const ForeachAttribute&);
               ForeachAttribute&operator=(const ForeachAttribute&);

               /** Default constructor */
            protected:
               ForeachAttribute() throws();

               /** Destructor */
            public:
               ~ForeachAttribute() throws();

               /**
                * Create an instruction that loops over each attribute of the current element.
                * @param body the loop body
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(const ::timber::Reference< Instruction>& body) throws();

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);

               /**
                * Get the loop body. The loop body are the instructions that must be executed serially
                * in this loop.
                * @return a list of instructions that need to be executed one after the other
                */
            public:
               virtual ::timber::Reference< Instruction> body() const throws() = 0;
         };

      }
   }
}
#endif
