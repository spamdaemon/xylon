#ifndef _XYLON_MODEL_INSTRUCTIONS_MOVEELEMENT_H
#define _XYLON_MODEL_INSTRUCTIONS_MOVEELEMENT_H

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

namespace xylon {
   namespace model {
      namespace instructions {

         /**
          *This instruction moves the current element cursor up, down, or to the next element.
          */
         class MoveCursor : public Instruction
         {
               MoveCursor(const MoveCursor&);
               MoveCursor&operator=(const MoveCursor&);

               /**
                * The type of move to make.
                */
            public:
               enum Move
               {
                  /** move to the parent element */
                  UP_ELEMENT,
                  /** move to the child element */
                  DOWN_ELEMENT,
                  /** move to the sibling element */
                  NEXT_ELEMENT
               };

               /** Default constructor */
            protected:
               MoveCursor() throws();

               /** Destructor */
            public:
               ~MoveCursor() throws();

               /**
                * Create an NextElement instruction.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> createUp() throws()
               {
                  return create(UP_ELEMENT);
               }

               /**
                * Create an NextElement instruction.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> createDown() throws()
               {
                  return create(DOWN_ELEMENT);
               }

               /**
                * Create an NextElement instruction.
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> createNext() throws()
               {
                  return create(NEXT_ELEMENT);
               }

               /**
                * Create an MoveElement instruction.
                * @param dir the direction in which to move
                * @return an instruction
                */
            public:
               static ::timber::Reference< Instruction> create(Move dir) throws();

               /**
                * Get the direction into which to move the element cursor.
                * @return the direction of movement
                */
            public:
               virtual Move direction() const throws() = 0;

               /**
                * Accept the specified visitor.
                * @param v a visitor
                */
            public:
               void accept(InstructionVisitor& v);
         };

      }
   }
}
#endif
