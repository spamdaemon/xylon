#include <xylon/model/instructions/WriteBaseType.h>
#include <xylon/model/TypeModel.h>
#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      namespace instructions {

         WriteBaseType::WriteBaseType() throws()
         {
         }
         WriteBaseType::~WriteBaseType() throws()
         {
         }
         ::timber::Reference< Instruction> WriteBaseType::create() throws()
         {
            struct Impl : public WriteBaseType
            {

                  ~Impl() throws()
                  {
                  }

            };

            Impl* res = new Impl();
            return ::timber::Reference< Instruction>(static_cast< Instruction*> (res));
         }

         void WriteBaseType::accept(InstructionVisitor& v)
         {
            v.visitWriteBaseType(toRef< WriteBaseType> ());
         }

      }
   }
}
