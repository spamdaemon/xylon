#include <xylon/model/ElementTypeModel.h>
#include <xylon/model/ModelVisitor.h>
#include <xylon/model/Instruction.h>

namespace xylon {
   namespace model {

      ElementTypeModel::ElementTypeModel() throws()
      {
      }

      ElementTypeModel::~ElementTypeModel() throws()
      {
      }

      void ElementTypeModel::accept(ModelVisitor& v) throws()
      {
         v.visitElementType(toRef< ElementTypeModel> ());
      }

   }
}
