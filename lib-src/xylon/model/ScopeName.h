#ifndef _XYLON_MODEL_SCOPENAME_H
#define _XYLON_MODEL_SCOPENAME_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _XYLON_GLOBALNAME_H
#include <xylon/GlobalName.h>
#endif

#include <iosfwd>

namespace xylon {
   namespace model {
      /**
       * A ScopeName consists of a namespace uri and a local tag name. The prefix
       * of a node is not considered.
       */
      class ScopeName
      {
            /** The node types that can be bound to a class or attribute */
         public:
            enum ScopeType
            {
               ELEMENT, GROUP, ATTRIBUTE, ATTRIBUTE_GROUP, TYPE, KEY, SCHEMA, UNSPECIFIED
            };


            /**
             * Default constructor. This constructors exists for convenience only
             * and results in an invalid ScopeName
             */
         public:
            ScopeName() throws();

            /**
             * Create a new ScopeName consisting of a namespace URL and a local name
             * @param ns the namespace uri
             * @param name the name
             * @param type a scope type
             */
         public:
            ScopeName(const String& ns, const String& name, ScopeType type) throws();

            /**
             * Create a new ScopeName for a name.
             * @param name a name
             */
         public:
            ScopeName(const GlobalName& name) throws();

            /**
             * Create a new ScopeName for a URL only.
             * @param ns a namespace URL
             */
         public:
            ScopeName(const String& ns, ScopeType type) throws();

            /**
             * Determine if this is a valid ScopeName
             * @return true if this is a valid ScopeName
             */
         public:
            inline operator bool() const throws()
            {
               return _name || _namespace;
            }

            /**
             * Compare this ScopeName to the specified ScopeName.
             * @param n a ScopeName
             * @return true if the two names are exactly the same
             */
         public:
            bool operator==(const ScopeName& n) const throws();

            /**
             * Compare this ScopeName to the specified ScopeName.
             * @param n a ScopeName
             * @return true if the this ScopeName is lexicographically less
             */
         public:
            bool operator<(const ScopeName& n) const throws();

            /**
             * Get the local name
             * @return the local name
             */
         public:
            inline const String& name() const throws()
            {
               return _name;
            }

            /**
             * Get the local name
             * @return the local name
             */
         public:
            inline const String& namespaceURI() const throws()
            {
               return _namespace;
            }

            /**
             * Get a unique string for this ScopeName
             * @return the string for this ScopeName that will also be emitted
             */
         public:
            String string() const throws();

            /**
             * Get the scope type
             * @return the scope type
             */
         public:
            inline ScopeType type() const throws() { return _type; }

            /** The namespace uri and the name */
         private:
            String _namespace, _name;

            /** The scope type */
         private:
            ScopeType _type;
      };
   }
}
::std::ostream& operator<<(::std::ostream& out, const ::xylon::model::ScopeName& ScopeName);

#endif

