#ifndef _XYLON_MODEL_ELEMENTTYPEMODEL_H
#define _XYLON_MODEL_ELEMENTTYPEMODEL_H

#ifndef _XYLON_MODEL_TYPEMODEL_H
#include <xylon/model/TypeModel.h>
#endif

namespace xylon {
   namespace model {

      /**
       * This type model is used to model elements with a type attribute. In addition to a standard
       * processing instruction, such types also support processing instructions that take an additional
       * type.
       */
      class ElementTypeModel : public virtual TypeModel
      {
            ElementTypeModel&operator=(const ElementTypeModel&);
            ElementTypeModel(const ElementTypeModel&) throws();

            /** The attributes */
         public:
            typedef ::std::vector< ::timber::Reference< AttributeModel> > Attributes;

            /** The locally defined */
         public:
            typedef ::std::vector< ModelRef< TypeModel> > LocalTypes;

            /** Constructor */
         public:
            ElementTypeModel() throws();

            /** Destructor */
         public:
            ~ElementTypeModel() throws();

            /**
             * Accept a visitor.
             * @param v
             */
         public:
            void accept(ModelVisitor& v) throws();

            /**
             * Get the content type for this element.
             * @return the content type for this element.
             */
         public:
            virtual ModelRef<TypeModel> contentModelType() const throws() = 0;

            /**
             * Create a parse instruction for the element given the element's type.
             * @param type a type model.
             * @return a parse instruction for this element and the specified content type.
             */
         public:
            virtual ::timber::Reference< ::xylon::model::Instruction> createParseInstruction(const ModelRef< TypeModel>& xmodel) const throws() = 0;
      };
   }
}

#endif
