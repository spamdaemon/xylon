#ifndef _XYLON_MODEL_SCOPEMODEL_H
#define _XYLON_MODEL_SCOPEMODEL_H

#ifndef _XYLON_H
#include <xylon/xylon.h>
#endif

#ifndef _XYLON_MODEL_MODEL_H
#include <xylon/model/Model.h>
#endif

#ifndef _XYLON_MODEL_SCOPENAME_H
#include <xylon/model/ScopeName.h>
#endif

namespace xylon {
  namespace model {
    /**
     * An ScopeModel is used to represent namespaces of a TypeModel.
     */
    class ScopeModel : public Model {
      ScopeModel&operator=(const ScopeModel&);
      ScopeModel(const ScopeModel&) throws();
      
      /** Constructor */
    protected:
      ScopeModel() throws();

      /** Destructor */
    public:
      ~ScopeModel() throws();

      /**
       * Get the scoped name defined by this model.
       * @return a scoped name.
       */
    public:
      virtual ScopeName scopeName() const throws() = 0;

    };
  }
}

#endif
