#include <xylon/model/InstructionVisitor.h>

namespace xylon {
   namespace model {
      InstructionVisitor::InstructionVisitor() throws()
      {
      }
      InstructionVisitor::~InstructionVisitor() throws()
      {
      }

      void InstructionVisitor::visitBuiltin(
            const ::timber::Reference< ::xylon::model::instructions::BuiltinInstruction>&)
      {
      }
      void InstructionVisitor::visitAbstractInstruction(const ::timber::Reference<
            ::xylon::model::instructions::AbstractInstruction>&)
      {
      }
      void InstructionVisitor::visitActivateElement(const ::timber::Reference<
            ::xylon::model::instructions::ActivateElement>&)
      {
      }
      void InstructionVisitor::visitComment(const ::timber::Reference< ::xylon::model::instructions::Comment>&)
      {
      }
      void InstructionVisitor::visitActivateAttribute(const ::timber::Reference<
            ::xylon::model::instructions::ActivateAttribute>&)
      {
      }
      void InstructionVisitor::visitActivateValue(const ::timber::Reference<
            ::xylon::model::instructions::ActivateValue>&)
      {
      }
      void InstructionVisitor::visitParseInstance(const ::timber::Reference<
            ::xylon::model::instructions::ParseInstance>&)
      {
      }
      void InstructionVisitor::visitParseNewInstance(const ::timber::Reference<
            ::xylon::model::instructions::ParseNewInstance>&)
      {
      }
      void InstructionVisitor::visitIterateList(const ::timber::Reference< ::xylon::model::instructions::IterateList>&)
      {
      }

      void InstructionVisitor::visitBind(const ::timber::Reference< ::xylon::model::instructions::Bind>&)
      {
      }
      void InstructionVisitor::visitBindAttribute(const ::timber::Reference<
            ::xylon::model::instructions::BindAttribute>&)
      {
      }
      void InstructionVisitor::visitBindElement(const ::timber::Reference< ::xylon::model::instructions::BindElement>&)
      {
      }
      void InstructionVisitor::visitNextElement(const ::timber::Reference< ::xylon::model::instructions::MoveCursor>&)
      {
      }
      void InstructionVisitor::visitError(const ::timber::Reference< ::xylon::model::instructions::Error>&)
      {
      }
      void InstructionVisitor::visitMessage(const ::timber::Reference< ::xylon::model::instructions::Message>&)
      {
      }
      void InstructionVisitor::visitLoop(const ::timber::Reference< ::xylon::model::instructions::Loop>&)
      {
      }
      void InstructionVisitor::visitNewInstance(const ::timber::Reference< ::xylon::model::instructions::NewInstance>&)
      {
      }
      void InstructionVisitor::visitOnAll(const ::timber::Reference< ::xylon::model::instructions::OnAll>&)
      {
      }
      void InstructionVisitor::visitOnAttribute(const ::timber::Reference< ::xylon::model::instructions::OnAttribute>&)
      {
      }
      void InstructionVisitor::visitOnAttributeNamespace(const ::timber::Reference<
            ::xylon::model::instructions::OnAttributeNamespace>&)
      {
      }
      void InstructionVisitor::visitOnElement(const ::timber::Reference< ::xylon::model::instructions::OnElement>&)
      {
      }
      void InstructionVisitor::visitOnElementNamespace(const ::timber::Reference<
            ::xylon::model::instructions::OnElementNamespace>&)
      {
      }
      void InstructionVisitor::visitOnValidElement(const ::timber::Reference<
            ::xylon::model::instructions::OnValidElement>&)
      {
      }
      void InstructionVisitor::visitOnXsiType(const ::timber::Reference< ::xylon::model::instructions::OnXsiType>&)
      {
      }
      void InstructionVisitor::visitSequence(const ::timber::Reference< ::xylon::model::instructions::Sequence>&)
      {
      }
      void InstructionVisitor::visitForeachAttribute(const ::timber::Reference<
            ::xylon::model::instructions::ForeachAttribute>&)
      {
      }

      void InstructionVisitor::visitCreateAndParse(const ::timber::Reference<
            ::xylon::model::instructions::CreateAndParse>&)
      {
      }

      void InstructionVisitor::visitStartWriteObject(const ::timber::Reference<
            ::xylon::model::instructions::StartWriteObject>&)
      {
      }
      void InstructionVisitor::visitEndWriteObject(const ::timber::Reference<
            ::xylon::model::instructions::EndWriteObject>&)
      {
      }
      void InstructionVisitor::visitWriteValue(const ::timber::Reference< ::xylon::model::instructions::WriteValue>&)
      {
      }

      void InstructionVisitor::visitWriteAttribute(const ::timber::Reference< ::xylon::model::instructions::WriteAttribute>&)
      {
      }

      void InstructionVisitor::visitWriteBaseType(const ::timber::Reference< ::xylon::model::instructions::WriteBaseType>&)
      {
      }

   }
}

