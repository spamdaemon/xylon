#ifndef _XYLON_MODEL_DOMVALIDATOR_H
#define _XYLON_MODEL_DOMVALIDATOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_W3C_XML_DOM_ELEMENT_H
#include <timber/w3c/xml/dom/Element.h>
#endif

namespace xylon {
   class XsdSchemaModel;
   namespace model {

      /**
       * This class is validator for DOM nodes conforming to a specific schema.
       */
      class DOMValidator
      {

            /** Default constructor */
         protected:
            DOMValidator() throws();

            /** Destructor */
         public:
            virtual ~DOMValidator() throws() = 0;

            /**
             * Create a new validator for the specified schema.
             * @param schema a schema binding
             * @return a validator
             */
         public:
            static ::std::unique_ptr< DOMValidator> create(const ::timber::Reference< ::xylon::XsdSchemaModel>& schema) throws();

            /**
             * Apply this dom walker to the specified tree.
             * @param node a dom tree
             * @throws ::std::exception if the element could not be validated
             */
         public:
            virtual void
            validate(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& node) throws (::std::exception) =0;

      };
   }
}

#endif

