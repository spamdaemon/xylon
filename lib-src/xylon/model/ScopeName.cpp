#include <xylon/model/ScopeName.h>

using namespace ::std;
using namespace ::timber;

namespace {
   static ::xylon::String typeToString(::xylon::model::ScopeName::ScopeType t)
   {
      switch (t) {
         case ::xylon::model::ScopeName::ATTRIBUTE:
            return "ATTRIBUTE";
         case ::xylon::model::ScopeName::ATTRIBUTE_GROUP:
            return "ATTRIBUTE_GROUP";
         case ::xylon::model::ScopeName::ELEMENT:
            return "ELEMENT";
         case ::xylon::model::ScopeName::GROUP:
            return "GROUP";
         case ::xylon::model::ScopeName::KEY:
            return "KEY";
         case ::xylon::model::ScopeName::SCHEMA:
            return "SCHEMA";
         case ::xylon::model::ScopeName::UNSPECIFIED:
            return "UNSPECIFIED";
         default:
            return "UNSPECIFIED";
      }
   }
}

namespace xylon {
   namespace model {
      ScopeName::ScopeName() throws() :
         _type(UNSPECIFIED)
      {
      }

      ScopeName::ScopeName(const String& ns, const String& nm, ScopeType t) throws() :
         _namespace(ns), _name(nm), _type(t)
      {
      }

      ScopeName::ScopeName(const GlobalName& qname) throws() :
         _namespace(qname.name().namespaceURI()), _name(qname.name().name())
      {
         switch (qname.type()) {
            case GlobalName::ATTRIBUTE:
               _type = ATTRIBUTE;
               break;
            case GlobalName::ATTRIBUTE_GROUP:
               _type = ATTRIBUTE_GROUP;
               break;
            case GlobalName::ELEMENT:
               _type = ELEMENT;
               break;
            case GlobalName::GROUP:
               _type = GROUP;
               break;
            case GlobalName::KEY:
               _type = KEY;
               break;
            case GlobalName::TYPE:
               _type = TYPE;
               break;
            default:
               _type = UNSPECIFIED;
               break;
         }
      }

      ScopeName::ScopeName(const String& ns, ScopeType t) throws() :
         _namespace(ns), _type(t)
      {
      }

      bool ScopeName::operator==(const ScopeName& n) const throws()
      {
         return _name == n._name && _namespace == n._namespace && _type == n._type;
      }

      bool ScopeName::operator<(const ScopeName& n) const throws()
      {
         int cmp = static_cast< int> (_type) - static_cast< int> (n._type);
         if (cmp == 0) {
            cmp = _name.compare(n._name);
            if (cmp == 0) {
               if (_namespace) {
                  if (n._namespace) {
                     cmp = _namespace.compare(n._namespace);
                  }
                  else {
                     cmp = 1;
                  }
               }
               else if (n._namespace) {
                  cmp = -1;
               }
            }
         }
         return cmp < 0;
      }

      String ScopeName::string() const throws()
      {
         if (!_name && !_namespace) {
            return "{}";
         }

         String res = "";

         if (_name) {
            res += _name;
         }
         if (_namespace) {
            res += '{';
            res += _namespace;
            res += '}';
         }
         res += typeToString(_type);

         return res;
      }
   }
}

ostream& operator<<(::std::ostream& out, const ::xylon::model::ScopeName& sn)
{
   if (sn.name()) {
      out << sn.name();
   }
   if (sn.namespaceURI()) {
      out << '{' << sn.namespaceURI() << "}";
   }
   out << "{" << typeToString(sn.type()) << "}";
   return out;
}

