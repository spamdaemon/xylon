#ifndef _XYLON_MODEL_MODEL_H
#define _XYLON_MODEL_MODEL_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace xylon {
  namespace model {
    class ModelVisitor;

    /** 
     * The Model is the base class for all models of XML schema objects
     * to objects in a given programming language.
     */
    class Model : public ::timber::Counted {
      /**
       * Create a new model
       */
    protected:
      Model() throws();
      
      /** Destructor */
    public:
      ~Model() throws();

      /**
       * Clear this model. This function removes any <em>downward</em> references to other model
       * elements. This ensures that any circular references that an implementation might have
       * are broken.<br>
       */
    public:
      virtual void clear() throws() = 0;

      /** 
       * Accept the specified model visitor.
       * @param v a visitor
       */
    public:
      virtual void accept (ModelVisitor& v) throws() = 0;
    };
    
  }
}
#endif
