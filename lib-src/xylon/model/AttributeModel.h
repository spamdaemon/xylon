#ifndef _XYLON_MODEL_ATTRIBUTEMODEL_H
#define _XYLON_MODEL_ATTRIBUTEMODEL_H

#ifndef _XYLON_MODEL_MODEL_H
#include <xylon/model/Model.h>
#endif

#ifndef _XYLON_MODEL_TYPEMODEL_H
#include <xylon/model/TypeModel.h>
#endif

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

#ifndef _XYLON_XSDOM_OCCURRENCE_H
#include <xylon/xsdom/Occurrence.h>
#endif

namespace xylon {
   namespace model {
      class TypeModel;
      class Method;

      /**
       * An AttributeModel is used to represent attributes of a TypeModel.
       */
      class AttributeModel : public Model
      {
            AttributeModel&operator=(const AttributeModel&);
            AttributeModel(const AttributeModel&) throws();

            /** The type of attribute model. This is used to indicate if the attribute
             * corresponds to an element, a type, attribute, or a reference to a group, etc. */
         public:
            enum Type
            {
               ELEMENT, ATTRIBUTE, OTHER
            };

            /** The unbounded value; a value set to all 1's*/
         public:
            static const size_t UNBOUNDED = ~static_cast<size_t>(0);

            /** Constructor */
         protected:
            AttributeModel() throws();

            /** Destructor */
         public:
            ~AttributeModel() throws();

            /**
             * Get the attribute's type.
             * @return the type of attribute
             */
         public:
            virtual Type attributeType() const throws() = 0;

            /**
             * Accept the specified model visitor.
             * @param v a visitor
             */
          public:
            virtual void accept (ModelVisitor& v) throws();

            /** The name of this attribute */
         public:
            virtual String name() const throws() = 0;

            /**
             * Get the TypeModel in whose scope this attribute is defined.
             * @return a type model
             */
         public:
            virtual ModelRef< TypeModel> scope() const throws() = 0;

            /**
             * Get the type of the attribute when it is defined as a global type.
             * @return the attribute's global type
             */
         public:
            virtual ModelRef< TypeModel> type() const throws() = 0;

            /**
             * Get the minimum value for the multiplicity.
             * @return the minimum number of times this attribute must at least occur
             */
         public:
            virtual size_t minOccurs() const throws() = 0;


            /**
             * Get the maximum value for the multiplicity. If the number of occurrences
             * is unbounded, then the value UNBOUNDED must be returned.
             * @return the maximum number of times this attribute may occur.
             */
         public:
            virtual size_t maxOccurs() const throws() = 0;

      };
   }
}

#endif
