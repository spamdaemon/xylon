#include <xylon/model/DOMValidator.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/model/models.h>
#include <xylon/XsdSchemaModel.h>
#include <xylon/XsdSchema.h>
#include <xylon/xsdom/SimpleType.h>
#include <xylon/xsdom/dom/util.h>
#include <xylon/xsdom/Element.h>
#include <xylon/model/instructions.h>

#include <timber/logging.h>
#include <timber/w3c/xml/dom/CharacterData.h>
#include <timber/w3c/xml/dom/Element.h>
#include <timber/w3c/xml/dom/Attr.h>
#include <timber/w3c/xml/dom/dom.h>

#include <stack>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::timber::w3c::xml::dom;
using namespace ::xylon::model::instructions;

namespace xylon {
   namespace model {
      namespace {
         static Log logger()
         {
            return Log("xylon.model.DOMValidator");
         }

         struct ValidationVisitor : public InstructionVisitor
         {
               ValidationVisitor(const Reference< ::xylon::XsdSchemaModel>& s, const ::timber::Reference<
                     ::timber::w3c::xml::dom::Element>& node) throws() :
                  _schema(s)
               {
                  _eStack.push(node);
                  _active = node;
               }

               ~ValidationVisitor() throws()
               {
               }

               /**
                * Pop the entire stack and generate a string that can be provide some hints
                * about the current location in the document.
                * @return
                */
               string popStack()
               {
                  string elementStack;
                  while (!_eStack.empty()) {
                     Pointer< Element> e = _eStack.top();
                     if (e) {
                        string name;
#if 0
                        if (e->hasAttributeNS(String(), "name")) {
                           name = " name=\"";
                           name += e->getAttributeNS(String(), "name").string();
                           name += '"';
                        }
#endif
                        elementStack = '<' + e->prefix().string() + ":" + e->localName().string() + name + ">"
                              + elementStack;
                     }
                     _eStack.pop();
                  }
                  return elementStack;
               }

               QName getElementModel(const QName& expected) const throw ()
               {
                  QName qname = qnameOf(_eStack.top());
                  if (qname == expected) {
                     return expected;
                  }

                  ::xylon::model::ModelRef< ::xylon::model::TypeModel> actualModel = _schema->getElementModel(qname);

                  ::xylon::model::ModelRef< ::xylon::model::TypeModel> model = actualModel;

                  while (model) {
                     if (model->globalName().name() == expected) {
                        return qnameOf(_eStack.top());
                     }
                     model = model->baseType();
                  }
                  return QName();
               }

               Pointer< Instruction> findElement(const ::std::map< QName, Reference< Instruction> >& instr,
                     bool ignoreSubstitutionGroup)
               {
                  Pointer< Instruction> res;
                  QName actual = qnameOf(_eStack.top());
                  try {
                     do {
                        ::std::map< QName, Reference< Instruction> >::const_iterator i = instr.find(actual);
                        if (i != instr.end()) {
                           res = i->second;
                           break;
                        }
                        if (ignoreSubstitutionGroup) {
                           break;
                        }
                        ::xylon::model::ModelRef< ::xylon::model::TypeModel> model = _schema->getElementModel(actual);
                        if (!model) {
                           break;
                        }
                        model = model->baseType();
                        if (!model) {
                           break;
                        }
                        actual = model->globalName().name();
                     } while (actual);
                  }
                  catch (...) {
                     // ignore
                  }
                  return res;
               }

               static QName qnameOf(const ::timber::Reference< Element>& e)
               {
                  return QName(e->namespaceURI(), e->localName());
               }

               static String namespaceOf(const ::timber::Reference< Element>& e)
               {
                  return e->namespaceURI();
               }

               void builtinParseList(const Reference< ListTypeModel>& model)
               {
                  activateValue();

                  LogEntry(logger()).info() << "*builtinParseList \"" << _value << '"' << doLog;
                  if (!_value) {
                     throw ::std::runtime_error("No list value");
                  }
                  vector< String> values = ::xylon::xsdom::dom::parseListValue(_value);
                  _value = String();
                  for (size_t i = 0; i < values.size(); ++i) {

                     _value = values[i];
                     LogEntry(logger()).info() << "LIST VALUE " << i << " : " << _value << doLog;
                     newInstance(model->type().target());
                     parseInstance(model->type().target());

                     //FIXME: need to bind the parsed value
                     _value = String();
                  }
               }

               void builtinParseUnion(const Reference< UnionTypeModel>& model)
               {
                  activateValue();
                  LogEntry(logger()).info() << "*builtinParseUnion \"" << _value << '"' << doLog;
                  if (!_value) {
                     throw ::std::runtime_error("No union value");
                  }
                  vector< ModelRef< TypeModel> > types = model->types();

                  String value = _value;
                  for (size_t i = 0; i < types.size(); ++i) {
                     try {
                        newInstance(types[i].target());
                        parseInstance(types[i].target());
                        _value = String();
                        return;
                     }
                     catch (...) {
                        // could not parse the type, try another
                        _value = value; // restore the value
                     }
                  }
                  _value = String();
                  throw ::std::runtime_error("Could not parse union type");
               }

               Reference< TypeModel> newInstance(const Reference< TypeModel>& type)
               {
                  Reference< TypeModel> res = type;
                  switch (type->globalName().type()) {
                     case GlobalName::ELEMENT:
                        if (type->globalName().name() != QName::xsQName("any")) {
                           QName qname = type->globalName().name();
                           QName actual = getElementModel(qname);
                           if (!actual) {
                              throw ::std::runtime_error("Cannot find expected element " + qname.string().string());
                           }
                           res = _schema->getElementModel(actual).target();
                        }
                        break;
                     default:
                        // noop
                        break;
                  }
                  if (res->isAbstract()) {
                     throw ::std::runtime_error("Type is abstract; cannot instantiate "
                           + type->globalName().string().string());
                  }
                  return res;
               }

               void parseInstance(const Reference< TypeModel>& t)
               {
                  // we need to figure out if we're parsing a simple type or a complex type
                  // for now, we're
                  if (t->globalName().name().namespaceURI() == QName::XSD_NAMESPACE_URI) {

                     // get the type from the schema manager
                     Pointer< ::xylon::xsdom::Type> ct;
                     if (t->globalName().type() == GlobalName::TYPE) {
                        ct = _schema->schema()->findType(t->globalName().name());
                     }
                     if (ct) {
                        if (ct.tryDynamicCast< ::xylon::xsdom::SimpleType> ()) {
                           visit(ActivateValue::create());
                           LogEntry(logger()).info() << "Parse simple type " << t->globalName() << " : \"" << _value
                                 << '"' << doLog;
                        }
                        else if (t->parseInstruction()) {
                           visit(t->parseInstruction());
                        }
                     }
                     else if (t->globalName().name().name() == "anySimpleType") {
                        // unknown type; probably the anySimpleType
                        visit(ActivateValue::create());
                        LogEntry(logger()).info() << "Parse as xs:anySimpleType (" << t->globalName() << ") : \""
                              << _value << '"' << doLog;
                     }
                     else if (t->parseInstruction()) {
                        visit(t->parseInstruction());
                     }
                  }
                  else if (t.tryDynamicCast< ListTypeModel> ()) {
                     builtinParseList(t.tryDynamicCast< ListTypeModel> ());
                  }
                  else if (t.tryDynamicCast< UnionTypeModel> ()) {
                     builtinParseUnion(t.tryDynamicCast< UnionTypeModel> ());
                  }
                  else if (t->parseInstruction()) {
                     visit(t->parseInstruction());
                  }
                  else {
                     string global(t->isGlobal() ? "global" : "local");
                     LogEntry(logger()).warn() << "Do not know how to parse " << global << "type : " << t->globalName()
                           << " [ " << t->name() << " ]" << doLog;
                     throw ::std::runtime_error("Do not know how to parse : " + t->globalName().string().string());
                  }
               }

               String valueOf(const Element& e) const
               {
                  String value("");
                  Pointer< Node> n = e.firstChild();
                  while (n) {
                     if (n.tryDynamicCast< CharacterData> ()) {
                        value += n->nodeValue();
                     }
                     else if (n->nodeType() == Node::ELEMENT_NODE) {
                        LogEntry(logger()).severe() << "Mixed element content not allowed : found element "
                              << n->localName() << " in " << e.localName() << doLog;
                        throw ::std::runtime_error("Mixed element content not allowed");
                     }
                     n = n->nextSibling();
                  }
                  return value;
               }

               Pointer< Instruction> testElement(const ::std::map< QName, Reference< Instruction> >& e) const throws()
               {
                  if (!_eStack.empty() && _eStack.top()) {
                     for (::std::map< QName, Reference< Instruction> >::const_iterator i = e.begin(); i != e.end(); ++i) {
                        if (getElementModel(i->first)) {
                           return i->second;
                        }
                     }
                  }
                  return Pointer< Instruction> ();
               }

               void visit(const ::timber::Pointer< Instruction>& e)
               {
                  if (e) {
                     e->accept(*this);
                  }
               }

               void activateValue()
               {
                  if (_value) {
                     return; // already active
                  }
                  if (_active) {
                     Pointer< Attr> attr = _active.tryDynamicCast< Attr> ();
                     if (attr) {
                        _value = attr->nodeValue();
                     }
                     Pointer< Element> elmt = _active.tryDynamicCast< Element> ();
                     if (elmt) {
                        elmt->normalize();
                        _value = valueOf(*elmt);
                     }
                     LogEntry(logger()).info() << "Activated value : " << _value << doLog;
                  }
               }

               void visitAbstractInstruction(const ::timber::Reference<
                     ::xylon::model::instructions::AbstractInstruction>& obj)
               {
                  LogEntry(logger()).info() << "*AbstractInstruction " << doLog;
                  visit(obj->instruction());
               }

               void visitBindElement(const ::timber::Reference< ::xylon::model::instructions::BindElement>& obj)
               {
                  LogEntry(logger()).info() << "*BindElement " << doLog;
               }

               void visitBindAttribute(const ::timber::Reference< ::xylon::model::instructions::BindAttribute>& obj)
               {
                  LogEntry(logger()).info() << "*BindAttribute " << obj->boundModel()->name() << doLog;
               }

               void visitBind(const ::timber::Reference< ::xylon::model::instructions::Bind>& obj)
               {
                  LogEntry(logger()).info() << "*Bind" << doLog;
               }

               void visitIterateList(const ::timber::Reference< ::xylon::model::instructions::IterateList>& obj)
               {
                  LogEntry(logger()).info() << "*IterateList \"" << _value << '"' << doLog;
                  if (!_value) {
                     throw ::std::runtime_error("No list value");
                  }
                  vector< String> values = ::xylon::xsdom::dom::parseListValue(_value);
                  _value = String();
                  for (size_t i = 0; i < values.size(); ++i) {
                     _value = values[i];
                     LogEntry(logger()).info() << "LIST VALUE " << i << " : " << _value << doLog;
                     visit(obj->body());
                     _value = String();
                  }
               }

               void downElement()
               {
                  LogEntry(logger()).info() << "*DownElement" << doLog;
                  assert(!_eStack.empty());
                  Pointer< Node> e = _eStack.top();
                  Pointer< Node> old = e;
                  if (!e) {
                     logger().warn("DownElement: null pointer");
                  }
                  else {
                     e = e->firstChild();
                     while (e && e->nodeType() != Node::ELEMENT_NODE) {
                        e = e->nextSibling();
                     }
                  }
                  LogEntry(logger()).info() << "DownElement " << (old ? old->localName() : String()) << "  -->"
                        << (e ? e->localName() : String()) << doLog;
                  _eStack.push(e);
               }

               void upElement()
               {
                  LogEntry(logger()).info() << "*UpElement" << doLog;
                  assert(!_eStack.empty());
                  _eStack.pop();
                  Pointer< Element> e;
                  if (!_eStack.empty()) {
                     e = _eStack.top();
                  }
                  LogEntry(logger()).info() << "UpElement " << (e ? e->localName() : String()) << doLog;
               }
               void nextElement()
               {
                  LogEntry(logger()).info() << "*NextElement" << doLog;
                  assert(!_eStack.empty());
                  Pointer< Node> e = _eStack.top();
                  if (!e) {
                     logger().warn("NextElement: null pointer");
                  }
                  else {
                     do {
                        e = e->nextSibling();
                     } while (e && e->nodeType() != Node::ELEMENT_NODE);
                  }
                  LogEntry(logger()).info() << "NextElement " << (e ? e->localName() : String()) << doLog;
                  _eStack.pop();
                  _eStack.push(e);
               }

               void visitNextElement(const ::timber::Reference< ::xylon::model::instructions::MoveCursor>& obj)
               {
                  LogEntry(logger()).info() << "*MoveElement" << obj->direction() << doLog;
                  if (_eStack.empty()) {
                     throw ::std::runtime_error("NextElement: no elements on stack");
                  }
                  switch (obj->direction()) {
                     case ::xylon::model::instructions::MoveCursor::NEXT_ELEMENT:
                        nextElement();
                        break;
                     case ::xylon::model::instructions::MoveCursor::DOWN_ELEMENT:
                        downElement();
                        break;
                     case ::xylon::model::instructions::MoveCursor::UP_ELEMENT:
                        upElement();
                        break;
                  }
               }

               void visitUpElement()
               {
                  LogEntry(logger()).info() << "*UpElement" << doLog;
                  if (_eStack.empty()) {
                     throw ::std::runtime_error("UpElement : no parent to go to");
                  }
                  _eStack.pop();
                  Pointer< Element> e;
                  if (!_eStack.empty()) {
                     e = _eStack.top();
                  }
                  LogEntry(logger()).info() << "UpElement " << (e ? e->localName() : String()) << doLog;
               }

               void visitError(const ::timber::Reference< ::xylon::model::instructions::Error>& obj)
               {
                  LogEntry(logger()).severe() << "*Error @ " << popStack() << doLog;
                  logger().severe("Document error");
                  assert(false);
                  throw ::std::runtime_error("Could not parse document");
               }

               void visitMessage(const ::timber::Reference< ::xylon::model::instructions::Message>& obj)
               {
                  LogEntry(logger()).info() << "*Message : " << obj->message() << doLog;
               }

               void visitLoop(const ::timber::Reference< ::xylon::model::instructions::Loop>& obj)
               {
                  LogEntry(logger()).info() << "*Loop " << obj->occurs().min() << " -> " << obj->occurs().max()
                        << doLog;
                  if (_eStack.empty()) {
                     throw ::std::runtime_error("Loop: no elements");
                  }

                  Pointer< Element> cur = _eStack.top();
                  size_t i = 0;
                  while (i < obj->occurs().min() && cur) {
                     visit(obj->body());
                     if (cur == _eStack.top()) {
                        break;
                     }
                     cur = _eStack.top();
                     ++i;
                  }

                  if (i < obj->occurs().min()) {
                     LogEntry(logger()).severe() << "Loop: expected " << obj->occurs().min()
                           << " occurrences , but only found " << i << " occurrence" << doLog;
                     throw ::std::runtime_error("Loop: too few occurrences " + popStack());
                  }
                  while (cur && (obj->occurs().max() == ::xylon::xsdom::Occurrence::UNBOUNDED || i
                        < obj->occurs().max())) {
                     LogEntry(logger()).info() << "Loop: iteratation " << i << doLog;
                     visit(obj->body());
                     if (cur == _eStack.top()) {
                        // not consumed any elements
                        break;
                     }
                     cur = _eStack.top();
                     ++i;
                  }
               }

               void visitNewInstance(const ::timber::Reference< ::xylon::model::instructions::NewInstance>& obj)
               {
                  newInstance(obj->type().target());
                  LogEntry(logger()).info() << "*NewInstance " << (obj->type() ? obj->type()->globalName().string()
                        : String()) << doLog;
                  logger().debugging("NewInstance: ignored");
               }

               void visitOnAll(const ::timber::Reference< ::xylon::model::instructions::OnAll>& obj)
               {
                  LogEntry(logger()).info() << "*OnAll" << doLog;

                  ::xylon::model::instructions::OnAll::Candidates required = obj->required();
                  ::xylon::model::instructions::OnAll::Candidates optional = obj->optional();

                  while (!required.empty() || !optional.empty()) {
                     Pointer< Element> e = _eStack.top();

                     for (size_t i = 0; i < required.size(); ++i) {
                        Pointer< Instruction> instr = testElement(required[i]);
                        if (instr) {
                           visit(instr);
                           required[i] = required.back();
                           required.pop_back();
                        }
                     }

                     for (size_t i = 0; i < optional.size(); ++i) {
                        Pointer< Instruction> instr = testElement(optional[i]);
                        if (instr) {
                           visit(instr);
                           optional[i] = optional.back();
                           optional.pop_back();
                        }
                     }
                     // did we make any progress
                     if (e == _eStack.top()) {
                        // no progress
                        if (required.empty()) {
                           // we've matched all required elements
                           break;
                        }
                        throw ::std::runtime_error("OnAll: not all required elements were found");
                     }
                  }
               }
               void visitOnXsiType(const ::timber::Reference< ::xylon::model::instructions::OnXsiType>& obj)
               {
                  LogEntry(logger()).info() << "*OnXsiType" << doLog;

                  //FIXME: no longer valid
                  Pointer< Attr> attr = _eStack.top()->getAttributeNodeNS(QName::XSI_NAMESPACE_URI, "type");
                  if (!attr) {
                     visit(CreateAndParse::create(obj->expectedType()));
                     return;
                  }

                  String prefix, localName;
                  splitQName(attr->nodeValue(), prefix, localName);
                  String namespaceURI = attr->lookupNamespaceURI(prefix);
                  const QName actual(namespaceURI, localName);
                  ::xylon::model::ModelRef< ::xylon::model::TypeModel> actualModel = _schema->getTypeModel(actual);

                  // find out if type type is a proper derived type of the expected type
                  QName expected = obj->expectedType()->globalName().name();
                  ::xylon::model::ModelRef< ::xylon::model::TypeModel> model = actualModel;
                  while (model) {
                     if (model->globalName().name() == expected) {
                        // ok the actual model
                        visit(CreateAndParse::create(actualModel));
                        return;
                     }
                     model = model->baseType();
                     if (!model) {
                        break;
                     }
                  }
                  visit(Error::create("Could not parse xsi:type " + actual.string().string()));

#if 0
                  ::std::map< QName, ::timber::Reference< Instruction> > matched = obj->matched();
                  for (::std::map< QName, ::timber::Reference< Instruction> >::const_iterator i = matched.begin(); i
                        != matched.end(); ++i) {
                     if (i->first == qname) {
                        visit(i->second);
                        return;
                     }
                  }
                  visit(obj->notMatched());
#endif
               }

               void visitOnAttribute(const ::timber::Reference< ::xylon::model::instructions::OnAttribute>& obj)
               {
                  LogEntry(logger()).info() << "*OnAttribute" << doLog;
                  if (!_eStack.empty() && _eStack.top()) {

                     const QName& q = obj->attribute();
                     Pointer< Attr> attr = _eStack.top()->getAttributeNodeNS(q.namespaceURI(), q.name());
                     if (attr) {
                        visit(obj->ifTrue());
                        return;
                     }
                  }
                  visit(obj->ifFalse());
               }

               void visitOnAttributeNamespace(const ::timber::Reference<
                     ::xylon::model::instructions::OnAttributeNamespace>& obj)
               {
                  LogEntry(logger()).info() << "*OnAttributeNamespace" << doLog;
                  ::timber::Pointer< ::timber::w3c::xml::dom::Attr> attr = _active.tryDynamicCast<
                        ::timber::w3c::xml::dom::Attr> ();
                  if (attr) {
                     set< String> ns = obj->namespaces();
                     if (ns.count(attr->namespaceURI())) {
                        visit(obj->ifTrue());
                        return;
                     }
                  }
                  visit(obj->ifFalse());
                  return;
               }

               void visitOnElement(const ::timber::Reference< ::xylon::model::instructions::OnElement>& obj)
               {
                  LogEntry(logger()).info() << "*OnElement" << doLog;
                  map< QName, Reference< Instruction> > candidates = obj->matchedElements();

                  Pointer< Instruction> instr;
                  if (!_eStack.empty() && _eStack.top()) {
                     instr = findElement(obj->matchedElements(), obj->isIgnoreSubstitutions());
                  }
                  if (instr) {
                     visit(instr);
                  }
                  else {
                     visit(obj->ifFalse());
                  }
#if 0
                  if (!_eStack.empty() && _eStack.top()) {
                     for (map< QName, Reference< Instruction> >::const_iterator i = candidates.begin(); i
                           != candidates.end(); ++i) {
                        if (i->first == qnameOf(_eStack.top())) {
                           visit(i->second);
                           return;
                        }
                     }
                  }
                  visit(obj->ifFalse());
#endif
               }

               void visitOnElementNamespace(
                     const ::timber::Reference< ::xylon::model::instructions::OnElementNamespace>& obj)
               {
                  LogEntry(logger()).info() << "*OnElementNamespace" << doLog;
                  if (_eStack.empty() || !_eStack.top() || obj->namespaces().count(namespaceOf(_eStack.top())) == 0) {
                     visit(obj->ifFalse());
                  }
                  else {
                     visit(obj->ifTrue());
                  }
               }

               void visitOnValidElement(const ::timber::Reference< ::xylon::model::instructions::OnValidElement>& obj)
               {
                  LogEntry(logger()).info() << "*OnValidElement" << doLog;
                  if (_eStack.empty() || !_eStack.top()) {
                     visit(obj->ifFalse());
                     return;
                  }
                  if (obj->requireKnownElement()) {
                     QName qname = qnameOf(_eStack.top());
                     // this will thrown an exception if the element isn't known
                     if (!_schema->schema()->findElement(qname)) {
                        visit(obj->ifFalse());
                        return;
                     }
                  }
                  visit(obj->ifTrue());
                  return;

               }

               void visitForeachAttribute(
                     const ::timber::Reference< ::xylon::model::instructions::ForeachAttribute>& obj)
               {
                  ::timber::Reference< ::timber::w3c::xml::dom::NodeList> attrs = _eStack.top()->getAttributeList();

                  Pointer< Node> activeBak = _active;
                  String valueBak = _value;
                  for (size_t i = 0, sz = attrs->length(); i < sz; ++i) {
                     Reference< Attr> attr = attrs->item(i);
                     if (attr->localName() == "xmlns" || attr->namespaceURI() == QName::XSI_NAMESPACE_URI
                           || attr->prefix() == "xmlns") {
                        continue;
                     }
                     _active = attrs->item(i);
                     _value = String();
                     visit(obj->body());
                  }

                  _active = activeBak;
                  _value = valueBak;
               }

               void visitActivateAttribute(
                     const ::timber::Reference< ::xylon::model::instructions::ActivateAttribute>& obj)
               {
                  LogEntry(logger()).info() << "*ActivateAttribute " << obj->attribute().name() << doLog;
                  if (_eStack.empty() || !_eStack.top()) {
                     throw ::std::runtime_error("NoElement: cannot activate attribute "
                           + obj->attribute().name().string());
                  }
                  const QName qname = obj->attribute();
                  Pointer< Attr> attr = _eStack.top()->getAttributeNodeNS(qname.namespaceURI(), qname.name());
                  if (attr) {
                     Pointer< Node> backupNode = _active;
                     String backupString = _value;
                     _active = attr;
                     _value = String();
                     LogEntry(logger()).info() << "*TRUE " << obj->attribute().name() << doLog;
                     visit(obj->ifTrue());
                     _active = backupNode;
                     _value = backupString;
                  }
                  else {
                     LogEntry(logger()).info() << "*FALSE " << obj->attribute().name() << doLog;
                     visit(obj->ifFalse());
                  }
               }

               void visitActivateElement(const ::timber::Reference< ::xylon::model::instructions::ActivateElement>& obj)
               {
                  LogEntry(logger()).info() << "*ActivateElement" << doLog;
                  if (_eStack.empty() || !_eStack.top()) {
                     throw ::std::runtime_error("ActivateElement: not element to activate");
                  }
                  _active = _eStack.top();
                  _value = String();
               }

               void visitActivateValue(const ::timber::Reference< ::xylon::model::instructions::ActivateValue>&)
               {
                  LogEntry(logger()).info() << "*ActivateValue" << doLog;
                  activateValue();
               }

               void visitParseInstance(const ::timber::Reference< ::xylon::model::instructions::ParseInstance>& obj)
               {
                  assert(obj->type());
                  ModelRef< TypeModel> t = obj->type();
                  if (!t.isBound()) {
                     throw ::std::runtime_error("Type is not bound");
                  }
                  LogEntry(logger()).info() << "*ParseInstance : " << t->globalName() << "/" << t->name() << ": "
                        << doLog;
                  parseInstance(t.target());
               }

               void visitParseNewInstance(
                     const ::timber::Reference< ::xylon::model::instructions::ParseNewInstance>& obj)
               {

                  // no need to do a new instance here, just parse the group
                  Reference< TypeModel> t = newInstance(obj->type().target());

                  LogEntry(logger()).info() << "*ParseNewInstance : " << t->globalName() << "/" << t->name() << " : "
                        << doLog;
                  bool error = false;
                  try {
                     parseInstance(t);
                  }
                  catch (...) {
                     error = true;
                  }
                  if (error) {
                     obj->ifFalse();
                  }
                  else {
                     obj->ifTrue();
                  }
               }

               void visitSequence(const ::timber::Reference< ::xylon::model::instructions::Sequence>& obj)
               {
                  //  LogEntry(logger()).info() << "*Sequence" << doLog;
                  for (size_t i = 0, sz = obj->count(); i < sz; ++i) {
                     //   LogEntry(logger()).info() << "*Sequence(" << i << ")" << doLog;
                     visit(obj->instruction(i));
                  }
               }

               void visitCreateAndParse(const ::timber::Reference< ::xylon::model::instructions::CreateAndParse>& obj)
               {
                  ModelRef< TypeModel> model = obj->type();
                  if (!obj->hasType()) {
                     QName qname = qnameOf(_eStack.top());
                     model = _schema->getElementModel(qname);
                  }
                  parseInstance(newInstance(model.target()));
               }

               /** The schema to be validated */
            private:
               Reference< ::xylon::XsdSchemaModel> _schema;

               /** The currently active value */
            private:
               String _value;

               /** The currently active node */
            private:
               Pointer< Node> _active;

               /** The element stack; use pointers, because we can push empty nodes onto the stack */
            private:
               ::std::stack< Pointer< Element> > _eStack;
         };

         static void validateElement(const Reference< ::xylon::XsdSchemaModel>& schema, const ::timber::Reference<
               ::timber::w3c::xml::dom::Element>& node)
         {
            ValidationVisitor v(schema, node);
            QName name(node->namespaceURI(), node->localName());

            ModelRef< TypeModel> model = schema->getElementModel(name);
            if (model.isBound()) {
               NewInstance::create(model)->accept(v);
               // we don't need to create a new instance here
               ParseInstance::create(model)->accept(v);
            }
            else {
               throw ::std::runtime_error("Element type not bound " + name.string().string());
            }
         }

      }

      DOMValidator::DOMValidator() throws()
      {
      }
      DOMValidator::~DOMValidator() throws()
      {
      }

      ::std::unique_ptr< DOMValidator> DOMValidator::create(const Reference< ::xylon::XsdSchemaModel>& schema) throws()
      {
         struct Impl : public DOMValidator
         {
               Impl(const Reference< ::xylon::XsdSchemaModel>& s) throws() :
                  _schema(s)
               {
               }
               ~Impl() throws()
               {
               }

               void validate(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& node) throws (::std::exception)
               {
                  validateElement(_schema, node);
               }
            private:
               Reference< ::xylon::XsdSchemaModel> _schema;
         };
         DOMValidator* impl = new Impl(schema);
         return ::std::unique_ptr< DOMValidator>(impl);
      }

   }
}

