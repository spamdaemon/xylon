#ifndef _XYLON_MODEL_UNIONTYPEMODEL_H
#define _XYLON_MODEL_UNIONTYPEMODEL_H

#ifndef _XYLON_MODEL_TYPEMODEL_H
#include <xylon/model/TypeModel.h>
#endif

namespace xylon {
  namespace model {
    
    /**
     * This class represents a union type. It directly corresponds to the
     * &lt;xs:union&gt; simple type.
     */
    class UnionTypeModel : public virtual TypeModel {
      UnionTypeModel(const UnionTypeModel&);
      UnionTypeModel&operator=(const UnionTypeModel&);

      /** Default constructor */
    protected:
      UnionTypeModel() throws();

      /** Destructor */
    public:
      ~UnionTypeModel() throws();

      /** 
       * Accept the specified model visitor.
       * @param v a visitor
       */
    public:
      virtual void accept (ModelVisitor& v) throws();

      /**
       * Get the types that are in the union.
       * @return a list of types
       */
    public:
      virtual ::std::vector< ModelRef<TypeModel> > types() const throws() = 0;
    };
  }
}

#endif
