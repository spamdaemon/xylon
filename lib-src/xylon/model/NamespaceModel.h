#ifndef _XYLON_MODEL_NAMESPACEMODEL_H
#define _XYLON_MODEL_NAMESPACEMODEL_H

#ifndef _XYLON_MODEL_SCOPEMODEL_H
#include <xylon/model/ScopeModel.h>
#endif

namespace xylon {
   namespace model {

      /**
       * The NamespaceModel defines the concept of a namespace scope.
       */
      class NamespaceModel : public ScopeModel
      {
            NamespaceModel(const NamespaceModel&);
            NamespaceModel&operator=(const NamespaceModel&);

            /** Default constructor */
         protected:
            NamespaceModel() throws();

            /** Destructor */
         public:
            ~NamespaceModel() throws();

            /**
             * Accept the specified model visitor.
             * @param v a visitor
             */
         public:
            virtual void accept(ModelVisitor& v) throws();

      };

   }

}

#endif
