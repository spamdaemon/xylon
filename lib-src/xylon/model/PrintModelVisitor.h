#ifndef _XYLON_MODEL_PRINTMODELVISITOR_H
#define _XYLON_MODEL_PRINTMODELVISITOR_H

#ifndef _XYLON_MODEL_MODELVISITOR_H
#include <xylon/model/ModelVisitor.h>
#endif

#include <iosfwd>

namespace xylon {
  namespace model {

    /**
     * This visitor can be used to print a text representation of a model.
     */
    class PrintModelVisitor : public ModelVisitor {
      PrintModelVisitor(const PrintModelVisitor&);
      PrintModelVisitor&operator=(const PrintModelVisitor&);
      
      
      /**
       * Create a new visitor that prints to the specified output stream.
       * @param out an output stream
       */
    public:
      PrintModelVisitor(::std::ostream& out) throws();

      /** Destructor */
    public:
      ~PrintModelVisitor() throws();

      void visitElementType(const ::timber::Reference<ElementTypeModel>& obj);
      void visitListType(const ::timber::Reference<ListTypeModel>& obj);
      void visitUnionType(const ::timber::Reference<UnionTypeModel>& obj);
      void visitType(const ::timber::Reference<TypeModel>& obj);
      void visitMethod(const ::timber::Reference<Method>& obj);
      void visitNamespace(const ::timber::Reference<NamespaceModel>& obj);
      void visitAttribute(const ::timber::Reference<AttributeModel>& obj);

      /** The output stream */
    private:
      ::std::ostream& _stream;
    };
  }
}

#endif

