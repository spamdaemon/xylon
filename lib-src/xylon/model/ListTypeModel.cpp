#include <xylon/model/ListTypeModel.h>
#include <xylon/model/ModelVisitor.h>

namespace xylon {
  namespace model {

    ListTypeModel::ListTypeModel() throws()
    {}

    ListTypeModel::~ListTypeModel() throws()
    {}

    void ListTypeModel::accept (ModelVisitor& v) throws()
    { v.visitListType(toRef<ListTypeModel>()); }
  }
}
