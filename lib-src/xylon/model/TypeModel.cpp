#include <xylon/model/TypeModel.h>
#include <xylon/model/ScopeModel.h>
#include <xylon/model/ModelVisitor.h>
#include <xylon/model/Instruction.h>
#include <xylon/model/AttributeModel.h>
#include <xylon/model/NamespaceModel.h>

namespace xylon {
   namespace model {
      namespace {
         struct BuiltinScope : public NamespaceModel
         {
               BuiltinScope() throws() :
                  _name(QName::XSD_NAMESPACE_URI, ScopeName::UNSPECIFIED)
               {
               }
               ~BuiltinScope() throws()
               {
               }

               ScopeName scopeName() const throws()
               {
                  return _name;
               }
               void clear() throws()
               {
               }

            private:
               const ScopeName _name;
         };

         struct Impl : public TypeModel
         {
               Impl(ModelRef< TypeModel> b, const String& n, const GlobalName& qn, bool c) :
                  _base(b), _name(n), _globalName(qn), _scope(new BuiltinScope()), _scopeName(qn), _complex(c)
               {
               }

               Impl(const String& n, const GlobalName& qn, bool c) :
                  _name(n), _globalName(qn), _scope(new BuiltinScope()), _scopeName(qn), _complex(c)
               {
               }

               ~Impl() throws()
               {
               }

               bool isAbstract() const throw ()
               {
                  return false;
               }
               bool isComplex() const throws()
               {
                  return _complex;
               }

               ::timber::Pointer< Instruction> parseInstruction() const throws()
               {
                  return ::timber::Pointer< Instruction>();
               }

               ::timber::Pointer< Instruction> writeInstruction() const throws()
               {
                  return ::timber::Pointer< Instruction>();
               }

               ModelRef< TypeModel> baseType() const throws()
               {
                  return _base;
               }
               String name() const throws()
               {
                  return _name;
               }
               ::std::vector< ::timber::Reference< AttributeModel> > attributes() const throws()
               {
                  return ::std::vector< ::timber::Reference< AttributeModel> >();
               }
               ::std::vector< ::timber::Reference< TypeModel> > localTypes() const throws()
               {
                  return ::std::vector< ::timber::Reference< TypeModel> >();
               }

               GlobalName globalName() const throws()
               {
                  return _globalName;
               }

               void clear() throws()
               {
               }

               ::timber::Reference< ScopeModel> scope() const throws()
               {
                  return _scope;
               }

               ScopeName scopeName() const throws()
               {
                  return _scopeName;
               }

            private:
               const ModelRef< TypeModel> _base;
               const String _name;
               const GlobalName _globalName;
               const ::timber::Reference< ScopeModel> _scope;
               const ScopeName _scopeName;
               const bool _complex;
         };
      }

      TypeModel::TypeModel() throws()
      {
      }

      TypeModel::~TypeModel() throws()
      {
      }

      void TypeModel::accept(ModelVisitor& v) throws()
      {
         v.visitType(toRef< TypeModel> ());
      }

      ModelRef< TypeModel> TypeModel::createAnyElementType() throws()
      {
         static ModelRef< TypeModel> ref(new Impl("AnyElement", GlobalName(QName::xsQName("any"), GlobalName::ELEMENT),
               true));
         return ref;
      }

      ModelRef< TypeModel> TypeModel::createAnyType() throws()
      {
         //FIXME: what to do about the any type and the complex/simple type classification; doesn't really make sense.
         static ModelRef< TypeModel> ref(new Impl(ModelRef< TypeModel> (), "AnyType", GlobalName(QName::xsQName(
               "anyType"), GlobalName::TYPE), true));
         return ref;
      }
   }
}
