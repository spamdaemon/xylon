#ifndef _XYLON_MODEL_TYPEMODEL_H
#define _XYLON_MODEL_TYPEMODEL_H

#ifndef _XYLON_MODEL_SCOPEMODEL_H
#include <xylon/model/ScopeModel.h>
#endif

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

#ifndef _XYLON_GLOBALNAME_H
#include <xylon/GlobalName.h>
#endif

#include <vector>
#include <string>

namespace xylon {
   namespace model {
      class AttributeModel;
      class TypeModel;
      class Instruction;

      /**
       * The TypeModel maps XML schema artifacts, such as complex types,
       * elements, groups, etc. into types in a target programming language.
       */
      class TypeModel : public ScopeModel
      {
            TypeModel&operator=(const TypeModel&);
            TypeModel(const TypeModel&) throws();

            /** The attributes */
         public:
            typedef ::std::vector< ::timber::Reference< AttributeModel> > Attributes;

            /** The locally defined */
         public:
            typedef ::std::vector< ModelRef< TypeModel> > LocalTypes;

            /** Constructor */
         public:
            TypeModel() throws();

            /** Destructor */
         public:
            ~TypeModel() throws();

            /**
             * The XML Schema ur-type. All types are derived from this type.
             * @return the type model that represents an any complex type
             */
         public:
            static ModelRef< TypeModel> createAnyType() throws();

            /**
             * Get the global type model used to represent the xs:any type
             * @return the type model that represents an any complex type
             */
         public:
            static ModelRef< TypeModel> createAnyElementType() throws();

            /**
             * Accept a visitor.
             * @param v
             */
         public:
            void accept(ModelVisitor& v) throws();

            /**
             * The optional base class for this type. The base class
             * is must be a globally known type.
             */
         public:
            virtual ModelRef< TypeModel> baseType() const throws() = 0;

            /**
             * Get the GlobalName by which this type is known in the XML schema. If this type is a locally
             * defined type, for example, then the global name will not be valid.
             * @return a scope name or an invalid scope name
             */
         public:
            virtual GlobalName globalName() const throws() = 0;

            /**
             * Determine if instanceof the type represented by this model can be instantiated.  The default is false.
             * @return true if this model abstract, false otherwise
             */
         public:
            virtual bool isAbstract() const throws() = 0;

            /**
             * Determine if this is a global type.
             * @return true if globalName()==true
             */
         public:
            inline bool isGlobal() const throws()
            {
               return globalName().type() != GlobalName::UNDEFINED;
            }

            /**
             * Determine if this is a local (or nested) type.
             * @return true if globalName()==false
             */
         public:
            inline bool isLocal() const throws()
            {
               return !isGlobal();
            }

            /*
             * The name of this type within its enclosing scope.
             * @return the name of this type within its enclosing scope
             */
         public:
            virtual String name() const throws() = 0;

             /**
               * Determine if this is a complex type.
               * @return true if this is a complex type.
               */
           public:
              virtual bool isComplex() const throws() = 0;

              /**
               * Determine if this is a simple type.
               * @return true if this is not a complex type
               */
           public:
              inline bool isSimple() const throws()
              {
                 return !isComplex();
              }

            /** The type attributes */
         public:
            virtual ::std::vector< ::timber::Reference< AttributeModel> > attributes() const throws() = 0;

            /** These are the locally defined types. */
         public:
            virtual ::std::vector< ::timber::Reference< TypeModel> > localTypes() const throws() = 0;

            /**
             * The instruction that can be used to populate this type.
             * @return an instruction or null if not known.
             */
         public:
            virtual ::timber::Pointer< Instruction> parseInstruction() const throws() = 0;

            /**
             * The write instruction.
             */
         public:
            virtual ::timber::Pointer< Instruction> writeInstruction() const throws() = 0;

            /**
             * Get the scope in which this type is declared.
             * @return a scope
             */
         public:
            virtual ::timber::Reference< ScopeModel> scope() const throws() = 0;
      };
   }
}

#endif
