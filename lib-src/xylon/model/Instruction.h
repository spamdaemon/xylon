#ifndef _XYLON_MODEL_INSTRUCTION_H
#define _XYLON_MODEL_INSTRUCTION_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <string>

namespace xylon {
   namespace model {
      class InstructionVisitor;

      /**
       * The Instruction is an extension of the model and is used to
       * model instructions and instructions.
       */
      class Instruction : public ::timber::Counted
      {
            Instruction(const Instruction&);
            Instruction&operator=(const Instruction&);

            /** Default constructor */
         protected:
            Instruction() throws();

            /** Destructor */
         public:
            ~Instruction() throws();

            /**
             * Accept the specified visitor.
             * @param v an instruction visitor
             */
         public:
            virtual void accept(InstructionVisitor& v) = 0;

            /**
             * Get the comment for this instruction. Return a possibly multiline comment. The returned
             * comment must not include comment characters, unless they are somehow escaped.
             * @return a string with a comment.
             */
         public:
            virtual ::std::string comment() const throws();

      };

   }
}

#endif
