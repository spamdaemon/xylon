#ifndef _XYLON_MODEL_MODELS_H
#define _XYLON_MODEL_MODELS_H

#ifndef _XYLON_MODEL_NAMESPACEMODEL_H
#include <xylon/model/NamespaceModel.h>
#endif

#ifndef _XYLON_MODEL_TYPEMODEL_H
#include <xylon/model/TypeModel.h>
#endif

#ifndef _XYLON_MODEL_ELEMENTTYPEMODEL_H
#include <xylon/model/ElementTypeModel.h>
#endif

#ifndef _XYLON_MODEL_UNIONTYPEMODEL_H
#include <xylon/model/UnionTypeModel.h>
#endif

#ifndef _XYLON_MODEL_LISTTYPEMODEL_H
#include <xylon/model/ListTypeModel.h>
#endif

#ifndef _XYLON_MODEL_ATTRIBUTEMODEL_H
#include <xylon/model/AttributeModel.h>
#endif

#ifndef _XYLON_MODEL_SCOPEMODEL_H
#include <xylon/model/ScopeModel.h>
#endif

#ifndef _XYLON_MODEL_NAMESPACEMODEL_H
#include <xylon/model/NamespaceModel.h>
#endif

#ifndef _XYLON_MODEL_INSTRUCTION_H
#include <xylon/model/Instruction.h>
#endif

#endif
