#include <xylon/model/ModelVisitor.h>
namespace xylon {
   namespace model {

      ModelVisitor::ModelVisitor() throws()
      {
      }

      ModelVisitor::~ModelVisitor() throws()
      {
      }

      void ModelVisitor::visitUnionType(const ::timber::Reference< UnionTypeModel>&)
      {
      }

      void ModelVisitor::visitElementType(const ::timber::Reference< ElementTypeModel>&)
      {
      }

      void ModelVisitor::visitListType(const ::timber::Reference< ListTypeModel>&)
      {
      }

      void ModelVisitor::visitType(const ::timber::Reference< TypeModel>&)
      {
      }

      void ModelVisitor::visitMethod(const ::timber::Reference< Method>&)
      {
      }

      void ModelVisitor::visitNamespace(const ::timber::Reference< NamespaceModel>&)
      {
      }

      void ModelVisitor::visitAttribute(const ::timber::Reference< AttributeModel>&)
      {
      }
   }
}
