#ifndef _XYLON_MODEL_MODELVISITOR_H
#define _XYLON_MODEL_MODELVISITOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace xylon {
   namespace model {
      class TypeModel;
      class ElementTypeModel;
      class ListTypeModel;
      class UnionTypeModel;
      class NamespaceModel;
      class AttributeModel;
      class Method;

      /**
       * A visitor for different xs nodes
       */
      class ModelVisitor
      {

            /** Default constructor */
         public:
            ModelVisitor() throws();

            /** Destructor */
         public:
            virtual ~ModelVisitor() throws() = 0;

            virtual void visitElementType(const ::timber::Reference< ElementTypeModel>& obj);
            virtual void visitListType(const ::timber::Reference< ListTypeModel>& obj);
            virtual void visitUnionType(const ::timber::Reference< UnionTypeModel>& obj);
            virtual void visitType(const ::timber::Reference< TypeModel>& obj);
            virtual void visitMethod(const ::timber::Reference< Method>& obj);
            virtual void visitNamespace(const ::timber::Reference< NamespaceModel>& obj);
            virtual void visitAttribute(const ::timber::Reference< AttributeModel>& obj);
      };
   }
}

#endif

