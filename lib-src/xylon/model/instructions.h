#ifndef _XYLON_MODEL_INSTRUCTIONS_H
#define _XYLON_MODEL_INSTRUCTIONS_H

#include <xylon/model/instructions/AbstractInstruction.h>
#include <xylon/model/instructions/BindAttribute.h>
#include <xylon/model/instructions/BindElement.h>
#include <xylon/model/instructions/Bind.h>

// conditional processing
#include <xylon/model/instructions/ForeachAttribute.h>
#include <xylon/model/instructions/Loop.h>
#include <xylon/model/instructions/OnAttribute.h>
#include <xylon/model/instructions/OnAttributeNamespace.h>
#include <xylon/model/instructions/OnAll.h>
#include <xylon/model/instructions/OnElement.h>
#include <xylon/model/instructions/OnElementNamespace.h>
#include <xylon/model/instructions/OnValidElement.h>
#include <xylon/model/instructions/OnXsiType.h>

// instruction grouping
#include <xylon/model/instructions/Sequence.h>

// moving down the XML element cursor
#include <xylon/model/instructions/MoveCursor.h>
#include <xylon/model/instructions/IterateList.h>

// errors & warnings
#include <xylon/model/instructions/Error.h>
#include <xylon/model/instructions/Message.h>
#include <xylon/model/instructions/Comment.h>

// instantiating a global type or local type
#include <xylon/model/instructions/NewInstance.h>
#include <xylon/model/instructions/ParseNewInstance.h>
#include <xylon/model/instructions/CreateAndParse.h>

// parsing a global element, type, etc.
#include <xylon/model/instructions/ParseInstance.h>

// activate the object on which parseInstance must work
#include <xylon/model/instructions/ActivateValue.h>
#include <xylon/model/instructions/ActivateElement.h>
#include <xylon/model/instructions/ActivateAttribute.h>

// a builtin instruction can be used to extend instructions for other purposes
#include <xylon/model/instructions/BuiltinInstruction.h>

// write instructions
#include <xylon/model/instructions/StartWriteObject.h>
#include <xylon/model/instructions/EndWriteObject.h>
#include <xylon/model/instructions/WriteAttribute.h>
#include <xylon/model/instructions/WriteValue.h>
#include <xylon/model/instructions/WriteBaseType.h>
#endif
