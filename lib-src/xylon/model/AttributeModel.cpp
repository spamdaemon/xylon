#include <xylon/model/AttributeModel.h>
#include <xylon/model/ModelVisitor.h>

namespace xylon {
   namespace model {
      AttributeModel::AttributeModel() throws()
      {
      }

      AttributeModel::~AttributeModel() throws()
      {
      }

      void AttributeModel::accept(ModelVisitor& v) throws()
      {
         v.visitAttribute(toRef< AttributeModel> ());
      }
   }
}
