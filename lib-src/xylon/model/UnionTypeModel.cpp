#include <xylon/model/UnionTypeModel.h>
#include <xylon/model/ModelVisitor.h>

namespace xylon {
  namespace model {

    UnionTypeModel::UnionTypeModel() throws()
    {}

    UnionTypeModel::~UnionTypeModel() throws()
    {}

    void UnionTypeModel::accept (ModelVisitor& v) throws()
    { v.visitUnionType(toRef<UnionTypeModel>()); }
  }
}
