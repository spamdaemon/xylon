#ifndef _XYLON_MODEL_LISTTYPEMODEL_H
#define _XYLON_MODEL_LISTTYPEMODEL_H

#ifndef _XYLON_MODEL_TYPEMODEL_H
#include <xylon/model/TypeModel.h>
#endif

namespace xylon {
  namespace model {
    
    /**
     * This class represents a union type. It directly corresponds to the
     * &lt;xs:union&gt; simple type.
     */
    class ListTypeModel : public virtual TypeModel {
      ListTypeModel(const ListTypeModel&);
      ListTypeModel&operator=(const ListTypeModel&);

      /** Default constructor */
    protected:
      ListTypeModel() throws();

      /** Destructor */
    public:
      ~ListTypeModel() throws();

      /** 
       * Accept the specified model visitor.
       * @param v a visitor
       */
    public:
      virtual void accept (ModelVisitor& v) throws();

      /**
       * Get the types that are in the list.
       * @return a list of types
       */
    public:
      virtual  ModelRef<TypeModel> type() const throws() = 0;
    };
  }
}

#endif
