#ifndef _XYLON_RT_LIST_H
#define _XYLON_RT_LIST_H

#ifndef _XYLON_RT_OBJECT_H
#include <xylon/rt/Object.h>
#endif

#include <memory>
#include <vector>

namespace xylon {
   namespace rt {

      /**
       * A list of xsd objects. The items in the list are stored by
       * pointer to accomodate subclasses.
       */
      template<class T> class List : public Object
      {

            /** A list iterator */
         public:
            typedef ::std::vector< ::std::unique_ptr< T> > ListType;

            /** A const iterator */
         public:
            typedef typename ListType::const_iterator const_iterator;

            /** A non-const iterator */
         public:
            typedef typename ListType::iterator iterator;

            /** A copy constructor */
         public:
            List(const List& other) :
               Object(other)
            {
               *this = other;
            }

            /** Default constructor */
         public:
            List() 
            {
            }

            /** Destructor */
         public:
            ~List() 
            {
               clear();
            }

            /** Copy operator */
         public:
            List& operator=(const List& other)
            {
               if (this != &other) {
                  clear();
                  _list.resize(other.size());
                  const_iterator j = other.begin();
                  for (iterator i = begin(); i != end(); ++i, ++j) {
                     ::std::unique_ptr< T> ptr((*j)->clone());
                     *i = ::std::move(ptr);
                  }
               }
               return *this;
            }

            /** Clone this list. */
         public:
            List* clone() const
            {
               return new List(*this);
            }

            /**
             * Get an iterator.
             * @return an iterator
             */
         public:
            const_iterator begin() const 
            {
               return _list.begin();
            }

            /**
             * Get an iterator.
             * @return an iterator
             */
         public:
            const_iterator end() const 
            {
               return _list.end();
            }

            /**
             * Get an iterator.
             * @return an iterator
             */
         public:
            iterator begin() 
            {
               return _list.begin();
            }

            /**
             * Get an iterator.
             * @return an iterator
             */
         public:
            iterator end() 
            {
               return _list.end();
            }

            /**
             * Add a new item to this list. Null is not a valid object.
             * @param obj the new item
             */
         public:
            template<class U>
            void add(::std::unique_ptr< U> obj)
            {
               _list.push_back(::std::move(obj));
            }

            /**
             * Add a new item to this list. Null is not a valid object.
             * @param obj the new item
             */
         public:
            void add(const T& obj)
            {
               ::std::unique_ptr< T> ptr(obj.clone());
               add(ptr);
            }

            /**
             * Clear this list.
             */
         public:
            void clear()
            {
               _list.clear();
            }

            /**
             * Remove the item at the specified index,
             * @param i an item index
             */
         public:
            ::std::unique_ptr< T> remove(size_t i)
            {
               ::std::unique_ptr< T> obj = ::std::move(_list.at(i));
               _list.erase(_list.begin() + i);
               return obj;
            }

            /**
             * Remove the item at the specified index,
             * @param i an item index
             */
         public:
            void erase(iterator i)
            {
               _list.erase(i);
            }

            /**
             * Get the size of this list.
             * @return the number of items in this list
             */
         public:
            inline size_t size() const 
            {
               return _list.size();
            }

            /**
             * Access the item at the specified index.
             * @param i an item index
             * @return a reference to the item at the specified index
             */
         public:
            inline const T& item(size_t i) const
            {
               return *_list.at(i);
            }

            /**
             * Access the item at the specified index.
             * @param i an item index
             * @return a reference to the item at the specified index
             */
         public:
            inline T& item(size_t i)
            {
               return *_list.at(i);
            }

            /**
             * Access the item at the specified index.
             * @param i an item index
             * @return a reference to the item at the specified index
             */
         public:
            inline const T& operator[](size_t i) const
            {
               return *_list.at(i);
            }

            /**
             * Access the item at the specified index.
             * @param i an item index
             * @return a reference to the item at the specified index
             */
         public:
            inline T& operator[](size_t i)
            {
               return *_list.at(i);
            }

            /** The list is just a vector, for now */
         private:
            ListType _list;

      };
   }
}

#endif
