#include <xylon/rt/AbstractTypeException.h>

namespace xylon {
   namespace rt {

      AbstractTypeException::AbstractTypeException(const QName& qname) :
         ParseError("Abstract type or element : " + qname.string().string()), _name(qname)
      {

      }

      AbstractTypeException::AbstractTypeException()
      {

      }

      AbstractTypeException::~AbstractTypeException()
      {

      }

   }
}
