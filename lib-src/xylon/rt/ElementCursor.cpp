#include <xylon/rt/ElementCursor.h>
#include <xylon/rt/Writer.h>
#include <timber/w3c/xml/dom/Attr.h>
#include <timber/w3c/xml/dom/Element.h>
#include <timber/w3c/xml/dom/CharacterData.h>
#include <timber/w3c/xml/dom/dom.h>
#include <timber/logging.h>

#include <vector>
#include <utility>

namespace xylon {
   namespace rt {
      namespace {
         using namespace ::timber;
         using namespace ::timber::logging;

         static const char XSI_NAMESPACE_URI[] = "http://www.w3.org/2001/XMLSchema-instance";

         static Log logger()
         {
            return Log("xylon.rt.DOMCursor");
         }

         struct DOMCursor : public ElementCursor
         {
               typedef ::std::pair< Pointer< ::timber::w3c::xml::dom::Element>, Attributes> Item;

               DOMCursor(const DOMCursor& src) 
                     : _stack(src._stack), _elementCount(src._elementCount)
               {
                  //FIXME: if we reference count the attributes, then we can shared them on the copy
               }

               DOMCursor(const Pointer< ::timber::w3c::xml::dom::Element>& e) 
                     : _elementCount(0)
               {
                  // push a dummy item onto the stack
                  _stack.push_back(Item());
                  if (e) {
                     _stack.push_back(Item(e, Attributes()));
                     populateAttributes();
                  }
               }

               ~DOMCursor() 
               {
               }

               ClonePtr clone() const 
               {
                  return ClonePtr(new DOMCursor(*this));
               }

               Pointer< ::timber::w3c::xml::dom::Element> element() const throws()
               {
                  const Item& item = _stack.back();
                  return item.first;
               }

               const Attributes& attributes() const
               {
                  return ensureValidItem().second;
               }

               bool hasAttribute(const QName& name) const
               {
                  const ElementCursor::Attributes& attrs = ensureValidItem().second;
                  ElementCursor::Attributes::const_iterator i = attrs.find(name);
                  return i != attrs.end();
               }

               String attributeValue(const QName& name) const

               {
                  const ElementCursor::Attributes& attrs = ensureValidItem().second;
                  ElementCursor::Attributes::const_iterator i = attrs.find(name);
                  if (i == attrs.end()) {
                     return String();
                  }
                  else {
                     return i->second;
                  }
               }

               size_t elementCount() const 
               {
                  return _elementCount;
               }

               QName xsiType(const QName& defaultQName) const
               {
                  ::timber::w3c::xml::dom::Element& e = ensureValid();

                  Pointer< ::timber::w3c::xml::dom::Attr> attr = e.getAttributeNodeNS(XSI_NAMESPACE_URI, "type");
                  // no such attribute, return the default name
                  if (!attr) {
                     return defaultQName;
                  }

                  String prefix, ns, local;
                  ::timber::w3c::xml::dom::splitQName(attr->nodeValue(), prefix, local);
                  ns = attr->lookupNamespaceURI(prefix);
                  if (!ns) {
                     throw ParseError("Invalid xsi:type attribute " + attr->nodeValue().string());
                  }

                  return QName(ns, local);
               }

               /**
                * Ensure that this cursor is valid and return a reference to the current element.
                * @return a reference to the current element
                * @throw ParseError if there is no current element
                */
               const Item& ensureValidItem() const
               {
                  const Item& e = _stack.back();
                  if (e.first) {
                     return e;
                  }
                  throw ParseError("No element");
               }

               /**
                * Ensure that this cursor is valid and return a reference to the current element.
                * @return a reference to the current element
                * @throw ParseError if there is no current element
                */
               Item& ensureValidItem()
               {
                  Item& e = _stack.back();
                  if (e.first) {
                     return e;
                  }
                  throw ParseError("No element");
               }

               /**
                * Ensure that this cursor is valid and return a reference to the current element.
                * @return a reference to the current element
                * @throw ParseError if there is no current element
                */
               ::timber::w3c::xml::dom::Element& ensureValid() const
               {
                  return *ensureValidItem().first;
               }

               String value() const
               {
                  ::timber::w3c::xml::dom::Element& e = ensureValid();

                  String res("");
                  Pointer< ::timber::w3c::xml::dom::Node> n = e.firstChild();
                  while (n) {
                     if (n.tryDynamicCast< ::timber::w3c::xml::dom::CharacterData>()) {
                        res += n->nodeValue();
                     }
                     else if (n->nodeType() == ::timber::w3c::xml::dom::Node::ELEMENT_NODE) {
                        // FIXME: need to address this eventually
                        // throw ParseError("Mixed element content not allowed");
                        return String();
                     }
                     else {
                        LogEntry(logger()).warn() << "Ignored node " << n->nodeValue() << doLog;
                     }
                     n = n->nextSibling();
                  }
                  return res;
               }

               void next()
               {
                  Item& e = ensureValidItem();
                  Pointer< ::timber::w3c::xml::dom::Node> n = e.first->nextSibling();

                  while (n && n->nodeType() != ::timber::w3c::xml::dom::Node::ELEMENT_NODE) {
                     n = n->nextSibling();
                  }
                  // clear the attributes
                  e.second.clear();
                  e.first = n;
                  populateAttributes();

                  // consume one more element
                  ++_elementCount;
               }

               void down()
               {
                  ::timber::w3c::xml::dom::Element& e = ensureValid();
                  Pointer< ::timber::w3c::xml::dom::Node> n = e.firstChild();

                  while (n && n->nodeType() != ::timber::w3c::xml::dom::Node::ELEMENT_NODE) {
                     n = n->nextSibling();
                  }
                  _stack.push_back(Item(n, Attributes()));
                  populateAttributes();

                  // consume one more element
                  ++_elementCount;
               }

               void up()
               {
                  // check for stack.size() == 1, because there is a dummy element on the stack
                  if (_stack.size() == 1) {
                     throw ParseError("No parent element");
                  }
                  _stack.pop_back();
               }

               bool isValid() const 
               {
                  return _stack.back().first;
               }

               QName qname() const
               {
                  const ::timber::w3c::xml::dom::Element& e = ensureValid();
                  return QName(e.namespaceURI(), e.localName());
               }

               String namespaceURI() const
               {
                  return ensureValid().namespaceURI();
               }

               /**
                * Populate the attributes of the current element (if there is one).
                * */
            private:
               void populateAttributes()
               {
                  Item& item = _stack.back();
                  // if there isn't a proper element, then return immediately
                  if (!item.first) {
                     return;
                  }
                  Reference< ::timber::w3c::xml::dom::NodeList> attrs = item.first->getAttributeList();
                  for (size_t i = 0, sz = attrs->length(); i < sz; ++i) {
                     Reference< ::timber::w3c::xml::dom::Attr> attr = attrs->item(i);
                     if (attr->localName() == "xmlns") {
                        continue;
                     }
                     if (attr->prefix() == "xmlns") {
                        continue;
                     }
                     if (attr->namespaceURI() == XSI_NAMESPACE_URI) {
                        continue;
                     }
                     QName name(attr->namespaceURI(), attr->localName());
                     item.second[name] = attr->nodeValue();
                  }
               }

               /** A stack of elements and their attributes */
            private:
               ::std::vector< Item> _stack;

               /** The number of elements seen so far. */
            private:
               size_t _elementCount;
         };

      }

      ElementCursor::ElementCursor() 
      {

      }

      ElementCursor::~ElementCursor() 
      {
      }

      ElementCursor::ClonePtr ElementCursor::create(
            const ::timber::Pointer< ::timber::w3c::xml::dom::Element>& element) 
      {
         return ClonePtr(new DOMCursor(element));
      }

      ElementCursor::ClonePtr ElementCursor::createInvalidCursor() 
      {
         // just use a DOM cursor without an element
         return create(::timber::Pointer< ::timber::w3c::xml::dom::Element>());
      }

      void ElementCursor::writeElement(Writer& w) const
      {
         // need to make a backup first
         ElementCursor::ClonePtr copy = clone();
         const QName xsi_type = QName::xsiQName("type");

         if (!copy->isValid()) {
            logger().warn("Not a valid cursor");
            return;
         }
         size_t level = 0;
         while (true) {
            if (copy->isValid()) {
               w.beginElement(copy->qname());
               const Attributes& attrs = copy->attributes();
               for (Attributes::const_iterator i = attrs.begin(); i != attrs.end(); ++i) {
                  w.beginAttribute(i->first);
                  w.writeText(i->second);
                  w.endAttribute();
               }
               QName xsi = xsiType();
               if (xsi) {
                  w.writeQNameAttribute(xsi_type, xsi);
               }

               // go down to the child
               copy->down();
               ++level;
            }
            else {
               w.endElement();

               --level;
               if (level == 0) {
                  break;
               }
               copy->up();
               if (!copy->isValid()) {
                  break;
               }
               w.writeText(copy->value());
               copy->next();
            }
         }
      }
   }

}
