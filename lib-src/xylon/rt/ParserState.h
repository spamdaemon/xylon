#ifndef _XYLON_RT_PARSERSTATE_H_
#define _XYLON_RT_PARSERSTATE_H_

#ifndef _XYLON_RT_OBJECT_H
#include <xylon/rt/Object.h>
#endif

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

#ifndef _XYLON_RT_ELEMENTCURSOR_H
#include <xylon/rt/ElementCursor.h>
#endif

#include <memory>
#include <vector>
#include <iostream>

namespace xylon {
   namespace rt {
      class Schema;
      class QName;
      class Instruction;

      /** The purpose of the parser state is to hold all state information needed
       * when parsing an element or an attribute. The state maintains a pointer to
       * schema, an element cursor, and maintains a stack of types created during traversal
       * of document.<p>
       * FIXME: remove print statements
       */
      class ParserState
      {
            /** The copy flags are used to indicate what to copy or append when copying ParserState */
         public:
            enum CopyFlags
            {
               /** A bit to indicate that the stack should be copied */
               COPY_STACK = 1,
               /** A bit to indicate that the stack should be appended */
               APPEND_STACK = 2,
               /** A but to indicate that the attribute should be copied */
               COPY_ATTRIBUTES = 4
            };

            /** The copy operator is not supported */
         private:
            ParserState& operator=(const ParserState&);

            /** An object pointer */
         public:
            typedef ::std::unique_ptr< Object> ObjectPtr;

            /** The attributes that have been parsed into objects */
         private:
            typedef ::std::map< QName, Object*> ParsedAttributes;

            /** The type of object current active */
         public:
            enum ActiveObject
            {
               /** Nothing is currently active */
               NONE,

               /** The current element is active */
               ELEMENT,

               /** An attribute is active */
               ATTRIBUTE,

               /** A value unrelated to the current element or attribute has been activated. This is primarily used
                * when parsing the elements of a list, which can each be activated in turn.
                */
               STRING,

            };

            /**
             * Create a new parser state.
             * @param schema the schema being parsed
             * @param cursor the initial element for this parser state
             * @pre cursor.get()!=0
             */
         public:
            ParserState(const Schema& schema, ::std::unique_ptr< ElementCursor> cursor) ;

            /**
             * Copy constructor. This constructor is expensive to execute and is equivalent to @code ParserState(src,true) @endcode.
             * @param src the original to be copied
             *
             */
         public:
            ParserState(const ParserState& src) ;

            /**
             * Create a (partial) copy of a parser state. If the stack need not be copied,
             * then the stack of the ParserState is empty. <p>
             * Note that copying a stack is fairly expensive, as all objects on the stack must be cloned.
             * @param src the original to be copied
             * @param copyFlags the items to copy or append
             *
             */
         public:
            ParserState(const ParserState& src, size_t copyFlags) ;

            /** Destructor */
         public:
            ~ParserState();

            /**
             * Clear the parser's state. This is a private method that cleans up the stack is primarily used
             * by the destructor.
             */
         private:
            void cleanup() ;

            /**
             * Clear the object stack.
             */
         private:
            void clearStack() ;

            /**
             * Move the specified parser state into this state. If appendStack is specified,
             * then the src stack is appended, otherwise, the src stack overrides this object's current stack.<p>
             * Note that this operation may require deleting the objects on this stack which can be expensive.
             * @param src the source stack providing the data for the update
             * @param copyFlags the items to copy or append
             * @throw ParseError if the schemas of the states are different, or the src is this object
             */
         public:
            void move(ParserState& src, size_t copyFlags);

            /**
             * Push the specified object onto the stack.
             * @param obj a type object
             * @throw ParseError if obj.get() is null
             */
            template<class T>
            inline void pushObject(::std::unique_ptr< T> obj)
            {
               ::std::cerr << "Push type " << typeid(*obj).name() << ::std::endl;
               if (obj.get() == 0) {
                  throw ParseError("Attempting to push a null object onto the stack");
               }
               _stack.push_back(obj.get());
               obj.release();
            }

            /**
             * Pop the type that is currently on the stack.
             * @return the object on the stack, cast to the specified type
             * @throw ParserError if there is no matching type on the stack.
             */
            template<class T> ::std::unique_ptr< T> popObject()
            {
               ::std::cerr << "Pop object " << typeid(T).name() << ::std::endl;
               if (_stack.empty()) {
                  throw ParseError("Stack is empty");
               }
               T* ptr = dynamic_cast< T*>(_stack.back());
               if (ptr == 0) {
                  ::std::cerr << "Cast failed to << " << typeid(T).name() << "; type was "
                        << typeid(*_stack.back()).name() << ::std::endl;
                  throw ParseError("Unexpected type on stack");
               }
               _stack.pop_back();
               ::std::unique_ptr < T > res(ptr);
               return res;
            }

            /**
             * Get a reference to the object on the type stack.
             * @return the object on the stack, cast to the given type
             * @throw ParserError if there is no matching type on the stack.
             */
            template<class T>
            inline T& topObject()
            {
               if (_stack.empty()) {
                  throw ParseError("Stack is empty");
               }
               T* ptr = dynamic_cast< T*>(_stack.back());
               if (ptr == 0) {
                  throw ParseError("Unexpected type on the stack");
               }
               return *ptr;
            }

            /**
             * Determine if the stack is empty.
             * @return true if the object stack is empty
             */
         public:
            inline bool isEmpty() const 
            {
               return _stack.empty();
            }

            /**
             * Get the schema.
             * @return the schema
             */
         public:
            inline const Schema& schema() const 
            {
               return _schema;
            }

            /**
             * Get the current element cursor.
             * @return the element cursor
             */
         public:
            inline ElementCursor& cursor() const 
            {
               return *_cursor;
            }

            /**
             * Parse an attribute in the schema and push its type onto the stack.
             * @param name the name of an attribute.
             * @throw ParseError if the parsing error occurred.
             */
         public:
            void pushAttribute(const QName& name);

            /**
             * Parse an attribute group in the schema and push its type onto the stack.
             * @param name the name of an attribute group.
             * @throw ParseError if the parsing error occurred.
             */
         public:
            void pushAttributeGroup(const QName& name);

            /**
             * Parse an element in the schema and push its type onto the stack.
             * @param name the name of an element.
             * @throw ParseError if the parsing error occurred.
             */
         public:
            void pushElement(const QName& name);

            /**
             * Parse the current element and push its type onto the stack.
             * @throw ParseError if the parsing error occurred.
             */
         public:
            void pushElement();

            /**
             * Parse a group in the schema and push its type onto the stack.
             * @param name the name of a group.
             * @throw ParseError if the parsing error occurred.
             */
         public:
            void pushGroup(const QName& name);

            /**
             * Parse a complex or simple type in the schema and push its type onto the stack.
             * @param name the name of a type.
             * @throw ParseError if the parsing error occurred.
             */
         public:
            void pushType(const QName& name);

            /**
             * Parse simple or complex type. If the type cannot be parsed, then return
             * false instead of throwing an exception. This method will still throw an
             * exception if the parsing the type has somehow corrupted the parser's state and the state cannot be recovered.
             *
             * @param name the name of a type.
             * @return true if the type was parsed, false otherwise
             * @throw ParseError if a non-recoverable parsing error occurred.
             */
         public:
            bool tryPushType(const QName& name);

            /**
             * Determine if the current element has an xsi type attribute and push an instance
             * of that type onto the stack. If the current element has no xsi type, then specified
             * type is instantiated. If an xsi:type attribute is found, then a test is made to ensure
             * that the type is subtype of specified type.
             * @param name the name of a type
             * @throw ParseError if the parsing error occurred.
             */
         public:
            void pushXsiType(const QName& name);

            /**
             * Determine if the current element to which the cursor points is an element known
             * to the schema.
             * @return true if the current element is known
             * @throw ParseError if there is no current element
             */
         public:
            bool isKnownElement() const;

            /**
             * Activate the current element. If there is no valid element and
             * expectValidElement is false, then this method does not throw but
             * instead sets the active type to NONE_ACTIVE.
             * @param expectValidElement if true a valid element is expected
             * @throw ParseError if the parsing error occurred.
             */
         public:
            void activateElement(bool expectValidElement = false);

            /**
             * Activate the specified attribute of the currently element.
             * @param name the name of the attribute to activate
             * @return true if the attribute was activated, if the attribute does not exist
             * @throw ParseError if there is no valid element
             */
         public:
            bool activateAttribute(const QName& name);

            /**
             * Active a special value which is not necessarily either an element or an attribute value.
             * @param value the value to activate
             * @throw ParseError if value is a null string
             */
         public:
            void activateStringValue(const String& value);

            /**
             * Activate the the currently active object as a simple value. This affects how the pushType() family
             * of methods works.
             * @throw ParseError if there is nothing to activate
             */
         public:
            void activateValue();

            /**
             * Get  the currently active object.
             * @return the active object type
             */
         public:
            inline ActiveObject activeObject() const 
            {
               return _activeType;
            }

            /**
             * Get the value that is currently active.
             * @return the currently active value.
             * @throw ParseError if no value is active
             */
         public:
            String activeValue() const;

            /**
             * Get the active attribute.
             * @return the name of the active attribute.
             * @throw ParseError if no attribute is active
             */
         public:
            const QName& activeAttribute() const;

            /**
             * Given a sorted array of QName object, determine which of the names matches
             * the current element. This test takes substitution groups into account.
             * <p>
             * If the current element is unknown to the schema, then this method returns false, even
             * if it is in the list of qnames.
             * @param names a pointer to an array of qnames
             * @param n the number of qnames
             * @return the index of the qname that matches the current element or n if no match is found
             * @throw ParseError if there is no current element
             */
         public:
            size_t findElement(const QName* names, const size_t n) const;

            /**
             * Given a sorted array of QName object, determine which of the names matches
             * the current element. This method does not take substitution groups into account.
             * <p>
             * If the current element is unknown to the schema, then this method returns false, even
             * if it is in the list of qnames.
             * @param names a pointer to an array of qnames
             * @param n the number of qnames
             * @return the index of the qname that matches the current element or n if no match is found
             * @throw ParseError if there is no current element
             */
         public:
            size_t findElementIgnoreSubstitutions(const QName* names, const size_t n) const;

            /**
             * Determine if the current element corresponds to the specified qname. If the
             * given qname is the head of some substitution group, and the current element
             * is part of that substitution group, then true is returned.
             * @param qname the qname to check
             * @return true if the current element is in the substitution group of qname or is equal to qname
             * @throw ParseError if there is no element or the current element is not known to the schema
             */
         public:
            bool testElement(const QName& qname) const;

            /**
             * Parse a list into its constituents, activate each one as string
             * value and the invoke a specified instruction.
             * @param instr the instruction to execute
             * @return the number of elements in the list
             * @throw ParseError if an error occurred
             */
         public:
            size_t activateListValues(const Instruction& instr);

            /**
             * Execute an instruction a specified minimum and maximum number of times. This method
             * determines that an instruction was not executed, when the elementCount() before and after
             * the instruction was executed is the same. In other words, at least one element must be visited
             * in order in order for the loop to continue.<p>
             * @param instruction the instruction to be executed
             * @param min the minimum number of times to execute the loop
             * @param max the maximum number of times to execute the loop
             * @return true if the loop was executed at least min times, false otherwise
             */
         public:
            bool loop(const Instruction& instruction, size_t min, size_t max);

            /**
             * Clear the set of attributes still remaining.
             */
         public:
            void clearAttributes() ;

            /** The schema */
         private:
            const Schema& _schema;

            /** The current element */
         private:
            ::std::unique_ptr< ElementCursor> _cursor;

            /** The type instances on the stack;  */
         private:
            ::std::vector< Object*> _stack;

            /** The parsed attributes for the current element. */
         private:
            ParsedAttributes _attributes;

            /** The name of the currently active attribute (if set) */
         private:
            QName _activeAttribute;

            /** The value of the most recently activated attribute */
         private:
            String _activatedObjectValue;

            /** The active value or null if none is active. */
         private:
            String _activeValue;

            /** The currently activate object */
         private:
            ActiveObject _activeType;

      };

   }

}

#endif
