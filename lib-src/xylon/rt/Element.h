#ifndef _XYLON_RT_ELEMENT_H
#define _XYLON_RT_ELEMENT_H

#ifndef _XYLON_RT_TOPLEVELNODE_H
#include <xylon/rt/TopLevelNode.h>
#endif

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

#ifndef _XYLON_RT_ABSTRACTTYPEEXCEPTION_H
#include <xylon/rt/AbstractTypeException.h>
#endif

#ifndef _XYLON_RT_ELEMENTOBJECT_H
#include <xylon/rt/ElementObject.h>
#endif

#include <memory>

namespace xylon {
   namespace rt {
      class ParserState;

      /**
       * This class represents an XML element and any information needed to parse the element. The class is abstract
       * requiring subclassing.
       */
      class Element : public TopLevelNode
      {
            /** An object pointer; in the case of elements, it's not just an object */
         public:
            typedef ::std::unique_ptr< ElementObject> ObjectPtr;

            /** Default constructor */
         protected:
            Element() ;

            /** Destructor */
         public:
            virtual ~Element()  = 0;

            /**
             * Create an instance of the C++ class to which this node type is mapped.
             * @return an instance of the mapped type
             * @throw AbstractTypeException if the type is abstract
             */
         public:
            virtual ObjectPtr newInstance() const  = 0;

            /**
             * Determine if this is an abstract element. If this element is abstract, then
             * newInstance() <em>always</em> throws an AbstractTypeException.
             */
         public:
            virtual bool isAbstract() const  = 0;

            /**
             * Clone this node.
             * @return a copy of this node
             */
         public:
            virtual ::std::unique_ptr< Element> clone() const  = 0;

            /**
             * Get the substitution group of this element. If this element is not in any substitution group then it must
             * return the name of this element.
             * @return the substitution group of this element.
             */
         public:
            virtual QName substitutionGroup() const  = 0;

            /**
             * Parse an object represented by this class and which is located on top of the parser stack. All information needed
             * to parse this object is provided in the form of a ParserState.<p>
             * Upon a successfully parsing this type, the created object will be on top of the ParserState's stack.
             * @param ps a parser state
             * @throw ParseError if the element cannot be parsed into an object
             */
         public:
            virtual void parse(ParserState& ps) const  = 0;
      };

   }

}

#endif
