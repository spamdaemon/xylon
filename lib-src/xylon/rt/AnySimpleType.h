#ifndef _XYLON_RT_ANYSIMPLETYPE_H
#define _XYLON_RT_ANYSIMPLETYPE_H

#ifndef _XYLON_RT_TYPEOBJECT_H
#include <xylon/rt/TypeObject.h>
#endif

#include <string>
#include <iosfwd>

namespace xylon {
   namespace rt {
      /**
       * This class is the base class for all types defined by an XSD schema.
       */
      class AnySimpleType : public TypeObject
      {
            /** The native type */
         public:
            typedef ::std::string NativeType;

            /** The default constructor */
         public:
            AnySimpleType() ;

            /**
             * Copy constructor.
             * @param src the source anysimpletype
             */
         public:
            AnySimpleType(const AnySimpleType& src) ;

            /**
             * Copy constructor.
             * @param src the source anysimpletype
             */
         public:
            AnySimpleType(const NativeType& src) ;

            /** The destructor */
         public:
            ~AnySimpleType() ;

            /** Copy operator */
         public:
            AnySimpleType& operator=(const AnySimpleType&) ;

            /** Copy operator */
         public:
            AnySimpleType& operator=(const NativeType&) ;

            /** Clone this object */
         public:
            AnySimpleType* clone() const;

            /**
             * Get  the value as a native type.
             * @return the value as a native type
             */
         public:
            inline const NativeType& value() const
            {
               return _value;
            }

            /**
             * Assign this object from a string.
             * @param str a string value
             * */
         public:
            virtual void fromString(const ::std::string& str);

            /**
             * Get the string value of this object.
             * @return the string value
             */
         public:
            virtual ::std::string toString() const;

            /**
             * The input operator
             * @param stream an input stream
             */
         public:
            virtual void write(::std::ostream& stream) const;

            /**
             * The input operator
             * @param w a writer
             */
         public:
            void write(Writer& w) const;

            /**
             * The input operator
             * @param qname a qname
             * @param w a writer
             */
         public:
            void writeType(const QName& qname, Writer& w) const;

            /**
             * The input operator
             * @param stream an input stream
             */
         public:
            virtual void read(::std::istream& stream);

            /**
             * Compare two instances.
             * @param obj an object
             * @return true if this object is equal to obj
             */
         public:
            inline bool operator==(const AnySimpleType& obj) const
            {
               return _value == obj._value;
            }

            /**
             * Compare two instances.
             * @param obj an object
             * @return true if this object is less than obj
             */
         public:
            inline bool operator<(const AnySimpleType& obj) const
            {
               return _value < obj._value;
            }

            /**
             * Swap two instances of this type.
             * @param obj1 another object
             * @param obj2 another object
             */
         public:
            static inline void swap(AnySimpleType& obj1, AnySimpleType& obj2)
            {
               ::std::swap(obj1._value, obj2._value);
            }

            /**
             * Move the specified value into this object.
             * The moved value may be destroyed up completion.
             * @param val a value
             */
         public:
            inline void move(NativeType& val)
            {
               ::std::swap(_value, val);
            }

            /** The value is just a string for now */
         private:
            NativeType _value;
      };

      inline ::std::istream& operator>>(::std::istream& in, AnySimpleType& obj)
      {
         obj.read(in);
         return in;
      }

      inline ::std::ostream& operator<<(::std::ostream& out, const AnySimpleType& obj)
      {
         obj.write(out);
         return out;
      }

   }
}

#endif
