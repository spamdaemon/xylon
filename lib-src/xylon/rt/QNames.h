#ifndef _XYLON_RT_QNAMES_H_
#define _XYLON_RT_QNAMES_H_

#include <cstddef>
#include <map>

namespace xylon {
   namespace rt {
      class Schema;
      class QName;
      class Instruction;

      /**
       * QNames are used internally by parsers to represent the elements that may appear in
       * at some location in a document. Each element has an associated instruction object.
       */
      class QNames
      {
            /** Default constructor */
         public:
            QNames() ;

            /**
             *  Create a list of names. The schema is used to locate element names known to
             *  the schema that may be substituted for any of the provided names. Any elements
             *  that are substitutable for any of the provided names are associated with the same
             *  instruction.
             *  @param names a list of qnames
             *  @param schema a schema
             */
         public:
            QNames(const ::std::map< QName, Instruction>& names, const Schema& schema) ;

            /**
             *  Copy constructor.
             *  @param src the source names object
             */
         public:
            QNames(const QNames& src) ;

            /** Destructor */
         public:
            virtual ~QNames() ;

            /**
             * Copy operator.
             * @param src the source
             * @return *this;
             */
         public:
            QNames& operator=(const QNames& names) ;

            /**
             * Get the instruction for the specified element name.
             * @param name an element name
             * @return the instruction associated with the name or NoInstruction if there is no such instruction
             */
         public:
            Instruction findInstruction(const QName& name) const ;

            /** The names that may appear in the document. */
         private:
            ::std::map< QName, Instruction> _names;
      };

   }

}

#endif
