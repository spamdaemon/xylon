#include <xylon/rt/ElementObject.h>

namespace xylon {
   namespace rt {
      ElementObject::ElementObject() 
      {
      }

      ElementObject::ElementObject(const ElementObject& a)  :
         Object(a)
      {
      }

      ElementObject& ElementObject::operator=(const ElementObject& a) 
      {
         Object::operator=(a);
         return *this;
      }

      ElementObject::~ElementObject() 
      {
      }

   }
}
