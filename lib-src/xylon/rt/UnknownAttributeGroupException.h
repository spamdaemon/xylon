#ifndef _XYLON_RT_UNKNOWNATTRIBUTEGROUPEXCEPTION_H
#define _XYLON_RT_UNKNOWNATTRIBUTEGROUPEXCEPTION_H

#ifndef _XYLON_RT_UNKNOWNQNAMEEXCEPTION_H
#include <xylon/rt/UnknownQNameException.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This exception is thrown when an unknown element is encountered by a schema.
       */
      class UnknownAttributeGroupException : public UnknownQNameException
      {

            /**
             * Constructor.
             * @param qname name
             */
         public:
            UnknownAttributeGroupException(const QName& qname) ;

            /**
             * Constructor
             */
         public:
            UnknownAttributeGroupException() ;

            /**
             * Destructor
             */
         public:
            ~UnknownAttributeGroupException() ;
      };

   }
}
#endif
