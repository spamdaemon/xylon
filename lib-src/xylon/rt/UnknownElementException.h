#ifndef _XYLON_RT_UNKNOWNELEMENTEXCEPTION_H
#define _XYLON_RT_UNKNOWNELEMENTEXCEPTION_H

#ifndef _XYLON_RT_UNKNOWNQNAMEEXCEPTION_H
#include <xylon/rt/UnknownQNameException.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This exception is thrown when an unknown element is encountered by a schema.
       */
      class UnknownElementException : public UnknownQNameException
      {

            /**
             * Constructor.
             * @param qname name
             */
         public:
            UnknownElementException(const QName& qname) ;

            /**
             * Contructor
             */
         public:
            UnknownElementException() ;

            /**
             * Destructor
             */
         public:
            ~UnknownElementException() ;
      };

   }
}
#endif
