#ifndef _XYLON_RT_PARSEERROR_H
#define _XYLON_RT_PARSEERROR_H

#include <stdexcept>

namespace xylon {
   namespace rt {

      /**
       * This exception is thrown when an unknown element is encountered by a schema.
       */
      class ParseError : public ::std::runtime_error
      {

            /**
             * Constructor.
             * @param msg a message
             */
         public:
            ParseError(const ::std::string& msg);

            /**
             * Default constructor.
             */
         public:
            ParseError();

            /**
             * Destructor
             */
         public:
            ~ParseError();

      };

   }
}
#endif
