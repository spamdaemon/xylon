#ifndef _XYLON_RT_GROUP_H
#define _XYLON_RT_GROUP_H

#ifndef _XYLON_RT_TOPLEVELNODE_H
#include <xylon/rt/TopLevelNode.h>
#endif

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

#ifndef _XYLON_RT_OBJECT_H
#include <xylon/rt/Object.h>
#endif

#include <memory>

namespace xylon {
   namespace rt {
      class ParserState;

      /**
       * This class represents an XML element and any information needed to parse the element. The class is abstract
       * requiring subclassing.
       */
      class Group : public TopLevelNode
      {
            /** An object pointer */
         public:
            typedef ::std::unique_ptr< Object> ObjectPtr;

           /** Default constructor */
         protected:
            Group() ;

            /** Destructor */
         public:
            virtual ~Group()  = 0;

            /**
             * Create an instance of the C++ class to which this node type is mapped.
             * @return an instance of the mapped type
             * @throw AbstractTypeException if the type is abstract
             */
         public:
            virtual ObjectPtr newInstance() const  = 0;

            /**
             * Clone this node.
             * @return a copy of this node
             */
         public:
            virtual ::std::unique_ptr<Group> clone() const  = 0;

            /**
             * Create and parse an object represented by this class. All information needed
             * to parse this object is provided in the form of a ParserState.<p>
             * Upon a successfully parsing this type, the created object will be on top of the ParserState's stack.
             * @param ps a parser state
             * @throws ParseError if the group could not be parsed correctly
             */
         public:
            virtual void parse(ParserState& ps) const  = 0;
      };

   }

}

#endif
