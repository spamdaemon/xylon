#ifndef _XYLON_RT_GENERICINSTRUCTION_H
#define _XYLON_RT_GENERICINSTRUCTION_H

#ifndef _XYLON_RT_INSTRUCTION_H
#include <xylon/rt/Instruction.h>
#endif

namespace xylon {
   namespace rt {
      class ParserState;

      /**
       * A generic instruction.
       */
      template<class T>
      class GenericInstruction : public Instruction
      {
            /** A function pointer */
         public:
            typedef void (T::*InstructionPtr)(ParserState&) const;

            /**
             * A constructor.
             * @param t an object on which to invoke the specified instruction
             * @param ptr an instruction pointer
             */
         public:
            inline GenericInstruction(const T& t, InstructionPtr ptr)  :
               _object(t), _pointer(ptr)
            {
            }

            /** Destructor */
         public:
            ~GenericInstruction() 
            {
            }

            /**
             * A generic instruction is always valid.
             * @return true
             */
            bool isValid() const 
            {
               return true;
            }

            /**
             * Execute this instruction.
             * @param state the parse state
             */
         public:
            void execute(ParserState& state) const
            {
               (_object.*_pointer)(state);
            }

            /** The object on which to invoke the instruction */
         private:
            const T& _object;

            /** The instruction to be invoked */
         private:
            InstructionPtr _pointer;
      };

   }

}

#endif
