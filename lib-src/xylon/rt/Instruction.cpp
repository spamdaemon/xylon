#include <xylon/rt/Instruction.h>

namespace xylon {
   namespace rt {

      Instruction::Instruction() 
      {
      }

      Instruction::~Instruction() 
      {
      }

      bool Instruction::isValid() const 
      {
         return false;
      }

      void Instruction::execute(ParserState&) const
      {
         throw ParseError("Cannot execute instruction");
      }

   }

}
