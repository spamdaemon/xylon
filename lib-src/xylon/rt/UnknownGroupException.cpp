#include <xylon/rt/UnknownGroupException.h>

namespace xylon {
   namespace rt {

      UnknownGroupException::UnknownGroupException(const QName& qname)  :
         UnknownQNameException(qname)
      {

      }

      UnknownGroupException::UnknownGroupException() 
      {

      }

      UnknownGroupException::~UnknownGroupException() 
      {

      }

   }
}
