#include <xylon/rt/UnknownQNameException.h>

namespace xylon {
   namespace rt {

      UnknownQNameException::UnknownQNameException(const QName& qname)  :
         ParseError("Unknown element : " + qname.string().string()), _name(qname)
      {

      }

      UnknownQNameException::UnknownQNameException() 
      {

      }

      UnknownQNameException::~UnknownQNameException() 
      {

      }

   }
}
