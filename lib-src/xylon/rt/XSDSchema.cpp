#include <xylon/rt/XSDSchema.h>
#include <xylon/rt/Type.h>
#include <xylon/rt/ParserState.h>
#include <xsd/types.h>
#include <iostream>

namespace xylon {
   namespace rt {
      namespace {

         /**
          * The XSD type is used to represent the builtin simple XSD types.
          */
         template<class XSD>
         class XSDType : public Type
         {
               XSDType&operator=(const XSDType&);

               /** The impl type */
            public:
               typedef XSD Impl;

               /** Create a new XSD type.
                * @param xname the name of the type.
                */
            public:
               XSDType(const String& xname)  :
                  _name(QName::xsQName(xname)), _base(QName::xsQName("anySimpleType"))
               {
               }

               /** Create a new XSD type.
                * @param src the source type
                */
            public:
               XSDType(const XSDType& src)  :
                  _name(src._name), _base(src._base)
               {
               }

               ~XSDType() 
               {
               }

               ::std::unique_ptr< Type> clone() const 
               {
                  return ::std::unique_ptr< Type>(new XSDType(*this));
               }

               ::xylon::rt::QName qname() const 
               {
                  return _name;
               }

               /**
                * Get the base type.
                * @return xs:anySimpleType
                */
               ::xylon::rt::QName baseType() const 
               {
                  return _base;
               }

               ObjectPtr newInstance() const 
               {
                  return ObjectPtr(new Impl());
               }

               bool isAbstract() const 
               {
                  return false;
               }

               void marshal(const Object& obj, Writer& writer) const
               {
                  const Impl& impl = dynamic_cast< const Impl&> (obj);
                  impl.write(writer);
               }

               /**
                * Parsing a primitive type accomplished by activating the current value.
                * @param ps
                */
               void parse(::xylon::rt::ParserState& ps) const
               {
                  ps.activateValue();
                  ::std::string value = ps.activeValue().string();

                  ::std::cerr << "Setting simple type : " << typeid(Impl).name() << " to " << value << ::std::endl;
                  Impl & obj = ps.topObject< Impl> ();
                  try {
                     obj.fromString(value);
                  }
                  catch (const ::std::exception& e) {
                     throw ParseError("Could not parse XSD object " + _name.string().string() + " : " + value);
                  }
               }

               /** The qname and the base type */
            private:
               const QName _name, _base;
         };

      }
      XSDSchema::XSDSchema() 
      {
         addType< ::xsd::String> ("string");
         addType< ::xsd::Boolean> ("boolean");
         addType< ::xsd::Float> ("float");
         addType< ::xsd::Double> ("double");
         addType< ::xsd::Decimal> ("decimal");
         addType< ::xsd::Duration> ("duration");
         addType< ::xsd::DateTime> ("dateTime");
         addType< ::xsd::Time> ("time");
         addType< ::xsd::Date> ("date");
         addType< ::xsd::GYearMonth> ("gYearMonth");
         addType< ::xsd::GYear> ("gYear");
         addType< ::xsd::GMonthDay> ("gMonthDay");
         addType< ::xsd::GDay> ("gDay");
         addType< ::xsd::GMonth> ("gMonth");
         addType< ::xsd::HexBinary> ("hexBinary");
         addType< ::xsd::Base64Binary> ("base64Binary");
         addType< ::xsd::AnyURI> ("anyURI");
         addType< ::xsd::QName> ("QName");
         addType< ::xsd::NOTATION> ("NOTATION");
         addType< ::xsd::NormalizedString> ("normalizedString");
         addType< ::xsd::Token> ("token");
         addType< ::xsd::Language> ("language");
         addType< ::xsd::IDREFS> ("IDREFS");
         addType< ::xsd::ENTITIES> ("ENTITIES");
         addType< ::xsd::NMTOKEN> ("NMTOKEN");
         addType< ::xsd::NMTOKENS> ("NMTOKENS");
         addType< ::xsd::Name> ("Name");
         addType< ::xsd::NCName> ("NCName");
         addType< ::xsd::ID> ("ID");
         addType< ::xsd::IDREF> ("IDREF");
         addType< ::xsd::ENTITY> ("ENTITY");
         addType< ::xsd::Integer> ("integer");
         addType< ::xsd::NonPositiveInteger> ("nonPositiveInteger");
         addType< ::xsd::NegativeInteger> ("negativeInteger");
         addType< ::xsd::Long> ("long");
         addType< ::xsd::Int> ("int");
         addType< ::xsd::Short> ("short");
         addType< ::xsd::Byte> ("byte");
         addType< ::xsd::NonNegativeInteger> ("nonNegativeInteger");
         addType< ::xsd::UnsignedLong> ("unsignedLong");
         addType< ::xsd::UnsignedInt> ("unsignedInt");
         addType< ::xsd::UnsignedShort> ("unsignedShort");
         addType< ::xsd::UnsignedByte> ("unsignedByte");
         addType< ::xsd::PositiveInteger> ("positiveInteger");
      }

      XSDSchema::~XSDSchema() 
      {
      }

      template<class XSD>
      void XSDSchema::addType(const String& xname) 
      {
         ::std::unique_ptr< Type> tmp(new XSDType< XSD> (xname));
         AbstractBasicSchema::addType(::std::move(tmp));
      }

   }

}
