#include <xylon/rt/AbstractBasicSchema.h>
#include <xylon/rt/ParserState.h>
#include <xylon/rt/Attribute.h>
#include <xylon/rt/AttributeGroup.h>
#include <xylon/rt/Element.h>
#include <xylon/rt/Group.h>
#include <xylon/rt/Type.h>

#include <algorithm>

using namespace ::std;

namespace xylon {
   namespace rt {
      namespace {
         template<class T> static void clear(map< QName, T*>& xmap) 
         {
            for (typename map< QName, T*>::const_iterator i = xmap.begin(); i != xmap.end(); ++i) {
               delete i->second;
            }
            xmap.clear();
         }

         template<class T> static bool add(map< QName, T*>& xmap, unique_ptr< T> obj) 
         {
            bool ok = xmap.insert(make_pair(obj->qname(), obj.get())).second;
            if (ok) {
               obj.release();
            }
            return ok;

         }

         template<class T> static set< QName> keys(const map< QName, T*>& xmap) 
         {
            set< QName> res;
            for (typename map< QName, T*>::const_iterator i = xmap.begin(); i != xmap.end(); ++i) {
               res.insert(i->first);
            }
            return res;
         }

         // returns true if there is a conflict
         static bool checkForConflict(const set< QName>& s1, const set< QName>& s2) 
         {
            for (set< QName>::const_iterator i = s1.begin(); i != s1.end(); ++i) {
               if (s2.count(*i) > 0) {
                  return true;
               }
            }
            return false;
         }

         // returns true if there is a conflict

         template<class T, class U> static void merge(const set< QName>& src, const Schema& srcSchema, U find, map<
               QName, T*>& dest) 
         {
            for (typename set< QName>::const_iterator i = src.begin(); i != src.end(); ++i) {
               assert(dest.count(*i) == 0);
               unique_ptr< T> n = (srcSchema.*find)(*i).clone();
               assert(n.get() != 0);
               dest.insert(make_pair(*i, n.get()));
               n.release();
            }
         }
      }

      AbstractBasicSchema::AbstractBasicSchema() 
      {
      }

      AbstractBasicSchema::AbstractBasicSchema(const AbstractBasicSchema& src) 
      {
         mergeSchema(src, false);
      }

      AbstractBasicSchema::AbstractBasicSchema(const Schema& src) 
      {
         mergeSchema(src, false);
      }

      AbstractBasicSchema::~AbstractBasicSchema() 
      {
         clear(_attributes);
         clear(_attributeGroups);
         clear(_elements);
         clear(_groups);
         clear(_types);
      }

      AbstractBasicSchema& AbstractBasicSchema::operator=(const AbstractBasicSchema& src) 
      {
         if (&src == this) {
            return *this;
         }
         AbstractBasicSchema tmp(src);
         _attributes.swap(tmp._attributes);
         _attributeGroups.swap(tmp._attributeGroups);
         _elements.swap(tmp._elements);
         _groups.swap(tmp._groups);
         _types.swap(tmp._types);

         return *this;
      }

      AbstractBasicSchema& AbstractBasicSchema::operator=(const Schema& src) 
      {
         if (&src == this) {
            return *this;
         }
         AbstractBasicSchema tmp(src);
         _attributes.swap(tmp._attributes);
         _attributeGroups.swap(tmp._attributeGroups);
         _elements.swap(tmp._elements);
         _groups.swap(tmp._groups);
         _types.swap(tmp._types);

         return *this;
      }

      bool AbstractBasicSchema::mergeSchema(const Schema& s, bool mergeWithoutConflicts) 
      {
         if (mergeWithoutConflicts) {
            if (checkForConflict(attributes(), s.attributes()) || checkForConflict(attributeGroups(),
                  s.attributeGroups()) || checkForConflict(elements(), s.elements()) || checkForConflict(groups(),
                  s.groups()) || checkForConflict(types(), s.types())) {
               return false;
            }
         }

         merge(s.attributes(), s, &Schema::findAttribute, _attributes);
         merge(s.attributeGroups(), s, &Schema::findAttributeGroup, _attributeGroups);
         merge(s.elements(), s, &Schema::findElement, _elements);
         merge(s.groups(), s, &Schema::findGroup, _groups);
         merge(s.types(), s, &Schema::findType, _types);
         return true;
      }

      set< QName> AbstractBasicSchema::attributes() const 
      {
         return keys(_attributes);
      }

      set< QName> AbstractBasicSchema::attributeGroups() const 
      {
         return keys(_attributeGroups);

      }

      set< QName> AbstractBasicSchema::elements() const 
      {
         return keys(_elements);

      }

      set< QName> AbstractBasicSchema::groups() const 
      {
         return keys(_groups);

      }

      set< QName> AbstractBasicSchema::types() const 
      {
         return keys(_types);

      }

      bool AbstractBasicSchema::addAttribute(unique_ptr< Attribute> obj) 
      {
         return add(_attributes, move(obj));
      }

      bool AbstractBasicSchema::addAttributeGroup(unique_ptr< AttributeGroup> obj) 
      {
         return add(_attributeGroups, move(obj));
      }

      bool AbstractBasicSchema::addElement(unique_ptr< Element> obj) 
      {
         return add(_elements, move(obj));
      }

      bool AbstractBasicSchema::addGroup(unique_ptr< Group> obj) 
      {
         return add(_groups, move(obj));
      }

      bool AbstractBasicSchema::addType(unique_ptr< Type> obj) 
      {
         return add(_types, move(obj));
      }

      bool AbstractBasicSchema::hasElement(const QName& name) const 
      {
         return _elements.find(name) != _elements.end();
      }

      set< QName> AbstractBasicSchema::findSubstitutions(const QName& qname) const
      {
         set< QName> res;
         // FIXME: be more efficient
         for (map< QName, Element*>::const_iterator i = _elements.begin(); i != _elements.end(); ++i) {
            if (testElementSubstitution(qname, i->first)) {
               res.insert(i->first);
            }
         }
         return res;
      }

      const Attribute& AbstractBasicSchema::findAttribute(const QName& qname) const
      {
         map< QName, Attribute*>::const_iterator i = _attributes.find(qname);
         if (i == _attributes.end()) {
            throw UnknownAttributeException(qname);
         }
         return *(i->second);

      }

      const AttributeGroup& AbstractBasicSchema::findAttributeGroup(const QName& qname) const
      {
         map< QName, AttributeGroup*>::const_iterator i = _attributeGroups.find(qname);
         if (i == _attributeGroups.end()) {
            throw UnknownAttributeGroupException(qname);
         }
         return *(i->second);

      }

      const Element& AbstractBasicSchema::findElement(const QName& qname) const
      {
         map< QName, Element*>::const_iterator i = _elements.find(qname);
         if (i == _elements.end()) {
            throw UnknownElementException(qname);
         }
         return *(i->second);
      }

      const Group& AbstractBasicSchema::findGroup(const QName& qname) const
      {
         map< QName, Group*>::const_iterator i = _groups.find(qname);
         if (i == _groups.end()) {
            throw UnknownGroupException(qname);
         }
         return *(i->second);

      }

      const Type& AbstractBasicSchema::findType(const QName& qname) const
      {
         map< QName, Type*>::const_iterator i = _types.find(qname);
         if (i == _types.end()) {
            throw UnknownTypeException(qname);
         }
         return *(i->second);

      }

      bool AbstractBasicSchema::testTypeDerivation(const QName& baseType, const QName& derivedType) const 
      {
         map< QName, Type*>::const_iterator i = _types.find(derivedType);
         // search the type hierarchy
         while (i != _types.end()) {
            const Type* obj = i->second;
            if (obj->baseType() == baseType) {
               return true;
            }
            i = _types.find(obj->baseType());
         }
         return false;
      }

      bool AbstractBasicSchema::testElementSubstitution(const QName& expected, const QName& actual) const 
      {
         map< QName, Element*>::const_iterator i = _elements.find(actual);
         // check if element even exists
         if (i == _elements.end()) {
            return false;
         }

         // same element, return now
         if (actual == expected) {
            return true;
         }
         // search the type hierarchy
         while (i != _elements.end()) {
            const Element* obj = i->second;
            if (obj->substitutionGroup() == expected) {
               return true;
            }
            i = _elements.find(obj->substitutionGroup());
         }
         return false;
      }
   }
}
