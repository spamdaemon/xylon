#ifndef _XYLON_RT_ABSTRACTTYPEEXCEPTION_H
#define _XYLON_RT_ABSTRACTTYPEEXCEPTION_H

#ifndef _XYLON_RT_QNAME_H
#include <xylon/rt/QName.h>
#endif

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This exception is thrown when an attempt is made to instantiate and abstract type or element.
       */
      class AbstractTypeException : public ParseError
      {

            /**
             * Constructor.
             * @param qname name
             */
         public:
            AbstractTypeException(const QName& qname);

            /**
             * Destructor
             */
         public:
            AbstractTypeException();

            /**
             * Destructor
             */
         public:
            ~AbstractTypeException();

            /**
             * Get the qname that was not found.
             * @return the qname
             */
         public:
            inline const QName& name() const
            {
               return _name;
            }

            /** The unknown type */
         private:
            const QName _name;
      };

   }
}
#endif
