#ifndef _XYLON_RT_XSDSCHEMA_H
#define _XYLON_RT_XSDSCHEMA_H

#ifndef _XYLON_RT_SCHEMA_H
#include <xylon/rt/Schema.h>
#endif

#ifndef _XYLON_RT_BASICSCHEMA_H
#include <xylon/rt/BasicSchema.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This is an implementation of the public parts of the XSD schema itself. It is used
       * as the foundation required by all other schemas.
       */
      class XSDSchema : public AbstractBasicSchema
      {
            /** Copy operator and constructor */
         private:
            XSDSchema(const XSDSchema&);
            XSDSchema&operator=(const XSDSchema&);

            /** Default constructor */
         public:
            XSDSchema() ;

            /** The destructor */
         public:
            ~XSDSchema() ;

            /** Add an xsd type to this schema */
         private:
            template<class XSD>
            void addType(const String& xname) ;

      };

   }

}

#endif
