#include <xylon/rt/TypeObject.h>
#include <xylon/rt/Writer.h>
#include <xylon/rt/QName.h>

namespace xylon {
   namespace rt {
      TypeObject::TypeObject() 
      {
      }

      TypeObject::TypeObject(const TypeObject& a)  :
         Object(a)
      {
      }

      TypeObject& TypeObject::operator=(const TypeObject& a) 
      {
         Object::operator=(a);
         return *this;
      }

      TypeObject::~TypeObject() 
      {
      }

      void TypeObject::writeTypeImpl(const QName& expected, const QName& actual, Writer& writer) const
      {
         if (expected != actual) {
            writer.writeQNameAttribute(QName::xsiQName("type"), actual);
         }
         write(writer);
      }

   }
}
