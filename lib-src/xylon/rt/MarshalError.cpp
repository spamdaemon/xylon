#include <xylon/rt/MarshalError.h>

namespace xylon {
   namespace rt {

      MarshalError::MarshalError()  :
         ::std::runtime_error("Marshal error")
      {

      }

      MarshalError::MarshalError(const ::std::string& msg)  :
         ::std::runtime_error(msg)
      {

      }
      MarshalError::~MarshalError() 
      {
      }

   }
}
