#ifndef _XYLON_RT_STREAMWRITER_H
#define _XYLON_RT_STREAMWRITER_H

#ifndef _XYLON_RT_WRITER_H
#include <xylon/rt/Writer.h>
#endif

#ifndef _XYLON_RT_QNAME_H
#include <xylon/rt/QName.h>
#endif

#include <iosfwd>
#include <map>
#include <set>
#include <vector>

namespace xylon {
   namespace rt {

      /**
       * This class is used marshal an XML object.
       */
      class StreamWriter : public virtual Writer
      {
            /** A namespace mapping */
         public:
            struct NamespacePrefix
            {
                  /** A default constructor */
                  NamespacePrefix() ;

                  /** A destructor */
                  ~NamespacePrefix() ;

                  /** True if the prefix is currently active */
                  bool active;

                  /** The chosen prefix */
                  String prefix;
            };

            /** The prefix to namespace URI mapping */
         private:
            typedef ::std::map< String, String> Prefixes;

            /** The namespace to prefix mapping */
         private:
            typedef ::std::map< String, NamespacePrefix> Namespaces;

            /** The stack object */
         private:
            struct StackItem
            {
                  StackItem() ;

                  /** The name of the element on the stack */
                  QName element;

                  /** If an attribute has been opened */
                  bool attribute;

                  /** True if the current element's content has been started */
                  bool hasContent;

                  /** True if a newline must not followed */
                  bool noNewline;

                  /** The namespaces that have been pushed at the time the element was opened */
                  ::std::set< String> namespaces;

                  /** These are namespaces that must take effect whenever a child element is opened */
                  ::std::set< String> neededNamespaces;
            };

            /**
             * Default constructor
             * @param stream an output stream
             */
         public:
            StreamWriter(::std::ostream& stream) ;

            /** Destructor */
         public:
            ~StreamWriter() ;

            /**
             * Set the indent. With each level, the indent string is repeated. An empty string
             * turns off indenting.
             * @param indent the indent string.
             */
         public:
            void setIndent(const ::std::string& indent) ;

            /**
             * Add a namespace to this writer. The new namespace will apply to all the current element
             * and any child elements. If there is no current document or element or an attribute is currently active, then this namespace
             * will be applied  to all other elements.<p>
             * A prefix will automatically be chosen for the uri.
             * @param uri the uri
             * @return the chosen prefix
             */
         public:
            String addNamespace(const String& uri);

            /**
             * Begin a document.
             * @throw MarshalError if there is not current element
             */
         public:
            void beginDocument();

            /**
             * End all open elements to close the document. If no elements
             * are open then this method does nothing.
             */
         public:
            void endDocument();

            /**
             * Begin an element. If the element's namespace is not known yet, then it added via addNamespace().
             * @param qname the qualified name of the element.
             * @throw MarshalError if the object could not be marshaled
             */
         public:
            void beginElement(const QName& qname);

            /**
             * End the current element.
             * @throw MarshalError if there is not current element
             */
         public:
            void endElement();

            /**
             * Begin an attribute.
             * @param attr the attribute name
             */
         public:
            void beginAttribute(const QName& attr);

            /**
             * Begin an attribute.
             * @param attr the attribute name
             */
         public:
            void endAttribute();

            /**
             * Write text into the current element or attribute.
             * @param value a text value
             */
         public:
            void writeText(const String& text);

            /**
             * Write a QName attribute. The namespace of the value is replaced by a prefix that is mapped
             * to the namespace.<p>
             * The primary purpose of this method is to provide <tt>xsi:type</tt> support.
             * @param attr the name of the attribute
             * @param value a qname
             * @return the mapped prefix
             * @throw MarshalError if the object could not be marshaled
             */
         public:
            String writeQNameAttribute(const QName& attr, const QName& value);

            /**
             * Activate the specified namespace. The namespace cannot be activate if an attribute
             * is currently active.
             * @param ns a new namespace
             * @return the prefix
             * @throws MarshalError if namespace cannot be added at this time.
             */
         private:
            String activateNamespace(const String& ns);

            /**
             * Deactivate all namespaces that were activated by the current element.
             * @return the prefix to which the namespace of the current element had been mapped
             */
         private:
            String deactivateNamespaces();

            /**
             * Generate a new prefix.
             * @param uri a namespace uri
             * @return a new prefix
             */
         private:
            String generatePrefix(const String& uri) ;

            /**
             * Mark the current element has having content
             */
         private:
            void setHasContent() ;

            /**
             * Emit an indent line.
             */
         private:
            void doIndent() ;

            /** The output stream */
         private:
            ::std::ostream& _stream;
            /** The next prefix index */
         private:
            size_t _nextPrefix;

            /** The prefix stem */
         private:
            ::std::string _prefixStem;

            /** The currently active namespace mappings */
         private:
            Namespaces _namespaces;

            /** The known prefixes */
         private:
            Prefixes _prefixes;

            /** The current element */
         private:
            ::std::vector< StackItem> _stack;

            /** The index character */
         private:
            ::std::string _indentString;

            /** The current indent */
         private:
            ::std::string _indent;
      };

   }

}

#endif
