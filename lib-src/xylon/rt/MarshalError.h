#ifndef _XYLON_RT_MARSHALERROR_H
#define _XYLON_RT_MARSHALERROR_H

#include <stdexcept>

namespace xylon {
   namespace rt {

      /**
       * When a problem was encountered during a Marshaling operation.
       */
      class MarshalError : public ::std::runtime_error
      {

            /**
             * Constructor.
             * @param msg a message
             */
         public:
         MarshalError(const ::std::string& msg) ;

            /**
             * Default constructor.
             */
         public:
         MarshalError() ;

         /**
          * Default constructor.
          */
      public:
      ~MarshalError() ;
};

   }
}
#endif
