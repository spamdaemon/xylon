#ifndef _XYLON_RT_BASICSCHEMA_H
#define _XYLON_RT_BASICSCHEMA_H

#ifndef _XYLON_RT_ABSTRACTBASICSCHEMA_H
#include <xylon/rt/AbstractBasicSchema.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * The schema provides access to types, elements, and attributes defined by an XML schema document. A schema is typically
       * a composite schema and contains the subset of an original XML schema document, which is necessary to parse a document
       * corresponding to the schema. <p>
       */
      class BasicSchema : public virtual AbstractBasicSchema
      {
            /** Default constructor */
         public:
            BasicSchema() ;

            /**
             * Copy constructor.
             * @param src a source schema
             */
         public:
            BasicSchema(const BasicSchema& src) ;

            /**
             * Copy constructor.
             * @param src a source schema
             */
         public:
            BasicSchema(const Schema& src) ;

            /** Destructor */
         public:
            ~BasicSchema() ;

            /**
             * Copy a schema.
             * @param src another schema
             * @return *this
             */
         public:
            BasicSchema& operator=(const BasicSchema& src) ;

            /**
             * Copy a schema. This function is equivalent to the following code:
             * @code
             *   *this = BasicSchema(src);
             * @endcode
             * @param src another schema
             * @return *this
             */
         public:
            BasicSchema& operator=(const Schema& src) ;

            /**
             * Create a new basic schema instance by combining various other schemas. Conflicts among
             * the schemas are ignored.
             * @param schemas an array of schemas terminated by a null pointer.
             * @return a basic schema consisting of the union of groups, elements, etc of the schemas.
             */
         public:
            static ::std::unique_ptr< BasicSchema> create(const Schema** schemas) ;

            /**
             * Merge the specified schema into this schema. If this schema already contains some types, groups
             * that are also in the other schema, then they are not overriden, preserving the original.
             * @param s a schema
             * @param mergeWithoutConflicts if true only merge if there are no conflicts.
             * @return true if the merged, false otherwise
             */
         public:
            bool mergeSchema(const Schema& s, bool mergeWithoutConflicts = false) ;

            /**
             * Add an attribute to this schema.
             * @param attribute an attribute
             * @return true if the attribute was added, false if there was already such an attribute
             */
         public:
            bool addAttribute(::std::unique_ptr< Attribute> attribute) ;

            /**
             * Add an attribute group to this schema.
             * @param attribute an attribute group
             * @return true if the attribute group was added, false if there was already such an attribute group
             */
         public:
            bool addAttributeGroup(::std::unique_ptr< AttributeGroup> attributeGroup) ;

            /**
             * Add an element to this schema.
             * @param element an element
             * @return true if the element was added, false if there was already such an element
             */
         public:
            bool addElement(::std::unique_ptr< Element> element) ;

            /**
             * Add a group to this schema.
             * @param group a group
             * @return true if the group was added, false if there was already such a groups
             */
         public:
            bool addGroup(::std::unique_ptr< Group> group) ;

            /**
             * Add a type to this schema.
             * @param type a type
             * @return true if the type was added, false if there was already such a type
             */
         public:
            bool addType(::std::unique_ptr< Type> type) ;

      };

   }

}

#endif
