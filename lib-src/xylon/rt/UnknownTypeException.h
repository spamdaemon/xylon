#ifndef _XYLON_RT_UNKNOWNTYPEEXCEPTION_H
#define _XYLON_RT_UNKNOWNTYPEEXCEPTION_H

#ifndef _XYLON_RT_UNKNOWNQNAMEEXCEPTION_H
#include <xylon/rt/UnknownQNameException.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This exception is thrown when an unknown type is encountered by a schema.
       */
      class UnknownTypeException : public UnknownQNameException
      {

            /**
             * Constructor.
             * @param qname name
             */
         public:
            UnknownTypeException(const QName& qname) ;

            /**
             * Constructor
             */
         public:
            UnknownTypeException() ;

            /**
             * Destructor
             */
         public:
            ~UnknownTypeException() ;
      };

   }
}
#endif
