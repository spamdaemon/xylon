#include <xylon/rt/QNames.h>
#include <xylon/rt/QName.h>
#include <xylon/rt/Schema.h>
#include <xylon/rt/Instruction.h>

#include <utility>
#include <iostream>

namespace xylon {
   namespace rt {
      QNames::QNames() 
      {
      }
      QNames::QNames(const QNames& names)  :
         _names(names._names)
      {
      }

      QNames::QNames(const ::std::map< QName, Instruction>& names, const Schema& schema)  :
         _names(names)
      {
         for (::std::map< QName, Instruction>::const_iterator i = names.begin(); i != names.end(); ++i) {
            ::std::set< QName> s = schema.findSubstitutions(i->first);
            for (::std::set< QName>::const_iterator j = s.begin(); j != s.end(); ++j) {
               bool inserted = _names.insert(::std::make_pair(*j, i->second)).second;
               if (!inserted) {
                  ::std::cerr << "Warning: schema is ambiguous : " << *j << ::std::endl;
               }
            }
         }
      }

      QNames::~QNames() 
      {
      }

      QNames& QNames::operator=(const QNames& names) 
      {
         _names = names._names;
         return *this;
      }

      Instruction QNames::findInstruction(const QName& name) const 
      {
         ::std::map< QName, Instruction>::const_iterator i = _names.find(name);
         if (i == _names.end()) {
            return Instruction();
         }
         else {
            return i->second;
         }
      }

   }

}
