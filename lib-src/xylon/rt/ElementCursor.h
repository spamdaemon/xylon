#ifndef _XYLON_RT_ELEMENTCURSOR_H
#define _XYLON_RT_ELEMENTCURSOR_H

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

#ifndef _XYLON_RT_QNAME_H
#include <xylon/rt/QName.h>
#endif

#ifndef _XYLON_RT_H
#include <xylon/rt/rt.h>
#endif

#include <map>

namespace timber {
   namespace w3c {
      namespace xml {
         namespace dom {
            class Element;
         }
      }
   }
}
namespace xylon {
   namespace rt {
      class Writer;

      /**
       * The element cursor is used to traverse the elements of an XML document. Different types of cursors can be created
       * to support DOM based parsing, or direct text based parsing.<p>
       * As part of traversing the elements of a document, the cursor keeps track of the count of elements it has traversed so far.
       */
      class ElementCursor
      {
            /** The element attributes */
         public:
            typedef ::std::map< QName, String> Attributes;

            /** A clone pointer */
         public:
            typedef ::std::unique_ptr< ElementCursor> ClonePtr;

            /** Default constructor */
         protected:
            ElementCursor() ;

            /** Destructor */
         public:
            virtual ~ElementCursor()  = 0;

            /**
             * Create an invalid element cursor.
             */
         public:
            static ClonePtr createInvalidCursor () ;

            /**
             * Create a cursor for a DOM element.<p>
             * FIXME: eventually move this into its own class.
             * @param element a DOM element
             * @return an element cursor
             */
         public:
            static ClonePtr create(const ::timber::Pointer< ::timber::w3c::xml::dom::Element>& element) ;

            /**
             * Write the elements rooted at the current element to the specified writer.
             * @param w a writer
             */
         public:
            virtual void writeElement (Writer& w) const;

            /**
             * Get the current element.
             * @return a DOM element or null
             * @deprecated
             */
         public:
            virtual  ::timber::Pointer< ::timber::w3c::xml::dom::Element> element() const throws() = 0;

            /**
             * Create a clone of this cursor.
             * @return a clone
             */
         public:
            virtual ClonePtr clone() const  = 0;

            /**
             * Get all attributes that are not either XSI attributes or standard XML attributes. The returned
             * reference becomes obsolete once the the current element is modified.
             * moves to another element, the returned reference
             * @return a reference to a map of attributes for the current element.
             * @throw ParseError if the cursor is not valid when calling this method.
             */
         public:
            virtual const Attributes& attributes() const  = 0;

            /**
             * Determine if the current element has the given attribute.
             * @param name the name of an attribute
             * @return true if the current element has such an attribute, false otherwise
             * @throw ParseError if the cursor is not valid when calling this method.
             */
         public:
            virtual bool hasAttribute (const QName& name) const  = 0;

            /**
             * Get the value of the specified attribute. This method provides access to the same
             * attributes that would be returned by attributes().
             * @param name the name of an attribute
             * @return a string or a null string if the current element does not have this attribute
             * @throw ParseError if the cursor is not valid when calling this method.
             */
         public:
            virtual String attributeValue(const QName& name) const  = 0;

            /**
             * Get the value of the <tt>xsi:type</tt> attribute of the current element. If the current
             * element has no <tt>xsi:type</tt> attribute, then the default QName is returned.
             * @param defaultQName the default qname that should be returned if the xsi:type attribute is not found
             * @throw ParseError if the cursor is not valid when calling this method.
             */
         public:
            virtual QName xsiType(const QName& defaultQName = QName()) const  = 0;

            /**
             * Get the value of the current element.
             * @return the string value of this element
             * @throw ParseError if the content of the current element is mixed or if this cursor is not valid
             */
         public:
            virtual String value() const  = 0;

            /**
             * Get the number of elements seen so far. This value changes each time next() or down()
             * are complete successfully, but not if up() is called. This method may be used to determine if
             * a function moves the cursor ahead.
             * @return the current traversal id.
             */
         public:
            virtual size_t elementCount() const  = 0;

            /**
             * Move this cursor to the next element.
             * @throw ParseError if the cursor is not valid when calling this method.
             */
         public:
            virtual void next()  = 0;

            /**
             * Move this cursor to the first child element. The semantics of this operation is that of a push.
             * The current element can be restored by calling up.
             * @throw ParseError if the cursor is not valid when calling this method.
             */
         public:
            virtual void down()  = 0;

            /**
             * Move this cursor to the parent.
             * @throw ParseError if the cursor cannot be moved up
             */
         public:
            virtual void up()  = 0;

            /**
             * Test if this cursor is valid.
             * @return true if this cursor points to a valid element
             */
         public:
            virtual bool isValid() const  = 0;

            /**
             * Get the qualified name of the element to which this cursor points.
             * @return the name of the element
             * @throw ParseError if the cursor is not valid when calling this method.
             */
         public:
            virtual QName qname() const  = 0;

            /**
             * Get the namespace URI of the  the element to which this cursor points.
             * @return the namespace uri of the element or null if the element has no namespace
             * @throw ParseError if the cursor is not valid when calling this method.
             */
         public:
            virtual String namespaceURI() const = 0;

      };

   }

}

#endif /* ELEMENTCURSOR_H_ */
