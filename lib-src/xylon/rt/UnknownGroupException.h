#ifndef _XYLON_RT_UNKNOWNGROUPEXCEPTION_H
#define _XYLON_RT_UNKNOWNGROUPEXCEPTION_H

#ifndef _XYLON_RT_UNKNOWNQNAMEEXCEPTION_H
#include <xylon/rt/UnknownQNameException.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This exception is thrown when an unknown element is encountered by a schema.
       */
      class UnknownGroupException : public UnknownQNameException
      {

            /**
             * Constructor.
             * @param qname name
             */
         public:
            UnknownGroupException(const QName& qname) ;

            /**
             * Constructor
             */
         public:
            UnknownGroupException() ;

            /**
             * Destructor
             */
         public:
            ~UnknownGroupException() ;
      };

   }
}
#endif
