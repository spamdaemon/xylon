#ifndef _XYLON_RT_OBJECT_H
#define _XYLON_RT_OBJECT_H

namespace xylon {
   namespace rt {
      class Writer;

      /**
       * This class is the base class for all types defined by an XSD schema. It this corresponds
       * to the xsd:anyType.
       */
      class Object
      {
            /** The default constructor */
         protected:
            Object() ;

            /**
             * Copy constructor.
             * @param src the source object
             */
         protected:
            Object(const Object&) ;

            /** Copying of this object is allowed for subclasses. */
         protected:
            inline Object& operator=(const Object&) 
            {
               return *this;
            }

            /** The destructor */
         public:
            virtual ~Object()  = 0;

            /**
             * Clone this object
             * @return a deep clone of this object
             */
         public:
            virtual Object* clone() const = 0;

            /**
             * Write this object.
             * @param writer a writer
             * @throw MarshalError if this object cannot be marshaled.
             */
         public:
            virtual void write (Writer& writer) const = 0;
      };

   }
}

#endif
