#include <xylon/rt/AnySimpleType.h>
#include <xylon/rt/Writer.h>
#include <sstream>
#include <stdexcept>

namespace xylon {
   namespace rt {
      AnySimpleType::AnySimpleType(const AnySimpleType& obj)  :
         TypeObject(obj), _value(obj._value)
      {
      }

      AnySimpleType::AnySimpleType(const NativeType& obj)  :
         _value(obj)
      {
      }

      AnySimpleType::AnySimpleType() 
      {
      }

      AnySimpleType::~AnySimpleType() 
      {
      }


      AnySimpleType& AnySimpleType::operator=(const AnySimpleType& obj) 
      {
         _value = obj._value;
         return *this;
      }
      AnySimpleType& AnySimpleType::operator=(const NativeType& obj) 
      {
         _value = obj;
         return *this;
      }

      void AnySimpleType::fromString(const ::std::string& str)
      {
         ::std::istringstream in(str);
         read(in);
         if (in.fail()) {
            throw ::std::runtime_error("Could not instantiate AnySimpleType from " + str);
         }
      }
      AnySimpleType* AnySimpleType::clone() const
      {
         return new AnySimpleType(*this);
      }
      void AnySimpleType::write(::std::ostream& stream) const
      {
         stream << toString();
      }

      void AnySimpleType::write(Writer& w) const
      {
         w.writeText(toString());
      }

      void AnySimpleType::writeType(const QName& expected, Writer& w) const
      {
         write(w);
      }

      void AnySimpleType::read(::std::istream& stream)
      {
         NativeType tmp;
         stream >> tmp;
         fromString(tmp);
      }

      ::std::string AnySimpleType::toString() const
      {
         ::std::ostringstream out;
         write(out);
         if (out.fail()) {
            throw ::std::runtime_error("Conversion to string failed");
         }
         return out.str();
      }
   }
}
