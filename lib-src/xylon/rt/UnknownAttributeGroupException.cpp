#include <xylon/rt/UnknownAttributeGroupException.h>

namespace xylon {
   namespace rt {

      UnknownAttributeGroupException::UnknownAttributeGroupException(const QName& qname)  :
         UnknownQNameException(qname)
      {

      }

      UnknownAttributeGroupException::UnknownAttributeGroupException() 
      {

      }

      UnknownAttributeGroupException::~UnknownAttributeGroupException() 
      {

      }

   }
}
