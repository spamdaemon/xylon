#include <xylon/rt/UnknownElementException.h>

namespace xylon {
   namespace rt {

      UnknownElementException::UnknownElementException(const QName& qname)  :
         UnknownQNameException(qname)
      {

      }

      UnknownElementException::UnknownElementException() 
      {

      }

      UnknownElementException::~UnknownElementException() 
      {

      }

   }
}
