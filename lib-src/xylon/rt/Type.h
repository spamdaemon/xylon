#ifndef _XYLON_RT_TYPE_H
#define _XYLON_RT_TYPE_H

#ifndef _XYLON_RT_TOPLEVELNODE_H
#include <xylon/rt/TopLevelNode.h>
#endif

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

#ifndef _XYLON_RT_ABSTRACTTYPEEXCEPTION_H
#include <xylon/rt/AbstractTypeException.h>
#endif

#ifndef _XYLON_RT_TYPEOBJECT_H
#include <xylon/rt/TypeObject.h>
#endif

#include <memory>

namespace xylon {
   namespace rt {
      class ParserState;

      /**
       * This class represents an XML element and any information needed to parse the element. The class is abstract
       * requiring subclassing.
       */
      class Type : public TopLevelNode
      {

            /** An object pointer */
         public:
            typedef ::std::unique_ptr< TypeObject> ObjectPtr;

            /** Default constructor */
         protected:
            Type() ;

            /** Destructor */
         public:
            virtual ~Type()  = 0;

            /**
             * Create an instance of the C++ class to which this node type is mapped.
             * @return an instance of the mapped type
             * @throw AbstractTypeException if the type is abstract
             */
         public:
            virtual ObjectPtr newInstance() const  = 0;

            /**
             * Determine if this is an abstract type. If this type is abstract, then
             * newInstance() <em>always</em> throws an AbstractTypeException.
             */
         public:
            virtual bool isAbstract() const  = 0;

            /**
             * Clone this node.
             * @return a copy of this node
             */
         public:
            virtual ::std::unique_ptr<Type> clone() const  = 0;

          /**
             * Get the base type of this type.
             * @return the qname of the basetype
             */
         public:
            virtual QName baseType() const  = 0;

            /**
             * Parse an instance of this type. The instance of the represented type is on top of
             * the parser state.
             * @param ps a parser state
             * @throw ParseError if the type cannot be parsed into the object
             */
         public:
            virtual void
            parse( ParserState& ps) const = 0;
      };

   }

}

#endif
