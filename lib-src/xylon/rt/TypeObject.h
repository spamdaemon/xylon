#ifndef _XYLON_RT_TYPEOBJECT_H
#define _XYLON_RT_TYPEOBJECT_H

#ifndef _XYLON_RT_OBJECT_H
#include <xylon/rt/Object.h>
#endif

namespace xylon {
   namespace rt {
      class QName;

      /**
       * This class is the base class for objects that are Types.
       */
      class TypeObject : public Object
      {
            /** The default constructor */
         protected:
            TypeObject() ;

            /**
             * Copy constructor.
             * @param src the source any
             */
         protected:
            TypeObject(const TypeObject& src) ;

            /** The destructor */
         public:
            ~TypeObject() ;

            /** Copy operator */
         protected:
            TypeObject& operator=(const TypeObject&) ;

            /**
             * Clone this object
             * @return a deep clone of this object
             */
         public:
            virtual TypeObject* clone() const = 0;

            /**
             * Write this instance. This method is only invoked
             * by an element that may require the use of a xsi:type
             * specification. If this object does represent the specified
             * type, then an xsi type definition must be emitted.
             * <p>
             * Implementations of this method must invoke the equivalent code to that
             * implemented by writeTypeImpl()
             *
             * @param expected the expected type name
             * @param writer a writer
             */
         public:
//            virtual void writeType(const QName& expected, Writer& writer) const = 0;

            /**
             * A simple helper method that provides the basic implementation for writeType. This method
             * implements the following sequence:
             * @code
             *   if (expected!=actual) {
             *      writer.writeQNameAttribute(QName::xsiQName("type"),actual);
             *   }
             *   write(writer);
             * @endcode
             *
             * @param expected the expected type
             * @param actual the actual type
             * @param writer a writer
             */
         protected:
            void writeTypeImpl(const QName& expected, const QName& actual, Writer& writer) const;
      };

   }
}

#endif
