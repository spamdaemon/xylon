#include <xylon/rt/UnknownTypeException.h>

namespace xylon {
   namespace rt {

      UnknownTypeException::UnknownTypeException(const QName& qname)  :
      UnknownQNameException(qname)
      {

      }

      UnknownTypeException::UnknownTypeException() 
      {

      }

      UnknownTypeException::~UnknownTypeException() 
      {

      }

   }
}
