#ifndef _XYLON_RT_UNKNOWNQNAMEEXCEPTION_H
#define _XYLON_RT_UNKNOWNQNAMEEXCEPTION_H

#ifndef _XYLON_RT_QNAME_H
#include <xylon/rt/QName.h>
#endif

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This exception is thrown when an unknown element is encountered by a schema.
       */
      class UnknownQNameException : public ParseError
      {

            /**
             * Constructor.
             * @param qname name
             */
         public:
            UnknownQNameException(const QName& qname) ;

            /**
             * Destructor
             */
         public:
            UnknownQNameException() ;

            /**
             * Destructor
             */
         public:
            ~UnknownQNameException() ;

            /**
             * Get the qname that was not found.
             * @return the qname
             */
         public:
            inline const QName& name() const 
            {
               return _name;
            }

            /** The unknown type */
         private:
            const QName _name;
      };

   }
}
#endif
