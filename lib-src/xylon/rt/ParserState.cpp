#include <xylon/rt/ParserState.h>
#include <xylon/rt/ElementCursor.h>
#include <xylon/rt/Schema.h>
#include <xylon/rt/Attribute.h>
#include <xylon/rt/AttributeGroup.h>
#include <xylon/rt/Element.h>
#include <xylon/rt/Group.h>
#include <xylon/rt/Type.h>
#include <xylon/rt/Instruction.h>

#include <algorithm>
#include <iostream>

namespace xylon {

   namespace rt {

      ParserState::ParserState(const Schema& xschema, ::std::unique_ptr< ElementCursor> xcursor)  :
         _schema(xschema), _cursor(::std::move(xcursor)), _activeType(NONE)
      {
      }

      ParserState::ParserState(const ParserState& src, size_t copyFlags)  :
         _schema(src._schema), _cursor(src._cursor->clone()), _activeAttribute(src._activeAttribute),
               _activatedObjectValue(src._activatedObjectValue), _activeValue(src._activeValue), _activeType(
                     src._activeType)
      {
         try {
            if ((copyFlags & (COPY_STACK | APPEND_STACK)) != 0) {
               _stack.reserve(src._stack.size());
               for (::std::vector< Object*>::const_iterator i = src._stack.begin(); i != src._stack.end(); ++i) {
                  ObjectPtr ptr((*i)->clone());
                  _stack.push_back(ptr.get());
                  ptr.release();
               }
            }

            if ((copyFlags & COPY_ATTRIBUTES) != 0) {
               for (ParsedAttributes::const_iterator i = src._attributes.begin(); i != src._attributes.end(); ++i) {
                  ObjectPtr ptr;
                  if (i->second != 0) {
                     ptr.reset(i->second->clone());
                  }
                  _attributes[i->first] = ptr.get();
                  ptr.release();
               }
            }
         }
         catch (...) {
            cleanup();
            throw;
         }
      }

      ParserState::ParserState(const ParserState& src)  :
               //FIXME: call ParserState(src,true)
               _schema(src._schema), _cursor(src._cursor->clone()), _activeAttribute(src._activeAttribute),
               _activatedObjectValue(src._activatedObjectValue), _activeValue(src._activeValue), _activeType(
                     src._activeType)
      {
         try {
            // the stack is already copied, but only the raw pointers; clone each pointer
            _stack.reserve(src._stack.size());
            for (::std::vector< Object*>::const_iterator i = src._stack.begin(); i != src._stack.end(); ++i) {
               ObjectPtr ptr((*i)->clone());
               _stack.push_back(ptr.get());
               ptr.release();
            }

            for (ParsedAttributes::const_iterator i = src._attributes.begin(); i != src._attributes.end(); ++i) {
               ObjectPtr ptr;
               if (i->second != 0) {
                  ptr.reset(i->second->clone());
               }
               _attributes[i->first] = ptr.get();
               ptr.release();
            }
         }
         catch (...) {
            cleanup();
            throw;
         }

      }

      ParserState::~ParserState()
      {
         cleanup();
      }

      void ParserState::clearStack() 
      {
         while (!_stack.empty()) {
            delete _stack.back();
            _stack.pop_back();
         }
      }

      void ParserState::cleanup() 
      {
         clearAttributes();
         clearStack();
      }

      void ParserState::move(ParserState& src, size_t copyFlags)
      {
         if (&src == this || &src._schema != &_schema) {
            throw ParseError("Cannot move parser state from different schemas");
         }
         assert(((copyFlags & (APPEND_STACK | COPY_STACK)) != (APPEND_STACK | COPY_STACK))
               && "Cannot both copy and append");

         _cursor = ::std::move(src._cursor);
         _activeAttribute = src._activeAttribute;
         _activatedObjectValue = src._activatedObjectValue;
         _activeValue = src._activeValue;
         _activeType = src._activeType;

         // clear this stack,
         if ((copyFlags & COPY_STACK) != 0) {
            clearStack();
            _stack.swap(src._stack);
         }
         else if ((copyFlags & APPEND_STACK) != 0) {
            _stack.insert(_stack.end(), src._stack.begin(), src._stack.end());
            src._stack.clear();
         }
         else {
            // don't modify the current objects at all
         }

         if ((copyFlags & COPY_ATTRIBUTES) != 0) {
            clearAttributes();
            _attributes.swap(src._attributes);
         }
         else {
            // leave the attributes alone
         }
      }

      void ParserState::pushAttribute(const QName& name)
      {
         const Attribute& type = _schema.findAttribute(name);
         type.parse(*this);
      }

      void ParserState::pushAttributeGroup(const QName& name)
      {
         const AttributeGroup& type = _schema.findAttributeGroup(name);
         type.parse(*this);
      }

      void ParserState::pushElement(const QName& expected)
      {
         const QName& actual = cursor().qname();
         if (!_schema.testElementSubstitution(expected, actual)) {
            if (actual == expected) {
               // a special case; this means the element doesn't exist
               throw ParseError("Element " + actual.string().string() + " is not known.");
            }
            else {
               throw ParseError("Element " + actual.string().string() + " cannot be substituted for "
                     + expected.string().string());
            }
         }

         const Element& type = _schema.findElement(actual);
         ObjectPtr obj(type.newInstance());
         _stack.push_back(obj.get());
         obj.release();
         type.parse(*this);
      }

      void ParserState::pushElement()
      {
         const QName& actual = cursor().qname();
         const Element& type = _schema.findElement(actual);
         ObjectPtr obj(type.newInstance());
         _stack.push_back(obj.get());
         obj.release();
         type.parse(*this);
      }

      void ParserState::pushGroup(const QName& name)
      {
         const Group& type = _schema.findGroup(name);
         type.parse(*this);

      }

      void ParserState::pushType(const QName& name)
      {
         const Type& type = _schema.findType(name);
         ObjectPtr obj(type.newInstance());
         _stack.push_back(obj.get());
         obj.release();
         type.parse(*this);
      }

      bool ParserState::tryPushType(const QName& name)
      {
         ParserState bak(*this, false);
         try {
            bak.pushType(name);
         }
         catch (const ParseError& e) {
            return false;
         }
         // do this outside the try-catch block, because we need to throw
         // a ParseError if we cannot move
         move(bak, APPEND_STACK);
         return true;
      }

      void ParserState::pushXsiType(const QName& name)
      {
         const QName xsiType = cursor().xsiType(name);
         if (xsiType == name || _schema.testTypeDerivation(name, xsiType)) {
            pushType(xsiType);
         }
         else if (!xsiType) {
            // this only happens when name is an invalid name, but can also happen if xsi type is invalid
            throw ParseError("The current element has an invalid xsi:type definition");
         }
         else {
            throw ParseError("xsi:type " + xsiType.string().string() + " is not a valid subtype of "
                  + name.string().string());
         }
      }

      void ParserState::clearAttributes() 
      {
         for (ParsedAttributes::iterator i = _attributes.begin(); i != _attributes.end(); ++i) {
            delete i->second;
         }
         _attributes.clear();
      }

      bool ParserState::isKnownElement() const
      {
         const QName& actual = cursor().qname();
         return _schema.hasElement(actual);
      }

      void ParserState::activateElement(bool expectValidElement)
      {
         if (cursor().isValid()) {
            _activeValue = String();
            _activeType = ELEMENT;
         }
         else if (expectValidElement) {
            throw ParseError("ElementCursor not valid: cannot activate element");
         }
         else {
            _activeValue = String();
            _activeType = NONE;
         }

         // clear any values
         _activeAttribute = QName();
         _activatedObjectValue = String();
      }

      bool ParserState::activateAttribute(const QName& name)
      {
         String value = cursor().attributeValue(name);
         _activeValue = String();
         if (value) {
            _activeAttribute = name;
            _activatedObjectValue = value;
            _activeType = ATTRIBUTE;
            return true;
         }
         else {
            _activeAttribute = QName();
            _activatedObjectValue = String();
            _activeType = NONE;
            return false;
         }
      }

      void ParserState::activateStringValue(const String& value)
      {
         _activeAttribute = QName();
         _activeValue = String();
         _activatedObjectValue = value;
         if (value) {
            _activeType = STRING;
         }
         else {
            _activeType = NONE;
         }
      }

      const QName& ParserState::activeAttribute() const
      {
         if (_activeType == ATTRIBUTE) {
            return _activeAttribute;
         }
         else {
            throw ParseError("No active attribute");
         }
      }

      void ParserState::activateValue()
      {
         switch (_activeType) {
            case ELEMENT:
               _activeValue = cursor().value();
               break;
            case ATTRIBUTE:
               // fall-through
            case STRING:
               _activeValue = _activatedObjectValue;
               break;
            default:
               throw ParseError("Nothing activated");
         }

         if (!_activeValue) {
            throw new ParseError("Internal parser error");
         }
      }

      String ParserState::activeValue() const
      {
         if (_activeValue) {
            return _activeValue;
         }
         throw ParseError("There is no activated value");
      }

      size_t ParserState::findElement(const QName* names, const size_t n) const
      {
         QName actual = cursor().qname();
         const QName* end = names + n;
         try {
            // execute this function at least once, even if we don't know the element in the schema by a global name
            // the element might be still be a local element or one that can be matched to an xs:any.
            do {
               // check if the item exists
               const QName* ptr = ::std::lower_bound(names, end, actual);
               // if found, return the index
               if (ptr != end && (*ptr == actual)) {
                  return ptr - names;
               }

               // at this point, the element needs to be a global type; if it's not, we'll catch the exception return a negative response.
               const Element& type = _schema.findElement(actual);

               actual = type.substitutionGroup();
            } while (actual);
         }
         catch (const ::std::exception& e) {
         }
         return n;
      }

      size_t ParserState::findElementIgnoreSubstitutions(const QName* names, const size_t n) const
      {
         QName actual = cursor().qname();
         const QName* end = names + n;
         try {
            // check if the item exists
            const QName* ptr = ::std::lower_bound(names, end, actual);
            // if found, return the index
            if (ptr != end && (*ptr == actual)) {
               return ptr - names;
            }
         }
         catch (const ::std::exception& e) {
         }
         return n;
      }

      bool ParserState::testElement(const QName& qname) const
      {
         const QName actual = cursor().qname();
         // need to check here, because the qname may be a locally defined element and as
         // such, the schema won't recognize it
         if (actual == qname) {
            return true;
         }
         return _schema.testElementSubstitution(qname, actual);
      }

      size_t ParserState::activateListValues(const Instruction& instr)
      {
         String list = activeValue();

         size_t n = 0;
         const size_t len = list.length();
         for (size_t pos = 0, i = 0; pos < len; pos = i + 1) {
            i = list.indexOf(' ', pos);
            if (i == String::NOT_FOUND) {
               i = len;
            }
            String substr = list.substring(pos, i - pos);
            if (!substr.isEmpty()) {
               // activate the string and then make it the current value
               activateStringValue(substr);
               activateValue();
               instr.execute(*this);
               ++n;
            }
            else {
               assert(pos >= i);
            }
         }
         return n;
      }

      bool ParserState::loop(const Instruction& instruction, size_t min, size_t max)
      {
         assert(max >= min && "Loop requires that min <= max");
         assert(max > 0 && "Maximum number of iterations must at least be 1");

         size_t count = cursor().elementCount();
         size_t i = 0;

         // loop up to a total of max times
         while (i != max && cursor().isValid()) {
            size_t n = count;
            instruction.execute(*this);
            n = cursor().elementCount();
            // if no elements were "consumed" then break out
            if (count == n) {
               break;
            }
            count = n;
            ++i;
         }

         // minimum number of times not matched
         return i >= min;
      }

   }

}
