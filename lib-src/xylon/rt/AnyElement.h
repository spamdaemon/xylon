#ifndef _XYLON_RT_ANYELEMENT_H
#define _XYLON_RT_ANYELEMENT_H

#ifndef _XYLON_RT_ELEMENTOBJECT_H
#include <xylon/rt/ElementObject.h>
#endif

#ifndef _XYLON_RT_ELEMENTCURSOR_H
#include <xylon/rt/ElementCursor.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This class allow any element content.
       */
      class AnyElement : public ElementObject
      {
            /** Type of object to which this element is set */
         public:
            enum ObjectType
            {
               /** No object currently set */
               NONE,

               /** A parsed object was set and the method object() returns a valid pointer or refernce. */
               PARSED,

               /** An unparsed element is set and the method element returns a valid reference */
               UNPARSED
            };

            /** The default constructor */
         public:
            AnyElement() ;

            /**
             * Copy constructor.
             * @param src the source any
             */
         public:
            AnyElement(const AnyElement& src) ;

            /** The destructor */
         public:
            ~AnyElement() ;

            /** Copy operator */
         public:
            AnyElement& operator=(const AnyElement&) ;

            /** Clone this any object */
         public:
            AnyElement* clone() const;

            /**
             * Get the type of object currently set.
             * @return the type of object
             */
         public:
            inline ObjectType type() const 
            {
               return _type;
            }

            /** The xsd object contained in this any */
         public:
            void setObject(::std::unique_ptr< ElementObject> obj) ;

            /** The xsd object contained in this any.
             * @return a reference to an object
             * @throw ::std::logic_error if <tt>type()!=PARSED</tt>
             */
         public:
            ElementObject& object();

            /**
             * The xsd object contained in this any.
             * @return a reference to an object
             * @throw ::std::logic_error if <tt>type()!=PARSED</tt>
             */
         public:
            const ElementObject& object() const;

            /**
             * The element for this any object.
             * @return the document element
             * @throw ::std::logic_error if <tt>type()!=UNPARSED</tt>
             */
         public:
            const ElementCursor& element() const;

            /**
             * The element for this any object.
             * @note This method does not make a copy of the provided cursor and relies on the caller to make such a copy.
             * @param e the element contained in this any object or null
             * @return the document element
             * @throw ::std::invalid_argument if e is not a valid cursor
             */
         public:
            void setElement(::std::unique_ptr< ElementCursor> e) throws (::std::invalid_argument);

            /**
             * The element for this any object. This method makes a copy of the element cursor.
             * @param e the element contained in this any object
             * @return the document element
             * @throw ::std::invalid_argument if e is not a valid cursor
             */
         public:
            void setElement(const ElementCursor& e) throws (::std::invalid_argument);

            /**
             * Write this element
             */
         public:
            void write(Writer& w) const;

            /** The type of object */
         private:
            ObjectType _type;

            /** The element */
         private:
            ::std::unique_ptr< ElementCursor> _element;

            /** The object */
         private:
            ::std::unique_ptr< ElementObject> _object;
      };

   }
}
#endif
