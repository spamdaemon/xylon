#ifndef _XYLON_RT_TOPLEVELNODE_H
#define _XYLON_RT_TOPLEVELNODE_H

#ifndef _XYLON_RT_MARSHALERROR_H
#include <xylon/rt/MarshalError.h>
#endif

namespace xylon {
   namespace rt {
      class QName;
      class Writer;
      class Object;

      /**
       * This class represents a top-level in an XML schema. There 5 types of top-level nodes:
       * # Attribute
       * # AttributeGroup
       * # ElementObject
       * # Group
       * # TypeObject (complex and simple)
       */
      class TopLevelNode
      {
            /** Default constructor */
         protected:
            TopLevelNode() ;

            /** Destructor */
         public:
            virtual ~TopLevelNode()  = 0;

            /**
             * Get the qname of this node given to it in the schema.
             * @return a reference to a qname
             */
         public:
            virtual QName qname() const  = 0;

            /**
             * Write an object using the specified writer.
             * @param obj an object
             * @param writer a writer
             * @throw MarshalError if the object could not be marshaled
             */
         public:
            virtual void marshal(const Object& obj, Writer& writer) const = 0;
      };

   }

}

#endif
