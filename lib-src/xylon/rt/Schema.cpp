#include <xylon/rt/Schema.h>
#include <xylon/rt/ElementCursor.h>
#include <xylon/rt/Element.h>
#include <xylon/rt/ParserState.h>

namespace xylon {
   namespace rt {

      Schema::Schema() 
      {
      }

      Schema::~Schema() 
      {
      }
      bool Schema::hasElement(const QName& name) const 
      {
         // not the most efficient way of doing things, but's it ok
         return testElementSubstitution(name, name);
      }

      Schema::ObjectPtr Schema::parseElement(ElementCursor& cursor) const
      {
         ParserState ps(*this, cursor.clone());
         ps.pushElement(cursor.qname());
         return ps.popObject< Object> ();
      }

      Schema::ObjectPtr Schema::parseElement(const QName& expected, ElementCursor& cursor) const
      {
         ParserState ps(*this, cursor.clone());
         ps.pushElement(expected);
         return ps.popObject< Object> ();
      }
   }

}
