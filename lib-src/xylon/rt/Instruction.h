#ifndef _XYLON_RT_INSTRUCTION_H
#define _XYLON_RT_INSTRUCTION_H

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

namespace xylon {
   namespace rt {
      class ParserState;

      /**
       * A xylon instruction.
       */
      class Instruction
      {
            /**
             * Default constructor creates an invalid instruction. When executed this instruction
             * generates a ParseError.
             */
         public:
            Instruction() ;

            /** Destructor */
         public:
            virtual ~Instruction()  ;

            /**
             * Determine if this is a valid instruction. The default is to return true.
             * @return true if this is a valid instruction, false otherwise
             */
         public:
            virtual bool isValid() const ;

            /**
             * Execute this instruction.
             * @param state the parse state
             */
         public:
            virtual void execute (ParserState& state) const ;
      };

   }

}

#endif
