#include <xylon/rt/BasicSchema.h>
#include <xylon/rt/Attribute.h>
#include <xylon/rt/AttributeGroup.h>
#include <xylon/rt/Element.h>
#include <xylon/rt/Group.h>
#include <xylon/rt/Type.h>

using namespace ::std;

namespace xylon {
   namespace rt {

      BasicSchema::BasicSchema() 
      {
      }

      BasicSchema::BasicSchema(const BasicSchema& src)  :
         AbstractBasicSchema(src)
      {
      }

      BasicSchema::BasicSchema(const Schema& src)  :
         AbstractBasicSchema(src)
      {
      }

      BasicSchema::~BasicSchema() 
      {
      }

      BasicSchema& BasicSchema::operator=(const BasicSchema& src) 
      {
         AbstractBasicSchema::operator=(src);
         return *this;
      }

      BasicSchema& BasicSchema::operator=(const Schema& src) 
      {
         AbstractBasicSchema::operator=(src);
         return *this;
      }

      unique_ptr< BasicSchema> BasicSchema::create(const Schema** schemas) 
      {
         unique_ptr< BasicSchema> res(new BasicSchema());

         const Schema** ptr = schemas;
         while (*ptr) {
            if (!res->mergeSchema(**ptr, false)) {
               // cannot merge
               return unique_ptr< BasicSchema> ();
            }
         }

         return res;
      }

      bool BasicSchema::mergeSchema(const Schema& s, bool mergeWithoutConflicts) 
      {
         return AbstractBasicSchema::mergeSchema(s, mergeWithoutConflicts);
      }

      bool BasicSchema::addAttribute(::std::unique_ptr< Attribute> attribute) 
      {
         return AbstractBasicSchema::addAttribute(::std::move(attribute));
      }

      bool BasicSchema::addAttributeGroup(::std::unique_ptr< AttributeGroup> attributeGroup) 
      {
         return AbstractBasicSchema::addAttributeGroup(::std::move(attributeGroup));
      }

      bool BasicSchema::addElement(::std::unique_ptr< Element> element) 
      {
         return AbstractBasicSchema::addElement(::std::move(element));
      }

      bool BasicSchema::addGroup(::std::unique_ptr< Group> group) 
      {
         return AbstractBasicSchema::addGroup(::std::move(group));
      }

      bool BasicSchema::addType(::std::unique_ptr< Type> type) 
      {
         return AbstractBasicSchema::addType(::std::move(type));
      }

   }
}
