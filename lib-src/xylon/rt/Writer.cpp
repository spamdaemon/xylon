#include <xylon/rt/Writer.h>
#include <xylon/rt/QName.h>
#include <string>

namespace xylon {
   namespace rt {
      Writer::Writer() 
      {
      }

      Writer::~Writer() 
      {
      }

      void Writer::writeAttribute(const QName& attr, const String& value)
      {
         beginAttribute(attr);
         writeText(value);
         endAttribute();
      }


   }

}
