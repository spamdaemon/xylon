#ifndef _XYLON_RT_ABSTRACTBASICSCHEMA_H
#define _XYLON_RT_ABSTRACTBASICSCHEMA_H

#ifndef _XYLON_RT_SCHEMA_H
#include <xylon/rt/Schema.h>
#endif

namespace xylon {
   namespace rt {
      class QName;
      class ElementCursor;
      class Attribute;
      class AttributeGroup;
      class Element;
      class Group;
      class Type;

      /**
       * The schema provides access to types, elements, and attributes defined by an XML schema document. A schema is typically
       * a composite schema and contains the subset of an original XML schema document, which is necessary to parse a document
       * corresponding to the schema. <p>
       */
      class AbstractBasicSchema : public virtual Schema
      {
            /** Default constructor */
         protected:
            AbstractBasicSchema() ;

            /**
             * Copy constructor.
             * @param src a source schema
             */
         protected:
            AbstractBasicSchema(const AbstractBasicSchema& src) ;

            /**
             * Copy constructor.
             * @param src a source schema
             */
         protected:
            AbstractBasicSchema(const Schema& src) ;

            /** Destructor */
         public:
            ~AbstractBasicSchema() ;

            /**
             * Copy a schema.
             * @param src another schema
             * @return *this
             */
         protected:
            AbstractBasicSchema& operator=(const AbstractBasicSchema& src) ;

            /**
             * Copy a schema. This function is equivalent to the following code:
             * @code
             *   *this = AbstractBasicSchema(src);
             * @endcode
             * @param src another schema
             * @return *this
             */
         protected:
            AbstractBasicSchema& operator=(const Schema& src) ;

            /**
             * Merge the specified schema into this schema. If this schema already contains some types, groups
             * that are also in the other schema, then they are not overriden, preserving the original.
             * @param s a schema
             * @param mergeWithoutConflicts if true only merge if there are no conflicts.
             * @return true if the merged, false otherwise
             */
         protected:
            bool mergeSchema(const Schema& s, bool mergeWithoutConflicts = false) ;

         public:
            ::std::set< QName> attributes() const ;

            ::std::set< QName> attributeGroups() const ;

            ::std::set< QName> elements() const ;

            ::std::set< QName> groups() const ;

            ::std::set< QName> types() const ;

            bool hasElement(const QName& name) const ;

            /**
             * Add an attribute to this schema.
             * @param attribute an attribute
             * @return true if the attribute was added, false if there was already such an attribute
             */
         protected:
            bool addAttribute(::std::unique_ptr< Attribute> attribute) ;

            /**
             * Add an attribute group to this schema.
             * @param attribute an attribute group
             * @return true if the attribute group was added, false if there was already such an attribute group
             */
         protected:
            bool addAttributeGroup(::std::unique_ptr< AttributeGroup> attributeGroup) ;

            /**
             * Add an element to this schema.
             * @param element an element
             * @return true if the element was added, false if there was already such an element
             */
         protected:
            bool addElement(::std::unique_ptr< Element> element) ;

            /**
             * Add a group to this schema.
             * @param group a group
             * @return true if the group was added, false if there was already such a groups
             */
         protected:
            bool addGroup(::std::unique_ptr< Group> group) ;

            /**
             * Add a type to this schema.
             * @param type a type
             * @return true if the type was added, false if there was already such a type
             */
         protected:
            bool addType(::std::unique_ptr< Type> type) ;

            /**
             * Get the elements that can be substituted for the specified element. The returned collection
             * does <em>not</em> contain qname itself.
             * @param qname the name of an element
             * @return a set of names that can be substituted for the specified element
             * @throws UnknownAttributeException on error
             */
         public:
            ::std::set< QName> findSubstitutions(const QName& qname) const;

            /**
             * Find the specified attribute.
             * @param qname the name of an attribute
             * @return an attribute reference
             * @throws UnknownAttributeException if the attribute is not known to this schema
             */
         public:
            const Attribute& findAttribute(const QName& qname) const ;

            /**
             * Find the specified attribute group.
             * @param qname the name of an attribute group
             * @return an attribute group reference
             * @throws UnknownGroupException if the attribute group is not known to this schema
             */
         public:
            const AttributeGroup& findAttributeGroup(const QName& qname) const;

            /**
             * Find the specified element.
             * @param qname the name of a element
             * @return a element reference
             * @throws UnknownElementException if the element is not known to this schema
             */
         public:
            const Element& findElement(const QName& qname) const ;

            /**
             * Find the specified group.
             * @param qname the name of a group
             * @return a group reference
             * @throws UnknownGroupException if the group is not known to this schema
             */
         public:
            const Group& findGroup(const QName& qname) const ;

            /**
             * Find the specified type.
             * @param qname the name of a type
             * @return a type reference
             * @throws UnknownTypeException if the type is not known to this schema
             */
         public:
            const Type& findType(const QName& qname) const ;

            /**
             * Determine if the specified types are base type and derived type. If <tt>baseType == derivedType</tt>
             * then this method returns false.
             * @param baseType the name of a type
             * @param derivedType the name of type
             * @return true if derivedType is directly or indirectly derived from baseType.
             */
         public:
            bool testTypeDerivation(const QName& baseType, const QName& derivedType) const ;

            /**
             * Determine if an actual element can be substituted for an expected element. If <tt>actual==expected</tt> then
             * this method must return <tt>true</tt>.
             * @param expected an expected element
             * @param actual an actual element
             * @return true <tt>actual</tt> is in the substitution group headed by <tt>expected</tt>
             */
         public:
            bool testElementSubstitution(const QName& expected, const QName& actual) const ;

            /** The known attributes */
         private:
            ::std::map< QName, Attribute*> _attributes;

            /** The known attribute groups */
         private:
            ::std::map< QName, AttributeGroup*> _attributeGroups;

            /** The known elements */
         private:
            ::std::map< QName, Element*> _elements;

            /** The known groups */
         private:
            ::std::map< QName, Group*> _groups;

            /** The known types */
         private:
            ::std::map< QName, Type*> _types;

      };

   }

}

#endif
