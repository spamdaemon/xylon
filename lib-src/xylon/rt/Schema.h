#ifndef _XYLON_RT_SCHEMA_H
#define _XYLON_RT_SCHEMA_H

#ifndef _XYLON_RT_OBJECT_H
#include <xylon/rt/Object.h>
#endif

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

#ifndef _XYLON_RT_UNKNOWNATTRIBUTEEXCEPTION_H
#include <xylon/rt/UnknownAttributeException.h>
#endif

#ifndef _XYLON_RT_UNKNOWNATTRIBUTEGROUPEXCEPTION_H
#include <xylon/rt/UnknownAttributeGroupException.h>
#endif

#ifndef _XYLON_RT_UNKNOWNELEMENTEXCEPTION_H
#include <xylon/rt/UnknownElementException.h>
#endif

#ifndef _XYLON_RT_UNKNOWNGROUPEXCEPTION_H
#include <xylon/rt/UnknownGroupException.h>
#endif

#ifndef _XYLON_RT_UNKNOWNTYPEEXCEPTION_H
#include <xylon/rt/UnknownTypeException.h>
#endif

#ifndef _XYLON_RT_ELEMENTCURSOR_H
#include <xylon/rt/ElementCursor.h>
#endif

#include <memory>
#include <map>
#include <set>

namespace xylon {
   namespace rt {
      class QName;
      class Attribute;
      class AttributeGroup;
      class Element;
      class Group;
      class Type;

      /**
       * The schema provides access to types, elements, and attributes defined by an XML schema document. A schema is typically
       * a composite schema and contains the subset of an original XML schema document, which is necessary to parse a document
       * corresponding to the schema. <p>
       */
      class Schema
      {
            /** An object pointer */
         public:
            typedef ::std::unique_ptr< Object> ObjectPtr;

            /** Default constructor */
         protected:
            Schema() ;

            /** Destructor */
         public:
            virtual ~Schema()  = 0;

            /**
             * Get the attributes.
             * @return the top-level attributes in this schema.
             */
         public:
            virtual ::std::set< QName> attributes() const  = 0;

            /**
             * Get the attribute groups.
             * @return the top-level attribute groups in this schema.
             */
         public:
            virtual ::std::set< QName> attributeGroups() const  = 0;

            /**
             * Get the elements.
             * @return the top-level elements in this schema.
             */
         public:
            virtual ::std::set< QName> elements() const  = 0;

            /**
             * Get the groups.
             * @return the top-level groups in this schema.
             */
         public:
            virtual ::std::set< QName> groups() const  = 0;

            /**
             * Get the groups.
             * @return the top-level groups in this schema.
             */
         public:
            virtual ::std::set< QName> types() const  = 0;

            /**
             * Get the elements that can be substituted for the specified element. The returned collection
             * does <em>not</em> contain qname itself.
             * @param qname the name of an element
             * @return a set of names that can be substituted for the specified element
             */
         public:
            virtual ::std::set< QName> findSubstitutions(const QName& qname) const  = 0;

            /**
             * Find the specified attribute.
             * @param qname the name of an attribute
             * @return an attribute reference
             * @throws UnknownAttributeException if the attribute is not known to this schema
             */
         public:
            virtual const Attribute& findAttribute(const QName& qname) const  = 0;

            /**
             * Find the specified attribute group.
             * @param qname the name of an attribute group
             * @return an attribute group reference
             * @throws UnknownGroupException if the attribute group is not known to this schema
             */
         public:
            virtual const AttributeGroup& findAttributeGroup(const QName& qname) const
                  = 0;

            /**
             * Find the specified element.
             * @param qname the name of a element
             * @return a element reference
             * @throws UnknownElementException if the element is not known to this schema
             */
         public:
            virtual const Element& findElement(const QName& qname) const  = 0;

            /**
             * Find the specified group.
             * @param qname the name of a group
             * @return a group reference
             * @throws UnknownGroupException if the group is not known to this schema
             */
         public:
            virtual const Group& findGroup(const QName& qname) const = 0;

            /**
             * Find the specified type.
             * @param qname the name of a type
             * @return a type reference
             * @throws UnknownTypeException if the type is not known to this schema
             */
         public:
            virtual const Type& findType(const QName& qname) const  = 0;

            /**
             * Determine if the specified name is that of a known element. By default, this method uses
             * testElementSubstitution() to determine if the element is known.
             * @param name an element name
             * @return true if the element name is known
             */
         public:
            virtual bool hasElement(const QName& name) const ;

            /**
             * Determine if the specified types are base type and derived type. If <tt>baseType == derivedType</tt>
             * then this method returns false.<br>
             * If neither is known to this schema, then this method returns false.
             * @param baseType the name of a type
             * @param derivedType the name of type
             * @return true if derivedType is directly or indirectly derived from baseType.
             */
         public:
            virtual bool testTypeDerivation(const QName& baseType, const QName& derivedType) const  = 0;

            /**
             * Determine if an actual element can be substituted for an expected element. If <tt>actual==expected</tt> and they
             * denote known elements, then this method returns <tt>true</tt>.<br>
             * If either name denotes an unknown element, then this method returns false.
             * @param expected an expected element
             * @param actual an actual element
             * @return true <tt>actual</tt> is in the substitution group headed by <tt>expected</tt> or <tt>actual==expected</tt>
             */
         public:
            virtual bool testElementSubstitution(const QName& expected, const QName& actual) const  = 0;

            /**
             * Parse the specified element into an object. If the element is not a known top-level element then an exception is thrown.
             * @param cursor an element cursor
             * @return the parsed object
             * @throws ParseError if the element is not known to this schema or the element's content could not be parsed into an object
             */
         public:
            virtual ObjectPtr
            parseElement(ElementCursor& cursor) const ;

            /**
             * Parse the specified element into an object. If the element is not in the substitution group headed by the expected element
             * then an parse error is thrown.
             * @param expected the name of the expected element.
             * @param cursor an element cursor
             * @return the parsed object
             * @throws ParseError if the element is not known to this schema or the element's content could not be parsed into an object
             */
         public:
            virtual ObjectPtr
            parseElement(const QName& expected, ElementCursor& cursor) const ;

            /**
             * Parse the specified element into an object. If the element is not a known top-level element then an exception is thrown.
             * @param cursor an element cursor
             * @return the parsed object
             * @throws ParseError if the element is not known to this schema or the element's content could not be parsed into an object
             */
         public:
            template<class T>
            inline ::std::unique_ptr< T> parseElement(::std::unique_ptr< ElementCursor> cursor) const
            {
               ObjectPtr ptr = parseElement(*cursor);
               T* tPtr = dynamic_cast< T*> (ptr.get());
               ::std::unique_ptr< T> res(tPtr);
               if (res.get()) {
                  ptr.release();
               }
               return res;
            }

            /**
             * Parse the specified element into an object. If the element is not in the substitution group headed by the expected element
             * then an parse error is thrown.
             * @param expected the name of the expected element.
             * @param cursor an element cursor
             * @return the parsed object
             * @throws ParseError if the element is not known to this schema or the element's content could not be parsed into an object
             */
         public:
            template<class T>
            ::std::unique_ptr< T> parseElement(const QName& expected, ::std::unique_ptr< ElementCursor> cursor) const

            {
               ObjectPtr ptr = parseElement(expected, *cursor);
               T* tPtr = dynamic_cast< T*> (ptr.get());
               ::std::unique_ptr< T> res(tPtr);
               if (res.get()) {
                  ptr.release();
               }
               return res;
            }
      };

   }

}

#endif
