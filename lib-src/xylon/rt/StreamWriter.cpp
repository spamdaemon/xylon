#include <xylon/rt/StreamWriter.h>
#include <xylon/rt/QName.h>

#include <sstream>
#include <iostream>

using namespace ::std;

namespace xylon {
   namespace rt {

      StreamWriter::StackItem::StackItem()  :
         attribute(false), hasContent(false), noNewline(false)
      {
      }

      StreamWriter::NamespacePrefix::NamespacePrefix()  :
         active(false)
      {
      }

      /** A destructor */
      StreamWriter::NamespacePrefix::~NamespacePrefix() 
      {
      }

      StreamWriter::StreamWriter(ostream& out)  :
         _stream(out), _nextPrefix(0), _prefixStem("ns")
      {
         // this item always resides on the stack
         StackItem item;
         item.hasContent = true;
         _stack.push_back(item);
      }

      StreamWriter::~StreamWriter() 
      {
         _stream << flush;
      }

      void StreamWriter::setIndent(const ::std::string& indent) 
      {
         _indentString = indent;

         if (indent.empty()) {
            _indent = indent;
         }
         else {
            _indent = "";
            for (size_t i = 1; i < _stack.size(); ++i) {
               _indent += _indent;
            }
         }
      }

      void StreamWriter::doIndent() 
      {
         if (_indentString.length() == 0) {
            // no indenting wanted
            return;
         }

         size_t n = 0;
         if (_stack.size() > 1) {
            n = _stack.size() - 2;
         }
         size_t len = n * _indentString.length();
         if (_indent.length() > len) {
            _indent = string(_indent, 0, len);
         }
         else
            while (_indent.length() < len) {
               _indent += _indentString;
            }
         _stream << endl << _indent;
      }

      String StreamWriter::generatePrefix(const String& ns) 
      {
         assert(ns);
         // allocate a namespace
         NamespacePrefix& nsPrefix = _namespaces[ns];
         // do not check the use-count, only if the prefix is valid
         if (!nsPrefix.prefix) {
            ostringstream tmp;
            tmp << _prefixStem << _nextPrefix;
            ++_nextPrefix;

            nsPrefix.prefix = tmp.str();
            _prefixes.insert(make_pair(nsPrefix.prefix, ns));
         }
         return nsPrefix.prefix;
      }

      void StreamWriter::setHasContent() 
      {
         // there is always an item on the stack!
         StackItem& item = _stack.back();
         if (!item.hasContent) {
            if (item.attribute) {
               endAttribute();
            }
            _stream << '>';
            item.hasContent = true;
         }
      }

      String StreamWriter::activateNamespace(const String& ns)
      {
         if (!ns) {
            return ns;
         }

         String prefix = generatePrefix(ns);

         // only if the namespace isn't already active do we need to take action
         NamespacePrefix& nsPrefix = _namespaces[ns];

         // if the prefix is aleady active, then we don't need to push the namespace
         if (nsPrefix.active) {
            return prefix;
         }

         StackItem& item = _stack.back();
         if (item.hasContent || item.attribute) {
            // cannot activate the namespace yet; wait until there is a child element
            item.neededNamespaces.insert(ns);
         }
         else {
            // here, we activate the namespace and remember that we need to deactivate later
            item.namespaces.insert(ns);
            nsPrefix.active = true;
            // emit the prefix
            _stream << " xmlns:" << prefix << "='" << ns << "' ";
         }

         return prefix;
      }

      String StreamWriter::deactivateNamespaces()
      {
         StackItem& item = _stack.back();
         String prefix;
         String ns = item.element.namespaceURI();
         if (ns) {
            prefix = _namespaces.find(ns)->second.prefix;
         }

         for (set< String>::iterator i = item.namespaces.begin(); i != item.namespaces.end(); ++i) {
            Namespaces::iterator j = _namespaces.find(*i);
            assert(j->second.active && "Namespace is not active");
            j->second.active = false;

         }
         item.namespaces.clear();
         return prefix;
      }

      String StreamWriter::addNamespace(const String& uri)
      {
         return activateNamespace(uri);
      }

      void StreamWriter::beginDocument()
      {
         _stream << "<?xml version='1.0' encoding='utf-8' ?>";
      }

      void StreamWriter::beginElement(const QName& qname)
      {
         // close the old stack item
         setHasContent();

         // create a new item on the stack
         StackItem item;
         item.element = qname;
         _stack.push_back(item);

         // allocate a prefix; it's going to have a use-count of 0
         String ns = qname.namespaceURI();
         String prefix;
         if (ns) {
            prefix = generatePrefix(ns);
         }

         doIndent();
         // emit the opening tag
         _stream << '<';
         if (prefix) {
            _stream << prefix << ":";
         }
         _stream << qname.name();

         // push the namespace onto the stack
         String tmp = activateNamespace(qname.namespaceURI());
         assert(tmp == prefix);

         // get the parent item
         StackItem& parent = _stack[_stack.size() - 2];
         parent.noNewline = false;

         // emit any namespace prefixes we need to open
         for (::std::set< String>::const_iterator i = parent.neededNamespaces.begin(); i
               != parent.neededNamespaces.end(); ++i) {
            activateNamespace(*i);
         }
      }

      void StreamWriter::endElement()
      {
         String prefix = deactivateNamespaces();
         StackItem item = _stack.back();

         // we've not yet started the item's content and are already closing it
         if (!item.hasContent) {
            _stream << "/>";
         }
         else {
            if (!item.noNewline) {
               doIndent();
            }
            _stream << "</";
            if (prefix) {
               _stream << prefix << ":";
            }
            _stream << item.element.name() << '>';
         }
         _stack.pop_back();
      }

      void StreamWriter::endDocument()
      {
         if (_stack.back().attribute) {
            endAttribute();
         }
         while (_stack.size() > 1) {
            endElement();
         }
         _stream << endl;
      }

      void StreamWriter::beginAttribute(const QName& attr)
      {
         if (_stack.back().attribute) {
            endAttribute();
         }

         if (_stack.size() == 1) {
            throw MarshalError("No document element");
         }
         if (_stack.back().hasContent) {
            throw MarshalError("Element content has already been started");
         }
         String prefix = activateNamespace(attr.namespaceURI());

         _stream << ' ';
         if (prefix) {
            _stream << prefix << ':';
         }

         _stream << attr.name() << "=\"";
         _stack.back().attribute = true;
      }

      void StreamWriter::endAttribute()
      {
         if (!_stack.back().attribute) {
            throw MarshalError("No attribute to end");
         }
         _stack.back().attribute = false;
         _stream << "\" ";
      }

      void StreamWriter::writeText(const String& text)
      {
         if (_stack.size() == 1) {
            throw MarshalError("No document element");
         }
         if (text) {
            if (!_stack.back().attribute) {
               setHasContent();
               _stack.back().noNewline = true;
            }
            _stream << text;
         }
      }

      String StreamWriter::writeQNameAttribute(const QName& attr, const QName& value)
      {
         if (_stack.back().attribute) {
            endAttribute();
         }
         // need to activate this before we begin the attribute or it won't take effect
         String valuePrefix = activateNamespace(value.namespaceURI());

         beginAttribute(attr);

         if (valuePrefix) {
            _stream << valuePrefix << ':' << value.name();
         }
         else {
            _stream << value.name();
         }
         endAttribute();
         return valuePrefix;
      }

   }

}
