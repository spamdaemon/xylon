#ifndef _XYLON_RT_H
#define _XYLON_RT_H

#ifndef _TIMBER_W3C_XML_XMLSTRING_H
#include <timber/w3c/xml/XMLString.h>
#endif

namespace xylon {

   /**
    *  This namespace contains xylon's runtime library.
    */
   namespace rt {

      /** The string type */
      typedef ::timber::w3c::xml::XMLString String;

   }
}

#endif
