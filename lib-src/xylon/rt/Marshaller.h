#ifndef _XYLON_RT_MARSHALLER_H
#define _XYLON_RT_MARSHALLER_H

#ifndef _XYLON_RT_MARSHALERROR_H
#include <xylon/rt/MarshalError.h>
#endif

#ifndef _XYLON_RT_H
#include <xylon/rt/rt.h>
#endif

namespace xylon {
   namespace rt {
      class QName;
      class ElementObject;
      class TypeObject;

      /**
       * This class is used marshal an XML object.
       */
      class Marshaller
      {
            /** Default constructor */
         protected:
            Marshaller() ;

            /** Destructor */
         public:
            virtual ~Marshaller()  = 0;

            /**
             * Marshal the object as an XML element.
             * @param element an object that corresponds to an XML element
             * @throw MarshalError if the object could not be marshaled
             */
         public:
            virtual void marshal(const ElementObject& obj) = 0;

            /**
             * Marshal a type object. This will generate an opening tag using the specified
             * name, but use the type to emit the content and the attributes.
             * @param name the qualified name of the element
             * @param obj a type object
             * @throw MarshalError if the object could not be marshaled
             */
         public:
            virtual void marshalElement(const QName& name, const TypeObject& obj) = 0;
      };

   }

}

#endif
