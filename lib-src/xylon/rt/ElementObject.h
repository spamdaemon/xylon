#ifndef _XYLON_RT_ELEMENTOBJECT_H
#define _XYLON_RT_ELEMENTOBJECT_H

#ifndef _XYLON_RT_OBJECT_H
#include <xylon/rt/Object.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This class is the base class for objects that are top-level elements.
       */
      class ElementObject : public Object
      {
            /** The default constructor */
         protected:
            ElementObject() ;

            /**
             * Copy constructor.
             * @param src the source any
             */
         protected:
            ElementObject(const ElementObject& src) ;

            /** The destructor */
         public:
            ~ElementObject() ;

            /** Copy operator */
         protected:
            ElementObject& operator=(const ElementObject&) ;

            /**
             * Clone this object
             * @return a deep clone of this object
             */
         public:
            virtual ElementObject* clone() const = 0;
      };

   }
}

#endif
