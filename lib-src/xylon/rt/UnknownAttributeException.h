#ifndef _XYLON_RT_UNKNOWNATTRIBUTEGEXCEPTION_H
#define _XYLON_RT_UNKNOWNATTRIBUTEGEXCEPTION_H

#ifndef _XYLON_RT_UNKNOWNQNAMEEXCEPTION_H
#include <xylon/rt/UnknownQNameException.h>
#endif

namespace xylon {
   namespace rt {

      /**
       * This exception is thrown when an unknown element is encountered by a schema.
       */
      class UnknownAttributeException : public UnknownQNameException
      {

            /**
             * Constructor.
             * @param qname name
             */
         public:
            UnknownAttributeException(const QName& qname) ;

            /**
             * Constructor.
             */
         public:
            UnknownAttributeException() ;
            /**
             * Destructor
             */
         public:
            ~UnknownAttributeException() ;
      };

   }
}
#endif
