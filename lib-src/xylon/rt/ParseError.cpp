#include <xylon/rt/ParseError.h>

namespace xylon {
   namespace rt {

      ParseError::ParseError()  :
         ::std::runtime_error("Parse error")
      {

      }

      ParseError::ParseError(const ::std::string& msg)  :
         ::std::runtime_error(msg)
      {

      }

      ParseError::~ParseError() 
      {

      }

   }
}
