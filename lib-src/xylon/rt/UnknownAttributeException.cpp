#include <xylon/rt/UnknownAttributeException.h>

namespace xylon {
   namespace rt {

      UnknownAttributeException::UnknownAttributeException(const QName& qname)  :
         UnknownQNameException(qname)
      {

      }

      UnknownAttributeException::UnknownAttributeException() 
      {

      }

      UnknownAttributeException::~UnknownAttributeException() 
      {

      }

   }
}
