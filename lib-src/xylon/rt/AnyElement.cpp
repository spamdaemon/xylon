#include <xylon/rt/AnyElement.h>
#include <xylon/rt/ElementCursor.h>

namespace xylon {
   namespace rt {
      AnyElement::AnyElement()  :
         _type(NONE)
      {
      }

      AnyElement::AnyElement(const AnyElement& a)  :
         ElementObject(a), _type(a._type)
      {
         if (a._element.get()) {
            _element = a._element->clone();
            assert(_element->isValid() && "AnyElement: Copied cursor is not valid");
         }
         else if (a._object.get()) {
            _object.reset(a._object->clone());
         }
      }

      AnyElement& AnyElement::operator=(const AnyElement& a) 
      {
         if (this != &a) {

            _element.reset(0);
            _object.reset(0);

            if (a._object.get() != 0) {
               _object.reset(a._object->clone());
            }
            else if (a._element.get()) {
               _element = a._element->clone();
               assert(_element->isValid() && "AnyElement: Copied cursor is not valid");
            }
            _type = a._type;
         }
         return *this;
      }

      AnyElement::~AnyElement() 
      {
      }

      AnyElement* AnyElement::clone() const
      {
         return new AnyElement(*this);
      }

      void AnyElement::write(Writer& w) const
      {
         if (_object.get()) {
            _object->write(w);
         }
         else if (_element.get()) {
            assert(_element->isValid() && "AnyElement: ElementCursor is not valid");
            _element->writeElement(w);
         }
      }

      void AnyElement::setObject(::std::unique_ptr< ElementObject> obj) 
      {
         _object = ::std::move(obj);
         _element.reset();
         _type = _object.get() ? PARSED : NONE;
      }

      ElementObject& AnyElement::object()
      {
         return *_object.get();
      }

      const ElementObject& AnyElement::object() const
      {
         return *_object.get();
      }

      const ElementCursor& AnyElement::element() const
      {
         return *_element;
      }
      void AnyElement::setElement(const ElementCursor& cursor) throws(::std::invalid_argument)
      {
         setElement(cursor.clone());
      }

      void AnyElement::setElement(::std::unique_ptr< ElementCursor> cursor) throws(::std::invalid_argument)
      {
         if (cursor.get() && !cursor->isValid()) {
            throw ::std::invalid_argument("Invalid cursor");
         }
         _element = ::std::move(cursor);
         _object.reset();
         _type = _element.get() ? UNPARSED : NONE;
      }

   }
}
