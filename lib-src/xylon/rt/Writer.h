#ifndef _XYLON_RT_WRITER_H
#define _XYLON_RT_WRITER_H

#ifndef _XYLON_RT_MARSHALERROR_H
#include <xylon/rt/MarshalError.h>
#endif

#ifndef _XYLON_RT_H
#include <xylon/rt/rt.h>
#endif

namespace xylon {
   namespace rt {
      class QName;

      /**
       * This class provides an interface that is used by the Marshaler to write an XML document.
       */
      class Writer
      {
            /** Default constructor */
         protected:
            Writer() ;

            /** Destructor */
         public:
            virtual ~Writer() = 0;

            /**
             * Add a namespace to this writer. The new namespace will apply to all the current element
             * and any child elements. If there is no current document or element, then this namespace
             * will be applied  to all other elements.<p>
             * A prefix will automatically be chosen for the uri.
             * @param uri the uri
             * @return the chosen prefix
             */
         public:
            virtual String addNamespace(const String& uri) = 0;

            /**
             * Begin a document.
             * @throw MarshalError if there is not current element
             */
         public:
            virtual void beginDocument() = 0;

            /**
             * End all open elements to close the document. If no elements
             * are open then this method does nothing.
             */
         public:
            virtual void endDocument() = 0;

            /**
             * Begin an element. If the element's namespace is not known yet, then it added via addNamespace().
             * @param qname the qualified name of the element.
             * @throw MarshalError if the object could not be marshaled
             */
         public:
            virtual void beginElement(const QName& qname) = 0;

            /**
             * End the current element.
             * @throw MarshalError if there is not current element
             */
         public:
            virtual void endElement() = 0;

            /**
             * Begin an attribute.
             * @param attr the attribute name
             */
         public:
            virtual void beginAttribute(const QName& attr) = 0;

            /**
             * Begin an attribute.
             * @param attr the attribute name
             */
         public:
            virtual void endAttribute() = 0;

            /**
             * Write text into the current element or attribute. If the text is null,
             * then the no text is written.
             * @param value a text value
             */
         public:
            virtual void writeText(const String& text) = 0;

            /**
             * Write an attribute. This method is equivalent to
             * @code
             *   beginAttribute(attr);
             *   writeText(value);
             *   endAttribute();
             * @endcode
             * @param attr the name of the attribute
             * @param value the value to write
             * @throw MarshalError if the object could not be marshaled
             */
         public:
            virtual void writeAttribute(const QName& attr, const String& value);

            /**
             * Write a QName attribute. The namespace of the value is replaced by a prefix that is mapped
             * to the namespace.<p>
             * The primary purpose of this method is to provide <tt>xsi:type</tt> support.
             * @param attr the name of the attribute
             * @param value a qname
             * @return the mapped prefix for the value's namespace
             * @throw MarshalError if the object could not be marshaled
             */
         public:
            virtual String writeQNameAttribute(const QName& attr, const QName& value) = 0;
      };

   }

}

#endif
