#include <xylon/rt/Parser.h>
#include <xylon/rt/QNames.h>
#include <xylon/rt/Instruction.h>
#include <xylon/rt/ElementCursor.h>
#include <xylon/rt/ParserState.h>

namespace xylon {
   namespace rt {

      Parser::Parser() 
      {
      }

      Parser::~Parser() 
      {
      }

      void Parser::registerNames(const Instruction& i, const QNames& names) 
      {
#if 0
         _names[i] = names;
#endif
      }

      void Parser::executeNextInstruction(const Instruction& cur, ParserState& ps, const QName& name) const
      {
#if 0
         ::std::map< Instruction, QNames>::const_iterator i = _names.find(cur);
         if (i == _names.end()) {
            throw ParseError("Invalid current instruction");
         }
         return i->second.findInstruction(name);
#endif
      }

   }

}
