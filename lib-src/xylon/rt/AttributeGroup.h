#ifndef _XYLON_RT_ATTRIBUTEGROUP_H
#define _XYLON_RT_ATTRIBUTEGROUP_H

#ifndef _XYLON_RT_TOPLEVELNODE_H
#include <xylon/rt/TopLevelNode.h>
#endif

#ifndef _XYLON_RT_PARSEERROR_H
#include <xylon/rt/ParseError.h>
#endif

#ifndef _XYLON_RT_OBJECT_H
#include <xylon/rt/Object.h>
#endif

#include <memory>

namespace xylon {
   namespace rt {
      class ParserState;

      /**
       * This class represents an XML element and any information needed to parse the element. The class is abstract
       * requiring subclassing.
       */
      class AttributeGroup : public TopLevelNode
      {
            /** An object pointer */
         public:
            typedef ::std::unique_ptr< Object> ObjectPtr;

            /** Default constructor */
         protected:
            AttributeGroup() ;

            /** Destructor */
         public:
            virtual ~AttributeGroup()  = 0;

            /**
             * Create an instance of the C++ class to which this node type is mapped.
             * @return an instance of the mapped type
             * @throw AbstractTypeException if the type is abstract
             */
         public:
            virtual ObjectPtr newInstance() const  = 0;

            /**
             * Clone this node.
             * @return a copy of this node
             */
         public:
            virtual ::std::unique_ptr<AttributeGroup> clone() const  = 0;

            /**
             * Create and parse an object represented by this class. All information needed
             * to parse this object is provided in the form of a ParserState.<p>
             * Upon a successfully parsing this type, the created object will be on top of the ParserState's stack.
             * @param ps a parser state
             * @throw ParseError if the attribute  group cannot be parsed.
             */
         public:
            virtual void parse(ParserState& ps) const = 0;
      };

   }

}

#endif
