#ifndef _XYLON_XSDOM_TYPE_H
#define _XYLON_XSDOM_TYPE_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
   namespace xsdom {
      /**
       * A schema node. This is the base class for all
       * schema elements.
       */
      class Type : public Node
      {

            /** The default constructor */
         public:
            Type() throws();

            /** Destructor */
         public:
            ~Type() throws();

             /**
             * Get the qualified name for this element
             * @return the qualified name for this element
             */
         public:
            virtual QName qname() const throws() = 0;

            /**
             * Determine if this complex type derives from the given type through an extension or restriction.
             * @param baseType the name of a base type
             * @return true if this type extends or restricts the specified type.
             */
            virtual bool isSubtypeOf (const QName& baseType) const throws() = 0;

      };
   }
}
#endif
