#include <xylon/xsdom/Sequence.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Sequence::Sequence() throws()
    {}
		      	
    Sequence::~Sequence() throws()
    {}

    void Sequence::accept(Visitor& visitor)
    {
      visitor.visitSequence(toRef<Sequence>());
    }

  }
}
