#ifndef _XYLON_XSDOM_SIMPLETYPE_H
#define _XYLON_XSDOM_SIMPLETYPE_H

#ifndef _XYLON_XSDOM_TYPE_H
#include <xylon/xsdom/Type.h>
#endif

namespace xylon {
   namespace xsdom {
      /**
       * A schema node. This is the base class for all
       * schema elements.
       */
      class SimpleType : public Type
      {

            /** Whether extensions or restrictions are blocked */
         public:
            enum Final
            {
               FINAL_NONE, FINAL_LIST, FINAL_RESTRICTION, FINAL_UNION, FINAL_ALL
            };

            /** The default constructor */
         public:
            SimpleType() throws();

            /** Destructor */
         public:
            ~SimpleType() throws();

            /**
             * Accept a node visitor
             * @param visitor a visitor
             */
         public:
            void accept(Visitor& visitor);

            /**
             * Indicate that this type is a restriction
             * @param res a restriction
             */
         public:
            virtual ::timber::Pointer< Restriction> restriction() const throws() = 0;

            /**
             * Indicate that this type is a list
             * @param res a list
             */
         public:
            virtual ::timber::Pointer< List> list() const throws() = 0;

            /**
             * Indicate that this type is a union
             * @param res a union
             */
         public:
            virtual ::timber::Pointer< Union> getUnion() const throws() = 0;

            /**
             * Determine if how this type can be derived.
             * @return the type of derivation allowed
             */
         public:
            virtual int final() const throws() = 0;

            /**
             * Determine if this complex type derives from the given type through an extension.
             * @param baseType the name of a base type
             * @return true if this type extends the specified type.
             */
            bool isSubtypeOf(const QName& baseType) const throws();

      };
   }
}
#endif
