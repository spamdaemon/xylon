#include <xylon/xsdom/Documentation.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Documentation::Documentation() throws()
    {}
		      	
    Documentation::~Documentation() throws()
    {}

    void Documentation::accept(Visitor& visitor)
    {
      visitor.visitDocumentation(toRef<Documentation>());
    }

  }
}
