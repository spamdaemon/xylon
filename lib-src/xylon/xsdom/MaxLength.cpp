#include <xylon/xsdom/MaxLength.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    MaxLength::MaxLength() throws()
    {}
		      	
    MaxLength::~MaxLength() throws()
    {}

    void MaxLength::accept(Visitor& visitor)
    {
      visitor.visitMaxLength(toRef<MaxLength>());
    }

  }
}
