#include <xylon/xsdom/MinInclusive.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    MinInclusive::MinInclusive() throws()
    {}
		      	
    MinInclusive::~MinInclusive() throws()
    {}

    void MinInclusive::accept(Visitor& visitor)
    {
      visitor.visitMinInclusive(toRef<MinInclusive>());
    }

  }
}
