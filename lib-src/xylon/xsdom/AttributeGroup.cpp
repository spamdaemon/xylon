#include <xylon/xsdom/AttributeGroup.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    AttributeGroup::AttributeGroup() throws()
    {}
		      	
    AttributeGroup::~AttributeGroup() throws()
    {}

    void AttributeGroup::accept(Visitor& visitor)
    {
      visitor.visitAttributeGroup(toRef<AttributeGroup>());
    }

  }
}
