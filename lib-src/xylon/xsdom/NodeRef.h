#ifndef _XYLON_XSDOM_NODEREF_H
#define _XYLON_XSDOM_NODEREF_H

#ifndef _XYLON_GLOBALNAME_H
#include <xylon/GlobalName.h>
#endif


namespace xylon {
  namespace xsdom {

    /**
     * A NodeRef can be used to index nodes by their qualified names. Unfortunately,
     * different types of nodes can have the same qualified names and so an extra node
     * type must be used  to distinguish such qualified names.
     */
    typedef GlobalName NodeRef;
  }
}
#endif
