#ifndef _XYLON_XSDOM_EXTENSION_H
#define _XYLON_XSDOM_EXTENSION_H

#ifndef _XYLON_XSDOM_DERIVATION_H
#include <xylon/xsdom/Derivation.h>
#endif

namespace xylon {
   namespace xsdom {
      /**
       * A schema node. This is the base class for all
       * schema elements.
       */
      class Extension : public Derivation
      {

            /** The default constructor */
         public:
            Extension() throws();

            /** Destructor */
         public:
            ~Extension() throws();

            /**
             * Accept a node visitor
             * @param visitor a visitor
             */
         public:
            void accept(Visitor& visitor);

            /**
             * Get the enumerable. If is possible, that null is returned, because
             * using an enumerable is just short-hand for a restriction.
             * @return the enumerable
             */
         public:
            virtual ::timber::Pointer< Enumerable> enumerable() const throws() = 0;

            /**
             * Get the any attribute
             * @return the any attribute
             */
         public:
            virtual ::timber::Pointer< AnyAttribute> anyAttribute() const throws() = 0;

            /**
             * Set the attributes defined by this group.
             * @param attrs the attributes
             */
         public:
            virtual const ::std::vector< ::timber::Reference< Attribute> >& attributes() const throws() =0;

            /**
             * Set the attributes or attribute groups. Each node must be an attribute
             * or attribute group.
             * @param grps the contained attribute groups (they will be references)
             */
         public:
            virtual const ::std::vector< ::timber::Reference< AttributeGroup> >& attributeGroups() const throws() =0;
      };
   }
}
#endif
