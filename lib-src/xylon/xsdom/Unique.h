#ifndef _XYLON_XSDOM_UNIQUE_H
#define _XYLON_XSDOM_UNIQUE_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Unique : public Node {
      
      /** The default constructor */
    public:
      Unique() throws();
      
      /** Destructor */
    public:
      ~Unique() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
