#ifndef _XYLON_XSDOM_COMPOSITOR_H
#define _XYLON_XSDOM_COMPOSITOR_H

#ifndef _XYLON_XSDOM_ENUMERABLE_H
#include <xylon/xsdom/Enumerable.h>
#endif

#include <vector>

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Compositor : public Enumerable {
      
      /** The default constructor */
    public:
      Compositor() throws();
      
      /** Destructor */
    public:
      ~Compositor() throws();

      /**
       * The vector of enumerables
       * @get the enumerables
       */
    public:
      virtual const ::std::vector< ::timber::Reference<Enumerable> >& items() const throws() = 0;
    };
  }
}
#endif
