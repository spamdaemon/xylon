#include <xylon/xsdom/Include.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Include::Include() throws()
    {}
		      	
    Include::~Include() throws()
    {}

    void Include::accept(Visitor& visitor)
    {
      visitor.visitInclude(toRef<Include>());
    }

  }
}
