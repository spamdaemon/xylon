#include <xylon/xsdom/Occurrence.h>

namespace xylon {
   namespace xsdom {
      namespace {
         // safely compute the multiplication of two occurs values
         static size_t multiplyOccurs(size_t a, size_t b) throws( ::std::exception)
         {
            if (a == Occurrence::UNBOUNDED || b == Occurrence::UNBOUNDED) {
               return Occurrence::UNBOUNDED;
            }

            if (a == 0 || (Occurrence::UNBOUNDED / a) >= b) {
               // it's safe to make this
               return a * b;
            }
            return Occurrence::UNBOUNDED;
         }
      }

      const size_t Occurrence::UNBOUNDED;

      Occurrence::Occurrence(size_t mn, size_t mx) throws() :
         _min(::std::min(mn, mx)), _max(::std::max(mn, mx))
      {
      }

      Occurrence::Occurrence() throws() :
         _min(1), _max(1)
      {
      }

      Occurrence Occurrence::optional() throws()
      {
         return Occurrence(0, 1);
      }

      Occurrence Occurrence::required() throws()
      {
         return Occurrence(1, 1);
      }
      Occurrence Occurrence::prohibited() throws()
      {
         return Occurrence(0, 0);
      }
      Occurrence Occurrence::unbounded() throws()
      {
         return Occurrence(0, UNBOUNDED);
      }

      Occurrence Occurrence::multiply(size_t minMult, size_t maxMult) const throws()
      {
         return Occurrence(multiplyOccurs(_min, minMult), multiplyOccurs(_max, maxMult));
      }

      Occurrence::Multiplicity Occurrence::multiplicity() const throws()
      {
         if (_max == 0) {
            return ZERO;
         }
         if (_min == 0 && _max > 1) {
            return ZERO_OR_MORE;
         }
         else if (_min == 0 && _max == 1) {
            return ZERO_OR_ONE;
         }
         else if (_min == 1 && _max == 1) {
            return ONE;
         }
         else {
            // default for anything else is ZERO_OR_MORE (actually, 1 or more, but we don't have that
            return ZERO_OR_MORE;
         }
      }
   }
}
