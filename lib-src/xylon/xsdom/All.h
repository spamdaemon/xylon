#ifndef _XYLON_XSDOM_ALL_H
#define _XYLON_XSDOM_ALL_H

#ifndef _XYLON_XSDOM_COMPOSITOR_H
#include <xylon/xsdom/Compositor.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class All : public Compositor {
      
      /** The default constructor */
    public:
      All() throws();
      
      /** Destructor */
    public:
      ~All() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      
    };
  }
}
#endif
