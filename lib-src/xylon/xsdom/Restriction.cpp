#include <xylon/xsdom/Restriction.h>
#include <xylon/xsdom/SimpleType.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>

namespace xylon {
   namespace xsdom {

      Restriction::Restriction() throws()
      {
      }

      Restriction::~Restriction() throws()
      {
      }

      void Restriction::accept(Visitor& visitor)
      {
         visitor.visitRestriction(toRef< Restriction> ());
      }

      bool Restriction::derivesFromType(const QName& name) const throws()
      {
         if (base()) {
            return Derivation::derivesFromType(name);
         }
         else if (simpleType()) {
            return simpleType()->isSubtypeOf(name);
         }
         else {
            return false;
         }
      }

   }
}
