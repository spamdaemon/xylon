#ifndef _XYLON_XSDOM_NODE_H
#define _XYLON_XSDOM_NODE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _XYLON_XSDOM_H
#include <xylon/xsdom/xsdom.h>
#endif

#ifndef _XYLON_XREF_H
#include <xylon/XRef.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

namespace xylon {
  namespace xsdom {
    class Visitor;
    class All;
    class Annotation;
    class Any;
    class AnyAttribute;
    class AppInfo;
    class Attribute;
    class AttributeGroup;
    class Choice;
    class ComplexContent;
    class ComplexType;
    class Documentation;
    class Element;
    class Extension;
    class Field;
    class Group;
    class Import;
    class Include;
    class Key;
    class KeyRef;
    class List;
    class Notation;
    class Redefine;
    class Restriction;
    class Schema;
    class Selector;
    class Sequence;
    class SimpleContent;
    class SimpleType;
    class Union;
    class Unique;
    class Enumeration;
    class FractionDigits;
    class Length;
    class MaxExclusive;
    class MaxInclusive;
    class MaxLength;
    class MinExclusive;
    class MinInclusive;
    class MinLength;
    class Pattern;
    class TotalDigits;
    class WhiteSpace;

    class Type;
    class Derivation;
    class Enumerable;
    class ContentType;
    class Facet;

    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Node : public ::timber::Counted {
      
      /** The default constructor */
    public:
      Node() throws();
      
      /** Destructor */
    public:
      virtual ~Node() throws() = 0;

      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      virtual void accept(Visitor& visitor) = 0;

      /**
       * Get the annotation for this node. 
       * @return the annotation
       */
    public:
      virtual ::timber::Pointer<Annotation> annotation() const throws();

    };
  }
}
#endif
