#include <xylon/xsdom/xsdom.h>
#include <xylon/xsdom/nodes.h>
#include <xylon/XsdSchema.h>
#include <set>

namespace xylon {
   namespace xsdom {
      namespace {
         template<class T> void addNodes(const ::std::vector< XRef< T> >& v,
               ::std::vector< ::timber::Reference< Node> >& nodes)
         {
            for (size_t i = 0; i < v.size(); ++i) {
               nodes.push_back(v[i].target());
            }
         }

      }
      const char* XSD_NAMESPACE_URI = "http://www.w3.org/2001/XMLSchema";

      ::std::vector< ::timber::Reference< Node> > getSchemaNodes(const ::timber::Reference< ::xylon::XsdSchema>& s) throws()
      {
         ::std::vector< ::timber::Reference< Node> > nodes;
         addNodes(s->attributeGroups(), nodes);
         addNodes(s->attributes(), nodes);
         addNodes(s->elements(), nodes);
         addNodes(s->groups(), nodes);
         addNodes(s->types(), nodes);
         return nodes;
      }

      ::timber::Reference< Any> createAny() throws()
      {
         return createAny(Occurrence::required());
      }

      ::timber::Reference< Any> createAny(const Occurrence& occurs) throws()
      {
         struct Impl : public Any
         {
               Impl(const Occurrence& xoccurs) throws() :
                  _tns(XSD_NAMESPACE_URI), _occurs(xoccurs)
               {
                  _ns.insert("##any");
               }
               ~Impl() throws()
               {
               }

               String targetNamespace() const throws()
               {
                  return _tns;
               }

               const ::std::set< String>& namespaces() const throws()
               {
                  return _ns;
               }

               ProcessContents getProcessContents() const throws()
               {
                  return SKIP;
               }
               Occurrence occurs() const throws()
               {
                  return _occurs;
               }

               /** The target namespace */
            private:
               const String _tns;

               /** The namespaces: this is ##any */
            private:
               ::std::set< String> _ns;

               /** The occurence */
            private:
               const Occurrence _occurs;
         };

         Any* obj = new Impl(occurs);
         return obj;
      }

   }
}
