#include <xylon/xsdom/Enumerable.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {
    
    const size_t Enumerable::UNBOUNDED;

    Enumerable::Enumerable() throws()
    {}
		      	
    Enumerable::~Enumerable() throws()
    {}

  }
}
