#ifndef _XYLON_XSDOM_FIELD_H
#define _XYLON_XSDOM_FIELD_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Field : public Node {
      
      /** The default constructor */
    public:
      Field() throws();
      
      /** Destructor */
    public:
      ~Field() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Get the XPath expression
       * @return the expath expression
       */
    public:
      virtual  String xpath() const throws() = 0;
    };
  }
}
#endif
