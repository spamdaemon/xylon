#ifndef _XYLON_XSDOM_ANY_H
#define _XYLON_XSDOM_ANY_H

#ifndef _XYLON_XSDOM_ENUMERABLE_H
#include <xylon/xsdom/Enumerable.h>
#endif

#include <set>

namespace xylon {
   namespace xsdom {
      /**
       * This node is used to represent the xs:any element tag.<br>
       * Note: in order to support the ##local tag, a null namespace is added.
       */
      class Any : public Enumerable
      {

            /** How to process the contents */
         public:
            enum ProcessContents
            {
               LAX, SKIP, STRICT
            };

            /**
             * Create a new Any object.
             * @param tns the namespace of the schema in which this node appears.
             */
         protected:
            Any() throws();

            /** Destructor */
         public:
            ~Any() throws();

            /**
             * Accept a node visitor
             * @param visitor a visitor
             */
         public:
            void accept(Visitor& visitor);

            /**
             * Get the target namespace in which this any instance
             * is defined.
             * @return the namespace in which this object is defined
             */
         public:
            virtual String targetNamespace() const throws() = 0;

            /**
             * Get the namespace
             * @return the namespace that should be matched.
             */
         public:
            virtual const ::std::set< String>& namespaces() const throws() = 0;

            /**
             * Get how contents should be processed.
             * @return how to process contents
             */
         public:
            virtual ProcessContents getProcessContents() const throws() = 0;

            /**
             * Check if the specified namespace is accepted by this any object.
             * @param ns a namespace uri
             * @return true if ns matches an valid namespace
             */
         public:
            virtual bool acceptsNamespace(const String& ns) const throws();

            /**
             * Determine if this xs:any instance conflicts with the specified instance.
             * A conflict occurs, when there exists at least one element that would
             * be accepted by both instances.
             *
             * @param any another any
             * @return true if there is at least element that can be accepted by both.
             */
         public:
            virtual bool conflictsWith(const Any& any) const throws();
      };
   }
}
#endif
