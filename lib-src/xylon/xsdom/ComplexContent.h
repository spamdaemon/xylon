#ifndef _XYLON_XSDOM_COMPLEXCONTENT_H
#define _XYLON_XSDOM_COMPLEXCONTENT_H

#ifndef _XYLON_XSDOM_CONTENTTYPE_H
#include <xylon/xsdom/ContentType.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class ComplexContent : public ContentType {
      
      /** The default constructor */
    public:
      ComplexContent() throws();
      
      /** Destructor */
    public:
      ~ComplexContent() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Determine if the content can be mixed.
       * @return true if the content can be mixed
       */
    public:
      virtual bool isMixed() const throws() = 0;
    };
  }
}
#endif
