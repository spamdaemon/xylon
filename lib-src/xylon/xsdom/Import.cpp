#include <xylon/xsdom/Import.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Import::Import() throws()
    {}
		      	
    Import::~Import() throws()
    {}

    void Import::accept(Visitor& visitor)
    {
      visitor.visitImport(toRef<Import>());
    }

  }
}
