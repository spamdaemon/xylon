#ifndef _XYLON_XSDOM_SEQUENCE_H
#define _XYLON_XSDOM_SEQUENCE_H

#ifndef _XYLON_XSDOM_COMPOSITOR_H
#include <xylon/xsdom/Compositor.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Sequence : public Compositor {
      
      /** The default constructor */
    public:
      Sequence() throws();
      
      /** Destructor */
    public:
      ~Sequence() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
