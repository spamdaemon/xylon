#ifndef _XYLON_XSDOM_SCHEMA_H
#define _XYLON_XSDOM_SCHEMA_H

#ifndef _TIMBER_W3C_URI_H
#include <timber/w3c/URI.h>
#endif

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

#ifndef _XYLON_XREF_H
#include <xylon/XRef.h>
#endif

#ifndef _XYLON_XSDOM_NODEREF_H
#include <xylon/xsdom/NodeRef.h>
#endif

#include <vector>
#include <map>
#include <set>

namespace xylon {
  namespace xsdom {
    class Import;

    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Schema : public Node {

      /** The form default value */
    public:
      enum Form {
	FORM_QUALIFIED,
	FORM_UNQUALIFIED
      };

      enum Block {
	BLOCK_NONE = 0,
	BLOCK_EXTENSION = 1,
	BLOCK_RESTRICTION = 2,
	BLOCK_SUBSTITUTION = 4,
	BLOCK_ALL = 7
      };

     /** Whether extensions or restrictions are final */
    public:
      enum Final {
	FINAL_NONE = 0,
	FINAL_EXTENSION = 1,
	FINAL_RESTRICTION = 2,
	FINAL_ALL = 3
      };

      /** The default constructor */
    public:
      Schema() throws();
      
      /** Destructor */
    public:
      ~Schema() throws();

      /**
       * Create a schema by parsing a xsd schema file.
       * @param file the schema file
       */
    public:
      static ::timber::Reference<Schema> create (const ::timber::SharedRef< ::timber::w3c::URI>& uri) throws (::std::exception);

      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Get the default block
       * @return the block default
       */
    public:
      virtual int blockDefaults() const throws() = 0;

      /**
       * Get the default block
       * @return the block default
       */
    public:
      virtual int finalDefaults() const throws() = 0;

      /**
       * Get the target namespace
       * @return the targetnamespace
       */
    public:
      virtual String targetNamespace() const throws() = 0;
      
      /**
       * Get the schema version
       * @return the schema version
       */
    public:
      virtual String version() const throws() = 0;

      /**
       * Get the schema language
       * @return the schema language
       */
    public:
      virtual String language() const throws() = 0;

       /**
       * Get the form.
       * @return the prefix form 
       */
    public:
      virtual Form attributeFormDefault() const throws() = 0;

       /**
       * Get the form.
       * @return the prefix form 
       */
    public:
      virtual Form elementFormDefault() const throws() = 0;

      /**
       * @defgroup TopLevelTypes Accessors for top-level XS types.
       * @{
       */

      /**
       * Get the list of top-level attributes
       * @return a list of top-level attributes
       */
    public:
      virtual ::std::vector<AttributeXRef > attributes() const throws() = 0;

      /**
       * Get the list of top-level attribute groups
       * @return a list of top-level attribute groups
       */
    public:
      virtual ::std::vector<AttributeGroupXRef > attributeGroups() const throws() = 0;

      /**
       * Get the list of top-level elements
       * @return a list of top-level element references
       */
    public:
      virtual ::std::vector<ElementXRef > elements() const throws() = 0;

      /**
       * Get the list of top-level groups
       * @return a list of top-level groups references
       */
    public:
      virtual ::std::vector<GroupXRef > groups() const throws() = 0;

      /**
       * Get the list of top-level type references
       * @return a list of top-level types references
       */
    public:
      virtual ::std::vector<TypeXRef > types() const throws() = 0;

      /*@}*/

      /**
       * Get the import nodes.
       * @return the a list of imported schemas 
       */
    public:
      virtual ::std::vector< ::timber::Reference<Import> > imports() const  throws() = 0;

      /**
       * Get the nodes that appear at the top-level.
       * @note the returned array is a copy.
       * @param nodes the nodes
       */
    public:
      virtual ::std::vector< ::timber::Reference<Node> > nodes () const throws() = 0;

      /**
       * Get the parent for the specified node.
       * @param node a node
       * @return the parent node, or null if this schema does not define parent child relations
       */
    public:
      virtual ::timber::Pointer<Node> parentOf (const ::timber::Reference<Node>& node) const throws() = 0;

      /**
       * Determine if the specified node is atop-level node. A top-level node is node
       * hanging just off this schema.
       * @param n a node
       * @return true if the specified node is a top-level node.
       */
    public:
      virtual bool isTopLevelNode(const ::timber::Reference<Node>& node) const throws() = 0;

      /**
       * Find node by its node ref
       * @param ref the node ref
       * @return the node or null if not found
       */
    public:
      virtual ::timber::Pointer<Node> findNode (const NodeRef& name) const throws() = 0;

      /*
       * @defgroup Find. Locate top-level objects in this schema.
       * @{
       */

      /**
       * Find an type
       * @param qname the name of the type to search for
       * @return a pointer to an type or 0 if not found
       */
    public:
      virtual ::timber::Pointer<Type> findType (const QName& qname) const throws() = 0;

      /**
       * Find an group.
       * @param qname the name of the group to search for
       * @return a pointer to an group or 0 if not found
       */
    public:
      virtual ::timber::Pointer<Group> findGroup (const QName& qname) const throws() = 0;

      /**
       * Find an attribute group.
       * @param qname the name of the attribute group to search for
       * @return a pointer to an attribute group or 0 if not found
       */
    public:
      virtual ::timber::Pointer<AttributeGroup> findAttributeGroup (const QName& qname) const throws() = 0;

      /**
       * Find an group.
        * @param qname the name of the attribute to search for
       * @return a pointer to an attribute  or 0 if not found
       */
    public:
      virtual ::timber::Pointer<Attribute> findAttribute (const QName& qname) const throws() = 0;

      /**
       * Find an element
       * @param qname the name of the element to search for
       * @return a pointer to an element or 0 if not found
       */
    public:
      virtual ::timber::Pointer<Element> findElement (const QName& qname) const throws() = 0;

      /**
       * Find an element
       * @param qname the name of the element to search for
       * @return a pointer to an element or 0 if not found
       */
    public:
      virtual ::timber::Pointer<Key> findKey (const QName& qname) const throws() = 0;

      /*@}*/
    };
  }
}
#endif
