#ifndef _XYLON_XSDOM_IMPORT_H
#define _XYLON_XSDOM_IMPORT_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Import : public Node {
      
      /** The default constructor */
    public:
      Import() throws();
      
      /** Destructor */
    public:
      ~Import() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Get the namespace being imported.
       * @return the imported namespace
       */
    public:
      virtual String targetNamespace() const throws() = 0;

      /**
       * Get the URI from which the file may be imported.
       * @return a schema location.
       */
    public:
      virtual String schemaLocation() const throws() = 0;
    };
  }
}
#endif
