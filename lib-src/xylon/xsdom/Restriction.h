#ifndef _XYLON_XSDOM_RESTRICTION_H
#define _XYLON_XSDOM_RESTRICTION_H

#ifndef _XYLON_XSDOM_DERIVATION_H
#include <xylon/xsdom/Derivation.h>
#endif

namespace xylon {
   namespace xsdom {
      /**
       * A schema node. This is the base class for all
       * schema elements.
       */
      class Restriction : public Derivation
      {

            /** The default constructor for a non simple type restriction */
         public:
            Restriction() throws();

            /** Destructor */
         public:
            ~Restriction() throws();

            /**
             * Accept a node visitor
             * @param visitor a visitor
             */
         public:
            void accept(Visitor& visitor);

            /**
             * Determine if this restriction was defined as part of a a simple type.
             * @return true if this restriction is a restriction of a simple type
             */
         public:
            virtual bool isSimpleTypeRestriction() const throws() = 0;

            /**
             * Get the simple type for this restriction.
             * @return the simple type on which this restriction is based
             */
         public:
            virtual ::timber::Pointer< SimpleType> simpleType() const throws() = 0;

            /**
             * Set the attributes defined by this group.
             * @param attrs the attributes
             */
         public:
            virtual const ::std::vector< ::timber::Reference< Facet> >& facets() const throws() = 0;

            /**
             * @name The complex restriction part
             * @{
             */

            /**
             * Get the enumerable. If is possible, that null is returned, because
             * using an enumerable is just short-hand for a restriction.
             * @return the enumerable
             */
         public:
            virtual ::timber::Pointer< Enumerable> enumerable() const throws() =0;

            /**
             * Get the any attribute
             * @return the any attribute
             */
         public:
            virtual ::timber::Pointer< AnyAttribute> anyAttribute() const throws() =0;

            /**
             * Set the attributes defined by this group.
             * @param attrs the attributes
             */
         public:
            virtual const ::std::vector< ::timber::Reference< Attribute> >& attributes() const throws() =0;

            /**
             * Set the attributes or attribute groups. Each node must be an attribute
             * or attribute group.
             * @param grps the contained attribute groups (they will be references)
             */
         public:
            virtual const ::std::vector< ::timber::Reference< AttributeGroup> >& attributeGroups() const throws()=0;

            bool derivesFromType(const QName& name) const throws();
            /*@}*/

      };
   }
}
#endif
