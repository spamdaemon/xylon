#include <xylon/xsdom/Visitor.h>

using namespace ::timber;

namespace xylon {
  namespace xsdom {

    Visitor::Visitor() throws() {}
    Visitor::~Visitor() throws() {}
    
    void Visitor::visitUrType(const Reference<UrType>&) {}
    void Visitor::visitAll(const Reference<All>&) {}
    void Visitor::visitAnnotation(const Reference<Annotation>&) {}
    void Visitor::visitAny(const Reference<Any>&) {}
    void Visitor::visitAnyAttribute(const Reference<AnyAttribute>&) {}
    void Visitor::visitAppInfo(const Reference<AppInfo>&) {}
    void Visitor::visitAttribute(const Reference<Attribute>&) {}
    void Visitor::visitAttributeGroup(const Reference<AttributeGroup>&) {}
    void Visitor::visitChoice(const Reference<Choice>&) {}
    void Visitor::visitComplexContent(const Reference<ComplexContent>&) {}
    void Visitor::visitComplexType(const Reference<ComplexType>&) {}
    void Visitor::visitDocumentation(const Reference<Documentation>&) {}
    void Visitor::visitElement(const Reference<Element>&) {}
    void Visitor::visitExtension(const Reference<Extension>&) {}
    void Visitor::visitField(const Reference<Field>&) {}
    void Visitor::visitGroup(const Reference<Group>&) {}
    void Visitor::visitImport(const Reference<Import>&) {}
    void Visitor::visitInclude(const Reference<Include>&) {}
    void Visitor::visitKey(const Reference<Key>&) {}
    void Visitor::visitKeyRef(const Reference<KeyRef>&) {}
    void Visitor::visitList(const Reference<List>&) {}
    void Visitor::visitNotation(const Reference<Notation>&) {}
    void Visitor::visitRedefine(const Reference<Redefine>&) {}
    void Visitor::visitRestriction(const Reference<Restriction>&) {}
    void Visitor::visitSchema(const Reference<Schema>&) {}
    void Visitor::visitSelector(const Reference<Selector>&) {}
    void Visitor::visitSequence(const Reference<Sequence>&) {}
    void Visitor::visitSimpleContent(const Reference<SimpleContent>&) {}
    void Visitor::visitSimpleType(const Reference<SimpleType>&) {}
    void Visitor::visitUnion(const Reference<Union>&) {}
    void Visitor::visitUnique(const Reference<Unique>&) {}
    void Visitor::visitEnumeration(const Reference<Enumeration>&) {}
    void Visitor::visitFractionDigits(const Reference<FractionDigits>&) {}
    void Visitor::visitLength(const Reference<Length>&) {}
    void Visitor::visitMaxExclusive(const Reference<MaxExclusive>&) {}
    void Visitor::visitMaxInclusive(const Reference<MaxInclusive>&) {}
    void Visitor::visitMaxLength(const Reference<MaxLength>&) {}
    void Visitor::visitMinExclusive(const Reference<MinExclusive>&) {}
    void Visitor::visitMinInclusive(const Reference<MinInclusive>&) {}
    void Visitor::visitMinLength(const Reference<MinLength>&) {}
    void Visitor::visitPattern(const Reference<Pattern>&) {}
    void Visitor::visitTotalDigits(const Reference<TotalDigits>&) {}
    void Visitor::visitWhiteSpace(const Reference<WhiteSpace>&) {}

  }
}
