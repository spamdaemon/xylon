#ifndef _XYLON_XSDOM_ENUMERABLE_H
#define _XYLON_XSDOM_ENUMERABLE_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

#ifndef _XYLON_XSDOM_OCCURRENCE_H
#include <xylon/xsdom/Occurrence.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Enumerable : public Node {
      
      /** The unbounded value for maxOccurs */
    public:
      static const size_t UNBOUNDED = ~size_t(0);

      /** The default constructor */
    public:
      Enumerable() throws();
      
      /** Destructor */
    public:
      ~Enumerable() throws();

      /**
       * Get the min occurs for this enumerable.
       * @return the min occurs
       */
    public:
      virtual Occurrence occurs() const throws() = 0;
    };
  }
}
#endif
