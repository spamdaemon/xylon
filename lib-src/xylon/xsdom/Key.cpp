#include <xylon/xsdom/Key.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Key::Key() throws()
    {}
		      	
    Key::~Key() throws()
    {}

    void Key::accept(Visitor& visitor)
    {
      visitor.visitKey(toRef<Key>());
    }

  }
}
