#ifndef _XYLON_XSDOM_MAXLENGTH_H
#define _XYLON_XSDOM_MAXLENGTH_H

#ifndef _XYLON_XSDOM_FACET_H
#include <xylon/xsdom/Facet.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class MaxLength : public Facet {
      
      /** The default constructor */
    public:
      MaxLength() throws();
      
      /** Destructor */
    public:
      ~MaxLength() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
