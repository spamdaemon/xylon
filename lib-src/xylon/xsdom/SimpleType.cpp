#include <xylon/xsdom/SimpleType.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/Restriction.h>

using namespace ::std;
using namespace ::timber;

namespace xylon {
   namespace xsdom {

      SimpleType::SimpleType() throws()
      {
      }

      SimpleType::~SimpleType() throws()
      {
      }

      void SimpleType::accept(Visitor& visitor)
      {
         visitor.visitSimpleType(toRef< SimpleType> ());
      }

      bool SimpleType::isSubtypeOf(const QName& name) const throws()
      {
         // a type cannot be a subtype of itself
         if (qname() == name) {
            return false;
         }
         // a type is always a subtype of anytype through restriction or extension.
         if (name == QName::xsQName("anyType")) {
            return true;
         }

         // a type is always a subtype of anytype through restriction or extension.
         if (name == QName::xsQName("anySimpleType")) {
            return true;
         }

         Pointer< Restriction> res = restriction();

         // by default, if no restriction is specified, then the type is not a subtype
         if (!res) {
            return false;
         }
         else {
            return res->derivesFromType(name);
         }
      }
   }
}
