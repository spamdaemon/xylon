#include <xylon/xsdom/Schema.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/nodes.h>

#include <vector>

using namespace ::std;
using namespace ::timber;

namespace xylon {
  namespace xsdom {

    Schema::Schema() throws()
    {}
		      	
    Schema::~Schema() throws()
    {}


    void Schema::accept(Visitor& visitor)
    {
      visitor.visitSchema(toRef<Schema>());
    }

    Reference<Schema> Schema::create (const ::timber::SharedRef< ::timber::w3c::URI>& uri) throws (::std::exception)
    { 
      //return SchemaLoader::loadSchema(uri);
      return Pointer<Schema>();
    }

  }
}
