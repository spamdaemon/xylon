#include <xylon/xsdom/Node.h>
#include <xylon/xsdom/Annotation.h>

using namespace ::std;

namespace xylon {
  namespace xsdom {

    Node::Node() throws()
    {}
		      	
    Node::~Node() throws()
    {}

    ::timber::Pointer<Annotation> Node::annotation() const throws()
    { return ::timber::Pointer<Annotation>(); }

  }
}
