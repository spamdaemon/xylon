#include <xylon/xsdom/Pattern.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Pattern::Pattern() throws()
    {}
		      	
    Pattern::~Pattern() throws()
    {}

    void Pattern::accept(Visitor& visitor)
    {
      visitor.visitPattern(toRef<Pattern>());
    }

  }
}
