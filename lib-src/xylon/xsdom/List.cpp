#include <xylon/xsdom/List.h>
#include <xylon/xsdom/SimpleType.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    List::List() throws()
    {}
		      	
    List::~List() throws()
    {}

    void List::accept(Visitor& visitor)
    {
      visitor.visitList(toRef<List>());
    }
  }
}
