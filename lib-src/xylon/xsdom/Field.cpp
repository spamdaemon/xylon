#include <xylon/xsdom/Field.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Field::Field() throws()
    {}
		      	
    Field::~Field() throws()
    {}

    void Field::accept(Visitor& visitor)
    {
      visitor.visitField(toRef<Field>());
    }

  }
}
