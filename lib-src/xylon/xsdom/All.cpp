#include <xylon/xsdom/All.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    All::All() throws()
    {}
		      	
    All::~All() throws()
    {}

    void All::accept(Visitor& visitor)
    {
      visitor.visitAll(toRef<All>());
    }


  }
}
