#include <xylon/xsdom/AnyAttribute.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
   namespace xsdom {

      AnyAttribute::AnyAttribute() throws()
      {
      }

      AnyAttribute::~AnyAttribute() throws()
      {
      }

      void AnyAttribute::accept(Visitor& visitor)
      {
         visitor.visitAnyAttribute(toRef< AnyAttribute> ());
      }

      bool AnyAttribute::acceptsNamespace(const String& ns) const throws()
      {
         const ::std::set< String>& nspaces = namespaces();

         if (nspaces.empty()) {
            // invalid schema!
            return false;
         }

         // match any namespace
         if (nspaces.count("##any") != 0) {
            return true;
         }

         // match any namespace that is not the default one;
         // note, the default namespace can be empty and so
         // need to test for ##local after this test
         if (nspaces.count("##other") != 0) {
            return ns != targetNamespace();
         }
         return nspaces.count(ns) != 0;
      }

   }
}
