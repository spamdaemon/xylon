#include <xylon/xsdom/Length.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Length::Length() throws()
    {}
		      	
    Length::~Length() throws()
    {}

    void Length::accept(Visitor& visitor)
    {
      visitor.visitLength(toRef<Length>());
    }

  }
}
