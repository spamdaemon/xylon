#ifndef _XYLON_XSDOM_DOM_UTIL_H
#define _XYLON_XSDOM_DOM_UTIL_H

#include <xylon/QName.h>
#include <xylon/xsdom/dom/SchemaException.h>
#include <timber/w3c/xml/dom/Element.h>
#include <timber/w3c/xml/dom/Attr.h>

namespace xylon {
  namespace xsdom {
    namespace dom {

      const size_t UNBOUNDED = ~size_t(0);
    
      /** An XML element reference */
      typedef ::timber::Reference< ::timber::w3c::xml::dom::Element> ElementRef;
    
      /** An XML element pointer */
      typedef ::timber::Pointer< ::timber::w3c::xml::dom::Element> ElementPtr;

      /** An XML element reference */
      typedef ::timber::Reference< ::timber::w3c::xml::dom::Attr> AttrRef;
    
      /** An XML element pointer */
      typedef ::timber::Pointer< ::timber::w3c::xml::dom::Attr> AttrPtr;

      /**
       * Get the first child element.
       * @return the first child element
       */
      ElementPtr firstElement(const ElementRef& e);

      /**
       * Get the next element sibling
       * @return the next element sibling
       */
      ElementPtr nextElement (const ElementRef& e);

      /**
       * Parse the value into a list.
       * @param value the value to parse into a list
       * @return a list
       */
      ::std::vector<String> parseListValue (const String& value);
      
      /**
       * Get the value of a string attribute
       * @param string the attribute name
       * @param found if not null, set to true if attribute was found, false otherwise
       * @return the attribute value or null if not defined
       */
      ::std::vector<String> getAttributeValueList (const ElementRef& e, const String& attr, bool* found=0);

      /**
       * Get the value of a string attribute
       * @param string the attribute name
       * @param defValue a default value to be placed into the list
       * @return the attribute value
       */
      ::std::vector<String> getAttributeValueList (const ElementRef& e, const String& attr, const String& def);
      
      /**
       * Get the value of a string attribute
       * @return e an element
       * @param string the attribute name
       * @return the attribute value or null if not defined
       */
      String getAttributeValue (const ElementRef& e, const String& attr);

      /**
       * Get the value of a string attribute
       * @return e an element
       * @param string the attribute name
       * @param defValue the default attribute value
       * @return the attribute value
       */
      String getAttributeValue (const ElementRef& e, const String& attr, const String& def);
      
      /**
       * Get the value of the minoccurs property of an element.
       * @param e an element
       * @return the value of the minOccurs attribute or 1 if not defined
       */
      size_t getMinOccurs(const ElementRef& e, size_t defaultMinOccurs=1);

      /**
       * Get the value of the maxoccurs property of an element.
       * @param e an element
       * @return the value of the maxOccurs attribute or 1 if not defined
       */
      size_t getMaxOccurs(const ElementRef& e, size_t defaultMaxOccurs=1);

      /**
       * Determine if a type or element is abstract
       * @param e an element
       * @return true if the specified element has an "abstract" attribute
       */
      bool isAbstract(const ElementRef& e);

      /**
       * Is the specified element a top-level element. A toplevel element
       * occurs immediately below the <em>schema</em> element.
       * @param e an element 
       * @return true if e is a top-level object
       */
      bool isTopLevelElement (const ElementRef& e);

      /**
       * Locate the scheme element at or above a given element.
       * @param e an element
       * @return the XSD schema element
       * @throws SchemaException if the schema could not be found
       */
      ElementRef findSchemaElement (const ElementRef& e);

      /**
       * Find the top-level group with the specified name.
       * @param e an element
       * @param ref a reference to another element
       * @return the referenced element in a schema
       */
      ElementRef findGroupByRef (const ElementRef& e, const String& ref);

      /**
       * Find the top-level element with the specified name.
       * @param e an element
       * @param ref a reference to another element
       * @return the referenced element in a schema
       */
      ElementRef findElementByRef (const ElementRef& e, const String& ref);

      /**
       * Find the top-level attribute with the specified name.
       * @param e an element
       * @param ref a reference to another attribute element
       * @return the referenced element in a schema
       */
      ElementRef findAttributeGroupByRef (const ElementRef& e, const String& ref);

      /**
       * Find the top-level attribute with the specified name.
       * @param e an element
       * @param ref a reference to another attribute element
       * @return the referenced element in a schema
       */
      ElementRef findAttributeByRef (const ElementRef& e, const String& ref);
    
      /**
       * Find the top-level type with the specified name.
       * @param e an element
       * @param ref a reference to another attribute element
       * @return the referenced element in a schema
       */
      ElementRef findTypeByRef (const ElementRef& e, const String& ref);
    
      /**
       * Get the target namespace that applies to the specified element. The
       * target namespace is found by locating the element's scheme element
       * @param e 
       * @return the target namespace or a null string of no namespace was defined
       * @throws SchemaException if the schema could not be found
       */
      String targetNamespace(const ElementRef& e);

      /**
       * Determine th enamespace uri for a possibly prefixed node associcated
       * with a given element
       * @param e an element
       * @param a prefixed name
       * @return the namespace uri that matches the prefix in the name (can be null)
       * @throws SchemaException if the namespace could not be resolved
       */
      String namespaceURI(const ElementRef& e, const String& nodename);
      

      /**
       * Get the <em>name</em> attribute.
       * @param e an element
       * @return the name attribute
       * @throws SchemaException if the element has no attribute <em>name</em>
       */
      String getElementName (const ElementRef& e);
    
      /**
       * Get the qname corresponding to the element's name.
       * @param e an element
       * @return a unique qname
       * @throws SchemaException if the element has no attribute <em>name</em>.
       * @throws SchemaException if the targetNamespace cannot be determined
       */
      QName getElementQName (const ElementRef& e);

      /**
       * Compute the qualified name corresponding to a prefixed name reference.
       * @param e the element which which the reference is associated.
       * @param ref the prefixed reference itself
       * @return a qualified name
       */
      QName getRefQName (const ElementRef& e, String ref);

      /**
       * Get the qualified name associated with a ref attribute.
       * @param e an element
       * @return a qname or an invalid qname if there is no such attribute
       */
      QName getReference(const ElementRef& e);

      /**
       * Get the qualified name associated with a type attribute.
       * @param e an element
       * @return a qname or an invalid qname if the element has no type attribute
       */
      QName getTypeReference(const ElementRef& e);

      /**
       * Get the qualified name associated with a base attribute.
       * @param e an element
       * @return a qname or an invalid qname if the element has no base attribute
       */
      QName getBaseReference(const ElementRef& e);

      /**
       * Get the qualified name associated of an element's substitution group.
       * @param e an element
       * @return a qname or an invalid qname if the element has no substitutionGroup attribute.
       */
      QName getSubstitutionGroup(const ElementRef& e);

      /**
       * Get the element content. If the element contains other child elements,
       * then null is returned, otherwise at least an empty string is returned.
       * @return the text content below an element.
       */
      String getElementContent(const ElementRef& e);
    }
  }
}

#endif
