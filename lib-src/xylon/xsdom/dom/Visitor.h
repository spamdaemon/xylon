#ifndef _XYLON_XSDOM_DOM_VISITOR_H
#define _XYLON_XSDOM_DOM_VISITOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_LOGGING_H
#include <timber/logging.h>
#endif

#ifndef _TIMBER_W3C_XML_DOM_ELEMENT_H
#include <timber/w3c/xml/dom/Element.h>
#endif

namespace xylon {
  namespace xsdom {
    namespace dom {
    
      /**
       * A visitor. 
       */
      class Visitor {
      
      protected:
	Visitor() throws();
      
      public:
	virtual ~Visitor() throws() = 0;
      
	/**
	 * Visit the specified element. This method determines which of the visit* functions should
	 * be invoked.
	 * @param schema a schema 
	 * @param obj an element node to be visited
	 */
      public:
	virtual void visit (const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
      
	/**
	 * Visit the children of the specified element node. This method must be tolerant of removal of this
	 * object fomr its document. This method can not be expected to deal with replacement of the object node.
	 * @param schema a schema 
	 * @param obj an element node whose children are visited
	 */
      public:
	virtual void visitChildren (const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
      
	/**
	 * The default visitation function for a node.
	 * @param schema a schema 
	 * @param obj an element node whose children are visited
	 */
      public:
	virtual void visitDefault (const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
      
	/**
	 * Notify the subclass that the specified element is not understood. This method is invoked by visit() when an 
	 * element is node valid XSD element.
	 * The default is to throw a SchemaException.
	 * @param schema a schema
	 * @param obj an element node that is not a valid element
	 */
      protected:
	virtual void elementNotUnderstood(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);

	/**
	 * Get the logger for this visitor.
	 * @return the logger
	 */
      protected:
	inline ::timber::logging::Log& logger() const throws() { return _log; }

	/**
	 * Set the logger to use for this visitor.
	 * @param lg the new logger
	 */
      protected:
	void setLogger (const ::timber::logging::Log& lg) throws();

	/**
	 * @name The various visitation function. One for each possible element in an XSD schema.
	 * @{
	 */
	virtual void visitAll(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitAnnotation(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitAny(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitAnyAttribute(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitAppInfo(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitAttribute(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitAttributeGroup(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitChoice(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitComplexContent(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitComplexType(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitDocumentation(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitElement(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitExtension(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitField(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitGroup(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitImport(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitInclude(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitKey(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitKeyRef(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitList(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitNotation(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitRedefine(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitRestriction(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitSchema(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitSelector(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitSequence(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitSimpleContent(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitSimpleType(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitUnion(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitUnique(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitEnumeration(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitFractionDigits(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitLength(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitMaxExclusive(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitMaxInclusive(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitMaxLength(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitMinExclusive(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitMinInclusive(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitMinLength(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitPattern(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitTotalDigits(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
	virtual void visitWhiteSpace(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& obj);
 
	/*@}*/

	/**
	 * A logger for this visitor.
	 */
      private:
	mutable ::timber::logging::Log _log;
      };
    }
  }
}
#endif
