#include <xylon/xsdom/dom/util.h>
#include <xylon/xsdom/xsdom.h>
#include <xylon/QName.h>
#include <timber/w3c/xml/xml.h>
#include <timber/w3c/xml/dom/dom.h>

#include <sstream>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::w3c::xml;
using namespace ::timber::w3c::xml::dom;

namespace xylon {
   namespace xsdom {
      namespace dom {

         bool isXSDObject(const ElementRef& e, const String& name)
         {
            return e->namespaceURI() && e->namespaceURI() == XSD_NAMESPACE_URI && e->localName() == name;
         }

         bool isAbstract(const ElementRef& e)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), "abstract");
            return a && a->nodeValue() == "true";
         }

         size_t getMinOccurs(const ElementRef& e, size_t defaultMinOccurs)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), "minOccurs");
            if (a) {
               istringstream sin(a->nodeValue().string());
               size_t res;
               sin >> res;
               if (sin.fail()) {
                  throw SchemaException("Invalid minOccurs", e);
               }
               return res;
            }
            else {
               return defaultMinOccurs;
            }
         }

         size_t getMaxOccurs(const ElementRef& e, size_t defaultMaxOccurs)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), "maxOccurs");
            if (a) {
               if (a->nodeValue() == "unbounded") {
                  return UNBOUNDED;
               }

               istringstream sin(a->nodeValue().string());
               size_t res;
               sin >> res;
               if (sin.fail()) {
                  throw SchemaException("Invalid maxOccurs", e);
               }
               return res;
            }
            else {
               return defaultMaxOccurs;
            }
         }

         ::std::vector< String> parseListValue(const String& value)
         {
            ::std::vector< String> list;
            const size_t len = value.length();
            for (size_t pos = 0, i = 0; pos < len; pos = i + 1) {
               i = value.indexOf(' ', pos);
               if (i == String::NOT_FOUND) {
                  i = len;
               }
               String substr = value.substring(pos, i - pos);
               if (!substr.isEmpty()) {
                  list.push_back(substr);
               }
               else {
                  assert(pos>=i);
               }
            }
            return list;
         }

         ::std::vector< String> getAttributeValueList(const ElementRef& e, const String& attr, bool* found)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), attr);
            if (a) {
               if (found) {
                  *found = true;
               }
               return parseListValue(a->nodeValue());
            }
            else {
               if (found) {
                  *found = false;
               }
               return ::std::vector< String>();
            }
         }

         ::std::vector< String> getAttributeValueList(const ElementRef& e, const String& attr, const String& def)
         {
            ::std::vector< String> res = getAttributeValueList(e, attr);
            if (res.empty()) {
               res.push_back(def);
            }
            return res;
         }

         String getAttributeValue(const ElementRef& e, const String& attr)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), attr);
            return a ? a->nodeValue() : String();
         }

         String getAttributeValue(const ElementRef& e, const String& attr, const String& def)
         {
            String v = getAttributeValue(e, attr);
            if (!v) {
               v = def;
            }
            return v;
         }

         ElementRef findSchemaElement(const ElementRef& e)
         {
            Pointer< ::timber::w3c::xml::dom::Node> p = e;
            while (p) {
               if (p->namespaceURI() && p->localName() == "schema" && p->namespaceURI() == XSD_NAMESPACE_URI) {
                  return p;
               }
               p = p->parent();
            }
            throw SchemaException("Schema element found");
         }

         String targetNamespace(const ElementRef& e)
         {
            ElementRef p = findSchemaElement(e);
            AttrPtr a = p->getAttributeNodeNS(String(), "targetNamespace");
            if (a) {
               return a->nodeValue();
            }
            else {
               // no namespace defined in the document
               return String();
            }
         }

         String namespaceURI(const ElementRef& e, const String& nodename)
         {
            String pre, l;
            splitQName(nodename, pre, l);

            if (pre == "xml") {
               return ::timber::w3c::xml::XML_NAMESPACE;
            }

            String nsURI;
            String localName;

            // if we have prefix, then we need to look for a namespace declaration
            // of the form : xmlns:<pre>="<namespace>"
            if (pre) {
               nsURI = XML_NAMESPACE_DECL;
               localName = pre;
            }
            else {
               // leave ns as String()
               localName = "xmlns";
            }

            // traverse upward, looking for xmlns definitions; this traverses all the way back
            // and so will work even with schema elements embedded in other documents, such as wsdl
            for (Pointer< ::timber::w3c::xml::dom::Node> p = e; p; p = p->parent()) {
               if (p->nodeType() != ::timber::w3c::xml::dom::Node::ELEMENT_NODE) {
                  break;
               }
               ElementRef elmt = p;
               // look for an attribute "xmlns:<pre>="<namespace>"; xmlns is bound to XML_NAMESPACE_DECL
               AttrPtr attr = elmt->getAttributeNodeNS(nsURI, localName);
               if (attr) {
                  return attr->nodeValue();
               }

               // check the element's nodename itself, incase the xmlns="" is not set
               if (elmt->prefix() == pre) {
                  return elmt->namespaceURI();
               }

            }

            if (!pre) {
               // no default namespace was defined; check if there is a target namespace defined
               String tns = targetNamespace(e);
               if (!tns) {
                  return tns;
               }
               else {
                  // loop up to the top until we have a schema that defines the target namespace
                  for (Pointer< ::timber::w3c::xml::dom::Node> p = e; p; p = p->parent()) {
                     if (p->nodeType() != ::timber::w3c::xml::dom::Node::ELEMENT_NODE) {
                        break;
                     }
                     ElementRef elmt = p;
                     // look for an attribute "xmlns:<pre>="<namespace>"; xmlns is bound to XML_NAMESPACE_DECL
                     AttrPtr attr = elmt->getAttributeNodeNS(String(), "targetNamespace");
                     if (attr && isXSDObject(elmt, "schema")) {
                        return attr->nodeValue();
                     }
                  }
               }
            }
            throw SchemaException("Namespace not found for referenced element " + nodename.string(), e);
         }

         // construct a type name
         String getElementName(const ElementRef& e)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), "name");
            if (a) {
               return a->nodeValue();
            }
            else {
               throw SchemaException("Element has no name", e);
            }
         }

         // construct a qname from an object's name and the schema's target namespace
         QName getElementQName(const ElementRef& e)
         {
            if (e->localName() == "any") {
               return QName(e->namespaceURI(), e->localName());
            }
            return QName(targetNamespace(e), getElementName(e));
         }

         // construct a qname for a reference made by the specified object
         QName getRefQName(const ElementRef& e, String ref)
         {
            String nsURI = namespaceURI(e, ref);
            String pre, localname;
            splitQName(ref, pre, localname);
            return QName(nsURI, localname);
         }

         QName getReference(const ElementRef& e)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), "ref");
            if (!a) {
               return QName();
            }
            else {
               return getRefQName(e, a->nodeValue());
            }
         }

         QName getTypeReference(const ElementRef& e)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), "type");
            if (!a) {
               return QName();
            }
            else {
               return getRefQName(e, a->nodeValue());
            }
         }

         QName getBaseReference(const ElementRef& e)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), "base");
            if (!a) {
               return QName();
            }
            else {
               return getRefQName(e, a->nodeValue());
            }
         }

         QName getSubstitutionGroup(const ElementRef& e)
         {
            AttrPtr a = e->getAttributeNodeNS(String(), "substitutionGroup");
            if (!a) {
               return QName();
            }
            else {
               return getRefQName(e, a->nodeValue());
            }
         }

         ElementPtr firstElement(const ElementRef& e)
         {
            for (Pointer< ::timber::w3c::xml::dom::Node> n = e->firstChild(); n; n = n->nextSibling()) {
               if (n->nodeType() == ::timber::w3c::xml::dom::Node::ELEMENT_NODE) {
                  return n;
               }
            }
            return ElementPtr();
         }

         ElementPtr nextElement(const ElementRef& e)
         {
            for (Pointer< ::timber::w3c::xml::dom::Node> n = e->nextSibling(); n; n = n->nextSibling()) {
               if (n->nodeType() == ::timber::w3c::xml::dom::Node::ELEMENT_NODE) {
                  return n;
               }
            }
            return ElementPtr();
         }

         ElementRef findGroupByRef(const ElementRef& e, const String& name)
         {
            for (ElementPtr n = firstElement(findSchemaElement(e)); n; n = nextElement(n)) {
               if (isXSDObject(n, "group")) {
                  if (name == getElementQName(n)) {
                     return n;
                  }
               }
            }
            throw SchemaException("Reference to element " + name.string() + " not found", e);
         }

         ElementRef findElementByRef(const ElementRef& e, const String& name)
         {
            for (ElementPtr n = firstElement(findSchemaElement(e)); n; n = nextElement(n)) {
               if (isXSDObject(n, "element")) {
                  if (name == getElementQName(n)) {
                     return n;
                  }
               }
            }
            throw SchemaException("Reference to element " + name.string() + " not found", e);
         }

         ElementRef findAttributeByRef(const ElementRef& e, const String& name)
         {
            for (ElementPtr n = firstElement(findSchemaElement(e)); n; n = nextElement(n)) {
               if (isXSDObject(n, "attribute")) {
                  if (name == getElementQName(n)) {
                     return n;
                  }
               }
            }
            throw SchemaException("Reference to attribute " + name.string() + " not found", e);
         }

         ElementRef findAttributeGroupByRef(const ElementRef& e, const String& name)
         {
            for (ElementPtr n = firstElement(findSchemaElement(e)); n; n = nextElement(n)) {
               if (isXSDObject(n, "attributeGroup")) {
                  if (name == getElementQName(n)) {
                     return n;
                  }
               }
            }
            throw SchemaException("Reference to attribute " + name.string() + " not found", e);
         }

         ElementRef findTypeByRef(const ElementRef& e, const String& name)
         {
            for (ElementPtr n = firstElement(findSchemaElement(e)); n; n = nextElement(n)) {
               if (isXSDObject(n, "complexType") || isXSDObject(n, "simpleType")) {
                  if (name == getElementQName(n)) {
                     return n;
                  }
               }
            }
            throw SchemaException("Reference to type " + name.string() + " not found", e);
         }

         bool isTopLevelElement(const ElementRef& e)
         {
            Pointer< ::timber::w3c::xml::dom::Node> p = e->parent();
            return p && p->nodeType() == ::timber::w3c::xml::dom::Node::ELEMENT_NODE && p->localName() == "schema" && p->namespaceURI()
                  == XSD_NAMESPACE_URI;
         }

         String getElementContent(const ElementRef& e)
         {
            String value;
            for (Pointer< ::timber::w3c::xml::dom::Node> n = e->firstChild(); n; n = n->nextSibling()) {
               if (n->nodeType() == ::timber::w3c::xml::dom::Node::ELEMENT_NODE) {
                  return String(); //FIXME: indicates mixed content
               }
               if (n->nodeType() == ::timber::w3c::xml::dom::Node::TEXT_NODE || n->nodeType() == ::timber::w3c::xml::dom::Node::CDATA_SECTION_NODE) {
                  if (value) {
                     value = value + n->nodeValue();
                  }
                  else {
                     value = n->nodeValue();
                  }
               }
            }
            return value;
         }

      }
   }
}
