#include <xylon/xsdom/dom/Visitor.h>
#include <xylon/xsdom/dom/util.h>
#include <xylon/xsdom/dom/SchemaException.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

#include <string>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace w3c::xml::dom;

namespace xylon {
  namespace xsdom {
    namespace dom {
      Visitor::Visitor() throws()
      : _log("xylon.xsdom.dom.Visitor")
      {}
    
      Visitor::~Visitor() throws()
      {}
    
      void Visitor::setLogger (const ::timber::logging::Log& lg) throws()
      { _log = lg; }
    
      void Visitor::elementNotUnderstood(const Reference< ::timber::w3c::xml::dom::Element>& obj)
      {
	throw SchemaException("Invalid schema element found",obj);
      }

      void Visitor::visit (const Reference< ::timber::w3c::xml::dom::Element>& obj)
      {
	try {
	  if (!obj->namespaceURI()) {
	    logger().warn(string("Element has no namespace; expected ")+XSD_NAMESPACE_URI);
	    elementNotUnderstood(obj);
	    return;
	  }
	  if (obj->namespaceURI()!=XSD_NAMESPACE_URI) {
	    logger().warn(string("Element has unknown namespace; expected ")+XSD_NAMESPACE_URI);
	    elementNotUnderstood(obj);
	  }

	  if (obj->localName()=="all") {
	    visitAll(obj);
	    return;
	  }
	  if (obj->localName()=="annotation") {
	    visitAnnotation(obj);
	    return;
	  }
	  if (obj->localName()=="any") {
	    visitAny(obj);
	    return;
	  }
	  if (obj->localName()=="anyAttribute") {
	    visitAnyAttribute(obj);
	    return;
	  }
	  if (obj->localName()=="appInfo") {
	    visitAppInfo(obj);
	    return;
	  }
	  if (obj->localName()=="attribute") {
	    visitAttribute(obj);
	    return;
	  }
	  if (obj->localName()=="attributeGroup") {
	    visitAttributeGroup(obj);
	    return;
	  }
	  if (obj->localName()=="choice") {
	    visitChoice(obj);
	    return;
	  }
	  if (obj->localName()=="complexContent") {
	    visitComplexContent(obj);
	    return;
	  }
	  if (obj->localName()=="complexType") {
	    visitComplexType(obj);
	    return;
	  }
	  if (obj->localName()=="documentation") {
	    visitDocumentation(obj);
	    return;
	  }
	  if (obj->localName()=="element") {
	    visitElement(obj);
	    return;
	  }
	  if (obj->localName()=="extension") {
	    visitExtension(obj);
	    return;
	  }
	  if (obj->localName()=="field") {
	    visitField(obj);
	    return;
	  }
	  if (obj->localName()=="group") {
	    visitGroup(obj);
	    return;
	  }
	  if (obj->localName()=="import") {
	    visitImport(obj);
	    return;
	  }
	  if (obj->localName()=="include") {
	    visitInclude(obj);
	    return;
	  }
	  if (obj->localName()=="key") {
	    visitKey(obj);
	    return;
	  }
	  if (obj->localName()=="keyRef") {
	    visitKeyRef(obj);
	    return;
	  }
	  if (obj->localName()=="list") {
	    visitList(obj);
	    return;
	  }
	  if (obj->localName()=="notation") {
	    visitNotation(obj);
	    return;
	  }
	  if (obj->localName()=="redefine") {
	    visitRedefine(obj);
	    return;
	  }
	  if (obj->localName()=="restriction") {
	    visitRestriction(obj);
	    return;
	  }
	  if (obj->localName()=="schema") {
	    visitSchema(obj);
	    return;
	  }
	  if (obj->localName()=="selector") {
	    visitSelector(obj);
	    return;
	  }
	  if (obj->localName()=="sequence") {
	    visitSequence(obj);
	    return;
	  }
	  if (obj->localName()=="simpleContent") {
	    visitSimpleContent(obj);
	    return;
	  }
	  if (obj->localName()=="simpleType") {
	    visitSimpleType(obj);
	    return;
	  }
	  if (obj->localName()=="union") {
	    visitUnion(obj);
	    return;
	  }
	  if (obj->localName()=="unique") {
	    visitUnique(obj);
	    return;
	  }
	  if (obj->localName()=="enumeration") {
	    visitEnumeration(obj);
	    return;
	  }
	  if (obj->localName()=="fractionDigits") {
	    visitFractionDigits(obj);
	    return;
	  }
	  if (obj->localName()=="length") {
	    visitLength(obj);
	    return;
	  }
	  if (obj->localName()=="maxExclusive") {
	    visitMaxExclusive(obj);
	    return;
	  }
	  if (obj->localName()=="maxInclusive") {
	    visitMaxInclusive(obj);
	    return;
	  }
	  if (obj->localName()=="maxLength") {
	    visitMaxLength(obj);
	    return;
	  }
	  if (obj->localName()=="minExclusive") {
	    visitMinExclusive(obj);
	    return;
	  }
	  if (obj->localName()=="minInclusive") {
	    visitMinInclusive(obj);
	    return;
	  }
	  if (obj->localName()=="minLength") {
	    visitMinLength(obj);
	    return;
	  }
	  if (obj->localName()=="pattern") {
	    visitPattern(obj);
	    return;
	  }
	  if (obj->localName()=="totalDigits") {
	    visitTotalDigits(obj);
	    return;
	  }
	  if (obj->localName()=="whiteSpace") {
	    visitWhiteSpace(obj);
	    return;
	  }
      
	  logger().warn("Found unknown XSD element "+obj->localName().string());
	}
	catch (const SchemaException& e) {
	  throw;
	}
	catch (const ::std::exception& e) {
	  LogEntry(logger()).warn() << "Exception while parsing : " << obj->localName().string() << " : " << e.what() << doLog;
	}
	catch (...) {
	  LogEntry(logger()).warn() << "Unexpexted exception caught : " << obj->localName().string() << doLog;
	}
	elementNotUnderstood(obj);
      }
      
    
      void Visitor::visitDefault (const Reference< ::timber::w3c::xml::dom::Element>& obj)
      {}

      void Visitor::visitChildren (const Reference< ::timber::w3c::xml::dom::Element>& obj)
      {
	for (Pointer< ::timber::w3c::xml::dom::Element> n = firstElement(obj);n;) {
	  // access the next sibling now; this enables the visitation
	  // function to remove the current node
	  Pointer< ::timber::w3c::xml::dom::Element> next = nextElement(n);
	  visit(n);
	  n = next;
	  if (n && n->parent()!=obj) {
	    throw ::std::runtime_error("Visitor removed an object that is should not have removed");
	  }
	}
      }
    
      void Visitor::visitAll(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitAnnotation(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitAny(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitAnyAttribute(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitAppInfo(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitAttribute(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitAttributeGroup(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitChoice(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitComplexContent(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitComplexType(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitDocumentation(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitElement(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitExtension(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitField(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitGroup(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitImport(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitInclude(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitKey(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitKeyRef(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitList(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitNotation(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitRedefine(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitRestriction(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitSchema(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitSelector(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitSequence(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitSimpleContent(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitSimpleType(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitUnion(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitUnique(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitEnumeration(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitFractionDigits(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitLength(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitMaxExclusive(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitMaxInclusive(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitMaxLength(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitMinExclusive(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitMinInclusive(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitMinLength(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitPattern(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitTotalDigits(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
      void Visitor::visitWhiteSpace(const Reference< ::timber::w3c::xml::dom::Element>& obj) { visitDefault(obj); }
    }
  }
}
