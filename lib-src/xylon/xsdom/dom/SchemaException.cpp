#include <xylon/xsdom/dom/SchemaException.h>
#include <timber/w3c/xml/dom/dom.h>

#include <sstream>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::w3c::xml::dom;

namespace xylon {
  namespace xsdom {
    namespace dom {

      SchemaException::SchemaException(const ::std::string& msg, const ::timber::Pointer< ::timber::w3c::xml::dom::Element>& e) throws()
      : _element(e)
      {
	if (e) {
	  ostringstream out;
	  out << msg << endl << ": offending element " << endl;
	  print(e,out);
	  out << endl;
	  _message = out.str();
	}
	else {
	  _message = msg;
	}
	::std::cerr << _message  << ::std::endl;
     }

      SchemaException::SchemaException(const ::std::string& msg) throws()
      : _message(msg) 
      {
	::std::cerr << _message  << ::std::endl;
      }
	
      SchemaException::~SchemaException() throw()
      {}

      const char* SchemaException::what() const throw()
      { return _message.c_str(); }


    }
  }
}
