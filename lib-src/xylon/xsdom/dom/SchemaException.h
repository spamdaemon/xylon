#ifndef _XYLON_XSDOM_DOM_SCHEMAEXCEPTION_H
#define _XYLON_XSDOM_DOM_SCHEMAEXCEPTION_H

#ifndef _TIMBER_W3C_XML_DOM_ELEMENT_H
#include <timber/w3c/xml/dom/Element.h>
#endif

#include <stdexcept>

namespace xylon {
  namespace xsdom {
    namespace dom {
      /**
       * A schema exception.
       */
      class SchemaException : public ::std::exception {
      
	/** 
	 * Create a new exception
	 * @param msg a message
	 * @param e an optional element
	 */
      public:
	SchemaException(const ::std::string& msg, const ::timber::Pointer< ::timber::w3c::xml::dom::Element>& e) throws();
	
	/** 
	 * Create a new exception
	 * @param msg a message
	 */
      public:
	SchemaException(const ::std::string& msg) throws();
	
	/**
	 * Destructor 
	 */
      public:
	~SchemaException() throw();

	/** 
	 * Create an error message
	 * @return an error message
	 */
      public:
	const char* what() const throw();

	/** The error message */
      private:
	::std::string _message;

	/** The xml element that was in error */
      private:
	::timber::Pointer< ::timber::w3c::xml::dom::Element> _element;
      };

    }
  }
}
#endif
