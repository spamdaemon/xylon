#include <xylon/xsdom/MaxInclusive.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    MaxInclusive::MaxInclusive() throws()
    {}
		      	
    MaxInclusive::~MaxInclusive() throws()
    {}

    void MaxInclusive::accept(Visitor& visitor)
    {
      visitor.visitMaxInclusive(toRef<MaxInclusive>());
    }

  }
}
