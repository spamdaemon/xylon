#ifndef _XYLON_XSDOM_TOTALDIGITS_H
#define _XYLON_XSDOM_TOTALDIGITS_H

#ifndef _XYLON_XSDOM_FACET_H
#include <xylon/xsdom/Facet.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class TotalDigits : public Facet {
      
      /** The default constructor */
    public:
      TotalDigits() throws();
      
      /** Destructor */
    public:
      ~TotalDigits() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
