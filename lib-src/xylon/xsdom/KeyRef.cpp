#include <xylon/xsdom/KeyRef.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    KeyRef::KeyRef() throws()
    {}
		      	
    KeyRef::~KeyRef() throws()
    {}

    void KeyRef::accept(Visitor& visitor)
    {
      visitor.visitKeyRef(toRef<KeyRef>());
    }

  }
}
