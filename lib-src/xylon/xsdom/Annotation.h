#ifndef _XYLON_XSDOM_ANNOTATION_H
#define _XYLON_XSDOM_ANNOTATION_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Annotation : public Node {
      
      /** The default constructor */
    public:
      Annotation() throws();
      
      /** Destructor */
    public:
      ~Annotation() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
