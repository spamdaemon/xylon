#include <xylon/xsdom/FractionDigits.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    FractionDigits::FractionDigits() throws()
    {}
		      	
    FractionDigits::~FractionDigits() throws()
    {}

    void FractionDigits::accept(Visitor& visitor)
    {
      visitor.visitFractionDigits(toRef<FractionDigits>());
    }

  }
}
