#ifndef _XYLON_XSDOM_COMPLEXTYPE_H
#define _XYLON_XSDOM_COMPLEXTYPE_H

#ifndef _XYLON_XSDOM_TYPE_H
#include <xylon/xsdom/Type.h>
#endif

namespace xylon {
   namespace xsdom {
      /**
       * A schema node. This is the base class for all
       * schema elements.
       */
      class ComplexType : public Type
      {
            /** Whether extensions or restrictions are blocked */
         public:
            enum Block
            {
               BLOCK_NONE, BLOCK_EXTENSION, BLOCK_RESTRICTION, BLOCK_ALL
            };

            /** Whether extensions or restrictions are blocked */
         public:
            enum Final
            {
               FINAL_NONE, FINAL_EXTENSION, FINAL_RESTRICTION, FINAL_ALL
            };

            /** The default constructor */
         public:
            ComplexType() throws();

            /** Destructor */
         public:
            ~ComplexType() throws();

            /**
             * Accept a node visitor
             * @param visitor a visitor
             */
         public:
            void accept(Visitor& visitor);

            /**
             * Determine if this complex type derives from the given type through an extension.
             * @param baseType the name of a base type
             * @return true if this type extends the specified type.
             */
            bool isSubtypeOf (const QName& baseType) const throws();

            /**
             * Determine if the content can be mixed.
             * @return true if the content can be mixed
             */
         public:
            virtual bool isMixed() const throws() =0;

            /**
             * Determine if this type is abstract
             * @return true if this type is abstract
             */
         public:
            virtual bool isAbstract() const throws() =0;

            /**
             * Determine if subtypes can be substituted for this type
             * @return the type of substitutions for this type
             */
         public:
            virtual int block() const throws() =0;

            /**
             * Determine if how this type can be derived.
             * @return the type of derivation allowed
             */
         public:
            virtual int final() const throws() =0;

            /**
             * Get the content type.
             * @return the enumerable
             */
         public:
            virtual ::timber::Pointer< ContentType> contentType() const throws() =0;

            /**
             * Get the enumerable. It is possible, that null is returned, because
             * using an enumerable is just short-hand for a restriction.
             * @return the enumerable
             */
         public:
            virtual ::timber::Pointer< Enumerable> enumerable() const throws() =0;

            /**
             * Get the any attribute
             * @return the any attribute
             */
         public:
            virtual ::timber::Pointer< AnyAttribute> anyAttribute() const throws() =0;

            /**
             * Set the attributes defined by this group.
             * @param attrs the attributes
             */
         public:
            virtual const ::std::vector< ::timber::Reference< Attribute> >& attributes() const throws() =0;
            /**
             * Set the attributes or attribute groups. Each node must be an attribute
             * or attribute group.
             * @param grps the contained attribute groups (they will be references)
             */
         public:
            virtual const ::std::vector< ::timber::Reference< AttributeGroup> >& attributeGroups() const throws() =0;
      };
   }
}
#endif
