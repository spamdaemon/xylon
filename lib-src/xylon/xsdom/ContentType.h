#ifndef _XYLON_XSDOM_CONTENTTYPE_H
#define _XYLON_XSDOM_CONTENTTYPE_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    class Derivation;
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class ContentType : public Node {
      
      /** The default constructor */
    public:
      ContentType() throws();
      
      /** Destructor */
    public:
      ~ContentType() throws();

      /**
       * Get the derivation
       * @return the derivation
       */
    public:
      virtual ::timber::Pointer<Derivation> derivation() const throws() =0;
    };
  }
}
#endif
