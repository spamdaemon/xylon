#ifndef _XYLON_XSDOM_NODES_H
#define _XYLON_XSDOM_NODES_H

#include <xylon/xsdom/All.h>
#include <xylon/xsdom/Annotation.h>
#include <xylon/xsdom/Any.h>
#include <xylon/xsdom/AnyAttribute.h>
#include <xylon/xsdom/AppInfo.h>
#include <xylon/xsdom/Attribute.h>
#include <xylon/xsdom/AttributeGroup.h>
#include <xylon/xsdom/Choice.h>
#include <xylon/xsdom/ComplexContent.h>
#include <xylon/xsdom/ComplexType.h>
#include <xylon/xsdom/Documentation.h>
#include <xylon/xsdom/Element.h>
#include <xylon/xsdom/Extension.h>
#include <xylon/xsdom/Field.h>
#include <xylon/xsdom/Group.h>
#include <xylon/xsdom/Import.h>
#include <xylon/xsdom/Include.h>
#include <xylon/xsdom/Key.h>
#include <xylon/xsdom/KeyRef.h>
#include <xylon/xsdom/List.h>
#include <xylon/xsdom/Notation.h>
#include <xylon/xsdom/Redefine.h>
#include <xylon/xsdom/Restriction.h>
#include <xylon/xsdom/Schema.h>
#include <xylon/xsdom/Selector.h>
#include <xylon/xsdom/Sequence.h>
#include <xylon/xsdom/SimpleContent.h>
#include <xylon/xsdom/SimpleType.h>
#include <xylon/xsdom/Union.h>
#include <xylon/xsdom/Unique.h>
#include <xylon/xsdom/Enumeration.h>
#include <xylon/xsdom/FractionDigits.h>
#include <xylon/xsdom/Length.h>
#include <xylon/xsdom/MaxExclusive.h>
#include <xylon/xsdom/MaxInclusive.h>
#include <xylon/xsdom/MaxLength.h>
#include <xylon/xsdom/MinExclusive.h>
#include <xylon/xsdom/MinInclusive.h>
#include <xylon/xsdom/MinLength.h>
#include <xylon/xsdom/Pattern.h>
#include <xylon/xsdom/TotalDigits.h>
#include <xylon/xsdom/WhiteSpace.h>

#include <xylon/xsdom/UrType.h>


#endif
