#ifndef _XYLON_XSDOM_KEY_H
#define _XYLON_XSDOM_KEY_H

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Key : public Node {
      
      /** The default constructor */
    public:
      Key() throws();
      
      /** Destructor */
    public:
      ~Key() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Get the qualified name for this element
       * @return the qualified name for this element
       */
    public:
      virtual QName qname() const throws() = 0;
    };
  }
}
#endif
