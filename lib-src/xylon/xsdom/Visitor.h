#ifndef _XYLON_XSDOM_VISITOR_H
#define _XYLON_XSDOM_VISITOR_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace xylon {
   namespace xsdom {
      class All;
      class Annotation;
      class Any;
      class AnyAttribute;
      class AppInfo;
      class Attribute;
      class AttributeGroup;
      class Choice;
      class ComplexContent;
      class ComplexType;
      class Documentation;
      class Element;
      class Extension;
      class Field;
      class Group;
      class Import;
      class Include;
      class Key;
      class KeyRef;
      class List;
      class Notation;
      class Redefine;
      class Restriction;
      class Schema;
      class Selector;
      class Sequence;
      class SimpleContent;
      class SimpleType;
      class Union;
      class Unique;
      class Enumeration;
      class FractionDigits;
      class Length;
      class MaxExclusive;
      class MaxInclusive;
      class MaxLength;
      class MinExclusive;
      class MinInclusive;
      class MinLength;
      class Pattern;
      class TotalDigits;
      class WhiteSpace;

      class UrType;

      /**
       * A visitor for different xs nodes
       */
      class Visitor
      {

            /** Default constructor */
         public:
            Visitor() throws();

            /** Destructor */
         public:
            virtual ~Visitor() throws() = 0;

            virtual void visitUrType(const ::timber::Reference< UrType>& obj);
            virtual void visitAll(const ::timber::Reference< All>& obj);
            virtual void visitAnnotation(const ::timber::Reference< Annotation>& obj);
            virtual void visitAny(const ::timber::Reference< Any>& obj);
            virtual void visitAnyAttribute(const ::timber::Reference< AnyAttribute>& obj);
            virtual void visitAppInfo(const ::timber::Reference< AppInfo>& obj);
            virtual void visitAttribute(const ::timber::Reference< Attribute>& obj);
            virtual void visitAttributeGroup(const ::timber::Reference< AttributeGroup>& obj);
            virtual void visitChoice(const ::timber::Reference< Choice>& obj);
            virtual void visitComplexContent(const ::timber::Reference< ComplexContent>& obj);
            virtual void visitComplexType(const ::timber::Reference< ComplexType>& obj);
            virtual void visitDocumentation(const ::timber::Reference< Documentation>& obj);
            virtual void visitElement(const ::timber::Reference< Element>& obj);
            virtual void visitExtension(const ::timber::Reference< Extension>& obj);
            virtual void visitField(const ::timber::Reference< Field>& obj);
            virtual void visitGroup(const ::timber::Reference< Group>& obj);
            virtual void visitImport(const ::timber::Reference< Import>& obj);
            virtual void visitInclude(const ::timber::Reference< Include>& obj);
            virtual void visitKey(const ::timber::Reference< Key>& obj);
            virtual void visitKeyRef(const ::timber::Reference< KeyRef>& obj);
            virtual void visitList(const ::timber::Reference< List>& obj);
            virtual void visitNotation(const ::timber::Reference< Notation>& obj);
            virtual void visitRedefine(const ::timber::Reference< Redefine>& obj);
            virtual void visitRestriction(const ::timber::Reference< Restriction>& obj);
            virtual void visitSchema(const ::timber::Reference< Schema>& obj);
            virtual void visitSelector(const ::timber::Reference< Selector>& obj);
            virtual void visitSequence(const ::timber::Reference< Sequence>& obj);
            virtual void visitSimpleContent(const ::timber::Reference< SimpleContent>& obj);
            virtual void visitSimpleType(const ::timber::Reference< SimpleType>& obj);
            virtual void visitUnion(const ::timber::Reference< Union>& obj);
            virtual void visitUnique(const ::timber::Reference< Unique>& obj);
            virtual void visitEnumeration(const ::timber::Reference< Enumeration>& obj);
            virtual void visitFractionDigits(const ::timber::Reference< FractionDigits>& obj);
            virtual void visitLength(const ::timber::Reference< Length>& obj);
            virtual void visitMaxExclusive(const ::timber::Reference< MaxExclusive>& obj);
            virtual void visitMaxInclusive(const ::timber::Reference< MaxInclusive>& obj);
            virtual void visitMaxLength(const ::timber::Reference< MaxLength>& obj);
            virtual void visitMinExclusive(const ::timber::Reference< MinExclusive>& obj);
            virtual void visitMinInclusive(const ::timber::Reference< MinInclusive>& obj);
            virtual void visitMinLength(const ::timber::Reference< MinLength>& obj);
            virtual void visitPattern(const ::timber::Reference< Pattern>& obj);
            virtual void visitTotalDigits(const ::timber::Reference< TotalDigits>& obj);
            virtual void visitWhiteSpace(const ::timber::Reference< WhiteSpace>& obj);

      };
   }
}

#endif

