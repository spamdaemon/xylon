#include <xylon/xsdom/MinLength.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    MinLength::MinLength() throws()
    {}
		      	
    MinLength::~MinLength() throws()
    {}

    void MinLength::accept(Visitor& visitor)
    {
      visitor.visitMinLength(toRef<MinLength>());
    }

  }
}
