#include <xylon/xsdom/Selector.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Selector::Selector() throws()
    {}
		      	
    Selector::~Selector() throws()
    {}

    void Selector::accept(Visitor& visitor)
    {
      visitor.visitSelector(toRef<Selector>());
    }
  }
}
