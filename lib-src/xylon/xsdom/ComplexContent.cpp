#include <xylon/xsdom/ComplexContent.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    ComplexContent::ComplexContent() throws()
    {}
		      	
    ComplexContent::~ComplexContent() throws()
    {}

    void ComplexContent::accept(Visitor& visitor)
    {
      visitor.visitComplexContent(toRef<ComplexContent>());
    }

  }
}
