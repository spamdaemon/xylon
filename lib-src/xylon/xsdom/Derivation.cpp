#include <xylon/xsdom/Derivation.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/Type.h>
#include <xylon/xsdom/xsdom.h>

namespace xylon {
   namespace xsdom {

      Derivation::Derivation() throws()
      {
      }

      Derivation::~Derivation() throws()
      {
      }

      bool Derivation::derivesFromType(const QName& name) const throws()
      {
         // all derivations are made from the anyType
         if (name == QName::xsQName("anyType")) {
            return true;
         }
         if (base()) {
            if (base().name() == name) {

               return true;
            }
            else {
               return base()->isSubtypeOf(name);
            }
         }
         return false;
      }

   }
}
