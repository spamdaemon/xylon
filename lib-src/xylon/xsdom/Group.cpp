#include <xylon/xsdom/Compositor.h>
#include <xylon/xsdom/Group.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Group::Group() throws()
    {}
		      	
    Group::~Group() throws()
    {}

    void Group::accept(Visitor& visitor)
    {
      visitor.visitGroup(toRef<Group>());
    }
  }
}
