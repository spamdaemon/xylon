#ifndef _XYLON_XSDOM_DERIVATION_H
#define _XYLON_XSDOM_DERIVATION_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Derivation : public Node {
      
      /** The default constructor */
    public:
      Derivation() throws();
      
      /** Destructor */
    public:
      ~Derivation() throws();

      /**
       * Set the base type
       * @param base the base type
       */
    public:
      virtual TypeXRef base() const throws() = 0;

      /**
       * Determine if this derivation derives from the specified type.
       * @param name the name of a type
       */
    public:
      virtual bool derivesFromType (const QName& name) const throws();

    };
  }
}
#endif
