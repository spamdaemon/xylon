#ifndef _XYLON_XSDOM_ATTRIBUTE_H
#define _XYLON_XSDOM_ATTRIBUTE_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

#ifndef _XYLON_XREF_H
#include <xylon/XRef.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Attribute : public Node {

      /** The attribute use */
    public:
      enum Use {
	USE_OPTIONAL,
	USE_REQUIRED,
	USE_PROHIBITED
      };

      /** The attribute form */
    public:
      enum Form {
	FORM_QUALIFIED,
	FORM_UNQUALIFIED
      };
      
      /** The default constructor */
    public:
      Attribute() throws();
      
      /** Destructor */
    public:
      ~Attribute() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Get the fixed value.
       * @return the fixed value
       */
    public:
      virtual String fixedValue() const throws() =0;

      /**
       * Get the default value.
       * @return the default value
       */
    public:
      virtual String defaultValue() const throws() =0;

      /**
       * Get the form.
       * @return the prefix form 
       */
    public:
      virtual Form form() const throws() =0;

      /**
       * Set the referenced attribute.
       * @return the referenced attribute
       */
    public:
      virtual AttributeXRef ref () const throws() =0;

      /**
       * Set the type refererenced.
       * @param type the referenced type
       */
    public:
      virtual TypeXRef type () const throws() =0;

      /**
       * Set the simple type defined locally for this attribute.
       * @param type the simple type
       */
    public:
      virtual ::timber::Pointer<SimpleType> attributeType () const throws() =0;
      
      /**
       * Set the use of this attribute
       * @param use the use
       */
    public:
      virtual Use use () const throws() =0;

      /**
       * Get the qualified name for this attribute
       * @return the qualified name for this attribute
       */
    public:
      virtual QName qname() const throws() =0;

      /**
       * Get the qualified name that is to be used for this attribute in an xml document.
       * @param applyForm if true, then the namespace is removed if the attribute form is unqualified.
       * @return the qualified name for this element as it is to be used in an document
       */
    public:
      QName qname(bool applyForm) const throws();

    };
  }
}
#endif
