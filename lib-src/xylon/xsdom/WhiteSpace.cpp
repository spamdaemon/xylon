#include <xylon/xsdom/WhiteSpace.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    WhiteSpace::WhiteSpace() throws()
    {}
		      	
    WhiteSpace::~WhiteSpace() throws()
    {}

    void WhiteSpace::accept(Visitor& visitor)
    {
      visitor.visitWhiteSpace(toRef<WhiteSpace>());
    }

  }
}
