#include <xylon/xsdom/Element.h>
#include <xylon/xsdom/Type.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Element::Element() throws()
    {}
		      	
    Element::~Element() throws()
    {}

    void Element::accept(Visitor& visitor)
    {
      visitor.visitElement(toRef<Element>());
    }

    QName Element::qname(bool applyForm) const throws()
    {
      QName qn=qname();
      if (applyForm && form() != FORM_QUALIFIED) {
	qn = QName(String(),qn.name());
      }
      return qn;
    }
 
  }
}
