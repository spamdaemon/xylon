#include <xylon/xsdom/FirstSets.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/nodes.h>
#include <xylon/xsdom/dom/util.h>
#include <xylon/xsdom/xsdom.h>
#include <xylon/XsdSchema.h>
#include <timber/logging.h>
#include <algorithm>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
   namespace xsdom {
      namespace {
         static const bool LOG_TABLE = true; // true to log the table of first sets

         static Log logger()
         {
            return Log("xylon.xs.FirstSets");
         }

         class SetBuilder;

         /** This is the first set of a node */
         struct FirstSet : public ::timber::Counted
         {
               friend class SetBuilder;

               /**
                * Create a new first set
                */
            public:
               FirstSet(const ::timber::Reference< Node>& node) throws() :
                  _node(node), _visited(false), _allowsEmpty(false)
               {
               }

               /**
                * Destroy this first set
                */
            public:
               ~FirstSet() throws()
               {
               }

               /**
                * Clear this first set. Calling this ensures that any circular references
                * are cleared and won't lead to memory leaks.
                */
            public:
               void clear() throws()
               {
                  _sets.clear();
               }

               /**
                * Mark this set as an empty set
                */
            public:
               void markEmpty() throws()
               {
                  _allowsEmpty = true;
               }

               /**
                * Merge in another first set
                * @param ref a first set
                */
            public:
               void merge(const ::timber::Reference< FirstSet>& fs)
               {
                  _sets.insert(fs);
               }

               /**
                * Merge a name
                * @param ref a first set
                */
            public:
               void mergeName(const QName& name)
               {
                  _elements.insert(name);
               }

               /**
                * Merge a name
                * @param ref a first set
                */
            public:
               void mergeAny(const ::timber::Reference< Any>& any)
               {
                  _any.insert(any);
               }

               /**
                * Determine if the specified element is in the first set of the given enumerable. For an element
                * the first set will only contain itself or the referenced element!
                * @param obj an enumerable
                * @param qname the qname to be matched.
                * @return true if the element is in the first set of this object
                */
            public:
               bool inFirstSet(const QName& qname) const
               {
                  return firstSet().count(qname) == 1;
               }

               /**
                * Is allowed any element.
                * @param obj an enumerable
                * @param ns a namespace uri
                * @return true if there is an xs:any that matches the specified namespace
                */
            public:
               bool matchesAny(const String& ns) const
               {
                  if (_visited) {
                     throw ::std::runtime_error("Schema is ambiguous due to recursion");
                     return false;
                  }
                  bool result = false;

                  _visited = true;
                  for (::std::set< ::timber::Reference< Any> >::const_iterator i = _any.begin(); i != _any.end(); ++i) {
                     if ((*i)->acceptsNamespace(ns)) {
                        result = true;
                        break;
                     }
                  }
                  for (::std::set< ::timber::Reference< FirstSet> >::const_iterator i = _sets.begin(); i != _sets.end(); ++i) {
                     if ((*i)->matchesAny(ns)) {
                        result = true;
                        break;
                     }
                  }
                  _visited = false;
                  return result;
               }

               /**
                * Determine if the first set of the specified enumerable is allowed to be empty. An empty
                * first set might be due to a minOccurs='0'
                * @param obj an enumerable
                * @return true if the first for obj can be empty
                */
            public:
               bool allowsEmpty() const throws()
               {
                  return _allowsEmpty;
               }

               /**
                * Determine if there are elements that are accepted by two different nodes. This
                * method may be used to determine if sequences, choices, etc, are ambiguous.
                * @param n1 a node
                * @param n2 a node
                * @return true if the first sets overlap
                */
            public:
               bool testForConflict(const Reference< FirstSet>& fs) const
               {
                  ::std::set< QName> s1 = firstSet();
                  ::std::set< QName> s2 = fs->firstSet();

                  ::std::set< QName> intersection;
                  ::std::set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), insert_iterator< set< QName> > (
                        intersection, intersection.begin()));
                  if (!intersection.empty()) {
                     return false;
                  }
                  // FIXME: deal with xs:any
                  return true;
               }

               /**
                * Get the set of names that are in the first set of the specified node.
                * @param node a node
                * @return a set of qualified names that may appear the first set of an XSD node
                */
            public:
               ::std::set< QName> firstSet() const
               {
                  ::std::set< QName> res;
                  ::std::set< ::timber::Reference< Any> > anys;
                  collectNames(res, anys);
                  return res;
               }

               /**
                * Get the set of names that are in the first set of the specified node.
                * @param node a node
                * @return a set of qualified names that may appear the first set of an XSD node
                */
            public:
               ::std::set< ::timber::Reference< Any> > anySet() const
               {
                  ::std::set< QName> names;
                  ::std::set< ::timber::Reference< Any> > res;
                  collectNames(names, res);
                  return res;
               }

               /** Validate that a set of names and an any set do not have any intersections */
            private:
               static void checkAnyVsQName(const ::std::set< QName>& names,
                     const ::std::set< ::timber::Reference< Any> >& anys)
               {

                  for (::std::set< QName>::const_iterator i = names.begin(); i != names.end(); ++i) {
                     for (::std::set< ::timber::Reference< Any> >::const_iterator j = anys.begin(); j != anys.end(); ++j) {
                        if ((*j)->acceptsNamespace(i->namespaceURI())) {
                           throw ::std::runtime_error("Ambiguous schema : namespace " + i->namespaceURI().string()
                                 + " is accepted by xs:any");
                        }
                     }
                  }
               }

            private:
               void collectNames(::std::set< QName>& names, ::std::set< ::timber::Reference< Any> >& anys) const
               {
                  if (_visited) {
                     //FIXME: how do we know the schema is valid if we have recursion?
                     throw ::std::runtime_error("Schema is ambiguous due to recursion");
                     return;
                  }
                  _visited = true;
                  for (::std::set< QName>::const_iterator i = _elements.begin(); i != _elements.end(); ++i) {
                     if (!names.insert(*i).second) {
                        // oops, the schema is ambiguous; the element was already inserted by some other first set
                        throw ::std::runtime_error("Ambiguous schema : " + (*i).string().string());
                     }
                  }
                  for (::std::set< ::timber::Reference< Any> >::const_iterator i = _any.begin(); i != _any.end(); ++i) {
                     for (::std::set< ::timber::Reference< Any> >::const_iterator j = anys.begin(); j != anys.end(); ++j) {
                        if ((*i)->conflictsWith(**j)) {
                           throw ::std::runtime_error("Ambiguous schema : two or more xs:any conflict with each other");
                        }
                     }
                     // inserting the i here also make sure that the _any themselves are not in conflict with each other
                     anys.insert(*i);
                  }

                  LogEntry e(logger());
                  e.debugging() << "Collect : " << typeid(*_node) << " == ";
                  for (::std::set< ::timber::Reference< FirstSet> >::const_iterator i = _sets.begin(); i != _sets.end(); ++i) {
                     e << ' ' << typeid(*(*i)->_node);
                     (*i)->collectNames(names, anys);
                  }
                  //	  e << doLog;
                  _visited = false;

                  checkAnyVsQName(names, anys);
               }

               /** A node */
            private:
               ::timber::Reference< Node> _node;

               /** True if this set is marked */
            private:
               mutable bool _visited;

               /** True if the first set can be empty */
            private:
               bool _allowsEmpty;

               /** The qnames of elements that are in the first set */
            private:
               ::std::set< QName> _elements;

               /** A set of xs-any nodes in this set */
            private:
               ::std::set< ::timber::Reference< Any> > _any;

               /** The other first sets that are or'red to build this first set; the first sets are non-overlapping */
            private:
               ::std::set< ::timber::Reference< FirstSet> > _sets;
         };

         static String nodeString(const Node& node) throws()
         {
            const Type* t = dynamic_cast< const Type*> (&node);
            if (t) {
               return t->qname().string();
            }
            const Element* e = dynamic_cast< const Element*> (&node);
            if (e) {
               return e->qname().string();
            }
            const Group* g = dynamic_cast< const Group*> (&node);
            if (g) {
               return g->qname().string();
            }
            const Attribute* a = dynamic_cast< const Attribute*> (&node);
            if (a) {
               return a->qname().string();
            }
            const AttributeGroup* ag = dynamic_cast< const AttributeGroup*> (&node);
            if (ag) {
               return ag->qname().string();
            }
            return String(typeid(node).name());
         }

         // the follow visitor determine whether or not the first set of a node is accepted
         // in the subtree on which the visitor is invoked
         struct ConflictFinder : public Visitor
         {
               ConflictFinder(const FirstSets& fs, const Reference< Node>& n, const Reference< ::xylon::XsdSchema>& schema) :
                  _haveConflict(false), // assume we have no conflict
                        _ambiguous(false), // assume the first conflict is going to be false
                        _firsts(fs), _node(n), _schema(schema)
               {
               }
               ~ConflictFinder() throws()
               {
               }

               bool visitNode(const Pointer< Node>& n)
               {
                  if (n) {
                     //	    LogEntry(logger()).debugging() << "Visiting node : " << nodeString(*n) << doLog;
                     n->accept(*this);
                     return _haveConflict && _ambiguous;
                  }
                  return false;
               }

               bool visitNode(const ElementXRef& ref)
               {
                  Pointer< Node> n = _schema->derefElement(ref);
                  if (n) {
                     //LogEntry(logger()).debugging() << "Visiting element node : " << nodeString(*n) << doLog;
                     return visitNode(n);
                  }
                  return false;
               }

               bool visitNode(const GroupXRef& ref)
               {
                  Pointer< Node> n = _schema->derefGroup(ref);
                  if (n) {
                     //LogEntry(logger()).debugging() << "Visiting group : " << nodeString(*n) << doLog;
                     return visitNode(n);
                  }
                  return false;
               }

               bool visitNode(const AttributeXRef& ref)
               {
                  Pointer< Node> n = _schema->derefAttribute(ref);
                  if (n) {
                     //LogEntry(logger()).debugging() << "Visiting attribute : " << nodeString(*n) << doLog;
                     return visitNode(n);
                  }
                  return false;
               }

               bool visitNode(const AttributeGroupXRef& ref)
               {
                  Pointer< Node> n = _schema->derefAttributeGroup(ref);
                  if (n) {
                     //LogEntry(logger()).debugging() << "Visiting attributeGroup : " << nodeString(*n) << doLog;
                     return visitNode(n);
                  }
                  return false;
               }

               bool visitNode(const KeyXRef& ref)
               {
                  Pointer< Node> n = _schema->derefKey(ref);
                  if (n) {
                     //LogEntry(logger()).debugging() << "Visiting key : " << nodeString(*n) << doLog;
                     return visitNode(n);
                  }
                  return false;
               }

               bool visitNode(const TypeXRef& ref)
               {
                  Pointer< Node> n = _schema->derefType(ref);
                  if (n) {
                     //LogEntry(logger()).debugging() << "Visiting type : " << nodeString(*n) << doLog;
                     return visitNode(n);
                  }
                  return false;
               }

               // set the ambiguous flag if there is currently a conflict
               // and the enumerable can occur a variable number of times
               void markAmbiguous(const Enumerable& obj, const char* msg)
               {
                  Occurrence occurs = obj.occurs();

                  if (_haveConflict && !_ambiguous && (occurs.min() != occurs.max())) {
                     if (msg) {
                        logger().debugging(msg);
                     }
                     _ambiguous = true;
                  }
               }
               void visitAll(const Reference< All>& obj)
               {
                  for (size_t i = 0; i < obj->items().size() && !_haveConflict; ++i) {
                     Reference< Enumerable> n = obj->items()[i];
                     visitNode(n);
                  }
                  markAmbiguous(*obj, "xs:all: found an ambiguous conflict");
               }

               void visitAny(const Reference< Any>& obj)
               {
                  _haveConflict = _firsts.testForConflict(obj, _node);
                  markAmbiguous(*obj, "xs:any: found an ambiguous conflict");
               }

               void visitChoice(const Reference< Choice>& obj)
               {
                  for (size_t i = 0; i < obj->items().size() && !_haveConflict; ++i) {
                     Reference< Enumerable> n = obj->items()[i];
                     visitNode(n);
                  }
                  markAmbiguous(*obj, "xs:choice: found an ambiguous conflict");
               }

               void visitUrType(const Reference< UrType>&)
               {
                  // nothing to do for ur-type
               }

               void visitComplexContent(const Reference< ComplexContent>& obj)
               {
                  visitNode(obj->derivation());
               }

               void visitComplexType(const Reference< ComplexType>& obj)
               {
                  // either we have a content type or an enumerable, but not both
                  visitNode(obj->contentType());
                  visitNode(obj->enumerable());
               }

               void visitElement(const Reference< Element>& obj)
               {
                  _haveConflict = _firsts.testForConflict(obj, _node);
                  markAmbiguous(*obj, "xs:element: found an ambiguous conflict");
               }

               void visitExtension(const Reference< Extension>& obj)
               {
                  visitNode(obj->enumerable());
                  if (!_haveConflict && !_ambiguous) {
                     visitNode(obj->base());
                  }
               }

               void visitGroup(const Reference< Group>& obj)
               {
                  if (obj->ref()) {
                     visitNode(obj->ref());
                     markAmbiguous(*obj, "xs:group: found an ambiguous conflict");
                  }
                  else {
                     visitNode(obj->enumerable());
                  }
               }

               void visitRestriction(const Reference< Restriction>& obj)
               {
                  visitNode(obj->enumerable());
               }

               void visitSequence(const Reference< Sequence>& obj)
               {
                  for (size_t i = obj->items().size(); i-- > 0 && !_haveConflict;) {
                     Reference< Enumerable> n = obj->items()[i];
                     visitNode(n);
                     if (!_firsts.allowsEmpty(n)) {
                        break;
                     }
                  }
                  markAmbiguous(*obj, "xs:group: found an ambiguous conflict");
               }

               bool _haveConflict;
               bool _ambiguous; // true if the conflict is ambiguous

               const FirstSets& _firsts;
               const Reference< Node> _node;

               Reference< ::xylon::XsdSchema> _schema;
         };

         struct StructureChecker : public Visitor
         {
               StructureChecker(const FirstSets& firsts, const Reference< ::xylon::XsdSchema>& schema) throws() :
                  _firsts(firsts), _schema(schema)
               {
               }

               ~StructureChecker() throws()
               {
               }

               void visitNode(const Pointer< Node>& n)
               {
                  if (n) {
                     //	    LogEntry(logger()).debugging() << "StructureChecker: Visiting node : " << nodeString(*n) << doLog;
                     n->accept(*this);
                  }
               }

               void visitAll(const Reference< All>& obj)
               {
                  for (size_t i = 0; i < obj->items().size(); ++i) {
                     visitNode(obj->items()[i]);
                  }
               }

               void visitAny(const Reference< Any>&)
               {
               }

               void visitUrType(const Reference< UrType>&)
               {
                  // nothing to do for ur-type
               }

               void visitChoice(const Reference< Choice>& obj)
               {
                  for (size_t i = 0; i < obj->items().size(); ++i) {
                     visitNode(obj->items()[i]);
                  }
               }

               void visitComplexContent(const Reference< ComplexContent>& obj)
               {
                  visitNode(obj->derivation());
               }

               void visitComplexType(const Reference< ComplexType>& obj)
               {
                  visitNode(obj->contentType());
                  visitNode(obj->enumerable());
               }

               void visitElement(const Reference< Element>& obj)
               {
                  visitNode(obj->elementType());
               }

               void visitExtension(const Reference< Extension>& obj)
               {
                  if (obj->enumerable()) {
                     ConflictFinder finder(_firsts, obj->enumerable(), _schema);
                     if (finder.visitNode(obj->base())) {
                        throw ::std::runtime_error("Conflict found in xs:extension");
                     }
                  }
               }

               void visitGroup(const Reference< Group>& obj)
               {
                  visitNode(obj->enumerable());
               }

               void visitRestriction(const Reference< Restriction>& obj)
               {
                  visitNode(obj->enumerable());
               }

               void visitSchema(const Reference< Schema>& obj)
               {
                  const vector< Reference< Node> > nodes = obj->nodes();
                  for (size_t i = 0; i < nodes.size(); ++i) {
                     visitNode(nodes[i]);
                  }
               }

               void visitSequence(const Reference< Sequence>& obj)
               {
                  if (obj->items().empty()) {
                     return;
                  }
                  visitNode(obj->items()[0]);
                  for (size_t i = 1; i < obj->items().size(); ++i) {
                     visitNode(obj->items()[i]);

                     for (size_t j = i; j-- > 0;) {
                        Reference< Node> n = obj->items()[j];
                        ConflictFinder cf(_firsts, obj->items()[i], _schema);
                        if (cf.visitNode(n)) {
                           throw ::std::runtime_error("Conflict found in xs:sequence");
                        }
                        if (!_firsts.allowsEmpty(n)) {
                           // don't need to check
                           break;
                        }
                     }
                  }
               }

               const FirstSets& _firsts;
               Reference< ::xylon::XsdSchema> _schema;
         };

         // this visitor actually builds the firstset for node
         struct SetBuilder : public Visitor
         {
               SetBuilder(const Reference< ::xylon::XsdSchema>& schema) throws() :
                  _queue(getSchemaNodes(schema)), _schema(schema), _any(createAny(Occurrence::unbounded()))
               {
               }

               ~SetBuilder() throws()
               {
                  for (map< Reference< Node> , Reference< FirstSet> >::iterator i = _sets.begin(); i != _sets.end(); ++i) {
                     i->second->clear();
                  }
               }

               void visitNodes()
               {
                  while (!_queue.empty()) {
                     Reference< Node> n = _queue.back();
                     _queue.pop_back();
                     visitNode(n);
                  }
               }

               void enqueueNode(const Pointer< Node>& n)
               {
                  if (n) {
                     _queue.push_back(n);
                  }
               }

               Reference< FirstSet> getSet(const Reference< Node>& p)
               {
                  if (_sets.count(p) == 0) {
                     _sets.insert(map< Reference< Node> , Reference< FirstSet> >::value_type(p, new FirstSet(p)));
                  }
                  return _sets.find(p)->second;
               }

               // already processed?
               bool isProcessed(const Reference< Node>& p)
               {
                  return _firstSets.count(p) == 1;
               }

               void visitNode(const Pointer< Node>& n)
               {
                  if (n) {
                     getSet(n);
                     LogEntry(logger()).debugging() << "Visiting node : " << nodeString(*n) << doLog;
                     n->accept(*this);
                  }
               }

               // merge a source enumerable into the target enumerable
               bool merge(const Reference< Node>& p, const Reference< Node>& c, bool doMerge = true)
               {
                  // first, visit the child
                  getSet(c);
                  visitNode(c);

                  Reference< FirstSet> fsP = getSet(p);
                  Reference< FirstSet> fsC = getSet(c);

                  // if the child is optional then place into the empty sets
                  {
                     Pointer< Enumerable> xenum = c.tryDynamicCast< Enumerable> ();
                     if (xenum && xenum->occurs().min() == 0) {
                        fsC->markEmpty();
                     }
                  }

                  // the object itself could have a minOccurs=0, making the whole thing optional
                  // this is case for example for this:
                  //  <complexContext><sequence minOccurs="0"/></complexContent>
                  {
                     Pointer< Enumerable> xenum = p.tryDynamicCast< Enumerable> ();
                     if (xenum && xenum->occurs().min() == 0) {
                        fsP->markEmpty();
                     }
                  }

                  if (doMerge) {
                     // we're not going to check here if the other elements in the set
                     // are compatible; that is done somewhere else
                     fsP->merge(fsC);
                  }

                  // c is optional if it has a minOccurs=0 or can be empty itself
                  return fsC->allowsEmpty();
               }

               void visitAll(const Reference< All>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  bool canBeEmpty = true;
                  for (size_t i = 0; i < obj->items().size(); ++i) {
                     if (!merge(obj, obj->items()[i])) {
                        canBeEmpty = false;
                     }
                  }
                  if (canBeEmpty) {
                     getSet(obj)->markEmpty();
                  }
               }

               void visitAny(const Reference< Any>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  _firstSets[obj].insert(obj);
                  getSet(obj)->mergeAny(obj);
               }

               void visitChoice(const Reference< Choice>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  bool canBeEmpty = false;
                  for (size_t i = 0; i < obj->items().size(); ++i) {
                     if (merge(obj, obj->items()[i])) {
                        // one choice can be empty -> thus the choice can be empty
                        canBeEmpty = true;
                     }
                  }
                  if (canBeEmpty) {
                     getSet(obj)->markEmpty();
                  }
               }

               void visitComplexContent(const Reference< ComplexContent>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  ::timber::Pointer< Derivation> d = obj->derivation();
                  if (d) {
                     if (merge(obj, d)) {
                        getSet(obj)->markEmpty();
                     }
                  }
                  else {
                     logger().warn("Complex content has no restriction or extension");
                  }
               }

               void visitComplexType(const Reference< ComplexType>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  // either we have a content type or an enumerable, but not both
                  ::timber::Pointer< ContentType> ct = obj->contentType();
                  if (ct) {
                     if (merge(obj, ct)) {
                        getSet(obj)->markEmpty();
                     }
                     return;
                  }

                  ::timber::Pointer< Enumerable> e = obj->enumerable();
                  if (e) {
                     if (merge(obj, e)) {
                        getSet(obj)->markEmpty();
                     }
                     return;
                  }

                  // optional
                  getSet(obj)->markEmpty();
               }

               void visitElement(const Reference< Element>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  XRef< Type> typeRef;
                  Pointer< Type> anonType;

                  XRef< Element> ref = obj->ref();
                  if (ref) {
                     Pointer< Element> elmt = _schema->derefElement(ref);
                     _firstSets[obj].insert(elmt);
                     getSet(obj)->mergeName(ref.name());

                     typeRef = elmt->type();
                     anonType = elmt->elementType();
                  }
                  else {
                     _firstSets[obj].insert(obj);
                     getSet(obj)->mergeName(obj->qname(true));

                     typeRef = obj->type();
                     anonType = obj->elementType();
                  }
                  if (typeRef) {
                     enqueueNode(_schema->derefType(typeRef));
                  }
                  else if (anonType) {
                     enqueueNode(anonType);
                  }
               }

               void visitExtension(const Reference< Extension>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  XRef< Type> base = obj->base();
                  if (base) {
                     // recursively check the base type; this is OK, because the basetype cannot recursive
                     // refer to this type through extension or restriction
                     if (!merge(obj, _schema->derefType(base))) {
                        // ok, base is non-empty, so we don't have to check the first of the enumerable
                        // and just visit, to build up the set
                        visitNode(obj->enumerable());
                        return;
                     }
                  }

                  Pointer< Enumerable> e = obj->enumerable();
                  if (e) {
                     if (merge(obj, e)) {
                        getSet(obj)->markEmpty();
                     }
                     return;
                  }

                  getSet(obj)->markEmpty();
               }

               void visitGroup(const Reference< Group>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  XRef< Group> ref = obj->ref();
                  if (ref) {
                     Pointer< Enumerable> e = _schema->derefGroup(ref)->enumerable();
                     if (e) {
                        if (merge(obj, e)) {
                           getSet(obj)->markEmpty();
                        }
                     }
                     else {
                        logger().warn("Group has no content");
                     }
                  }
                  else if (obj->qname()) {
                     // do not process a top-level group, i.e. one that has a name or xref
                     visitNode(obj->enumerable());
                  }
                  else {
                     Pointer< Enumerable> e = obj->enumerable();
                     if (e) {
                        if (merge(obj, e)) {
                           getSet(obj)->markEmpty();
                        }
                     }
                     else {
                        logger().warn("Group has no content");
                     }
                  }
               }

               void visitRestriction(const Reference< Restriction>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];

                  // restrictions are strange, because they must override
                  // the basetype type completely, but still be consistent with it
                  // we're not going to check self-consistency here
                  XRef< Type> base = obj->base();

                  // enqueue the basetype, but don't use the result here
                  enqueueNode(_schema->derefType(base));

                  Pointer< Enumerable> e = obj->enumerable();

                  if (e) {
                     if (merge(obj, e)) {
                        getSet(obj)->markEmpty();
                     }
                  }
                  else {
                     //FIXME: if no enumerable is specified, should we set empty???
                     getSet(obj)->markEmpty();
                  }

                  // FIXME: should we check and make sure that the restriction is a proper
                  // subset of the base type:
                  // if the restriction allows empty then so must the base type
                  // each element in the first set of the restriction must also be in the first set of the base type
               }

               void visitSchema(const Reference< Schema>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];

                  const vector< Reference< Node> > nodes = obj->nodes();
                  for (size_t i = 0; i < nodes.size(); ++i) {
                     visitNode(nodes[i]);
                  }
               }

               void visitSequence(const Reference< Sequence>& obj)
               {
                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];

                  bool canBeEmpty = true;
                  for (size_t i = 0; i < obj->items().size(); ++i) {
                     Pointer< Enumerable> node = obj->items()[i];
                     if (!merge(obj, node, canBeEmpty)) {
                        // cannot be entirely empty
                        canBeEmpty = false;
                     }
                  }
                  if (canBeEmpty) {
                     getSet(obj)->markEmpty();
                  }
               }

               void visitUrType(const Reference< UrType>& obj)
               {

                  if (isProcessed(obj)) {
                     return;
                  }
                  _firstSets[obj];
                  if (obj->isAnyType()) {
                     getSet(obj)->markEmpty();
                     getSet(obj)->mergeAny(_any);
                  }
                  else {
                     // FIXME: unsupported ur-type
                     assert("Unsupported ur-type");
                  }
               }

               vector< Reference< Node> > _queue;
               Reference< ::xylon::XsdSchema> _schema;
               map< Reference< Node> , set< Reference< Enumerable> > > _firstSets;
               set< Reference< Node> > _emptySets;
               map< Reference< Node> , Reference< FirstSet> > _sets;
               Reference< Any> _any;
         };
      }

      FirstSets::FirstSets(const Reference< ::xylon::XsdSchema>& s)
      {
         SetBuilder v(s);
         v.visitNodes();

         for (map< Reference< Node> , Reference< FirstSet> >::iterator i = v._sets.begin(); i != v._sets.end(); ++i) {
            const Reference< Node> n = i->first;
            const FirstSet& fs = *(i->second);

            if (fs.allowsEmpty()) {
               _emptySets.insert(n);
            }
            const ::std::set< QName> firstSet = fs.firstSet();
            const ::std::set< ::timber::Reference< Any> > anySet = fs.anySet();
            ::std::vector< ::timber::Reference< Any> >& any = _any[n];

            _firstSets[n] = firstSet;
            any.insert(any.end(), anySet.begin(), anySet.end());

            if (LOG_TABLE) {
               QName x;
               Pointer< Element> xe = i->first.tryDynamicCast< Element> ();
               if (xe) {
                  if (xe == i->first) {
                     // ignore self references
                  }
                  else if (xe->ref()) {
                     x = xe->ref().name();
                  }
                  else {
                     x = xe->qname();
                  }
               }
               else {
                  Pointer< ComplexType> xt = i->first.tryDynamicCast< ComplexType> ();
                  if (xt) {
                     x = xt->qname();
                  }
                  else {
                  }
               }

               if (x) {
                  LogEntry log(logger());
                  log.debugging() << x << " : ( ";
                  for (::std::set< QName>::const_iterator j = firstSet.begin(); j != firstSet.end(); ++j) {
                     log << (*j) << ' ';
                  }

                  if (!anySet.empty()) {
                     log << " *any* ";
                  }

                  log << ")";
                  if (fs.allowsEmpty()) {
                     log << "?";
                  }
                  log << doLog;
               }
            }
         }

         // check the structure of the schema
         StructureChecker cf(*this, s);
         vector< Reference< Node> > nodes = getSchemaNodes(s);

         for (size_t i = 0; i < nodes.size(); ++i) {
            cf.visitNode(nodes[i]);
         }
      }

      FirstSets::~FirstSets() throws()
      {
      }

      bool FirstSets::inFirstSet(const Reference< Node>& obj, const QName& qname) const throws()
      {
         map< Reference< Node> , set< QName> >::const_iterator i = _firstSets.find(obj);
         // should really always find the entry for the enumerable; if not found, then there's a problem
         // either with the implementation, or a client side bug
         if (i == _firstSets.end()) {

            LogEntry(logger()).warn() << "Enumerable not found: " << nodeString(*obj) << "  when looking for " << qname
                  << "  :  this may indicate a bug" << doLog;
            return false;
         }

         return i->second.count(qname) == 1;
      }

      bool FirstSets::matchesAny(const ::timber::Reference< Node>& obj, const String& ns) const throws()
      {
         map< Reference< Node> , vector< Reference< Any> > >::const_iterator i = _any.find(obj);
         if (i == _any.end()) {
            return false;
         }
         for (size_t j = 0, n = i->second.size(); j < n; ++j) {
            if (i->second[j]->acceptsNamespace(ns)) {
               return true;
            }
         }
         return false;
      }

      bool FirstSets::allowsEmpty(const Reference< Node>& obj) const throws()
      {
         bool res = _emptySets.count(obj) == 1;

         // FIXME: remove this eventually
         Pointer< Group> g = obj.tryDynamicCast< Group> ();
         if (g && g->ref()) {
            if (allowsEmpty(g->ref()->enumerable()) && !res) {
               LogEntry(logger()).bug() << "Inconsistent allowsEmpty: " << g->ref() << doLog;
               assert(!allowsEmpty(g->ref()->enumerable()) || res);
            }
         }
         return res;
      }

      set< QName> FirstSets::firstSetOf(const Reference< Node>& node) const throws()
      {
         map< Reference< Node> , set< QName> >::const_iterator i = _firstSets.find(node);
         if (i == _firstSets.end()) {
            return set< QName> ();
         }
         else {
            return i->second;
         }
      }

      bool FirstSets::testForConflict(const Reference< Node>& n1, const Reference< Node>& n2) const throws()
      {
         // check the element sets first
         const set< QName> defaultSet;

         const set< QName>* q1 = &defaultSet;
         const set< QName>* q2 = &defaultSet;

         if (_firstSets.find(n1) != _firstSets.end()) {
            q1 = &_firstSets.find(n1)->second;
         }
         if (_firstSets.find(n2) != _firstSets.end()) {
            q2 = &_firstSets.find(n2)->second;
         }

         for (set< QName>::const_iterator i = q1->begin(); i != q1->end(); ++i) {
            if (q2->find(*i) != q2->end()) {
               // ok, they overlap
               return true;
            }
         }

         const vector< Reference< Any> > defaultAny;
         const vector< Reference< Any> >* a1 = &defaultAny;
         const vector< Reference< Any> >* a2 = &defaultAny;
         if (_any.find(n1) != _any.end()) {
            a1 = &_any.find(n1)->second;
         }
         if (_any.find(n2) != _any.end()) {
            a1 = &_any.find(n2)->second;
         }
         for (size_t i = 0; i < a1->size(); ++i) {
            for (size_t j = 0; j < a2->size(); ++j) {
               if ((*a1)[i]->conflictsWith(*(*a2)[j])) {
                  return true;
               }
            }
         }

         for (size_t i = 0; i < a1->size(); ++i) {
            for (set< QName>::const_iterator j = q2->begin(); j != q2->end(); ++j) {
               if ((*a1)[i]->acceptsNamespace(j->namespaceURI())) {
                  return true;
               }
            }
         }

         for (size_t i = 0; i < a2->size(); ++i) {
            for (set< QName>::const_iterator j = q1->begin(); j != q1->end(); ++j) {
               if ((*a2)[i]->acceptsNamespace(j->namespaceURI())) {
                  return true;
               }
            }
         }

         return false;
      }

      ::std::vector< ::timber::Reference< Any> > FirstSets::anyOf(const ::timber::Reference< Node>& node) const throws()
      {
         ::std::vector< ::timber::Reference< Any> > res;
         ::std::map< ::timber::Reference< Node>, ::std::vector< ::timber::Reference< Any> > >::const_iterator i =
               _any.find(node);
         if (i != _any.end()) {
            res = i->second;
         }
         return res;
      }

   }
}
