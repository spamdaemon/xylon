#include <xylon/xsdom/Choice.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Choice::Choice() throws()
    {}
		      	
    Choice::~Choice() throws()
    {}

    void Choice::accept(Visitor& visitor)
    {
      visitor.visitChoice(toRef<Choice>());
    }

  }
}
