#ifndef _XYLON_XSDOM_ANYATTRIBUTE_H
#define _XYLON_XSDOM_ANYATTRIBUTE_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

#include <set>

namespace xylon {
   namespace xsdom {
      /**
       * A schema node. This is the base class for all
       * schema elements.
       */
      class AnyAttribute : public Node
      {

            /** How to process the contents */
         public:
            enum ProcessContents
            {
               LAX, SKIP, STRICT
            };

            /** The default constructor */
         public:
            AnyAttribute() throws();

            /** Destructor */
         public:
            ~AnyAttribute() throws();

            /**
             * Get the target namespace in which this any instance
             * is defined.
             * @return the namespace in which this object is defined
             */
         public:
            virtual String targetNamespace() const throws() = 0;

            /**
             * Accept a node visitor
             * @param visitor a visitor
             */
         public:
            void accept(Visitor& visitor);

            /**
             * Get the namespaces.
             * @return the namespace that should be matched.
             */
         public:
            virtual const ::std::set< String>& namespaces() const throws() = 0;

            /**
             * Get how contents should be processed.
             * @return how to process contents
             */
         public:
            virtual ProcessContents getProcessContents() const throws() = 0;

            /**
             * Check if the specified namespace is accepted by this any object.
             * @param ns a namespace uri or 0 if not a namespace
             * @return true if ns matches an valid namespace
             */
         public:
            virtual bool acceptsNamespace(const String& ns) const throws();
      };
   }
}
#endif
