#ifndef _XYLON_XSDOM_FIRSSETS_H
#define _XYLON_XSDOM_FIRSSETS_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _XYLON_XSDOM_H
#include <xylon/xsdom/xsdom.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#include <map>
#include <set>

namespace xylon {
   class XsdSchema;

   namespace xsdom {
      class Node;
      class Enumerable;
      class Any;

      /**
       * The first contains the pointer to elements that can appear
       * below a given enumerable defined in a schema. The elements in the
       * first set include also abstract elements.
       * Building the first set is not actually that easy, because schemas
       * can refer types and elements in a recursive way.<br/>
       */
      class FirstSets
      {
            FirstSets();

            /**
             * Create a first set for a given schema.
             * @param s a schema
             */
         public:
            FirstSets(const ::timber::Reference< ::xylon::XsdSchema>& s);
            /** Destructor */
         public:
            virtual ~FirstSets() throws();

            /**
             * Determine if the specified element is in the first set of the given enumerable. For an element
             * the first set will only contain itself or the referenced element!
             * @param obj an enumerable
             * @param qname the qname to be matched.
             * @return true if the element is in the first set of this object
             */
         public:
            virtual bool inFirstSet(const ::timber::Reference< Node>& obj, const QName& qname) const throws();

            /**
             * Is allowed any element.
             * @param obj an enumerable
             * @param ns a namespace uri
             * @return true if there is an xs:any that matches the specified namespace
             */
         public:
            virtual bool matchesAny(const ::timber::Reference< Node>& obj, const String& ns) const throws();

            /**
             * Determine if the first set of the specified enumerable is allowed to be empty. An empty
             * first set might be due to a minOccurs='0'
             * @param obj an enumerable
             * @return true if the first for obj can be empty
             */
         public:
            virtual bool allowsEmpty(const ::timber::Reference< Node>& obj) const throws();

            /**
             * Determine if there are elements that are accepted by two different nodes. This
             * method may be used to determine if sequences, choices, etc, are ambiguous.
             * @param n1 a node
             * @param n2 a node
             * @return true if the first sets overlap
             */
         public:
            virtual bool
                  testForConflict(const ::timber::Reference< Node>& n1, const ::timber::Reference< Node>& n2) const throws();

            /**
             * Get the set of names that are in the first set of the specified node.
             * @param node a node
             * @return a set of qualified names that may appear the first set of an XSD node
             */
         public:
            virtual ::std::set< QName> firstSetOf(const ::timber::Reference< Node>& node) const throws();

            /**
             * Get the xs:any nodes that can appear in the first set.
             * @return the any nodes that can appear in the first set of the specified node.
             */
         public:
            virtual ::std::vector< ::timber::Reference< Any> >
                  anyOf(const ::timber::Reference< Node>& node) const throws();

            /**
             * Get the set of names that are in the first set of the specified node.
             * @param node a node
             * @return a set of qualified names that may appear the first set of an XSD node
             */
         public:
            inline ::std::set< QName> operator()(const ::timber::Reference< Node>& node) const throws()
            {
               return firstSetOf(node);
            }

            /** The first sets for each enumerable maps to an ElementObject or Any */
         private:
            ::std::map< ::timber::Reference< Node>, ::std::set< QName> > _firstSets;

            /** The any nodes that may be contained in this first set */
         private:
            ::std::map< ::timber::Reference< Node>, ::std::vector< ::timber::Reference< Any> > > _any;

            /** The set of enumerables that allow an empty set */
         private:
            ::std::set< ::timber::Reference< Node> > _emptySets;
      };
   }
}
#endif
