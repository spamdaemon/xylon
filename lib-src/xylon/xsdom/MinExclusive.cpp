#include <xylon/xsdom/MinExclusive.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    MinExclusive::MinExclusive() throws()
    {}
		      	
    MinExclusive::~MinExclusive() throws()
    {}

    void MinExclusive::accept(Visitor& visitor)
    {
      visitor.visitMinExclusive(toRef<MinExclusive>());
    }

  }
}
