#include <xylon/xsdom/Extension.h>
#include <xylon/xsdom/Element.h>
#include <xylon/xsdom/Enumerable.h>
#include <xylon/xsdom/Attribute.h>
#include <xylon/xsdom/AnyAttribute.h>
#include <xylon/xsdom/AttributeGroup.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
   namespace xsdom {

      Extension::Extension() throws()
      {
      }

      Extension::~Extension() throws()
      {
      }

      void Extension::accept(Visitor& visitor)
      {
         visitor.visitExtension(toRef< Extension> ());
      }
   }
}
