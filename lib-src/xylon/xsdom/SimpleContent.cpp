#include <xylon/xsdom/SimpleContent.h>
#include <xylon/xsdom/Visitor.h>

using namespace ::std;
using namespace ::timber;

namespace xylon {
  namespace xsdom {

    SimpleContent::SimpleContent() throws()
    {}
		      	
    SimpleContent::~SimpleContent() throws()
    {}

    void SimpleContent::accept(Visitor& visitor)
    {
      visitor.visitSimpleContent(toRef<SimpleContent>());
    }

  }
}
