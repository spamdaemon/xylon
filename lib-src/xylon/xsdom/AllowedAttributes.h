#ifndef _XYLON_XSDOM_ALLOWEDATTRIBUTES_H
#define _XYLON_XSDOM_ALLOWEDATTRIBUTES_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#include <set>
#include <map>
#include <vector>

namespace xylon {
   class XsdSchema;

   namespace xsdom {
      class Attribute;
      class Type;

      /**
       * This class is used to represent the set of allowed attributes for a complex type or element in a schema.
       */
      class AllowedAttributes
      {
            /** Attributes names that are allowed, optional, or prohibited */
         public:
            class AttributeNames
            {
                  /**
                   * Default constructor.
                   */
               public:
                  AttributeNames() throws();
                  /**
                   * Constructor for the attributes of a complex type.
                   */
               public:
                  AttributeNames(const ::timber::Reference< ::xylon::xsdom::Type>& type) throws();

                  /**
                   * Destructor.
                   */
               public:
                  ~AttributeNames() throws();

                  /**
                   * Get the required attributes.
                   * @return a vector of required attributes
                   */
               public:
                  ::std::vector< QName> requiredAttributes() const throws();

                  /**
                   * Get the optional attributes.
                   * @return a vector of optional attributes
                   */
               public:
                  ::std::vector< QName> optionalAttributes() const throws();

                  /**
                   * Get the prohibited attributes.
                   * @return a vector of prohibited attributes
                   */
               public:
                  ::std::vector< QName> prohibitedAttributes() const throws();

                  /**
                   * Get the allowed namespaces.
                   * @return the allowed namespaces
                   */
               public:
                  ::std::vector< String> allowedNamespaces() const throws();

                  /**
                   * Get the prohibited namespaces
                   * @return the prohibited namespaces
                   */
               public:
                  ::std::vector< String> prohibitedNamespaces() const throws();

                  /**
                   * Add a required attribute.
                   * @param name the name of the attribute as it must appear in a document
                   */
               public:
                  void addRequiredAttribute(const QName& name);

                  /**
                   * Add a prohibited attribute.
                   * @param name the name of a prohibited attribute
                   */
               public:
                  void addProhibitedAttribute(const QName& name);

                  /**
                   * Add an optional attribute.
                   * @param name the name of an optional attribute
                   */
               public:
                  void addOptionalAttribute(const QName& name);

                  /**
                   * True if any attribute is allowed on an element.
                   * @return true if any element is allowed.
                   */
               public:
                  bool isAnyAttributeAllowed() const throws();

                  /**
                   * Indicate whether any attribute whatsoever is allowed.
                   * @param allowed true if any attribute is indeed allowed
                   */
               public:
                  void setAnyAttributeAllowed (bool allowed) throws();

                  /**
                   * Add a namespace for any attribute.
                   * @param name the namespace name
                   */
               public:
                  void addAllowedNamespace(const String& name);

                  /**
                   * Add a disallowed namespace.
                   * @param name a disallowed namespace
                   */
               public:
                  void addProhibitedNamespace(const String& name);

                  /** The required, prohibited, and optional attributes */
               private:
                  ::std::set< QName> _required, _prohibited, _optional;

                  /** The required and prohibited namespace for attribute names*/
               private:
                  ::std::set< String> _allowedNS, _disallowedNS;

                  /** True if any attribute is allowed */
               private:
                  bool _anyAttributeAllowed;
            };

            /** The element attributes */
         private:
            typedef ::std::map< QName, AttributeNames> Attributes;

            /**
             * Create a new allowed attributes object.
             * @param schema a schema
             */
         public:
            AllowedAttributes(const ::timber::Reference< ::xylon::XsdSchema>& schema) throws();

            /**
             * Destructor.
             */
         public:
            ~AllowedAttributes() throws();

            /**
             * Get the element attributes.
             * @param qname an element name
             * @return the attributes allowed on the specified element or 0
             */
         public:
            const AttributeNames* elementAttributes(const QName& qname) const throws();

            /**
             * Get the types attributes.
             * @param qname a complex type name
             * @return the attributes allowed on the specified complex type or 0
             */
         public:
            const AttributeNames* typeAttributes(const QName& qname) const throws();

            /**
             * The names of attributes allowed on an element
             */
         private:
            Attributes _elementAttributes;

            /** The names of attributes allowed on a complex type */
         private:
            Attributes _typeAttributes;
      };

   }

}

#endif
