#include <xylon/xsdom/ComplexType.h>
#include <xylon/xsdom/ContentType.h>
#include <xylon/xsdom/Derivation.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
   namespace xsdom {

      ComplexType::ComplexType() throws()
      {
      }

      ComplexType::~ComplexType() throws()
      {
      }

      void ComplexType::accept(Visitor& visitor)
      {
         visitor.visitComplexType(toRef< ComplexType> ());
      }

      bool ComplexType::isSubtypeOf(const QName& name) const throws()
      {

         // a type cannot be a subtype of itself
         if (qname() == name) {
            return false;
         }
         // a type is always a subtype of anytype through restriction or extension.
         if (name == QName::xsQName("anyType")) {
            return true;
         }

         Pointer< ContentType> ct = contentType();

         // by default, if no content type is specified, then the type is a restriction
         // of anyType and we've already checked that above
         if (!ct) {
            return false;
         }

         Pointer< Derivation> derived = ct->derivation();
         if (derived) {
            // no derivation, then we cannot say anything
            return derived->derivesFromType(name);
         }
         return false;
      }
   }
}

