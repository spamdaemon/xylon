#ifndef _XYLON_XSDOM_MAXINCLUSIVE_H
#define _XYLON_XSDOM_MAXINCLUSIVE_H

#ifndef _XYLON_XSDOM_FACET_H
#include <xylon/xsdom/Facet.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class MaxInclusive : public Facet {
      
      /** The default constructor */
    public:
      MaxInclusive() throws();
      
      /** Destructor */
    public:
      ~MaxInclusive() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
