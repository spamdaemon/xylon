#ifndef _XYLON_XSDOM_ELEMENT_H
#define _XYLON_XSDOM_ELEMENT_H

#ifndef _XYLON_XSDOM_ENUMERABLE_H
#include <xylon/xsdom/Enumerable.h>
#endif

namespace xylon {
   namespace xsdom {
      /**
       * A schema node. This is the base class for all
       * schema elements.
       */
      class Element : public Enumerable
      {

            /** The element form */
         public:
            enum Form
            {
               FORM_QUALIFIED, FORM_UNQUALIFIED
            };

            /** Whether extensions or restrictions are blocked */
         public:
            enum Block
            {
               BLOCK_NONE, BLOCK_EXTENSION, BLOCK_RESTRICTION, BLOCK_SUBSTITUTION, BLOCK_ALL
            };

            /** Whether extensions or restrictions are blocked */
         public:
            enum Final
            {
               FINAL_NONE, FINAL_EXTENSION, FINAL_ALL
            };

            /** The default constructor */
         public:
            Element() throws();

            /** Destructor */
         public:
            ~Element() throws();

            /**
             * Accept a node visitor
             * @param visitor a visitor
             */
         public:
            void accept(Visitor& visitor);

            /**
             * Get the qualified name that is to be used for this element in an xml document.
             * @param applyForm if true, then the namespace is removed if the attribute form is unqualified.
             * @return the qualified name for this element as it is to be used in an document
             */
         public:
            QName qname(bool applyForm) const throws();

            /**
             * Get the qualified name for this element
             * @return the qualified name for this element
             */
         public:
            virtual QName qname() const throws() = 0;

            /**
             * Determine if this element is nillable. If true, then @code xsi:nil @endcode
             * can be used to indiate that the content of the element is null, rather than empty.
             * @return true if this element is nillable.
             */
         public:
            virtual bool isNillable() const throws() = 0;

            /**
             * Determine if this type is abstract
             * @return true if this type is abstract
             */
         public:
            virtual bool isAbstract() const throws() = 0;

            /**
             * Determine if subtypes can be substituted for this type
             * @return the type of substitutions for this type
             */
         public:
            virtual int block() const throws() = 0;

            /**
             * Determine if how this type can be derived.
             * @return the type of derivation allowed
             */
         public:
            virtual int final() const throws() = 0;

            /**
             * Get the form.
             * @return the prefix form
             */
         public:
            virtual Form form() const throws() = 0;

            /**
             * Get the fixed value.
             * @return the fixed value
             */
         public:
            virtual String fixedValue() const throws() = 0;

            /**
             * Get the default value.
             * @return the default value
             */
         public:
            virtual String defaultValue() const throws() = 0;

            /**
             * Get the referenced element.
             * @return the referenced element
             */
         public:
            virtual ElementXRef ref() const throws() = 0;

            /**
             * Get the referenced type.
             * @return the referenced type
             */
         public:
            virtual TypeXRef type() const throws() = 0;

            /**
             * Get the element that defines the substitution group for this element.
             * @return the substitution group
             */
         public:
            virtual ElementXRef substitutionGroup() const throws() = 0;

            /**
             * Get the locally defined type.
             * @return a locally defined element type.
             */
         public:
            virtual ::timber::Pointer< Type> elementType() const throws() = 0;
      };
   }
}
#endif
