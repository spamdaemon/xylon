#ifndef _XYLON_XSDOM_REDEFINE_H
#define _XYLON_XSDOM_REDEFINE_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Redefine : public Node {
      
      /** The default constructor */
    public:
      Redefine() throws();
      
      /** Destructor */
    public:
      ~Redefine() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
