#ifndef _XYLON_XSDOM_SIMPLECONTENT_H
#define _XYLON_XSDOM_SIMPLECONTENT_H

#ifndef _XYLON_XSDOM_CONTENTTYPE_H
#include <xylon/xsdom/ContentType.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class SimpleContent : public ContentType {
      
      /** The default constructor */
    public:
      SimpleContent() throws();
      
      /** Destructor */
    public:
      ~SimpleContent() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
