#include <xylon/xsdom/Unique.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Unique::Unique() throws()
    {}
		      	
    Unique::~Unique() throws()
    {}

    void Unique::accept(Visitor& visitor)
    {
      visitor.visitUnique(toRef<Unique>());
    }

  }
}
