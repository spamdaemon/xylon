#include <xylon/xsdom/UrType.h>
#include <xylon/xsdom/Visitor.h>

namespace xylon {
   namespace xsdom {

      UrType::UrType() throws()
      {
      }

      UrType::~UrType() throws()
      {
      }

      void UrType::accept(Visitor& visitor)
      {
         visitor.visitUrType(toRef< UrType> ());
      }

      bool UrType::isAnyType() const throws()
      {
         return qname() == QName::xsQName("anyType");
      }

      bool UrType::isAnySimpleType() const throws()
      {
         return qname() == QName::xsQName("anySimpleType");
      }
      bool UrType::isSubtypeOf(const QName& baseType) const throws()
      {
         if (baseType == qname()) {
            return false;
         }

         if (isAnyType()) {
            return false;
         }

         if (isAnySimpleType() && baseType == QName::xsQName("anyType")) {
            return true;
         }
         return false;
      }

   }
}
