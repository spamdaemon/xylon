#ifndef _XYLON_XSDOM_H
#define _XYLON_XSDOM_H

#ifndef _XYLON_H
#include <xylon/xylon.h>
#endif

#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#ifndef _XYLON_XREF_H
#include <xylon/XRef.h>
#endif

#include <vector>

namespace xylon {
   class XsdSchema;

   /**
    * The xsdom namespace implements an XML Schema Object Model (xsdom)
    * which is used as the basic for binding generation and schema validation.
    * The xsdom is similar to the xml DOM, but the nodes have been processed
    * for their information set.
    */
   namespace xsdom {
      class Node;
      class Element;
      class Group;
      class Attribute;
      class AttributeGroup;
      class Type;
      class Key;
      class Any;
      class Occurrence;

      /** The XSD namespace */
      extern const char* XSD_NAMESPACE_URI;

      /**
       * A type reference
       */
      typedef XRef< Type, QName> TypeXRef;

      /**
       * An reference to an element
       */
      typedef XRef< Element, QName> ElementXRef;

      /**
       * A group reference
       */
      typedef XRef< Group, QName> GroupXRef;

      /**
       * An attribute reference
       */
      typedef XRef< Attribute, QName> AttributeXRef;

      /**
       * An attribute group reference
       */
      typedef XRef< AttributeGroup, QName> AttributeGroupXRef;

      /**
       * An attribute group reference
       */
      typedef XRef< Key, QName> KeyXRef;

      /**
       * Get a list of XML schema nodes corresponding to the top-level types in a xylon schema.
       * @param schema a xylon schema
       * @return a list of top-level nodes in the schema
       */
      ::std::vector< ::timber::Reference< Node> >
      getSchemaNodes(const ::timber::Reference< ::xylon::XsdSchema>& schema) throws();

      /**
       * Create an instance of Any, defined in the xml schema namespace. Processing is lax.<br>
       * The primary user of this function is the FirstSets.
       *
       * @param occurrence the occurrence value for the any
       * @return an <tt>xs:any</tt>
       */
      ::timber::Reference< Any> createAny() throws();

      /**
       * Create an instance of Any, defined in the xml schema namespace.
       * The primary user of this function is the FirstSets.
       *
       * @param occurrence the occurrence value for the any
       * @return an <tt>xs:any</tt>
       */
      ::timber::Reference< Any> createAny(const Occurrence& occurs) throws();

   }
}

#endif
