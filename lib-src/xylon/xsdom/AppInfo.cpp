#include <xylon/xsdom/AppInfo.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    AppInfo::AppInfo() throws()
    {}
		      	
    AppInfo::~AppInfo() throws()
    {}

    void AppInfo::accept(Visitor& visitor)
    {
      visitor.visitAppInfo(toRef<AppInfo>());
    }

  }
}
