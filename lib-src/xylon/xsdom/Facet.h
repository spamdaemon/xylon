#ifndef _XYLON_XSDOM_FACET_H
#define _XYLON_XSDOM_FACET_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Facet : public Node {
      
      /** The default constructor */
    public:
      Facet() throws();
      
      /** Destructor */
    public:
      ~Facet() throws();
    };
  }
}
#endif
