#ifndef _XYLON_XSDOM_XMLSCHEMA_H
#define _XYLON_XSDOM_XMLSCHEMA_H

#ifndef _XYLON_XSDOM_SCHEMA_H
#include <xylon/xsdom/Schema.h>
#endif

#include <map>

namespace xylon {
  namespace xsdom {
    
    /**
     * XmlSchema provides a subset of the types defined in the actual XMLSchema document. The
     * supported subset only includes the official simple types.
     * This schema cannot be used for validation purposes!
     */
    class XmlSchema : public Schema { 
      
      /**
       * Create a new instance of the XML schema.
       */
    private:
      XmlSchema() throws();

      /**
       * Destructor
       */
    public:
      ~XmlSchema() throws();


      /**
       * Get an instance of this schema.
       * @return an instance of this schema
       */
    public:
      static ::timber::Reference<Schema> instance() throws();

      int blockDefaults() const throws();
      int finalDefaults() const throws();
      String targetNamespace() const throws();
      String version() const throws();
      String language() const throws();
      Form attributeFormDefault() const throws();
      Form elementFormDefault() const throws();

      ::std::vector<AttributeXRef > attributes() const throws();
      ::std::vector<AttributeGroupXRef > attributeGroups() const throws();
      ::std::vector<ElementXRef > elements() const throws();
      ::std::vector<GroupXRef > groups() const throws();
      ::std::vector<TypeXRef > types() const throws();
      
      ::std::vector< ::timber::Reference<Import> > imports() const  throws();
      ::std::vector< ::timber::Reference<Node> > nodes () const throws();
      ::timber::Pointer<Node> parentOf (const ::timber::Reference<Node>& node) const throws();
      bool isTopLevelNode(const ::timber::Reference<Node>& node) const throws();
      ::timber::Pointer<Node> findNode (const NodeRef& name) const throws();
      ::timber::Pointer<Type> findType (const QName& qname) const throws();
      ::timber::Pointer<Group> findGroup (const QName& qname) const throws();
      ::timber::Pointer<AttributeGroup> findAttributeGroup (const QName& qname) const throws();
      ::timber::Pointer<Attribute> findAttribute (const QName& qname) const throws();
      ::timber::Pointer<Element> findElement (const QName& qname) const throws();
      ::timber::Pointer<Key> findKey (const QName& qname) const throws();
      
      /** The types */
    private:
      ::std::map<QName,XRef<Type> > _types;
    };    
  }
}
#endif
