#include <xylon/xsdom/XmlSchema.h>
#include <xylon/xsdom/nodes.h>

using namespace ::std;
using namespace ::timber;

namespace xylon {
   namespace xsdom {
      namespace {
         static const char* btypes[] = {
               "anySimpleType", "anyURI", "base64Binary", "boolean", "byte", "date", "dateTime", "decimal", "double",
               "duration", "ENTITIES", "ENTITY", "float", "gDay", "gMonth", "gMonthDay", "gYear", "gYearMonth",
               "hexBinary", "ID", "IDREF", "IDREFS", "int", "integer", "language", "long", "Name", "NCName",
               "negativeInteger", "NMTOKEN", "NMTOKENS", "nonNegativeInteger", "nonPositiveInteger",
               "normalizedString", "NOTATION", "positiveInteger", "QName", "short", "string", "time", "token",
               "unsignedByte", "unsignedInt", "unsignedLong", "unsignedShort", 0 };

         /** The any type */
         struct AnyType : public UrType
         {
               AnyType() :
                  _name(QName::xsQName("anyType"))
               {
               }

               ~AnyType() throws()
               {
               }

               bool isAnyType() const throws()
               {
                  return true;
               }

               QName qname() const throws()
               {
                  return _name;
               }

            private:
               const QName _name;
         };

         struct SimpleBuiltInType : public SimpleType
         {
               SimpleBuiltInType(const String& n) :
                  _name(QName::xsQName(n))
               {
               }

               ~SimpleBuiltInType() throws()
               {
               }
               QName qname() const throws()
               {
                  return _name;
               }

               ::timber::Pointer< Restriction> restriction() const throws()
               {
                  return Pointer< Restriction> ();
               }

               ::timber::Pointer< List> list() const throws()
               {
                  return Pointer< List> ();
               }

               ::timber::Pointer< Union> getUnion() const throws()
               {
                  return Pointer< Union> ();
               }

               int final() const throws()
               {
                  return FINAL_NONE;
               }

            private:
               const QName _name;
         };
      }

      XmlSchema::XmlSchema() throws()
      {
         for (size_t i = 0; btypes[i] != 0; ++i) {
            String name = btypes[i];
            Reference< Type> t(new SimpleBuiltInType(name));
            XRef< Type> ref(t->qname(), true);
            ref.bind(t);
            _types.insert(map< QName, XRef< Type> >::value_type(ref.name(), XRef< Type> (ref, false)));
         }
         {
            Reference< Type> t(new AnyType());
            XRef< Type> ref(t->qname(), true);
            ref.bind(t);
            _types.insert(map< QName, XRef< Type> >::value_type(ref.name(), XRef< Type> (ref, false)));
         }
      }

      XmlSchema::~XmlSchema() throws()
      {
      }

      Reference< Schema> XmlSchema::instance() throws()
      {
         return new XmlSchema();
      }

      int XmlSchema::blockDefaults() const throws()
      {
         return BLOCK_ALL;
      }
      int XmlSchema::finalDefaults() const throws()
      {
         return FINAL_NONE;
      }

      String XmlSchema::targetNamespace() const throws()
      {
         return String(XSD_NAMESPACE_URI);
      }

      String XmlSchema::version() const throws()
      {
         return String("1.0");
      }
      String XmlSchema::language() const throws()
      {
         return String("EN");
      }

      Schema::Form XmlSchema::attributeFormDefault() const throws()
      {
         return FORM_UNQUALIFIED;
      }
      Schema::Form XmlSchema::elementFormDefault() const throws()
      {
         return FORM_QUALIFIED;
      }

      vector< AttributeXRef> XmlSchema::attributes() const throws()
      {
         return vector< AttributeXRef> ();
      }

      vector< AttributeGroupXRef> XmlSchema::attributeGroups() const throws()
      {
         return vector< AttributeGroupXRef> ();
      }

      vector< ElementXRef> XmlSchema::elements() const throws()
      {
         return vector< ElementXRef> ();
      }

      vector< GroupXRef> XmlSchema::groups() const throws()
      {
         return vector< GroupXRef> ();
      }

      vector< TypeXRef> XmlSchema::types() const throws()
      {
         vector< TypeXRef> res;
         for (map< QName, XRef< Type> >::const_iterator i = _types.begin(); _types.end() != i; ++i) {
            res.push_back(i->second);
         }
         return res;
      }

      vector< Reference< Import> > XmlSchema::imports() const throws()
      {
         return vector< Reference< Import> > ();
      }

      vector< Reference< Node> > XmlSchema::nodes() const throws()
      {
         vector< Reference< Node> > res;
         for (map< QName, XRef< Type> >::const_iterator i = _types.begin(); _types.end() != i; ++i) {
            res.push_back(i->second.target());
         }
         return res;
      }

      Pointer< Node> XmlSchema::parentOf(const Reference< Node>& node) const throws()
      {
         return Pointer< Node> ();
      }

      bool XmlSchema::isTopLevelNode(const Reference< Node>& node) const throws()
      {
         return true;
      }

      Pointer< Node> XmlSchema::findNode(const NodeRef& name) const throws()
      {
         if (name.type() != NodeRef::TYPE) {
            return Pointer< Node> ();
         }
         return findType(name.name());
      }

      Pointer< Type> XmlSchema::findType(const QName& qname) const throws()
      {
         map< QName, XRef< Type> >::const_iterator i = _types.find(qname);
         if (i == _types.end()) {
            return Pointer< Type> ();
         }
         return i->second.target();
      }

      Pointer< Group> XmlSchema::findGroup(const QName& qname) const throws()
      {
         return Pointer< Group> ();
      }
      Pointer< AttributeGroup> XmlSchema::findAttributeGroup(const QName& qname) const throws()
      {
         return Pointer< AttributeGroup> ();
      }
      Pointer< Attribute> XmlSchema::findAttribute(const QName& qname) const throws()
      {
         return Pointer< Attribute> ();
      }
      Pointer< Element> XmlSchema::findElement(const QName& qname) const throws()
      {
         return Pointer< Element> ();
      }
      Pointer< Key> XmlSchema::findKey(const QName& qname) const throws()
      {
         return Pointer< Key> ();
      }

   }
}
