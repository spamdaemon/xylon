#ifndef _XYLON_XSDOM_NOTATION_H
#define _XYLON_XSDOM_NOTATION_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Notation : public Node {
      
      /** The default constructor */
    public:
      Notation() throws();
      
      /** Destructor */
    public:
      ~Notation() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
