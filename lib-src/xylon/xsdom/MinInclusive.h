#ifndef _XYLON_XSDOM_MININCLUSIVE_H
#define _XYLON_XSDOM_MININCLUSIVE_H

#ifndef _XYLON_XSDOM_FACET_H
#include <xylon/xsdom/Facet.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class MinInclusive : public Facet {
      
      /** The default constructor */
    public:
      MinInclusive() throws();
      
      /** Destructor */
    public:
      ~MinInclusive() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
