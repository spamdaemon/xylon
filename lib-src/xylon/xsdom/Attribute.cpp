#include <xylon/xsdom/Attribute.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Attribute::Attribute() throws()
    {}
    
    Attribute::~Attribute() throws()
    {}

    void Attribute::accept(Visitor& visitor)
    {
      visitor.visitAttribute(toRef<Attribute>());
    }


    QName Attribute::qname(bool applyForm) const throws()
    {
      QName qn=qname();
      if (applyForm && form() != FORM_QUALIFIED) {
	qn = QName(String(),qn.name());
      }
      return qn;
    }
  }
}
