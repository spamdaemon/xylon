#ifndef _XYLON_XSDOM_LIST_H
#define _XYLON_XSDOM_LIST_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class List : public Node {
      
      /** The default constructor */
    public:
      List() throws();
      
      /** Destructor */
    public:
      ~List() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Get the item type
       * @return the reference to a global type
       */
    public:
      virtual TypeXRef itemType() const throws() = 0;

      /**
       * Get the locally defined type.
       * @return the locally defined type.
       */
    public:
      virtual ::timber::Pointer<SimpleType> type() const throws() = 0;
    };
  }
}
#endif
