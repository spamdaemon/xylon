#ifndef _XYLON_XSDOM_UNION_H
#define _XYLON_XSDOM_UNION_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Union : public Node {
      
      /** The default constructor */
    public:
      Union() throws();
      
      /** Destructor */
    public:
      ~Union() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Get the simple types that are part of this union
       * @return the types of this union
       */
    public:
      virtual const ::std::vector< TypeXRef >& memberTypes() const throws() = 0;
      
      /**
       * Get the simple types that are part of this union
       * @return the types of this union
       */
    public:
      virtual const ::std::vector< ::timber::Reference<SimpleType> > & types() const throws() = 0;

    };
  }
}
#endif
