#include <xylon/xsdom/AllowedAttributes.h>
#include <xylon/xsdom/nodes.h>
#include <xylon/XsdSchema.h>
#include <xylon/QName.h>

#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
   namespace xsdom {

      namespace {

         static Log logger()
         {
            return Log("xylon.xsdom.AllowedAttributes");
         }

         static void insertAttribute(const Reference< Attribute>& attr, AllowedAttributes::AttributeNames& names)
         {
            if (attr->ref()) {
               insertAttribute(attr->ref().target(), names);
               return;
            }

            switch (attr->use()) {
               case Attribute::USE_OPTIONAL:
                  names.addOptionalAttribute(attr->qname(true));
                  break;
               case Attribute::USE_PROHIBITED:
                  names.addProhibitedAttribute(attr->qname(true));
                  break;
               case Attribute::USE_REQUIRED:
                  names.addRequiredAttribute(attr->qname(true));
                  break;
            }
         }

         static void insertAnyAttribute(const Pointer< AnyAttribute>& attr, AllowedAttributes::AttributeNames& names)
         {
            if (!attr) {
               return;
            }
            set< String> namespaces = attr->namespaces();
            if (namespaces.count("##any") != 0) {
               names.setAnyAttributeAllowed(true);
            }
            else if (namespaces.count("##other") != 0) {
               names.addProhibitedNamespace(String());
               names.addProhibitedNamespace(attr->targetNamespace());

               // any attribute will be allowed, unless it is explicitly disallowed
               names.setAnyAttributeAllowed(true);
            }
            else {
               for (set< String>::const_iterator i = namespaces.begin(); i != namespaces.end(); ++i) {
                  names.addAllowedNamespace(*i);
               }
            }
         }

         static void findAttributes(const AttributeGroup& g, AllowedAttributes::AttributeNames& names);

         static void findAttributes(const Reference< ComplexType>& ct, AllowedAttributes::AttributeNames& names);

         template<class T>
         static void processAttributes(const T& obj, AllowedAttributes::AttributeNames& names)
         {
            vector < Reference < Attribute >> attrs = obj->attributes();
            for (size_t i = 0; i < attrs.size(); ++i) {
               insertAttribute(attrs[i], names);
            }
            insertAnyAttribute(obj->anyAttribute(), names);
            vector< Reference< AttributeGroup> > grp = obj->attributeGroups();
            for (size_t i = 0; i < grp.size(); ++i) {
               findAttributes(*grp[i], names);
            }
         }

         static void findAttributes(const Derivation& ct, AllowedAttributes::AttributeNames& names)
         {
            TypeXRef base = ct.base();
            if (base) {
               Pointer< ComplexType> baseT = base.target().tryDynamicCast< ComplexType> ();
               if (baseT) {
                  findAttributes(baseT, names);
               }
            }

            const Extension* ext = dynamic_cast< const Extension*> (&ct);
            if (ext) {
               processAttributes(ext, names);
            }

            const Restriction* res = dynamic_cast< const Restriction*> (&ct);
            if (res) {
               processAttributes(res, names);
            }
         }

         static void findAttributes(const AttributeGroup& g, AllowedAttributes::AttributeNames& names)
         {
            if (g.ref()) {
               findAttributes(*g.ref().target(), names);
            }
            else {
               processAttributes(&g, names);
            }
         }

         static void findAttributes(const Reference< ComplexType>& ct, AllowedAttributes::AttributeNames& names)
         {
            if (ct->contentType()) {
               findAttributes(*ct->contentType()->derivation(), names);
            }
            else {
               processAttributes(ct, names);
            }
         }
      }

      AllowedAttributes::AttributeNames::AttributeNames() throws() :
         _anyAttributeAllowed(false)
      {
      }

      AllowedAttributes::AttributeNames::~AttributeNames() throws()
      {
      }

      void AllowedAttributes::AttributeNames::addRequiredAttribute(const QName& name)
      {
         LogEntry(logger()).debugging() << "required attribute " << name << doLog;
         _required.insert(name);
      }

      void AllowedAttributes::AttributeNames::addProhibitedAttribute(const QName& name)
      {
         LogEntry(logger()).debugging() << "prohibited attribute " << name << doLog;
         _prohibited.insert(name);
         _optional.erase(name);
      }

      void AllowedAttributes::AttributeNames::addOptionalAttribute(const QName& name)
      {
         if (_prohibited.count(name) == 0) {
            LogEntry(logger()).debugging() << "optional attribute " << name << doLog;
            _optional.insert(name);
         }
      }

      void AllowedAttributes::AttributeNames::addAllowedNamespace(const String& name)
      {
         if (_disallowedNS.count(name) == 0) {
            _allowedNS.insert(name);
         }
      }

      void AllowedAttributes::AttributeNames::addProhibitedNamespace(const String& name)
      {
         _disallowedNS.insert(name);
         _allowedNS.erase(name);
      }
      bool AllowedAttributes::AttributeNames::isAnyAttributeAllowed() const throws()
      {
         return _anyAttributeAllowed;
      }

      void AllowedAttributes::AttributeNames::setAnyAttributeAllowed(bool allowed) throws()
      {
         _anyAttributeAllowed = allowed;
      }

      ::std::vector< QName> AllowedAttributes::AttributeNames::requiredAttributes() const throws()
      {
         return ::std::vector< QName>(_required.begin(), _required.end());
      }

      ::std::vector< QName> AllowedAttributes::AttributeNames::optionalAttributes() const throws()
      {
         return ::std::vector< QName>(_optional.begin(), _optional.end());
      }

      ::std::vector< QName> AllowedAttributes::AttributeNames::prohibitedAttributes() const throws()
      {
         return ::std::vector< QName>(_prohibited.begin(), _prohibited.end());
      }

      ::std::vector< String> AllowedAttributes::AttributeNames::allowedNamespaces() const throws()
      {
         return ::std::vector< String>(_allowedNS.begin(), _allowedNS.end());
      }

      ::std::vector< String> AllowedAttributes::AttributeNames::prohibitedNamespaces() const throws()
      {
         return ::std::vector< String>(_disallowedNS.begin(), _disallowedNS.end());
      }

      AllowedAttributes::AttributeNames::AttributeNames(const Reference< Type>& type) throws() :
         _anyAttributeAllowed(false)
      {
         Pointer< ComplexType> ct = type.tryDynamicCast< ComplexType> ();
         if (ct) {
            findAttributes(ct, *this);
         }
      }

      AllowedAttributes::AllowedAttributes(const Reference< ::xylon::XsdSchema>& schema) throws()
      {
         vector< XRef< Type> > types = schema->types();
         for (size_t i = 0; i < types.size(); ++i) {
            Pointer< ComplexType> ct = types[i].target().tryDynamicCast< ComplexType> ();
            if (ct) {
               LogEntry(logger()).debugging() << "Find attributes for complexType " << ct->qname() << doLog;
               findAttributes(ct, _typeAttributes[ct->qname()]);
            }
         }

         vector< XRef< Element> > elements = schema->elements();
         for (size_t i = 0; i < elements.size(); ++i) {
            if (elements[i]->elementType()) {
               Pointer< ComplexType> ct = elements[i]->elementType().tryDynamicCast< ComplexType> ();
               if (ct) {
                  LogEntry(logger()).debugging() << "Find attributes for element " << elements[i]->qname() << doLog;
                  findAttributes(ct, _elementAttributes[elements[i]->qname()]);
               }
            }
         }
      }

      AllowedAttributes::~AllowedAttributes() throws()
      {
      }

      const AllowedAttributes::AttributeNames* AllowedAttributes::elementAttributes(const QName& qname) const throws()
      {
         Attributes::const_iterator i = _elementAttributes.find(qname);
         if (i == _elementAttributes.end()) {
            return 0;
         }
         return &i->second;
      }
      const AllowedAttributes::AttributeNames* AllowedAttributes::typeAttributes(const QName& qname) const throws()
      {
         Attributes::const_iterator i = _typeAttributes.find(qname);
         if (i == _typeAttributes.end()) {
            return 0;
         }
         return &i->second;
      }

   }

}
