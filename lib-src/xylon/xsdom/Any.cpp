#include <xylon/xsdom/Any.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
   namespace xsdom {
      namespace {
         static Log logger()
         {
            return Log("xylon.xs.Any");
         }
      }

      Any::Any() throws()
      {
      }

      Any::~Any() throws()
      {
      }

      void Any::accept(Visitor& visitor)
      {
         visitor.visitAny(toRef< Any> ());
      }

      bool Any::acceptsNamespace(const String& ns) const throws()
      {
         const ::std::set< String>& nspaces = namespaces();


         if (nspaces.empty()) {
            // invalid schema!
            return false;
         }

         // match any namespace
         if (nspaces.count("##any") != 0) {
            return true;
         }

         // match any namespace that is not the default one;
         // note, the default namespace can be empty and so
         // need to test for ##local after this test
         if (nspaces.count("##other") != 0) {
            return ns != targetNamespace();
         }

         // if we accept a ##local element, then nspaces will contain a null value for a namespace

         return nspaces.count(ns) != 0;
      }

      bool Any::conflictsWith(const Any& any) const throws()
      {
         const String tns = targetNamespace();
         const String anyTns = any.targetNamespace();
         const ::std::set< String>& nspaces = namespaces();
         const ::std::set< String>& anyNspaces = any.namespaces();

         if (nspaces.empty()) {
            //FIXME: log something here; the schema is invalid
            return true;
         }

         if (nspaces.count("##any") != 0 || anyNspaces.count("##any") != 0) {
            // there's a conflict, for sure
            return true;
         }

         // if this any accept other namespaces, then the only acceptable
         // namespace for any is this target namespace!
         if (nspaces.count("##other") != 0) {
            return anyNspaces.count(tns) != 0;
         }
         if (anyNspaces.count("##other") != 0) {
            return nspaces.count(anyTns) != 0;
         }

         for (::std::set< String>::const_iterator i = anyNspaces.begin(); i != anyNspaces.end(); ++i) {
            if (nspaces.count(*i) != 0) {
               return true;
            }
         }
         for (::std::set< String>::const_iterator j = nspaces.begin(); j != nspaces.end(); ++j) {
            if (anyNspaces.count(*j) != 0) {
               return true;
            }
         }

         return false;
      }

   }
}
