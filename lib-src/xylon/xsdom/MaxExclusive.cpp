#include <xylon/xsdom/MaxExclusive.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    MaxExclusive::MaxExclusive() throws()
    {}
		      	
    MaxExclusive::~MaxExclusive() throws()
    {}

    void MaxExclusive::accept(Visitor& visitor)
    {
      visitor.visitMaxExclusive(toRef<MaxExclusive>());
    }

  }
}
