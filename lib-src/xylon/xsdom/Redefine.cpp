#include <xylon/xsdom/Redefine.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Redefine::Redefine() throws()
    {}
		      	
    Redefine::~Redefine() throws()
    {}

    void Redefine::accept(Visitor& visitor)
    {
      visitor.visitRedefine(toRef<Redefine>());
    }

  }
}
