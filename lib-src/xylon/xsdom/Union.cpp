#include <xylon/xsdom/Union.h>
#include <xylon/xsdom/SimpleType.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Union::Union() throws()
    {}
		      	
    Union::~Union() throws()
    {}

    void Union::accept(Visitor& visitor)
    {
      visitor.visitUnion(toRef<Union>());
    }
  }
}
