#include <xylon/xsdom/Notation.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Notation::Notation() throws()
    {}
		      	
    Notation::~Notation() throws()
    {}

    void Notation::accept(Visitor& visitor)
    {
      visitor.visitNotation(toRef<Notation>());
    }

  }
}
