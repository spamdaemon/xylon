#ifndef _XYLON_XSDOM_ATTRIBUTEGROUP_H
#define _XYLON_XSDOM_ATTRIBUTEGROUP_H

#ifndef _XYLON_XSDOM_NODE_H
#include <xylon/xsdom/Node.h>
#endif

#ifndef _XYLON_XREF_H
#include <xylon/XRef.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class AttributeGroup : public Node {
      
      /** The default constructor */
    public:
      AttributeGroup() throws();
      
      /** Destructor */
    public:
      ~AttributeGroup() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Set the referenced attribute group.
       * @return the referenced attribute group.
       */
    public:
      virtual AttributeGroupXRef ref () const throws() = 0;

      /**
       * Get the any attribute
       * @return the any attribute 
       */
    public:
      virtual ::timber::Pointer<AnyAttribute> anyAttribute() const throws() = 0;    

      /**
       * Set the attributes defined by this group.
       * @param attrs the attributes
       */
    public:
      virtual const ::std::vector< ::timber::Reference<Attribute> >& attributes () const throws() = 0;

      /**
       * Set the attributes or attribute groups. Each node must be an attribute
       * or attribute group.
       * @param grps the contained attribute groups (they will be references)
       */
    public:
      virtual const ::std::vector< ::timber::Reference<AttributeGroup> >& attributeGroups () const throws() = 0;

      /**
       * Get the qualified name for this element
       * @return the qualified name for this element
       */
    public:
      virtual QName qname() const throws() = 0;

    };
  }
}
#endif
