#ifndef _XYLON_XSDOM_GROUP_H
#define _XYLON_XSDOM_GROUP_H

#ifndef _XYLON_XSDOM_ENUMERABLE_H
#include <xylon/xsdom/Enumerable.h>
#endif

#ifndef _XYLON_XREF_H
#include <xylon/XRef.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class Group : public Enumerable {
      
      /** The default constructor */
    public:
      Group() throws();
      
      /** Destructor */
    public:
      ~Group() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);

      /**
       * Set the referenced attribute group.
       * @return the referenced attribute group.
       */
    public:
      virtual GroupXRef ref () const throws() = 0;

      /**
       * Get the enmerable that makes the content of this group. 
       * @return an enumerable
       */
    public:
      virtual ::timber::Pointer<Enumerable> enumerable () const throws() = 0;
      
      /**
       * Get the qualified name for this element
       * @return the qualified name for this element
       */
    public:
      virtual QName qname() const throws() = 0;
    };
  }
}
#endif
