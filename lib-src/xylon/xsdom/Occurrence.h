#ifndef _XYLON_XSDOM_OCCURRENCE_H
#define _XYLON_XSDOM_OCCURRENCE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

namespace xylon {
   namespace xsdom {

      /** The occurrence value associated with attributes and Enumerable objects */
      class Occurrence
      {

            /** A simpler way to specify occurrence */
         public:
            enum Multiplicity
            {
               ZERO, //< Attribute is not allowed
               ZERO_OR_ONE, //< attribute is optional
               ONE, //< attribute is not optional
               ZERO_OR_MORE
            //< attribute is a list of items
            };

            /** The unbounded value */
         public:
            static const size_t UNBOUNDED = ~size_t(0);

            /**
             * Create a multiplicity instance. The true values are determined by
             * taking the maximum and minimum of the values, respectively.
             * @param min the min value
             * @param max the max value
             */
         public:
            Occurrence(size_t min, size_t max) throws();

            /**
             * Default constructor that creates a multiplicity instance of (1,1)
             */
         public:
            Occurrence() throws();

            /**
             * Create an occurrence of zero or one. This is equivalent to Occurrence(0,1).
             * @return an occurrence for an optional element.
             */
         public:
            static Occurrence optional() throws();

            /**
             * Create an occurrence of exactly one. This is equivalent to Occurrence(1,1).
             * @return an occurrence for a required element.
             */
         public:
            static Occurrence required() throws();

            /**
             * Create an occurrence of exactly none. This is equivalent to Occurrence(0,0).
             * @return an occurrence for a prohibited element.
             */
         public:
            static Occurrence prohibited() throws();

            /**
             * Create an occurrence of any number. This is equivalent to Occurrence(0,UNBOUNDED).
             * @return an occurrence for an unbounded number of elements, possibly 0
             */
         public:
            static Occurrence unbounded() throws();

            /**
             * Get the occurrence as a multiplicity value
             * @return the multiplicity corresponding to occurrence
             */
         public:
            Multiplicity multiplicity() const throws();

            /**
             * Get the minimum occurrence value
             * @return the minimum occurrence value
             */
         public:
            inline size_t min() const throws()
            {
               return _min;
            }

            /**
             * Get the maximum occurrence value. If this value is unbounded, then this method
             * returns the <em>largest</em> possible value for the type. Note that the result
             * is different on 32-bit and 64-bit systems.
             * @return the maximum occurrence value
             */
         public:
            inline size_t max() const throws()
            {
               return _max;
            }

            /**
             * Determine if the max occurrence is unbounded.
             * @return true if the max() returns UNBOUNDED.
             */
         public:
            inline bool isUnbounded() const throws()
            {
               return _max == UNBOUNDED;
            }

            /**
             * Multiply the min and max values for this occurs by a multiplier each.
             * @param minMult a multiplier for min
             * @param maxMult a multiplier for max
             * @return a new occurrence value
             */
         public:
            Occurrence multiply(size_t minMult, size_t maxMult) const throws();

            /** The min and max values */
         private:
            size_t _min, _max;
      };
   }
}
#endif

