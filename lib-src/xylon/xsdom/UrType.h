#ifndef _XYLON_XSDOM_URTYPE_H
#define _XYLON_XSDOM_URTYPE_H

#ifndef _XYLON_XSDOM_TYPE_H
#include <xylon/xsdom/Type.h>
#endif

namespace xylon {
   namespace xsdom {
      /**
       * This element is used to represent the two ur-types in XML, namely <tt>xs:anyType</tt>
       * and <tt>xs:anySimpleType</tt>.
       */
      class UrType : public Type
      {

            /** The default constructor */
         public:
            UrType() throws();

            /** Destructor */
         public:
            ~UrType() throws();

            /**
             * Accept a node visitor.
             * @param visitor a visitor
             */
         public:
            void accept(Visitor& visitor);

            /**
             * Determine if this type instance is the anyType.
             * @return true if this type instance is the anyType.
             */
         public:
            virtual bool isAnyType() const throws();

            /**
             * Determine if this type instance is the anySimpleType.
             * @return true if this type instance is the anySimpleType.
             */
         public:
            virtual bool isAnySimpleType() const throws();

            /**
             * Check if this type is a subtype of the <tt>xs:anyType</tt>.
             * @param baseType the name of a base type
             * @return true if this is a subtype of xs:anyType.
             */
            bool isSubtypeOf (const QName& baseType) const throws();
      };
   }
}
#endif
