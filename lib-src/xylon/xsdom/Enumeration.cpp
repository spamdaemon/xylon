#include <xylon/xsdom/Enumeration.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <timber/logging.h>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;

namespace xylon {
  namespace xsdom {

    Enumeration::Enumeration() throws()
    {}
		      	
    Enumeration::~Enumeration() throws()
    {}

    void Enumeration::accept(Visitor& visitor)
    {
      visitor.visitEnumeration(toRef<Enumeration>());
    }

  }
}
