#ifndef _XYLON_XSDOM_MAXEXCLUSIVE_H
#define _XYLON_XSDOM_MAXEXCLUSIVE_H

#ifndef _XYLON_XSDOM_FACET_H
#include <xylon/xsdom/Facet.h>
#endif

namespace xylon {
  namespace xsdom {
    /**
     * A schema node. This is the base class for all 
     * schema elements. 
     */
    class MaxExclusive : public Facet {
      
      /** The default constructor */
    public:
      MaxExclusive() throws();
      
      /** Destructor */
    public:
      ~MaxExclusive() throws();
      
      /**
       * Accept a node visitor
       * @param visitor a visitor
       */
    public:
      void accept(Visitor& visitor);
    };
  }
}
#endif
