#ifndef _XYLON_XSDSCHEMA_H
#define _XYLON_XSDSCHEMA_H

#ifndef _XYLON_SCHEMA_H
#include <xylon/Schema.h>
#endif

#ifndef _XYLON_XSDOM_H
#include <xylon/xsdom/xsdom.h>
#endif

#include <set>

namespace xylon {
   namespace xsdom {
      class Element;
      class Group;
      class Attribute;
      class AttributeGroup;
      class Type;
      class ComplexType;
      class SimpleType;
      class Key;
   }

   /**
    * The SchemaManager is a singleton class which provides access to all schemas
    * known to the application. The SchemaManger's primary task is to create
    * schemas.
    */
   class XsdSchema : public Schema
   {
         /**
          * Elements can have different types of content model, ranging from not content at all
          * to mixed content. Examples of mixed content is typically markup, such as HTML.
          */
      public:
         enum ContentModel
         {
            NO_CONTENT, ///< The element has no content at all
            SIMPLE_TYPE_CONTENT, ///< The element has a simple type has only textual content and no attributes
            SIMPLE_CONTENT, ///< The element has only textual content, but possibly attributes
            COMPLEX_CONTENT, ///< The element has complex content, but is not mixed
            MIXED_CONTENT, ///< The element has mixed content,
            ANY_CONTENT
         ///< Any content is supported
         };

         /** The type of an element. Elements have either no type, a simple type, or a complex type. */
      public:
         enum ElementType
         {
            NO_TYPE, ///< The element has no content at all and thus no type
            SIMPLE_TYPE, ///< The element has only textual content, but no attributes
            COMPLEX_TYPE,
            ///< The element has complex content with attributes and child nodes
            ANY_TYPE /// The element has xs:anyType
         };

         /** Default constructor */
      protected:
         XsdSchema() throws();

         /** Destructor */
      public:
         virtual ~XsdSchema() throws() = 0;

         /**
          * Get the list of top-level attributes
          * @return a list of top-level attributes
          */
      public:
         virtual ::std::vector< ::xylon::xsdom::AttributeXRef> attributes() const throws() = 0;

         /**
          * Get the list of top-level attribute groups
          * @return a list of top-level attribute groups
          */
      public:
         virtual ::std::vector< ::xylon::xsdom::AttributeGroupXRef> attributeGroups() const throws() = 0;

         /**
          * Get the list of top-level elements
          * @return a list of top-level element references
          */
      public:
         virtual ::std::vector< ::xylon::xsdom::ElementXRef> elements() const throws() = 0;

         /**
          * Get the list of top-level groups
          * @return a list of top-level groups references
          */
      public:
         virtual ::std::vector< ::xylon::xsdom::GroupXRef> groups() const throws() = 0;

         /**
          * Get the list of top-level type references
          * @return a list of top-level types references
          */
      public:
         virtual ::std::vector< ::xylon::xsdom::TypeXRef> types() const throws() = 0;

         /**
          * Get the namespaces of the XML schemas that have been loaded. The resulting list
          * contains no duplicates, but it may contain a single null string, representing a no-namespace schema.
          * @return a list of namespaces
          */
      public:
         virtual ::std::vector< String> schemas() const throws() = 0;

         /**
          * @defgroup Find Locate top-level objects in this schema.
          * @{
          */

         /**
          * Find an type
          * @param qname the name of the type to search for
          * @return a pointer to an type or 0 if not found
          */
      public:
         virtual ::timber::Pointer< ::xylon::xsdom::Type> findType(const QName& qname) const throws() = 0;

         /**
          * Find all subtypes of the specified type. A subtype is not only an  directly derived type, but also
          * an indirectly derived type. Only types that that are top-level types are returned.
          * @param name the name of a type
          * @return all subtypes known in the schema.
          */
      public:
         virtual ::std::set< QName> findSubtypesOf(const QName& typeName) const throws();

         /**
          * Find an group.
          * @param qname the name of the group to search for
          * @return a pointer to an group or 0 if not found
          */
      public:
         virtual ::timber::Pointer< ::xylon::xsdom::Group> findGroup(const QName& qname) const throws() = 0;

         /**
          * Find an attribute group.
          * @param qname the name of the attribute group to search for
          * @return a pointer to an attribute group or 0 if not found
          */
      public:
         virtual ::timber::Pointer< ::xylon::xsdom::AttributeGroup>
         findAttributeGroup(const QName& qname) const throws() = 0;

         /**
          * Find an group.
          * @param qname the name of the attribute to search for
          * @return a pointer to an attribute  or 0 if not found
          */
      public:
         virtual ::timber::Pointer< ::xylon::xsdom::Attribute> findAttribute(const QName& qname) const throws() = 0;

         /**
          * Find an element
          * @param qname the name of the element to search for
          * @return a pointer to an element or 0 if not found
          */
      public:
         virtual ::timber::Pointer< ::xylon::xsdom::Element> findElement(const QName& qname) const throws() = 0;

         /**
          * Find an element
          * @param qname the name of the element to search for
          * @return a pointer to an element or 0 if not found
          */
      public:
         virtual ::timber::Pointer< ::xylon::xsdom::Key> findKey(const QName& qname) const throws() = 0;

         /*@}*/

         /**
          * Get the target of the specified reference.
          * @param xref a type reference
          * @return a type or 0 if not bound
          */
      public:
         ::timber::Pointer< ::xylon::xsdom::Type> derefType(const ::xylon::xsdom::TypeXRef& xref) const throws();

         /**
          * Get the target of the specified reference.
          * @param xref a type reference
          * @return a type or 0 if not bound
          */
      public:
         ::timber::Pointer< ::xylon::xsdom::Group> derefGroup(const ::xylon::xsdom::GroupXRef& xref) const throws();

         /**
          * Get the target of the specified reference.
          * @param xref a type reference
          * @return a type or 0 if not bound
          */
      public:
         ::timber::Pointer< ::xylon::xsdom::Attribute>
         derefAttribute(const ::xylon::xsdom::AttributeXRef& xref) const throws();

         /**
          * Get the target of the specified reference.
          * @param xref a type reference
          * @return a type or 0 if not bound
          */
      public:
         ::timber::Pointer< ::xylon::xsdom::AttributeGroup> derefAttributeGroup(
               const ::xylon::xsdom::AttributeGroupXRef& xref) const throws();

         /**
          * Get the target of the specified reference.
          * @param xref a type reference
          * @return a type or 0 if not bound
          */
      public:
         ::timber::Pointer< ::xylon::xsdom::Key> derefKey(const ::xylon::xsdom::KeyXRef& xref) const throws();

         /**
          * Get the target of the specified reference.
          * @param xref a type reference
          * @return a type or 0 if not bound
          */
      public:
         ::timber::Pointer< ::xylon::xsdom::Element>
         derefElement(const ::xylon::xsdom::ElementXRef& xref) const throws();

         /**
          * @defgroup Substitutions Working with substitution groups
          * @{
          */

         /**
          * Get the set elements that can be substituted for the specified element. This is essentially
          * listing the members of the substitution groups.
          * @param qname the name of the element
          * @return a set of elements names that are in the substitution group of qname
          */
      public:
         virtual ::std::set< QName> findSubstitutions(const QName& qname) const throws() = 0;

         /**
          * Determine if the specified qname is in the substitution group headed by root.
          * @note This method <em>does not</em> check if the element named group is actually the
          * head of a substitution group. Therefore, this method returns true if @code group==qname @endcode.
          * @param group the name of a substitution group
          * @param qname a name
          * @return true if qname is in the substitution group defined by group.
          * @throws ::std::exception if group memebership of qname cannot be determined (yet)
          */
      public:
         virtual bool inSubstitutionGroup(const QName& group, const QName& qname) const throws (::std::exception);

         /**
          * Determine the root of the specified substitution group.
          * @param group the name of an element
          * @return the name of the element that is the root or head of the substitution group.
          */
      public:
         virtual QName getSubstitutionGroupRoot(const QName& qname) const throws();

         /*@}*/

         /**
          * @defgroup ContentModel Querying the type and content model of elements
          * @{
          */

         /**
          * Determine if a global element has a simple type or a complex type. If the type is not resolved
          * then an exception is thrown.
          * @param name the qname of an element
          * @return true if the type of e is a simple type, false if it's a complex type.
          * @throws ::std::exception if the type cannot be determined.
          */
      public:
         virtual ElementType getElementType(const QName& name) const throws (::std::exception);

         /**
          * Determine if an element has a simple type or a complex type. If the type is not resolved
          * then an exception is thrown.
          * @param e an element
          * @return true if the type of e is a simple type, false if it's a complex type.
          * @throws ::std::exception if the type cannot be determined.
          */
      public:
         virtual ElementType
         getElementType(const ::timber::Reference< ::xylon::xsdom::Element>& e) const throws (::std::exception);

         /**
          * Determine if the content model of the specified element is a complex or a simple content model.
          * If the element has a simple type (or a subtype such as list), then this method returns true.
          * @param name the qname of an element
          * @return true if the element has a simple content model.
          * @throws ::std::exception if the content model cannot be determined.
          */
      public:
         virtual ContentModel getContentModel(const QName& name) const throws (::std::exception);

         /**
          * Determine if the content model of the specified element is a complex or a simple content model.
          * If the element has a simple type (or a subtype such as list), then this method returns true.
          * @param name the qname of an element
          * @return true if the element has a simple content model.
          * @throws ::std::exception if the content model cannot be determined.
          */
      public:
         virtual ContentModel
         getContentModel(const ::timber::Reference< ::xylon::xsdom::Element>& e) const throws (::std::exception);

         /**
          * Determine if an element is abstract. An element is abstract if either the
          * element has been declared abstract, or its type has been.
          * @param name the qname of an element
          * @return true if the named element is abstract
          * @throws ::std::exception if it cannot be determined if the element is abstract.
          */
      public:
         virtual bool isAbstractElement(const QName& name) const throws (::std::exception);

         /**
          * Determine if an element is abstract. An element is abstract if either the
          * element has been declared abstract, or its type has been.
          * @param e an element
          * @return true if the named element is abstract
          * @throws ::std::exception if it cannot be determined if the element is abstract.
          */
      public:
         virtual bool
         isAbstractElement(const ::timber::Reference< ::xylon::xsdom::Element>& e) const throws (::std::exception);
         /*@}*/

   };
}
#endif
