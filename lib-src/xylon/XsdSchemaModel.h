#ifndef _XYLON_XSDSCHEMAMODEL_H
#define _XYLON_XSDSCHEMAMODEL_H

#ifndef _XYLON_MODEL_MODELVISITOR_H
#include <xylon/model/ModelVisitor.h>
#endif

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

#ifndef _XYLON_XREF_H
#include <xylon/XRef.h>
#endif

namespace xylon {
   class XsdSchema;

   /**
    * The schema binding represents a binding of the nodes in schema instance
    * to models in programming language.
    */
   class XsdSchemaModel : public ::timber::Counted
   {
         XsdSchemaModel(const XsdSchemaModel&);
         XsdSchemaModel&operator=(const XsdSchemaModel&);

         /** An xref for this binding */
      public:
         typedef ::xylon::XRef< XsdSchemaModel, String> XRef;

         /**
          * Default constructor
          */
      protected:
         XsdSchemaModel() throws();

         /** Destructor */
      public:
         ~XsdSchemaModel() throws();

         /**
          * Accept a model visitor. This method simple invokes the visitor on each contained model.
          * @param v a model visitor
          */
      public:
         virtual void accept(::xylon::model::ModelVisitor& v) = 0;

         /**
          * Get the schema that was being bound.
          * @return the bound schema
          */
      public:
         virtual ::timber::Reference< ::xylon::XsdSchema> schema() const throws () = 0;

         /**
          * @defgroup Accessors Access the models for specific types of xs objects.
          * These methods are just short-cuts and all calls are forwarded to the appropriate SchemaModel method.
          * @{
          */
         /**
          * Get the type model for the specified element
          * @param qname the fully qualified name of an element
          * @return a global type model or null if the element was not bound
          */
      public:
         virtual ::xylon::model::ModelRef< ::xylon::model::TypeModel>
         getElementModel(const QName& name) const throws (::std::exception) = 0;

         /**
          * Get the type model for the specified complex type or simple type.
          * @param qname the fully qualified name of a simple or complex type.
          * @return a global type model or null if the type was not bound
          */
      public:
         virtual ::xylon::model::ModelRef< ::xylon::model::TypeModel>
         getTypeModel(const QName& name) const throws (::std::exception) = 0;

         /**
          * Get the type model for the specified group.
          * @param qname the fully qualified name of a group
          * @return a global type model or null if the group was not bound
          */
      public:
         virtual ::xylon::model::ModelRef< ::xylon::model::TypeModel>
         getGroupModel(const QName& name) const throws (::std::exception) = 0;

         /**
          * Get the type model for the specified attribute.
          * @param qname the fully qualified name of an attribute
          * @return a global type model or null if the attribute was not bound
          */
      public:
         virtual ::xylon::model::ModelRef< ::xylon::model::TypeModel>
         getAttributeModel(const QName& name) const throws (::std::exception) = 0;

         /**
          * Get the type model for the specified attribute group.
          * @param qname the fully qualified name of an attribute group
          * @return a global type model or null if the attribute group was not bound
          */
      public:
         virtual ::xylon::model::ModelRef< ::xylon::model::TypeModel>
         getAttributeGroupModel(const QName& name) const throws (::std::exception) = 0;

         /*@}*/
         /**
          * @defgroup Accessors Access all known models for each model type.
          * @{
          */
         /**
          * Get the type binding for the specified element
          * @param qname the fully qualified name of an element
          * @return a global type binding or null if the element was not bound
          */
      public:
         virtual ::std::vector< ::xylon::model::ModelRef< ::xylon::model::TypeModel> >
         elementModels() const throws() = 0;

         /**
          * Get the type model for the specified complex type or simple type.
          * @param qname the fully qualified name of a simple or complex type.
          * @return a global type model or null if the type was not bound
          */
      public:
         virtual ::std::vector< ::xylon::model::ModelRef< ::xylon::model::TypeModel> > typeModels() const throws() = 0;

         /**
          * Get the type model for the specified group.
          * @param qname the fully qualified name of a group
          * @return a global type model or null if the group was not bound
          */
      public:
         virtual ::std::vector< ::xylon::model::ModelRef< ::xylon::model::TypeModel> > groupModels() const throws() = 0;

         /**
          * Get the type model for the specified attribute.
          * @param qname the fully qualified name of an attribute
          * @return a global type model or null if the attribute was not bound
          */
      public:
         virtual ::std::vector< ::xylon::model::ModelRef< ::xylon::model::TypeModel> >
         attributeModels() const throws() = 0;

         /**
          * Get the type model for the specified attribute group.
          * @param qname the fully qualified name of an attribute group
          * @return a global type model or null if the attribute group was not bound
          */
      public:
         virtual ::std::vector< ::xylon::model::ModelRef< ::xylon::model::TypeModel> >
         attributeGroupModels() const throws() = 0;

         /*@}*/
   };
}
#endif
