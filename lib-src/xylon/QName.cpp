#include <xylon/QName.h>

using namespace ::std;
using namespace ::timber;

namespace xylon {

   const char* QName::XSD_NAMESPACE_URI = "http://www.w3.org/2001/XMLSchema";
   /** The XSI namespace */
   const char* QName::XSI_NAMESPACE_URI = "http://www.w3.org/2001/XMLSchema-instance";

   QName::QName(const String& ns, const String& nm) throws() :
      _namespace(ns), _name(nm)
   {
   }

   QName QName::xsQName(const String& n) throws()
   {
      return QName(XSD_NAMESPACE_URI, n);
   }

   bool QName::operator==(const QName& n) const throws()
   {
      return _name == n._name && _namespace == n._namespace;
   }

   bool QName::operator<(const QName& n) const throws()
   {
      int cmp = _name.compare(n._name);
      if (cmp == 0) {
         if (_namespace) {
            if (n._namespace) {
               cmp = _namespace.compare(n._namespace);
            }
            else {
               cmp = 1;
            }
         }
         else if (n._namespace) {
            cmp = -1;
         }
      }
      return cmp < 0;
   }

   String QName::string() const throws()
   {
      if (_namespace) {
         String res = _name;
         res += '{';
         res += _namespace;
         res += '}';
         return res;
      }
      return _name;
   }
}

ostream& operator<<(::std::ostream& out, const ::xylon::QName& qname)
{
   out << qname.name();
   if (qname.namespaceURI()) {
      out << '{' << qname.namespaceURI() << "}";
   }
   return out;
}

