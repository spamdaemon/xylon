#include <xylon/binding/DefaultElementTypeModel.h>
#include <xylon/model/AttributeModel.h>
#include <xylon/model/ScopeModel.h>
#include <xylon/model/instructions.h>

using namespace ::std;
using namespace ::timber;
using namespace ::xylon::model;
using namespace ::xylon::model::instructions;

namespace xylon {
   namespace binding {
      DefaultElementTypeModel::DefaultElementTypeModel(String n, const GlobalName& gn, const Reference< ScopeModel>& sc)
      throws() :
         DefaultTypeModel(n, gn, sc, Pointer< Instruction> ())
      {
      }

      DefaultElementTypeModel::DefaultElementTypeModel(String n, const Reference< ScopeModel>& sc)
      throws() :
         DefaultTypeModel(n, GlobalName(), sc, Pointer< Instruction> ())
      {
      }

      DefaultElementTypeModel::~DefaultElementTypeModel()
      throws()
      {
      }
      void DefaultElementTypeModel::setContentModelType(
            const ::xylon::model::ModelRef< ::xylon::model::TypeModel>& model)
      throws()
      {
         _contentType = model;
      }

      ModelRef< TypeModel> DefaultElementTypeModel::contentModelType() const
      throws()
      {
         return _contentType;
      }

      ::timber::Reference< Instruction> DefaultElementTypeModel::createParseInstruction(const ModelRef< TypeModel>& type) const
      throws ()
      {
         vector< Reference< Instruction> > pi;
         if (type->globalName().type()== GlobalName::TYPE) {
            pi .push_back(OnXsiType::create(type));
         }
         else {
            pi .push_back(CreateAndParse::create(type));
         }
         pi.push_back(BindAttribute::create(attributes()[0]));
         return Sequence::create(pi);
      }

   }
}
