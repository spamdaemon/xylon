#ifndef _XYLON_BINDING_DEFAULTELEMENTTYPEBINDING_H
#define _XYLON_BINDING_DEFAULTELEMENTTYPEBINDING_H

#ifndef _XYLON_MODEL_MODELS_H
#include <xylon/model/models.h>
#endif

#ifndef _XYLON_MODEL_H
#include <xylon/model/model.h>
#endif

#ifndef _XYLON_BINDING_DEFAULTTYPEMODEL_H
#include <xylon/binding/DefaultTypeModel.h>
#endif

#ifndef _XYLON_MODEL_ELEMENTTYPEMODEL_H
#include <xylon/model/ElementTypeModel.h>
#endif

namespace xylon {

   namespace binding {
      /**
       * The TypeBinding maps XML schema artifacts, such as complex types,
       * elements, groups, etc. into types in a target programming language.
       */
      class DefaultElementTypeModel : public virtual DefaultTypeModel, public virtual ::xylon::model::ElementTypeModel
      {
            DefaultElementTypeModel&operator=(const DefaultElementTypeModel&);
            DefaultElementTypeModel(const DefaultElementTypeModel&);

            /**
             * Create a new typebinding for the specified name and scope.
             * @param name the name of the bound within the specified scope
             * @param gn the global name
             * @param scope the scope in which this model is defined
             */
         public:
            DefaultElementTypeModel(String name, const GlobalName& gn, const ::timber::Reference<
                  ::xylon::model::ScopeModel>& scope)throws();

            /**
             * Create a new default type model.
             * @param name the the type name
             * @param scope the enclosing type
             */
         public:
            DefaultElementTypeModel(String name, const ::timber::Reference< ::xylon::model::ScopeModel>& scope)throws();

            /** Destructor */
         public:
            ~DefaultElementTypeModel()throws();

            ::xylon::model::ModelRef< ::xylon::model::TypeModel> contentModelType() const throws();

            void setContentModelType(const ::xylon::model::ModelRef< ::xylon::model::TypeModel>& model)throws();

            ::timber::Reference< ::xylon::model::Instruction>
            createParseInstruction(const ::xylon::model::ModelRef< ::xylon::model::TypeModel>& type) const throws();

         private:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> _contentType;
      };
   }
}
#endif
