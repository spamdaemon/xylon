#ifndef _XYLON_BINDING_ABSTRACTBINDINGPROVIDER_H
#define _XYLON_BINDING_ABSTRACTBINDINGPROVIDER_H

#ifndef _XYLON_BINDINGPROVIDER_H
#include <xylon/BindingProvider.h>
#endif

#include <vector>

namespace xylon {
   namespace xsdom {
      class Schema;
      class Node;
      class Element;
      class ComplexType;
      class SimpleType;
      class Group;
      class Attribute;
      class AttributeGroup;
   }

   namespace model {
      class TypeModel;
   }

   namespace binding {
      class DefaultSchemaModel;
   }

   namespace binding {
      /**
       * The BindingProvider is used to create bindings for individual top-level objects
       * in an xml schema. A binding factory may only accept a subset of all possible target namespaces
       * in which case it will simply fails to returns a valid binding. Other factorys should be tried
       * before giving up on generating schema bindings.
       */
      class AbstractBindingProvider : public BindingProvider
      {
            /** Default constructor */
         protected:
            AbstractBindingProvider() throws();

            /** Destructor */
         public:
            virtual ~AbstractBindingProvider() throws() = 0;

            /**
             * Create a binding for the specified element.
             * @param xref an unbound XRef.
             */
         public:
            ::timber::Pointer< ::xylon::model::TypeModel>
                  bindElement(const ::xylon::XsdSchemaModel& schema,
                        const ::timber::Reference< ::xylon::xsdom::Element>& s) const throws (::std::exception);

            /**
             * Create a binding for the specified complex type.
             * @param xref an unbound XRef.
             */
         public:
            ::timber::Pointer< ::xylon::model::TypeModel>
            bindComplexType(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference<
                  ::xylon::xsdom::ComplexType>& s) const throws (::std::exception);

            /**
             * Create a binding for the specified simple type.
             * @param xref an unbound XRef.
             */
         public:
            ::timber::Pointer< ::xylon::model::TypeModel>
            bindSimpleType(const ::xylon::XsdSchemaModel& schema,
                  const ::timber::Reference< ::xylon::xsdom::SimpleType>& s) const throws (::std::exception);

            /**
             * Create a binding for the specified group.
             * @param xref an unbound XRef.
             */
         public:
            ::timber::Pointer< ::xylon::model::TypeModel>
                  bindGroup(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference< ::xylon::xsdom::Group>& s) const throws (::std::exception);

            /**
             * Create a binding for the specified attribute.
             * @param xref an unbound XRef.
             */
         public:
            ::timber::Pointer< ::xylon::model::TypeModel>
            bindAttribute(const ::xylon::XsdSchemaModel& schema,
                  const ::timber::Reference< ::xylon::xsdom::Attribute>& s) const throws (::std::exception);

            /**
             * Create a binding for the specified attribute group.
             * @param xref an unbound XRef.
             */
         public:
            ::timber::Pointer< ::xylon::model::TypeModel>
            bindAttributeGroup(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference<
                  ::xylon::xsdom::AttributeGroup>& s) const throws (::std::exception);
      };
   }
}
#endif
