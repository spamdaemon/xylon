#ifndef _XYLON_BINDING_DEFAULTBINDINGPROVIDER_H
#define _XYLON_BINDING_DEFAULTBINDINGPROVIDER_H

#ifndef _XYLON_BINDING_ABSTRACTBINDINGPROVIDER_H
#include <xylon/binding/AbstractBindingProvider.h>
#endif

#ifndef _XYLON_MODEL_H
#include <xylon/model/model.h>
#endif

#ifndef _XYLON_MODEL_SCOPEMODEL_H
#include <xylon/model/ScopeModel.h>
#endif

#include <map>

namespace xylon {
   namespace xsdom {
      class FirstSets;
      class AllowedAttributes;
   }
   namespace binding {

      /**
       * This is a default factory for creating a schema binding.
       */
      class DefaultBindingProvider : public AbstractBindingProvider
      {

            /**
             * Create a binding factory that supports only the specified schema namespace
             * @param tns the namespace of the supported schema.
             */
         public:
            DefaultBindingProvider(String tns) throws();

            /** Destructor */
         public:
            ~DefaultBindingProvider() throws();

         public:
            ::timber::Pointer< ::xylon::model::TypeModel>
                  bindElement(const ::xylon::XsdSchemaModel& schema,
                        const ::timber::Reference< ::xylon::xsdom::Element>& s) const throws (::std::exception);

            ::timber::Pointer< ::xylon::model::TypeModel>
            bindComplexType(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference<
                  ::xylon::xsdom::ComplexType>& s) const throws (::std::exception);

            ::timber::Pointer< ::xylon::model::TypeModel>
            bindSimpleType(const ::xylon::XsdSchemaModel& schema,
                  const ::timber::Reference< ::xylon::xsdom::SimpleType>& s) const throws (::std::exception);

            ::timber::Pointer< ::xylon::model::TypeModel>
                  bindGroup(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference< ::xylon::xsdom::Group>& s) const throws (::std::exception);
            ::timber::Pointer< ::xylon::model::TypeModel>
            bindAttribute(const ::xylon::XsdSchemaModel& schema,
                  const ::timber::Reference< ::xylon::xsdom::Attribute>& s) const throws (::std::exception);

            ::timber::Pointer< ::xylon::model::TypeModel>
            bindAttributeGroup(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference<
                  ::xylon::xsdom::AttributeGroup>& s) const throws (::std::exception);

            /** The supported namespace */
         private:
            const String _targetNamespace;

            /** The namespace bindings for each supported namespace */
         private:
            ::std::map< ::xylon::model::ScopeName, ::timber::Pointer< ::xylon::model::ScopeModel> > _namespaces;

            /** The first sets for each schema */
         private:
            mutable ::std::map< String, ::xylon::xsdom::FirstSets> _firsts;

            /** The allowed attributes */
         private:
            mutable ::std::map< String, ::xylon::xsdom::AllowedAttributes> _attributes;

      };

   }
}

#endif
