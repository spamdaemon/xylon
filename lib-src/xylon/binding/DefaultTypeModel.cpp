#include <xylon/binding/DefaultTypeModel.h>
#include <xylon/model/AttributeModel.h>
#include <xylon/model/ScopeModel.h>

using namespace ::timber;
using namespace ::xylon::model;

namespace xylon {
   namespace binding {
      DefaultTypeModel::DefaultTypeModel(String n, const GlobalName& gn, const Reference< ScopeModel>& sc,
            const Pointer< Instruction>& pi) throws() :
         _name(n), _globalName(gn), _scope(sc), _parse(pi), _scopeName(gn), _complex(true), _abstract(false)
      {
      }

      DefaultTypeModel::DefaultTypeModel(String n, const Reference< ScopeModel>& sc, const Pointer< Instruction>& pi) throws() :
         _name(n), _scope(sc), _parse(pi), _scopeName(n, ScopeName::TYPE), _complex(true), _abstract(false)
      {
      }

      DefaultTypeModel::~DefaultTypeModel() throws()
      {
      }

      void DefaultTypeModel::setComplex(bool c) throws()
      {
         _complex = c;
      }

      bool DefaultTypeModel::isComplex() const throws()
      {
         return _complex;
      }

      void DefaultTypeModel::setAbstract(bool c) throws()
      {
         _abstract = c;
      }

      bool DefaultTypeModel::isAbstract() const throws()
      {
         return _abstract;
      }

      void DefaultTypeModel::clear() throws()
      {
         for (; !_attributes.empty(); _attributes.pop_back()) {
            _attributes.back()->clear();
         }
         for (; !_localTypes.empty(); _localTypes.pop_back()) {
            _localTypes.back()->clear();
         }
      }

      ModelRef< TypeModel> DefaultTypeModel::baseType() const throws()
      {
         return _base;
      }

      void DefaultTypeModel::setBaseType(ModelRef< TypeModel> bt) throws()
      {
         _base = bt;
      }

      String DefaultTypeModel::name() const throws()
      {
         return _name;
      }

      ::std::vector< Reference< AttributeModel> > DefaultTypeModel::attributes() const throws()
      {
         return _attributes;
      }

      ::std::vector< Reference< TypeModel> > DefaultTypeModel::localTypes() const throws()
      {
         return _localTypes;
      }

      void DefaultTypeModel::addLocalTypeModel(const Reference< TypeModel>& lc) throws()
      {
         _localTypes.push_back(lc);
      }

      void DefaultTypeModel::addAttributeModel(const Reference< AttributeModel>& attr) throws()
      {
         _attributes.push_back(attr);
      }
      GlobalName DefaultTypeModel::globalName() const throws()
      {
         return _globalName;
      }
      void DefaultTypeModel::setParseInstruction(const Pointer< Instruction>& pi) throws()
      {
         _parse = pi;
      }

      Pointer< Instruction> DefaultTypeModel::parseInstruction() const throws()
      {
         return _parse;
      }

      void DefaultTypeModel::setWriteInstruction(const Pointer< Instruction>& pi) throws()
      {
         _write = pi;
      }

      Pointer< Instruction> DefaultTypeModel::writeInstruction() const throws()
      {
         return _write;
      }

      Reference< ScopeModel> DefaultTypeModel::scope() const throws()
      {
         return _scope;
      }
      ScopeName DefaultTypeModel::scopeName() const throws()
      {
         return _scopeName;
      }
   }
}
