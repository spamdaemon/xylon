#include <xylon/binding/AbstractBindingProvider.h>
#include <xylon/model/TypeModel.h>

namespace xylon {
   namespace binding {

      AbstractBindingProvider::AbstractBindingProvider() throws()
      {
      }

      AbstractBindingProvider::~AbstractBindingProvider() throws()
      {
      }

      ::timber::Pointer< ::xylon::model::TypeModel> AbstractBindingProvider::bindElement(const ::xylon::XsdSchemaModel&,
            const ::timber::Reference< ::xylon::xsdom::Element>&) const throws (::std::exception)
      {
         return ::timber::Pointer< ::xylon::model::TypeModel>();
      }
      ::timber::Pointer< ::xylon::model::TypeModel> AbstractBindingProvider::bindComplexType(
            const ::xylon::XsdSchemaModel&, const ::timber::Reference< ::xylon::xsdom::ComplexType>&) const throws (::std::exception)
      {
         return ::timber::Pointer< ::xylon::model::TypeModel>();
      }
      ::timber::Pointer< ::xylon::model::TypeModel> AbstractBindingProvider::bindSimpleType(
            const ::xylon::XsdSchemaModel&, const ::timber::Reference< ::xylon::xsdom::SimpleType>&) const throws (::std::exception)
      {
         return ::timber::Pointer< ::xylon::model::TypeModel>();
      }
      ::timber::Pointer< ::xylon::model::TypeModel> AbstractBindingProvider::bindGroup(const ::xylon::XsdSchemaModel&,
            const ::timber::Reference< ::xylon::xsdom::Group>&) const throws (::std::exception)
      {
         return ::timber::Pointer< ::xylon::model::TypeModel>();
      }
      ::timber::Pointer< ::xylon::model::TypeModel> AbstractBindingProvider::bindAttribute(
            const ::xylon::XsdSchemaModel&, const ::timber::Reference< ::xylon::xsdom::Attribute>&) const throws (::std::exception)
      {
         return ::timber::Pointer< ::xylon::model::TypeModel>();
      }
      ::timber::Pointer< ::xylon::model::TypeModel> AbstractBindingProvider::bindAttributeGroup(
            const ::xylon::XsdSchemaModel&, const ::timber::Reference< ::xylon::xsdom::AttributeGroup>&) const throws (::std::exception)
      {
         return ::timber::Pointer< ::xylon::model::TypeModel>();
      }

   }
}
