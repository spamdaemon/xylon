#ifndef _XYLON_BINDING_DEFAULTLISTTYPEBINDING_H
#define _XYLON_BINDING_DEFAULTLISTTYPEBINDING_H

#ifndef _XYLON_MODEL_MODELS_H
#include <xylon/model/models.h>
#endif

#ifndef _XYLON_MODEL_H
#include <xylon/model/model.h>
#endif

#ifndef _XYLON_BINDING_DEFAULTTYPEMODEL_H
#include <xylon/binding/DefaultTypeModel.h>
#endif

#ifndef _XYLON_MODEL_LISTTYPEMODEL_H
#include <xylon/model/ListTypeModel.h>
#endif

namespace xylon {

   namespace binding {
      /**
       * The TypeBinding maps XML schema artifacts, such as complex types,
       * elements, groups, etc. into types in a target programming language.
       */
      class DefaultListTypeModel : public virtual DefaultTypeModel, public virtual ::xylon::model::ListTypeModel
      {
            DefaultListTypeModel&operator=(const DefaultListTypeModel&);
            DefaultListTypeModel(const DefaultListTypeModel&) throws();

            /**
             * Create a new typebinding for the specified name and scope.
             * @param name the name of the bound within the specified scope
             * @param gn the global name
             * @param scope the scope in which this model is defined
             * @param pi an optional parse instruction
             */
         public:
            DefaultListTypeModel(String name, const GlobalName& gn, const ::timber::Reference<
                  ::xylon::model::ScopeModel>& scope, const ::timber::Pointer< ::xylon::model::Instruction>& pi =
                  ::timber::Pointer< ::xylon::model::Instruction>()) throws();

            /**
             * Create a new default type model.
             * @param name the the type name
             * @param scope the enclosing type
             * @param pi an optional instruction
             */
         public:
            DefaultListTypeModel(String name, const ::timber::Reference< ::xylon::model::ScopeModel>& scope,
                  const ::timber::Pointer< ::xylon::model::Instruction>& pi = ::timber::Pointer<
                        ::xylon::model::Instruction>()) throws();

            /** Destructor */
         public:
            ~DefaultListTypeModel() throws();

            /** The ListTypeModel is not a complex type.
             * @return false
             */
            bool isComplex() const throws();

            /**
             * Set the type for this list.
             * @param t
             */
         public:
            void setType(const ::xylon::model::ModelRef< ::xylon::model::TypeModel>& t) throws();

            ::xylon::model::ModelRef< ::xylon::model::TypeModel> type() const throws();

            /** The base type (must be a  type) */
         private:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> _type;
      };
   }
}
#endif
