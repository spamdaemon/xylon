#include <xylon/binding/XmlSchemaBindingProvider.h>
#include <xylon/binding/DefaultTypeModel.h>
#include <xylon/binding/DefaultScopeModel.h>
#include <xylon/xsdom/SimpleType.h>

#include <xylon/QName.h>
#include <xylon/XsdSchemaModel.h>

using namespace std;
using namespace timber;
using namespace xylon::model;
using namespace xylon::binding;
using namespace xylon::xsdom;

namespace xylon {
   namespace binding {
      XmlSchemaBindingProvider::XmlSchemaBindingProvider() throws() :
         _scope(new DefaultScopeModel(ScopeName(QName::XSD_NAMESPACE_URI, ScopeName::TYPE)))
      {
      }

      XmlSchemaBindingProvider::~XmlSchemaBindingProvider() throws()
      {
      }

      unique_ptr< BindingProvider> XmlSchemaBindingProvider::create() throws()
      {
         return unique_ptr< BindingProvider> (new XmlSchemaBindingProvider());
      }

      Pointer< TypeModel> XmlSchemaBindingProvider::bindSimpleType(const XsdSchemaModel& schema, const Reference<
            SimpleType>& s) const throws (::std::exception)
      {
         QName qname = s->qname();
         if (qname.namespaceURI() != QName::XSD_NAMESPACE_URI) {
            return ::timber::Pointer< TypeModel>();
         }
         GlobalName gname(qname, GlobalName::TYPE);
         Reference< DefaultTypeModel> model = new DefaultTypeModel(qname.name(), gname, _scope);
         model->setComplex(false);

         if (qname.name() != "anySimpleType") {
            model->setBaseType(schema.getTypeModel(QName::xsQName("anySimpleType")));
         }
         return model;
      }
   }
}
