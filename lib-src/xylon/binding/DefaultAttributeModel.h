#ifndef _XYLON_BINDING_DEFAULTATTRIBUTEMODEL_H
#define _XYLON_BINDING_DEFAULTATTRIBUTEMODEL_H

#ifndef _XYLON_MODEL_MODELS_H
#include <xylon/model/models.h>
#endif

#ifndef _XYLON_XSDOM_OCCURRENCE_H
#include <xylon/xsdom/Occurrence.h>
#endif

namespace xylon {
   namespace binding {
      /**
       * An Attribute is used to represent attributes of a TypeBinding.
       */
      class DefaultAttributeModel : public virtual ::xylon::model::AttributeModel
      {
            DefaultAttributeModel&operator=(const DefaultAttributeModel&);
            DefaultAttributeModel(const DefaultAttributeModel&);

            /**
             * Create a new attribute.
             * @param name the name of the attribute
             * @param scope the enclosing type
             * @param gc the atttribute's type
             * @param occurs the multiplicity for the new attribute
             * @pre occurs.maxOccurs() != 0
             * */
         public:
            DefaultAttributeModel(String name, ::xylon::model::ModelRef< ::xylon::model::TypeModel> scope,
                  ::xylon::model::ModelRef< ::xylon::model::TypeModel> gc, ::xylon::xsdom::Occurrence occurs,
                  Type attrType) throws();

            /** Destructor */
         public:
            ~DefaultAttributeModel() throws();

            void clear() throws();

            Type attributeType() const throws();

            /**
             * Get the name of this attribute.
             * @return the name of this attribute
             */
         public:
            String name() const throws();

            /**
             * Get the TypeBinding in whose scope this attribute is defined.
             * @return a type binding
             */
         public:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> scope() const throws();

            /**
             * Get the type of the attribute when it is defined as a global type.
             * @return the attribute's global type
             * @FIXME change to attributeType()
             */
         public:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> type() const throws();

            /**
             * Get the minimum value for the multiplicity.
             * @return the minimum number of times this attribute must at least occur
             */
         public:
            size_t minOccurs() const throws();

            /**
             * Get the maximum value for the multiplicity. If the number of occurrences
             * is unbounded, then the value UNBOUNDED must be returned.
             * @return the maximum number of times this attribute may occur.
             */
         public:
            size_t maxOccurs() const throws();

            /** The name */
         private:
            const String _name;

            /** The scope of this attribute */
         private:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> _scope;

            /** The global type for the attribute's type */
         private:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> _type;

            /** The min and max multiplicity values */
         private:
            size_t _min, _max;

            /** The attribute type */
         private:
            const Type _attributeType;

      };
   }
}
#endif
