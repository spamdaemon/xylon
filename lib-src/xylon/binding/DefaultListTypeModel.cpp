#include <xylon/binding/DefaultListTypeModel.h>
#include <xylon/model/AttributeModel.h>
#include <xylon/model/ScopeModel.h>

using namespace ::timber;
using namespace ::xylon::model;

namespace xylon {
   namespace binding {
      DefaultListTypeModel::DefaultListTypeModel(String n, const GlobalName& gn, const Reference< ScopeModel>& sc,
            const Pointer< Instruction>& pi) throws() :
         DefaultTypeModel(n, gn, sc, pi)
      {
      }

      DefaultListTypeModel::DefaultListTypeModel(String n, const Reference< ScopeModel>& sc,
            const Pointer< Instruction>& pi) throws() :
         DefaultTypeModel(n, GlobalName(), sc, pi)
      {
      }

      DefaultListTypeModel::~DefaultListTypeModel() throws()
      {
      }

      void DefaultListTypeModel::setType(const ModelRef< TypeModel>& t) throws()
      {
         _type = t;
      }

      ModelRef< TypeModel> DefaultListTypeModel::type() const throws()
      {
         return _type;
      }

      bool DefaultListTypeModel::isComplex() const throws()
      {
         return false;
      }

   }
}
