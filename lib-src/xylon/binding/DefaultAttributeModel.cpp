#include <xylon/binding/DefaultAttributeModel.h>

using namespace ::xylon::model;

namespace xylon {
   namespace binding {
      DefaultAttributeModel::DefaultAttributeModel(String n, ModelRef< ::xylon::model::TypeModel> s, ModelRef<
            TypeModel> gc, ::xylon::xsdom::Occurrence occurs, Type t) throws() :
         _name(n), _scope(s), _type(gc), _min(occurs.min()), _max(
               occurs.max() == ::xylon::xsdom::Occurrence::UNBOUNDED ? UNBOUNDED : occurs.max()),
               _attributeType(t)
      {
         assert(occurs.max() != 0 && "Maximum occurrence must at least be 1");

      }

      DefaultAttributeModel::~DefaultAttributeModel() throws()
      {
      }
      DefaultAttributeModel::Type DefaultAttributeModel::attributeType() const throws()
      {
         return _attributeType;
      }

      void DefaultAttributeModel::clear() throws()
      {
         _scope = ModelRef< TypeModel> ();
      }

      String DefaultAttributeModel::name() const throws()
      {
         return _name;
      }

      ModelRef< TypeModel> DefaultAttributeModel::scope() const throws()
      {
         return _scope;
      }

      ModelRef< TypeModel> DefaultAttributeModel::type() const throws()
      {
         return _type;
      }
      size_t DefaultAttributeModel::minOccurs() const throws()
      {
         return _min;
      }

      size_t DefaultAttributeModel::maxOccurs() const throws()
      {
         return _max;
      }

   }
}
