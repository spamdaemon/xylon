#ifndef _XYLON_BINDING_XMLSCHEMABINDINGPROVIDER_H
#define _XYLON_BINDING_XMLSCHEMABINDINGPROVIDER_H

#ifndef _XYLON_BINDING_ABSTRACTBINDINGPROVIDER_H
#include <xylon/binding/AbstractBindingProvider.h>
#endif

namespace xylon {
   namespace model {
      class ScopeModel;
   }
   namespace binding {

      /**
       * This is a xmlschema factory for creating a schema binding.
       */
      class XmlSchemaBindingProvider : public AbstractBindingProvider
      {

            /**
             * Create a binding factory that supports only the specified schema namespace
             */
         private:
            XmlSchemaBindingProvider() throws();

            /** Destructor */
         public:
            ~XmlSchemaBindingProvider() throws();

         public:
            ::timber::Pointer< ::xylon::model::TypeModel>
            bindSimpleType(const ::xylon::XsdSchemaModel& schema,
                  const ::timber::Reference< ::xylon::xsdom::SimpleType>& s) const throws (::std::exception);

            /**
             * Get an instance of this provider
             * @return a binding provider for the XML schema and any included or imported schema
             */
         public:
            static ::std::unique_ptr< BindingProvider> create() throws();

            /**
             * A scope model.
             */
         private:
            ::timber::Reference< ::xylon::model::ScopeModel> _scope;
      };

   }
}

#endif
