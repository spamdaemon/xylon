#include <xylon/binding/DefaultScopeModel.h>

using namespace ::xylon::model;

namespace xylon {
  namespace binding {
    DefaultScopeModel :: DefaultScopeModel(const ::xylon::model::ScopeName& n) throws()
    : _name(n)
    {}
    
    DefaultScopeModel :: ~DefaultScopeModel() throws() 
    {}

    void DefaultScopeModel ::clear() throws()
    {
    }

    ::xylon::model::ScopeName DefaultScopeModel::scopeName() const throws()
    {return _name; }

  }
}
