#ifndef _XYLON_BINDING_DEFAULTSCOPEMODEL_H
#define _XYLON_BINDING_DEFAULTSCOPEMODEL_H

#ifndef _XYLON_MODEL_NAMESPACEMODEL_H
#include <xylon/model/NamespaceModel.h>
#endif


namespace xylon {
  namespace binding {
    /**
     * An Namespace is used to represent namespaces of a TypeBinding.
     */
    class DefaultScopeModel : public  ::xylon::model::NamespaceModel {
      DefaultScopeModel&operator=(const DefaultScopeModel&);
      DefaultScopeModel(const DefaultScopeModel&) throws();

      /** 
       * Create a new scope model.
       * @param name the scope name
       **/
    public:
      DefaultScopeModel(const ::xylon::model::ScopeName& name) throws();

      /** Destructor */
    public:
      ~DefaultScopeModel() throws();

    public:
      void clear() throws();

      /**
       * Get the scope name.
       * @return the scope name
       */
    public:
      ::xylon::model::ScopeName scopeName() const throws();

      /** The scoped name */
    private:
      const ::xylon::model::ScopeName _name;
    };
  }
}
#endif
