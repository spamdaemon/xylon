#include <xylon/binding/DefaultBindingProvider.h>
#include <xylon/binding/DefaultTypeModel.h>
#include <xylon/binding/DefaultUnionTypeModel.h>
#include <xylon/binding/DefaultListTypeModel.h>
#include <xylon/binding/DefaultScopeModel.h>
#include <xylon/binding/DefaultAttributeModel.h>
#include <xylon/binding/DefaultElementTypeModel.h>

#include <xylon/xsdom/FirstSets.h>
#include <xylon/xsdom/AllowedAttributes.h>
#include <xylon/model/instructions.h>

#include <xylon/xsdom/nodes.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/QName.h>
#include <xylon/XsdSchemaModel.h>
#include <xylon/XsdSchema.h>

#include <timber/logging.h>

#include <stack>
#include <map>
#include <set>
#include <iostream>

//FIXME: determine if we should keep using ListModelType or just create a struct with an iterate list bound to it
#define USE_LIST_TYPE_MODEL 0
#define USE_UNION_TYPE_MODEL 0

using namespace std;
using namespace timber;
using namespace timber::logging;
using namespace xylon::model;
using namespace xylon::model::instructions;
using namespace xylon::binding;

namespace xylon {
   namespace binding {
      namespace {
         static Log logger()
         {
            return Log("xylon.binding.DefaultBindingProvider");
         }

         static bool isComplexAnyDerivation(const Reference< ::xylon::xsdom::Type>& node)
         {
            Pointer< ::xylon::xsdom::ComplexType> ct = node.tryDynamicCast< ::xylon::xsdom::ComplexType> ();
            if (!ct) {
               return false;
            }
            if (!ct->contentType()) {
               return true;
            }

            Pointer< ::xylon::xsdom::ComplexContent> content = ct->contentType().tryDynamicCast<
                  ::xylon::xsdom::ComplexContent> ();
            if (!content || !content->derivation()) {
               return false;
            }

            if (content->derivation()->base().name() == QName::xsQName("anyType")) {
               return true;
            }
            return false;
         }

         struct TypeModelInfo
         {
            public:
               TypeModelInfo()throws()
               {
               }

               /**
                * Get the default type model.
                * @return a default type model
                */
               inline DefaultTypeModel* operator->() const throws()
               {
                  return &*target();
               }

               /**
                * Get the default type model.
                * @return a default type model
                */
               inline Pointer< DefaultTypeModel> target() const throws()
               {
                  assert(_model);
                  return _model;
               }

               /**
                * Create a child of this model.
                * @param m the child of the this model
                * @return
                */
               TypeModelInfo newChild()
               {
                  TypeModelInfo p;
                  p._scope = _model;

                  return p;
               }

               /**
                * Get the global name.
                * @return the global name
                */
               const GlobalName& globalName() const throws()
               {
                  return _globalName;
               }

               GlobalName _globalName;
               Pointer< TypeModel> _model;
               String _name;
               Pointer< ScopeModel> _scope;
         };

         typedef multimap< Reference< ::xylon::xsdom::Node> , Reference< AttributeModel> > NodeBindings;

         typedef map< Reference< ::xylon::xsdom::Node> , Reference< Instruction> > BindInstructions;

         static ::xylon::xsdom::FirstSets& createFirstSets(String tns,
               ::std::map< String, ::xylon::xsdom::FirstSets>& fs, const Reference< ::xylon::XsdSchema>& schema)
         {
            ::std::map< String, ::xylon::xsdom::FirstSets>::iterator i = fs.find(tns);
            if (i == fs.end()) {
               i = fs.insert(::std::map< String, ::xylon::xsdom::FirstSets>::value_type(tns, ::xylon::xsdom::FirstSets(
                     schema))).first;
            }
            return i->second;
         }

         static ::xylon::xsdom::AllowedAttributes& createAttributeSet(String tns, ::std::map< String,
               ::xylon::xsdom::AllowedAttributes>& fs, const Reference< ::xylon::XsdSchema>& schema)
         {
            ::std::map< String, ::xylon::xsdom::AllowedAttributes>::iterator i = fs.find(tns);
            if (i == fs.end()) {
               i = fs.insert(::std::map< String, ::xylon::xsdom::AllowedAttributes>::value_type(tns,
                     ::xylon::xsdom::AllowedAttributes(schema))).first;
            }
            return i->second;
         }

         struct Binder : public ::xylon::xsdom::Visitor
         {
               //FIXME: first sets should be computed here
               Binder(const ::xylon::XsdSchemaModel& schema, const ::xylon::xsdom::AllowedAttributes& attrs,
                     const ::xylon::xsdom::FirstSets& fs, const Reference< ::xylon::xsdom::Node>& n,
                     TypeModelInfo global) :
                  _anyElement(TypeModel::createAnyElementType()), _anySimpleType(schema.getTypeModel(QName::xsQName(
                        "anySimpleType"))), _attributes(attrs), _firstSets(fs), _schema(schema)
               {
                  // create the initial type binding and push it onto the stack
                  _typeStack.push(global);
               }

               ~Binder()throws()
               {
               }

               /**
                * Make the first set of an item.
                * @param item
                * @return the set of elements that can appear in the first set of the enumerable
                */
               set< QName> findFirstSet(const Reference< ::xylon::xsdom::Enumerable>& e) const throws()
               {
                  return _firstSets(e);
               }

               /**
                * Resolve an attribute to global type reference.
                * @param xref a reference to a globally defined attribute
                * @return a xref to a global type
                */
               ModelRef< TypeModel> resolveAttributeRef(const QName& xref)
               {
                  LogEntry(logger()).debugging() << __FUNCTION__ << " : " << xref << doLog;
                  ModelRef< TypeModel> ref = _schema.getAttributeModel(xref);
                  if (!ref) {
                     LogEntry(logger()).severe() << "Attribute not defined " << xref << doLog;
                  }
                  return ref;
               }

               /**
                * Resolve an attribute to global type reference.
                * @param xref a reference to a globally defined attribute group
                * @return a xref to a global type
                */
               ModelRef< TypeModel> resolveAttributeGroupRef(const QName& xref)
               {
                  LogEntry(logger()).debugging() << __FUNCTION__ << " : " << xref << doLog;
                  ModelRef< TypeModel> ref = _schema.getAttributeGroupModel(xref);
                  if (!ref) {
                     LogEntry(logger()).severe() << "AttributeGroup not defined " << xref << doLog;
                  }
                  return ref;
               }

               /**
                * Resolve an attribute to global type reference.
                * @param xref a reference to a globally defined element
                * @return a xref to a global type
                */
               ModelRef< TypeModel> resolveElementRef(const QName& xref)
               {
                  if (xref == QName::xsQName("any")) {
                     return _anyElement;
                  }

                  LogEntry(logger()).debugging() << __FUNCTION__ << " : " << xref << doLog;
                  ModelRef< TypeModel> ref = _schema.getElementModel(xref);
                  if (!ref) {
                     LogEntry(logger()).severe() << "Element not defined " << xref << doLog;
                  }
                  return ref;
               }

               /**
                * Resolve an attribute to global type reference.
                * @param xref a reference to a globally defined group
                * @return a xref to a global type
                */
               ModelRef< TypeModel> resolveGroupRef(const QName& xref)
               {
                  LogEntry(logger()).debugging() << __FUNCTION__ << " : " << xref << doLog;
                  ModelRef< TypeModel> ref = _schema.getGroupModel(xref);
                  if (!ref) {
                     LogEntry(logger()).severe() << "Group not defined " << xref << doLog;
                  }
                  return ref;
               }

               /**
                * Resolve an attribute to global type reference.
                * @param xref a reference to a globally defined type
                * @return a xref to a global type
                */
               ModelRef< TypeModel> resolveTypeRef(const QName& xref)
               {
                  //FIXME: somewhat inelegant to hardcode this here.
                  if (xref == QName::xsQName("anySimpleType")) {
                     return _anySimpleType;
                  }

                  LogEntry(logger()).debugging() << __FUNCTION__ << " : " << xref << doLog;
                  ModelRef< TypeModel> ref = _schema.getTypeModel(xref);
                  if (!ref) {
                     LogEntry(logger()).severe() << "Type not defined " << xref << doLog;
                     assert(false);
                  }
                  return ref;
               }

               /**
                * Push an empty slot for a type model onto the stack.
                * @param n the node that defines the type
                * @param name the name of the type
                * @return the type binding
                */
               void pushTypeModel(const Reference< ::xylon::xsdom::Node>&, const String& name)
               {
                  TypeModelInfo info = _typeStack.top().newChild();
                  info._name = name;
                  _typeStack.push(info);
               }

               /**
                * Get the name of the type on top of the stack.
                * @return the name of the type on top of the type stack.
                */
               String getModelName() const throws()
               {
                  return _typeStack.top()._name;
               }

               /**
                * Get the name of the type on top of the stack.
                * @return the name of the type on top of the type stack.
                */
               const GlobalName& getGlobalName() const throws()
               {
                  return _typeStack.top()._globalName;
               }

               /**
                * Get the scope for the model on top of the stack.
                * @return the type model for the parent
                */
               Pointer< ScopeModel> getModelScope() const throws()
               {
                  return _typeStack.top()._scope;
               }

               /**
                * Bind the model on top of the stack to the specified type.
                * @param t a new type
                */
               void bindTypeModel(const Reference< DefaultTypeModel> model)throws()
               {
                  assert(!_typeStack.top()._model && "Model already bound");
                  _typeStack.top()._model = model;
                  Pointer< DefaultTypeModel> scope = _typeStack.top()._scope.tryDynamicCast< DefaultTypeModel> ();
                  if (scope) {
                     scope->addLocalTypeModel(_typeStack.top()._model);
                  }
               }

               /**
                * Determine if the type on top of the stack is bound.
                * @return true if the type on top of the type stack is bound
                */
               bool isTypeModelBound() const throws()
               {
                  return _typeStack.top()._model;
               }

               /**
                * Get the current type on the type stack. Invoking this method with an empty type stack is prohibited.
                * @return the type that was on the stack.
                */
               template<class T>
               Pointer< T> topType() const
               {
                  assert(!_typeStack.empty() && "Type stack is empty");
                  TypeModelInfo t = _typeStack.top(); // must be a local type binding
                  assert(t._model && "topType: no model defined");
                  return t._model.tryDynamicCast< T> ();
               }

               /**
                * Pop the current type on the type stack. Invoking this method with an empty type stack is prohibited.
                * @return the type that was on the stack.
                */
               Reference< TypeModel> popType()
               {
                  assert(!_typeStack.empty() && "Type stack is empty");
                  TypeModelInfo t = _typeStack.top(); // must be a local type binding
                  _typeStack.pop();
                  assert(t._model && "popType: no model defined");
                  return t._model;
               }

               /**
                * Pop the current type on the type stack and set the type's parse and write instructions
                * to those of the specified node.
                * @return the type that was on the stack.
                */
               Reference< TypeModel> popType(const Reference< ::xylon::xsdom::Node>& n)
               {
                  Reference< TypeModel> t = popType();

                  Reference< DefaultTypeModel> def = t;
                  def->setParseInstruction(getPI(n));
                  def->setWriteInstruction(getWI(n));

                  return t;
               }

               Reference< AttributeModel> newAttribute(const String& name, ModelRef< TypeModel> type,
                     const ::xylon::xsdom::Occurrence& occurs, AttributeModel::Type attrType)
               {
                  Reference< TypeModel> scope = _typeStack.top()._model;
                  return new DefaultAttributeModel(name, scope, type, occurs, attrType);
               }

               /**
                * Get the attribute binding.
                * @param n a node
                * @return an attribute binding
                */
               Reference< AttributeModel> getAttributeBinding(const Reference< ::xylon::xsdom::Node>& n)
               {
                  NodeBindings::const_iterator i = _attributeBindings.find(n);
                  if (i == _attributeBindings.end()) {
                     logger().bug("Expected an attribute binding");
                     assert(false);
                     throw ::std::logic_error("Attribute binding not found");
                  }
                  return i->second;
               }

               /**
                * Get the attribute binding.
                * @param n a node
                * @return an attribute binding
                */
               ::std::vector< Reference< AttributeModel> > getAttributeBindings(
                     const Reference< ::xylon::xsdom::Node>& n)
               {
                  ::std::vector< Reference< AttributeModel> > res;

                  ::std::pair < NodeBindings::const_iterator, NodeBindings::const_iterator > range
                        = _attributeBindings.equal_range(n);
                  for (NodeBindings::const_iterator i = range.first; i != range.second; ++i) {
                     res.push_back(i->second);
                  }
                  if (res.empty()) {
                     logger().bug("Expected an attribute binding");
                     assert(false);
                     throw ::std::logic_error("Attribute binding not found");
                  }
                  return res;
               }

               /**
                * Create an instruction that validates that the expected attributes are specified.
                * @param e an element
                */
               Pointer< Instruction> createValidateAttributesInstruction(
                     const ::xylon::xsdom::AllowedAttributes::AttributeNames* names)
               {
                  // FIXME: support STRICT, LAX, and SKIP for anyAttribute
                  Reference< Instruction> pi = Error::create();
                  if (names != 0) {
                     vector< QName> qnames;
                     vector< String> ns;
                     if (names->isAnyAttributeAllowed()) {
                        // any attribute is allowed, so no error
                        pi = Sequence::create();
                     }
                     else {
                        ns = names->allowedNamespaces();
                        for (size_t i = 0; i < ns.size(); ++i) {
                           pi = OnAttributeNamespace::create(ns[i], Pointer< Instruction> (), pi);
                        }
                     }
                     // apply this first
                     ns = names->prohibitedNamespaces();
                     for (size_t i = 0; i < ns.size(); ++i) {
                        pi = OnAttributeNamespace::create(ns[i], Error::create(), pi);
                     }

                     qnames = names->optionalAttributes();
                     for (size_t i = 0; i < qnames.size(); ++i) {
                        pi = OnAttribute::create(qnames[i], Pointer< Instruction> (), pi);
                     }
                     qnames = names->requiredAttributes();
                     for (size_t i = 0; i < qnames.size(); ++i) {
                        pi = OnAttribute::create(qnames[i], Pointer< Instruction> (), pi);
                     }

                     qnames = names->prohibitedAttributes();
                     for (size_t i = 0; i < qnames.size(); ++i) {
                        pi = OnAttribute::create(qnames[i], Error::create(), pi);
                     }
                  }
                  return ForeachAttribute::create(pi);
               }

               /**
                * Create an instruction that validates that the expected attributes are specified.
                * @param e an element
                */
               Pointer< Instruction> createValidateAttributesInstruction(const ::xylon::xsdom::Element& e)
               {
                  if (e.elementType()) {
                     return createValidateAttributesInstruction(_attributes.elementAttributes(e.qname()));
                  }
                  return Pointer< Instruction> ();
               }

               /**
                * Create an instruction that validates that the expected attributes are specified.
                * @param e a type (complex or simple)
                */
               Pointer< Instruction> createValidateAttributesInstruction(const ::xylon::xsdom::Type& e)
               {
                  return createValidateAttributesInstruction(_attributes.typeAttributes(e.qname()));
               }

               /**
                * Create an attribute binding. The enclosing type of the attribute is the one on top of the stack.
                * @param n the node that defines the type
                * @param name the name of the attribute
                * @return the attribute binding
                */
               Pointer< AttributeModel> createAttributeBinding(const Reference< ::xylon::xsdom::Node>& n,
                     const String& name, ModelRef< TypeModel> type, const AttributeModel::Type attrType,
                     const ::xylon::xsdom::Occurrence& occurs = ::xylon::xsdom::Occurrence::required())
               {
                  if (!type) {
                     LogEntry(logger()).warn() << "No global type for attribute '" << name
                           << "' found. Cannot bind attribute" << doLog;
                     return Pointer< AttributeModel> ();
                  }

                  LogEntry(logger()).debugging() << "Created attribute binding '" << name << "' for type "
                        << (type.isBound() ? type->globalName().string() : String("[unbound]")) << doLog;

                  //FIXME: get the multiplicity for the attribute
                  Reference< AttributeModel> bnd = newAttribute(name, type, occurs, attrType);
                  _typeStack.top()->addAttributeModel(bnd);

                  _attributeBindings.insert(NodeBindings::value_type(n, bnd));
                  return bnd;
               }

               /**
                * Create an attribute binding. The enclosing type of the attribute is the one on top of the stack.
                * @param n the node that defines the type
                * @param name the name of the attribute
                * @return the attribute binding
                */
               Pointer< AttributeModel> createAttributeBinding(const Reference< ::xylon::xsdom::Node>& n,
                     const String& name, const QName& qname, const AttributeModel::Type attrType,
                     const ::xylon::xsdom::Occurrence& occurs = ::xylon::xsdom::Occurrence::required())
               {
                  ModelRef< TypeModel> type = resolveTypeRef(qname);
                  if (!type) {
                     LogEntry(logger()).warn() << "No global type model for " << qname << " found. Attribute " << name
                           << " cannot be bound" << doLog;
                     return Pointer< AttributeModel> ();
                  }
                  return createAttributeBinding(n, name, type, attrType, occurs);
               }

               /**
                * Pop the top off the type stack and use that type as the type of a new attribute binding for the type
                * that has become the top of the type stack.
                * @param n the node that defines the type
                * @param name the name of the attribute
                * @return the attribute binding
                */
               Reference< AttributeModel> popAttributeBinding(const Reference< ::xylon::xsdom::Node>& n,
                     const String& name, const AttributeModel::Type xattrType,
                     const ::xylon::xsdom::Occurrence& occurs = ::xylon::xsdom::Occurrence::required())
               {
                  //FIXME: get the multiplicity for the attribute

                  // FIXME: why are not pasing the node to popType()? It turns out that the node may not yet
                  // have any instructions bounds to it; so it's not possible.
                  ModelRef< TypeModel> attrType = popType();
                  assert(attrType.isBound() && "Attribute type has not been bound; how come? it's a local type");

                  Reference< AttributeModel> bnd = newAttribute(name, attrType, occurs, xattrType);
                  _typeStack.top()->addAttributeModel(bnd);
                  _attributeBindings.insert(NodeBindings::value_type(n, bnd));
                  return bnd;
               }

               void bindBaseElement(const QName& ref)
               {
                  // get the current type on the stack

                  // the enclosing type in which this attribute is defined is the top-level class
                  assert(!_typeStack.empty());
                  _typeStack.top()->setBaseType(resolveElementRef(ref));
                  LogEntry(logger()).debugging() << "bound baseclass " << ref << " to " << _typeStack.top()->name()
                        << doLog;
               }

               void bindBaseType(const QName& ref)
               {
                  // get the current type on the stack
                  if (_typeStack.top().globalName().type() == GlobalName::ELEMENT) {
                     // don't bind a global element
                     return;
                  }
                  // the enclosing type in which this attribute is defined is the top-level class
                  _typeStack.top()->setBaseType(resolveTypeRef(ref));
                  LogEntry(logger()).debugging() << "bound  baseclass " << ref << " to " << _typeStack.top()->name()
                        << doLog;
               }

               /**
                * Determine the multiplicity of the enumerable node and multiply it by
                * any occurrence values already on the stack. If the occurrence stack
                * is empty the the node add its occurrence value to the stack
                */
               void pushOccurs(const Reference< ::xylon::xsdom::Enumerable>& node)
               {
                  if (_occurs.empty()) {
                     _occurs.push(node->occurs());
                  }
                  else {
                     ::xylon::xsdom::Occurrence occurs = node->occurs();
                     _occurs.push(_occurs.top().multiply(occurs.min(), occurs.max()));
                  }
               }

               /**
                * Pop an occurs off the stack
                * @return the current occurs value
                */
               ::xylon::xsdom::Occurrence popOccurs()
               {
                  ::xylon::xsdom::Occurrence res = _occurs.top();
                  _occurs.pop();
                  return res;
               }

               /**
                * Pop an occurs off the stack
                * @return the current occurs value
                */
               ::xylon::xsdom::Occurrence pushPopOccurs(const Reference< ::xylon::xsdom::Enumerable>& node)
               {
                  pushOccurs(node);
                  return popOccurs();
               }

               /**
                * Bind a parse instruction.
                * @param node a node
                * @param pi a parse instruction
                */
               Reference< Instruction> bindPI(const Reference< ::xylon::xsdom::Node>& node, Reference< Instruction> pi)
               {
                  if (_parseInstructions.find(node) != _parseInstructions.end()) {
                     logger().bug("Duplicate parse instruction for node generated");
                  }
                  _parseInstructions.insert(BindInstructions::value_type(node, pi));
                  return pi;
               }

               /**
                * Bind a parse instruction.
                * @param node a node
                * @param pi a parse instruction
                */
               Reference< Instruction> bindPI(const Reference< ::xylon::xsdom::Node>& node, const ::std::vector<
                     Reference< Instruction> >& pi)
               {
                  return bindPI(node, Sequence::create(pi));
               }

               /**
                * Bind a write instruction.
                * @param node a node
                * @param wi a write instruction
                */
               Reference< Instruction> bindWI(const Reference< ::xylon::xsdom::Node>& node, Reference< Instruction> wi)
               {
                  if (_writeInstructions.find(node) != _writeInstructions.end()) {
                     LogEntry(logger()).bug() << "Duplicate write instruction for node generated "
                           << typeid(*node) << doLog;
                  }
                  _writeInstructions.insert(BindInstructions::value_type(node, wi));
                  return wi;
               }

               /**
                * Bind a write instruction.
                * @param node a node
                * @param wi a vector of write instructions
                */
               Reference< Instruction> bindWI(const Reference< ::xylon::xsdom::Node>& node, const ::std::vector<
                     Reference< Instruction> >& wi)
               {
                  return bindWI(node, Sequence::create(wi));
               }

               /**
                * Get the parse instruction for a node. If a parse instruction does not exist, then one
                * is created on-demand.
                * @param node a node
                * @return a parse instruction
                */
               Reference< Instruction> getPI(const Reference< ::xylon::xsdom::Node>& node)
               {
                  BindInstructions::iterator i = _parseInstructions.find(node);
                  if (i != _parseInstructions.end()) {
                     return i->second;
                  }
                  logger().warn("No parse instruction found; need to create one on-demand");
                  Reference< Instruction> inst = Sequence::create();
                  _parseInstructions.insert(BindInstructions::value_type(node, inst));
                  return inst;
               }

               /**
                * Get the parse instruction for a node. If a parse instruction does not exist, then one
                * is created on-demand.
                * @param node a node
                * @return a parse instruction
                */
               Reference< Instruction> getWI(const Reference< ::xylon::xsdom::Node>& node)
               {
                  BindInstructions::iterator i = _writeInstructions.find(node);
                  if (i != _writeInstructions.end()) {
                     return i->second;
                  }
                  logger().warn("No write instruction found; need to create one on-demand");
                  assert(false);
                  Reference< Instruction> inst = Sequence::create();
                  _writeInstructions.insert(BindInstructions::value_type(node, inst));
                  return inst;
               }

               void visitNode(const Pointer< ::xylon::xsdom::Node>& node)
               {
                  if (!node) {
                     return;
                  }

                  LogEntry(logger()).debugging() << "Visiting node" << typeid(*node) << doLog;
                  node->accept(*this);
                  BindInstructions::iterator i = _parseInstructions.find(node);
                  if (i == _parseInstructions.end()) {
                     Reference< Instruction> inst = Sequence::create();
                     _parseInstructions.insert(BindInstructions::value_type(node, inst));
                  }
               }

               Pointer< Instruction> createOnAnyInstruction(const Reference< ::xylon::xsdom::Node>& node, Pointer<
                     Instruction> ifTrue, Pointer< Instruction> ifFalse) const throws()
               {
                  vector< Reference< ::xylon::xsdom::Any> > anys = _firstSets.anyOf(node);
                  set< String> allowedNS;
                  set< String> disallowedNS;

                  for (size_t i = 0; i < anys.size(); ++i) {
                     const set< String>& ns = anys[i]->namespaces();
                     if (ns.count("##any") != 0) {
                        // we've got the instruction
                        allowedNS.clear();
                        disallowedNS.clear();
                        return OnValidElement::create(ifTrue, ifFalse);
                     }
                     else if (ns.count("##other") != 0) {
                        disallowedNS.insert(String());
                        disallowedNS.insert(anys[i]->targetNamespace());
                     }
                     else {
                        allowedNS.insert(ns.begin(), ns.end());
                     }
                  }

                  // remove those disallowed namespaces that are explicitly allowed
                  for (set< String>::iterator i = allowedNS.begin(); i != allowedNS.end(); ++i) {
                     disallowedNS.erase(*i);
                  }

                  // we any must be parsed like this:
                  Pointer< Instruction> res = ifFalse;
                  // this construct is equivalent to: if (isAllowed() || !isDisallowed()) { ifTrue } else { ifFalse }
                  if (!disallowedNS.empty()) {
                     res = OnElementNamespace::create(disallowedNS, ifFalse, ifTrue);
                  }
                  if (!allowedNS.empty()) {
                     res = OnElementNamespace::create(allowedNS, ifTrue, res);
                  }

                  return res;
               }

               Pointer< Instruction> createOnFirstInstruction(const Reference< ::xylon::xsdom::Node>& node, Pointer<
                     Instruction> ifTrue, Pointer< Instruction> ifFalse) const throws()
               {
                  set< QName> fs = findFirstSet(node);
                  Reference< Instruction> res = OnElement::create(fs, ifTrue, createOnAnyInstruction(node, ifTrue,
                              ifFalse), false);
                  return res;
               }

               string firstSetToString(const Reference< ::xylon::xsdom::Enumerable>& obj)
               {
                  string res;

                  set< QName> n = findFirstSet(obj);
                  for (set< QName>::const_iterator i = n.begin(); i != n.end(); ++i) {
                     res += ' ';
                     res += i->string().string();
                  }

                  return res;
               }
               /**
                * Make an enumerable instruction and associate it with the specified instruction.
                * @param obj an enumerable node
                * @param inst an instruction for the content of the enumerable.
                * @return the instruction.
                */
               void visitEnumerable(const Pointer< ::xylon::xsdom::Enumerable>& obj)
               {
                  if (!obj) {
                     return;
                  }

                  visitNode(obj);

                  Reference< Instruction> res = getPI(obj);

                  // remove the instruction and instead wrap it in a loop
                  _parseInstructions.erase(obj);

                  // no need for a loop instruction if the occurrence count is 1,
                  // but because the enumerable instructions might include a Break
                  // we need to have a loop to ensure that we're not breaking out of the
                  // the wrong loop.

                  if (obj->occurs().min() == 0 || _firstSets.allowsEmpty(obj)) {

                     // make the loop mandatory, but use an OnElement to see if
                     // it should be executed
                     res = Loop::create(::xylon::xsdom::Occurrence(1, obj->occurs().max()), res);
                     res = createOnFirstInstruction(obj, res, Pointer< Instruction> ());
                  }
                  else {
                     res = Loop::create(obj->occurs(), res);
                  }
                  bindPI(obj, res);
               }

               template<class T>
               void visitAttributes(const Reference< T>& node, Pointer< Instruction>& resPI,
                     Pointer< Instruction>& resWI)
               {
                  resWI = resPI = Pointer< Instruction> ();

                  vector< Reference< Instruction> > pi, wi;
                  {
                     visitNode(node->anyAttribute());
                     if (node->anyAttribute()) {
                        pi.push_back(getPI(node->anyAttribute()));
                        wi.push_back(getWI(node->anyAttribute()));
                     }
                     for (size_t i = 0; i < node->attributes().size(); ++i) {
                        visitNode(node->attributes()[i]);
                        pi.push_back(getPI(node->attributes()[i]));
                        wi.push_back(getWI(node->attributes()[i]));
                     }
                     for (size_t i = 0; i < node->attributeGroups().size(); ++i) {
                        visitNode(node->attributeGroups()[i]);
                        pi.push_back(getPI(node->attributeGroups()[i]));
                        wi.push_back(getWI(node->attributeGroups()[i]));
                     }
                  }

                  if (!pi.empty()) {
                     resPI = Sequence::create(pi);
                  }
                  if (!wi.empty()) {
                     resWI = Sequence::create(wi);

                  }

               }

               void visitAll(const Reference< ::xylon::xsdom::All>& node)
               {
                  {
                     pushOccurs(node);
                     for (size_t i = 0; i != node->items().size(); ++i) {
                        visitEnumerable(node->items()[i]);
                     }
                     popOccurs();
                  }

                  {
                     vector< Reference< Instruction> > wi;
                     for (size_t i = 0; i != node->items().size(); ++i) {
                        wi.push_back(getWI(node->items()[i]));
                     }
                     bindWI(node, wi);
                  }

                  {
                     OnAll::Candidates required, optional;

                     for (size_t i = 0; i < node->items().size(); ++i) {
                        Reference< ::xylon::xsdom::Enumerable> e = node->items()[i];

                        set< QName> names = findFirstSet(e);
                        Reference< Instruction> pi = getPI(e);
                        OnAll::Candidate c;

                        for (set< QName>::const_iterator j = names.begin(); j != names.end(); ++j) {
                           c.insert(make_pair(*j, pi));
                        }
                        if (e->occurs().min() == 0) {
                           optional.push_back(c);
                        }
                        else {
                           required.push_back(c);
                        }
                     }
                     bindPI(node, OnAll::create(required, optional));
                  }
               }

               void visitAny(const Reference< ::xylon::xsdom::Any>& node)
               {
                  {
                     createAttributeBinding(node, "anyElement", _anyElement, AttributeModel::OTHER, pushPopOccurs(node));
                  }

                  // write instruction
                  {
                     Reference< AttributeModel> attr = getAttributeBinding(node);
                     bindWI(node, WriteAttribute::create(attr));
                  }

                  //FIXME: when strict, this really just transforms into onElement with all the possible elements
                  //FIXME when LAX, we create a combo on ElementObject and a just
                  {
                     Reference< AttributeModel> attr = getAttributeBinding(node);

                     Pointer< Instruction> pi = Error::create();
                     Reference< Instruction> skip = Sequence::create(ActivateElement::create(), BindElement::create(),
                           MoveCursor::createNext());
                     Reference< Instruction> strict = Sequence::create(CreateAndParse::create(), Bind::create());

                     // first, create the parse instruction
                     switch (node->getProcessContents()) {
                        case ::xylon::xsdom::Any::STRICT:
                           pi = strict;
                           break;
                        case ::xylon::xsdom::Any::LAX:
                           pi = OnValidElement::create(true, strict, skip);
                           break;
                        default:
                           pi = skip;
                           break;
                     }

                     // generate instructions to inspect the namespaces
                     {
                        ::std::set< String> ns = node->namespaces();
                        // the object currently on the stack should be the anyElement; this binds the
                        // current element

                        // make sure the current element is accepted by the namespace
                        if (ns.count("##any") != 0) {
                           // noop
                        }
                        else if (ns.count("##other") != 0) {
                           // reset the namespace list to those that are not allowed for ##other
                           ns.clear();
                           ns.insert(String());
                           ns.insert(node->targetNamespace());
                           pi = OnElementNamespace::create(ns, Error::create(
                                 "Expected qualified element with a namespace other than "
                                       + node->targetNamespace().string()), pi);
                        }
                        else {
                           pi = OnElementNamespace::create(ns, pi, Error::create("Namespace not allowed"));
                        }
                     }

                     ::std::vector< Reference< Instruction> > xpi;
                     xpi.push_back(NewInstance::create(attr->type()));
                     xpi.push_back(pi);
                     xpi.push_back(BindAttribute::create(attr));
                     bindPI(node, xpi);
                  }
               }

               void visitAnyAttribute(const Reference< ::xylon::xsdom::AnyAttribute>& node)
               {
                  // there is no binding to an object; any attribute is simply a validation tool to
                  // allow specification of additional attributes for other purposes
                  // eventually, this needs to be a map of attributes
                  {
                  }

                  {
                     bindWI(node, Sequence::create());
                  }

                  {
                     Pointer< Instruction> pi = Sequence::create();
#if 0
                     pi = Error::createNext();
                     Reference< Instruction> skip = Sequence::createNext(ActivateElement::createNext(), BindElement::createNext(),
                           MoveCursor::createNext());
                     Reference< Instruction> strict = Sequence::createNext(CreateAndParse::createNext(), Bind::createNext());

                     ::std::vector< Reference< Instruction> > xpi;
                     // first, create the parse instruction
                     switch (node->getProcessContents()) {
                        case ::xylon::xsdom::AnyAttribute::STRICT:
                        pi = strict;
                        break;
                        case ::xylon::xsdom::AnyAttribute::LAX:
                        pi = OnValidElement::createNext(true, strict, skip);
                        break;
                        default:
                        pi = skip;
                        break;
                     }
#endif

                     bindPI(node, pi);
                  }
               }

               void visitAttribute(const Reference< ::xylon::xsdom::Attribute>& node)
               {

                  {
                     ::xylon::xsdom::Occurrence occurs;
                     switch (node->use()) {
                        case ::xylon::xsdom::Attribute::USE_OPTIONAL:
                           occurs = ::xylon::xsdom::Occurrence::optional();
                           break;
                        case ::xylon::xsdom::Attribute::USE_REQUIRED:
                           occurs = ::xylon::xsdom::Occurrence::required();
                           break;
                        case ::xylon::xsdom::Attribute::USE_PROHIBITED:
                           occurs = ::xylon::xsdom::Occurrence::prohibited();
                           break;
                     }

                     // create any bindings
                     if (occurs.max() != 0) {
                        if (node->ref()) {
                           createAttributeBinding(node, node->ref().name().name(), resolveAttributeRef(
                                 node->ref().name()), AttributeModel::ATTRIBUTE, occurs);
                        }
                        else if (node->type()) {
                           createAttributeBinding(node, node->qname().name(), node->type().name(),
                                 AttributeModel::ATTRIBUTE, occurs);
                        }
                        else if (node->attributeType()) {
                           pushTypeModel(node, node->qname().name());
                           visitNode(node->attributeType());
                           popAttributeBinding(node, node->qname().name(), AttributeModel::ATTRIBUTE, occurs);
                        }
                        else {
                           createAttributeBinding(node, node->qname().name(), QName::xsQName("anySimpleType"),
                                 AttributeModel::ATTRIBUTE, occurs);
                        }
                     }
                  }

                  {
                     ::std::vector< Reference< Instruction> > pi, wi;
                     Pointer< Instruction> ifTrue, ifFalse;
                     QName qname;
                     if (node->use() == ::xylon::xsdom::Attribute::USE_PROHIBITED) {
                        ifTrue = Error::create();
                        if (node->ref()) {
                           qname = node->ref()->qname(true);
                        }
                        else {
                           qname = node->qname(true);
                        }
                     }
                     else {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        if (node->ref()) {
                           qname = node->ref()->qname(true);
                           // instantiate an object and push the type onto the stack
                           pi.push_back(CreateAndParse::create(attr->type()));
                           // bind the contents of the type to the current
                           pi.push_back(BindAttribute::create(attr));
                           wi.push_back(WriteAttribute::create(attr));
                        }
                        else {
                           qname = node->qname(true);

                           wi.push_back(WriteAttribute::create(attr, StartWriteObject::createAttribute(qname),
                                 EndWriteObject::createAttribute(qname)));

                           if (node->attributeType()) {
                              pi.push_back(NewInstance::create(attr->type()));
                              //FIXME: WHY can't it be attr->type()???
                              // pi.push_back(ParseInstance::create(attr->type()));
                              pi.push_back(getPI(node->attributeType()));
                           }
                           else {
                              pi.push_back(CreateAndParse::create(attr->type()));
                           }
                           pi.push_back(BindAttribute::create(attr));
                        }
                        // DO the bind here
                        if (node->use() == ::xylon::xsdom::Attribute::USE_REQUIRED) {
                           ifFalse = Error::create();
                        }

                        ifTrue = Sequence::create(pi);
                     }
                     bindWI(node, wi);
                     bindPI(node, ActivateAttribute::create(qname, ifTrue, ifFalse));
                  }
               }

               void visitAttributeGroup(const Reference< ::xylon::xsdom::AttributeGroup>& node)
               {
                  {
                     if (node->ref()) {
                        createAttributeBinding(node, node->ref().name().name(), resolveAttributeGroupRef(
                              node->ref().name()), AttributeModel::OTHER, ::xylon::xsdom::Occurrence::required());
                     }
                  }

                  {
                     ::std::vector< Reference< Instruction> > wi;
                     if (node->ref()) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        wi.push_back(WriteAttribute::create(attr));
                     }
                     bindWI(node, wi);
                  }

                  {
                     ::std::vector< Reference< Instruction> > pi;
                     if (node->ref()) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        pi.push_back(CreateAndParse::create(attr->type()));
                        pi.push_back(BindAttribute::create(attr));
                     }
                     bindPI(node, pi);

                  }
               }

               void visitComplexContent(const Reference< ::xylon::xsdom::ComplexContent>& node)
               {
                  {
                     visitNode(node->derivation());
                  }
                  {
                     // the parse instruction is just a pass-through
                     bindWI(node, getWI(node->derivation()));
                     bindPI(node, getPI(node->derivation()));
                  }
               }

               void visitUrType(const Reference< ::xylon::xsdom::UrType>& node)
               {
                  // FIXME: nothing to be done for the ur-type, yet
               }

               void visitComplexType(const Reference< ::xylon::xsdom::ComplexType>& node)
               {
                  _domTypeStack.push(node);
                  Pointer< Instruction> attrPi, attrWi;

                  // create a top-level type and push it onto the stack
                  {
                     Pointer< DefaultTypeModel> m;
                     if (!isTypeModelBound()) {
                        if (getGlobalName()) {
                           m = new DefaultTypeModel(getModelName(), getGlobalName(), getModelScope());
                        }
                        else {
                           m = new DefaultTypeModel(getModelName(), getModelScope());
                        }
                        LogEntry(logger()).info() << "Created new type " << getModelName() << ", " << getGlobalName()
                              << doLog;
                        m->setComplex(true);
                        bindTypeModel(m);
                     }
                     if (node->contentType()) {
                        visitNode(node->contentType());
                     }
                     else {
                        if (node->enumerable()) {
                           bindBaseType(QName::xsQName("anyType"));
                           visitEnumerable(node->enumerable());
                        }
                        visitAttributes(node, attrPi, attrWi);
                     }
                  }

                  {
                     ::std::vector< Reference< Instruction> > wi;
                     if (node->contentType()) {
                        wi.push_back(getWI(node->contentType()));
                     }
                     else {
                        if (attrWi) {
                           wi.push_back(attrWi);
                        }
                        if (node->enumerable()) {
                           wi.push_back(getWI(node->enumerable()));
                        }
                     }
                     bindWI(node, wi);
                     topType< DefaultTypeModel> ()->setWriteInstruction(getWI(node));
                  }

                  {
                     if (node->contentType()) {
                        assert(!attrPi && "Cannot have attrs if there is a content type");
                        bindPI(node, getPI(node->contentType()));
                     }
                     else {
                        ::std::vector< Reference< Instruction> > pi;

                        // first, parse the attributes

                        // need to push down, regardless of whether we're expecting content
                        if (attrPi) {
                           pi.push_back(attrPi);
                        }
                        pi.push_back(MoveCursor::createDown());
                        if (node->enumerable()) {
                           pi.push_back(getPI(node->enumerable()));
                        }
                        bindPI(node, pi);
                     }
                  }
                  _domTypeStack.pop();
               }

               void visitElement(const Reference< ::xylon::xsdom::Element>& node)
               {
                  {
                     if (node->ref()) {
                        createAttributeBinding(node, node->ref().name().name(), resolveElementRef(node->ref().name()),
                              AttributeModel::ELEMENT, pushPopOccurs(node));
                     }
                     else if (node->type()) {
                        createAttributeBinding(node, node->qname().name(), node->type().name(),
                              AttributeModel::ELEMENT, pushPopOccurs(node));
                     }
                     else if (node->elementType()) {
                        stack< ::xylon::xsdom::Occurrence> bak;
                        bak.swap(_occurs);
                        // need to create a local type name that is different from the attribute name

                        pushTypeModel(node, node->qname().name());
                        visitNode(node->elementType());
                        _occurs.swap(bak);
                        popAttributeBinding(node, node->qname().name(), AttributeModel::ELEMENT, pushPopOccurs(node));
                     }
                     else {
                        pushTypeModel(node, node->qname().name());
                        bindTypeModel(new DefaultTypeModel(getModelName(), getModelScope()));
                        // need to push down, because one of the instructions below performs a up element
                        popAttributeBinding(node, node->qname().name(), AttributeModel::ELEMENT, pushPopOccurs(node));
                     }
                     //                     bindBaseElement(QName::xsQName("any"));

                  }

                  {
                     ::std::vector< Reference< Instruction> > wi;

                     if (node->ref()) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        wi.push_back(WriteAttribute::create(attr));
                     }
                     else if (node->type()) {
                        QName qname = node->qname(true);
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        wi.push_back(WriteAttribute::create(attr, StartWriteObject::createElement(qname),
                              EndWriteObject::createElement(qname)));
                     }
                     else if (node->elementType()) {
                        QName qname = node->qname(true);
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        wi.push_back(WriteAttribute::create(attr, StartWriteObject::createElement(qname),
                              EndWriteObject::createElement(qname)));
                        //                               wi.push_back(getWI(node->elementType()));
                     }
                     else {
                        QName qname = node->qname(true);
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        wi.push_back(WriteAttribute::create(attr, StartWriteObject::createElement(qname),
                              EndWriteObject::createElement(qname)));
                     }
                     bindWI(node, wi);
                  }

                  {
                     ::std::vector< Reference< Instruction> > pi;
                     Reference< AttributeModel> attr = getAttributeBinding(node);
                     if (node->ref()) {
                        ModelRef< TypeModel> model = _schema.getElementModel(node->ref().name());
                        vector< Reference< Instruction> > xpi;
                        xpi.push_back(ActivateElement::create());
                        // create an instance of the element subclass and parse it
                        xpi.push_back(CreateAndParse::create(model));
                        // bind the parsed element to the attribute
                        xpi.push_back(BindAttribute::create(attr));

                        pi.push_back(OnElement::create(node->ref().name(), Sequence::create(xpi), Error::create(),
                              false));

                     }
                     else {
                        pi.push_back(ActivateElement::create());

                        if (node->type() || node->elementType()) {
                           if (node->type()) {
                              Pointer< Instruction> tmpPI(createValidateAttributesInstruction(*node->type().target()));
                              if (tmpPI) {
                                 pi.push_back(tmpPI);
                              }
                              pi .push_back(OnXsiType::create(attr->type()));
                              // need to call the instruction associated with the specified type;
                              // this is going to perform a push-instruction
                           }
                           else if (node->elementType()) {
                              //FIXME: collect all attributes
                              ::xylon::xsdom::AllowedAttributes::AttributeNames attrs(node->elementType());

                              Pointer< Instruction> tmpPI(createValidateAttributesInstruction(&attrs));
                              if (tmpPI) {
                                 pi.push_back(tmpPI);
                              }

                              // get the instruction for parsing the element type
                              pi.push_back(NewInstance::create(attr->type()));
                              // this is the parsing code
                              //FIXME: something's wrong here! why does attr->type() not have a parse instruction?
                              //pi.push_back(ParseInstance::create(attr->type()));
                              pi.push_back(getPI(node->elementType()));
                           }
                           switch (_schema.schema()->getContentModel(node)) {
                              case ::xylon::XsdSchema::ANY_CONTENT:
                              case ::xylon::XsdSchema::NO_CONTENT:
                                 // FIXME: we should actually check that there is indeed no content at all
                              case ::xylon::XsdSchema::SIMPLE_TYPE_CONTENT:
                              case ::xylon::XsdSchema::SIMPLE_CONTENT:
                                 pi.push_back(MoveCursor::createDown());
                                 break;
                              default:
                                 break;
                           }
                        }
                        else {
                           // get the instruction for parsing the element type
                           pi.push_back(NewInstance::create(attr->type()));
                           pi.push_back(MoveCursor::createDown());
                        }

                        // it's an error if there are any unprocessed child elements after the type has been parsed
                        pi.push_back(OnValidElement::create(Error::create(), MoveCursor::createUp()));

                        // since the type generating is going to drop-down into the child, we need to drop back up
                        pi.push_back(BindAttribute::create(attr));
                        pi.push_back(MoveCursor::createNext());
                     }

                     bindPI(node, OnElement::create(findFirstSet(node), Sequence::create(pi), Pointer< Instruction> (),
                           false));
                  }
               }

               void visitExtension(const Reference< ::xylon::xsdom::Extension>& node)
               {
                  Pointer< Instruction> attrPi, attrWi;
                  //FIXME: there are some interactions here with global elements
                  // that are part of a substitution group

                  {

                     if (!node->base()) {
                        throw ::std::runtime_error("No base specified for extension");
                     }
                     visitEnumerable(node->enumerable());
                     visitAttributes(node, attrPi, attrWi);

                     const QName qname = node->base().name();

                     // if the base is a simple type, then we need to bind it
                     if (node->base().target().tryDynamicCast< ::xylon::xsdom::SimpleType> ()) {
                        createAttributeBinding(node, "value", qname, AttributeModel::OTHER,
                              ::xylon::xsdom::Occurrence::required());
                     }
                     else {
                        bindBaseType(qname);
                     }
                  }

                  {
                     ::std::vector< Reference< Instruction> > wi;
                     // write the attribute before anything else
                     if (attrWi) {
                        wi.push_back(attrWi);
                     }

                     if (node->base().target().tryDynamicCast< ::xylon::xsdom::SimpleType> ()) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        wi.push_back(WriteAttribute::create(attr));
                     }
                     else {
                        // we can only have an enumerable if the base type is a complex type
                        // now, we emit the instruction for the enumerable
                        if (node->base()) {
                           wi.push_back(WriteBaseType::create());
                        }
                        if (node->enumerable()) {
                           wi.push_back(getWI(node->enumerable()));
                        }
                     }
                     bindWI(node, wi);
                  }

                  {
                     ::std::vector< Reference< Instruction> > pi;
                     if (attrPi) {
                        pi.push_back(attrPi);
                     }

                     // FIXME: create the instructions to parse the attributes first, then do the content of the current element
                     // need to call the instruction associated with the base type?
                     if (node->base().target().tryDynamicCast< ::xylon::xsdom::SimpleType> ()) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        pi.push_back(CreateAndParse::create(attr->type()));
                        pi.push_back(BindAttribute::create(attr));
                     }
                     else {

                        pi.push_back(ParseInstance::create(resolveTypeRef(node->base().name())));
                        // we can only have an enumerable if the base type is a complex type
                        // now, we emit the instruction for the enumerable
                        if (node->enumerable()) {
                           pi.push_back(getPI(node->enumerable()));
                        }
                     }
                     bindPI(node, pi);
                  }
               }

               void visitGroup(const Reference< ::xylon::xsdom::Group>& node)
               {
                  {
                     if (node->ref()) {

                        createAttributeBinding(node, node->ref().name().name(), resolveGroupRef(node->ref().name()),
                              AttributeModel::OTHER, pushPopOccurs(node));
                     }
                     else {
                        logger().warn("Group has no reference");
                     }

                     // FIXME: can a local group be defined, within the scope of an element only?
                  }

                  {
                     ::std::vector< Reference< Instruction> > wi;
                     if (node->ref()) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        // we've got an attribute binding here
                        wi.push_back(WriteAttribute::create(attr));
                     }
                     bindWI(node, wi);
                  }

                  {
                     ::std::vector< Reference< Instruction> > pi;
                     if (node->ref()) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        // we've got an attribute binding here
                        pi.push_back(CreateAndParse::create(attr->type()));
                        pi.push_back(BindAttribute::create(attr));
                     }
                     bindPI(node, pi);
                  }
               }

               void visitComplexTypeRestriction(const Reference< ::xylon::xsdom::Restriction>& node)
               {
                  bool haveBaseType = node->base() && node->base().name() != QName::xsQName("anyType");

                  Pointer< Instruction> attrPi, attrWi;
                  {
                     //FIXME: we really should create an attribute, say base
                     //       and create methods that forward all calls to the base
                     //       instead of replicating the data
                     //FIXME: since this is a restriction we can at least for now use the base type

                     if (haveBaseType) {
                        bindBaseType(node->base().name());
                     }
                     else {
                        if (node->simpleType()) {
                           //FIXME: ???	      bindClass(*b,"Type");
                           visitNode(node->simpleType());
                           //FIXME: ???              bindAttribute(*b,"value",node);
                        }
                        visitEnumerable(node->enumerable());
                        for (size_t i = 0; i < node->facets().size(); ++i) {
                           visitNode(node->facets()[i]);
                        }
                        visitAttributes(node, attrPi, attrWi);
                     }

                  }

                  {
                     ::std::vector< Reference< Instruction> > wi;

                     if (haveBaseType) {
                        wi.push_back(WriteBaseType::create());
                     }
                     else {
                        if (attrWi) {
                           wi.push_back(attrWi);
                        }
                        if (node->enumerable()) {
                           wi.push_back(getWI(node->enumerable()));
                        }
                     }
                     bindWI(node, wi);
                  }

                  {
                     ::std::vector< Reference< Instruction> > pi;

                     //FIXME: every restriction must have a base type!
                     if (haveBaseType) {
                        pi.push_back(ParseInstance::create(resolveTypeRef(node->base().name())));
                     }
                     else {
                        if (attrPi) {
                           pi.push_back(attrPi);
                        }
                        pi.push_back(MoveCursor::createDown());
                        if (node->enumerable()) {
                           pi.push_back(getPI(node->enumerable()));
                        }
                     }
                     bindPI(node, pi);
                  }
               }

               void visitRestriction(const Reference< ::xylon::xsdom::Restriction>& node)
               {
                  assert(!_domTypeStack.empty());
                  if (_domTypeStack.top().tryDynamicCast< ::xylon::xsdom::SimpleType> ()) {
                     visitSimpleTypeRestriction(node);
                  }
                  else {
                     visitComplexTypeRestriction(node);
                  }
               }

               void visitChoice(const Reference< ::xylon::xsdom::Choice>& node)
               {
                  {
                     pushOccurs(node);
                     for (size_t i = 0; i != node->items().size(); ++i) {
                        visitEnumerable(node->items()[i]);
                     }
                     popOccurs();
                  }

                  {
                     vector< Reference< Instruction> > wi;
                     for (size_t i = 0; i != node->items().size(); ++i) {
                        wi.push_back(getWI(node->items()[i]));
                     }
                     bindWI(node, wi);
                  }

                  {

                     bool optional = true;
                     Pointer< Instruction> res;

                     OnElement::Candidates candidates;

                     for (size_t i = 0; i != node->items().size(); ++i) {
                        Reference< ::xylon::xsdom::Enumerable> item = node->items()[i];
                        optional = optional && _firstSets.allowsEmpty(item);
                        set< QName> fs = findFirstSet(item);
                        Reference< Instruction> xpi = getPI(item);
                        for (set< QName>::const_iterator j = fs.begin(); j != fs.end(); ++j) {
                           candidates.insert(make_pair(*j, xpi));
                        }
                     }
                     if (optional) {
                        res = Pointer< Instruction> ();
                     }
                     else {
                        //FIXME: I think this should be an error
                     }

                     for (size_t i = 0; i != node->items().size(); ++i) {
                        Reference< ::xylon::xsdom::Enumerable> item = node->items()[i];
                        Reference< Instruction> xpi = getPI(item);
                        res = createOnAnyInstruction(item, xpi, res);
                     }
                     res = OnElement::create(candidates, res, false);

#if old
                     for (size_t i = 0; i != node->items().size(); ++i) {
                        Reference< ::xylon::xsdom::Enumerable> item = node->items()[i];
                        optional = optional && _firstSets.allowsEmpty(item);
                        res = createOnFirstInstruction(item, getPI(item), res);
                     }
                     if (optional) {
                        res = createOnFirstInstruction(node, res, Pointer< Instruction> ());
                     }
#endif
                     bindPI(node, res);
                  }
               }

               void visitSequence(const Reference< ::xylon::xsdom::Sequence>& node)
               {
                  {
                     pushOccurs(node);
                     for (size_t i = 0; i != node->items().size(); ++i) {
                        visitEnumerable(node->items()[i]);
                     }
                     popOccurs();
                  }

                  {
                     vector< Reference< Instruction> > wi;
                     for (size_t i = 0; i != node->items().size(); ++i) {
                        wi.push_back(getWI(node->items()[i]));
                     }
                     bindWI(node, wi);
                  }

#if 1
                  {
                     ::std::vector< Reference< Instruction> > pi(node->items().size(), Sequence::create());

                     bool optional = true;
                     for (size_t i = node->items().size(); i-- > 0;) {
                        Reference< ::xylon::xsdom::Enumerable> item = node->items()[i];
                        Pointer< Instruction> noMatch;
                        OnElement::Candidates candidates;

                        // the order in which we access "optional" is important. here, optional
                        // means all items following the current item are optional; if one
                        // of the followers is not optional, then we need to throw an error
                        // if we don't encounter the required elements
                        if (!optional) {
                           noMatch = Error::create();
                        }

                        for (size_t j = i + 1; j < node->items().size(); ++j) {
                           Reference< ::xylon::xsdom::Enumerable> jitem = node->items()[j];
                           // add the instructions
                           set< QName> fs = findFirstSet(jitem);
                           Reference< Instruction> xpi = pi[j];
                           for (set< QName>::const_iterator k = fs.begin(); k != fs.end(); ++k) {
                              candidates.insert(make_pair(*k, xpi));
                           }
                           noMatch = createOnAnyInstruction(jitem, xpi, noMatch);
                           if (!_firstSets.allowsEmpty(jitem)) {
                              break;
                           }
                        }

                        if (i == node->items().size() - 1) {
                           pi[i] = getPI(item);
                        }
                        else {
                           pi[i] = Sequence::create(getPI(item), OnElement::create(candidates, noMatch, false));
                        }
                        // update the "optional" flag
                        if (!_firstSets.allowsEmpty(item)) {
                           optional = false;
                        }

                     }

                     // create the instruction for the entire sequence
                     {
                        Pointer< Instruction> noMatch;
                        if (!optional) {
                           noMatch = Error::create();
                        }
                        OnElement::Candidates candidates;
                        for (size_t j = 0; j < node->items().size(); ++j) {
                           Reference< ::xylon::xsdom::Enumerable> jitem = node->items()[j];
                           // add the instructions
                           set< QName> fs = findFirstSet(jitem);
                           Reference< Instruction> xpi = pi[j];
                           for (set< QName>::const_iterator k = fs.begin(); k != fs.end(); ++k) {
                              candidates.insert(make_pair(*k, xpi));
                           }
                           noMatch = createOnAnyInstruction(jitem, xpi, noMatch);
                           if (!_firstSets.allowsEmpty(jitem)) {
                              break;
                           }
                        }
                        bindPI(node, Sequence::create(Comment::create("xs:sequence"), OnElement::create(candidates,
                              noMatch, false)));
                     }

                  }
#else
                  {

                     ::std::vector< Reference< Instruction> > pi, xpi;
                     bool optional = true;
                     for (size_t i = 0; i != node->items().size(); ++i) {
                        Reference< ::xylon::xsdom::Enumerable> item = node->items()[i];

                        // what happens, if we don't have a match for this enumerable
                        // this depends on whether or not the item is optional
                        // if it is not optional, then an Error must be issued if the element is not matched
                        Pointer< Instruction> noMatch;
                        if (!_firstSets.allowsEmpty(item)) {
                           optional = false;
                           noMatch = Error::createNext();
                        }
                        // generate the instruction for the item
                        pi.push_back(createOnFirstInstruction(item, getPI(item), noMatch));
                     }
                     Reference< Instruction> res = Sequence::createNext(pi);
                     if (optional) {
                        res = createOnFirstInstruction(node, res, Pointer< Instruction> ());
                     }
                     bindPI(node, res);
                  }
#endif
               }

               void visitSimpleContent(const Reference< ::xylon::xsdom::SimpleContent>& node)
               {
                  {
                     visitNode(node->derivation());
                  }

                  {
                     // the parse instruction is just a pass-through
                     bindWI(node, getWI(node->derivation()));
                     bindPI(node, getPI(node->derivation()));
                  }
               }

               void visitSimpleType(const Reference< ::xylon::xsdom::SimpleType>& node)
               {
                  const bool bound = isTypeModelBound();

                  _domTypeStack.push(node);
                  Pointer< DefaultTypeModel> model;
                  Pointer< ::xylon::xsdom::Node> child;
                  {

                     // if the type is already bound, then we need to create an attribute
                     if (bound) {
                        // create an unbound type on top of the stack
                        pushTypeModel(node, "SimpleType");
                     }
                     String modelName = getModelName();
                     Reference< ScopeModel> scope = getModelScope();

                     if (node->getUnion()) {
#if USE_UNION_TYPE_MODEL
                        model = new DefaultUnionTypeModel(modelName + String("_Union"), scope);
#else
                        if (getGlobalName()) {
                           model = new DefaultTypeModel(modelName + String("_XUnion"), getGlobalName(), scope);
                        }
                        else {
                           model = new DefaultTypeModel(modelName + String("_XUnion"), scope);
                        }
#endif
                        child = node->getUnion();
                     }
                     else if (node->list()) {
#if USE_LIST_TYPE_MODEL == 1
                        model = new DefaultListTypeModel(modelName + String("_List"), scope);
#else
                        if (getGlobalName()) {
                           model = new DefaultTypeModel(modelName + String("_XList"), getGlobalName(), scope);
                        }
                        else {
                           model = new DefaultTypeModel(modelName + String("_XList"), scope);
                        }
#endif
                        child = node->list();
                     }
                     else if (node->restriction()) {
                        if (getGlobalName()) {
                           model = new DefaultTypeModel(modelName, getGlobalName(), scope);
                        }
                        else {
                           model = new DefaultTypeModel(modelName, scope);
                        }
                        child = node->restriction();
                     }
                     model->setComplex(false);
                     bindTypeModel(model);
                     visitNode(child);
                     model->setWriteInstruction(getWI(child));

                     if (bound) {
                        // we need to pop the type and we create an attribute
                        popAttributeBinding(node, "impl", AttributeModel::OTHER, xylon::xsdom::Occurrence::required());
                     }
                  }

                  {
                     if (bound) {
                        bindWI(node, WriteAttribute::create(getAttributeBinding(node)));
                     }
                     else {
                        bindWI(node, getWI(child));
                     }
                     if (model) {
                        assert(model->writeInstruction());
                     }
                  }

                  {
                     ::std::vector< Reference< Instruction> > pi;
                     pi.push_back(ActivateValue::create());
                     if (bound) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        pi.push_back(CreateAndParse::create(attr->type()));
                        pi.push_back(BindAttribute::create(attr));
                     }
                     else {
                        // use the parse instruction of the child as this node's parse instruction
                        // FIXME: I'm not sure I really like this: we should associate processing instructions always with types
                        // or with nodes
                        // can we just associate the node instruction with the enclosing node?
                        pi.push_back(getPI(child));
                     }
                     bindPI(node, pi);
                  }
                  _domTypeStack.pop();
               }

               void visitSimpleTypeRestriction(const Reference< ::xylon::xsdom::Restriction>& node)
               {
                  {
                     if (node->base()) {
                        bindBaseType(node->base().name());
                     }
                     else if (node->simpleType()) {
                        pushTypeModel(node->simpleType(), "Value");
                        visitNode(node->simpleType());
                        // FIXME: this is quite hackish (in other places as well)
                        Pointer< DefaultTypeModel> tm = topType< DefaultTypeModel> ();
                        assert(tm && "Not a default model on the stack; how's that possible");
                        tm->setParseInstruction(getPI(node->simpleType()));
                        tm->setWriteInstruction(getWI(node->simpleType()));
                        popAttributeBinding(node, "value", AttributeModel::OTHER,
                              ::xylon::xsdom::Occurrence::required());
                     }
                     else {
                        bindBaseType(QName::xsQName("anySimpleType"));
                     }
                  }

                  {
                     if (node->base()) {
                        bindWI(node, WriteBaseType::create());
                     }
                     else if (node->simpleType()) {
                        bindWI(node, WriteAttribute::create(getAttributeBinding(node)));
                     }
                     else {
                        bindWI(node, WriteBaseType::create());
                     }
                  }
                  {

                     if (node->base()) {
                        bindPI(node, ParseInstance::create(resolveTypeRef(node->base().name())));
                     }
                     else if (node->simpleType()) {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        ::std::vector< Reference< Instruction> > pi;
                        pi.push_back(CreateAndParse::create(attr->type()));
                        pi.push_back(BindAttribute::create(attr));
                        bindPI(node, pi);
                     }
                     else {
                        bindPI(node, ParseInstance::create(_anySimpleType));
                     }

                  }
               }

               void visitList(const Reference< ::xylon::xsdom::List>& node)
               {
#if USE_LIST_TYPE_MODEL == 1
                  {

                     Reference< DefaultListTypeModel> model = _typeStack.top().target();

                     ModelRef< TypeModel> listType;

                     if (node->itemType()) {
                        // get the type ref for the item and create list model
                        listType = resolveTypeRef(node->itemType().name());
                     }
                     else if (node->type()) {
                        pushTypeModel(node->type(), "ItemType");
                        visitNode(node->type());
                        listType = popType(node->type());
                        assert(listType->parseInstruction());
                     }
                     model->setType(listType);
                     {
                        ::std::vector< Reference< Instruction> > wi;
                        assert("Not implemented"==0);
                        bindWI(node,wi);
                     }
                  }
#else
                  {
                     bindBaseType(QName::xsQName("anySimpleType"));

                     if (node->itemType()) {
                        // get the type ref for the item and create list model
                        ModelRef< TypeModel> listType = resolveTypeRef(node->itemType().name());
                        createAttributeBinding(node, "items", listType, AttributeModel::OTHER,
                              ::xylon::xsdom::Occurrence::unbounded());
                     }
                     else if (node->type()) {
                        pushTypeModel(node->type(), "ItemType");
                        visitNode(node->type());

                        // FIXME: this is quite hackish (in other places as well)
                        Pointer< DefaultTypeModel> tm = topType< DefaultTypeModel> ();
                        assert(tm && "Not a default model on the stack; how's that possible");
                        tm->setParseInstruction(getPI(node->type()));
                        tm->setWriteInstruction(getWI(node->type()));
                        popAttributeBinding(node, "items", AttributeModel::OTHER,
                              ::xylon::xsdom::Occurrence::unbounded());
                     }
                     else {
                        assert(false);
                     }

                     {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        bindWI(node, WriteAttribute::create(attr));
                     }

                     {
                        Reference< AttributeModel> attr = getAttributeBinding(node);
                        ::std::vector< Reference< Instruction> > pi, body;
                        body.push_back(CreateAndParse::create(attr->type()));
                        body.push_back(BindAttribute::create(attr));

                        pi.push_back(ActivateValue::create());
                        pi.push_back(IterateList::create(Sequence::create(body)));
                        bindPI(node, pi);
                     }
                  }
#endif
               }

               void visitUnion(const Reference< ::xylon::xsdom::Union>& node)
               {
#if USE_UNION_TYPE_MODEL
                  {
                     Reference< DefaultUnionTypeModel> model = _typeStack.top().target();

                     vector< ModelRef< TypeModel> > types;
                     for (size_t i = 0; i != node->memberTypes().size(); ++i) {

                        // bind an attribute
                        QName qname = node->memberTypes()[i].name();
                        ModelRef< TypeModel> xref = resolveTypeRef(qname);

                        // need to create attributes here
                        model->addType(xref);
                     }

                     for (size_t i = 0; i != node->types().size(); ++i) {
                        ostringstream out;
                        out << "T" << i;
                        pushTypeModel(node->types()[i], out.str());
                        visitNode(node->types()[i]);
                        model->addType(popType());
                     }

                  }
                  {
                     ::std::vector< Reference< Instruction> > wi;
                     assert("Not implemented"==0);
                     bindWI(node,wi);
                  }
#else
                  {
                     bindBaseType(QName::xsQName("anySimpleType"));
                     vector< ModelRef< TypeModel> > types;
                     for (size_t i = 0; i != node->memberTypes().size(); ++i) {
                        // bind an attribute
                        QName qname = node->memberTypes()[i].name();
                        ModelRef< TypeModel> xref = resolveTypeRef(qname);
                        createAttributeBinding(node, qname.name(), xref, AttributeModel::OTHER,
                              ::xylon::xsdom::Occurrence::optional());
                     }

                     for (size_t i = 0; i != node->types().size(); ++i) {
                        ostringstream out;
                        // FIXME: we should base the name of the local type on the ultimate basetype of the simple type
                        out << "ItemType" << i;
                        pushTypeModel(node->types()[i], out.str());
                        visitNode(node->types()[i]);
                        Pointer< DefaultTypeModel> tm = topType< DefaultTypeModel> ();
                        assert(tm && "Not a default model on the stack; how's that possible");
                        tm->setParseInstruction(getPI(node->types()[i]));
                        tm->setWriteInstruction(getWI(node->types()[i]));
                        // FIXME: why is there an I?
                        popAttributeBinding(node, "I" + out.str(), AttributeModel::OTHER,
                              ::xylon::xsdom::Occurrence::optional());
                     }
                  }

                  {
                     vector< Reference< AttributeModel> > attrs = getAttributeBindings(node);
                     ::std::vector< Reference< Instruction> > wi;
                     for (size_t i = 0, sz = attrs.size(); i != sz; ++i) {
                        wi.push_back(WriteAttribute::create(attrs[i]));
                     }
                     bindWI(node, wi);
                  }

                  vector< Reference< AttributeModel> > attrs = getAttributeBindings(node);
                  Reference< Instruction> pi = Error::create();
                  for (size_t i = 0, sz = attrs.size(); i != sz; ++i) {
                     Reference< AttributeModel> attr = attrs[i];
                     pi = ParseNewInstance::create(attr->type(), BindAttribute::create(attr), pi);
                  }
                  bindPI(node, Sequence::create(ActivateValue::create(), pi));

#endif
               }

               void visitEnumeration(const Reference< ::xylon::xsdom::Enumeration>&)
               {
               }
               void visitFractionDigits(const Reference< ::xylon::xsdom::FractionDigits>&)
               {
               }
               void visitLength(const Reference< ::xylon::xsdom::Length>&)
               {
               }
               void visitMaxExclusive(const Reference< ::xylon::xsdom::MaxExclusive>&)
               {
               }
               void visitMaxInclusive(const Reference< ::xylon::xsdom::MaxInclusive>&)
               {
               }
               void visitMaxLength(const Reference< ::xylon::xsdom::MaxLength>&)
               {
               }
               void visitMinExclusive(const Reference< ::xylon::xsdom::MinExclusive>&)
               {
               }
               void visitMinInclusive(const Reference< ::xylon::xsdom::MinInclusive>&)
               {
               }
               void visitMinLength(const Reference< ::xylon::xsdom::MinLength>&)
               {
               }
               void visitPattern(const Reference< ::xylon::xsdom::Pattern>&)
               {
               }
               void visitTotalDigits(const Reference< ::xylon::xsdom::TotalDigits>&)
               {
               }
               void visitWhiteSpace(const Reference< ::xylon::xsdom::WhiteSpace>&)
               {
               }

               /** This stack of boolean, indictating if the current type is a simple type or a complex type */
               ::std::stack< Reference< ::xylon::xsdom::Type> > _domTypeStack;

               ModelRef< TypeModel> _anyElement;
               ModelRef< TypeModel> _anySimpleType;
               NodeBindings _attributeBindings;

               /** The bind instructions for each node */
               BindInstructions _parseInstructions;

               /** The bind instructions for each node */
               BindInstructions _writeInstructions;

               /** The allowed attributes for complex types and elements */
               const ::xylon::xsdom::AllowedAttributes& _attributes;

               /** The first set */
               const ::xylon::xsdom::FirstSets& _firstSets;

               /** The schema */
               const ::xylon::XsdSchemaModel& _schema;

               stack< TypeModelInfo> _typeStack;
               stack< ::xylon::xsdom::Occurrence> _occurs;
         };
      }

      DefaultBindingProvider::DefaultBindingProvider(String tns)
      throws() :
      _targetNamespace(tns)
      {
         {
            ScopeName sn(tns, ScopeName::ATTRIBUTE);
            _namespaces[sn] = new DefaultScopeModel(sn);
         }
         {
            ScopeName sn(tns, ScopeName::ATTRIBUTE_GROUP);
            _namespaces[sn] = new DefaultScopeModel(sn);
         }
         {
            ScopeName sn(tns, ScopeName::ELEMENT);
            _namespaces[sn] = new DefaultScopeModel(sn);
         }
         {
            ScopeName sn(tns, ScopeName::GROUP);
            _namespaces[sn] = new DefaultScopeModel(sn);
         }
         {
            ScopeName sn(tns, ScopeName::KEY);
            _namespaces[sn] = new DefaultScopeModel(sn);
         }
         {
            ScopeName sn(tns, ScopeName::TYPE);
            _namespaces[sn] = new DefaultScopeModel(sn);
         }

      }

      DefaultBindingProvider::~DefaultBindingProvider()
      throws()
      {
      }

      Pointer< TypeModel> DefaultBindingProvider::bindElement(const ::xylon::XsdSchemaModel& schema, const Reference<
            ::xylon::xsdom::Element>& node) const
      throws (::std::exception)
      {
         const QName qname = node->qname();
         if (qname.namespaceURI() != _targetNamespace) {
            return Pointer< TypeModel> ();
         }

         Reference< ScopeModel> tns = _namespaces.find(ScopeName(qname.namespaceURI(), ScopeName::ELEMENT))->second;
         GlobalName gname(qname, GlobalName::ELEMENT);
         Pointer< DefaultTypeModel> tm;
         Pointer< DefaultElementTypeModel> elmtModel;
         bool complexAny = false;

         if (node->substitutionGroup() || node->type()) {
            LogEntry(logger()).info() << "DefaultElementTypeModel for node : " << node->qname() << doLog;
            elmtModel = new DefaultElementTypeModel(qname.name(), gname, tns);
            tm = elmtModel;
            tm->setComplex(true);
            tm->setAbstract(node->isAbstract());
         }
         else if (!node->elementType() || !isComplexAnyDerivation(node->elementType())) {
            tm = new DefaultTypeModel(qname.name(), gname, tns);
            //FIXME: not sure that this is really true
            tm->setComplex(true);
            tm->setAbstract(node->isAbstract());
         }
         else {
            complexAny = true;
         }
         TypeModelInfo info;
         info._name = qname.name();
         info._model = tm;
         info._scope = tns;
         info._globalName = gname;

         LogEntry(logger()).debugging() << __func__ << ": bound " << qname.name() << doLog;

         Binder b(schema, createAttributeSet(_targetNamespace, _attributes, schema.schema()), createFirstSets(
               _targetNamespace, _firsts, schema.schema()), node, info);

         bool bindBaseType = true;
         vector< Reference< Instruction> > wi;

{      if (node->substitutionGroup()) {
         LogEntry(logger()).debugging() << "Element : " << node->qname() << " is in substitution group of "
         << node->substitutionGroup().name() << doLog;
         bindBaseType = false;
         b.bindBaseElement(node->substitutionGroup().name());

         // FIXME: check the type here of the element here;
         if (node->type()) {
            elmtModel->setContentModelType(schema.getTypeModel(node->type().name()));
         }
         else if (node->elementType()) {
            b.pushTypeModel(node, "Local");
            b.visitNode(node->elementType());
            ModelRef< TypeModel> localType(b.popType(node->elementType()));
            elmtModel->setContentModelType(localType);
         }
         else {
            // inheriting the type of the base type
         }
         //FIXME: how to write the base type's attribute??
      }
      else if (node->type()) {
         if (elmtModel) {
            elmtModel->setContentModelType(schema.getTypeModel(node->type().name()));
         }
         Pointer< AttributeModel> attr = b.createAttributeBinding(node, "value", node->type().name(),
               AttributeModel::ELEMENT, ::xylon::xsdom::Occurrence::required());
         wi.push_back(WriteAttribute::create(attr));
      }
      else if (node->elementType()) {
         // FIXME: need a content model type nonetheless if this is a substitution group member
         if (complexAny) {
            b.visitNode(node->elementType());
            wi.push_back(b.getWI(node->elementType()));
         }
         else {
            b.pushTypeModel(node, "Local");
            b.visitNode(node->elementType());
            Pointer< AttributeModel> attr = b.popAttributeBinding(node, "value", AttributeModel::ELEMENT,
                  ::xylon::xsdom::Occurrence::required());
            b.getWI(node->elementType());
            wi.push_back(WriteAttribute::create(attr));
         }
      }
      else {
         // the element has no type at all;
         // FIXME: that's wrong, needs to be xs:anyType
      }
   }

   tm = b._typeStack.top()._model;
   tm->setAbstract(node->isAbstract());
   tm->setWriteInstruction(b.bindWI(node, wi));

   if (bindBaseType) {
      //            b.bindBaseElement(QName::xsQName("any"));
   }
   b._typeStack.pop();
   assert(b._typeStack.empty());

   {
      ::std::vector< Reference< Instruction> > pi;
      if (node->substitutionGroup()) {
         struct LocalInstruction : public AbstractInstruction
         {
            LocalInstruction(const ModelRef< TypeModel>& eModel, const ModelRef< TypeModel>& tModel,
                  bool simple) :
            _elementModel(eModel), _implType(tModel)
            {
               if (simple) {
                  _downElement = MoveCursor::createDown();
               }
            }
            ~LocalInstruction() throws()
            {
            }

            Pointer< Instruction> instruction() const throws()
            {
               if (_instruction) {
                  return _instruction;
               }

               Pointer< ElementTypeModel> rootType =
               _elementModel.target().tryDynamicCast< ElementTypeModel> ();
               if (rootType) {
                  ::std::vector< Reference< Instruction> > xpi;
                  LogEntry(logger()).info() << "RootType " << rootType->globalName() << doLog;
                  if (_implType) {
                     // the casting is delayed to ensure that the binding has been fully generated
                     xpi.push_back(rootType->createParseInstruction(_implType));
                  }
                  else {
                     xpi.push_back(rootType->createParseInstruction(rootType->contentModelType()));
                  }
                  if (_downElement) {
                     xpi.push_back(_downElement);
                  }
                  xpi.push_back(OnValidElement::create(Error::create(), MoveCursor::createUp()));
                  xpi.push_back(MoveCursor::createNext());
                  _instruction = Sequence::create(xpi);

               }
               else {
                  // the base type defines a local type model which means the substitution cannot
                  // add to the type and we simply extend the base type and invoke its parse function
                  _instruction = _elementModel->parseInstruction();
               }
               return _instruction;
            }

            ModelRef< TypeModel> _elementModel;
            ModelRef< TypeModel> _implType;
            Pointer< Instruction> _downElement;

            private:
            mutable Pointer< Instruction> _instruction;
         };

         bool simpleContent = false;
         switch (schema.schema()->getContentModel(node)) {
            case ::xylon::XsdSchema::NO_CONTENT:
            LogEntry(logger()).bug() << "3. No content, but have type " << schema.schema()->getElementType(
                  node) << doLog;
            throw ::std::logic_error("Should not be in this branch");
            break;
            case ::xylon::XsdSchema::SIMPLE_TYPE_CONTENT:
            case ::xylon::XsdSchema::SIMPLE_CONTENT:
            simpleContent = true;
            break;
            default:
            break;
         }

         // since the node uses a substitution group, can access the type() of the referenced element
         // because it must be a global type
         QName root = schema.schema()->getSubstitutionGroupRoot(qname);

         LogEntry(logger()).info() << "Substitution group of " << qname << " is " << root << doLog;

         ModelRef< TypeModel> type = schema.getElementModel(root);
         assert(type);
         pi.push_back(ActivateElement::create());

         Instruction* iPtr = new LocalInstruction(type, elmtModel->contentModelType(), simpleContent);
         pi.push_back(Reference< Instruction> (iPtr));

      }
      else if (node->type() || node->elementType()) {
         pi.push_back(ActivateElement::create());
         if (node->type()) {
            // validate the attributes first
            Pointer< Instruction> tmpPI(b.createValidateAttributesInstruction(*node->type().target()));
            if (tmpPI) {
               pi.push_back(tmpPI);
            }
            Reference< AttributeModel> attr = b.getAttributeBinding(node);
            pi .push_back(OnXsiType::create(attr->type()));
            pi.push_back(BindAttribute::create(attr));
         }
         else if (node->elementType()) {
            //FIXME: why is tm->parseInstruction() == NULL?????
            //               pi.push_back(tm->parseInstruction());
            Pointer< Instruction> tmpPI(b.createValidateAttributesInstruction(*node));
            if (tmpPI) {
               pi.push_back(tmpPI);
            }
            if (complexAny) {
               pi.push_back(b.getPI(node->elementType()));
            }
            else {
               Reference< AttributeModel> attr = b.getAttributeBinding(node);
               pi.push_back(NewInstance::create(attr->type()));
               pi.push_back(b.getPI(node->elementType()));
               pi.push_back(BindAttribute::create(attr));
            }
         }

         switch (schema.schema()->getContentModel(node)) {
            case ::xylon::XsdSchema::NO_CONTENT:
            LogEntry(logger()).bug() << "2. No content, but have type " << schema.schema()->getElementType(
                  node) << doLog;
            throw ::std::logic_error("Should not be in this branch");
            break;
            case ::xylon::XsdSchema::SIMPLE_TYPE_CONTENT:
            case ::xylon::XsdSchema::SIMPLE_CONTENT:
            pi.push_back(MoveCursor::createDown());
            break;
            default:
            break;
         }
         pi.push_back(OnValidElement::create(Error::create(), MoveCursor::createUp()));
         pi.push_back(MoveCursor::createNext());
      }
      else {
         // need to move to the next element
         Pointer< Instruction> tmpPI = b.createValidateAttributesInstruction(*node);
         if (tmpPI) {
            pi.push_back(tmpPI);
         }
         pi.push_back(MoveCursor::createNext());
      }

      assert(tm);
      tm->setParseInstruction(b.bindPI(node, OnElement::create(node->qname(true), Sequence::create(pi),
                        Error::create(), false)));

   }
   assert(tm->isGlobal());
   return tm;
}

Pointer< TypeModel> DefaultBindingProvider::bindComplexType(const ::xylon::XsdSchemaModel& schema, const Reference<
      ::xylon::xsdom::ComplexType>& node) const throws (::std::exception)
{
   QName qname = node->qname();
   if (qname.namespaceURI() != _targetNamespace) {
      return Pointer< TypeModel> ();
   }
   GlobalName gname(qname, GlobalName::TYPE);
   Reference< ScopeModel> tns = _namespaces.find(ScopeName(qname.namespaceURI(), ScopeName::TYPE))->second;
   Reference< DefaultTypeModel> tm(new DefaultTypeModel(qname.name(), gname, tns));
   tm->setComplex(true);
   tm->setAbstract(node->isAbstract());
   LogEntry(logger()).debugging() << __func__ << ": bound " << qname.name() << doLog;

   TypeModelInfo info;
   info._model = tm;
   info._name = qname.name();
   info._scope = tns;
   info._globalName = gname;

   Binder b(schema, createAttributeSet(_targetNamespace, _attributes, schema.schema()), createFirstSets(
               _targetNamespace, _firsts, schema.schema()), node, info);
   b._domTypeStack.push(node);

   Pointer< Instruction> attrPi, attrWi;
   {
      b.visitNode(node->contentType());
      if (node->enumerable()) {
         b.bindBaseType(QName::xsQName("anyType"));
         b.visitEnumerable(node->enumerable());
      }
      b.visitAttributes(node, attrPi, attrWi);
   }

   {
      if (node->contentType()) {
         tm->setParseInstruction(b.bindPI(node, b.getPI(node->contentType())));
         tm->setWriteInstruction(b.bindWI(node, b.getWI(node->contentType())));
      }
      else {
         ::std::vector< Reference< Instruction> > pi, wi;
         // do the attributes first
         if (attrPi) {
            pi.push_back(attrPi);
         }
         if (attrWi) {
            wi.push_back(attrWi);
         }
         pi.push_back(MoveCursor::createDown());
         if (node->enumerable()) {
            pi.push_back(b.getPI(node->enumerable()));
            wi.push_back(b.getWI(node->enumerable()));
         }
         tm->setParseInstruction(b.bindPI(node, pi));
         tm->setWriteInstruction(b.bindWI(node, wi));
      }
   }
   assert(tm->isGlobal());
   b._domTypeStack.pop();
   return tm;
}

Pointer< TypeModel> DefaultBindingProvider::bindSimpleType(const ::xylon::XsdSchemaModel& schema, const Reference<
      ::xylon::xsdom::SimpleType>& node) const
throws (::std::exception)
{
   QName qname = node->qname();
   if (qname.namespaceURI() != _targetNamespace) {
      return Pointer< TypeModel> ();
   }

   Reference< ScopeModel> tns = _namespaces.find(ScopeName(qname.namespaceURI(), ScopeName::TYPE))->second;
   GlobalName gname(qname, GlobalName::TYPE);

   Pointer< DefaultTypeModel> tm;
   if (node->getUnion()) {
#if USE_UNION_TYPE_MODEL
      tm = new DefaultUnionTypeModel(qname.name(), gname, tns);
#else
      tm = new DefaultTypeModel(qname.name(), gname, tns);
#endif
   }
   else if (node->list()) {
#if USE_LIST_TYPE_MODEL == 1
      tm = new DefaultListTypeModel(qname.name(), gname, tns);
#else
      tm = new DefaultTypeModel(qname.name(), gname, tns);
#endif
   }
   else {
      tm = new DefaultTypeModel(qname.name(), gname, tns);
   }
   tm->setComplex(false);
   LogEntry(logger()).debugging() << "Bound " << qname.name() << doLog;

   TypeModelInfo info;
   info._model = tm;
   info._name = qname.name();
   info._scope = tns;
   info._globalName = gname;

   Binder b(schema, createAttributeSet(_targetNamespace, _attributes, schema.schema()), createFirstSets(
         _targetNamespace, _firsts, schema.schema()), node, info);
   b._domTypeStack.push(node);
   {

      b.visitNode(node->restriction());
      b.visitNode(node->list());
      b.visitNode(node->getUnion());
   }

   {
      ::std::vector< Reference< Instruction> > wi;
      if (node->restriction()) {
         wi.push_back(b.bindWI(node, b.getWI(node->restriction())));
      }
      else if (node->list()) {
         wi.push_back(b.bindWI(node, b.getWI(node->list())));
      }
      else if (node->getUnion()) {
         wi.push_back(b.bindWI(node, b.getWI(node->getUnion())));
      }
      tm->setWriteInstruction(Sequence::create(wi));
   }

   {
      ::std::vector< Reference< Instruction> > pi;
      pi.push_back(ActivateValue::create());
      if (node->restriction()) {
         pi.push_back(b.bindPI(node, b.getPI(node->restriction())));
      }
      else if (node->list()) {
         pi.push_back(b.bindPI(node, b.getPI(node->list())));
      }
      else if (node->getUnion()) {
         pi.push_back(b.bindPI(node, b.getPI(node->getUnion())));
      }
      tm->setParseInstruction(Sequence::create(pi));
   }
   assert(tm->isGlobal());
   b._domTypeStack.pop();
   return tm;
}

Pointer< TypeModel> DefaultBindingProvider::bindGroup(const ::xylon::XsdSchemaModel& schema, const Reference<
      ::xylon::xsdom::Group>& node) const
throws (::std::exception)
{

   QName qname = node->qname();
   if (qname.namespaceURI() != _targetNamespace) {
      return Pointer< TypeModel> ();
   }
   GlobalName gname(qname, GlobalName::GROUP);
   Reference< ScopeModel> tns = _namespaces.find(ScopeName(qname.namespaceURI(), ScopeName::GROUP))->second;
   Reference< DefaultTypeModel> tm(new DefaultTypeModel(qname.name(), gname, tns));
   tm->setComplex(true);

   TypeModelInfo info;
   info._model = tm;
   info._name = qname.name();
   info._scope = tns;
   info._globalName = gname;
   Binder b(schema, createAttributeSet(_targetNamespace, _attributes, schema.schema()), createFirstSets(
         _targetNamespace, _firsts, schema.schema()), node, info);
   {
      b.visitEnumerable(node->enumerable());
   }

   {
      tm->setWriteInstruction(b.bindWI(node, b.getWI(node->enumerable())));
      tm->setParseInstruction(b.bindPI(node, b.getPI(node->enumerable())));
   }

   return tm;
}

Pointer< TypeModel> DefaultBindingProvider::bindAttribute(const ::xylon::XsdSchemaModel& schema, const Reference<
      ::xylon::xsdom::Attribute>& node) const
throws (::std::exception)
{
   QName qname = node->qname();
   if (qname.namespaceURI() != _targetNamespace) {
      return Pointer< TypeModel> ();
   }
   Reference< ScopeModel> tns = _namespaces.find(ScopeName(qname.namespaceURI(), ScopeName::ATTRIBUTE))->second;
   GlobalName gname(qname, GlobalName::ATTRIBUTE);
   Pointer< DefaultTypeModel> tm;

   if (!node->attributeType()) {
      tm = new DefaultTypeModel(qname.name(), gname, tns);
      tm->setComplex(true);
      assert(tm->isGlobal());
   }
   TypeModelInfo info;
   info._model = tm;
   info._name = qname.name();
   info._scope = tns;
   info._globalName = gname;
   LogEntry(logger()).debugging() << __func__ << ": bound " << qname.name() << doLog;

   Binder b(schema, createAttributeSet(_targetNamespace, _attributes, schema.schema()), createFirstSets(
         _targetNamespace, _firsts, schema.schema()), node, info);

   vector< Reference< Instruction> > wi;
   {
      QName attrName = node->qname(true);
      if (node->type()) {
         Pointer< AttributeModel> attr = b.createAttributeBinding(node, "value", node->type().name(),
               AttributeModel::ATTRIBUTE, ::xylon::xsdom::Occurrence::required());
         wi.push_back(WriteAttribute::create(attr, StartWriteObject::createAttribute(attrName),
                     EndWriteObject::createAttribute(attrName)));
      }
      else if (node->attributeType()) {
         wi.push_back(StartWriteObject::createAttribute(node->qname(true)));
         b.visitNode(node->attributeType());
         wi.push_back(b.getWI(node->attributeType()));
         wi.push_back(EndWriteObject::createAttribute(node->qname(true)));
      }
      else {
         Pointer< AttributeModel> attr = b.createAttributeBinding(node, node->qname().name(), QName::xsQName(
                     "anySimpleType"), AttributeModel::ATTRIBUTE, ::xylon::xsdom::Occurrence::required());
         wi.push_back(WriteAttribute::create(attr, StartWriteObject::createAttribute(attrName),
                     EndWriteObject::createAttribute(attrName)));
      }
   }

   tm = b._typeStack.top()._model;
   tm->setWriteInstruction(b.bindWI(node, wi));
   assert(tm->isGlobal());
   b._typeStack.pop();
   assert(b._typeStack.empty());

   {
      ::std::vector< Reference< Instruction> > pi;
      if (node->type()) {
         Reference< AttributeModel> attr = b.getAttributeBinding(node);
         pi.push_back(CreateAndParse::create(attr->type()));
         pi.push_back(BindAttribute::create(attr));
      }
      else if (node->attributeType()) {
         pi.push_back(b.getPI(node->attributeType()));
      }
      else {
         Reference< AttributeModel> attr = b.getAttributeBinding(node);
         pi.push_back(b.getPI(node->attributeType()));
      }
      tm->setParseInstruction(b.bindPI(node, pi));

   }

   return tm;

}

Pointer< TypeModel> DefaultBindingProvider::bindAttributeGroup(const ::xylon::XsdSchemaModel& schema, const Reference<
      ::xylon::xsdom::AttributeGroup>& node) const
throws (::std::exception)
{
   QName qname = node->qname();
   if (qname.namespaceURI() != _targetNamespace) {
      return Pointer< TypeModel> ();
   }
   Reference< ScopeModel> tns = _namespaces.find(ScopeName(qname.namespaceURI(), ScopeName::ATTRIBUTE_GROUP))->second;
   GlobalName gname(qname, GlobalName::ATTRIBUTE_GROUP);
   Reference< DefaultTypeModel> tm = new DefaultTypeModel(qname.name(), gname, tns);
   tm->setComplex(true);

   TypeModelInfo info;
   info._model = tm;
   info._name = qname.name();
   info._scope = tns;
   info._globalName = gname;

   Binder b(schema, createAttributeSet(_targetNamespace, _attributes, schema.schema()), createFirstSets(
         _targetNamespace, _firsts, schema.schema()), node, info);
   Pointer< Instruction> attrPi, attrWi;
   b.visitAttributes(node, attrPi, attrWi);
   tm->setParseInstruction(attrPi);
   tm->setWriteInstruction(attrWi);
   return tm;
}

}

}
