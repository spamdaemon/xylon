#ifndef _XYLON_BINDING_DEFAULTTYPEBINDING_H
#define _XYLON_BINDING_DEFAULTTYPEBINDING_H

#ifndef _XYLON_MODEL_MODELS_H
#include <xylon/model/models.h>
#endif

#ifndef _XYLON_MODEL_H
#include <xylon/model/model.h>
#endif

namespace xylon {

   namespace binding {
      /**
       * The TypeBinding maps XML schema artifacts, such as complex types,
       * elements, groups, etc. into types in a target programming language.
       */
      class DefaultTypeModel : public virtual ::xylon::model::TypeModel
      {
            DefaultTypeModel&operator=(const DefaultTypeModel&);
            DefaultTypeModel(const DefaultTypeModel&) throws();

            /**
             * Create a new typebinding for the specified name and scope.
             * @param name the name of the bound within the specified scope
             * @param gn the global name
             * @param scope the scope in which this model is defined
             * @param pi an optional parse instruction
             */
         public:
            DefaultTypeModel(String name, const GlobalName& gn,
                  const ::timber::Reference< ::xylon::model::ScopeModel>& scope, const ::timber::Pointer<
                        ::xylon::model::Instruction>& pi = ::timber::Pointer< ::xylon::model::Instruction>()) throws();

            /**
             * Create a new default type model.
             * @param name the the type name
             * @param scope the enclosing type
             * @param pi an optional instruction
             */
         public:
            DefaultTypeModel(String name, const ::timber::Reference< ::xylon::model::ScopeModel>& scope,
                  const ::timber::Pointer< ::xylon::model::Instruction>& pi = ::timber::Pointer<
                        ::xylon::model::Instruction>()) throws();

            /** Destructor */
         public:
            ~DefaultTypeModel() throws();

            void clear() throws();

            GlobalName globalName() const throws();

            /**
             * The optional base class for this type. The base class
             * is must be a known type.
             */
         public:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> baseType() const throws();

            /**
             * Set the base type
             * @param xref the base type
             */
         public:
            void setBaseType(::xylon::model::ModelRef< ::xylon::model::TypeModel> bt) throws();

            /**
             * The name of this type within its enclosing scope.
             * @return the name of this type within its enclosing scope
             */
         public:
            String name() const throws();

            /** The type attributes */
         public:
            ::std::vector< ::timber::Reference< ::xylon::model::AttributeModel> > attributes() const throws();
            ::std::vector< ::timber::Reference< ::xylon::model::TypeModel> > localTypes() const throws();

            virtual void addLocalTypeModel(const ::timber::Reference< ::xylon::model::TypeModel>& lc) throws();
            void addAttributeModel(const ::timber::Reference< ::xylon::model::AttributeModel>& attr) throws();

            /**
             * Set the parse instruction.
             * @param pi a reference to an instruction
             */
         public:
            void setParseInstruction(const ::timber::Pointer< ::xylon::model::Instruction>& pi) throws();
            ::timber::Pointer< ::xylon::model::Instruction> parseInstruction() const throws();

            /**
             * Set the write instruction.
             * @param pi a reference to an instruction
             */
         public:
            void setWriteInstruction(const ::timber::Pointer< ::xylon::model::Instruction>& pi) throws();
            ::timber::Pointer< ::xylon::model::Instruction> writeInstruction() const throws();

            /**
             * Make this a complex type.
             * @param c true if this should be a complex type, false otherwise
             */
         public:
            void setComplex(bool c) throws();

            /**
             * True if this is a complex type.
             * @return true if this is a complex type
             */
         public:
            bool isComplex() const throws();

            /**
             * Make this an abstract type.
             * @param c true if this should be an abstract type, false otherwise
             */
         public:
            void setAbstract(bool c) throws();

            /**
             * True if this is an abstract type.
             * @return true if this is an abstract type
             */
         public:
            bool isAbstract() const throws();

            /**
             * Get the scope in which this type is declared.
             * @return a scope
             */
         public:
            ::timber::Reference< ScopeModel> scope() const throws();

            /** The name of this scope */
         private:
            ::xylon::model::ScopeName scopeName() const throws();

            /** The type name */
         private:
            const String _name;

            /** The base type (must be a  type) */
         private:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> _base;

            /** The attributes of this type */
         private:
            ::std::vector< ::timber::Reference< ::xylon::model::AttributeModel> > _attributes;

            /** The locally defined types */
         private:
            ::std::vector< ::timber::Reference< ::xylon::model::TypeModel> > _localTypes;

            /** The global name (if defined) */
         private:
            const GlobalName _globalName;

            /** The scope in which this type is defined */
         private:
            ::timber::Reference< ::xylon::model::ScopeModel> _scope;

            /** The parser instruction */
         private:
            ::timber::Pointer< ::xylon::model::Instruction> _parse;

            /** The parser instruction */
         private:
            ::timber::Pointer< ::xylon::model::Instruction> _write;

            /** A scope name */
         private:
            ::xylon::model::ScopeName _scopeName;

            /** True if this is a complex type */
         private:
            bool _complex;

            /** True if this is an abstract type, false otherwise */
         private:
            bool _abstract;

      };
   }
}
#endif
