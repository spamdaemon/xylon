#include <xylon/binding/DefaultUnionTypeModel.h>
#include <xylon/model/AttributeModel.h>
#include <xylon/model/ScopeModel.h>

using namespace ::timber;
using namespace ::xylon::model;

namespace xylon {
   namespace binding {
      DefaultUnionTypeModel::DefaultUnionTypeModel(String n, const GlobalName& gn, const Reference< ScopeModel>& sc,
            const Pointer< Instruction>& pi) throws() :
         DefaultTypeModel(n, gn, sc, pi)
      {
      }

      DefaultUnionTypeModel::DefaultUnionTypeModel(String n, const Reference< ScopeModel>& sc, const Pointer<
            Instruction>& pi) throws() :
         DefaultTypeModel(n, GlobalName(), sc, pi)
      {
      }

      DefaultUnionTypeModel::~DefaultUnionTypeModel() throws()
      {
      }

      void DefaultUnionTypeModel::addType(const ModelRef< TypeModel>& t) throws()
      {
         _types.push_back(t);
      }

      ::std::vector< ModelRef< TypeModel> > DefaultUnionTypeModel::types() const throws()
      {
         return _types;
      }
      bool DefaultUnionTypeModel::isComplex() const throws()
      {
         return false;
      }
   }
}
