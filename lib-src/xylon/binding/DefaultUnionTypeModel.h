#ifndef _XYLON_BINDING_DEFAULTUNIONTYPEBINDING_H
#define _XYLON_BINDING_DEFAULTUNIONTYPEBINDING_H

#ifndef _XYLON_MODEL_MODELS_H
#include <xylon/model/models.h>
#endif

#ifndef _XYLON_MODEL_H
#include <xylon/model/model.h>
#endif

#ifndef _XYLON_BINDING_DEFAULTTYPEMODEL_H
#include <xylon/binding/DefaultTypeModel.h>
#endif

#ifndef _XYLON_MODEL_UNIONTYPEMODEL_H
#include <xylon/model/UnionTypeModel.h>
#endif

namespace xylon {

   namespace binding {
      /**
       * The TypeBinding maps XML schema artifacts, such as complex types,
       * elements, groups, etc. into types in a target programming language.
       */
      class DefaultUnionTypeModel : public virtual DefaultTypeModel, public virtual ::xylon::model::UnionTypeModel
      {
            DefaultUnionTypeModel&operator=(const DefaultUnionTypeModel&);
            DefaultUnionTypeModel(const DefaultUnionTypeModel&) throws();

            /**
             * Create a new typebinding for the specified name and scope.
             * @param name the name of the bound within the specified scope
             * @param gn the global name
             * @param scope the scope in which this model is defined
             * @param pi an optional parse instruction
             */
         public:
            DefaultUnionTypeModel(String name, const GlobalName& gn, const ::timber::Reference<
                  ::xylon::model::ScopeModel>& scope, const ::timber::Pointer< ::xylon::model::Instruction>& pi =
                  ::timber::Pointer< ::xylon::model::Instruction>()) throws();

            /**
             * Create a new default type model.
             * @param name the the type name
             * @param scope the enclosing type
             * @param pi an optional instruction
             */
         public:
            DefaultUnionTypeModel(String name, const ::timber::Reference< ::xylon::model::ScopeModel>& scope,
                  const ::timber::Pointer< ::xylon::model::Instruction>& pi = ::timber::Pointer<
                        ::xylon::model::Instruction>()) throws();

            /** Destructor */
         public:
            ~DefaultUnionTypeModel() throws();

         public:
            ::std::vector< ::xylon::model::ModelRef< ::xylon::model::TypeModel> > types() const throws();

            /**
             * Union type model is not a complex type.
             * @return false
             */
            bool isComplex() const throws();

            /**
             * Add the type for this list.
             * @param t
             */
         public:
            void addType(const ::xylon::model::ModelRef< ::xylon::model::TypeModel>& t) throws();

         private:
            ::std::vector< ::xylon::model::ModelRef< ::xylon::model::TypeModel> > _types;
      };
   }
}
#endif
