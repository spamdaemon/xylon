#ifndef _XYLON_CODE_CPP_PARSERGENERATOR_H
#define _XYLON_CODE_CPP_PARSERGENERATOR_H

#ifndef _XYLON_H
#include <xylon/xylon.h>
#endif

#include <iosfwd>
#include <vector>
#include <set>
#include <map>

namespace xylon {
   namespace model {
      class TypeModel;
   }
   namespace code {
      namespace cpp {
         class BindingManager;

         /**
          * FIXME:
          */
         class ParserGenerator
         {
               /**
                * The parser specification.
                */
            public:
               struct Specification
               {
                     /** The output directory */
                     ::std::string destination;

                     /** The fully namespace qualified name of the parser */
                     ::std::string fullName;

                     /** The target namespaces supported by this parser (may contain a null pointer) */
                     ::std::set< ::xylon::String> targetNamespaces;

                     /** A header guard (can be empty) */
                     ::std::string headerGuard;

                     /** The file for the header (including any directories) */
                     ::std::string headerFilename;

                     /** The file for the implementation file (including any directories) */
                     ::std::string implFilename;

                     /** The filenames containing the schemas needed for parsing */
                     ::std::set< ::std::string> schemaFilenames;
               };

               /** Default constructor */
            protected:
               ParserGenerator() throws();

               /** Destructor */
            public:
               virtual ~ParserGenerator() throws() = 0;

               /**
                * Create a new parser generator.
                * @return a parser generator
                */
            public:
               static ::std::unique_ptr< ParserGenerator> create() throws();
               /**
                * Generate the parser declarations. This emits the header of a parser.
                * @param mgr a binding manager
                * @param spec a specification
                * @param models the type models to be generated
                */
            public:
               virtual void generateDeclaration(const BindingManager& mgr, const Specification& spec,
                     const ::std::vector< ::timber::Reference< ::xylon::model::TypeModel> >& models) = 0;

               /**
                * Generate a parser definition for the specified types.
                * @param mgr a binding manager
                * @param spec a specification
                * @param models the type models to be generated
                */
            public:
               virtual void generateDefinition(const BindingManager& mgr, const Specification& spec,
                     const ::std::vector< ::timber::Reference< ::xylon::model::TypeModel> >& models) = 0;

         };
      }
   }
}
#endif

