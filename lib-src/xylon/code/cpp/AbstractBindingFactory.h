#ifndef _XYLON_CODE_CPP_ABSTRACTBINDINGFACTORY_H
#define _XYLON_CODE_CPP_ABSTRACTBINDINGFACTORY_H

#ifndef _XYLON_CODE_CPP_BINDINGFACTORY_H
#include <xylon/code/cpp/BindingFactory.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {

         /**
          * An abstract implementation of a binding factory.
          */
         class AbstractBindingFactory : public BindingFactory
         {
               AbstractBindingFactory(const AbstractBindingFactory&);
               AbstractBindingFactory&operator=(const AbstractBindingFactory&);

               /** Default constructor */
            protected:
               AbstractBindingFactory() throws();

               /** Destructor */
            public:
               ~AbstractBindingFactory() throws() =0;

               /**
                * Create a namespace binding for the given scope model
                * @param scope a scope model
                * @return a namespace binding or null if not known
                */
            public:
               ::timber::Pointer< Namespace> createNamespace(const ::timber::Reference<
                     ::xylon::model::NamespaceModel>& model) throws();

               /**
                * Create a binding for the specified model type. It is possible that this factory
                * cannot create a binding for the specified model. In such cases, null is returned
                * and another factory may be used to create a binding.
                * @param model a model type
                * @param ns a namespace binding
                * @return a Type or null
                */
            public:
               ::timber::Pointer< Type> createType(const ::timber::Reference<
                     ::xylon::model::TypeModel>& model, const ::timber::Reference< Namespace>& ns) throws();

               /**
                * Create a local binding.
                * @param model a model type
                * @param enclosing the binding of the enclosing type
                * @return a Type or null
                */
            public:
               ::timber::Pointer< Type>
               createLocalType(const ::timber::Reference< ::xylon::model::TypeModel>& model,
                     const ::timber::Reference< Type>& enclosing) throws();

               /**
                * Create a binding for an attribute.
                * @param model a model
                * @param type the bound type of the attribute's typemodel
                * @param enclosing the binding of the enclosing type
                * @return a Type or null
                */
            public:
               ::timber::Pointer< Attribute>
               createAttribute(const ::timber::Reference< ::xylon::model::AttributeModel>& model,
                     const ::timber::Reference< Type>& type,
                     const ::timber::Reference< Type>& enclosing) throws();
         };

      }

   }

}

#endif
