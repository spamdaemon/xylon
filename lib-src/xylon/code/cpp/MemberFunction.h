#ifndef _XYLON_CODE_CPP_MEMBERFUNCTION_H
#define _XYLON_CODE_CPP_MEMBERFUNCTION_H

#ifndef _XYLON_CODE_CPP_FUNCTION_H
#include <xylon/code/cpp/Function.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {

         /**
          * This method is a baseclass for the functions that are members of a class.<br/>
          * Methods exist to emit a declaration and the function definition, but it is up to the
          * specializing classes to define methods to emit calls to the function.<p>
          * Technically, functions introduces a scope, but a decision was made at this time not to
          * extend the Scope and instead extend only the CppBinding interface.<br/> Furthermore,
          * no attempts have been made to reference parameters and return types in the function
          * as that would necessitate a type model that would need to be able to express arbitrary C++
          * type constructs.
          */
         class MemberFunction : public virtual Function
         {
               MemberFunction(const MemberFunction&);
               MemberFunction&operator=(const MemberFunction&);

               /** Default constructor */
            public:
               MemberFunction() throws();

               /** Destructor */
            public:
               virtual ~MemberFunction() throws();


         };

      }

   }

}

#endif
