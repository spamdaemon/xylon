#include <xylon/code/cpp/cpp.h>

using namespace ::std;

namespace xylon {
   namespace code {
      namespace cpp {

         vector< string> splitFullname(const string& ns) throws()
         {
            vector< string> res;
            if (ns.empty()) {
               return res;
            }

            size_t i = ns.find("::");
            size_t start = 0;

            while (start < ns.length()) {
               if (i == string::npos) {
                  res.push_back(ns.substr(start, string::npos));
                  break;
               }
               else if (i > start) {
                  res.push_back(ns.substr(start, i - start));
               }

               start = i + 2;
               i = ns.find("::", start);
            }

            return res;
         }
      }
   }
}
