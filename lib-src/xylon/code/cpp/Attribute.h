#ifndef _XYLON_CODE_CPP_ATTRIBUTE_H
#define _XYLON_CODE_CPP_ATTRIBUTE_H

#ifndef _XYLON_CODE_CPP_CPPBINDING_H
#include <xylon/code/cpp/CppBinding.h>
#endif

namespace xylon {
   namespace model {
      class AttributeModel;
   }
   namespace code {
      namespace cpp {
         class Type;

         class Attribute : public virtual CppBinding
         {
               Attribute(const Attribute&);
               Attribute&operator=(const Attribute&);

               /**
                * Default constructor.
                */
            protected:
               Attribute() throws();

               /** Destructor */
            public:
               ~Attribute() throws();

               /**
                * Get the name for this attribute. The returned name must not be a keyword.
                * @return a proper name for this attribute.
                */
            public:
               virtual ::std::string name() const throws() = 0;

               /**
                * Get the attribute model.
                * @return the attribute model
                */
            public:
               virtual ::timber::Reference< ::xylon::model::AttributeModel> model() const throws() = 0;

               /**
                * Get attribute's type.
                * @return the attribute type
                */
            public:
               virtual ::timber::Reference< Type> type() const throws() = 0;

               /**
                * Get attribute's scope.
                * @return the attribute's scope.
                */
            public:
               virtual ::timber::Reference< Type> scope() const throws() = 0;

               /**
                * Generate the method declarations for the attribute. This is used to generate
                * accessors and settors.
                * @param out an output stream
                */
            public:
               virtual void genMethodDeclarations(::std::ostream& out) const = 0;

               /**
                * Generate the member variable definition for this attribute. The appropriate visibility
                * value must be generated and cannot be assumed to be <em>private</em>.
                * @param out an output stream
                */
            public:
               virtual void genVariableDefinition(::std::ostream& out) const = 0;

               /**
                * Generate the method definitions.
                * @param out an output stream
                */
            public:
               virtual void genMethodDefinitions(::std::ostream& out) const = 0;

               /**
                * Generate a default initializer for use in an initializer list.
                * @param out an output stream
                */
            public:
               virtual void genDefaultInitializer(::std::ostream& out) const =0;

               /**
                * Generate the destruction expression statement. If the attribute's type
                * supports auto-destruction, then this may not write anything to the output stream.
                * @param out an output stream
                */
               virtual void genDestructor(::std::ostream& out) const =0;

               /**
                * Generate a copy initializer for use in an initializer list. The name of the
                * variable is given.
                * @param out an output stream
                * @param expr a C++ expression to reference of the enclosing type
                */
            public:
               virtual void genCopyInitializer(::std::ostream& out, const ::std::string& expr) const = 0;

               /**
                * Generate the copy constructor body for this attribute.
                * @param out an output stream
                * @param expr a C++ expression to reference of the enclosing type
                */
            public:
               virtual void genCopyConstructor(::std::ostream& out, const ::std::string& expr) const =0;

               /**
                * Generate a copy statement of this attribute from one object to another object. The statement
                * must be terminated by a @code ; @endcode (semi-colon) character.
                * @param out an output stream
                * @param src the source object variable name (of the enclosing type)
                * @param dest the destination object variable name (of the enclosing type)
                */
            public:
               virtual void
               genCopyStatement(::std::ostream& out, const ::std::string& src, const ::std::string& dest) const = 0;

               /**
                * Generate an expression to access this attribute given an object. This method is
                * expected to generate an expression to obtain the value of the attribute, assuming
                * that the expression will be preceded by @code . @endcode or @code -> @endcode .
                * @return an expression to access the specified object
                */
            public:
               virtual ::std::string genGetter() const throws() = 0;

               /**
                * Generate a set expression.
                * @param newValueExpr an expression for the new value
                * @return a string for an expression will set this attribute
                */
            public:
               virtual ::std::string genSetter(const ::std::string& newValueExpr) const throws() = 0;

               /**
                * Generate an enumeration statement. The enumeration state will loops over an instance
                * of this this attribute and invokes the specified object with the value of the attribute.
                * @param out an output stream
                * @param obj the enclosing object
                * @param cb the callback expression that is invoked.
                */
            public:
               virtual void genEnumerationStatement(::std::ostream& out, const ::std::string& obj,
                     const ::std::string& cb) const = 0;

               /**
                * Generate a call to bind the specified value to this attribute which is contained
                * within the enclosed object. The object is an expression resulting a references to the
                * attribute's enclosing type. The value is an expression resulting in a reference to an
                * unique_ptr with the type compatible with the attribute's type.
                * @param out an output stream
                * @param obj the enclosing object
                * @param value the value for this attribute.
                */
            public:
               virtual void genBindingStatement(::std::ostream& out, const ::std::string& obj,
                     const ::std::string& value) const = 0;
         };

      }

   }

}

#endif
