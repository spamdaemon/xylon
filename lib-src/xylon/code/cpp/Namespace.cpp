#include <xylon/code/cpp/Namespace.h>
#include <xylon/code/cpp/Identifier.h>
#include <xylon/code/cpp/cpp.h>

#include <ostream>
#include <vector>

using namespace ::std;
using namespace ::timber;

namespace xylon {
   namespace code {
      namespace cpp {
         Namespace::Namespace(const string& str) throws()
         {
             for (const ::std::string& s : splitFullname(str)) {
        	     _namespace+="::";
        		 _namespace+=Identifier::create(s);
        	 }
         }

         Namespace::~Namespace() throws()
         {
         }

         Reference< Namespace> Namespace::create(const string& str) throws (exception)
         {
            return new Namespace(str);
         }
         string Namespace::getNamespace() const throws()
         {
            return _namespace;
         }

         void Namespace::open(ostream& out) const
         {
            vector< string> vns = splitFullname(_namespace);
            for (size_t i = 0; i < vns.size(); ++i) {
               out << "namespace " << vns[i] << " {" << endl;
            }
         }

         void Namespace::close(ostream& out) const
         {
            vector< string> vns = splitFullname(_namespace);
            for (size_t i = 0; i < vns.size(); ++i) {
               out << "}" << endl;
            }
         }

         void Namespace::print(ostream& out) const
         {
            out << _namespace;
         }
      }

   }

}
