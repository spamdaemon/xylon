#ifndef _XYLON_CODE_CPP_BINDINGFACTORY_H
#define _XYLON_CODE_CPP_BINDINGFACTORY_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <vector>

namespace xylon {
   namespace model {
      class TypeModel;
      class NamespaceModel;
      class AttributeModel;
   }

   namespace code {
      namespace cpp {
         class Type;
         class Namespace;
         class Attribute;

         /**
          * The BindingFactory is the basetype for creating bindings for individual type objects.
          */
         class BindingFactory
         {
               BindingFactory(const BindingFactory&);
               BindingFactory&operator=(const BindingFactory&);

               /** Default constructor */
            protected:
               BindingFactory() throws();

               /** Destructor */
            public:
               virtual ~BindingFactory() throws() = 0;

               /**
                * Get includes needed for this binding factory. These includes are included in the
                * header of the schema.
                * @return a list of files in quotes or angle-brackets which can be used with a C++ #include directive
                */
            public:
               virtual ::std::vector< ::std::string> getIncludes() const throws() = 0;

               /**
                * Create a namespace binding for the given scope model
                * @param scope a scope model
                * @return a namespace binding or null if not known
                */
            public:
               virtual ::timber::Pointer< Namespace> createNamespace(const ::timber::Reference<
                     ::xylon::model::NamespaceModel>& model) throws() = 0;

               /**
                * Create a binding for the specified model type. It is possible that this factory
                * cannot create a binding for the specified model. In such cases, null is returned
                * and another factory may be used to create a binding.
                * @param model a model type
                * @param ns the namespace binding within which the type is defined
                * @return a Type or null
                */
            public:
               virtual ::timber::Pointer< Type> createType(
                     const ::timber::Reference< ::xylon::model::TypeModel>& model,
                     const ::timber::Reference< Namespace>& ns) throws() = 0;

               /**
                * Create a local binding.
                * @param model a model type
                * @param enclosing the binding of the enclosing type
                * @return a Type or null
                * @todo We should get rid of this one, because createType should be suffcient if we introduce the concept of scope.
                */
            public:
               virtual ::timber::Pointer< Type>
               createLocalType(const ::timber::Reference< ::xylon::model::TypeModel>& model, const ::timber::Reference<
                     Type>& enclosing) throws() = 0;

               /**
                * Create a binding for an attribute.
                * @param model a model
                * @param type the bound type of the attribute's typemodel
                * @param enclosing the binding of the enclosing type
                * @return a Type or null
                */
            public:
               virtual ::timber::Pointer< Attribute>
               createAttribute(const ::timber::Reference< ::xylon::model::AttributeModel>& model,
                     const ::timber::Reference< Type>& type, const ::timber::Reference< Type>& enclosing) throws() = 0;
         };

      }
   }
}
#endif
