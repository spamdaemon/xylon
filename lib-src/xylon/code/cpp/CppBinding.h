#ifndef _XYLON_CODE_CPPBINDING_H
#define _XYLON_CODE_CPPBINDING_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_CONFIG_CONFIGURATION_H
#include <timber/config/Configuration.h>
#endif

#include <iosfwd>

namespace xylon {
   namespace model {
      class SchemaModel;
   }
   namespace code {
      /**
       * This namespace provides a code generator for C++.
       */
      namespace cpp {

         /**
          * A CppBinding is used to bind a SchemaModel and generate C++ code.
          */
         class CppBinding : public ::timber::Counted
         {
               /** No copying */
               CppBinding(const CppBinding&);
               CppBinding&operator=(const CppBinding&);

               /** The default constructor */
            protected:
               CppBinding() throws();

               /** Destructor */
            public:
               ~CppBinding() throws();
          };

      }
   }
}
#endif
