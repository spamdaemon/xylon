#ifndef XYLON_CODE_CPP_BINDINGMANAGER_H
#define XYLON_CODE_CPP_BINDINGMANAGER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace xylon {
   class XsdSchemaModel;

   namespace model {
      class AttributeModel;
      class TypeModel;
   }
   namespace code {
      namespace cpp {
         class Type;
         class Attribute;

         /**
          * The BindingManager is used to create different kinds of bindings of a SchemaModel
          * to code.
          */
         class BindingManager : public ::timber::Counted
         {
               /** No copying */
               BindingManager(const BindingManager&);
               BindingManager&operator=(const BindingManager&);

               /** Default constructor */
            protected:
               BindingManager() throws();

               /** Destructor */
            protected:
               ~BindingManager() throws();

               /**
                * Get the type binding for the specified type.
                * @param b a model
                * @return a type binding
                * @throws ::std::exception if there is not such binding.
                */
            public:
               virtual ::timber::Reference< Type> findTypeBinding(const ::timber::Reference<
                     ::xylon::model::TypeModel>& m) const throws (::std::exception) = 0;

               /**
                * Get the type binding for the specified type.
                * @param b a model
                * @return a type binding
                * @throws ::std::exception if there is not such binding.
                */
            public:
               virtual ::timber::Reference< Attribute> findAttributeBinding(const ::timber::Reference<
                     ::xylon::model::AttributeModel>& m) const throws (::std::exception) = 0;

               /**
                * Create a binding for the specified model.
                * @param model a schema model
                * @return a new code binding
                * @throws ::std::exception if the schema model could not be bound
                */
            public:
               virtual void
                     bindModel(const ::timber::Reference< ::xylon::XsdSchemaModel>& model) throws (::std::exception) = 0;

               /**
                * Emit the all binding as code. This method will fail if there are any unresolved
                * schemas, types, etc.
                * @throws ::std::exception if the code could not be generated fully
                */
            public:
               virtual void emitCode() throws (::std::exception) = 0;
         };
      }
   }
}

#endif
