#ifndef _XYLON_CODE_CPP_ABSTRACTSCOPE_H
#define _XYLON_CODE_CPP_ABSTRACTSCOPE_H

#ifndef _XYLON_CODE_CPP_SCOPE_H
#include <xylon/code/cpp/Scope.h>
#endif

#include <set>
#include <string>

namespace xylon {
   namespace code {
      namespace cpp {

         /**
          * This interface defines a C++ scope object. Scopes are usually classes or namespaces, however
          * other C++ artifacts such as functions also introduce scopes.
          */
         class AbstractScope : public virtual Scope
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(AbstractScope);

               /**
                * Construct a namespace binding. An empty string signifies
                * the global namespace.
                * @param str a string
                * */
            protected:
               AbstractScope() throws();

               /** Destructor */
            public:
               ~AbstractScope() throws();

               /**
                * Add a new name to this scope. If there is already such a name, the the scope needs to disambiguate
                * and return a new name to be used.<br>
                * The returned name must not be a keyword.
                * @param name a name
                * @return the name added to this scope.
                */
            public:
               ::std::string addName(const ::std::string& name) throws();

            public:
               bool inScope (const ::std::string& name) const throws();

               /** The names in this scope */
            private:
               ::std::set< ::std::string> _names;
         };
      }
   }
}

#endif
