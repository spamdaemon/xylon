#ifndef _XYLON_CODE_CPP_SIMPLEBINDINGMANAGER_H
#define _XYLON_CODE_CPP_SIMPLEBINDINGMANAGER_H

#ifndef _XYLON_CODE_CPP_BINDINGMANAGER_H
#include <xylon/code/cpp/BindingManager.h>
#endif

#ifndef _XYLON_CODE_CPP_BINDINGFACTORY_H
#include <xylon/code/cpp/BindingFactory.h>
#endif

#ifndef _TIMBER_CONFIG_CONFIGURATION_H
#include <timber/config/Configuration.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {

         /**
          * This class provides a simple binding manager implementation.
          */
         class SimpleBindingManager
         {
               SimpleBindingManager();
               ~SimpleBindingManager();

               /**
                * Create a binding manager that uses a default binding factory.
                * @param factory the binding factory
                * @return a binding manager
                */
            public:
               static ::timber::Reference< BindingManager> create(::std::unique_ptr< BindingFactory> factory,
                     const ::timber::SharedRef< ::timber::config::Configuration>& cfg) throws();
         };
      }
   }
}

#endif
