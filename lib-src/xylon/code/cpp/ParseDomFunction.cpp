#include <xylon/code/cpp/ParseDomFunction.h>
#include <xylon/code/cpp/BindingManager.h>
#include <xylon/code/cpp/Type.h>
#include <xylon/code/cpp/Attribute.h>
#include <xylon/code/cpp/Namespace.h>
#include <xylon/model/models.h>
#include <xylon/model/instructions.h>

using namespace ::std;
using namespace ::timber;
using namespace ::xylon::model;
using namespace ::xylon::model::instructions;

namespace xylon {
   namespace code {
      namespace cpp {
         namespace {




            struct AbstractParseDomFunction : public ParseDomFunction
            {
                  AbstractParseDomFunction(const ::std::string& fname, const Reference< TypeModel>& typeModel) :
                     _model(typeModel), _name(fname)
                  {

                  }
                  ~AbstractParseDomFunction() throws()
                  {
                  }

                  string name() const throws()
                  {
                     return _name;
                  }

                  Reference< TypeModel> model() const throws()
                  {
                     return _model;
                  }
                  /** The type model to be parsed */
               private:
                  Reference< TypeModel> _model;

                  /** The function name */
               private:
                  const ::std::string _name;

            };

            struct ParseElementFunction : public AbstractParseDomFunction
            {
                  ParseElementFunction(const ::std::string& fname, const Reference< TypeModel>& typeModel) :
                     AbstractParseDomFunction(fname, typeModel)
                  {

                  }
                  ~ParseElementFunction() throws()
                  {
                  }

                  void genDeclaration(const BindingManager& mgr, ::std::ostream& stream) const
                  {
                     const string typeName = mgr.findTypeBinding(model())->className();

                     stream << "/**" << endl;
                     stream << " * Parse an XML element into the type " << typeName << "." << endl;
                     stream << " * @param e the current element" << endl;
                     stream << " * @param value the type value to be populated" << endl;
                     stream << " * @return the next unparsed element that is a sibling of e" << endl;
                     stream << " * @throws ::std::exception if the element cannot be parsed" << endl;
                     stream << " */" << endl;
                     stream << "::timber::Pointer< ::timber::w3c::xml::dom::Element> " << name() << "(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& e, "
                           << typeName << "& value);" << endl;
                  }

                  void genDefinition(const BindingManager& mgr, ::std::ostream& stream) const
                  {
                     const string typeName = mgr.findTypeBinding(model())->className();
                     stream << "::timber::Pointer< ::timber::w3c::xml::dom::Element> " << name() << "(const ::timber::Reference< ::timber::w3c::xml::dom::Element>& e, "
                           << typeName << "& value)" << endl;
                     stream << "{" << endl;
                     stream << "   return e->nextSibling();" << endl;
                     stream << "}" << endl;
                  }
            };
            struct ParseStringFunction : public AbstractParseDomFunction
            {
                  ParseStringFunction(const ::std::string& fname, const Reference< TypeModel>& typeModel) :
                     AbstractParseDomFunction(fname, typeModel)
                  {

                  }
                  ~ParseStringFunction() throws()
                  {
                  }

                  void genDeclaration(const BindingManager& mgr, ::std::ostream& stream) const
                  {
                     const string typeName = mgr.findTypeBinding(model())->className();

                     stream << "/**" << endl;
                     stream << " * Parse an UTF-8 encoded string into the type " << typeName << "." << endl;
                     stream << " * @param e a UTF-8 encoded string, possible empty" << endl;
                     stream << " * @param value the type value to be populated" << endl;
                     stream << " * @throws ::std::exception if the string cannot be parsed" << endl;
                     stream << " */" << endl;
                     stream << "void " << name() << "(const ::std::string& e, " << typeName << "& value);" << endl;
                  }
                  void genDefinition(const BindingManager& mgr, ::std::ostream& stream) const
                  {
                     const string typeName = mgr.findTypeBinding(model())->className();
                     stream << "void " << name() << "(const ::std::string& e, " << typeName << "& value)" << endl;
                     stream << "{" << endl;
                     stream << "}" << endl;
                  }

            };
         }
         ParseDomFunction::ParseDomFunction() throws()
         {
         }

         ParseDomFunction::~ParseDomFunction() throws()
         {
         }

         Reference< ParseDomFunction> ParseDomFunction::createElementParser(const ::std::string& name, const Reference<
               TypeModel>& type)
         {
            return new ParseElementFunction(name, type);
         }

         Reference< ParseDomFunction> ParseDomFunction::createStringParser(const ::std::string& name, const Reference<
               TypeModel>& type)
         {
            return new ParseStringFunction(name, type);
         }

      }

   }

}
