#include <xylon/code/cpp/AbstractScope.h>
#include <xylon/code/cpp/Identifier.h>

#include <sstream>

using namespace std;

namespace xylon {
   namespace code {
      namespace cpp {
         AbstractScope::AbstractScope() throws()
         {
         }

         AbstractScope::~AbstractScope() throws()
         {
         }

         bool AbstractScope::inScope(const ::std::string& name) const throws()
         {
            return _names.count(name) != 0;
         }

         string AbstractScope::addName(const string& theName) throws()
         {
            const string n = Identifier::create(theName);

            // try to insert the name; if that succeeds, return that name
            size_t i = 0;
            string str(n);
            while (true) {
               if (!inScope(str)) {
                  bool inserted = _names.insert(str).second;
                  assert(inserted);
                  return str;
               }
               ++i;

               // nameclash; append an integer

               str= n + ::std::to_string(i);
            }

         }
      }
   }
}
