#include <xylon/code/cpp/SimpleBindingManager.h>
#include <xylon/code/cpp/XmlSchemaBindingFactory.h>
#include <xylon/code/cpp/Namespace.h>
#include <xylon/code/cpp/Type.h>
#include <xylon/code/cpp/Attribute.h>
#include <xylon/code/cpp/ParseDomFunction.h>
#include <xylon/model/models.h>
#include <xylon/XsdSchemaModel.h>
#include <timber/logging.h>

#include <xylon/code/cpp/ParserGenerator.h>

#include <canopy/fs/FileSystem.h>

#define PARSERGENERATOR ParserGenerator

#include <fstream>
#include <map>
#include <set>
#include <memory>
#include <vector>
#include <typeinfo>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::config;
using namespace ::timber::logging;
using namespace ::xylon::model;

namespace xylon {
   namespace code {
      namespace cpp {
         namespace {
            static Log logger()
            {
               return Log("xylon.code.cpp.SimpleBindingManager");
            }

            static ::std::string createFileName(const ::std::string& dir, const ::std::string& fname)
            {
               canopy::fs::FileSystem fs;
               ::std::string res;
               if (!fs.isAbsolutePath(fname) && !dir.empty()) {
                  res= fs.joinPaths(dir, fname);
               }
               else {
                  res= fname;
               }
               logger().info("Writing to "+res);
               return res;
            }
            struct TypeSpecification
            {
                  /** The target namespaces supported by this parser (may contain a null pointer) */
                  ::std::set< ::xylon::String> targetNamespaces;

                  /** A header guard (can be empty) */
                  ::std::string headerGuard;

                  /** The destination directory */
                  ::std::string destination;

                  /** The file for the header (including any directories) */
                  ::std::string headerFilename;

                  /** The file for the implementation file (including any directories) */
                  ::std::string implFilename;

                  /** The filenames containing  other schemas */
                  ::std::set< ::std::string> typeFilenames;
            };

            void collectDependencies(const BindingManager& mgr, Reference< TypeModel> m,
                  vector< Reference< TypeModel> >& sortedBindings)
            {
               sortedBindings.push_back(m);
               ModelRef< TypeModel> base = m->baseType();
               vector< Reference< TypeModel> > locals = m->localTypes();
               for (size_t i = 0; i < locals.size(); ++i) {
                  collectDependencies(mgr, locals[i], sortedBindings);
               }
               while (base) {
                  sortedBindings.push_back(base.target());
                  base = base->baseType();
               }
            }

            class BindingManagerImpl : public BindingManager
            {
                  /** The type bindings */
               public:
                  typedef map< Reference< TypeModel>, Reference< Type> > TypeBindings;

                  /** The type bindings */
               public:
                  typedef map< Reference< TypeModel>, Reference< ParseDomFunction> > TypeParsers;

                  /** The type bindings */
               public:
                  typedef map< Reference< AttributeModel>, Reference< Attribute> > Attributes;

                  /**
                   * Create a new binding manager.
                   */
               public:
                  BindingManagerImpl(unique_ptr< BindingFactory> factory, const SharedRef< Configuration>& cfg) throws()
                        : _configuration(cfg)
                  {
                     _factories.push_back(unique_ptr < BindingFactory > (XmlSchemaBindingFactory::create()));
                     _factories.push_back(move(factory));

                     canopy::fs::FileSystem fs;

                     // create the parser files
                     for (size_t i = 0, isz = cfg->count("schema"); i != isz; ++i) {
                       auto schema = cfg->getConfiguration(Configuration::Selector("schema", i));
                        ParserGenerator::Specification parser;
                        for (size_t j = 0, jsz = schema->count("xsd-targetNamespace"); j != jsz; ++j) {
                           string tns = schema->get(Configuration::Selector("xsd-targetNamespace", j));
                           parser.targetNamespaces.insert(tns);
                        }
                        parser.destination = cfg->get("destination-dir");
                        parser.fullName = schema->get("cpp-name");
                        parser.headerFilename = schema->get("cpp-parser.header");
                        parser.implFilename = schema->get("cpp-parser.impl");
                        parser.headerGuard = schema->get("cpp-parser.headerGuard");
                        _parsers.push_back(parser);
                     }

                     // create the type files
                     for (size_t i = 0, isz = cfg->count("type"); i != isz; ++i) {
                        auto schema = cfg->getConfiguration(Configuration::Selector("type", i));
                        TypeSpecification type;
                        for (size_t j = 0, jsz = schema->count("xsd-targetNamespace"); j != jsz; ++j) {
                           string tns = schema->get(Configuration::Selector("xsd-targetNamespace", j));
                           type.targetNamespaces.insert(tns);
                        }
                        type.destination = cfg->get("destination-dir");
                        type.headerFilename = schema->get("cpp-header");
                        type.implFilename = schema->get("cpp-impl");
                        type.headerGuard = schema->get("cpp-headerGuard");

                        if (type.headerGuard.empty()) {
                           type.headerGuard = type.headerFilename;
                           for (string::iterator si = type.headerGuard.begin(); si != type.headerGuard.end(); ++si) {
                              if (*si == '/' || *si == '.') {
                                 *si = '_';
                              }
                           }
                        }

                        _types.push_back(type);
                     }

                  }

                  ~BindingManagerImpl() throws()
                  {
                  }

                  Reference< Type> findTypeBinding(const Reference< TypeModel>& m) const throws(exception)
                  {
                     TypeBindings::const_iterator i = _bindings.find(m);
                     if (i == _bindings.end()) {
                        LogEntry(logger()).severe() << "Type model not bound : " << m->name() << doLog;
                        throw ::std::runtime_error("Not bound");
                     }
                     return i->second;
                  }

                  Reference< Attribute> findAttributeBinding(
                        const Reference< AttributeModel>& m) const throws(exception)
                  {
                     Attributes::const_iterator i = _attributes.find(m);
                     if (i == _attributes.end()) {
                        LogEntry(logger()).severe() << "Attribute model not bound : " << m->name() << doLog;
                        throw ::std::runtime_error("Not bound");
                     }
                     return i->second;
                  }

                  void bindAttribute(const Reference< AttributeModel> model)
                  {
                     bindType(model->scope().target());
                     bindType(model->type().target());
                     Reference< Type> enclosing = findTypeBinding(model->scope().target());
                     Reference< Type> type = findTypeBinding(model->type().target());

                     LogEntry(logger()).debugging() << "Binding attribute model " << model->name() << doLog;
                     if (_attributes.find(model) != _attributes.end()) {
                        return;
                     }
                     Pointer< Attribute> b;
                     for (size_t i = 0; i < _factories.size() && !b; ++i) {
                        b = _factories[i]->createAttribute(model, type, enclosing);
                     }
                     if (b) {
                        _attributes.insert(Attributes::value_type(model, b));
                     }
                     else
                        throw ::std::runtime_error("Attribute not bound " + model->name().string());
                  }

                  void bindType(const Reference< TypeModel> type) throws()
                  {
                     if (_bindings.find(type) != _bindings.end()) {
                        return;
                     }
                     Pointer< Type> b;

                     if (type->isGlobal()) {

                        LogEntry(logger()).debugging() << "Binding global type model " << type->name() << " : "
                              << type->globalName() << " addr : " << &*type << doLog;

                        Pointer< Namespace> nb;

                        for (size_t i = 0; i < _factories.size() && !nb; ++i) {
                           nb = _factories[i]->createNamespace(type->scope());
                        }
                        if (!nb) {
                           nb = Namespace::create("");
                        }

                        for (size_t i = 0; i < _factories.size() && !b; ++i) {
                           b = _factories[i]->createType(type, nb);
                        }

                     }
                     else {
                        LogEntry(logger()).info() << "Binding local type model " << type->name() << doLog;

                        // make sure the enclosing type is being generated first
                        try {
                           bindType(type->scope());
                        }
                        catch (...) {
                           LogEntry(logger()).info() << "type->scope " << typeid(*type->scope()) << doLog;
                           assert(false);
                        }
                        Reference< Type> parent = findTypeBinding(type->scope());

                        // the recursive nature of things might have led to the being instantiated
                        if (_bindings.find(type) != _bindings.end()) {
                           return;
                        }

                        for (size_t i = 0; i < _factories.size() && !b; ++i) {
                           b = _factories[i]->createLocalType(type, parent);
                        }
                     }

                     if (b) {
                        _bindings.insert(TypeBindings::value_type(type, b));
                        if (type->baseType()) {
                           bindType(type->baseType().target());
                        }

                        ::std::vector< Reference< TypeModel> > locals = type->localTypes();
                        for (size_t i = 0; i < locals.size(); ++i) {
                           bindType(locals[i]);
                        }
                     }
                     else {
                        throw ::std::runtime_error("Could not bind type " + type->name().string());
                     }

                     // bind the attributes of the type
                     {
                        ::std::vector< Reference< AttributeModel> > attrs = type->attributes();
                        for (size_t i = 0; i < attrs.size(); ++i) {
                           bindAttribute(attrs[i]);
                        }
                     }

                     // bind the parse function
                     {
                        ostringstream sout;
                        sout << "pe" << _elementParsers.size();

                        //FIXME: how do we know that the type is a complex type
                        Reference< ParseDomFunction> parser = ParseDomFunction::createElementParser(sout.str(), type);
                        _elementParsers.insert(TypeParsers::value_type(type, parser));
                        sout.str("");
                        sout << "ps" << _stringParsers.size();
                        parser = ParseDomFunction::createStringParser(sout.str(), type);
                        _stringParsers.insert(TypeParsers::value_type(type, parser));
                     }
                  }

                  void bindTypes(const vector< ModelRef< TypeModel> >& types)
                  {
                     for (vector< ModelRef< TypeModel> >::const_iterator i = types.begin(); i != types.end(); ++i) {
                        bindType(i->target());
                     }
                  }
                  void bindModel(const Reference< XsdSchemaModel>& model) throws(exception)
                  {
                     bindTypes(model->attributeGroupModels());
                     bindTypes(model->attributeModels());
                     bindTypes(model->elementModels());
                     bindTypes(model->groupModels());
                     bindTypes(model->typeModels());
                  }

                  void emitCode() throws(exception)
                  {

                     // a list of bindings such that for any given type, all its base classes appear
                     // after it in the vector; there may be duplicates in the sortedBindings
                     vector< Reference< TypeModel> > sortedBindings;
                     set< Reference< TypeModel> > foundBindings;

		     LogEntry entry(logger(), Level::INFO);
                     entry << "Number of bindings and types: " << _bindings.size() << ", "
                           << _types.size() << doLog;

                     for (const auto& b : _bindings) {
                        Reference< TypeModel> m = b.first;
                        collectDependencies(*this, m, sortedBindings);
                     }
                     set < string > includes;
                     for (const auto& spec : _types) {
                        bool found = false;
                        for (size_t i = 0; !found && i != sortedBindings.size(); ++i) {
                           GlobalName gn = sortedBindings[i]->globalName();
                           if (gn && spec.targetNamespaces.count(gn.name().namespaceURI()) != 0) {
                              includes.insert(spec.headerFilename);
                              found = true;
                           }
                        }
                        if (!found) {
                           continue;
                        }
                     }

                     ::canopy::fs::FileSystem fs;

                     for (::std::vector< TypeSpecification>::const_iterator t = _types.begin(); t != _types.end();
                           ++t) {
                        const TypeSpecification& spec = *t;
                        if (includes.count(spec.headerFilename) == 0) {
                           logger().info("No header for "+spec.headerFilename);
                           continue;
                        }

                        string dirname = fs.directoryName(createFileName(spec.destination, spec.headerFilename));
                        if (!dirname.empty()) {
                           try {
                              fs.mkdirs(dirname);
                           }
                           catch (const ::std::exception& e) {
                              throw ::std::runtime_error("Could not create directory :" + dirname);
                           }
                        }

                        for (::std::vector< ParserGenerator::Specification>::iterator p = _parsers.begin();
                              p != _parsers.end(); ++p) {
                           p->schemaFilenames.insert(spec.headerFilename);
                        }

                        string guard = spec.headerGuard;

                        ofstream header(createFileName(spec.destination, spec.headerFilename).c_str());
                        header << "#ifndef " << guard << endl;
                        header << "#define " << guard << endl;
                        header << endl;
                        header << "#include <memory>" << endl;
                        header << "#include <string>" << endl;
                        header << "#include <vector>" << endl;
                        header << "#include <stdexcept>" << endl;
                        header << "#include <iostream>" << endl;
                        header << endl;

                        for (::std::vector< TypeSpecification>::const_iterator tx = _types.begin(); tx != _types.end();
                              ++tx) {
                           if (tx->headerFilename != t->headerFilename && includes.count(tx->headerFilename) != 0) {
                              header << "#ifndef " << tx->headerGuard << endl;
                              header << "#include <" << tx->headerFilename << ">" << endl;
                              header << "#endif" << endl;
                           }
                        }

                        for (size_t i = 0; i < _factories.size(); ++i) {
                           for (const auto& inc : _factories[i]->getIncludes()) {
                              header << "#include " << inc << endl;
                           }
                        }

                        header << endl;

                        for (size_t i = 0; i < sortedBindings.size(); ++i) {
                           Reference< TypeModel> m = sortedBindings[i];

                           if (foundBindings.count(m) == 0 && m->isGlobal()) {
                              Reference< Type> b = findTypeBinding(m);
                              b->genForwardDeclaration(header);
                              foundBindings.insert(m);
                           }
                        }

                        foundBindings.clear();
                        // traverse from the rear and ensure that no duplicate bindings ar
                        // being generated
                        for (size_t i = sortedBindings.size(); i-- > 0;) {
                           Reference< TypeModel> m = sortedBindings[i];
                           GlobalName gn = m->globalName();
                           if (foundBindings.count(m) == 0 && gn
                                 && spec.targetNamespaces.count(gn.name().namespaceURI()) != 0) {
                              foundBindings.insert(m);
                              assert(_bindings.find(m) != _bindings.end());
                              Reference< Type> b = _bindings.find(m)->second;
                              b->genDeclaration(*this, header);
                           }
                        }
                        header << "#endif" << endl;
                     }

                     for (::std::vector< TypeSpecification>::const_iterator t = _types.begin(); t != _types.end();
                           ++t) {
                        const TypeSpecification& spec = *t;

                        if (includes.count(spec.headerFilename) == 0) {
                           continue;
                        }

                        string dirname = fs.directoryName(createFileName(spec.destination, spec.implFilename));
                        if (!dirname.empty()) {
                           try {
                              fs.mkdirs(dirname);
                           }
                           catch (const ::std::exception& e) {
                              throw ::std::runtime_error("Could not create directory :" + dirname);
                           }
                        }

                        ofstream impl(createFileName(spec.destination, spec.implFilename).c_str());
                        impl << "#include <" << spec.headerFilename << ">" << endl;
                        for (TypeBindings::const_iterator i = _bindings.begin(); i != _bindings.end(); ++i) {
                           Reference< TypeModel> m = i->first;
                           Pointer< TypeModel> cur = m;

                           while (cur) {
                              GlobalName gn = cur->globalName();

                              if (gn && spec.targetNamespaces.count(gn.name().namespaceURI()) != 0) {
                                 Reference< Type> b = i->second;
                                 b->genDefinition(*this, impl);
                                 break;
                              }
                              else {
                                 cur = cur->scope().tryDynamicCast< TypeModel>();
                              }
                           }
                        }
                     }

                     for (::std::vector< ParserGenerator::Specification>::const_iterator p = _parsers.begin();
                           p != _parsers.end(); ++p) {

                        vector< Reference< TypeModel> > models;
                        for (TypeBindings::const_iterator i = _bindings.begin(); i != _bindings.end(); ++i) {
                           models.push_back(i->first);
                        }

                        unique_ptr< PARSERGENERATOR> generator = PARSERGENERATOR::create();
                        generator->generateDeclaration(*this, *p, models);
                        generator->generateDefinition(*this, *p, models);
                     }

                  }

                  /** The binding factories */
               private:
                  ::std::vector< ::std::unique_ptr< BindingFactory> > _factories;

                  /** The configuration */
               private:
                  const SharedRef< Configuration> _configuration;

                  /** The binding for each type */
               private:
                  TypeBindings _bindings;

                  /** The attributes */
               private:
                  Attributes _attributes;

                  /** The output directory */
               private:
                  string _outputDir;

                  /** The element  and string parsers */
               private:
                  TypeParsers _elementParsers, _stringParsers;

                  /** The schema header and impl files, one per namespace */
               private:
                  vector< ParserGenerator::Specification> _parsers;

                  /** The types */
               private:
                  vector< TypeSpecification> _types;
            };

         }

         Reference< BindingManager> SimpleBindingManager::create(::std::unique_ptr< BindingFactory> factory,
               const SharedRef< Configuration>& cfg) throws()
         {
            return new BindingManagerImpl(::std::move(factory), cfg);
         }

      }
   }
}
