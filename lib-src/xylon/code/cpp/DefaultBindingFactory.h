#ifndef _XYLON_CODE_CPP_DEFAULTBINDINGFACTORY_H
#define _XYLON_CODE_CPP_DEFAULTBINDINGFACTORY_H

#ifndef _XYLON_CODE_CPP_BINDINGFACTORY_H
#include <xylon/code/cpp/BindingFactory.h>
#endif

#ifndef _TIMBER_CONFIG_CONFIGURATION_H
#include <timber/config/Configuration.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {
         class DefaultBindingFactory
         {
               DefaultBindingFactory();
               ~DefaultBindingFactory();

               /**
                * Create a new binding factory. The factory uses the specified configuration
                * to determine type prefixes, type suffixes, namespace names, etc.
                * @param cfg a configuration
                * @return a new BindingFactory
                */
            public:
               static ::std::unique_ptr< BindingFactory>
               create(const ::timber::SharedRef< ::timber::config::Configuration>& cfg) throws();

         };

      }

   }

}

#endif
