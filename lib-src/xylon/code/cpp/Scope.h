#ifndef _XYLON_CODE_CPP_SCOPE_H
#define _XYLON_CODE_CPP_SCOPE_H

#ifndef _XYLON_CODE_CPP_CPPBINDING_H
#include <xylon/code/cpp/CppBinding.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {

         /**
          * This interface defines a C++ scope object. Scopes are usually classes or namespaces, however
          * other C++ artifacts such as functions also introduce scopes.
          */
         class Scope : public virtual CppBinding
         {
               CANOPY_BOILERPLATE_PREVENT_COPYING(Scope);

               /**
                * Construct a namespace binding. An empty string signifies
                * the global namespace.
                * @param str a string
                * */
            protected:
               Scope() throws();

               /** Destructor */
            public:
               ~Scope() throws();

               /**
                * Determine if the specified name already exists in this scope.
                * @param name a name
                * @return true if this name exists in this scope, false otherwise
                */
            public:
               virtual bool inScope (const ::std::string& name) const throws() = 0;

               /**
                * Add a new name to this scope. If there is already such a name, the the scope needs to disambiguate
                * and return a new name to be used.<br>
                * The returned name must not be a keyword.
                * @param name a name
                * @return the name added to this scope.
                */
            public:
               virtual ::std::string addName (const ::std::string& name) throws() = 0;
         };
      }
   }
}

#endif
