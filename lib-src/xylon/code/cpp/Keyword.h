#ifndef _XYLON_CODE_CPP_KEYWORD_H
#define _XYLON_CODE_CPP_KEYWORD_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace xylon {

   namespace code {

      namespace cpp {

         /**
          * A representation of a C++ keyword. Currently, only a function is supported
          * to test if a string is a keyword.
          */
         class Keyword
         {
            private:
               Keyword();
               virtual ~Keyword();

               /**
                * Test if the specified string corresponds to a keyword.
                * @param str a string
                * @return true if str is a valid keyword, false otherwise
                */
            public:
               static bool isKeyword(const char* str) throws();

         };

      }

   }

}

#endif
