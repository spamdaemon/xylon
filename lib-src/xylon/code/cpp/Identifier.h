#ifndef _XYLON_CODE_CPP_IDENTIFIER_H
#define _XYLON_CODE_CPP_IDENTIFIER_H

#include <string>

namespace xylon {
   namespace code {
      namespace cpp {

         /**
          * Representation of C++ identifiers.
          */
         class Identifier
         {
            private:
        	 Identifier() = delete;
        	 Identifier(const Identifier&) = delete;

        	 /**
        	  * Create an identifier.
        	  * @param name a string representation some name in the schema
        	  * @return mapping of the name into a valid c++ identifier
        	  */
            public:
        	 static ::std::string create(const ::std::string& name);
         };
      }
   }
}

#endif
