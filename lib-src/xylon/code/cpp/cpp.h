#ifndef _XYLON_CODE_CPP_H
#define _XYLON_CODE_CPP_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <string>
#include <vector>

namespace xylon {
   namespace code {
      /**
       * This namespace provides a code generator for C++.
       */
      namespace cpp {

         /**
          * Split a C++ name into its components.
          * @param name a C++ name containing <tt>::</tt> separators.
          * @return a vector of components that were separated by <tt>::</tt>.
          */
         ::std::vector< ::std::string> splitFullname(const ::std::string& name) throws();
      }

   }
}
#endif
