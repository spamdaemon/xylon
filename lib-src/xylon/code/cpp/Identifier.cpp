#include <xylon/code/cpp/Identifier.h>
#include <cstring>

namespace xylon {
namespace code {
namespace cpp {

namespace {

static const char* keywords[] = { "and", "and_eq", "asm", "auto", "bitand",
		"bitor", "bool", "break", "case", "catch", "char", "class", "compl",
		"const", "const_cast", "continue", "default", "delete", "do", "double",
		"dynamic_cast", "else", "enum", "explicit", "export", "extern", "false",
		"float", "for", "friend", "goto", "if", "inline", "int", "long",
		"mutable", "namespace", "new", "not", "not_eq", "operator", "or",
		"or_eq", "private", "protected", "public", "register",
		"reinterpret_cast", "return", "short", "signed", "sizeof", "static",
		"static_cast", "struct", "switch", "template", "this", "throw", "true",
		"try", "typedef", "typeid", "typename", "union", "unsigned", "using",
		"virtual", "void", "volatile", "wchar_t", "while", "xor", "xor_eq", 0 };

bool isKeyword(const char* str) {
	for (const char** f = keywords; *f != 0; ++f) {
		if (::std::strcmp(str, *f) == 0) {
			return true;
		}
	}
	return false;

}

static bool isValidIdentifierChar(char ch) {
	return ::std::isalnum(ch) || ch == '_';
}

}

::std::string Identifier::create(const ::std::string& name) {
	if (isKeyword(name.c_str())) {
		return 'X' + name;
	}
	::std::string res(name);
	auto i = res.begin();
	while (i != res.end()) {
		if (!isValidIdentifierChar(*i)) {
			*i = '_';
		}
		++i;
	}
	return res;
}

}

}
}
