#ifndef _XYLON_CODE_CPP_NAMESPACE_H
#define _XYLON_CODE_CPP_NAMESPACE_H

#ifndef _XYLON_CODE_CPP_ABSTRACTSCOPE_H
#include <xylon/code/cpp/AbstractScope.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {

         /**
          * This is a binding of a target namespace to a C++ namespace. A  namespace
          * is a @code :: @endcode separated string of C++ identifiers.
          */
         class Namespace : public AbstractScope
         {
               Namespace(const Namespace&);
               Namespace&operator=(const Namespace&);

               /**
                * Construct a namespace binding. An empty string signifies
                * the global namespace.
                * @param str a string
                * */
            private:
               Namespace(const ::std::string& ns) throws();

               /** Destructor */
            public:
               ~Namespace() throws();

               /**
                * Create a namespace binding. An empty string signifies
                * the global namespace.
                * @param str string
                * @throws ::std::exception if the string does not consist of valid c++ identifiers
                */
            public:
               static ::timber::Reference<Namespace> create (const ::std::string& str) throws (::std::exception);

               /**
                * Get the namespace name as a @code :: @endcode separated string.
                * @return the namespace name.
                */
            public:
               virtual ::std::string getNamespace() const throws();

               /**
                * Emit a declaration to open this namespace.
                * @param out an output stream
                */
            public:
               virtual void open( ::std::ostream& out) const;

               /**
                * Emit a closing declaration of this namespace.
                * @param out an output stream
                */
            public:
               virtual void close (::std::ostream& out) const;

               /**
                * Emit a the full namespace as a @code :: @endcode separated string.
                * @param out an output stream
                */
            public:
               virtual void print (::std::ostream& out) const;

               /** The namespace string is either empty or a string beginning with @code :: @endcode */
            private:
               ::std::string _namespace;
         };

      }

   }

}

#endif
