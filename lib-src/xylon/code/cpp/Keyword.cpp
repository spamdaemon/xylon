#include <xylon/code/cpp/Keyword.h>
#include <cstring>

namespace xylon {
   namespace code {
      namespace cpp {
         namespace {
            static const char* keywords[] = {
                  "and", "and_eq", "asm", "auto", "bitand", "bitor", "bool", "break", "case", "catch", "char", "class",
                  "compl", "const", "const_cast", "continue", "default", "delete", "do", "double", "dynamic_cast",
                  "else", "enum", "explicit", "export", "extern", "false", "float", "for", "friend", "goto", "if",
                  "inline", "int", "long", "mutable", "namespace", "new", "not", "not_eq", "operator", "or", "or_eq",
                  "private", "protected", "public", "register", "reinterpret_cast", "return", "short", "signed",
                  "sizeof", "static", "static_cast", "struct", "switch", "template", "this", "throw", "true", "try",
                  "typedef", "typeid", "typename", "union", "unsigned", "using", "virtual", "void", "volatile",
                  "wchar_t", "while", "xor", "xor_eq", 0 };
         }

         Keyword::Keyword()
         {
         }

         Keyword::~Keyword()
         {
         }

         bool Keyword::isKeyword(const char* str) throws()
      {
         for (const char** f = keywords; *f!=0; ++f) {
            if (::std::strcmp(str,*f)==0) {
               return true;
            }
         }
         return false;

      }

   }

}
}
