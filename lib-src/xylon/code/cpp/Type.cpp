#include <xylon/code/cpp/Type.h>
#include <xylon/code/cpp/Namespace.h>

using namespace ::std;

namespace xylon {
   namespace code {
      namespace cpp {
         Type::Type() throws()
         {
         }

         Type::~Type() throws()
         {
         }

         ::timber::Pointer< Type> Type::base() const throws()
         {
            return ::timber::Pointer< Type> ();
         }

         bool Type::inScope (const ::std::string& name) const throws()
         {
            ::timber::Pointer< Type> b = base();

            return b && b->inScope(name);
         }

         string Type::className() const throws()
         {
            string cname;
            if (enclosingType()) {
               cname += enclosingType()->className();
            }
            if (enclosingNamespace()) {
               cname += enclosingNamespace()->getNamespace();
            }
            cname += "::";
            cname += name();
            return cname;
         }

      }
   }
}
