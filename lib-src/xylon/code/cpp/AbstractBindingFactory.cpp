#include <xylon/code/cpp/AbstractBindingFactory.h>
#include <xylon/code/cpp/Type.h>
#include <xylon/code/cpp/Namespace.h>
#include <xylon/code/cpp/Attribute.h>
#include <xylon/model/models.h>

using namespace ::timber;
using namespace ::xylon::model;

namespace xylon {

   namespace code {

      namespace cpp {

         AbstractBindingFactory::AbstractBindingFactory() throws()
         {
         }

         AbstractBindingFactory::~AbstractBindingFactory() throws()
         {
         }

         Pointer< Namespace> AbstractBindingFactory::createNamespace(const Reference< NamespaceModel>&) throws()
         {
            return Pointer< Namespace> ();
         }

         Pointer< Type> AbstractBindingFactory::createType(const Reference< TypeModel>&,
               const Reference< Namespace>&) throws()
         {
            return Pointer< Type> ();
         }

         Pointer< Type> AbstractBindingFactory::createLocalType(const Reference< TypeModel>&,
               const Reference< Type>&) throws()
         {
            return Pointer< Type> ();
         }
         Pointer< Attribute> AbstractBindingFactory::createAttribute(

         const Reference< AttributeModel>&, const ::timber::Reference< Type>&, const Reference<
               Type>&) throws()
         {
            return Pointer< Attribute> ();
         }

      }

   }

}
