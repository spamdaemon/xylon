#ifndef _XYLON_CODE_CPP_FUNCTION_H
#define _XYLON_CODE_CPP_FUNCTION_H

#ifndef _XYLON_CODE_CPP_CPPBINDING_H
#include <xylon/code/cpp/CppBinding.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {
         class BindingManager;

         /**
          * This method is a baseclass for all functions.
          */
         class Function : public virtual CppBinding
         {
               Function(const Function&);
               Function&operator=(const Function&);

               /** Default constructor */
            public:
               Function() throws();

               /** Destructor */
            public:
               virtual ~Function() throws();

               /**
                * Get the name of this function.
                * @return the name of this function
                */
            public:
               virtual ::std::string name() const throws() = 0;

               /**
                * Emit the prototype declaration for this  function. This method
                * is assumed to be emitted within the enclosing class' declaration.
                * @param stream an output stream
                */
            public:
               virtual void genDeclaration(const BindingManager& mgr, ::std::ostream& stream) const = 0;

               /**
                * Emit the implementation definition of this  function. This method may
                * not assume that any enclosing namespaces have been opened.
                * @param stream an output stream.
                */
            public:
               virtual void genDefinition(const BindingManager& mgr, ::std::ostream& stream) const = 0;

         };

      }

   }

}

#endif
