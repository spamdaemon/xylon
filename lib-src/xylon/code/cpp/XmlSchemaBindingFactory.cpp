#include <xylon/code/cpp/XmlSchemaBindingFactory.h>
#include <xylon/code/cpp/AbstractBindingFactory.h>
#include <xylon/code/cpp/AbstractScope.h>
#include <xylon/code/cpp/Namespace.h>
#include <xylon/code/cpp/Type.h>
#include <xylon/code/cpp/Attribute.h>
#include <xylon/code/cpp/BindingManager.h>
#include <xylon/model/models.h>
#include <xylon/xsdom/xsdom.h>

#include <timber/logging.h>
#include <timber/w3c/xml/xml.h>

#include <ostream>
#include <vector>
#include <sstream>
#include <set>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::config;
using namespace ::timber::logging;
using namespace ::xylon::model;

namespace xylon {
   namespace code {
      namespace cpp {
         namespace {
            static const char* btypes[] = {
                  "anyURI", "base64Binary", "boolean", "byte", "date", "dateTime", "decimal", "double", "duration",
                  "ENTITIES", "ENTITY", "float", "gDay", "gMonth", "gMonthDay", "gYear", "gYearMonth", "hexBinary",
                  "ID", "IDREF", "IDREFS", "int", "integer", "language", "long", "Name", "NCName", "negativeInteger",
                  "NMTOKEN", "NMTOKENS", "nonNegativeInteger", "nonPositiveInteger", "normalizedString", "NOTATION",
                  "positiveInteger", "QName", "short", "string", "time", "token", "unsignedByte", "unsignedInt",
                  "unsignedLong", "unsignedShort", 0 };

            static Log logger()
            {
               return Log("xylon.code.cpp.XmlSchemaBindingFactory");
            }

            struct XsdBinding : public virtual Type, public AbstractScope
            {
                  XsdBinding(const Reference< TypeModel>& m, const Pointer< Namespace>& encNamespace) :
                     _model(m), _enclosingNamespace(encNamespace)
                  {
                     _name = m->name().string();
                     _name[0] = toupper(_name[0]);
                  }

                  ~XsdBinding() throws()
                  {
                  }
                  bool inScope(const string& name) const throws()
                  {
                     return Type::inScope(name) || AbstractScope::inScope(name);
                  }

                  string name() const throws()
                  {
                     return _name;
                  }

                  inline Reference< TypeModel> model() const throws()
                  {
                     return _model;
                  }

                  string baseClassname(const BindingManager& manager) const
                  {
                     if (model()->baseType()) {
                        return manager.findTypeBinding(model()->baseType().target())->className();
                     }
                     return "::xylon::rts::Object";
                  }

                  Pointer< Namespace> enclosingNamespace() const throws()
                  {
                     return _enclosingNamespace;
                  }

                  Pointer< Type> enclosingType() const throws()
                  {
                     return Pointer< Type> ();
                  }

                  void genForwardDeclaration(ostream& stream) const
                  {
                     stream << "namespace xsd {" << endl;
                     stream << "class " << _name << ';' << endl;
                     stream << "}" << endl;
                  }

                  void genDeclaration(const BindingManager&, ostream&) const
                  {
                     // no need to generate the decl
                  }

                  void genDefinition(const BindingManager&, ostream&) const
                  {
                     // no need to generate definition
                  }
                  /**
                   * Generate the attributes.
                   *
                   */
                  void genAttributeDeclarations(const BindingManager&, ostream&) const
                  {
                  }

                  bool genBindingStatement(ostream& stream, const string& instanceExpr, const string& valueExpr) const throws()
                  {
                     stream << "(" << instanceExpr << ").fromString(" << valueExpr << ");" << endl;
                     return true;
                  }

               private:
                  ::std::string _name;

               private:
                  const Reference< TypeModel> _model;

               private:
                  Pointer< Namespace> _enclosingNamespace;
            };

            struct XsdAnyBinding : public virtual Type, public AbstractScope
            {
                  XsdAnyBinding(const Reference< TypeModel>& m, const Pointer< Namespace>& encNamespace) :
                     _name("AnyElement"), _model(m), _enclosingNamespace(Namespace::create("xylon::rt"))
                  {
                  }

                  ~XsdAnyBinding() throws()
                  {
                  }

                  bool inScope(const string& name) const throws()
                  {
                     return Type::inScope(name) || AbstractScope::inScope(name);
                  }
                  string name() const throws()
                  {
                     return _name;
                  }

                  inline Reference< TypeModel> model() const throws()
                  {
                     return _model;
                  }

                  string baseClassname(const BindingManager& manager) const
                  {
                     return "::xylon::rt::ElementObject";
                  }

                  Pointer< Namespace> enclosingNamespace() const throws()
                  {
                     return _enclosingNamespace;
                  }

                  Pointer< Type> enclosingType() const throws()
                  {
                     return Pointer< Type> ();
                  }

                  void genForwardDeclaration(ostream& stream) const
                  {
                     stream << "namespace xylon {" << endl;
                     stream << "namespace rt {" << endl;
                     stream << " class AnyElement;" << endl;
                     stream << "} }" << endl;
                  }

                  void genDeclaration(const BindingManager&, ostream&) const
                  {
                     // no need to generate the decl
                  }

                  void genDefinition(const BindingManager&, ostream&) const
                  {
                     // no need to generate definition
                  }

                  /**
                   * Generate the attributes.
                   *
                   */
                  void genAttributeDeclarations(const BindingManager& mgr, ostream& stream) const
                  {
                  }

                  bool genBindingStatement(ostream& stream, const string& instanceExpr, const string& valueExpr) const throws()
                  {
                     return false;
                  }

               private:
                  const ::std::string _name;

               private:
                  const Reference< TypeModel> _model;

               private:
                  Pointer< Namespace> _enclosingNamespace;
            };

            struct XsdAnySimpleTypeBinding : public virtual Type, public AbstractScope
            {
                  XsdAnySimpleTypeBinding(const Reference< TypeModel>& m, const Pointer< Namespace>& encNamespace) :
                     _name("AnySimpleType"), _model(m), _enclosingNamespace(Namespace::create("xylon::rt"))
                  {
                  }

                  ~XsdAnySimpleTypeBinding() throws()
                  {
                  }

                  bool inScope(const string& name) const throws()
                  {
                     return Type::inScope(name) || AbstractScope::inScope(name);
                  }
                  string name() const throws()
                  {
                     return _name;
                  }

                  inline Reference< TypeModel> model() const throws()
                  {
                     return _model;
                  }

                  string baseClassname(const BindingManager& manager) const
                  {
                     return "::xylon::rt::TypeObject";
                  }

                  Pointer< Namespace> enclosingNamespace() const throws()
                  {
                     return _enclosingNamespace;
                  }

                  Pointer< Type> enclosingType() const throws()
                  {
                     return Pointer< Type> ();
                  }

                  void genForwardDeclaration(ostream& stream) const
                  {
                     stream << "namespace xylon {" << endl;
                     stream << "namespace rt {" << endl;
                     stream << " class AnySimpleType;" << endl;
                     stream << "} }" << endl;
                  }

                  void genDeclaration(const BindingManager&, ostream&) const
                  {
                     // no need to generate the decl
                  }

                  void genDefinition(const BindingManager&, ostream&) const
                  {
                     // no need to generate definition
                  }

                  /**
                   * Generate the attributes.
                   *
                   */
                  void genAttributeDeclarations(const BindingManager& mgr, ostream& stream) const
                  {
                  }

                  bool genBindingStatement(ostream& stream, const string& instanceExpr, const string& valueExpr) const throws()
                  {
                     return false;
                  }

               private:
                  const ::std::string _name;

               private:
                  const Reference< TypeModel> _model;

               private:
                  Pointer< Namespace> _enclosingNamespace;
            };

            struct XsdAnyTypeBinding : public virtual Type, public AbstractScope
            {
               XsdAnyTypeBinding(const Reference< TypeModel>& m, const Pointer< Namespace>& encNamespace) :
                     _name("TypeObject"), _model(m), _enclosingNamespace(Namespace::create("xylon::rt"))
                  {
                  }

                  ~XsdAnyTypeBinding() throws()
                  {
                  }

                  bool inScope(const string& name) const throws()
                  {
                     return Type::inScope(name) || AbstractScope::inScope(name);
                  }
                  string name() const throws()
                  {
                     return _name;
                  }

                  inline Reference< TypeModel> model() const throws()
                  {
                     return _model;
                  }

                  string baseClassname(const BindingManager& manager) const
                  {
                     return "";
                  }

                  Pointer< Namespace> enclosingNamespace() const throws()
                  {
                     return _enclosingNamespace;
                  }

                  Pointer< Type> enclosingType() const throws()
                  {
                     return Pointer< Type> ();
                  }

                  void genForwardDeclaration(ostream& stream) const
                  {
                  }

                  void genDeclaration(const BindingManager&, ostream&) const
                  {
                     // no need to generate the decl
                  }

                  void genDefinition(const BindingManager&, ostream&) const
                  {
                     // no need to generate definition
                  }

                  /**
                   * Generate the attributes.
                   *
                   */
                  void genAttributeDeclarations(const BindingManager& mgr, ostream& stream) const
                  {
                  }

                  bool genBindingStatement(ostream& stream, const string& instanceExpr, const string& valueExpr) const throws()
                  {
                     return false;
                  }

               private:
                  const ::std::string _name;

               private:
                  const Reference< TypeModel> _model;

               private:
                  Pointer< Namespace> _enclosingNamespace;
            };

         }
         unique_ptr< BindingFactory> XmlSchemaBindingFactory::create() throws()

         {
            struct Impl : public AbstractBindingFactory
            {
                  Impl() throws()

                  {
                  }

                  ~Impl() throws()
                  {
                  }

                  bool checkURI(const String& nsURI) const throws()
                  {
                     return nsURI == ::xylon::xsdom::XSD_NAMESPACE_URI || nsURI
                           == ::timber::w3c::xml::XML_NAMESPACE;
                  }

                  vector< string> getIncludes() const throws()
                  {
                     vector< string> incs;
                     incs.push_back("<xsd/types.h>");
                     incs.push_back("<xylon/rt/AnyElement.h>");
                     return incs;
                  }

                  Pointer< Namespace> createNamespace(const Reference< NamespaceModel>& model) throws()
                  {
                     if (checkURI(model->scopeName().namespaceURI())) {
                        return Namespace::create("xsd");
                     }
                     return Pointer< Namespace> ();
                  }
                  Pointer< Type> createType(const Reference< TypeModel>& type,
                        const ::timber::Reference< Namespace>& ns) throws()
                  {
                     Pointer< Type> b;

                     if (!checkURI(type->globalName().name().namespaceURI())) {
                        return b;
                     }
                     if (ns->getNamespace() != "::xsd") {
                        LogEntry(logger()).warn() << "Inconsistent namespace for builtin XSD type "
                              << type->globalName().name() << doLog;
                        return b;
                     }
                     if (type->globalName().name() == QName::xsQName("any")) {
                        return new XsdAnyBinding(type, ns);
                     }
                     if (type->globalName().name() == QName::xsQName("anySimpleType")) {
                        return new XsdAnySimpleTypeBinding(type, ns);
                     }
                     if (type->globalName().name() == QName::xsQName("anyType")) {
                        return new XsdAnyTypeBinding(type, ns);
                     }
                     if (type->isComplex()) {
                        return b;
                     }
                     if (type->globalName().type() != GlobalName::TYPE) {
                        return b;
                     }
                     //FIXME: this is a little bit ugly
                     if (type.tryDynamicCast< UnionTypeModel> () || type.tryDynamicCast< ListTypeModel> ()) {
                        return b;
                     }

#if 0
                     for (const char** types = btypes; *types != 0; ++types) {
                        if (QName::xsQName(*types) == type->globalName().name()) {
                           return new XsdBinding(type, ns);
                        }
                     }
                     return b;
#else
                     return new XsdBinding(type, ns);
#endif
                  }

                  Pointer< Type> createLocalType(const Reference< TypeModel>& type,
                        const ::timber::Reference< Type>& enc) throws()
                  {
                     return Pointer< Type> ();
                  }

                  Pointer< Attribute> createAttribute(const Reference< AttributeModel>& model,
                        const ::timber::Reference< Type>& type, const Reference< Type>& enclosing) throws()
                  {
                     return Pointer< Attribute> ();
                  }

                  /** The builtin xsd types */
               private:
                  ::std::set< ::std::string> _xsdTypes;
            };

            BindingFactory* f = new Impl();
            return unique_ptr< BindingFactory> (f);
         }

      }

   }

}
