#ifndef _XYLON_CODE_CPP_PARSEDOMFUNCTION_H
#define _XYLON_CODE_CPP_PARSEDOMFUNCTION_H

#ifndef _XYLON_CODE_CPP_MEMBERFUNCTION_H
#include <xylon/code/cpp/MemberFunction.h>
#endif

namespace xylon {
   namespace model {
      class TypeModel;
   }
   namespace code {
      namespace cpp {
         class Type;

         /**
          * This function is the baseclass for function that can parse a dom node into an object.
          */
         class ParseDomFunction : public MemberFunction
         {
               ParseDomFunction(const ParseDomFunction&);
               ParseDomFunction&operator=(const ParseDomFunction&);

               /** Default constructor */
            public:
               ParseDomFunction() throws();

               /** Destructor */
            public:
               virtual ~ParseDomFunction() throws();

               /**
                 * Create a new DOM function to parse an element into a type. This function takes a
                 * reference to an element and a type, and parses the contents of the
                 * element into the type:
                 * @code void function (const ::timber::Reference< ::timber::w3c::xml::ElementObject>&, T& value); @endcode
                 * @param type the type whose content is to be parsed.
                 * @return a parser function
                 */
             public:
                static ::timber::Reference< ParseDomFunction> createElementParser(const ::std::string& name, const ::timber::Reference<
                      ::xylon::model::TypeModel>& type);

                /**
                  * Create a new DOM function to parse a string into a type. This function takes a
                  * reference to an element and a type, and parses the contents of the
                  * element into the type:
                  * @code void function (const ::timber::Reference< ::timber::w3c::xml::ElementObject>&, T& value); @endcode
                  * @param type the type whose content is to be parsed.
                  * @return a parser function
                  */
              public:
                 static ::timber::Reference< ParseDomFunction> createStringParser(const ::std::string& name, const ::timber::Reference<
                       ::xylon::model::TypeModel>& type);
         };

      }

   }

}

#endif
