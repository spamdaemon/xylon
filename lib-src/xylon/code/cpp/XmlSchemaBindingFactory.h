#ifndef _XYLON_CODE_CPP_XMLSCHEMABINDINGFACTORY_H
#define _XYLON_CODE_CPP_XMLSCHEMABINDINGFACTORY_H

#ifndef _XYLON_CODE_CPP_BINDINGFACTORY_H
#include <xylon/code/cpp/BindingFactory.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {
         class XmlSchemaBindingFactory
         {
               XmlSchemaBindingFactory();
               ~XmlSchemaBindingFactory();

               /**
                * Create the default binding factory for mapping xml schema classes. The created factory
                * can generate code for the builtin simple types of the xml schema.
                */
            public:
               static ::std::unique_ptr< BindingFactory> create() throws();

         };

      }

   }

}

#endif
