#ifndef _XYLON_CODE_CPP_CLASS_H
#define _XYLON_CODE_CPP_CLASS_H

#ifndef _XYLON_CODE_CPP_SCOPE_H
#include <xylon/code/cpp/Scope.h>
#endif

namespace xylon {
   namespace code {
      namespace cpp {
         class BindingManager;
         class Namespace;

         class Type : public virtual Scope
         {
               Type(const Type&);
               Type&operator=(const Type&);

               /**
                * Default constructor.
                */
            protected:
               Type() throws();

               /** Destructor */
            public:
               ~Type() throws();

               /**
                * Check if a name is in scope. A name is in scope if this class has a base type
                * and the name is in the scope of the base type. Subclasses typically still need to
                * override this method.
                */
            public:
               bool inScope (const ::std::string& name) const throws();

               /**
                * Get a pointer to the base type.
                * @return a pointer to a base type (0 by default).
                */
            public:
               virtual ::timber::Pointer<Type> base() const throws();

               /**
                * Get the unqualified name for the C++ type.
                * @return the name of the C++ type.
                */
            public:
               virtual ::std::string name() const throws() = 0;

               /**
                * Get the namespace binding if this is a top-level type.
                * @return the namespace scope for this type or null if this is local type
                */
            public:
               virtual ::timber::Pointer< Namespace> enclosingNamespace() const throws() = 0;

               /**
                * Get the class binding within which this binding is declared.
                * @return a type binding or null if this is <em>not</em> a local type
                */
            public:
               virtual ::timber::Pointer< Type> enclosingType() const throws() = 0;

               /**
                * Get the fully qualified name.The fully qualified name
                * for the bound type can be obtained as follows:
                * @code
                *  ::std::string qname = "";
                *  if (!this->namespaceScope().empty()) {
                *    qname += this->namespaceScope();
                *    qname += "::";
                *  }
                *  if (!this->classScope().empty()) {
                *    qname += this->classScope();
                *    qname += "::";
                *  }
                *  qname += this->name();
                * @endcode
                * @return fully qualified name for this type.
                */
            public:
               ::std::string className() const throws();

               /**
                * Generate the a forward declaration for the type. A forward declaration appears in a header file and not
                * an implementation file. It is assumed that forward declarations for any referenced types
                * have been emitted already and the enclosing scope has been opened.
                * @param stream an output stream
                */
            public:
               virtual void genForwardDeclaration(::std::ostream& stream) const = 0;

               /**
                * Generate the declaration for this binding. A declaration appears in a header file and not
                * an implementation file. It is assumed that forward declarations for any referenced types
                * have been emitted already and the enclosing scope has been opened.<br/>
                * The provided BindingManager can be used to to resolve other referenced types.
                * @param mgr the binding manager
                * @param stream an output stream
                */
            public:
               virtual void genDeclaration(const BindingManager& mgr, ::std::ostream& stream) const = 0;

               /**
                * Generate the definition of this binding. A definition appears in the implementation file.
                * It is assumed that the includes for all dependent types have been emitted.<br/>
                * The provided BindingManager can be used to to resolve other referenced types.
                * @param mgr the binding manager
                * @param stream an output stream
                */
            public:
               virtual void genDefinition(const BindingManager& mgr, ::std::ostream& stream) const = 0;

               /**
                * Generate a statement to bind a string value to this type. This method is primarily used for
                * simple types.
                * @param stream a stream
                * @param instanceExpr an expression of a reference to this type.
                * @param valueExpr a value expression
                * @return true if the statement was generated, false otherwise
                */
            public:
               virtual bool genBindingStatement (::std::ostream& stream, const ::std::string& instanceExpr, const ::std::string& valueExpr) const throws() = 0;
         };
      }
   }
}

#endif
