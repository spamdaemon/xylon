#include <xylon/code/cpp/DefaultBindingFactory.h>
#include <xylon/code/cpp/AbstractBindingFactory.h>
#include <xylon/code/cpp/Namespace.h>
#include <xylon/code/cpp/Type.h>
#include <xylon/code/cpp/Attribute.h>
#include <xylon/code/cpp/BindingManager.h>
#include <xylon/code/cpp/AbstractScope.h>
#include <xylon/model/models.h>
#include <xylon/model/instructions.h>
#include <xylon/model/InstructionVisitor.h>

#include <timber/logging.h>

#include <iostream>
#include <ostream>
#include <vector>
#include <sstream>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::config;
using namespace ::timber::logging;
using namespace ::xylon::model;

namespace xylon {
   namespace code {
      namespace cpp {
         namespace {
            static const char XSD_BINDING[] = { "xsd-binding" };
            static const char XSD_TARGETNAMESPACE[] = { "xsd-targetNamespace" };
            static const char CPP_NAMESPACE[] = { "cpp-namespace" };

            static Log logger()
            {
               return Log("xylon.code.cpp.DefaultBindingFactory");
            }

            Pointer< Type> resolveBaseType(Reference< TypeModel> model, const BindingManager& manager)
            {
               if (model->baseType()) {
                  return manager.findTypeBinding(model->baseType().target());
               }
               else {
                  return Pointer< Type> ();
               }
            }

            string baseClassname(Reference< TypeModel> model, const BindingManager& manager)
            {
               Pointer< Type> bc = resolveBaseType(model, manager);
               if (bc) {
                  return bc->className();
               }
               //FIXME: I bet that this doesn't work with local elements; do we even want that?
               if (model->globalName().type() == GlobalName::ELEMENT) {
                  return "::xylon::rt::ElementObject";
               }
               if (model->globalName().type() == GlobalName::TYPE) {
                  return "::xylon::rt::TypeObject";
               }
               return "::xylon::rt::Object";
            }

            static ::std::string makeQName(const QName& qname)
            {
               ostringstream out;
               out << " ::xylon::rt::QName(";
               if (qname) {
                  if (qname.namespaceURI()) {
                     out << "\"" << qname.namespaceURI() << "\",";
                  }
                  out << "\"" << qname.name() << "\"";
               }
               out << ")";
               return out.str();
            }

            struct MarshalCodeGenerator : public InstructionVisitor
            {
                  MarshalCodeGenerator(ostream& stream, const BindingManager& bindings,
                        const Reference< TypeModel>& model) throws() :
                     _stream(stream), _bindings(bindings), _type(model)
                  {
                  }

                  ~MarshalCodeGenerator() throws()
                  {
                  }
                  void visitSequence(const Reference< ::xylon::model::instructions::Sequence>& obj)
                  {
                     if (obj->count() == 0) {
                        logger().info("Empty sequence found");
                        _stream << "// empty sequence" << endl;
                     }
                     for (int i = 0, sz = obj->count(); i < sz; ++i) {
                        obj->instruction(i)->accept(*this);
                     }
                  }

                  void visitStartWriteObject(const Reference< ::xylon::model::instructions::StartWriteObject>& obj)
                  {
                     if (obj->isStartElement()) {
                        _stream << "w.beginElement(" << makeQName(obj->name()) << ");" << endl;
                     }
                     else {
                        _stream << "w.beginAttribute(" << makeQName(obj->name()) << ");" << endl;
                     }
                  }

                  void visitEndWriteObject(const Reference< ::xylon::model::instructions::EndWriteObject>& obj)
                  {
                     if (obj->isEndElement()) {
                        _stream << "w.endElement();" << endl;
                     }
                     else {
                        _stream << "w.endAttribute();" << endl;
                     }
                  }

                  void visitWriteValue(const Reference< ::xylon::model::instructions::WriteValue>& obj)
                  {
                     _stream << "w.writeText(\"FIXME: need value\");" << endl;
                  }

                  void visitWriteAttribute(const Reference< ::xylon::model::instructions::WriteAttribute>& obj)
                  {
                     Reference< AttributeModel> attr = obj->attribute();
                     Reference< TypeModel> type = attr->type().target();
                     Reference< Attribute> attrB = _bindings.findAttributeBinding(attr);

                     QName expectedXsiType;
                     if (attr->attributeType() == AttributeModel::ELEMENT && type->globalName().type()
                           == GlobalName::TYPE) {
                        expectedXsiType = type->globalName().name();
                     }

                     string targetType = _bindings.findTypeBinding(type)->className();

                     _stream << "// attribute : " << attr->scope()->name() << "::" << attr->name() << " : "
                           << attr->type()->name() << endl;
                     _stream << "{" << endl;
                     _stream << "   struct X {" << endl;
                     _stream << "      X(::xylon::rt::Writer& xw) : xwriter(xw) {}" << endl;
                     _stream << "      void operator()(size_t i , const " << targetType << "& xobj) {" << endl;
                     bool needSpace = true;

                     if (obj->before()) {
                        needSpace = false;
                        _stream << "{ ::xylon::rt::Writer& w = xwriter;" << endl;
                        obj->before()->accept(*this);
                        _stream << "}" << endl;
                     }
                     if (type->isSimple()) {
                        _stream << "    if (i==0) {" << endl;
                        if (expectedXsiType) {
                           _stream << "          xobj.writeType(" << makeQName(expectedXsiType) << ",xwriter);" << endl;
                        }
                        else {
                           _stream << "          xobj.write(xwriter);" << endl;
                        }
                        _stream << "}" << endl;
                        _stream << "else {" << endl;
                        if (needSpace) {
                           _stream << "       xwriter.writeText(\" \");" << endl;
                        }
                        _stream << "          xobj.write(xwriter);" << endl;
                        _stream << "}" << endl;
                     }
                     else {
                        if (expectedXsiType) {
                           _stream << "          xobj.writeType(" << makeQName(expectedXsiType) << ",xwriter);" << endl;
                        }
                        else {
                           _stream << "          xobj.write(xwriter);" << endl;
                        }
                     }
                     if (obj->after()) {
                        _stream << "{ ::xylon::rt::Writer& w = xwriter;" << endl;
                        obj->after()->accept(*this);
                        _stream << "}" << endl;
                     }
                     _stream << "      }" << endl;
                     _stream << "      ::xylon::rt::Writer& xwriter;" << endl;
                     _stream << "   };" << endl;
                     _stream << "   X x(w);" << endl;
                     attrB->genEnumerationStatement(_stream, "*this", "x");
                     _stream << "}" << endl;
                  }

                  void visitWriteBaseType(const Reference< ::xylon::model::instructions::WriteBaseType>& obj)
                  {
                     // write the base type
                     Pointer< Type> bc = resolveBaseType(_type, _bindings);
                     if (bc) {
                        string bName = baseClassname(_type, _bindings);
                        _stream << bName << "::write(w);" << endl;
                     }
                  }

                  void visitMessage(const Reference< ::xylon::model::instructions::Message>& obj)
                  {
                     _stream << "::std::cerr << \"" << obj->message() << "\" << ::std::endl;" << endl;
                  }

                  /** An output stream */
               private:
                  ostream& _stream;

                  /** The bindings */
               private:
                  const BindingManager& _bindings;

                  /** The type */
               private:
                  Reference< TypeModel> _type;
            };

            struct AbstractAttribute : public virtual Attribute
            {
                  AbstractAttribute(const SharedRef< Configuration> cfg, Reference< AttributeModel> m,
                        Reference< Type> t, Reference< Type> encType) :
                     _model(m), _type(t), _scope(encType), _name(encType->addName("_" + m->name().string()))
                  {

                  }
                  ~AbstractAttribute() throws()
                  {
                  }

                  /** The name of this attribute */
               public:
                  string memberName() const throws()
                  {
                     return _name;
                  }

                  /**
                   * Get the attribute's name. Returns a name based on <tt>model->name()</tt>.
                   * @return the name of the attribute
                   */
               public:
                  string name() const throws()
                  {
                     return _name;
                  }

                  /**
                   * Get the attribute model.
                   * @return the attribute model
                   */
               public:
                  Reference< AttributeModel> model() const throws()
                  {
                     return _model;
                  }

                  /**
                   * Get attribute's type.
                   * @return the attribute type
                   */
               public:
                  Reference< Type> type() const throws()
                  {
                     return _type;
                  }

                  /**
                   * Get attribute's scope.
                   * @return the attribute's scope.
                   */
               public:
                  Reference< Type> scope() const throws()
                  {
                     return _scope;
                  }

                  /** The attribute model */
               private:
                  Reference< AttributeModel> _model;

                  /** The attribute's bound type */
               private:
                  Reference< Type> _type;

                  /** The enclosing type */
               private:
                  Reference< Type> _scope;

                  /** The name */
               private:
                  const ::std::string _name;
            };

            struct SimpleAttribute : public AbstractAttribute
            {
                  SimpleAttribute(const SharedRef< Configuration> cfg, Reference< AttributeModel> m,
                        Reference< Type> t, Reference< Type> encType) :
                     AbstractAttribute(cfg, m, t, encType)
                  {

                  }
                  ~SimpleAttribute() throws()
                  {
                  }

               public:
                  void genDefaultInitializer(::std::ostream& out) const
                  {
                     //                     out << memberName() << "(new " << type()->className() << "())";
                     out << memberName() << "()";
                  }

               public:
                  void genDestructor(::std::ostream& out) const
                  {
                     out << memberName() << "=nullptr;";
                  }

               public:
                  void genCopyInitializer(::std::ostream& out, const string& src) const
                  {
                     const string ref = '(' + src + ")." + memberName();
                     out << memberName() << "( " << ref << ".get() ?  " << ref << "->clone() : nullptr)";
                  }

               public:
                  void genCopyConstructor(ostream& out, const string& src) const
                  {
                     // nothing to generate
                  }
               public:
                  void genCopyStatement(::std::ostream& out, const ::std::string& src, const ::std::string& dest) const
                  {
                     const string srcRef = '(' + src + ")." + memberName();
                     const string destRef = '(' + dest + ")." + memberName();
                     out << "if (" << srcRef << ") {" << endl;
                     out << "   " << destRef << ".reset(" << srcRef << "->clone());" << endl;
                     out << "}" << endl;
                     out << "else {" << endl;
                     out << "   " << destRef << "=nullptr;" << endl;
                     out << "}" << endl << endl;
                  }

               public:
                  void genMethodDeclarations(::std::ostream& out) const
                  {
                     out << "/**" << endl;
                     out << " * Set the value of the " << name() << " attribute. " << endl;
                     out << " * @param newValue the new value of the attribute" << endl;
                     out << "*/" << endl;
                     out << "public:" << endl;
                     out << "void set" << name() << "(const " << type()->className() << "& newValue);" << endl;
                     out << endl;
                     out << "/**" << endl;
                     out << " * Get the value of the " << name() << " attribute. " << endl;
                     out << " * @return the current value of " << name() << endl;
                     out << "*/" << endl;
                     out << "public:" << endl;
                     out << "      " << type()->className() << "& get" << name() << "();" << endl;
                     out << "const " << type()->className() << "& get" << name() << "() const;" << endl;
                     out << "bool isSet" << name() << "() const ;" << endl;
                  }

                  void genVariableDefinition(::std::ostream& out) const
                  {
                     out << "/** Attribute:  " << name() << " */" << endl;
                     out << "private:" << endl;
                     out << "::std::unique_ptr< " << type()->className() << " > " << memberName() << ';' << endl;
                  }

                  void genMethodDefinitions(::std::ostream& out) const
                  {
                     out << "/* Methods of attribute " << name() << "*/" << endl;
                     out << "void " << scope()->className() << "::set" << name() << "(const " << type()->className()
                           << "& newValue)" << endl;
                     out << "{" << endl;
                     out << "   if (&newValue!=" << memberName() << ".get()) {" << endl;
                     out << "      " << memberName() << ".reset(newValue.clone());" << endl;
                     out << "   }" << endl;
                     out << "}" << endl;
                     out << endl;
                     out << "const " << type()->className() << "& " << scope()->className() << "::get" << name()
                           << "() const" << endl;
                     out << "{" << endl;
                     out << "   return *" << memberName() << ";" << endl;
                     out << "}" << endl;
                     out << "      " << type()->className() << "& " << scope()->className() << "::get" << name()
                           << "()" << endl;
                     out << "{" << endl;
                     out << "   return *" << memberName() << ";" << endl;
                     out << "}" << endl;
                     out << "bool " << scope()->className() << "::isSet" << name() << "() const" << endl;
                     out << "{" << endl;
                     out << "   return " << memberName() << "!= nullptr;" << endl;
                     out << "}" << endl;
                     out << endl;
                  }
                  string genGetter() const throws()
                  {
                     return string("get") + name() + "()";
                  }
                  string genSetter(const string& newValueExpr) const throws()
                  {
                     return string("set") + name() + "(" + newValueExpr + ")";
                  }
                  void genBindingStatement(::std::ostream& out, const ::std::string& obj, const ::std::string& value) const
                  {
                     out << "(" << obj << ")." << genSetter("*(" + value + ")") << ";" << endl;
                  }
                  void genEnumerationStatement(::std::ostream& out, const ::std::string& obj, const ::std::string& cb) const
                  {
                     out << "  if (isSet" << name() << "()) {" << endl;
                     out << "     (" << cb << ")(0, (" << obj << ")." << genGetter() << ");" << endl;
                     out << "  }" << endl;
                  }
            };

            struct MultiValuedAttribute : public AbstractAttribute
            {
                  MultiValuedAttribute(const SharedRef< Configuration> cfg, Reference< AttributeModel> m, Reference<
                        Type> t, Reference< Type> encType) :
                     AbstractAttribute(cfg, m, t, encType)
                  {

                  }
                  ~MultiValuedAttribute() throws()
                  {
                  }

               public:
                  void genDefaultInitializer(::std::ostream& out) const
                  {
                     // not needed
                  }

               public:
                  void genDestructor(::std::ostream& out) const
                  {
                     out << "clear" << name() << "();" << endl;
                  }

               public:
                  void genCopyInitializer(::std::ostream&, const string&) const
                  {
                     // NOT applicable
                  }

               public:
                  void genCopyConstructor(ostream& out, const string& src) const
                  {
                     const string srcRef = '(' + src + ")." + memberName();
                     out << "for (size_t i=0,sz=" << srcRef << ".size();i!=sz;++i) {" << endl;
                     out << "  " << memberName() << ".push_back(::std::unique_ptr< " << type()->className() << " >((" << srcRef << ")[i]->clone()));" << endl;
                     out << "}" << endl;
                  }

               public:
                  void genCopyStatement(::std::ostream& out, const ::std::string& src, const ::std::string& dest) const
                  {
                     const string srcRef = '(' + src + ")." + memberName();
                     const string destRef = '(' + dest + ")." + memberName();

                     out << "if (&" << srcRef << " != &" << destRef << ") {" << endl;
                     out << "   clear" << name() << "();" << endl;
                     out << "   for (size_t i=0,sz=" << srcRef << ".size();i!=sz;++i) {" << endl;
                     out << "    " << memberName() << ".push_back(::std::unique_ptr< " << type()->className() << " >((" << srcRef << ")[i]->clone()));" << endl;
                     out << "  }" << endl;
                     out << "}" << endl << endl;
                  }

               public:
                  void genMethodDeclarations(::std::ostream& out) const
                  {
                     out << "/**" << endl;
                     out << " * Set the value of the " << name() << " attribute. " << endl;
                     out << " * @param newValue the new value of the attribute" << endl;
                     out << " * @param pos the position to be set." << endl;
                     out << "*/" << endl;
                     out << "public:" << endl;
                     out << "void set" << name() << "(const " << type()->className()
                           << "& newValue, size_t pos);" << endl;
                     out << endl;
                     out << "/**" << endl;
                     out << " * Add a new value of the " << name() << " attribute. " << endl;
                     out << " * @param newValue the new value of the attribute" << endl;
                     out << "*/" << endl;
                     out << "public:" << endl;
                     out << "void add" << name() << "(const " << type()->className() << "& newValue);" << endl;
                     out << endl;
                     out << "/**" << endl;
                     out << " * Get the value of the " << name() << " attribute. " << endl;
                     out << " * @return the current value of " << name() << endl;
                     out << "*/" << endl;
                     out << "public:" << endl;
                     out << "const " << type()->className() << "& get" << name() << "(size_t pos) const;"
                           << endl;
                     out << "      " << type()->className() << "& get" << name() << "(size_t pos);" << endl;
                     out << "/**" << endl;
                     out << " * Get the number of values of the " << name() << " attribute. " << endl;
                     out << " * @return the number of values of " << name() << endl;
                     out << "*/" << endl;
                     out << "public:" << endl;
                     out << "size_t " << "num" << name() << "() const;" << endl;
                     out << "/**" << endl;
                     out << " * Clear the values of the " << name() << " attribute. " << endl;
                     out << "*/" << endl;
                     out << "private:" << endl;
                     out << "void clear" << name() << "();" << endl;
                  }

                  void genVariableDefinition(::std::ostream& out) const
                  {
                     out << "/** Attribute:  " << name() << " */" << endl;
                     out << "private:" << endl;
                     out << "::std::vector< ::std::unique_ptr< " << type()->className() << " > > " << memberName() << ';' << endl;
                  }

                  void genMethodDefinitions(::std::ostream& out) const
                  {
                     out << "/* Methods of attribute " << name() << "*/" << endl;
                     out << "void " << scope()->className() << "::set" << name() << "(const " << type()->className()
                           << "& newValue, size_t pos)" << endl;
                     out << "{" << endl;
                     out << "   ::std::unique_ptr< " << type()->className() << " > tmp(newValue.clone());" << endl;
                     out << "   const size_t sz = " << memberName() << ".size();" << endl;
                     out << "   if (pos == sz) {" << endl;
                     out << "   " << memberName() << ".push_back(::std::move(tmp));" << endl;
                     out << "   }" << endl;
                     out << "   else {" << endl;
                     out << "      // create a temporary; this may fail with an out_of_range exception" << endl;
                     out << "      ::std::unique_ptr< " << type()->className() << " > old(::std::move(" << memberName()
                           << ".at(pos)));" << endl;
                     out << "      " << memberName() << "[pos] = ::std::move(tmp);" << endl;
                     out << "   }" << endl;
                     out << "}" << endl;

                     out << "void " << scope()->className() << "::add" << name() << "(const " << type()->className()
                           << "& newValue)" << endl;
                     out << "{" << endl;
                     out << "   " << memberName() << ".push_back(::std::unique_ptr< " << type()->className() << ">(newValue.clone()));" << endl;
                     out << "}" << endl;
                     out << endl;
                     out << "const " << type()->className() << "& " << scope()->className() << "::get" << name()
                           << "(size_t pos) const" << endl;
                     out << "{" << endl;
                     out << "   return *(" << memberName() << ".at(pos));" << endl;
                     out << "}" << endl;
                     out << endl;
                     out << "      " << type()->className() << "& " << scope()->className() << "::get" << name()
                           << "(size_t pos)" << endl;
                     out << "{" << endl;
                     out << "   return *(" << memberName() << ".at(pos));" << endl;
                     out << "}" << endl;
                     out << endl;
                     out << "void " << scope()->className() << "::clear" << name() << "()" << endl;
                     out << "{" << endl;
                     out << "    " << memberName() << ".clear();" << endl;
                     out << "}" << endl;
                     out << "size_t " << scope()->className() << "::num" << name() << "() const" << endl;
                     out << "{" << endl;
                     out << "   return " << memberName() << ".size();" << endl;
                     out << "}" << endl;
                     out << endl;
                  }
                  string genGetter() const throws()
                  {
                     return string("get") + name() + "()";
                  }

                  void genEnumerationStatement(::std::ostream& out, const ::std::string& obj, const ::std::string& cb) const
                  {
                     out << "   {" << endl;
                     out << "      for (size_t i=0,sz=(" << obj << ").num" << name() << "();i!=sz;++i) {" << endl;
                     out << "         (" << cb << ")(i, (" << obj << ").get" << name() << "(i));" << endl;
                     out << "      }" << endl;
                     out << "   }" << endl;
                  }

                  string genSetter(const string& newValueExpr) const throws()
                  {
                     return string("set") + name() + "(" + newValueExpr + ")";
                  }
                  void genBindingStatement(::std::ostream& out, const ::std::string& obj, const ::std::string& value) const
                  {
                     out << "(" << obj << ").add" << name() << "(*(" + value + "));" << endl;
                  }

            };

            struct AbstractTypeModelBinding : public virtual Type, public AbstractScope
            {
                  AbstractTypeModelBinding(const SharedRef< Configuration> cfg, const Reference< TypeModel>& m,
                        const Pointer< Namespace>& encNamespace, const Pointer< Type>& encClass) :
                     _model(m), _enclosingNamespace(encNamespace), _enclosingClass(encClass)
                  {
                     assert((encClass || encNamespace) && "Type defined outside of any scope");
                     _name = m->name().string();

                     if (m->globalName().type() == GlobalName::TYPE) {
                        _name += "Type";
                        if (_name.find("TypeType") != string::npos) {
                           _name = m->name().string();
                        }
                     }

                     if (encClass) {
                        _name = encClass->addName(_name);
                     }
                     else {
                        _name = encNamespace->addName(_name);
                     }

                  }

                  ~AbstractTypeModelBinding() throws()
                  {
                  }

                  bool inScope(const string& name) const throws()
                  {
                     return Type::inScope(name) || AbstractScope::inScope(name);
                  }

                  string name() const throws()
                  {
                     return _name;
                  }

                  Pointer< Namespace> enclosingNamespace() const throws()
                  {
                     return _enclosingNamespace;
                  }

                  Pointer< Type> enclosingType() const throws()
                  {
                     return _enclosingClass;
                  }

                  bool genBindingStatement(ostream& stream, const string& instanceExpr, const string& valueExpr) const throws()
                  {
                     if (_model->isSimple()) {
                        return false;
                     }
                     else {
                        return false;
                     }
                  }

                  void genForwardDeclaration(ostream& stream) const
                  {
                     if (_enclosingNamespace) {
                        _enclosingNamespace->open(stream);
                        stream << "class " << _name << ';' << endl;
                        _enclosingNamespace->close(stream);
                     }
                  }

                  inline Reference< TypeModel> model() const throws()
                  {
                     return _model;
                  }

                  /**
                   * Generate the boilerplace declarations. These are
                   * # the local types
                   * # the default constructor
                   * # the copy constructor
                   * # the copy operator
                   * # the destruct
                   */
               protected:
                  void genBoilerPlateDeclarations(const BindingManager& mgr, ostream& stream) const
                  {
                     const string nm = name();
                     //FIXME: emit local type declarations
                     ::std::vector< Reference< TypeModel> > locals = model()->localTypes();
                     if (!locals.empty()) {
                        stream << " /** Local types */" << endl;
                        stream << " public:" << endl;
                        for (size_t i = 0; i < locals.size(); ++i) {
                           Reference< Type> b = mgr.findTypeBinding(locals[i]);
                           b->genDeclaration(mgr, stream);
                           stream << endl;
                        }
                     }

                     stream << " public:" << endl;
                     stream << " /** Default constructor */" << endl << "  " << nm << "();" << endl;
                     stream << " /** Copy constructor */" << endl << "  " << nm << "(const " << nm << "& src);" << endl;
                     stream << " /** Copy operator */" << endl << "  " << nm << "& operator=(const " << nm << "& src);"
                           << endl;
                     stream << "  /** Destructor */" << endl << "  ~" << nm << "();" << endl;
                     stream << "  /** A deep-clone function */" << endl;
                     stream << "  virtual " << nm << "* clone() const";
                     if (model()->isAbstract()) {
                        stream << " =0";
                     }
                     stream << ";" << endl;

                     if (model()->globalName().type() == GlobalName::ELEMENT) {
                        if (!model()->isAbstract()) {
                           stream << "  void write( ::xylon::rt::Writer& w) const;" << endl;
                        }
                        if (!model()->baseType() || model()->baseType()->globalName().type() != GlobalName::ELEMENT) {
                           stream
                                 << "virtual void writeElement(const ::xylon::rt::QName&, ::xylon::rt::Writer&) const;"
                                 << endl;
                        }
                     }
                     else {
                        stream << "  void write( ::xylon::rt::Writer& w) const;" << endl;
                        // create the writeType method if the current object is a global type
                        if (model()->globalName().type() == GlobalName::TYPE) {
                           stream << "virtual void writeType(const ::xylon::rt::QName&, ::xylon::rt::Writer&) const";

                           if (model()->isAbstract()) {
                              stream << " =0";
                           }
                           stream << ";" << endl;
                        }
                     }
                  }

                  /**
                   * Generate the attributes.
                   *
                   */
                  void genAttributeDeclarations(const BindingManager& mgr, ostream& stream) const
                  {
                     ::std::vector< Reference< AttributeModel> > locals = model()->attributes();
                     for (size_t i = 0; i < locals.size(); ++i) {

                        Reference< Attribute> b = mgr.findAttributeBinding(locals[i]);
                        stream << "/**" << endl;
                        stream << " * @name " << locals[i]->name() << endl;
                        stream << " * Methods and variables related to " << locals[i]->name() << endl;
                        stream << " * @{" << endl;
                        stream << "*/" << endl;

                        b->genMethodDeclarations(stream);
                        stream << "  /**@}*/" << endl << endl;
                        b->genVariableDefinition(stream);

                        stream << endl;
                     }
                  }

                  /**
                   * Open this enclosing type.
                   */
               protected:
                  string openEnclosingType(ostream& out) const
                  {
                     Pointer< Type> bnd = enclosingType();
                     string typeName = bnd->name();
                     while (true) {
                        if (bnd->enclosingNamespace()) {
                           bnd->enclosingNamespace()->open(out);
                           break;
                        }
                        bnd = bnd->enclosingType();
                        typeName = bnd->name() + "::" + typeName;
                     }
                     return typeName;
                  }

                  /**
                   * Close this type.
                   */
               protected:
                  void closeEnclosingType(ostream& out) const
                  {
                     Pointer< Type> bnd = enclosingType();
                     while (true) {
                        if (bnd->enclosingNamespace()) {
                           bnd->enclosingNamespace()->close(out);
                           break;
                        }
                        bnd = bnd->enclosingType();
                     }
                  }

               private:
                  ::std::string _name;

               private:
                  const Reference< TypeModel> _model;

               private:
                  Pointer< Namespace> _enclosingNamespace;
                  Pointer< Type> _enclosingClass;
            };

            struct StructBinding : public AbstractTypeModelBinding
            {
                  StructBinding(const SharedRef< Configuration> cfg, const Reference< TypeModel>& m, const Pointer<
                        Namespace>& encNamespace, const Pointer< Type>& encClass) :
                     AbstractTypeModelBinding(cfg, m, encNamespace, encClass)
                  {
                  }

                  ~StructBinding() throws()
                  {
                  }

                  void genDeclaration(const BindingManager& mgr, ostream& stream) const
                  {
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     const string nm = name();
                     const string baseName = baseClassname(model(), mgr);

                     stream << "class " << nm;
                     if (baseName.empty()) {
                        stream << " : public ::xylon::rt::Object";
                     }
                     else {
                        stream << " : public " << baseName;
                     }
                     stream << " {" << endl;
                     genBoilerPlateDeclarations(mgr, stream);
                     stream << " private:" << endl;

                     genAttributeDeclarations(mgr, stream);

                     stream << "};" << endl;
                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                  }

                  void genDefinition(const BindingManager& mgr, ostream& stream) const
                  {
                     const vector< Reference< AttributeModel> > attributes = model()->attributes();
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        b->genMethodDefinitions(stream);
                     }

                     string cn = name();
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     else if (enclosingType()) {
                        cn = openEnclosingType(stream) + "::" + cn;
                     }

                     const string nm = name();
                     const string bn = baseClassname(model(), mgr);

                     // default constructor
                     stream << cn << "::" << nm << "()" << endl;
                     char initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "()" << endl;
                        initializerChar = ',';
                     }

                     // initializer list
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        ostringstream out;
                        b->genDefaultInitializer(out);
                        string tmp = out.str();
                        if (!tmp.empty()) {
                           stream << initializerChar << ' ' << tmp << endl;
                           initializerChar = ',';
                        }
                        stream << endl;
                     }

                     stream << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // copy constructor
                     stream << cn << "::" << nm << "(const " << cn << "& src)" << endl;
                     initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "(src)" << endl;
                        initializerChar = ',';
                     }
                     // initializer list
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);

                        ostringstream out;
                        b->genCopyInitializer(out, "src");
                        string tmp = out.str();
                        if (!tmp.empty()) {
                           stream << initializerChar << ' ' << tmp << endl;
                           initializerChar = ',';
                        }
                     }
                     stream << "{" << endl;
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        b->genCopyConstructor(stream, "src");
                        stream << endl;
                     }
                     stream << "}" << endl;
                     stream << endl;

                     // copy operator
                     stream << cn << "& " << cn << "::operator=(const " << nm << "& src)" << endl << "{" << endl;
                     stream << "if (&src != this) {" << endl;
                     if (!bn.empty()) {
                        stream << bn << "::operator=(src);" << endl;
                     }
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        b->genCopyStatement(stream, "src", "*this");
                     }
                     stream << "}" << endl;
                     stream << " return *this;" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // destructor
                     stream << cn << "::~" << nm << "()" << endl << "{" << endl;
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        b->genDestructor(stream);
                        stream << endl;
                     }
                     stream << "}" << endl;
                     stream << endl;
                     // clone
                     if (!model()->isAbstract()) {
                        stream << cn << "* " << cn << "::clone() const" << endl;
                        stream << "{" << endl;
                        stream << "  return new " << cn << "(*this);" << endl;
                        stream << "}" << endl;
                        stream << endl;
                     }
                     // writer
                     if (model()->globalName().type() == GlobalName::ELEMENT) {
                        if (!model()->isAbstract()) {
                           stream << "void " << cn << "::write( ::xylon::rt::Writer& w) const { writeElement("
                                 << makeQName(model()->globalName().name()) << ",w); }" << endl;
                        }
                        if (!model()->baseType() || model()->baseType()->globalName().type() != GlobalName::ELEMENT) {
                           stream << "void " << cn
                                 << "::writeElement(const ::xylon::rt::QName& qname, ::xylon::rt::Writer& w) const"
                                 << endl;
                           stream << "{" << endl;
                           stream << "    w.beginElement(qname);" << endl;
                           Pointer< Instruction> wi = model()->writeInstruction();
                           if (wi) {
                              MarshalCodeGenerator marshaller(stream, mgr, model());
                              wi->accept(marshaller);
                           }
                           else {
                              stream << "// no write instruction" << endl;
                           }
                           stream << "    w.endElement();" << endl;
                           stream << "}" << endl;
                        }
                     }
                     else {
                        stream << "void " << cn << "::write( ::xylon::rt::Writer& w) const" << endl;
                        stream << "{" << endl;
                        Pointer< Instruction> wi = model()->writeInstruction();
                        if (wi) {
                           MarshalCodeGenerator marshaller(stream, mgr, model());
                           wi->accept(marshaller);
                        }
                        else {
                           stream << "// no write instruction" << endl;
                        }
                        stream << "}" << endl;

                        if (model()->globalName().type() == GlobalName::TYPE) {

                           if (!model()->isAbstract()) {
                              stream << "void " << cn
                                    << "::writeType(const ::xylon::rt::QName& expected, ::xylon::rt::Writer& w) const"
                                    << endl;
                              stream << "{" << endl;
                              stream << "    writeTypeImpl(expected," << makeQName(model()->globalName().name())
                                    << ",w);" << endl;
                              stream << "}" << endl;
                           }
                        }
                     }

                     stream << endl;

                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                     else if (enclosingType()) {
                        closeEnclosingType(stream);
                     }

                  }

            };

            struct AnySimpleTypeBinding : public AbstractTypeModelBinding
            {
                  AnySimpleTypeBinding(const SharedRef< Configuration> cfg, const Reference< TypeModel>& m,
                        const Pointer< Namespace>& encNamespace, const Pointer< Type>& encClass) :
                     AbstractTypeModelBinding(cfg, m, encNamespace, encClass)
                  {
                  }
                  ~AnySimpleTypeBinding() throws()
                  {
                  }

                  bool genBindingStatement(ostream& stream, const string& instanceExpr, const string& valueExpr) const throws()
                  {
                     stream << "(" << instanceExpr << ").set(" << valueExpr << ");" << endl;
                     return true;
                  }

                  void genDeclaration(const BindingManager& mgr, ostream& stream) const
                  {
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     const string nm = name();
                     const string base = baseClassname(model(), mgr);

                     stream << "class " << nm;
                     if (base.empty()) {
                        stream << " : public ::xylon::rt::Object";
                     }
                     else {
                        stream << " : public " << base;
                     }
                     stream << " {" << endl;
                     genBoilerPlateDeclarations(mgr, stream);
                     stream
                           << " // Ideally, get should be virtual and the subclass would construct the appropriate value"
                           << endl;
                     stream << " const ::std::string&  get () const { return _value; }" << endl;
                     stream << " virtual void set (const ::std::string& str);" << endl;
                     stream << " private:" << endl;
                     stream << "    ::std::string _value;" << endl;

                     stream << "};" << endl;
                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                  }

                  void genDefinition(const BindingManager& mgr, ostream& stream) const
                  {
                     const vector< Reference< AttributeModel> > attributes = model()->attributes();
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        b->genMethodDefinitions(stream);
                     }

                     string cn = name();
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     else if (enclosingType()) {
                        cn = openEnclosingType(stream) + "::" + cn;
                     }

                     const string nm = name();
                     const string bn = baseClassname(model(), mgr);

                     // default constructor
                     stream << cn << "::" << nm << "()" << endl;
                     char initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "()" << endl;
                        initializerChar = ',';
                     }

                     stream << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // copy constructor
                     stream << cn << "::" << nm << "(const " << cn << "& src)" << endl;
                     initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "(src)" << endl;
                        initializerChar = ',';
                     }
                     stream << initializerChar << " _value(src._value)" << endl;
                     stream << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // copy operator
                     stream << cn << "& " << cn << "::operator=(const " << nm << "& src)" << endl << "{" << endl;
                     stream << "if (&src != this) {" << endl;
                     if (!bn.empty()) {
                        stream << bn << "::operator=(src);" << endl;
                     }
                     stream << "_value = src._value;" << endl;
                     stream << "}" << endl;
                     stream << " return *this;" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // destructor
                     stream << cn << "::~" << nm << "()" << endl << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;
                     // clone
                     stream << cn << "* " << cn << "::clone() const" << endl;
                     stream << "{" << endl;
                     stream << "  return new " << cn << "(*this);" << endl;
                     stream << "}" << endl;
                     stream << endl;
                     // writer
                     stream << "void " << cn << "::write( ::xylon::rt::Writer& w) const" << endl;
                     stream << "{" << endl;
                     stream << "   // FIXME: not implemented yet" << endl;
                     stream << "}" << endl;

                     stream << "void " << cn << "::set(const ::std::string& v) { _value = v; }" << endl;

                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                     else if (enclosingType()) {
                        closeEnclosingType(stream);
                     }

                  }

            };

            struct SimpleBinding : public AbstractTypeModelBinding
            {
                  SimpleBinding(const SharedRef< Configuration> cfg, const Reference< TypeModel>& m, const Pointer<
                        Namespace>& encNamespace, const Pointer< Type>& encClass) :
                     AbstractTypeModelBinding(cfg, m, encNamespace, encClass)
                  {
                  }
                  ~SimpleBinding() throws()
                  {
                  }

                  bool genBindingStatement(ostream& stream, const string& instanceExpr, const string& valueExpr) const throws()
                  {
                     stream << "(" << instanceExpr << ").set(" << valueExpr << ");" << endl;
                     return true;
                  }

                  void genDeclaration(const BindingManager& mgr, ostream& stream) const
                  {
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     const string nm = name();
                     const string base = baseClassname(model(), mgr);

                     stream << "class " << nm;
                     if (base.empty()) {
                        stream << " : public ::xylon::rt::Object";
                     }
                     else {
                        stream << " : public " << base;
                     }
                     stream << " {" << endl;
                     genBoilerPlateDeclarations(mgr, stream);
                     stream << "public:" << endl;
                     stream << "   void set (const ::std::string& str);" << endl;
                     stream << "};" << endl;
                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                  }

                  void genDefinition(const BindingManager& mgr, ostream& stream) const
                  {
                     const vector< Reference< AttributeModel> > attributes = model()->attributes();
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        b->genMethodDefinitions(stream);
                     }

                     string cn = name();
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     else if (enclosingType()) {
                        cn = openEnclosingType(stream) + "::" + cn;
                     }

                     const string nm = name();
                     const string bn = baseClassname(model(), mgr);
                     // default constructor
                     stream << cn << "::" << nm << "()" << endl;
                     char initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "()" << endl;
                        initializerChar = ',';
                     }

                     stream << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // copy constructor
                     stream << cn << "::" << nm << "(const " << cn << "& src)" << endl;
                     initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "(src)" << endl;
                        initializerChar = ',';
                     }
                     stream << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // copy operator
                     stream << cn << "& " << cn << "::operator=(const " << nm << "& src)" << endl << "{" << endl;
                     stream << "if (&src != this) {" << endl;
                     if (!bn.empty()) {
                        stream << bn << "::operator=(src);" << endl;
                     }
                     stream << "}" << endl;
                     stream << " return *this;" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // destructor
                     stream << cn << "::~" << nm << "()" << endl << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;
                     // clone
                     stream << cn << "* " << cn << "::clone() const" << endl;
                     stream << "{" << endl;
                     stream << "  return new " << cn << "(*this);" << endl;
                     stream << "}" << endl;
                     stream << endl;
                     // writer
                     stream << "void " << cn << "::write( ::xylon::rt::Writer& w) const" << endl;
                     stream << "{" << endl;
                     stream << "   // FIXME: not implemented yet" << endl;
                     stream << "}" << endl;

                     stream << "void " << cn << "::set(const ::std::string& v) { " << endl;
                     Pointer< Type> base = resolveBaseType(model(), mgr);
                     if (base) {
                        base->genBindingStatement(stream, "*this", "v");
                     }
                     else {
                        stream << "//FIXME: what do ?" << endl;
                     }
                     stream << "}" << endl;

                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                     else if (enclosingType()) {
                        closeEnclosingType(stream);
                     }

                  }

            };

            struct ListBinding : public AbstractTypeModelBinding
            {
                  ListBinding(const SharedRef< Configuration> cfg, const Reference< ListTypeModel>& m, const Pointer<
                        Namespace>& encNamespace, const Pointer< Type>& encClass) :
                     AbstractTypeModelBinding(cfg, m, encNamespace, encClass)
                  {
                  }
                  ~ListBinding() throws()
                  {
                  }
                  void genDeclaration(const BindingManager& mgr, ostream& stream) const
                  {
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     Reference< ListTypeModel> typeModel(model());
                     const string nm = name();
                     const string base = baseClassname(model(), mgr);

                     const string tn = mgr.findTypeBinding(typeModel->type().target())->className();

                     stream << "/** List Model " << tn << " */" << endl;
                     stream << "class " << nm;
                     if (!base.empty()) {
                        stream << " : public " << base;
                     }
                     stream << " {" << endl;
                     genBoilerPlateDeclarations(mgr, stream);

                     stream << "   size_t size() const { return _list.size(); }" << endl;
                     stream << "   void add(const " << tn << "& item);" << endl;

                     stream << " private:" << endl;
                     stream << "   ::std::vector<  " << tn << " *> _list;" << endl;
                     stream << "};" << endl;
                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                  }
                  void genDefinition(const BindingManager& mgr, ostream& stream) const
                  {
                     const vector< Reference< AttributeModel> > attributes = model()->attributes();
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        b->genMethodDefinitions(stream);
                     }

                     string cn = name();
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     else if (enclosingType()) {
                        cn = openEnclosingType(stream) + "::" + cn;
                     }
                     Reference< ListTypeModel> typeModel(model());

                     const string nm = name();
                     const string bn = baseClassname(model(), mgr);
                     const string tn = mgr.findTypeBinding(typeModel->type().target())->className();

                     // default constructor
                     stream << cn << "::" << nm << "()" << endl;
                     char initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "()" << endl;
                        initializerChar = ',';
                     }

                     stream << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // copy constructor
                     stream << cn << "::" << nm << "(const " << cn << "& src)" << endl;
                     initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "(src)" << endl;
                        initializerChar = ',';
                     }
                     stream << "{" << endl;
                     stream << "for (size_t i=0,sz=src._list.size();i!=sz;++i) {" << endl;
                     stream << "   add(*src._list[i]);" << endl;
                     stream << "}" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // copy operator
                     stream << cn << "& " << cn << "::operator=(const " << nm << "& src)" << endl << "{" << endl;
                     stream << "if (&src != this) {" << endl;
                     if (!bn.empty()) {
                        stream << bn << "::operator=(src);" << endl;
                     }
                     stream << "for (size_t i=0,sz=_list.size();i!=sz;++i) {" << endl;
                     stream << "   delete _list[i];" << endl;
                     stream << "}" << endl;
                     stream << "_list.clear();" << endl;
                     stream << "for (size_t i=0,sz=src._list.size();i!=sz;++i) {" << endl;
                     stream << "   add(*src._list[i]);" << endl;
                     stream << "}" << endl;
                     stream << "}" << endl;
                     stream << " return *this;" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // destructor
                     stream << cn << "::~" << nm << "()" << endl << "{" << endl;
                     stream << "   for (size_t i=0,sz=_list.size();i!=sz;++i) {" << endl;
                     stream << "      delete _list[i];" << endl;
                     stream << "   }" << endl;
                     stream << "}" << endl;
                     stream << endl;
                     // clone
                     stream << cn << "* " << cn << "::clone() const" << endl;
                     stream << "{" << endl;
                     stream << "  return new " << cn << "(*this);" << endl;
                     stream << "}" << endl;
                     stream << endl;
                     // writer
                     stream << "void " << cn << "::write( ::xylon::rt::Writer& w) const" << endl;
                     stream << "{" << endl;
                     stream << "   // FIXME: not implemented yet" << endl;
                     stream << "}" << endl;

                     stream << "   void " << cn << "::add(const " << tn << "& item)" << endl;
                     stream << "   {" << endl;
                     stream << "       _list.push_back(item.clone());" << endl;
                     stream << "   }" << endl;

                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                     else if (enclosingType()) {
                        closeEnclosingType(stream);
                     }
                  }
            };

            struct UnionBinding : public AbstractTypeModelBinding
            {
                  UnionBinding(const SharedRef< Configuration> cfg, const Reference< UnionTypeModel>& m, const Pointer<
                        Namespace>& encNamespace, const Pointer< Type>& encClass) :
                     AbstractTypeModelBinding(cfg, m, encNamespace, encClass)
                  {
                  }
                  ~UnionBinding() throws()
                  {
                  }

                  void genDeclaration(const BindingManager& mgr, ostream& stream) const
                  {
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     Reference< UnionTypeModel> typeModel(model());
                     vector< ModelRef< TypeModel> > types = typeModel->types();

                     const string nm = name();
                     const string base = baseClassname(model(), mgr);

                     stream << "// UNION TYPE " << endl;
                     stream << "class " << nm;
                     if (!base.empty()) {
                        stream << " : public " << base;
                     }
                     stream << " {" << endl;
                     genBoilerPlateDeclarations(mgr, stream);

                     for (size_t i = 0; i < types.size(); ++i) {
                        const string tn = mgr.findTypeBinding(types[i].target())->className();
                        stream << "   void set" << i << "(const " << tn << "& v);" << endl;
                     }

                     stream << " private:" << endl;
                     for (size_t i = 0; i < types.size(); ++i) {
                        const string tn = mgr.findTypeBinding(types[i].target())->className();
                        stream << "   ::std::unique_ptr< " << tn << " > _t" << i << ";" << endl;
                     }
                     stream << "};" << endl;
                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                  }
                  void genDefinition(const BindingManager& mgr, ostream& stream) const
                  {
                     const vector< Reference< AttributeModel> > attributes = model()->attributes();
                     for (size_t i = 0; i < attributes.size(); ++i) {
                        Reference< Attribute> b = mgr.findAttributeBinding(attributes[i]);
                        b->genMethodDefinitions(stream);
                     }
                     Reference< UnionTypeModel> typeModel(model());
                     vector< ModelRef< TypeModel> > types = typeModel->types();

                     string cn = name();
                     if (enclosingNamespace()) {
                        enclosingNamespace()->open(stream);
                     }
                     else if (enclosingType()) {
                        cn = openEnclosingType(stream) + "::" + cn;
                     }

                     const string nm = name();
                     const string bn = baseClassname(model(), mgr);

                     // default constructor
                     stream << cn << "::" << nm << "()" << endl;
                     char initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "()" << endl;
                        initializerChar = ',';
                     }

                     stream << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // copy constructor
                     stream << cn << "::" << nm << "(const " << cn << "& src)" << endl;
                     initializerChar = ':';
                     if (!bn.empty()) {
                        stream << initializerChar << ' ' << bn << "(src)" << endl;
                        initializerChar = ',';
                     }
                     stream << "{" << endl;
                     for (size_t i = 0; i < types.size(); ++i) {
                        const string tn = mgr.findTypeBinding(types[i].target())->className();
                        stream << " _t" << i << ".reset(0);" << endl;
                        stream << " if (src._t" << i << ".get()) {" << endl;
                        stream << "   _t" << i << ".reset((*src._t" << i << ").clone());" << endl;
                        stream << " }" << endl;
                     }
                     stream << "}" << endl;
                     stream << endl;

                     // copy operator
                     stream << cn << "& " << cn << "::operator=(const " << nm << "& src)" << endl << "{" << endl;
                     stream << "if (&src != this) {" << endl;
                     if (!bn.empty()) {
                        stream << bn << "::operator=(src);" << endl;
                     }
                     for (size_t i = 0; i < types.size(); ++i) {
                        const string tn = mgr.findTypeBinding(types[i].target())->className();
                        stream << " _t" << i << ".reset(0);" << endl;
                        stream << " if (src._t" << i << ".get()) {" << endl;
                        stream << "   _t" << i << ".reset((*src._t" << i << ").clone());" << endl;
                        stream << "  }" << endl;
                     }

                     stream << "}" << endl;
                     stream << " return *this;" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     // destructor
                     stream << cn << "::~" << nm << "()" << endl << "{" << endl;
                     stream << "}" << endl;
                     stream << endl;
                     // clone
                     stream << cn << "* " << cn << "::clone() const" << endl;
                     stream << "{" << endl;
                     stream << "  return new " << cn << "(*this);" << endl;
                     stream << "}" << endl;
                     stream << endl;

                     for (size_t i = 0; i < types.size(); ++i) {
                        const string tn = mgr.findTypeBinding(types[i].target())->className();
                        stream << "   void " << cn << "::set" << i << "(const " << tn << "& v)" << endl;
                        stream << "   {" << endl;
                        for (size_t j = 0; j < i; ++j) {
                           stream << "_t" << j << ".reset(0);" << endl;
                        }
                        stream << "_t" << i << ".reset((v).clone());" << endl;
                        stream << "   }" << endl;
                     }

                     if (enclosingNamespace()) {
                        enclosingNamespace()->close(stream);
                     }
                     else if (enclosingType()) {
                        closeEnclosingType(stream);
                     }

                  }

            };

         }

         unique_ptr< BindingFactory> DefaultBindingFactory::create(const SharedRef< Configuration>& config) throws()

         {
            struct Impl : public AbstractBindingFactory
            {
                  Impl(SharedRef< Configuration> cfg) throws() :
                     _configuration(cfg)
                  {
                  }

                  ~Impl() throws()
                  {
                  }
                  ::std::vector< ::std::string> getIncludes() const throws()
                  {
                     return vector< string> ();
                  }

                  Pointer< Namespace> createNamespace(const Reference< NamespaceModel>& model) throws()
                  {
                     string res;
                     size_t n = _configuration->count(XSD_BINDING);
                     for (size_t i = 0; i < n; ++i) {
                        auto bnd = _configuration->getConfiguration(Configuration::Selector(
                              XSD_BINDING, i));

                        if (bnd->get(Configuration::Path(XSD_TARGETNAMESPACE))
                              == model->scopeName().namespaceURI().string()) {
                           res = bnd->get(Configuration::Path(CPP_NAMESPACE));
                           return Namespace::create(res);
                        }
                     }
                     return Pointer< Namespace> ();
                  }

                  Pointer< Type> createType(const Reference< TypeModel>& type,
                        const ::timber::Reference< Namespace>& ns) throws()
                  {
                     Pointer< Type> b;
                     Pointer< TypeModel> m = type.tryDynamicCast< UnionTypeModel> ();

                     if (m) {
                        return new UnionBinding(_configuration, m, ns, Pointer< Type> ());
                     }
                     m = type.tryDynamicCast< ListTypeModel> ();
                     if (m) {
                        return new ListBinding(_configuration, m, ns, Pointer< Type> ());
                     }
                     if (type->globalName().name() == QName::xsQName("anySimpleType")) {
                        return new AnySimpleTypeBinding(_configuration, type, ns, Pointer< Type> ());
                     }
                     else if (false && type->isSimple()) {
                        //FIXME: this doesn't really work
                        return new SimpleBinding(_configuration, type, ns, Pointer< Type> ());
                     }
                     else {
                        return new StructBinding(_configuration, type, ns, Pointer< Type> ());
                     }
                  }

                  Pointer< Type> createLocalType(const Reference< TypeModel>& type,
                        const ::timber::Reference< Type>& enc) throws()
                  {
                     Pointer< Type> b;
                     Pointer< TypeModel> m = type.tryDynamicCast< UnionTypeModel> ();

                     if (m) {
                        return new UnionBinding(_configuration, m, Pointer< Namespace> (), enc);
                     }
                     m = type.tryDynamicCast< ListTypeModel> ();
                     if (m) {
                        return new ListBinding(_configuration, m, Pointer< Namespace> (), enc);
                     }
                     if (false && type->isSimple()) {
                        //FIXME: this doesn't really work
                        return new SimpleBinding(_configuration, type, Pointer< Namespace> (), enc);
                     }
                     else {
                        return new StructBinding(_configuration, type, Pointer< Namespace> (), enc);
                     }
                  }

                  Pointer< Attribute> createAttribute(const Reference< AttributeModel>& model,
                        const ::timber::Reference< Type>& type, const Reference< Type>& enclosing) throws()
                  {
                     if (model->maxOccurs() == 1) {
                        return new SimpleAttribute(_configuration, model, type, enclosing);
                     }
                     else {
                        return new MultiValuedAttribute(_configuration, model, type, enclosing);
                     }
                  }

               private:
                  SharedRef< Configuration> _configuration;
            };

            BindingFactory* f = new Impl(config);
            return unique_ptr< BindingFactory> (f);
         }

      }

   }

}
