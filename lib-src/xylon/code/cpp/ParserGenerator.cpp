#include <xylon/code/cpp/ParserGenerator.h>
#include <xylon/code/cpp/BindingManager.h>
#include <xylon/code/cpp/Attribute.h>
#include <xylon/code/cpp/Type.h>
#include <xylon/code/cpp/Namespace.h>
#include <xylon/code/cpp/cpp.h>
#include <xylon/model/InstructionVisitor.h>
#include <xylon/model/models.h>
#include <xylon/model/instructions.h>

#include <canopy/fs/FileSystem.h>
#include <timber/logging.h>

#include <iostream>
#include <fstream>
#include <map>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::xylon::model;
using namespace ::xylon::model::instructions;

namespace xylon {
   namespace code {
      namespace cpp {
         namespace {
            static Log logger()
            {
               return Log("xylon.code.cpp.ParserGenerator");
            }

            static ::std::string createFileName(const ::std::string& dir, const ::std::string& fname)
            {
               canopy::fs::FileSystem fs;
               if (!fs.isAbsolutePath(fname) && !dir.empty()) {
                  return fs.joinPaths(dir, fname);
               }
               else {
                  return fname;
               }
            }

            static ::std::string makeQName(const QName& qname)
            {
               ostringstream out;
               out << " ::xylon::rt::QName(";
               if (qname) {
                  if (qname.namespaceURI()) {
                     out << "\"" << qname.namespaceURI() << "\",";
                  }
                  out << "\"" << qname.name() << "\"";
               }
               out << ")";
               return out.str();
            }

            struct BindListValue : public ::xylon::model::instructions::BuiltinInstruction
            {
                  BindListValue(Reference< ListTypeModel> xlistType) throws() :
                     _listType(xlistType)
                  {
                  }
                  ~BindListValue() throws()
                  {
                  }

                  Reference< ListTypeModel> listType() const throws()
                  {
                     return _listType;
                  }

                  Reference< TypeModel> itemType() const throws()
                  {
                     return _listType->type().target();
                  }

                  Reference< ListTypeModel> _listType;
            };

            struct ParserCodeGenerator : public InstructionVisitor
            {
                  ParserCodeGenerator(ostream& stream, const BindingManager& bindings) throws() :
                     _stream(stream), _bindings(bindings)
                  {
                  }

                  ~ParserCodeGenerator() throws()
                  {
                  }

                  /**
                   * Get the name of the specified instruction.
                   * @param i an instruction
                   * @return a name for the instruction
                   */
               public:
                  string instructionName(Reference< Instruction> i) const throws()
                  {
                     if (_visited.count(i) == 0) {
                        _unvisited.insert(i);
                     }
                     string& iname = _instructionNames[i];
                     if (iname.empty()) {
                        ostringstream out;
                        out << "pi" << _instructionNames.size();
                        iname = out.str();
                     }
                     return iname;
                  }

                  void visit(const ::timber::Pointer< Instruction>& e)
                  {
                     if (e) {
                        _visited.insert(e);
                        _unvisited.erase(e);
                        e->accept(*this);
                     }
                  }

                  /**
                   * Emit a call to an instruction. If the instruction has not been visited, then
                   * it is remembered as an unvisited instruction.
                   * @param e an instruction
                   */
                  string call(const ::timber::Pointer< Instruction>& e, const string& objectName = "ps")
                  {
                     if (e) {
                        {
                           Pointer< MoveCursor> i = e.tryDynamicCast< MoveCursor> ();
                           if (i) {
                              switch (i->direction()) {
                                 case MoveCursor::UP_ELEMENT:
                                    return objectName + ".cursor().up()";
                                 case MoveCursor::DOWN_ELEMENT:
                                    return objectName + ".cursor().down()";
                                 case MoveCursor::NEXT_ELEMENT:
                                    return objectName + ".cursor().next()";
                              }
                           }
                        }
                        {
                           Pointer< Error> i = e.tryDynamicCast< Error> ();
                           if (i) {
                              return "throw ::xylon::rt::ParseError(\"" + i->message() + "\")";
                           }
                        }

                        Pointer< Comment> c = e.tryDynamicCast< Comment> ();
                        if (c) {
                           string comment = c->comment();
                           if (!comment.empty()) {
                              comment = "/* " + comment + " */";
                           }
                           return comment;
                        }
                        else {
                           if (_visited.count(e) == 0) {
                              _unvisited.insert(e);
                           }
                           return instructionName(e) + "(" + objectName + ")";
                        }
                     }
                     else {
                        return "/* NO OP */";
                     }
                  }

                  void builtinParseAnySimpleType(const Reference< TypeModel>& model)
                  {
                     ::timber::Reference< Type> type = _bindings.findTypeBinding(model);
                     string cn = type ->className();

                     _stream << "{";
                     _stream << "   " << cn << " & top = ps.topObject< " << cn << " >();" << ::std::endl;
                     _stream << "   ::std::cerr << \"Setting simple type : " << cn
                           << " to \" << ps.activeValue() << ::std::endl;" << endl;

                     type->genBindingStatement(_stream, "top", "ps.activeValue().string()");

                     _stream << "}" << endl;
                  }

                  void builtinParseList(const Reference< ListTypeModel>& model)
                  {

                     Reference< ::xylon::model::Instruction> pi = ::xylon::model::instructions::CreateAndParse::create(
                           model->type());
                     Reference< BindListValue> bindValue(new BindListValue(model));
                     pi = ::xylon::model::instructions::Sequence::create(pi, bindValue);

                     // for now, need to instruction but not emit it, so it gets added
                     call(pi);

                     _stream << "   ::xylon::rt::GenericInstruction<Impl> instr(*this, &Impl::" << instructionName(pi)
                           << ") ;" << endl;
                     _stream << "   ps.activateValue();" << endl;
                     _stream << "   ps.activateListValues( instr);" << endl;
                     _stream << "   ps.activateElement(false);" << endl;
                  }

                  void builtinParseUnion(const Reference< UnionTypeModel>& model)
                  {
                     string cn = _bindings.findTypeBinding(model)->className();

                     vector< ModelRef< TypeModel> > types = model->types();

                     _stream << "  // make a partial copy of the state" << endl;
                     _stream << "  ps.activateValue();" << endl;

                     for (size_t i = 0; i < types.size(); ++i) {
                        const string tn = _bindings.findTypeBinding(types[i].target())->className();
                        _stream << "{" << endl;
                        _stream << "  // try to parse the type; if that fails pop the type on the stack" << endl;
                        _stream << "  bool success=false;" << endl;
                        _stream << "  try {" << endl;
                        _stream << "     ParserState cps(ps);" << endl;
                        _stream << "     " << call(CreateAndParse::create(types[i]), "cps") << ";" << endl;
                        _stream << "     ::std::unique_ptr< " << tn << ">value(cps.popObject< " << tn << " >());" << endl;
                        _stream
                              << "     if (!cps.isEmpty()) { throw ::xylon::rt::ParserError(\"Error parsing state\");"
                              << endl;
                        _stream << "     ps.topObject< " << cn << ">().set_" << i << "(*value);" << endl;
                        _stream << "     ps.move(cps, ::xylon::rt::ParserState::APPEND_STACK);" << endl;
                        _stream << "      return;" << endl;
                        _stream << "  } catch (...) {" << ::std::endl;
                        _stream << "  }" << ::std::endl;
                        _stream << "}" << endl;
                     }
                     _stream << "     throw ::xylon::rt::ParseError(\"Could not parse union type\");" << endl;
                  }

                  void visitBuiltin(const ::timber::Reference< ::xylon::model::instructions::BuiltinInstruction>& obj)
                  {
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     {
                        Pointer< BindListValue> i = obj.tryDynamicCast< BindListValue> ();
                        if (i) {
                           string itemType = _bindings.findTypeBinding(i->itemType())->className();
                           string listType = _bindings.findTypeBinding(i->listType())->className();
                           _stream << "      ::std::unique_ptr< " << itemType << ">value(ps.popObject< " << itemType
                                 << " >());" << endl;
                           _stream
                                 << "      ::std::cerr << \"Adding value to builtin list type \" << ps._value << ::std::endl;"
                                 << endl;
                           _stream << "      ps.topObject< " << listType << ">().add(*value);" << endl;
                        }
                     }
                     _stream << "}" << endl;
                  }

                  void visitBindElement(const ::timber::Reference< ::xylon::model::instructions::BindElement>& obj)
                  {
                     _stream << "// ->BindElement" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;

                     _stream << "     ::xylon::rt::AnyElement& any = ps.topObject< ::xylon::rt::AnyElement>();" << endl;
                     _stream << "     any.setElement(ps.cursor().clone());" << endl;
                     _stream << "}" << endl;
                  }

                  void visitBindAttribute(const ::timber::Reference< ::xylon::model::instructions::BindAttribute>& obj)
                  {
                     _stream << "// ->BindAttribute " << obj->boundModel()->name() << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;

                     Reference< AttributeModel> m = obj->boundModel();
                     Reference< Attribute> attr = _bindings.findAttributeBinding(m);

                     // get the type name
                     Reference< Type> attrType = attr->type();
                     string cn = attrType->className();
                     string encName = _bindings.findTypeBinding(m->scope().target())->className();

                     _stream << "::std::unique_ptr< " << cn << " > value=ps.popObject< " << cn << " >();" << endl;
                     _stream << encName << "& obj = ps.topObject< " << encName << " >();" << endl;

                     attr->genBindingStatement(_stream, "obj", "::std::move(value)");

                     _stream << "}" << endl;
                  }

                  void visitBind(const ::timber::Reference< ::xylon::model::instructions::Bind>& obj)
                  {
                     _stream << "// ->Bind" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream
                           << "   ::std::unique_ptr< ::xylon::rt::ElementObject> obj = ps.popObject< ::xylon::rt::ElementObject>();"
                           << endl;
                     _stream << "   ::xylon::rt::AnyElement& any = ps.topObject< ::xylon::rt::AnyElement>();" << endl;
                     _stream << "   any.setObject(::std::move(obj));" << endl;
                     _stream << "}" << endl;
                  }

                  void visitIterateList(const ::timber::Reference< ::xylon::model::instructions::IterateList>& obj)
                  {
                     _stream << "// ->IterateList" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     // need to ensure that the body gets emitted
                     call(obj->body());
                     _stream << "   ::xylon::rt::GenericInstruction<Impl> instr(*this, &Impl::" << instructionName(
                           obj->body()) << ") ;" << endl;
                     _stream << "   ps.activateListValues(instr);" << endl;
                     _stream << "   ps.activateElement(false);" << endl;
                     _stream << "}" << endl;
                  }
                  void visitNextElement(const ::timber::Reference< ::xylon::model::instructions::MoveCursor>& obj)
                  {
                     _stream << "// ->NextElement" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   ps.cursor().next();" << endl;
                     _stream << "}" << endl;

                  }

                  void visitError(const ::timber::Reference< ::xylon::model::instructions::Error>& obj)
                  {
                     _stream << "// ->Error" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;

                     _stream << "   ::std::cerr << \"Parse error : " << obj->message() << "\" <<  ::std::endl;" << endl;
                     _stream << "   //assert(\"Parser Error\"==0);" << endl;
                     _stream << "   throw ::xylon::rt::ParseError(\"" << obj->message() << "\");" << endl;
                     _stream << "}" << endl;
                  }

                  void visitMessage(const ::timber::Reference< ::xylon::model::instructions::Message>& obj)
                  {
                     _stream << "// ->Message : " << obj->message() << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     //TODO: not yet implemented
                     _stream << "}" << endl;
                  }

                  void visitForeachAttribute(
                        const ::timber::Reference< ::xylon::model::instructions::ForeachAttribute>& obj)
                  {
                     _stream << "// ->ForeachAttribute" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   typedef ::xylon::rt::ElementCursor::Attributes Attributes;" << endl;
                     _stream << "   const Attributes& attrs = ps.cursor().attributes();" << endl;
                     _stream << "     for (Attributes::const_iterator i=attrs.begin();i!=attrs.end();++i) {" << endl;
                     _stream << "        ps.activateAttribute(i->first);" << endl;
                     _stream << "        ::std::cerr << \"Activate attribute \"  << i->first << \" : \" << i->second;"
                           << endl;
                     _stream << "        " << call(obj->body()) << ";" << endl;
                     _stream << "     }" << endl;
                     _stream << "     ps.activateElement(false);" << endl;
                     _stream << "}" << endl;
                  }

                  void visitLoop(const ::timber::Reference< ::xylon::model::instructions::Loop>& obj)
                  {
                     _stream << "// ->Loop " << obj->occurs().min() << " -> " << obj->occurs().max() << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     call(obj->body());
                     _stream << "   ::xylon::rt::GenericInstruction<Impl> instr(*this,&Impl::" << instructionName(
                           obj->body()) << ");" << endl;
                     _stream << "   ps.loop(instr, (size_t)" << obj->occurs().min() << "ull, (size_t)"
                           << obj->occurs().max() << "ull);" << endl;

                     _stream << "}" << endl;
                  }

                  void visitNewInstance(const ::timber::Reference< ::xylon::model::instructions::NewInstance>& obj)
                  {
                     string cn = _bindings.findTypeBinding(obj->type().target())->className();
                     ModelRef< TypeModel> t = obj->type();

                     _stream << "// ->NewInstance " << cn << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;

                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;

                     if (t->isGlobal()) {
                        LogEntry(logger()).info() << "NewInstance " << t->globalName() << doLog;
                        if (t->globalName().type() == GlobalName::ELEMENT && t->globalName().name() == QName::xsQName(
                              "any")) {
                           //FIXME: allow object creation
                           _stream << " ::xylon::rt::ParserState::ObjectPtr obj(new ::xylon::rt::AnyElement());"
                                 << endl;
                        }
                        else if (t->globalName().type() != GlobalName::TYPE) {
                           ::std::cerr << t->globalName() << " : ";
                           assert("NewInstance can only be applied to a global that is a complex or simple type" == 0);
                           throw runtime_error(
                                 "NewInstance can only be applied to a global that is a complex or simple type");
                        }
                        else {
                           _stream << " ::xylon::rt::ParserState::sParserState::ObjectPtr obj = ps.schema().findType("
                                 << makeQName(t->globalName().name()) << ").newInstance();" << endl;
                        }
                        _stream << " ps.pushObject< ::xylon::rt::Object>(::std::move(obj));" << endl;
                     }
                     else {
                        _stream << "  ::std::unique_ptr< " << cn << " > obj(new " << cn << "());" << endl;
                        _stream << "  ps.pushObject< " << cn << " >(::std::move(obj));" << endl;
                     }
                     _stream << "}" << endl;
                  }

                  void visitParseInstance(const ::timber::Reference< ::xylon::model::instructions::ParseInstance>& obj)
                  {
                     ModelRef< TypeModel> t = obj->type();

                     _stream << "// ->ParseInstance " << t->globalName() << "  global? " << t->isGlobal()
                           << "  simple? " << t->isSimple() << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     // we need to figure out if we're parsing a simple type or a complex type
                     // for now, we're
                     if (t->isGlobal()) {
                        if (t->globalName().type() != GlobalName::TYPE) {
                           ::std::cerr << t->globalName() << " : ";
                           assert("ParseInstance can only be applied to a global that is a complex or simple type" == 0);
                           throw runtime_error(
                                 "ParseInstance can only be applied to a global that is a complex or simple type");
                        }
                        _stream << "ps.schema().findType(" << makeQName(t->globalName().name()) << ").parse(ps);"
                              << endl;
                     }
                     else if (t.target().tryDynamicCast< ListTypeModel> ()) {
                        builtinParseList(t.castTarget< ListTypeModel> ());
                     }
                     else if (t.target().tryDynamicCast< UnionTypeModel> ()) {
                        builtinParseUnion(t.castTarget< UnionTypeModel> ());
                     }
                     else if (t->parseInstruction()) {
                        _stream << "   " << call(t->parseInstruction()) << ";" << endl;
                     }
                     else if (t->isGlobal()) {
                        throw ::std::runtime_error("Do not know how to parse global type : "
                              + t->globalName().string().string());
                     }
                     else {
                        throw ::std::runtime_error("Do not know how to parse local type : " + t->name().string());

                     }
                     _stream << "}" << endl;
                  }

                  void visitParseNewInstance(
                        const ::timber::Reference< ::xylon::model::instructions::ParseNewInstance>& obj)
                  {
                     assert(obj->type()->isSimple() && "ParseNewInstance is not supported for non-simple types, yet");

                     string cn = _bindings.findTypeBinding(obj->type().target())->className();

                     _stream << "// ->ParseNewInstance " << cn << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     if (obj->type()->isGlobal()) {
                        _stream << "  if (ps.tryPushType(" << makeQName(obj->type()->globalName().name()) << ")) {"
                              << endl;
                        _stream << "     " << call(obj->ifTrue()) << ";" << endl;
                        _stream << "  } else {" << endl;
                        _stream << "     " << call(obj->ifFalse()) << ";" << endl;
                        _stream << "  }" << endl;
                     }
                     else {
                        _stream << "::xylon::rt::ParserState bak(ps, false);" << endl;
                        _stream << "try {" << endl;
                        _stream << "   " << call(CreateAndParse::create(obj->type()), "bak") << ";" << endl;
                        _stream << "}" << endl;
                        _stream << "catch (const ::xylon::rt::ParseError& e) {" << endl;
                        _stream << "     " << call(obj->ifFalse()) << ";" << endl;
                        _stream << "     return;";
                        _stream << "}" << endl;
                        // do this outside the try-catch block, because we need to throw
                        // a ParseError if we cannot move
                        _stream << "ps.move(bak, ::xylon::rt::ParserState::APPEND_STACK);" << endl;
                        _stream << "     " << call(obj->ifTrue()) << ";" << endl;
                     }
                     _stream << "}" << endl;
                  }

                  void visitOnXsiType(const ::timber::Reference< ::xylon::model::instructions::OnXsiType>& obj)
                  {
                     _stream << "// ->OnXsiType" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   ps.pushXsiType(" << makeQName(obj->expectedType()->globalName().name()) << ");"
                           << endl;
                     _stream << "}" << endl;

                  }

                  void visitOnAll(const ::timber::Reference< ::xylon::model::instructions::OnAll>& obj)
                  {
                     _stream << "// ->OnAll" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;

                     ostringstream elmts;
                     ostringstream found;
                     ostringstream index;
                     ostringstream calls;

                     size_t nElements = 0;
                     ::xylon::model::instructions::OnAll::Candidates required = obj->required();
                     for (size_t i = 0; i < required.size(); ++i) {
                        found << "false,";

                        const ::xylon::model::instructions::OnAll::Candidate& names = required[i];
                        for (::xylon::model::instructions::OnAll::Candidate::const_iterator j = names.begin(); j
                              != names.end(); ++j) {
                           calls << "&Impl::" << instructionName(j->second) << ',';
                           elmts << makeQName(j->first) << "," << endl;
                           ++nElements;
                           index << i << ',';
                        }
                     }

                     ::xylon::model::instructions::OnAll::Candidates optional = obj->optional();
                     for (size_t i = 0; i < optional.size(); ++i) {
                        found << "false,";
                        const ::xylon::model::instructions::OnAll::Candidate& names = optional[i];
                        for (::xylon::model::instructions::OnAll::Candidate::const_iterator j = names.begin(); j
                              != names.end(); ++j) {
                           calls << "&Impl::" << instructionName(j->second) << ',';
                           elmts << makeQName(j->first) << "," << endl;
                           ++nElements;
                           index << (i + required.size()) << ',';
                        }
                     }

                     _stream << "   typedef void (Impl::*PFUNC)( ::xylon::rt::ParserState&) const;" << endl;
                     _stream << "    static const ::xylon::rt::QName ELEMENTS[] = { " << endl << elmts.str() << "};"
                           << endl;
                     _stream << "    static PFUNC functions[] = { " << endl << calls.str() << "};" << endl;
                     _stream << "   size_t nRequired = " << required.size() << ';' << endl;
                     _stream << "   bool found[] = { " << found.str() << "};" << endl;
                     _stream << "   size_t index[] = { " << index.str() << "};" << endl;
                     _stream << endl;
                     _stream << "   while(true) {" << endl;
                     _stream << "      size_t count = ps.cursor().elementCount();" << endl;
                     _stream << "      if (ps.cursor().isValid()) {" << endl;
                     _stream << "         for (size_t i=0;i<" << nElements << ";++i) {" << endl;
                     _stream << "            if (ps.testElement(ELEMENTS[i])) {" << endl;
                     _stream << "               if (found[index[i]]) {" << endl;
                     _stream << "                  break;" << endl;
                     _stream << "                  //throw ::xylon::rt::ParseError(\"Duplicate element \"+qname);"
                           << endl;
                     _stream << "               }" << endl;
                     _stream << "               found[index[i]] = true;" << endl;
                     _stream << "               if (index[i] < " << required.size() << ") {" << endl;
                     _stream << "                  --nRequired;" << endl;
                     _stream << "               }" << endl;
                     _stream << "               (this->*functions[i])(ps);" << endl;
                     _stream << "               break;" << endl;
                     _stream << "            }" << endl;
                     _stream << "         }" << endl;
                     _stream << "      }" << endl;
                     _stream << "      if (count==ps.cursor().elementCount()) {" << endl;
                     _stream << "         if (nRequired==0) {" << endl;
                     _stream << "            break;" << endl;
                     _stream << "         }" << endl;
                     _stream << "         else {" << endl;
                     _stream
                           << "            throw ::xylon::rt::ParseError(\"Not all required elements have been found\");"
                           << endl;
                     _stream << "         }" << endl;
                     _stream << "      }" << endl;
                     _stream << "   }" << endl;
                     _stream << "}" << endl;
                  }

                  void visitOnAttribute(const ::timber::Reference< ::xylon::model::instructions::OnAttribute>& obj)
                  {
                     QName qname(obj->attribute());
                     _stream << "// ->OnAttribute " << qname << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   if (ps.activeAttribute() == " << makeQName(qname) << ") {" << endl;
                     _stream << "         " << call(obj->ifTrue()) << ";" << endl;
                     _stream << "   } else {" << endl;
                     _stream << "         " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "   }" << endl;
                     _stream << "}" << endl;
                  }

                  void visitOnAttributeNamespace(const ::timber::Reference<
                        ::xylon::model::instructions::OnAttributeNamespace>& obj)
                  {
                     _stream << "// ->OnAttributeNamespace" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   ::xylon::rt::String nsURI = ps.activeAttribute().namespaceURI(); " << endl;
                     _stream << "   if (false" << endl;
                     set< String> ns = obj->namespaces();
                     for (set< String>::const_iterator i = ns.begin(); i != ns.end(); ++i) {
                        if (*i) {
                           _stream << " || nsURI==\"" << *i << "\"" << endl;
                        }
                        else {
                           _stream << " || (!nsURI)" << endl;
                        }
                     }
                     _stream << ") {" << endl;
                     _stream << "      " << call(obj->ifTrue()) << ";" << endl;
                     _stream << "}" << endl;
                     _stream << "else { " << endl;
                     _stream << "         " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "}" << endl;
                     _stream << "}" << endl;
                  }

                  void visitOnElement(const ::timber::Reference< ::xylon::model::instructions::OnElement>& obj)
                  {
                     map< QName, Reference< Instruction> > candidates = obj->matchedElements();
                     string var = "var_" + instructionName(obj);
                     _stream << "// ->OnElement" << endl;

                     // create a local class which initializes its fields
                     if (candidates.size() == 1) {
                        _stream << "struct " << instructionName(obj) << "_cl {" << endl;
                        _stream << "   " << instructionName(obj) << "_cl() throw() : _name(" << makeQName(
                              candidates.begin()->first) << ") {}" << endl;
                        _stream << "   const ::xylon::rt::QName _name;" << endl;
                        _stream << "};" << endl;
                        _stream << "const " << instructionName(obj) << "_cl " << var << ";" << endl;
                     }
                     else if (candidates.size() > 1) {
                        _stream << "struct " << instructionName(obj) << "_cl {" << endl;
                        _stream << "   " << instructionName(obj) << "_cl() throw() {" << endl;
                        for (map< QName, Reference< Instruction> >::const_iterator i = candidates.begin(); i
                              != candidates.end(); ++i) {
                           _stream << " _names.push_back(" << makeQName(i->first) << ");" << endl;
                           _stream << " _instructions.push_back(&Impl::" << instructionName(i->second) << ");" << endl;
                        }

                        _stream << "   };" << endl;
                        _stream << "   ::std::vector< ::xylon::rt::QName> _names;" << endl;
                        _stream << "   ::std::vector< InstructionPtr > _instructions;" << endl;
                        _stream << "};" << endl;
                        _stream << "const " << instructionName(obj) << "_cl " << var << ";" << endl;
                     }

                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     // if the set is empty we can skip everything and return
                     if (candidates.empty()) {
                        _stream << "   " << call(obj->ifFalse()) << ";" << endl;
                        _stream << "   // no elements expected" << endl;
                        _stream << "}" << endl;
                        return;
                     }
                     if (candidates.size() == 1) {
                        _stream << "   if (ps.cursor().isValid() && ";
                        if (obj->isIgnoreSubstitutions()) {
                           _stream << "(ps.cursor().qname() == " << var << "._name)";
                        }
                        else {
                           _stream << "ps.testElement(" << var << "._name)";
                        }
                        _stream << ") {" << endl;

                        _stream << "     " << call(candidates.begin()->second) << ";" << endl;
                        _stream << "   } else {" << endl;
                        _stream << "     " << call(obj->ifFalse()) << ";" << endl;
                        _stream << "   }" << endl;
                        _stream << "}" << endl;
                        return;
                     }
                     _stream << "   if (!ps.cursor().isValid()) {" << endl;
                     _stream << "     " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "     return;" << endl;
                     _stream << "   }" << endl;
                     if (obj->isIgnoreSubstitutions()) {
                        _stream << "   size_t i =  ps.findElementIgnoreSubstitutions(&" << var << "._names[0],"
                              << candidates.size() << ");" << endl;
                     }
                     else {
                        _stream << "   size_t i =  ps.findElement(&" << var << "._names[0]," << candidates.size()
                              << ");" << endl;
                     }
                     _stream << "   if (i < " << candidates.size() << ") {" << endl;
                     _stream << "       (this->*(" << var << "._instructions[i]))(ps);" << endl;
                     _stream << "   }" << endl;
                     _stream << "   else {" << endl;
                     _stream << "      " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "   }" << endl;
#if 0
                     _stream << "   const ::xylon::rt::QName qname = ps.cursor().qname();" << endl;

                     _stream << "   ::std::cerr << \"OnElement " << instructionName(obj)
                     << " : \" << qname << ::std::endl;" << endl;
                     _stream << "if(false) {}" << endl;
                     for (map< QName, Reference< Instruction> >::const_iterator i = candidates.begin(); i
                           != candidates.end(); ++i) {
                        _stream << "else if (qname == " << makeQName(i->first) << ") { " << call(i->second) << "; }"
                        << endl;
                     }
                     _stream << "  else { " << call(obj->ifFalse()) << "; }" << endl;
#endif
#if 0
                     set< QName> names = obj->elements();
                     _stream << "   if (false " << endl;
                     for (set< QName>::const_iterator i = names.begin(); i != names.end(); ++i) {
                        _stream << "  ||    (qname == " << makeQName(*i) << ")" << endl;
                     }
                     _stream << "   ) {" << endl;
                     _stream << "      " << call(obj->ifTrue()) << ";" << endl;
                     _stream << "   }" << endl;
                     _stream << "   else {" << endl;
                     _stream << "      " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "   }" << endl;
#endif
                     _stream << "}" << endl;
                  }

                  void visitOnElementNamespace(const ::timber::Reference<
                        ::xylon::model::instructions::OnElementNamespace>& obj)
                  {
                     _stream << "// ->OnElementNamespace" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;

                     _stream << "   if (!ps.cursor().isValid()) {" << endl;
                     _stream << "      " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "      return;" << endl;
                     _stream << "   }" << endl;
                     _stream << "   ::xylon::rt::String nsURI = ps.cursor().namespaceURI();" << endl;
                     _stream << "   if (false " << endl;
                     set< String> namespaces = obj->namespaces();
                     for (set< String>::const_iterator i = namespaces.begin(); i != namespaces.end(); ++i) {
                        if (*i) {
                           _stream << "  ||    (nsURI == \"" << i->string() << "\")" << endl;
                        }
                        else {
                           _stream << "  ||    (!nsURI)" << endl;
                        }
                     }
                     _stream << "   ) {" << endl;
                     _stream << "      " << call(obj->ifTrue()) << ";" << endl;
                     _stream << "   }" << endl;
                     _stream << "   else {" << endl;
                     _stream << "      " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "   }" << endl;

                     _stream << "}" << endl;
                  }

                  void visitOnValidElement(
                        const ::timber::Reference< ::xylon::model::instructions::OnValidElement>& obj)
                  {
                     _stream << "// ->OnValidElement" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   if (!ps.cursor().isValid()) {" << endl;
                     _stream << "      " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "   }";

                     if (obj->requireKnownElement()) {
                        _stream << "    else if (!ps.isKnownElement()) {" << endl;
                        _stream << "      " << call(obj->ifFalse()) << ";" << endl;
                        _stream << "   }";
                     }

                     _stream << "   else {" << endl;
                     _stream << "      " << call(obj->ifTrue()) << ";" << endl;
                     _stream << "   }" << endl;
                     _stream << "}" << endl;
                  }

                  void visitActivateAttribute(const ::timber::Reference<
                        ::xylon::model::instructions::ActivateAttribute>& obj)
                  {
                     const QName qname = obj->attribute();
                     string ns = "::timber::w3c::xml::dom::String()";
                     if (qname.namespaceURI()) {
                        ns = '"';
                        ns += qname.namespaceURI().string();
                        ns += '"';
                     }
                     _stream << "// ->ActivateAttribute " << qname << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   if (ps.activateAttribute(" << makeQName(qname) << ")) {" << endl;
                     _stream << "      " << call(obj->ifTrue()) << ";" << endl;
                     _stream << "      ps.activateElement(false);" << endl;
                     _stream << "   }" << endl;
                     _stream << "   else {" << endl;
                     _stream << "      " << call(obj->ifFalse()) << ";" << endl;
                     _stream << "   }" << endl;
                     _stream << "}" << endl;
                  }

                  void visitActivateElement(
                        const ::timber::Reference< ::xylon::model::instructions::ActivateElement>& obj)
                  {
                     _stream << "// ->ActivateElement" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   ps.activateElement(true);" << endl;
                     _stream << "}" << endl;
                  }

                  void visitActivateValue(const ::timber::Reference< ::xylon::model::instructions::ActivateValue>& obj)
                  {
                     _stream << "// ->ActivateValue" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     _stream << "   ps.activateValue();" << endl;
                     _stream << "}" << endl;
                  }

                  void visitSequence(const ::timber::Reference< ::xylon::model::instructions::Sequence>& obj)
                  {
                     _stream << "// ->Sequence" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     for (size_t i = 0, sz = obj->count(); i < sz; ++i) {
                        _stream << "   " << call(obj->instruction(i)) << ";" << endl;
                     }
                     _stream << "}" << endl;
                  }

                  void visitAbstractInstruction(const ::timber::Reference<
                        ::xylon::model::instructions::AbstractInstruction>& obj)
                  {
                     _stream << "// ->AbstractInstruction" << endl;
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;
                     Pointer< ::xylon::model::Instruction> i = obj->instruction();
                     if (i) {
                        _stream << "   " << call(i) << ";" << endl;
                     }
                     _stream << "}" << endl;
                  }

                  void visitCreateAndParse(
                        const ::timber::Reference< ::xylon::model::instructions::CreateAndParse>& obj)
                  {
                     if (obj->hasType()) {
                        _stream << "// ->CreateAndParse " << obj->type()->globalName() << endl;
                     }
                     else {
                        _stream << "// ->CreateAndParse element on the stack" << endl;
                     }
                     _stream << "void " << instructionName(obj) << "( ::xylon::rt::ParserState& ps ) const" << endl;
                     _stream << "{" << endl;
                     _stream << "   ::std::cerr << __LINE__ << \":TRACE " << instructionName(obj)
                           << "\"  << ::std::endl;" << endl;

                     if (!obj->hasType()) {
                        _stream << "  ps.pushElement();" << endl;
                     }
                     else if (obj->type()->isGlobal()) {

                        GlobalName name = obj->type()->globalName();

                        switch (name.type()) {
                           case GlobalName::ELEMENT:
                              _stream << "  ps.pushElement(" << makeQName(name.name()) << ");" << endl;
                              break;
                           case GlobalName::GROUP:
                              _stream << "  ps.pushGroup(" << makeQName(name.name()) << ");" << endl;
                              break;
                           case GlobalName::ATTRIBUTE:
                              _stream << "  ps.pushAttribute(" << makeQName(name.name()) << ");" << endl;
                              break;
                           case GlobalName::ATTRIBUTE_GROUP:
                              _stream << "  ps.pushAttributeGroup(" << makeQName(name.name()) << ");" << endl;
                              break;
                           case GlobalName::TYPE:
                              _stream << "  ps.pushType(" << makeQName(name.name()) << ");" << endl;
                              break;
                           default:
                              break;
                        }
                     }
                     else {
                        _stream << "    " << call(NewInstance::create(obj->type())) << ";" << endl;
                        _stream << "    " << call(ParseInstance::create(obj->type())) << ";" << endl;
                     }
                     _stream << "}" << endl;
                  }

                  /** The current variable holding the current element. */
               private:
                  ::std::string _element;

                  /** The next value */
               private:
                  size_t _nextValue;

                  /** An output stream */
               private:
                  ostream& _stream;

                  /** The bindings */
               private:
                  const BindingManager& _bindings;

                  /** A set of instructions; this is used to count them */
               public:
                  mutable ::std::map< Reference< Instruction> , string> _instructionNames;

                  /** A instructions that must still be visited */
               public:
                  mutable ::std::set< Reference< Instruction> > _unvisited;

                  /** A instructions that have been visited */
               public:
                  mutable ::std::set< Reference< Instruction> > _visited;
            };
         }

         ParserGenerator::ParserGenerator() throws()
         {
         }
         ParserGenerator::~ParserGenerator() throws()
         {
         }

         namespace {

            static void generateParseFunction(const BindingManager& mgr, const Reference< TypeModel> &model,
                  ostream& out)
            {
               // emit the parse instruction for this type
               ParserCodeGenerator gen(out, mgr);
               gen.visit(model->parseInstruction());
               while (!gen._unvisited.empty()) {
                  gen.visit(*gen._unvisited.begin());
               }

               string cn = mgr.findTypeBinding(model)->className();

               out << "void parse(::xylon::rt::ParserState& ps) const {" << endl;
               out << "   ps.pushObject(newInstance());" << endl;
               out << "   " << gen.call(model->parseInstruction()) << ";" << endl;
               out << "}" << endl;
            }

            static void generateElementImpl(const BindingManager& mgr, const Reference< TypeModel> & model,
                  ostream& out)
            {

               out << "::xylon::rt::QName substitutionGroup () const { return ";
               Pointer< TypeModel> base = model->baseType().target();
               if (base) {
                  out << makeQName(base->globalName().name());
               }
               else {
                  out << makeQName(QName());
               }
               out << "; }" << endl;

               // emit the parse instruction for this type
               ParserCodeGenerator gen(out, mgr);
               gen.visit(model->parseInstruction());
               while (!gen._unvisited.empty()) {
                  gen.visit(*gen._unvisited.begin());
               }

               string cn = mgr.findTypeBinding(model)->className();

               out << "void parse(::xylon::rt::ParserState& ps) const  {" << endl;
               out << "   " << gen.call(model->parseInstruction()) << ";" << endl;
               out << "}" << endl;
               out << "bool isAbstract() const throw() { return " << (model->isAbstract() ? "true" : "false") << "; }"
                     << endl;
            }

            static void generateGroupImpl(const BindingManager& mgr, const Reference< TypeModel> & model, ostream& out)
            {
               generateParseFunction(mgr, model, out);
            }
            static void generateAttributeImpl(const BindingManager& mgr, const Reference< TypeModel> & model,
                  ostream& out)
            {
               generateParseFunction(mgr, model, out);

            }
            static void generateAttributeGroupImpl(const BindingManager& mgr, const Reference< TypeModel> & model,
                  ostream& out)
            {
               generateParseFunction(mgr, model, out);

            }
            static void generateTypeImpl(const BindingManager& mgr, const Reference< TypeModel> & model, ostream& out)
            {
               out << "::xylon::rt::QName baseType() const throw () { return ";
               Pointer< TypeModel> base = model->baseType().target();
               if (base) {
                  out << makeQName(base->globalName().name());
               }
               else {
                  out << makeQName(QName());
               }
               out << "; }" << endl;

               string cn = mgr.findTypeBinding(model)->className();

               ParserCodeGenerator gen(out, mgr);
               gen.visit(model->parseInstruction());
               while (!gen._unvisited.empty()) {
                  gen.visit(*gen._unvisited.begin());
               }
               out << "void parse(::xylon::rt::ParserState& ps) const {" << endl;

               // get the type from the schema manager
               if (model->globalName().name().namespaceURI() == QName::XSD_NAMESPACE_URI) {
                  if (model->globalName().type() == GlobalName::TYPE) {
                     if (model->isSimple()) {
                        out << "   ps.activateValue();" << endl;
                        gen.builtinParseAnySimpleType(model);
                     }
                     else if (model->parseInstruction()) {
                        out << "   " << gen.call(model->parseInstruction()) << ";" << endl;
                     }
                  }
                  else if (model->globalName().name().name() == "anySimpleType") {
                     out << "   ps.activateValue();" << endl;
                     gen.builtinParseAnySimpleType(model);
                  }
                  else if (model->parseInstruction()) {
                     out << "   " << gen.call(model->parseInstruction()) << ";" << endl;
                  }
               }
               else if (model.tryDynamicCast< ListTypeModel> ()) {
                  gen.builtinParseList(model);
               }
               else if (model.tryDynamicCast< UnionTypeModel> ()) {
                  gen.builtinParseUnion(model);
               }
               else if (model->parseInstruction()) {
                  out << "   " << gen.call(model->parseInstruction()) << ";" << endl;
               }
               else {
                  throw runtime_error("Do not know how to parse local type : " + model->name().string());
               }
               out << "}" << endl;

               out << "bool isAbstract() const throw() { return " << (model->isAbstract() ? "true" : "false") << "; }"
                     << endl;
            }
         }

         ::std::unique_ptr< ParserGenerator> ParserGenerator::create() throws()
         {
            struct Impl : public ParserGenerator
            {
                  Impl() throws()
                  {
                  }
                  ~Impl() throws()
                  {
                  }

                  string generateDeclaration(const BindingManager& mgr, const string& createMethod, const Reference<
                        TypeModel>& model, ostream& out)
                  {

                     if (!model->isGlobal()) {
                        LogEntry(logger()).severe() << "Model is not global : " << model->globalName() << doLog;
                     }
                     assert(model->isGlobal());
                     if (model->globalName().name().namespaceURI() == QName::XSD_NAMESPACE_URI) {
                        return "";
                     }
                     string cn = mgr.findTypeBinding(model)->className();
                     ::timber::Pointer< Namespace> ns = mgr.findTypeBinding(model)->enclosingNamespace();
                     if (ns) {
                        //    ns->open(out);
                     }
                     string nsName = ""; // ns->getNamespace()+"::";

                     string name = mgr.findTypeBinding(model)->name();
                     string res;
                     switch (model->globalName().type()) {
                        case GlobalName::ELEMENT:
                           out << "::std::unique_ptr< ::xylon::rt::Element>" << createMethod << "();" << endl;
                           res = "addElement(" + nsName + createMethod + "())";
                           break;
                        case GlobalName::GROUP:
                           out << "::std::unique_ptr< ::xylon::rt::Group>" << createMethod << "();" << endl;
                           res = "addGroup(" + nsName + createMethod + "())";
                           break;
                        case GlobalName::ATTRIBUTE:
                           out << "::std::unique_ptr< ::xylon::rt::Attribute>" << createMethod << "();" << endl;
                           res = "addAttribute(" + nsName + createMethod + "())";
                           break;
                        case GlobalName::ATTRIBUTE_GROUP:
                           out << "::std::unique_ptr< ::xylon::rt::AttributeGroup>" << createMethod << "();" << endl;
                           res = "addAttributeGroup(" + nsName + createMethod + "())";
                           break;
                        case GlobalName::TYPE:
                           out << "::std::unique_ptr< ::xylon::rt::Type> createType_" << name << "();" << endl;
                           res = "addType(" + nsName + createMethod + "())";
                           break;
                        default:
                           break;
                     };
                     if (ns) {
                        //       ns->close(out);
                     }
                     return res;
                  }

                  string getType(const Reference< TypeModel> & model) const throw ()
                  {
                     string type;
                     switch (model->globalName().type()) {
                        case GlobalName::ELEMENT:
                           type = "Element";
                           break;
                        case GlobalName::GROUP:
                           type = "Group";
                           assert(!model->isAbstract() && "Types for groups cannot be abstract");
                           break;
                        case GlobalName::ATTRIBUTE:
                           type = "Attribute";
                           assert(!model->isAbstract() && "Types for attributes cannot be abstract");
                           break;
                        case GlobalName::ATTRIBUTE_GROUP:
                           type = "AttributeGroup";
                           assert(!model->isAbstract() && "Types for attribute groups cannot be abstract");
                           break;
                        case GlobalName::TYPE:
                           type = "Type";
                           break;
                        default:
                           break;
                     };
                     return type;

                  }
                  void generateDefinition(const BindingManager& mgr, const string& typeName,
                        const string& createMethod, const Reference< TypeModel> & model, ostream& out)
                  {
                     assert(model->isGlobal());
                     if (model->globalName().name().namespaceURI() == QName::XSD_NAMESPACE_URI) {
                        return;
                     }

                     string cn = mgr.findTypeBinding(model)->className();
                     ::timber::Pointer< Namespace> ns = mgr.findTypeBinding(model)->enclosingNamespace();
                     string type = getType(model);
                     if (type.empty()) {
                        return;
                     }

                     if (ns) {
                        //       ns->open(out);
                     }

                     out << "    struct " << typeName << " : public ::xylon::rt::" << type << " {" << endl;
                     out << "       const ::xylon::rt::QName _name;" << endl;
                     out << "       typedef " << typeName << " Impl;" << endl;
                     out << "       typedef void (" << typeName
                           << "::*InstructionPtr)(::xylon::rt::ParserState&) const;" << endl;
                     out << "       " << typeName << "() throw() : _name(" << makeQName(model->globalName().name())
                           << ") {}" << endl;
                     out << "      ~" << typeName << "() throw() {}" << endl;
                     out << "      ::std::unique_ptr< ::xylon::rt::" << type << "> clone() const throw() {" << endl;
                     out << "          return ::std::unique_ptr< ::xylon::rt::" << type << ">(new Impl());" << endl;
                     out << "      }" << ::std::endl;
                     out << "       ::xylon::rt::QName qname() const  throw() { return _name; }" << endl;
                     // FIXME: what about abstract types?
                     string except = model->isAbstract() ? "::xylon::rt::AbstractTypeException" : "";

                     out << "       ::xylon::rt::" << type << "::ObjectPtr newInstance() const throw(" << except
                           << ") {";
                     if (model->isAbstract()) {
                        out << " throw " << except << "(_name);" << endl;
                     }
                     else {
                        out << " return ::xylon::rt::" << type << "::ObjectPtr(new " << cn << "());";
                     }
                     out << " }" << endl;

                     out << "        void marshal(const ::xylon::rt::Object& obj, ::xylon::rt::Writer& writer) const"
                           << endl;
                     out << "        {" << endl;
                     out << "             const " << cn << "& impl = dynamic_cast<const " << cn << "& >(obj);" << endl;
                     //FIXME                     out << "             const " << cn << "& impl = dynamic_cast<const " << cn << " >(obj);" << endl;
                     out << "        }" << endl;

                     switch (model->globalName().type()) {
                        case GlobalName::ELEMENT:
                           generateElementImpl(mgr, model, out);
                           break;
                        case GlobalName::GROUP:
                           generateGroupImpl(mgr, model, out);
                           break;
                        case GlobalName::ATTRIBUTE:
                           generateAttributeImpl(mgr, model, out);
                           break;
                        case GlobalName::ATTRIBUTE_GROUP:
                           generateAttributeGroupImpl(mgr, model, out);
                           break;
                        case GlobalName::TYPE:
                           generateTypeImpl(mgr, model, out);
                           break;
                        default:
                           assert(false);

                     }
                     out << "    };" << endl;
                     out << "::std::unique_ptr< ::xylon::rt::" << type << "> " << createMethod << "()" << endl;
                     out << "{" << endl;
                     out << "    return ::std::unique_ptr< ::xylon::rt::" << type << ">(new " << typeName << "());"
                           << endl;
                     out << "}" << endl;
                     if (ns) {
                        //        ns->close(out);
                     }
                  }

                  void generateDeclaration(const BindingManager& mgr, const Specification& spec, const vector<
                        Reference< TypeModel> >& models)
                  {
                     bool found = false;
                     for (size_t i = 0; !found && i != models.size(); ++i) {
                        GlobalName gn = models[i]->globalName();
                        if (gn && spec.targetNamespaces.count(gn.name().namespaceURI()) != 0) {
                           found = true;
                        }
                     }
                     if (!found) {
                        return;
                     }

                     ::canopy::fs::FileSystem fs;
                     string dirname = fs.directoryName(spec.headerFilename);
                     if (!dirname.empty()) {
                        try {
                           fs.mkdirs(dirname);
                        }
                        catch (const ::std::exception& e) {
                           throw ::std::runtime_error("Could not create directory :" + dirname);
                        }
                     }
                     ofstream out(createFileName(spec.destination, spec.headerFilename).c_str());
                     // open the namespaces
                     vector< string> names = splitFullname(spec.fullName);

                     string guard = spec.headerGuard;
                     if (guard.empty()) {
                        for (size_t i = 0; i < names.size(); ++i) {
                           guard += '_';
                           guard += names[i];
                        }
                        guard += "_H";
                     }

                     out << "#ifndef " << guard << endl;
                     out << "#define " << guard << endl;
                     out << "#include <xylon/rt/AbstractBasicSchema.h>" << endl;

                     if (names.empty()) {
                        names.push_back("MySchema");
                     }
                     for (size_t i = 0; i < names.size() - 1; ++i) {
                        out << "namespace " << names[i] << "{" << endl;
                     }
                     string className = *names.rbegin();

                     out << "class " << className << " : public ::xylon::rt::AbstractBasicSchema {" << endl;
                     out << "    " << className << "(const " << className << "&);" << endl;
                     out << "    " << className << "&operator=(const " << className << "&);" << endl;
                     out << "  public:" << endl;
                     out << "    " << className << "() throw();" << endl;
                     out << "   ~" << className << "() throw();" << endl;
                     out << endl;

                     out << "};" << endl;
                     for (size_t i = 0; i < names.size() - 1; ++i) {
                        out << "}" << endl;
                     }
                     out << "#endif" << endl;
                  }

                  void generateDefinition(const BindingManager& mgr, const Specification& spec, const vector<
                        Reference< TypeModel> >& models)
                  {
                     bool found = false;
                     for (size_t i = 0; !found && i != models.size(); ++i) {
                        GlobalName gn = models[i]->globalName();
                        if (gn && spec.targetNamespaces.count(gn.name().namespaceURI()) != 0) {
                           found = true;
                        }
                     }
                     if (!found) {
                        return;
                     }

                     ::canopy::fs::FileSystem fs;
                     string dirname = fs.directoryName(spec.implFilename);
                     if (!dirname.empty()) {
                        try {
                           fs.mkdirs(dirname);
                        }
                        catch (const ::std::exception& e) {
                           throw ::std::runtime_error("Could not create directory :" + dirname);
                        }
                     }
                     ofstream out(createFileName(spec.destination, spec.implFilename).c_str());
                     out << "#include <" << spec.headerFilename << ">" << endl;
                     out << "#include <xylon/rt/ParserState.h>" << endl;
                     out << "#include <xylon/rt/Attribute.h>" << endl;
                     out << "#include <xylon/rt/AttributeGroup.h>" << endl;
                     out << "#include <xylon/rt/Element.h>" << endl;
                     out << "#include <xylon/rt/Group.h>" << endl;
                     out << "#include <xylon/rt/Type.h>" << endl;
                     out << "#include <xylon/rt/ElementCursor.h>" << endl;
                     out << "#include <xylon/rt/GenericInstruction.h>" << endl;
                     out << "#include <xylon/rt/XSDSchema.h>" << endl;
                     out << "#include <xylon/rt/ElementCursor.h>" << endl;
                     out << "#include <xylon/rt/Writer.h>" << endl;

                     for (set< string>::const_iterator i = spec.schemaFilenames.begin(); i
                           != spec.schemaFilenames.end(); ++i) {
                        out << "#include <" << *i << ">" << endl;
                     }

                     // open the namespaces
                     vector< string> names = splitFullname(spec.fullName);
                     if (names.empty()) {
                        names.push_back("MySchema");
                     }
                     for (size_t i = 0; i < names.size() - 1; ++i) {
                        out << "namespace " << names[i] << "{" << endl;
                     }
                     string className = *names.rbegin();

                     vector< string> calls;

                     out << "namespace {" << endl;

                     set< string> mappedNames;

                     for (size_t i = 0; i != models.size(); ++i) {
                        GlobalName gn = models[i]->globalName();
                        if (gn && spec.targetNamespaces.count(gn.name().namespaceURI()) != 0) {
                           string type = getType(models[i]);
                           string typeName = type + "_" + mgr.findTypeBinding(models[i])->name() + "_cl";
                           size_t cnt = 1;
                           while (mappedNames.count(typeName) != 0) {
                              ostringstream xout;
                              xout << type << '_' << mgr.findTypeBinding(models[i])->name() << "_" << cnt++ << "_cl";
                              typeName = xout.str();
                           }
                           mappedNames.insert(typeName);
                           string createMethod = "create_" + typeName;

                           string call = generateDeclaration(mgr, createMethod, models[i], out);
                           calls.push_back(call);
                           generateDefinition(mgr, typeName, createMethod, models[i], out);
                        }
                     }
                     out << "}" << endl;

                     out << "   " << endl;
                     out << "   " << className << "::" << className << "()  throw()" << endl;
                     out << "   : ::xylon::rt::AbstractBasicSchema(::xylon::rt::XSDSchema())" << endl;
                     out << "   {" << endl;
                     for (size_t i = 0; i < calls.size(); ++i) {
                        string call = calls[i];
                        if (!call.empty()) {
                           out << "      " << call << ";" << endl;
                        }
                     }
                     out << "   }" << endl;
                     out << "   " << className << "::~" << className << "()  throw()" << endl;
                     out << "   {}" << endl;
                     for (size_t i = 0; i < names.size() - 1; ++i) {
                        out << "}" << endl;
                     }

                  }

            };
            ParserGenerator* impl = new Impl();
            return ::std::unique_ptr< ParserGenerator>(impl);
         }

      }
   }
}
