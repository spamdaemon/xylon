#ifndef _XYLON_CODE_H
#define _XYLON_CODE_H

namespace xylon {

   /**
    * The code namespace provides various mappings of a schema model into
    * code in different languages. The generated code is able to not only represent
    * a schema, but can parse a schema into classes and write objects as XML conforming
    * to the XML Schema definition.
    */
   namespace code {

   }
}
#endif
