#ifndef _XYLON_H
#define _XYLON_H

#ifndef _TIMBER_W3C_XML_XMLSTRING_H
#include <timber/w3c/xml/XMLString.h>
#endif

namespace xylon {

  /** The string type */
  typedef ::timber::w3c::xml::XMLString String;
}

#endif
