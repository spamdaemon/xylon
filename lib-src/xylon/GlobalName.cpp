#include <xylon/GlobalName.h>
#include <iostream>

namespace xylon {

   GlobalName::GlobalName() throws() :
      _type(UNDEFINED) // it doesn't matter what the type name is
   {
   }

   GlobalName::GlobalName(const QName& n, Type t) throws() :
      _name(n), _type(t)
   {
      assert(((n && t != UNDEFINED) || (!n && t == UNDEFINED)) && "Not a valid global type");
   }

   GlobalName::GlobalName(const GlobalName& src) throws() :
      _name(src._name), _type(src._type)
   {
   }

   GlobalName& GlobalName::operator=(const GlobalName& src) throws()
   {
      _name = src._name;
      _type = src._type;
      return *this;
   }

   GlobalName::~GlobalName() throws()
   {
   }

   bool GlobalName::operator==(const GlobalName& r) const throws()
   {
      return _type == r._type && _name == r._name;
   }

   bool GlobalName::operator<(const GlobalName& r) const throws()
   {
      if (_type < r._type) {
         return true;
      }
      if (_type > r._type) {
         return false;
      }
      return _name < r._name;
   }

   String GlobalName::string() const throws()
   {
      String res(_name.string());
      switch (type()) {
         case ::xylon::GlobalName::ATTRIBUTE:
            res += "{ATTRIBUTE}";
            break;
         case ::xylon::GlobalName::ATTRIBUTE_GROUP:
            res += "{ATTRIBUTE_GROUP}";
            break;
         case ::xylon::GlobalName::ELEMENT:
            res += "{ELEMENT}";
            break;
         case ::xylon::GlobalName::GROUP:
            res += "{GROUP}";
            break;
         case ::xylon::GlobalName::KEY:
            res += "{KEY}";
            break;
         case ::xylon::GlobalName::TYPE:
            res += "{TYPE}";
            break;
         case ::xylon::GlobalName::UNDEFINED:
            res = "{{invalid}}";
            break;
      };
      return res;
   }
}

::std::ostream& operator<<(::std::ostream& out, const ::xylon::GlobalName& name)
{
   if (!name) {
      out << "{{invalid}}";
   }
   else {
      out << name.name();
      switch (name.type()) {
         case ::xylon::GlobalName::ATTRIBUTE:
            out << "{ATTRIBUTE}";
            break;
         case ::xylon::GlobalName::ATTRIBUTE_GROUP:
            out << "{ATTRIBUTE_GROUP}";
            break;
         case ::xylon::GlobalName::ELEMENT:
            out << "{ELEMENT}";
            break;
         case ::xylon::GlobalName::GROUP:
            out << "{GROUP}";
            break;
         case ::xylon::GlobalName::KEY:
            out << "{KEY}";
            break;
         case ::xylon::GlobalName::TYPE:
            out << "{TYPE}";
            break;
      };
   }
   return out;
}
