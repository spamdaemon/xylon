#ifndef _XYLON_MODELFACTORY_H
#define _XYLON_MODELFACTORY_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace xylon {
   class XsdSchema;
   class XsdSchemaModel;
   class BindingProvider;

   /**
    * The ModelFactory is a singleton class which provides access to all bindings
    * known to the application. The BindingManger's primary task is to create
    * bindings between node that can appear just below the schema node and models
    * of the code that will be generated.
    */
   class ModelFactory
   {

         /** Default constructor */
      protected:
         ModelFactory() throws();

         /** Destructor */
      public:
         virtual ~ModelFactory() throws() = 0;

         /**
          * Create an instance of a binding manager.
          * @return a binding manager
          */
      public:
         static ::std::unique_ptr< ModelFactory> create() throws();

         /**
          * Bind the specified schema. This binds the top-level nodes in the schema, if they have not
          * already been bound.
          * @param schema the schema to be bound
          * @return true if the schema was bound, false if it was already bound
          * @throws ::std::exception if the binding could not be established
          */
      public:
         virtual ::timber::Reference< XsdSchemaModel>
               createModel(const ::timber::Reference< ::xylon::XsdSchema>& schema) throws (::std::exception) = 0;

         /**
          * Register a new model factory. This factory is added to the end of a list of model factory.
          * @param gen the model factory for the schema.
          * @see #setDefaultFactory()
          */
      public:
         virtual void registerBindingProvider(::std::unique_ptr< BindingProvider> gen) throws() = 0;

         /**
          * Set the default model factory. This method must be called before the first model is generated, otherwise
          * a default factory is used and this method cannot be called again.
          * @param gen the default model factory
          * @return true if the model factory was set, false otherwise
          */
      public:
         virtual bool setDefaultBindingProvider(::std::unique_ptr< BindingProvider> gen) throws() = 0;
   };
}
#endif
