#ifndef _XYLON_QNAME_H
#define _XYLON_QNAME_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _XYLON_H
#include <xylon/xylon.h>
#endif

#include <iosfwd>

namespace xylon {
  /**
   * A qname consists of a namespace uri and a local tag name. The prefix
   * of a node is not considered. 
   */
  class QName {

    /** The XSD namespace URI */
  public:
    static const char* XSD_NAMESPACE_URI;

    /** The XSI namespace URI */
  public:
    static const char* XSI_NAMESPACE_URI;

    /**
     * Default constructor. This constructors exists for convenience only
     * and results in an invalid qname
     */
  public:
    inline QName() throws() {}

    /**
     * Create a new qname
     * @param ns the namespace uri
     * @param name the name
     */
  public:
    QName(const String& ns, const String& n) throws();
    
    /**
     * Make an XSD type.
     * @param name the name of an XSD type
     * @return a QName 
     */
  public:
    static QName xsQName(const String& n) throws();
    
    /**
     * Determine if this is a valid qname
     * @return true if this is a valid qname
     */
  public:
    inline operator bool() const throws() 
    { return _name; }

    /**
     * Compare this qname to the specified qname.
     * @param n a qname
     * @return true if the two names are exactly the same
     */
  public:
    bool operator==(const QName& n) const throws();

    /**
     * Compare this qname to the specified qname.
     * @param n a qname
     * @return true if the two names are exactly the same
     */
  public:
    inline bool operator!=(const QName& n) const throws()
    { return !(*this == n); }

    /**
     * Compare this qname to the specified qname. 
     * @param n a qname
     * @return true if the this qname is lexicographically less
     */
  public:
    bool operator<(const QName& n) const throws();

    /**
     * Get the local name
     * @return the local name
     */
  public:
    inline const String& name() const throws() { return _name; }

    /**
     * Get the local name
     * @return the local name
     */
  public:
    inline const String& namespaceURI() const throws() { return _namespace; }

    /**
     * Get a unique string for this qname
     * @return the string for this qname that will also be emitted
     */
  public:
    String string() const throws();

    /** The namespace uri and the name */
  private:
    String _namespace,_name;
  };
}
::std::ostream& operator<< (::std::ostream& out, const ::xylon::QName& qname);

#endif

