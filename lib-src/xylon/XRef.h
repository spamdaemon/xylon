#ifndef _XYLON_XREF_H
#define _XYLON_XREF_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif


#ifndef _XYLON_QNAME_H
#include <xylon/QName.h>
#endif

#include <ostream>

namespace xylon {
    
  /**
   * The XRef class works is a indirect pointer to a object of another type. The object
   * referred to is known by a unique name, which in this case is a qname.
   * XRefs are initially unbound, that is they do not point anything. Use the bind method
   * to bind an xref to an actual object. 
   */
  template <class T, class NAME= ::xylon::QName> struct XRef {

    /** The name type */
  public:
    typedef NAME Name;


    /** The reference implementation */
  private:
    struct Impl : public ::timber::Counted {
    Impl(const Name& nm)
      : _name(nm) 
      {}
	
      ~Impl() throws()
	{}
	
      /**
       * Access the real object
       * @return a reference to the real object
       */
    public:
      inline const T& ref() const
      {
	if (_object) {
	  return *_object;
	}
	throw ::std::runtime_error("XRef was not bound");
      }
	
      /** The namespace uri and localname */
    public:
      const Name _name;
      
      /** The pointer to the real object */
    public:
      ::timber::Pointer<T> _object;
    };

    /** The default constructor */
  public:
    XRef() throws() 
    : _modifiable(false)
    {}
      
     /** The constructor with a qname */
  public:
    XRef(const Name& qn, ::timber::Pointer<T> obj, bool modifiable)  throws()
    : _target(new Impl(qn)),_modifiable(modifiable)
    {
      ::timber::Pointer<T>::swap(obj,_target->_object);
    }
    
     /** The constructor with a qname */
  public:
    XRef(const Name& qn, bool modifiable)  throws()
    : _target(new Impl(qn)),_modifiable(modifiable)
    {}
      
    /**
     * Create a copy of the xref, possibly changing  its modifiability. Modifiability
     * cannot be changed from false to true. Therefore, the only value for
     * modifiable, which makes any sense is <em>false</em>.
     * 
     * @param orig the original xref
     * @param modifiable true if the xref should be modifiable, false otherwise.
     */
  public:
    XRef(const XRef& orig, bool modifiable) throws()
    : _target(orig._target),_modifiable(orig._modifiable && modifiable)
    {}

    /** The constructor with a name and a namespace */
  public:
    XRef(const XRef& orig) throws()
    : _target(orig._target),_modifiable(orig._modifiable)
    {}

    /** The constructor with a name and a namespace */
  public:
    XRef& operator=(const XRef& orig) throws()
    {
      _modifiable = orig._modifiable;
      _target = orig._target;
      return *this;
    }
      
    /**
     * Bind this reference to the specified object. 
     * @param obj an object or 0 to clear the reference
     * @throws exception if this xref is not modifiable
     */
  public:
    ::timber::Pointer<T> bind (::timber::Pointer<T> obj)
    {
      if (!_modifiable) {
	assert("XRef cannot be modified"==0);
	throw ::std::runtime_error("XRef cannot be modified");
      }
      ::timber::Pointer<T>::swap(obj,_target->_object);
      return obj;
    }

    /**
     * Unbind this xref.
     * @throws exception if this xref is not modifiable
     */
  public:
    ::timber::Pointer<T> unbind ()
    { return bind(::timber::Pointer<T>()); }

    /**
     * Determine if this non-const methods can be invoked on this target
     * of this reference.
     * @return true if this reference can be bound or cleared
     */
  public:
    inline bool isModifiable() const throws()
    { return _modifiable; }

    /**
     * Determine if this reference has been bound.
     * @return true if this reference has been resolved and this xref is valid.
     */
  public:
    inline bool isBound() const throws()
    { return _target && _target->_object; }
      
    /**
     * Determine if this reference is defined
     * @return true if this xref is a valid xref.
     */
  public:
    inline operator bool() const throw() 
    { return _target; }
      
    /**
     * Get the name
     * @return the name for this ref
     */
  public:
    inline const Name& name() const throws() { return _target->_name; }

    /**
     * Get the target of this reference.
     * @return the target or 0 if not bound
     */
  public:
    ::timber::Pointer<T> target() const throws()
    { 
      ::timber::Pointer<T> tgt;
      if (_target) {
	tgt = _target->_object;
      }
      return tgt;
    }

    /**
     * Derefence the object
     * @return the object
     * @throws Exception if this xref is null or it's not resolved
     */
  public:
    T* operator->() const throws()
    {
      if (_target && _target->_object) {
	return &*_target->_object;
      }
      if (!_target) {
	throw ::std::runtime_error("Reference is undefined");
      }
      else {
	assert("Reference is not resolved"==0);
	throw ::std::runtime_error("Reference is not resolved");
      }
    }

    /**
     * Compare two xrefs. Two xrefs are equal, if their names are the same
     * @param xref another ref
     * @return true if this xref is the same as xref
     */
  public:
    bool operator==(const XRef& node) const throws()
    {
      if (_target==node._target) {
	return true;
      }
      if (!_target || !node._target) {
	return false;
      }
      // compare by name first, because namespace is longer 
      return _target->_name == node._target->_name;
    }

    /**
     * Determine if this xref is lexicographically less than the specified xref
     * @param xref another xref
     * @return true iff this xref is lexicographically less than xref
     */
  public:
    bool operator< (const XRef& node) const throws()
    {
      if (_target==node._target) {
	return false;
      }
      if (!_target) {
	return true;
      }
      if (!node._target) {
	return false;
      }
      return _target->_name < node._target->_name;
    }

    /** The referenced object */
  private:
    ::timber::Pointer<Impl> _target;

    /** True if this reference is modifiable */
  private:
    bool _modifiable;
  };
}
template <class T>
inline ::std::ostream& operator<< (::std::ostream& out, const ::xylon::XRef<T>& ref)
{ return out << ref.name(); }


#endif

