#ifndef _XYLON_SCHEMA_H
#define _XYLON_SCHEMA_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _XYLON_H
#include <xylon/xylon.h>
#endif

namespace xylon {
   /**
    * The SchemaManager is a singleton class which provides access to all schemas
    * known to the application. The SchemaManger's primary task is to create
    * schemas.
    */
   class Schema : public ::timber::Counted
   {
         /** Default constructor */
      protected:
        Schema() throws();

         /** Destructor */
      public:
         virtual ~Schema() throws() = 0;
   };
}
#endif
