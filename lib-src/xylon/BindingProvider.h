#ifndef _XYLON_BINDINGPROVIDER_H
#define _XYLON_BINDINGPROVIDER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _XYLON_H
#include <xylon/xylon.h>
#endif

#ifndef _XYLON_MODEL_MODELREF_H
#include <xylon/model/ModelRef.h>
#endif

namespace xylon {

   class XsdSchemaModel;
   namespace model {
      class TypeModel;
   }
   namespace xsdom {
      class Element;
      class ComplexType;
      class SimpleType;
      class Group;
      class Attribute;
      class AttributeGroup;
   }

   /**
    * The BindingProvider is used to obtain a SchemaModel for schema. The schema is specified by
    * its namespace and may need to be looked up in the SchemaManager.
    */
   class BindingProvider
   {

         /** Default constructor */
      protected:
         BindingProvider() throws();

         /** Destructor */
      public:
         virtual ~BindingProvider() throws() = 0;

         /**
          * @defgroup NodeBinders Top-Level Node binding functions.
          * @{
          */

         /**
          * Create a binding for the specified element.
          * @param xref an unbound XRef.
          */
      public:
         virtual ::timber::Pointer< ::xylon::model::TypeModel>
               bindElement(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference< ::xylon::xsdom::Element>& s) const throws (::std::exception) = 0;

         /**
          * Create a binding for the specified complex type.
          * @param xref an unbound XRef.
          */
      public:
         virtual ::timber::Pointer< ::xylon::model::TypeModel>
         bindComplexType(const ::xylon::XsdSchemaModel& schema,
               const ::timber::Reference< ::xylon::xsdom::ComplexType>& s) const throws (::std::exception) = 0;

         /**
          * Create a binding for the specified simple type.
          * @param xref an unbound XRef.
          */
      public:
         virtual ::timber::Pointer< ::xylon::model::TypeModel>
               bindSimpleType(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference<
                     ::xylon::xsdom::SimpleType>& s) const throws (::std::exception)= 0;

         /**
          * Create a binding for the specified group.
          * @param xref an unbound XRef.
          */
      public:
         virtual ::timber::Pointer< ::xylon::model::TypeModel>
               bindGroup(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference< ::xylon::xsdom::Group>& s) const throws (::std::exception) = 0;

         /**
          * Create a binding for the specified attribute.
          * @param xref an unbound XRef.
          */
      public:
         virtual ::timber::Pointer< ::xylon::model::TypeModel>
               bindAttribute(const ::xylon::XsdSchemaModel& schema,
                     const ::timber::Reference< ::xylon::xsdom::Attribute>& s) const throws (::std::exception) = 0;

         /**
          * Create a binding for the specified attribute group.
          * @param xref an unbound XRef.
          */
      public:
         virtual ::timber::Pointer< ::xylon::model::TypeModel>
         bindAttributeGroup(const ::xylon::XsdSchemaModel& schema, const ::timber::Reference<
               ::xylon::xsdom::AttributeGroup>& s) const throws (::std::exception) = 0;

         /*@}*/
   };
}
#endif
