#ifndef _XYLON_SCHEMABUILDER_H
#define _XYLON_SCHEMABUILDER_H

#ifndef _TIMBER_W3C_URI_H
#include <timber/w3c/URI.h>
#endif

#ifndef _XYLON_XSDSCHEMA_H
#include <xylon/XsdSchema.h>
#endif

namespace xylon {
   namespace xsdom {
      class Schema;
   }

   /**
    * This schema builder is used to incrementally build an instance of an XML schema.
    */
   class SchemaBuilder
   {
         /** The default constructor */
      protected:
         SchemaBuilder() throws();

         /** Destructor */
      public:
         virtual ~SchemaBuilder() throws();

         /**
          * Create a new schema builder.
          * @return a schema builder.
          */
      public:
         static ::std::unique_ptr< SchemaBuilder> create() throws();

         /**
          * Import a new schema into the schema being built. The schema's namespace is unknown.
          * @param uri the schema uri
          * @throws ::std::exception if the schema could not be loaded.
          */
      public:
         virtual void
         importSchema(const ::timber::SharedRef< ::timber::w3c::URI>& uri) throws (::std::exception) = 0;

         /**
          * Import a new schema into the schema being built.
          * @param ns the schema's namespace
          * @param uri the schema uri
          * @return true if the schema was imported, false if the namespace is already known
          * @throws ::std::exception if the schema could not be loaded.
          */
      public:
         virtual bool
               importSchema(const String& ns, const ::timber::SharedRef< ::timber::w3c::URI>& uri) throws (::std::exception) = 0;

         /**
          * Add an XML schema which is an object.
          * @param schema a schema object
          */
      public:
         virtual void
               importSchema(const ::timber::Reference< ::xylon::xsdom::Schema>& schema) throws (::std::exception) = 0;

         /**
          * Get the schema that has been built. If the schema still contains unresolved references, then this method
          * returns a null pointer
          * @return a schema or null if there are still unresolved references
          */
      public:
         virtual ::timber::Pointer< ::xylon::XsdSchema> getSchema() throws() = 0;
   };
}
#endif
