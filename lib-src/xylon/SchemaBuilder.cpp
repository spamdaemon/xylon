#include <xylon/SchemaBuilder.h>
#include <xylon/xsdom/nodes.h>
#include <xylon/xsdom/dom/util.h>
#include <xylon/xsdom/dom/Visitor.h>
#include <xylon/xsdom/Visitor.h>
#include <xylon/xsdom/xsdom.h>
#include <xylon/xsdom/XmlSchema.h>

#include <timber/logging.h>
#include <timber/w3c/xml/xml.h>
#include <timber/w3c/xml/dom/dom.h>
#include <timber/w3c/xml/dom/Document.h>
#include <timber/w3c/xml/dom/Element.h>
#include <timber/w3c/xml/dom/Attr.h>
#include <timber/w3c/xml/dom/NodeList.h>
#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/dom/DocumentParser.h>
#include <timber/ios/URIInputStream.h>

#include <map>
#include <set>
#include <vector>
#include <stack>

using namespace ::std;
using namespace ::timber;
using namespace ::timber::logging;
using namespace ::timber::ios;
using namespace ::timber::w3c::xml;
using namespace ::xylon::xsdom::dom;
using namespace ::xylon::xsdom;

namespace xylon {
   namespace {
      typedef ::xylon::xsdom::Schema Schema;

      struct Log theLogger()
      {
         return Log("xylon.SchemaBuilder");
      }

      template<class T>
      static XRef< T> getXRef(map< QName, XRef< T> >& refs, const QName& qname, Pointer< T> ptr = Pointer< T>())
      {
         XRef< T>& xref = refs[qname];
         if (!xref) {
            xref = XRef< T>(qname, true);
         }
         if (!xref.isBound()) {
            if (ptr) {
               LogEntry(theLogger()).debugging() << "Binding " << qname << doLog;
               xref.bind(ptr);
            }
         }
         return XRef< T>(refs[qname], false);
      }

      static TypeXRef getTypeXRef(map< QName, TypeXRef>& refs, const QName& qname)
      {
         return getXRef< Type>(refs, qname);
      }
      static KeyXRef getKeyXRef(map< QName, KeyXRef>& refs, const QName& qname)
      {
         return getXRef< Key>(refs, qname);
      }
      static ElementXRef getElementXRef(map< QName, ElementXRef>& refs, const QName& qname)
      {
         return getXRef< Element>(refs, qname);
      }
      static GroupXRef getGroupXRef(map< QName, GroupXRef>& refs, const QName& qname)
      {
         return getXRef< Group>(refs, qname);
      }
      static AttributeXRef getAttributeXRef(map< QName, AttributeXRef>& refs, const QName& qname)
      {
         return getXRef< Attribute>(refs, qname);
      }
      static AttributeGroupXRef getAttributeGroupXRef(map< QName, AttributeGroupXRef>& refs, const QName& qname)
      {
         return getXRef< AttributeGroup>(refs, qname);
      }

      static QName getXRefQName(const ElementRef& obj, const String& ref)
      {
         String nsURI = namespaceURI(obj, ref);
         String pre, localname;
         ::timber::w3c::xml::dom::splitQName(ref, pre, localname);
         return QName(nsURI, localname);
      }

      static TypeXRef getTypeXRef(const ElementRef& obj, const String& ref, map< QName, TypeXRef>& refs)
      {
         return getTypeXRef(refs, getXRefQName(obj, ref));
      }
      static KeyXRef getKeyXRef(const ElementRef& obj, const String& ref, map< QName, KeyXRef>& refs)
      {
         return getKeyXRef(refs, getXRefQName(obj, ref));
      }
      static ElementXRef getElementXRef(const ElementRef& obj, const String& ref, map< QName, ElementXRef>& refs)
      {
         return getElementXRef(refs, getXRefQName(obj, ref));
      }
      static GroupXRef getGroupXRef(const ElementRef& obj, const String& ref, map< QName, GroupXRef>& refs)
      {
         return getGroupXRef(refs, getXRefQName(obj, ref));
      }
      static AttributeXRef getAttributeXRef(const ElementRef& obj, const String& ref, map< QName, AttributeXRef>& refs)
      {
         return getAttributeXRef(refs, getXRefQName(obj, ref));
      }
      static AttributeGroupXRef getAttributeGroupXRef(const ElementRef& obj, const String& ref,
            map< QName, AttributeGroupXRef>& refs)
      {
         return getAttributeGroupXRef(refs, getXRefQName(obj, ref));
      }

      template<class T>
      static XRef< T> makeXRef(const QName& qname, const Reference< T>& obj, map< QName, XRef< T> >& refs)
      {
         XRef< T>& ref = refs[qname];

         if (!ref) {
            ref = XRef< T>(qname, true);
         }
         if (!ref.isModifiable()) {
            if (ref.isBound()) {
               // already bound, probably to an external or builtin type
               return ref;
            }
            else {
               throw SchemaException("Internal error: xref is not modifiable, but is not bound");
            }
         }
         LogEntry(theLogger()).debugging() << "Binding " << typeid(T) << " : " << qname << doLog;
         if (ref.isBound()) {
            // FIXME: duplicate bindings are not yet supported; note clear what to do in this circumstance? Rebind?
            throw SchemaException("Duplicate name " + qname.string().string());
         }
         ref.bind(obj);
         return ref;
      }

      static TypeXRef makeTypeXRef(const QName& qname, const Reference< Type>& obj, map< QName, TypeXRef>& refs)
      {
         return makeXRef< Type>(qname, obj, refs);
      }
      static KeyXRef makeKeyXRef(const QName& qname, const Reference< Key>& obj, map< QName, KeyXRef>& refs)
      {
         return makeXRef< Key>(qname, obj, refs);
      }
      static ElementXRef makeElementXRef(const QName& qname, const Reference< Element>& obj,
            map< QName, ElementXRef>& refs)
      {
         return makeXRef< Element>(qname, obj, refs);
      }
      static GroupXRef makeGroupXRef(const QName& qname, const Reference< Group>& obj, map< QName, GroupXRef>& refs)
      {
         return makeXRef< Group>(qname, obj, refs);
      }
      static AttributeXRef makeAttributeXRef(const QName& qname, const Reference< Attribute>& obj,
            map< QName, AttributeXRef>& refs)
      {
         return makeXRef< Attribute>(qname, obj, refs);
      }
      static AttributeGroupXRef makeAttributeGroupXRef(const QName& qname, const Reference< AttributeGroup>& obj,
            map< QName, AttributeGroupXRef>& refs)
      {
         return makeXRef< AttributeGroup>(qname, obj, refs);
      }

      template<class T>
      static void clearXRef(map< QName, XRef< T> >& refs)
      {
         for (typename map< QName, XRef< T> >::iterator i = refs.begin(); i != refs.end(); ++i) {
            if (i->second.isModifiable()) {
               i->second.bind(Pointer< T>());
            }
         }
         refs.clear();
      }

      template<class T> bool isBound(XRef< T> ref)
      {
         return false;
      }
      template<> bool isBound< Type>(XRef< Type> ref)
      {
         return ref.isBound();
      }
      template<> bool isBound< Key>(XRef< Key> ref)
      {
         return ref.isBound();
      }
      template<> bool isBound< Element>(XRef< Element> ref)
      {
         return ref.isBound();
      }
      template<> bool isBound< Group>(XRef< Group> ref)
      {
         return ref.isBound();
      }
      template<> bool isBound< Attribute>(XRef< Attribute> ref)
      {
         return ref.isBound();
      }
      template<> bool isBound< AttributeGroup>(XRef< AttributeGroup> ref)
      {
         return ref.isBound();
      }

      template<class T>
      static bool checkBound(const string& name, const map< QName, XRef< T> >& refs)
      {
         bool allBound = true;
         for (typename map< QName, XRef< T> >::const_iterator i = refs.begin(); i != refs.end(); ++i) {
            if (!isBound< T>(i->second)) {
               LogEntry(theLogger()).severe() << name << " reference " << i->first << " was not bound" << doLog;
               throw ::std::runtime_error("Not bound : " + i->first.string().string());
            }
            allBound = false;
         }
         return allBound;
      }

      struct AllImpl : public All
      {
            AllImpl()
            {
            }
            ~AllImpl() throws()
            {
            }

            void setItems(const vector< Reference< Enumerable> >& xitems)
            {
               for (size_t i = 0; i < xitems.size(); ++i) {
                  if (!xitems[i].tryDynamicCast< Element>()) {
                     throw runtime_error("xs:all can only contain elements");
                  }
                  Occurrence xoccurs = xitems[i]->occurs();
                  if (xoccurs.min() > 1 || xoccurs.max() > 1) {
                     throw runtime_error("elements contained by xs:all must occurs 0 or 1 times, only");
                  }
               }
               _items = xitems;
            }

            const ::std::vector< ::timber::Reference< Enumerable> >& items() const throws()
            {
               return _items;
            }
            Occurrence occurs() const throws()
            {
               return _occurs;
            }
            Occurrence _occurs;
            ::std::vector< ::timber::Reference< Enumerable> > _items;
      };

      struct GroupImpl : public Group
      {
            ~GroupImpl() throws()
            {
            }
            GroupImpl()
            {
            }

            Occurrence occurs() const throws()
            {
               return _occurs;
            }

            GroupXRef ref() const throws()
            {
               return _ref;
            }
            ::timber::Pointer< Enumerable> enumerable() const throws()
            {
               return _enumerable;
            }
            QName qname() const throws()
            {
               return _qname;
            }

            Occurrence _occurs;
            QName _qname;
            GroupXRef _ref;
            ::timber::Pointer< Enumerable> _enumerable;
      };

      struct SimpleTypeImpl : public SimpleType
      {
            ~SimpleTypeImpl() throws()
            {
            }
            SimpleTypeImpl()
                  : _final(FINAL_NONE)
            {
            }

            QName qname() const throws()
            {
               return _qname;
            }
            ::timber::Pointer< Restriction> restriction() const throws()
            {
               return _restriction;
            }
            ::timber::Pointer< List> list() const throws()
            {
               return _list;
            }
            ::timber::Pointer< Union> getUnion() const throws()
            {
               return _union;
            }
            int final() const throws()
            {
               return _final;
            }

            ::timber::Pointer< Restriction> _restriction;
            ::timber::Pointer< List> _list;
            ::timber::Pointer< Union> _union;

            int _final;
            QName _qname;
      };

      struct AnyAttributeImpl : public AnyAttribute
      {
            ~AnyAttributeImpl() throws()
            {
            }
            AnyAttributeImpl(String tns) throws()
                  : _processContents(STRICT), _targetNamespace(tns)
            {
               _namespaces.insert("##any");
            }

            void setNamespaces(const ::std::set< String>& ns)
            {
               assert(!ns.empty());
               _namespaces = ns;
               if (ns.empty()) {
                  throw ::std::runtime_error("Empty namespace list specified");
               }
               if (ns.count("##any") != 0) {
                  _namespaces.clear();
                  _namespaces.insert(String("##any"));
               }
               else if (ns.count("##other")) {
                  _namespaces.clear();
                  _namespaces.insert(String("##other"));
               }
            }

            String targetNamespace() const throws()
            {
               return _targetNamespace;
            }
            const ::std::set< String>& namespaces() const throws()
            {
               return _namespaces;
            }
            ProcessContents getProcessContents() const throws()
            {
               return _processContents;
            }
            ::std::set< String> _namespaces;
            ProcessContents _processContents;
            String _targetNamespace;
      };

      struct AnyImpl : public Any
      {
            ~AnyImpl() throws()
            {
            }
            AnyImpl(String tns)
                  : _processContents(STRICT), _targetNamespace(tns)
            {
               _namespaces.insert("##any");
            }
            void setNamespaces(const ::std::set< String>& ns)
            {
               assert(!ns.empty());
               _namespaces = ns;
               if (ns.empty()) {
                  throw ::std::runtime_error("Empty namespace list specified");
               }
               if (ns.count("##any") != 0) {
                  _namespaces.clear();
                  _namespaces.insert(String("##any"));
               }
               else if (ns.count("##other")) {
                  _namespaces.clear();
                  _namespaces.insert(String("##other"));
               }
            }

            String targetNamespace() const throws()
            {
               return _targetNamespace;
            }

            const ::std::set< String>& namespaces() const throws()
            {
               return _namespaces;
            }
            ProcessContents getProcessContents() const throws()
            {
               return _processContents;
            }

            Occurrence occurs() const throws()
            {
               return _occurs;
            }
            Occurrence _occurs;
            ::std::set< String> _namespaces;
            ProcessContents _processContents;
            String _targetNamespace;
      };

      struct KeyImpl : public Key
      {
            ~KeyImpl() throws()
            {
            }

            QName qname() const throws()
            {
               return _qname;
            }

            QName _qname;
      };

      struct ElementImpl : public Element
      {
            ~ElementImpl() throws()
            {
            }
            ElementImpl()
                  : _nillable(false), _abstract(false), _block(BLOCK_NONE), _final(FINAL_NONE), _form(FORM_QUALIFIED)
            {
            }

            QName qname() const throws()
            {
               return _qname;
            }
            bool isNillable() const throws()
            {
               return _nillable;
            }
            bool isAbstract() const throws()
            {
               return _abstract;
            }
            int block() const throws()
            {
               return _block;
            }
            int final() const throws()
            {
               return _final;
            }
            Form form() const throws()
            {
               return _form;
            }
            String fixedValue() const throws()
            {
               return _fixedValue;
            }
            String defaultValue() const throws()
            {
               return _defaultValue;
            }
            ElementXRef ref() const throws()
            {
               return _ref;
            }
            TypeXRef type() const throws()
            {
               return _type;
            }
            ElementXRef substitutionGroup() const throws()
            {
               return _substitutionGroup;
            }
            ::timber::Pointer< Type> elementType() const throws()
            {
               return _localType;
            }
            Occurrence occurs() const throws()
            {
               return _occurs;
            }
            Occurrence _occurs;

            bool _nillable;
            bool _abstract;
            int _block;
            int _final;
            Form _form;
            QName _qname;
            ElementXRef _ref;
            TypeXRef _type;
            ElementXRef _substitutionGroup;
            ::timber::Pointer< Type> _localType;
            String _fixedValue;
            String _defaultValue;
      };

      struct AttributeGroupImpl : public AttributeGroup
      {
            ~AttributeGroupImpl() throws()
            {
            }

            AttributeGroupXRef ref() const throws()
            {
               return _ref;
            }
            ::timber::Pointer< AnyAttribute> anyAttribute() const throws()
            {
               return _anyAttribute;
            }
            const ::std::vector< ::timber::Reference< Attribute> >& attributes() const throws()
            {
               return _attributes;
            }
            const ::std::vector< ::timber::Reference< AttributeGroup> >& attributeGroups() const throws()
            {
               return _attributeGroups;
            }
            QName qname() const throws()
            {
               return _qname;
            }

            QName _qname;
            AttributeGroupXRef _ref;
            ::timber::Pointer< AnyAttribute> _anyAttribute;
            ::std::vector< ::timber::Reference< Attribute> > _attributes;
            ::std::vector< ::timber::Reference< AttributeGroup> > _attributeGroups;
      };

      struct UnionImpl : public Union
      {
            ~UnionImpl() throws()
            {
            }
            const ::std::vector< TypeXRef> & memberTypes() const throws()
            {
               return _memberTypes;
            }
            const ::std::vector< ::timber::Reference< SimpleType> > & types() const throws()
            {
               return _types;
            }

            ::std::vector< TypeXRef> _memberTypes;
            ::std::vector< ::timber::Reference< SimpleType> > _types;
      };

      struct AttributeImpl : public Attribute
      {
            ~AttributeImpl() throws()
            {
            }
            AttributeImpl() throws()
                  : _form(FORM_QUALIFIED), _use(USE_OPTIONAL)
            {
            }

            String fixedValue() const throws()
            {
               return _fixedValue;
            }
            String defaultValue() const throws()
            {
               return _defaultValue;
            }
            Form form() const throws()
            {
               return _form;
            }
            AttributeXRef ref() const throws()
            {
               return _ref;
            }
            TypeXRef type() const throws()
            {
               return _type;
            }
            ::timber::Pointer< SimpleType> attributeType() const throws()
            {
               return _localType;
            }
            Use use() const throws()
            {
               return _use;
            }
            QName qname() const throws()
            {
               return _qname;
            }

            String _fixedValue;
            String _defaultValue;
            QName _qname;
            AttributeXRef _ref;
            TypeXRef _type;
            Form _form;
            Use _use;
            ::timber::Pointer< SimpleType> _localType;
      };

      struct ListImpl : public List
      {
            ~ListImpl() throws()
            {
            }
            TypeXRef itemType() const throws()
            {
               return _itemType;
            }
            ::timber::Pointer< SimpleType> type() const throws()
            {
               return _type;
            }

            TypeXRef _itemType;
            ::timber::Pointer< SimpleType> _type;
      };

      struct ExtensionImpl : public Extension
      {
            ~ExtensionImpl() throws()
            {
            }

            TypeXRef base() const throws()
            {
               return _base;
            }
            TypeXRef _base;

            ::timber::Pointer< Enumerable> enumerable() const throws()
            {
               return _enumerable;
            }
            ::timber::Pointer< AnyAttribute> anyAttribute() const throws()
            {
               return _anyAttribute;
            }
            const ::std::vector< ::timber::Reference< Attribute> >& attributes() const throws()
            {
               return _attributes;
            }
            const ::std::vector< ::timber::Reference< AttributeGroup> >& attributeGroups() const throws()
            {
               return _attributeGroups;
            }

            ::timber::Pointer< Enumerable> _enumerable;
            ::timber::Pointer< AnyAttribute> _anyAttribute;
            ::std::vector< ::timber::Reference< Attribute> > _attributes;
            ::std::vector< ::timber::Reference< AttributeGroup> > _attributeGroups;
      };

      struct RestrictionImpl : public Restriction
      {
            ~RestrictionImpl() throws()
            {
            }
            RestrictionImpl() throws()
                  : _simpleRestriction(false)
            {
            }

            TypeXRef base() const throws()
            {
               return _base;
            }
            TypeXRef _base;

            bool isSimpleTypeRestriction() const throws()
            {
               return _simpleRestriction;
            }
            ::timber::Pointer< SimpleType> simpleType() const throws()
            {
               return _simpleType;
            }
            const ::std::vector< ::timber::Reference< Facet> >& facets() const throws()
            {
               return _facets;
            }
            ::timber::Pointer< Enumerable> enumerable() const throws()
            {
               return _enumerable;
            }
            ::timber::Pointer< AnyAttribute> anyAttribute() const throws()
            {
               return _anyAttribute;
            }
            const ::std::vector< ::timber::Reference< Attribute> >& attributes() const throws()
            {
               return _attributes;
            }
            const ::std::vector< ::timber::Reference< AttributeGroup> >& attributeGroups() const throws()
            {
               return _attributeGroups;
            }

            ::timber::Pointer< Enumerable> _enumerable;
            ::timber::Pointer< AnyAttribute> _anyAttribute;
            ::std::vector< ::timber::Reference< Attribute> > _attributes;
            ::std::vector< ::timber::Reference< AttributeGroup> > _attributeGroups;
            ::timber::Pointer< SimpleType> _simpleType;
            ::std::vector< ::timber::Reference< Facet> > _facets;
            bool _simpleRestriction;
      };

      struct ChoiceImpl : public Choice
      {
            ~ChoiceImpl() throws()
            {
            }
            ChoiceImpl() throws()
            {
            }

            const ::std::vector< ::timber::Reference< Enumerable> >& items() const throws()
            {
               return _items;
            }

            Occurrence occurs() const throws()
            {
               return _occurs;
            }
            Occurrence _occurs;
            ::std::vector< ::timber::Reference< Enumerable> > _items;
      };

      struct SchemaImpl : public ::xylon::xsdom::Schema
      {
            SchemaImpl() throws()
                  : _attributeFormDefault(FORM_UNQUALIFIED), _elementFormDefault(FORM_UNQUALIFIED), _blockDefaults(0), _finalDefaults(
                        0)
            {
            }

            ~SchemaImpl() throws()
            {
               clear();
            }

            void setNodes(const vector< Reference< Node> > & xnodes)
            {
               for (size_t i = 0; i < xnodes.size(); ++i) {
                  if (xnodes[i].tryDynamicCast< SimpleType>() || xnodes[i].tryDynamicCast< ComplexType>()) {
                     makeTypeXRef(Reference< Type>(xnodes[i])->qname(), xnodes[i], _types);
                  }
                  else if (xnodes[i].tryDynamicCast< Group>()) {
                     makeGroupXRef(Reference< Group>(xnodes[i])->qname(), xnodes[i], _groups);
                  }
                  else if (xnodes[i].tryDynamicCast< Element>()) {
                     makeElementXRef(Reference< Element>(xnodes[i])->qname(), xnodes[i], _elements);
                  }
                  else if (xnodes[i].tryDynamicCast< AttributeGroup>()) {
                     makeAttributeGroupXRef(Reference< AttributeGroup>(xnodes[i])->qname(), xnodes[i],
                           _attributeGroups);
                  }
                  else if (xnodes[i].tryDynamicCast< Attribute>()) {
                     makeAttributeXRef(Reference< Attribute>(xnodes[i])->qname(), xnodes[i], _attributes);
                  }
                  else if (xnodes[i].tryDynamicCast< Include>() || xnodes[i].tryDynamicCast< Import>()
                        || xnodes[i].tryDynamicCast< Redefine>() || xnodes[i].tryDynamicCast< Annotation>()
                        || xnodes[i].tryDynamicCast< Notation>()) {
                  }
                  else {
                     throw ::std::runtime_error("Unexpected top-level node");
                  }
               }
            }

            Pointer< Node> parentOf(const ::timber::Reference< Node>& node) const throws()
            {
               map< Reference< Node>, Reference< Node> >::const_iterator i = _parentOf.find(node);
               if (i == _parentOf.end()) {
                  return Pointer< Node>();
               }
               else {
                  return i->second;
               }
            }

            Pointer< Node> findNode(const NodeRef& ref) const throws()
            {
               switch (ref.type()) {
                  case NodeRef::ELEMENT:
                     return findElement(ref.name());
                  case NodeRef::GROUP:
                     return findGroup(ref.name());
                  case NodeRef::ATTRIBUTE:
                     return findAttribute(ref.name());
                  case NodeRef::ATTRIBUTE_GROUP:
                     return findAttributeGroup(ref.name());
                  case NodeRef::TYPE:
                     return findType(ref.name());
                  case NodeRef::KEY:
                     return findKey(ref.name());
                  default:
                     return Pointer< Node>();
               }
            }

            bool isTopLevelNode(const ::timber::Reference< Node>& node) const throws()
            {
               return _nodes.find(node) != _nodes.end();
            }

            void clear()
            {
               clearXRef(_types);
               clearXRef(_elements);
               clearXRef(_groups);
               clearXRef(_keys);
               clearXRef(_attributes);
               clearXRef(_attributeGroups);
            }

            vector< AttributeXRef> attributes() const throws()
            {
               vector< AttributeXRef> res;
               for (::std::map< QName, AttributeXRef>::const_iterator i = _attributes.begin(), end = _attributes.end();
                     i != end; ++i) {
                  res.push_back(i->second);
               }
               return res;
            }
            vector< AttributeGroupXRef> attributeGroups() const throws()
            {
               vector< AttributeGroupXRef> res;
               for (::std::map< QName, AttributeGroupXRef>::const_iterator i = _attributeGroups.begin(), end =
                     _attributeGroups.end(); i != end; ++i) {
                  res.push_back(i->second);
               }

               return res;
            }
            vector< ElementXRef> elements() const throws()
            {
               vector< ElementXRef> res;
               for (::std::map< QName, ElementXRef>::const_iterator i = _elements.begin(), end = _elements.end();
                     i != end; ++i) {
                  res.push_back(i->second);
               }

               return res;
            }
            vector< GroupXRef> groups() const throws()
            {
               vector< GroupXRef> res;
               for (::std::map< QName, GroupXRef>::const_iterator i = _groups.begin(), end = _groups.end(); i != end;
                     ++i) {
                  res.push_back(i->second);
               }

               return res;
            }
            vector< TypeXRef> types() const throws()
            {
               vector< TypeXRef> res;
               for (::std::map< QName, TypeXRef>::const_iterator i = _types.begin(), end = _types.end(); i != end;
                     ++i) {
                  res.push_back(i->second);
               }

               return res;
            }

            vector< Reference< Import> > imports() const throws()
            {
               return _imports;
            }

            Pointer< Type> findType(const QName& qname) const throws()
            {
               map< QName, TypeXRef>::const_iterator i = _types.find(qname);
               if (i == _types.end()) {
                  return Pointer< Type>();
               }
               else {
                  return i->second.target();
               }
            }

            Pointer< Group> findGroup(const QName& qname) const throws()
            {
               map< QName, GroupXRef>::const_iterator i = _groups.find(qname);
               if (i == _groups.end()) {
                  return Pointer< Group>();
               }
               else {
                  return i->second.target();
               }
            }

            Pointer< AttributeGroup> findAttributeGroup(const QName& qname) const throws()
            {
               map< QName, AttributeGroupXRef>::const_iterator i = _attributeGroups.find(qname);
               if (i == _attributeGroups.end()) {
                  return Pointer< AttributeGroup>();
               }
               else {
                  return i->second.target();
               }
            }

            Pointer< Attribute> findAttribute(const QName& qname) const throws()
            {
               map< QName, AttributeXRef>::const_iterator i = _attributes.find(qname);
               if (i == _attributes.end()) {
                  return Pointer< Attribute>();
               }
               else {
                  return i->second.target();
               }
            }

            Pointer< Element> findElement(const QName& qname) const throws()
            {
               map< QName, ElementXRef>::const_iterator i = _elements.find(qname);
               if (i == _elements.end()) {
                  return Pointer< Element>();
               }
               else {
                  return i->second.target();
               }
            }

            Pointer< Key> findKey(const QName& qname) const throws()
            {
               map< QName, KeyXRef>::const_iterator i = _keys.find(qname);
               if (i == _keys.end()) {
                  return Pointer< Key>();
               }
               else {
                  return i->second.target();
               }
            }

            int blockDefaults() const throws()
            {
               return _blockDefaults;
            }
            int finalDefaults() const throws()
            {
               return _finalDefaults;
            }
            String targetNamespace() const throws()
            {
               return _targetNamespace;
            }
            String version() const throws()
            {
               return _version;
            }
            String language() const throws()
            {
               return _language;
            }
            Schema::Form attributeFormDefault() const throws()
            {
               return _attributeFormDefault;
            }
            Schema::Form elementFormDefault() const throws()
            {
               return _elementFormDefault;
            }

            ::std::vector< ::timber::Reference< Node> > nodes() const throws()
            {
               ::std::vector< ::timber::Reference< Node> > res;
               res.insert(res.end(), _nodes.begin(), _nodes.end());
               return res;
            }

            /** The attribute and element forms */
            Form _attributeFormDefault, _elementFormDefault;
            int _blockDefaults, _finalDefaults;
            String _targetNamespace;
            String _version;
            String _language;
            ::std::set< ::timber::Reference< Node> > _nodes;
            ::std::map< ::timber::Reference< Node>, ::timber::Reference< Node> > _parentOf;
            ::std::map< QName, TypeXRef> _types;
            ::std::map< QName, ElementXRef> _elements;
            ::std::map< QName, GroupXRef> _groups;
            ::std::map< QName, KeyXRef> _keys;
            ::std::map< QName, AttributeXRef> _attributes;
            ::std::map< QName, AttributeGroupXRef> _attributeGroups;
            ::std::vector< Reference< Import> > _imports;
      };

      struct SimpleContentImpl : public SimpleContent
      {
            ~SimpleContentImpl() throws()
            {
            }
            ::timber::Pointer< Derivation> derivation() const throws()
            {
               return _derivation;
            }
            ::timber::Pointer< Derivation> _derivation;
      };
      struct ComplexContentImpl : public ComplexContent
      {
            ~ComplexContentImpl() throws()
            {
            }
            ComplexContentImpl()
                  : _mixed(false)
            {
            }

            ::timber::Pointer< Derivation> derivation() const throws()
            {
               return _derivation;
            }
            ::timber::Pointer< Derivation> _derivation;

            bool isMixed() const throws()
            {
               return _mixed;
            }
            //FIXME: make sure the mixed attribute matches that of the extension
            // then mixed must match the type's mixed attribute
            bool _mixed;
      };

      struct FieldImpl : public Field
      {
            ~FieldImpl() throws()
            {
            }
            String xpath() const throws()
            {
               return _xpath;
            }
            String _xpath;
      };

      struct ComplexTypeImpl : public ComplexType
      {
            ~ComplexTypeImpl() throws()
            {
            }
            ComplexTypeImpl() throws()
                  : _mixed(false), _abstract(false), _block(BLOCK_NONE), _final(FINAL_NONE)
            {
            }

            bool isMixed() const throws()
            {
               return _mixed;
            }
            bool isAbstract() const throws()
            {
               return _abstract;
            }
            int block() const throws()
            {
               return _block;
            }
            int final() const throws()
            {
               return _final;
            }
            ::timber::Pointer< ::xylon::xsdom::ContentType> contentType() const throws()
            {
               return _contentType;
            }
            ::timber::Pointer< Enumerable> enumerable() const throws()
            {
               return _enumerable;
            }

            timber::Pointer< AnyAttribute> anyAttribute() const throws()
            {
               return _anyAttribute;
            }
            const ::std::vector< ::timber::Reference< Attribute> >& attributes() const throws()
            {
               return _attributes;
            }
            const ::std::vector< ::timber::Reference< AttributeGroup> >& attributeGroups() const throws()
            {
               return _attributeGroups;
            }

            QName qname() const throws()
            {
               return _qname;
            }
            bool _mixed;
            bool _abstract;
            int _block;
            int _final;
            ::timber::Pointer< ::xylon::xsdom::ContentType> _contentType;
            ::timber::Pointer< Enumerable> _enumerable;
            ::timber::Pointer< AnyAttribute> _anyAttribute;
            ::std::vector< ::timber::Reference< Attribute> > _attributes;
            ::std::vector< ::timber::Reference< AttributeGroup> > _attributeGroups;
            QName _qname;
      };

      struct SelectorImpl : public Selector
      {
            ~SelectorImpl() throws()
            {
            }
            const String xpath() const throws()
            {
               return _xpath;
            }
            String _xpath;
      };

      struct SequenceImpl : public Sequence
      {
            ~SequenceImpl() throws()
            {
            }
            SequenceImpl()
            {
            }

            const ::std::vector< ::timber::Reference< Enumerable> >& items() const throws()
            {
               return _items;
            }
            Occurrence occurs() const throws()
            {
               return _occurs;
            }
            Occurrence _occurs;
            ::std::vector< ::timber::Reference< Enumerable> > _items;
      };

      struct EnumerationImpl : public Enumeration
      {
            ~EnumerationImpl() throws()
            {
            }
      };
      struct DocumentationImpl : public Documentation
      {
            ~DocumentationImpl() throws()
            {
            }
      };
      struct MinLengthImpl : public MinLength
      {
            ~MinLengthImpl() throws()
            {
            }
      };
      struct AnnotationImpl : public Annotation
      {
            ~AnnotationImpl() throws()
            {
            }
      };
      struct ImportImpl : public Import
      {
            ~ImportImpl() throws()
            {
            }

            String targetNamespace() const throws()
            {
               return _namespace;
            }
            String schemaLocation() const throws()
            {
               return _schemaLocation;
            }

            String _namespace, _schemaLocation;
      };

      struct IncludeImpl : public Include
      {
            ~IncludeImpl() throws()
            {
            }
      };
      struct TotalDigitsImpl : public TotalDigits
      {
            ~TotalDigitsImpl() throws()
            {
            }
      };
      struct AppInfoImpl : public AppInfo
      {
            ~AppInfoImpl() throws()
            {
            }
      };
      struct KeyRefImpl : public KeyRef
      {
            ~KeyRefImpl() throws()
            {
            }
      };
      struct NotationImpl : public Notation
      {
            ~NotationImpl() throws()
            {
            }
      };
      struct LengthImpl : public Length
      {
            ~LengthImpl() throws()
            {
            }
      };
      struct PatternImpl : public Pattern
      {
            ~PatternImpl() throws()
            {
            }
      };
      struct RedefineImpl : public Redefine
      {
            ~RedefineImpl() throws()
            {
            }
      };
      struct MaxExclusiveImpl : public MaxExclusive
      {
            ~MaxExclusiveImpl() throws()
            {
            }
      };
      struct WhiteSpaceImpl : public WhiteSpace
      {
            ~WhiteSpaceImpl() throws()
            {
            }
      };
      struct FacetImpl : public Facet
      {
            ~FacetImpl() throws()
            {
            }
      };
      struct MaxInclusiveImpl : public MaxInclusive
      {
            ~MaxInclusiveImpl() throws()
            {
            }
      };
      struct MaxLengthImpl : public MaxLength
      {
            ~MaxLengthImpl() throws()
            {
            }
      };
      struct MinExclusiveImpl : public MinExclusive
      {
            ~MinExclusiveImpl() throws()
            {
            }
      };
      struct FractionDigitsImpl : public FractionDigits
      {
            ~FractionDigitsImpl() throws()
            {
            }
      };
      struct MinInclusiveImpl : public MinInclusive
      {
            ~MinInclusiveImpl() throws()
            {
            }
      };
      struct UniqueImpl : public Unique
      {
            ~UniqueImpl() throws()
            {
            }
      };

      struct BuilderImpl : public ::xylon::xsdom::dom::Visitor
      {
            BuilderImpl() throws()
            {
            }

            ~BuilderImpl() throws()
            {
            }

            int blockDefaults() const throws()
            {
               return _schema->blockDefaults();
            }
            int finalDefaults() const throws()
            {
               return _schema->finalDefaults();
            }

            Schema::Form attributeFormDefault() const throws()
            {
               return _schema->attributeFormDefault();
            }
            Schema::Form elementFormDefault() const throws()
            {
               return _schema->elementFormDefault();
            }

            inline String targetNamespace() const throws()
            {
               return _targetNamespace.top();
            }

            ElementPtr setAnnotation(ElementPtr e, Node& node)
            {
               if (e && e->localName() == "annotation") {
                  e = visitAndGetNext(e);
                  //FIXME: annotations not supported
                  //node.setAnnotation(_node);
               }
               return e;
            }

            void setOccurs(AllImpl& enumerable, size_t min, size_t max)
            {
               if (min > 1) {
                  throw ::std::runtime_error("xs:all must have a minOccurs of 0 or 1");
               }
               if (max != 1) {
                  throw ::std::runtime_error("xs:all must have a maxOccurs of 1");
               }
               enumerable._occurs = Occurrence(min, max);
            }

            template<class T> void setOccurs(T& enumerable, size_t min, size_t max)
            {
               if (min > max) {
                  throw ::std::runtime_error("Invalid minOccurs and maxOccurs pair");
               }
               enumerable._occurs = Occurrence(min, max);
            }

            template<class T>
            void setOccurs(const ElementRef& obj, T& e)
            {
               size_t min = getMinOccurs(obj, 1);
               size_t max = getMaxOccurs(obj, 1);
               setOccurs(e, min, max);
            }

            void setEnumerable(GroupImpl& g, const ::timber::Reference< Enumerable>& item)
            {
               if (item.tryDynamicCast< Compositor>()) {
                  g._enumerable = item;
               }
               else if (item.tryDynamicCast< Group>()) {
                  g._enumerable = item;
               }
               else {
                  throw ::std::runtime_error("Invalid enumerable object");
               }
            }

            template<class T>
            void setEnumerable(T& g, const ::timber::Reference< Enumerable>& item)
            {
               g._enumerable = item;
            }

            ElementPtr visitAndGetNext(ElementPtr e)
            {
               visit(e);
               return nextElement(e);
            }

            void visit(const ElementRef& e)
            {
               LogEntry(theLogger()).debugging() << "Visit : " << e->namespaceURI() << "  " << e->localName() << doLog;
               _node = nullptr;
               Visitor::visit(e);
            }
            template<class T> ElementPtr createAttributes(ElementPtr e, T& t)
            {
               vector< Reference< Attribute> > attrs;
               vector< Reference< AttributeGroup> > groups;
               Pointer< AnyAttribute> any;
               while (e && !any) {
                  visit(e);
                  if (_node.tryDynamicCast< AnyAttribute>()) {
                     any = _node;
                  }
                  else if (_node.tryDynamicCast< AttributeGroup>()) {
                     groups.push_back(_node);
                  }
                  else if (_node.tryDynamicCast< Attribute>()) {
                     attrs.push_back(_node);
                  }
                  else {
                     break;
                  }
                  e = nextElement(e);
               }
               if (!attrs.empty()) {
                  t._attributes = attrs;
               }
               if (!groups.empty()) {
                  t._attributeGroups = groups;
               }
               if (any) {
                  t._anyAttribute = any;
               }
               return e;
            }

            template<class T>
            void visitCompositor(const ElementRef& obj)
            {
               Reference< T> node = new T();

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               setOccurs(obj, *node);
               vector< Reference< Enumerable> > enums;

               while (e) {
                  e = visitAndGetNext(e);
                  enums.push_back(_node);
                  _parentOf.insert(make_pair(_node, node));
               }
               node->_items = enums;
               _node = node;
            }

            void visitAll(const ElementRef& obj)
            {
               visitCompositor< AllImpl>(obj);
            }

            void visitAnnotation(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< AnnotationImpl> node = new AnnotationImpl();
               _node = node;
            }
            void visitAny(const ElementRef& obj)
            {
               Reference< AnyImpl> node = new AnyImpl(targetNamespace());
               setOccurs(obj, *node);

               vector< String> nsList = getAttributeValueList(obj, "namespace", "##any");
               set< String> namespaces(nsList.begin(), nsList.end());

               if (namespaces.count("##local") != 0) {
                  namespaces.insert(String());
                  namespaces.erase("##local");
               }
               if (namespaces.count("##targetNamespace") != 0) {
                  namespaces.insert(targetNamespace());
                  namespaces.erase("##targetNamespace");
               }
               node->setNamespaces(namespaces);
               String proc = getAttributeValue(obj, "processContents", "strict");
               if (proc == "strict") {
                  node->_processContents = Any::STRICT;
               }
               else if (proc == "lax") {
                  node->_processContents = Any::LAX;
               }
               else if (proc == "skip") {
                  node->_processContents = Any::SKIP;
               }
               else if (proc) {
                  throw SchemaException("Unknown attribute value " + proc.string());
               }
               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitAnyAttribute(const ElementRef& obj)
            {
               Reference< AnyAttributeImpl> node = new AnyAttributeImpl(targetNamespace());
               vector< String> nsList = getAttributeValueList(obj, "namespace", "##any");
               set< String> namespaces(nsList.begin(), nsList.end());

               if (namespaces.count("##local") != 0) {
                  namespaces.insert(String());
                  namespaces.erase("##local");
               }
               if (namespaces.count("##targetNamespace") != 0) {
                  namespaces.insert(targetNamespace());
                  namespaces.erase("##targetNamespace");
               }
               node->setNamespaces(namespaces);

               String proc = getAttributeValue(obj, "processContents", "strict");
               if (proc == "strict") {
                  node->_processContents = AnyAttribute::STRICT;
               }
               else if (proc == "lax") {
                  node->_processContents = AnyAttribute::LAX;
               }
               else if (proc == "skip") {
                  node->_processContents = AnyAttribute::SKIP;
               }
               else if (proc) {
                  throw SchemaException("Unknown attribute value " + proc.string());
               }
               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }

               _node = node;
            }
            void visitAppInfo(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< AppInfoImpl> node = new AppInfoImpl();

               _node = node;
            }
            void visitAttribute(const ElementRef& obj)
            {
               const bool topLevel = isTopLevelElement(obj);
               Reference< AttributeImpl> node = new AttributeImpl();
               String value = getAttributeValue(obj, "default");
               if (value) {
                  node->_defaultValue = value;
               }
               value = getAttributeValue(obj, "fixed");
               if (value) {
                  node->_fixedValue = value;
               }

               bool useDefaultType = true;

               value = getAttributeValue(obj, "ref");

               if (value) {
                  useDefaultType = false;
                  node->_ref = getAttributeXRef(obj, value, _attributes);
               }
               value = getAttributeValue(obj, "type");
               if (value) {
                  useDefaultType = false;
                  node->_type = getTypeXRef(obj, value, _types);
               }

               value = getAttributeValue(obj, "form");
               if (topLevel || node->ref()) {
                  if (value) {
                     theLogger().warn("Ignoring form for top-level attribute");
                  }
                  node->_form = Attribute::FORM_QUALIFIED;
               }
               else {
                  if (value == "qualified") {
                     node->_form = Attribute::FORM_QUALIFIED;
                  }
                  else if (value == "unqualified") {
                     node->_form = Attribute::FORM_UNQUALIFIED;
                  }
                  else if (value) {
                     throw SchemaException("Invalid value for attribute form " + value.string(), obj);
                  }
                  else {
                     // use the default specified by the schema
                     switch (attributeFormDefault()) {
                        case Schema::FORM_QUALIFIED:
                           node->_form = Attribute::FORM_QUALIFIED;
                           break;
                        case Schema::FORM_UNQUALIFIED:
                           node->_form = Attribute::FORM_UNQUALIFIED;
                           break;
                     }
                  }
               }
               value = getAttributeValue(obj, "name");
               if (value) {
                  node->_qname = (QName(targetNamespace(), value));
                  if (topLevel) {
                     makeAttributeXRef(node->qname(), node, _attributes);
                  }
               }
               value = getAttributeValue(obj, "use");
               if (value == "optional") {
                  node->_use = Attribute::USE_OPTIONAL;
               }
               else if (value == "prohibited") {
                  node->_use = Attribute::USE_PROHIBITED;
               }
               else if (value == "required") {
                  node->_use = Attribute::USE_REQUIRED;
               }
               else if (value) {
                  throw SchemaException("Invalid value for attribute use " + value.string(), obj);
               }

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e) {
                  e = visitAndGetNext(e);
                  node->_localType = _node;
                  _parentOf.insert(make_pair(_node, node));
                  useDefaultType = false;
               }
               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               if (useDefaultType) {
                  node->_type = getTypeXRef(_types, QName("http://www.w3.org/2001/XMLSchema", "string"));
               }
               _node = node;
            }
            void visitAttributeGroup(const ElementRef& obj)
            {
               const bool topLevel = isTopLevelElement(obj);
               Reference< AttributeGroupImpl> node = new AttributeGroupImpl();
               String value;
               value = getAttributeValue(obj, "ref");
               if (value) {
                  node->_ref = (getAttributeGroupXRef(obj, value, _attributeGroups));
               }
               value = getAttributeValue(obj, "name");
               if (value) {
                  node->_qname = (QName(targetNamespace(), value));
                  if (topLevel) {
                     makeAttributeGroupXRef(node->qname(), node, _attributeGroups);
                  }
               }

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               e = createAttributes(e, *node);
               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitChoice(const ElementRef& obj)
            {
               visitCompositor< ChoiceImpl>(obj);
            }

            void visitComplexContent(const ElementRef& obj)
            {
               Reference< ComplexContentImpl> node = new ComplexContentImpl();
               String value;
               value = getAttributeValue(obj, "mixed");
               if (value == "true") {
                  node->_mixed = true;
               }
               else if (value == "false") {
                  node->_mixed = false;
               }
               else if (value) {
                  throw SchemaException("Unknown value for mixed attribute " + value.string());
               }

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               e = visitAndGetNext(e);
               node->_derivation = _node;
               _parentOf.insert(make_pair(_node, node));
               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }

            void visitComplexType(const ElementRef& obj)
            {
               const bool topLevel = isTopLevelElement(obj);
               Reference< ComplexTypeImpl> node = new ComplexTypeImpl();
               String value;
               value = getAttributeValue(obj, "abstract");
               if (value) {
                  node->_abstract = (value == String("true"));
               }

               {
                  bool found;
                  vector< String> values = getAttributeValueList(obj, "block", &found);
                  int bits = found ? ComplexType::BLOCK_NONE : (blockDefaults() & ~Schema::BLOCK_SUBSTITUTION);
                  for (size_t i = 0; i < values.size(); ++i) {
                     if (values[i] == "extension") {
                        bits |= ComplexType::BLOCK_EXTENSION;
                     }
                     else if (values[i] == "restriction") {
                        bits |= ComplexType::BLOCK_RESTRICTION;
                     }
                     else if (values[i] == "#all") {
                        bits |= ComplexType::BLOCK_ALL;
                     }
                     else {
                        throw SchemaException("Invalid value for attribute: block " + values[i].string());
                     }
                  }
                  node->_block = bits;
               }

               {
                  bool found;
                  vector< String> values = getAttributeValueList(obj, "final", &found);
                  int bits = found ? ComplexType::FINAL_NONE : finalDefaults();
                  for (size_t i = 0; i < values.size(); ++i) {
                     if (values[i] == "extension") {
                        bits |= ComplexType::FINAL_EXTENSION;
                     }
                     else if (values[i] == "restriction") {
                        bits |= ComplexType::FINAL_RESTRICTION;
                     }
                     else if (values[i] == "#all") {
                        bits |= ComplexType::FINAL_ALL;
                     }
                     else {
                        throw SchemaException("Invalid value for attribute: final " + values[i].string());
                     }
                  }
                  node->_final = bits;
               }

               value = getAttributeValue(obj, "mixed");
               if (value == "true") {
                  node->_mixed = true;
               }
               else if (value == "false") {
                  node->_mixed = false;
               }
               else if (value) {
                  throw SchemaException("Unknown value for mixed attribute " + value.string());
               }
               value = getAttributeValue(obj, "name");
               if (value) {
                  node->_qname = (QName(targetNamespace(), value));
                  if (topLevel) {
                     makeTypeXRef(node->qname(), node, _types);
                  }
               }

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e) {
                  visit(e);
                  if (_node.tryDynamicCast< ::xylon::xsdom::ContentType>()) {
                     node->_contentType = _node;
                     _parentOf.insert(make_pair(_node, node));
                     e = nextElement(e);
                  }
                  else {
                     if (_node.tryDynamicCast< Any>() || _node.tryDynamicCast< Element>()) {
                        throw SchemaException(string("Unexpected content for complex type "));
                     }
                     if (_node.tryDynamicCast< Enumerable>()) {
                        setEnumerable(*node, _node);
                        _parentOf.insert(make_pair(_node, node));
                        e = nextElement(e);
                     }
                     e = createAttributes(e, *node);
                  }
               }
               if (e) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }

            void visitDocumentation(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< DocumentationImpl> node = new DocumentationImpl();
               String value;
               value = getAttributeValue(obj, "source");
               value = getAttributeValue(obj, "xml:lang");
               ElementPtr e = firstElement(obj);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }

            void visitElement(const ElementRef& obj)
            {
               const bool topLevel = isTopLevelElement(obj);
               Reference< ElementImpl> node = new ElementImpl();
               setOccurs(obj, *node);
               String value;
               value = getAttributeValue(obj, "default");
               if (value) {
                  node->_defaultValue = value;
               }
               value = getAttributeValue(obj, "fixed");
               if (value) {
                  node->_fixedValue = value;
               }
               value = getAttributeValue(obj, "abstract");
               if (value) {
                  node->_abstract = (value == String("true"));
               }

               {
                  bool found;
                  vector< String> values = getAttributeValueList(obj, "block", &found);
                  int bits = found ? Element::BLOCK_NONE : blockDefaults();
                  for (size_t i = 0; i < values.size(); ++i) {
                     if (values[i] == "extension") {
                        bits |= Element::BLOCK_EXTENSION;
                     }
                     else if (values[i] == "restriction") {
                        bits |= Element::BLOCK_RESTRICTION;
                     }
                     else if (values[i] == "substitution") {
                        bits |= Schema::BLOCK_SUBSTITUTION;
                     }
                     else if (values[i] == "#all") {
                        bits |= Element::BLOCK_ALL;
                     }
                     else {
                        throw SchemaException("Invalid value for attribute: block " + values[i].string());
                     }
                  }
                  node->_block = bits;
               }

               {
                  bool found;
                  vector< String> values = getAttributeValueList(obj, "final", &found);
                  int bits = found ? Element::FINAL_NONE : finalDefaults();
                  for (size_t i = 0; i < values.size(); ++i) {
                     if (values[i] == "extension") {
                        bits |= Element::FINAL_EXTENSION;
                     }
                     else if (values[i] == "#all") {
                        bits |= Element::FINAL_ALL;
                     }
                     else {
                        throw SchemaException("Invalid value for attribute: final " + values[i].string());
                     }
                  }
                  node->_final = bits;
               }

               value = getAttributeValue(obj, "ref");
               if (value) {
                  node->_ref = (getElementXRef(obj, value, _elements));
               }

               value = getAttributeValue(obj, "form");
               if (topLevel || node->ref()) {
                  // top-level nodes are always qualified
                  if (value) {
                     theLogger().warn("Ignoring form for top-level element");
                  }
                  node->_form = Element::FORM_QUALIFIED;
               }
               else {
                  if (value == "qualified") {
                     node->_form = Element::FORM_QUALIFIED;
                  }
                  else if (value == "unqualified") {
                     node->_form = Element::FORM_UNQUALIFIED;
                  }
                  else if (value) {
                     throw SchemaException("Invalid value for attribute form " + value.string(), obj);
                  }
                  else {
                     // use the default specified by the schema
                     switch (elementFormDefault()) {
                        case Schema::FORM_QUALIFIED:
                           node->_form = Element::FORM_QUALIFIED;
                           break;
                        case Schema::FORM_UNQUALIFIED:
                           node->_form = Element::FORM_UNQUALIFIED;
                           break;
                     }
                  }
               }

               value = getAttributeValue(obj, "name");
               if (value) {
                  node->_qname = (QName(targetNamespace(), value));
                  if (topLevel) {
                     makeElementXRef(node->qname(), node, _elements);
                  }
               }

               if (!node->_qname && !node->_ref) {
                  LogEntry(theLogger()).severe("Element has neither a name nor a reference");
                  throw SchemaException("Element has neither a name nor a reference");
               }
               if (node->_qname && node->_ref) {
                  LogEntry(theLogger()).severe("Element may not have bother a name and a reference");
                  throw SchemaException("Found name and reference on element");
               }
               value = getAttributeValue(obj, "nillable");
               if (value) {
                  node->_nillable = (value == "true");
               }
               value = getAttributeValue(obj, "substitutionGroup");
               if (value) {
                  ElementXRef eRef = getElementXRef(obj, value, _elements);
                  node->_substitutionGroup = eRef;
               }
               value = getAttributeValue(obj, "type");
               if (value) {
                  node->_type = (getTypeXRef(obj, value, _types));
                  //	    LogEntry(theLogger()).info() << "ElementObject " << node->qname() << " has type " << node->type().name() << doLog;
               }

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               while (e) {
                  visit(e);
                  if (_node.tryDynamicCast< Type>()) {
                     if (node->elementType()) {
                        throw SchemaException("Element type already specified", obj);
                     }
                     node->_localType = _node;
                     _parentOf.insert(make_pair(_node, node));
                  }
                  else if (_node.tryDynamicCast< Unique>() || _node.tryDynamicCast< Key>()
                        || _node.tryDynamicCast< KeyRef>()) {
                  }
                  else {
                     break;
                  }
                  e = nextElement(e);
               }

               if (e) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitExtension(const ElementRef& obj)
            {
               Reference< ExtensionImpl> node = new ExtensionImpl();
               String value;
               value = getAttributeValue(obj, "base");
               if (value) {
                  node->_base = getTypeXRef(obj, value, _types);
               }
               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e) {
                  visit(e);
                  if (_node.tryDynamicCast< Enumerable>()) {
                     setEnumerable(*node, _node);
                     _parentOf.insert(make_pair(_node, node));
                     e = nextElement(e);
                  }
                  e = createAttributes(e, *node);
               }
               if (e) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }

            void visitField(const ElementRef& obj)
            {
               Reference< FieldImpl> node = new FieldImpl();
               String value;
               value = getAttributeValue(obj, "xpath");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitGroup(const ElementRef& obj)
            {
               const bool topLevel = isTopLevelElement(obj);
               Reference< GroupImpl> node = new GroupImpl();
               String value;
               value = getAttributeValue(obj, "name");
               if (value) {
                  node->_qname = (QName(targetNamespace(), value));
                  if (topLevel) {
                     makeGroupXRef(node->qname(), node, _groups);
                  }
               }
               value = getAttributeValue(obj, "ref");
               if (value) {
                  node->_ref = (getGroupXRef(obj, value, _groups));
               }
               setOccurs(obj, *node);
               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e) {
                  e = visitAndGetNext(e);
                  setEnumerable(*node, _node);
                  _parentOf.insert(make_pair(_node, node));
               }
               else if (node->qname()) {
                  throw SchemaException("Expected one of : group, choice, all, or sequence");
               }

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitImport(const ElementRef& obj)
            {
               if (!isTopLevelElement(obj)) {
                  throw ::std::runtime_error("Unexpected xs:import");
               }
               Reference< ImportImpl> node = new ImportImpl();
               node->_namespace = getAttributeValue(obj, "namespace");
               node->_schemaLocation = getAttributeValue(obj, "schemaLocation");
               ElementPtr e = setAnnotation(firstElement(obj), *node);
               _imports.push_back(node);
               if (node->_schemaLocation) {
                  auto uri = ::timber::w3c::URI::create(
                        node->_schemaLocation.string());

                  if (_baseURI.top()) {
                     uri = uri->resolve(*_baseURI.top());
                  }
                  LogEntry(logger()).info() << "Attempting to import " << node->_namespace << " from "
                        << node->_schemaLocation << " : " << *uri << doLog;
                  Reference< SchemaImpl> s = _schema;
                  importSchema(node->_namespace, uri);
                  _schema = s;
               }

               _node = node;

            }

            void visitInclude(const ElementRef& obj)
            {
               if (!isTopLevelElement(obj)) {
                  throw ::std::runtime_error("Unexpected xs:include");
               }

               Reference< IncludeImpl> node = new IncludeImpl();
               String id = getAttributeValue(obj, "id");
               String loc = getAttributeValue(obj, "schemaLocation");
               ::std::shared_ptr< ::timber::w3c::URI> uri;

               if (loc) {
                  uri = ::timber::w3c::URI::create(loc.string());

                  if (_baseURI.top()) {
                     uri = uri->resolve(*_baseURI.top());
                  }
               }
               if (!uri) {
                  throw ::std::runtime_error("Missing schemaLocation for include");
               }
               auto s = _schema;
               includeSchema(uri);
               _schema = s;
               _node = node;
            }

            void visitKey(const ElementRef& obj)
            {
               Reference< KeyImpl> node = new KeyImpl();
               String value;
               value = getAttributeValue(obj, "name");
               if (value) {
                  node->_qname = QName(targetNamespace(), value);
                  if (!isTopLevelElement(obj)) {
                     makeKeyXRef(node->qname(), node, _keys);
                  }
                  else {
                     theLogger().warn("Not a top-level key : " + value.string());
                  }
               }
               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e == nullptr) {
                  throw SchemaException("Unexpected key");
               }
               // get a selector and any number of fields

               _node = node;
            }
            void visitKeyRef(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< KeyRefImpl> node = new KeyRefImpl();
               String value;
               value = getAttributeValue(obj, "id");
               value = getAttributeValue(obj, "name");
               value = getAttributeValue(obj, "refer");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitList(const ElementRef& obj)
            {
               Reference< ListImpl> node = new ListImpl();
               String value;
               value = getAttributeValue(obj, "itemType");
               if (value) {
                  node->_itemType = getTypeXRef(obj, value, _types);
               }

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e && !node->itemType()) {
                  e = visitAndGetNext(e);
                  node->_type = (_node);
                  _parentOf.insert(make_pair(_node, node));
               }
               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }

               _node = node;
            }
            void visitNotation(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< NotationImpl> node = new NotationImpl();
               String value;
               value = getAttributeValue(obj, "name");
               value = getAttributeValue(obj, "public");
               value = getAttributeValue(obj, "system");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitRedefine(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< RedefineImpl> node = new RedefineImpl();
               String value;
               value = getAttributeValue(obj, "schemaLocation");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitRestriction(const ElementRef& obj)
            {
               Reference< RestrictionImpl> node = new RestrictionImpl();
               String value;
               value = getAttributeValue(obj, "base");
               if (value) {
                  node->_base = getTypeXRef(obj, value, _types);
               }

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (e) {
                  visit(e);
                  if (_node.tryDynamicCast< SimpleType>() || _node.tryDynamicCast< Facet>()) {
                     vector< Reference< Facet> > facets;
                     if (_node.tryDynamicCast< SimpleType>()) {
                        if (node->base()) {
                           throw SchemaException("BaseType already specified");
                        }
                        node->_simpleType = _node;
                        _parentOf.insert(make_pair(_node, node));
                        e = nextElement(e);
                     }
                     else {
                        facets.push_back(_node);
                        e = nextElement(e);
                     }
                     while (e) {
                        visit(e);
                        if (_node.tryDynamicCast< Facet>()) {
                           facets.push_back(_node);
                           _parentOf.insert(make_pair(_node, node));
                           e = nextElement(e);
                        }
                        else {
                           break;
                        }
                     }
                     node->_facets = facets;
                  }
                  else {
                     if (_node.tryDynamicCast< Enumerable>()) {
                        setEnumerable(*node, _node);
                        _parentOf.insert(make_pair(_node, node));
                        e = nextElement(e);
                     }
                     e = createAttributes(e, *node);
                  }
               }
               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }

               _node = node;
            }
            void visitSchema(const ElementRef& obj)
            {
               String value;
               Reference< SchemaImpl> schema = new SchemaImpl(); // create a new schema implementation
               _schema = schema;

               value = getAttributeValue(obj, "attributeFormDefault");
               if (value == "qualified") {
                  schema->_attributeFormDefault = Schema::FORM_QUALIFIED;
               }
               else if (value == "unqualified") {
                  schema->_attributeFormDefault = Schema::FORM_UNQUALIFIED;
               }
               value = getAttributeValue(obj, "elementFormDefault");
               if (value == "qualified") {
                  schema->_elementFormDefault = Schema::FORM_QUALIFIED;
               }
               else if (value == "unqualified") {
                  schema->_elementFormDefault = Schema::FORM_UNQUALIFIED;
               }
               {
                  vector< String> values = getAttributeValueList(obj, "blockDefault");
                  int bits = 0;
                  for (size_t i = 0; i < values.size(); ++i) {
                     if (values[i] == "extension") {
                        bits |= Schema::BLOCK_EXTENSION;
                     }
                     else if (values[i] == "restriction") {
                        bits |= Schema::BLOCK_RESTRICTION;
                     }
                     else if (values[i] == "substitution") {
                        bits |= Schema::BLOCK_SUBSTITUTION;
                     }
                     else if (values[i] == "#all") {
                        bits |= Schema::BLOCK_ALL;
                     }
                     else {
                        throw SchemaException("Invalid block attribute value : " + values[i].string(), obj);
                     }
                  }
                  schema->_blockDefaults = bits;
               }

               {
                  int bits = 0;
                  vector< String> values = getAttributeValueList(obj, "finalDefault");
                  for (size_t i = 0; i < values.size(); ++i) {
                     if (values[i] == "extension") {
                        bits |= Schema::FINAL_EXTENSION;
                     }
                     else if (values[i] == "restriction") {
                        bits |= Schema::FINAL_RESTRICTION;
                     }
                     else if (values[i] == "#all") {
                        bits |= Schema::FINAL_ALL;
                     }
                     else {
                        throw SchemaException("Invalid final attribute value : " + values[i].string(), obj);
                     }
                  }
                  schema->_finalDefaults = bits;
               }
               value = getAttributeValue(obj, "targetNamespace");
               if (value) {
                  schema->_targetNamespace = value;
               }
               else {
                  schema->_targetNamespace = _targetNamespace.top();
               }

               value = getAttributeValue(obj, "version");
               if (value) {
                  schema->_version = value;
               }
               value = getAttributeValue(obj, "xml:lang");
               if (value) {
                  schema->_language = value;
               }

               ElementPtr e = firstElement(obj);
               vector< Reference< Node> > nodes;
               while (e) {
                  e = visitAndGetNext(e);
                  nodes.push_back(_node);
                  _parentOf.insert(make_pair(_node, schema));
               }
               schema->setNodes(nodes);
               _node = schema;
               _schema = nullptr;
            }

            void visitSelector(const ElementRef& obj)
            {
               Reference< SelectorImpl> node = new SelectorImpl();
               String value;
               value = getAttributeValue(obj, "xpath");
               ElementPtr e = firstElement(obj);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitSequence(const ElementRef& obj)
            {
               visitCompositor< SequenceImpl>(obj);
            }

            void visitSimpleContent(const ElementRef& obj)
            {
               Reference< SimpleContentImpl> node = new SimpleContentImpl();
               String value;
               value = getAttributeValue(obj, "id");
               ElementPtr e = setAnnotation(firstElement(obj), *node);
               e = visitAndGetNext(e);
               node->_derivation = _node;
               _parentOf.insert(make_pair(_node, node));

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }

            void visitSimpleType(const ElementRef& obj)
            {
               const bool topLevel = isTopLevelElement(obj);
               Reference< SimpleTypeImpl> node = new SimpleTypeImpl();
               String value;
               {
                  bool found;
                  vector< String> values = getAttributeValueList(obj, "final", &found);
                  int bits = found ? SimpleType::FINAL_NONE : finalDefaults();
                  for (size_t i = 0; i < values.size(); ++i) {
                     if (values[i] == "extension") {
                        bits |= SimpleType::FINAL_RESTRICTION;
                     }
                     else if (values[i] == "list") {
                        bits |= SimpleType::FINAL_LIST;
                     }
                     else if (values[i] == "union") {
                        bits |= SimpleType::FINAL_UNION;
                     }
                     else if (values[i] == "#all") {
                        bits |= SimpleType::FINAL_ALL;
                     }
                     else if (value) {
                        throw SchemaException("Invalid value for attribute: final = " + values[i].string());
                     }
                  }
                  node->_final = bits;
               }

               value = getAttributeValue(obj, "name");
               if (value) {
                  node->_qname = (QName(targetNamespace(), value));
                  if (topLevel) {
                     makeTypeXRef(node->qname(), node, _types);
                  }
               }
               ElementPtr e = setAnnotation(firstElement(obj), *node);
               if (!e) {
                  throw SchemaException("Expected one of restriction, union, or list");
               }
               visit(e);
               if (_node.tryDynamicCast< Restriction>()) {
                  node->_restriction = _node;

                  //FIXME: do no like dynamic cast here
                  Reference< RestrictionImpl> R = node->restriction();

                  R->_simpleRestriction = true;
                  _parentOf.insert(make_pair(_node, node));
               }
               else if (_node.tryDynamicCast< List>()) {
                  node->_list = _node;
                  _parentOf.insert(make_pair(_node, node));
               }
               else if (_node.tryDynamicCast< Union>()) {
                  node->_union = _node;
                  _parentOf.insert(make_pair(_node, node));
               }
               else {
                  throw SchemaException("Unexpected content", e);
               }
               e = nextElement(e);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitUnion(const ElementRef& obj)
            {
               Reference< UnionImpl> node = new UnionImpl();
               vector< String> values = getAttributeValueList(obj, "memberTypes");
               vector< TypeXRef> refs;
               for (size_t i = 0; i < values.size(); ++i) {
                  refs.push_back(getTypeXRef(obj, values[i], _types));
               }
               node->_memberTypes = refs;

               ElementPtr e = setAnnotation(firstElement(obj), *node);
               ::std::vector< ::timber::Reference< SimpleType> > types;
               while (e) {
                  visit(e);
                  if (!_node.tryDynamicCast< SimpleType>()) {
                     break;
                  }
                  types.push_back(_node);
                  _parentOf.insert(make_pair(_node, node));
                  e = nextElement(e);
               }
               node->_types = types;
               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitUnique(const ElementRef& obj)
            {
               Reference< UniqueImpl> node = new UniqueImpl();
               String value;
               value = getAttributeValue(obj, "name");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               //	  if (e!=0) {
               //	    throw SchemaException("Unexpected content",e);
               //	  }
               _node = node;
            }
            void visitEnumeration(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< EnumerationImpl> node = new EnumerationImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitFractionDigits(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< FractionDigitsImpl> node = new FractionDigitsImpl();
               String value;
               value = getAttributeValue(obj, "fixed");
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitLength(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< LengthImpl> node = new LengthImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitMaxExclusive(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< MaxExclusiveImpl> node = new MaxExclusiveImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitMaxInclusive(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< MaxInclusiveImpl> node = new MaxInclusiveImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitMaxLength(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< MaxLengthImpl> node = new MaxLengthImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitMinExclusive(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< MinExclusiveImpl> node = new MinExclusiveImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitMinInclusive(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< MinInclusiveImpl> node = new MinInclusiveImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitMinLength(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< MinLengthImpl> node = new MinLengthImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitPattern(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< PatternImpl> node = new PatternImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitTotalDigits(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< TotalDigitsImpl> node = new TotalDigitsImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }
            void visitWhiteSpace(const ElementRef& obj)
            {
               theLogger().debugging("xs:" + obj->localName().string() + " not implemented");
               Reference< WhiteSpaceImpl> node = new WhiteSpaceImpl();
               String value;
               value = getAttributeValue(obj, "value");
               ElementPtr e = setAnnotation(firstElement(obj), *node);

               if (e != nullptr) {
                  throw SchemaException("Unexpected content", e);
               }
               _node = node;
            }

            static Pointer< ::timber::w3c::xml::dom::Element> retrieveSchema(
                  const ::timber::SharedRef< ::timber::w3c::URI>& uri) throws (::std::exception)
            {
               ::timber::ios::URIInputStream in(uri);
               unique_ptr< ::timber::w3c::xml::Parser> parser = ::timber::w3c::xml::Parser::create();
               ::timber::w3c::xml::dom::DocumentParser docBuilder(false, true);

               parser->parseStream(in, docBuilder);

               Reference< ::timber::w3c::xml::dom::NodeList> nodelist =
                     docBuilder.getDocument()->getElementsByTagNameNS(XSD_NAMESPACE_URI, "schema");
               if (nodelist->length() != 1) {
                  throw ::std::runtime_error("Expected a single schema element in the XML file");
               }
               return nodelist->item(0);
            }

            void includeSchema(const ::timber::SharedRef< ::timber::w3c::URI>& uri) throws (::std::exception)
            {
               if (!uri->scheme()) {
                  LogEntry(theLogger()).warn()
                        << "Including schema with a relative URI; iany referenced schemas cannot be loaded if they are relative URIs"
                        << doLog;

               }
               // note: it's actually not a failure to not load the element if the URI
               // cannot be opened.
               Pointer< ::timber::w3c::xml::dom::Element> schema = retrieveSchema(uri);
               if (!schema) {
                  LogEntry(theLogger()).info() << "Could not load schema " << *uri << doLog;
                  return;
               }

               String tns = getAttributeValue(schema, "targetNamespace", _targetNamespace.top());

               if (!_targetNamespace.empty()) {
                  if (tns != _targetNamespace.top()) {
                     LogEntry(theLogger()).info() << "Cannot include schema with a target-namespace " << tns
                           << " into schema " << _targetNamespace.top() << doLog;
                     throw ::std::runtime_error(
                           "Included schema must have same target-namespace as the including schema");
                  }
               }

               _baseURI.push(uri);
               _targetNamespace.push(tns);
               _namespaces.insert(tns);
               visit(schema);
               _targetNamespace.pop();
               _baseURI.pop();
            }

            void importSchema(const String& ns,
                  const ::timber::SharedRef< ::timber::w3c::URI>& uri) throws (::std::exception)
            {
               if (!uri->scheme()) {
                  LogEntry(theLogger()).warn()
                        << "Importing schema with a relative URI; any referenced schemas cannot be loaded if they are relative URIs"
                        << doLog;

               }
               if (_namespaces.count(ns) == 1) {
                  LogEntry(theLogger()).info() << "Schema with target-namespace " << ns << " already loaded" << doLog;
                  return;
               }

               // note: it's actually not a failure to not load the element if the URI
               // cannot be opened.
               Pointer< ::timber::w3c::xml::dom::Element> schema = retrieveSchema(uri);
               if (!schema) {
                  LogEntry(theLogger()).info() << "Could not load schema " << *uri << doLog;
                  return;
               }
               String tns = getAttributeValue(schema, "targetNamespace", _targetNamespace.top());
               if (tns != ns) {
                  LogEntry(theLogger()).warn() << "Target-namespace " << tns << " of schema at " << *uri
                        << " does not match " << ns << doLog;
                  throw ::std::runtime_error("TargetNamespace mismatch");
               }

               _baseURI.push(uri);
               _targetNamespace.push(tns);
               _namespaces.insert(tns);
               visit(schema);
               _targetNamespace.pop();
               _baseURI.pop();
            }

            void importSchema(const ::timber::SharedRef< ::timber::w3c::URI>& uri) throws (::std::exception)
            {
               if (!uri->scheme()) {
                  LogEntry(theLogger()).warn()
                        << "Importing schema with a relative URI; any referenced schemas cannot be loaded if they are relative URIs"
                        << doLog;

               }
               // note: it's actually not a failure to not load the element if the URI
               // cannot be opened.
               Pointer< ::timber::w3c::xml::dom::Element> schema = retrieveSchema(uri);
               if (!schema) {
                  LogEntry(theLogger()).info() << "Could not load schema " << *uri << doLog;
                  return;
               }

               String tns = getAttributeValue(schema, "targetNamespace");
               if (_namespaces.count(tns) != 0) {
                  LogEntry(theLogger()).info() << "Schema with target-namespace " << tns << " already loaded" << doLog;
                  return;
               }
               _baseURI.push(uri);
               _targetNamespace.push(tns);
               _namespaces.insert(tns);
               visit(schema);
               _targetNamespace.pop();
               _baseURI.pop();
            }

            void importSchema(const ::timber::Reference< ::xylon::xsdom::Schema>& schema) throws (::std::exception)
            {
               {
                  vector< AttributeXRef> obj = schema->attributes();
                  for (size_t i = 0, sz = obj.size(); i != sz; ++i) {
                     getXRef< Attribute>(_attributes, obj[i].name(), obj[i].target());
                  }
               }
               {
                  vector< AttributeGroupXRef> obj = schema->attributeGroups();
                  for (size_t i = 0, sz = obj.size(); i != sz; ++i) {
                     getXRef< AttributeGroup>(_attributeGroups, obj[i].name(), obj[i].target());
                  }
               }
               {
                  vector< ElementXRef> obj = schema->elements();
                  for (size_t i = 0, sz = obj.size(); i != sz; ++i) {
                     getXRef< Element>(_elements, obj[i].name(), obj[i].target());
                  }
               }
               {
                  vector< GroupXRef> obj = schema->groups();
                  for (size_t i = 0, sz = obj.size(); i != sz; ++i) {
                     getXRef< Group>(_groups, obj[i].name(), obj[i].target());
                  }
               }
               {
                  vector< TypeXRef> obj = schema->types();
                  for (size_t i = 0, sz = obj.size(); i != sz; ++i) {
                     getXRef< Type>(_types, obj[i].name(), obj[i].target());
                  }
               }
               _namespaces.insert(schema->targetNamespace());
            }

            /** The current namespace. Each time a schema is loaded, a new namespace is pushed onto the stack. */
            ::std::stack< String> _targetNamespace;

            /** The current base URI.Each time a schema is loaded, it's absolute URI is pushed onto the stack. */

            ::std::stack< ::std::shared_ptr< ::timber::w3c::URI> > _baseURI;

            /** The current node on the top */
            Pointer< Node> _node;

            map< Reference< Node>, Reference< Node> > _parentOf;

            /** The currently processed schema */
            Pointer< SchemaImpl> _schema;

            /** The set of schemas */
            set< String> _namespaces;

            /** The xrefs in this schema */
            map< QName, TypeXRef> _types;
            map< QName, ElementXRef> _elements;
            map< QName, GroupXRef> _groups;
            map< QName, KeyXRef> _keys;
            map< QName, AttributeXRef> _attributes;
            map< QName, AttributeGroupXRef> _attributeGroups;
            ::std::vector< Reference< Import> > _imports;
      };

      struct XylonSchema : public ::xylon::XsdSchema
      {
            XylonSchema(BuilderImpl& builder) throws (::std::exception)
            {
               _types.swap(builder._types);
               _elements.swap(builder._elements);
               _groups.swap(builder._groups);
               _keys.swap(builder._keys);
               _attributes.swap(builder._attributes);
               _attributeGroups.swap(builder._attributeGroups);
               _namespaces.swap(builder._namespaces);

               checkBound("Attribute", _attributes);
               checkBound("AttributeGroup", _attributeGroups);
               checkBound("Element", _elements);
               checkBound("Group", _groups);
               checkBound("Type", _types);
               checkBound("Key", _keys);
            }

            ~XylonSchema() throws()
            {
            }
            ::std::vector< ::xylon::xsdom::AttributeXRef> attributes() const throws()
            {
               ::std::vector< ::xylon::xsdom::AttributeXRef> res;
               for (map< QName, AttributeXRef>::const_iterator i = _attributes.begin(), end = _attributes.end();
                     i != end; ++i) {
                  res.push_back(i->second);
               }

               return res;
            }

            ::std::vector< ::xylon::xsdom::AttributeGroupXRef> attributeGroups() const throws()
            {
               ::std::vector< ::xylon::xsdom::AttributeGroupXRef> res;
               for (map< QName, AttributeGroupXRef>::const_iterator i = _attributeGroups.begin(), end =
                     _attributeGroups.end(); i != end; ++i) {
                  res.push_back(i->second);
               }
               return res;

            }

            ::std::vector< ::xylon::xsdom::ElementXRef> elements() const throws()
            {
               ::std::vector< ::xylon::xsdom::ElementXRef> res;
               for (map< QName, ElementXRef>::const_iterator i = _elements.begin(), end = _elements.end(); i != end;
                     ++i) {
                  res.push_back(i->second);
               }
               return res;

            }

            ::std::vector< ::xylon::xsdom::GroupXRef> groups() const throws()
            {
               ::std::vector< ::xylon::xsdom::GroupXRef> res;
               for (map< QName, GroupXRef>::const_iterator i = _groups.begin(), end = _groups.end(); i != end; ++i) {
                  res.push_back(i->second);
               }
               return res;

            }

            ::std::vector< ::xylon::xsdom::TypeXRef> types() const throws()
            {
               ::std::vector< ::xylon::xsdom::TypeXRef> res;
               for (map< QName, TypeXRef>::const_iterator i = _types.begin(), end = _types.end(); i != end; ++i) {
                  res.push_back(i->second);
               }
               return res;

            }

            ::std::vector< String> schemas() const throws()
            {
               ::std::vector< String> res;
               res.insert(res.end(), _namespaces.begin(), _namespaces.end());
               return res;
            }

            ::timber::Pointer< ::xylon::xsdom::Type> findType(const QName& qname) const throws()
            {
               ::timber::Pointer< ::xylon::xsdom::Type> res;
               map< QName, TypeXRef>::const_iterator i = _types.find(qname);
               if (i != _types.end()) {
                  res = i->second.target();
               }
               return res;
            }

            ::timber::Pointer< ::xylon::xsdom::Group> findGroup(const QName& qname) const throws()
            {
               ::timber::Pointer< ::xylon::xsdom::Group> res;
               map< QName, GroupXRef>::const_iterator i = _groups.find(qname);
               if (i != _groups.end()) {
                  res = i->second.target();
               }
               return res;
            }
            ::timber::Pointer< ::xylon::xsdom::AttributeGroup> findAttributeGroup(const QName& qname) const throws()
            {
               ::timber::Pointer< ::xylon::xsdom::AttributeGroup> res;
               map< QName, AttributeGroupXRef>::const_iterator i = _attributeGroups.find(qname);
               if (i != _attributeGroups.end()) {
                  res = i->second.target();
               }
               return res;
            }

            ::timber::Pointer< ::xylon::xsdom::Attribute> findAttribute(const QName& qname) const throws()
            {
               ::timber::Pointer< ::xylon::xsdom::Attribute> res;
               map< QName, AttributeXRef>::const_iterator i = _attributes.find(qname);
               if (i != _attributes.end()) {
                  res = i->second.target();
               }
               return res;
            }

            ::timber::Pointer< ::xylon::xsdom::Element> findElement(const QName& qname) const throws()
            {
               ::timber::Pointer< ::xylon::xsdom::Element> res;
               map< QName, ElementXRef>::const_iterator i = _elements.find(qname);
               if (i != _elements.end()) {
                  res = i->second.target();
               }
               return res;
            }

            ::timber::Pointer< ::xylon::xsdom::Key> findKey(const QName& qname) const throws()
            {
               ::timber::Pointer< ::xylon::xsdom::Key> res;
               map< QName, KeyXRef>::const_iterator i = _keys.find(qname);
               if (i != _keys.end()) {
                  res = i->second.target();
               }
               return res;
            }

            ::std::set< QName> findSubstitutions(const QName& root) const throws()
            {
               set< QName> res;
               res.insert(root);
               vector< QName> tmp;

               for (map< QName, ElementXRef>::const_iterator i = _elements.begin(), end = _elements.end(); i != end;
                     ++i) {
                  ElementXRef s = i->second;
                  // already processed this element
                  if (res.count(s.name()) == 1) {
                     continue;
                  }
                  tmp.clear();
                  while (true) {
                     tmp.push_back(s.name());
                     Pointer< Element> e = derefElement(s);
                     if (!e) {
                        LogEntry(theLogger()).warn() << "Cannot dereference element " << s.name() << doLog;
                        break;
                     }

                     s = e->substitutionGroup();
                     if (!s) {
                        break;
                     }
                     if (s) {
                        // ok, we've found it.
                        if (s.name() == root) {
                           res.insert(tmp.begin(), tmp.end());
                           break;
                        }
                     }
                  }
               }

               return res;
            }
         private:
            set< String> _namespaces;
            map< QName, TypeXRef> _types;
            map< QName, ElementXRef> _elements;
            map< QName, GroupXRef> _groups;
            map< QName, KeyXRef> _keys;
            map< QName, AttributeXRef> _attributes;
            map< QName, AttributeGroupXRef> _attributeGroups;
      };

      struct XmlSchemaBuilder : public SchemaBuilder
      {
            XmlSchemaBuilder() throws()
                  : _builder(new BuilderImpl())
            {

            }

            ~XmlSchemaBuilder() throws()
            {
            }

            void importSchema(const ::timber::Reference< ::xylon::xsdom::Schema>& schema) throws (::std::exception)
            {
               _builder->importSchema(schema);
            }

            void importSchema(const SharedRef< ::timber::w3c::URI>& uri) throws (exception)
            {
               if (_builder.get() == 0) {
                  throw runtime_error("Cannot import anymore schema");
               }
               try {
                  _builder->importSchema(uri);
               }
               catch (...) {
                  // reset the builder, because it is bound to be corrupted
                  _builder.reset(0);
                  throw runtime_error("Could not import schema from " + uri->string());
               }
            }

            bool importSchema(const String& ns, const SharedRef< ::timber::w3c::URI>& uri) throws (exception)
            {
               if (_builder.get() == 0) {
                  throw runtime_error("Cannot import anymore schema");
               }
               try {
                  _builder->importSchema(uri);
                  return true;
               }
               catch (...) {
                  // reset the builder, because it is bound to be corrupted
                  _builder.reset(0);
                  throw runtime_error("Could not import schema " + ns.string() + " from " + uri->string());
               }
            }

            /**
             * Get the schema that has been built. If the schema still contains unresolved references, then this method
             * returns a null pointer
             * @return a schema or null if there are still unresolved references
             */
         public:
            Pointer< ::xylon::XsdSchema> getSchema() throws()
            {
               Pointer< ::xylon::XsdSchema> res;
               if (_builder.get() != 0) {
                  res = new XylonSchema(*_builder);
                  _builder.reset(new BuilderImpl());
               }
               return res;
            }

            /** The builder's implementation */
         private:
            ::std::unique_ptr< BuilderImpl> _builder;
      };
   }

   SchemaBuilder::SchemaBuilder() throws()
   {
   }

   SchemaBuilder::~SchemaBuilder() throws()
   {
   }

   unique_ptr< SchemaBuilder> SchemaBuilder::create() throws()
   {
      return unique_ptr < SchemaBuilder > (new XmlSchemaBuilder());
   }

}
