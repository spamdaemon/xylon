#include <xylon/XsdSchema.h>
#include <xylon/xsdom/nodes.h>
#include <timber/logging.h>

using namespace std;
using namespace timber;
using namespace timber::logging;
using namespace timber::w3c;
using namespace xylon::xsdom;

#include <map>

namespace xylon {
   namespace {
      static Log logger()
      {
         return Log("xylon.XsdSchema");
      }
   }

   XsdSchema::XsdSchema() throws()
   {
   }

   XsdSchema::~XsdSchema() throws()
   {
   }

   ::timber::Pointer< ::xylon::xsdom::Type> XsdSchema::derefType(const TypeXRef& xref) const throws()
   {
      if (!xref) {
         return Pointer< Type> ();
      }
      if (xref.isBound()) {
         return xref.target();
      }
      return findType(xref.name());
   }

   ::timber::Pointer< ::xylon::xsdom::Group> XsdSchema::derefGroup(const GroupXRef& xref) const throws()
   {
      if (!xref) {
         return Pointer< Attribute> ();
      }
      if (xref.isBound()) {
         return xref.target();
      }
      return findGroup(xref.name());
   }

   ::timber::Pointer< ::xylon::xsdom::Attribute> XsdSchema::derefAttribute(const AttributeXRef& xref) const throws()
   {
      if (!xref) {
         return Pointer< Attribute> ();
      }
      if (xref.isBound()) {
         return xref.target();
      }
      return findAttribute(xref.name());
   }

   ::timber::Pointer< ::xylon::xsdom::AttributeGroup> XsdSchema::derefAttributeGroup(const AttributeGroupXRef& xref) const throws()
   {
      if (!xref) {
         return Pointer< AttributeGroup> ();
      }
      if (xref.isBound()) {
         return xref.target();
      }
      return findAttributeGroup(xref.name());
   }

   ::timber::Pointer< ::xylon::xsdom::Key> XsdSchema::derefKey(const KeyXRef& xref) const throws()
   {
      if (!xref) {
         return Pointer< Key> ();
      }
      if (xref.isBound()) {
         return xref.target();
      }
      return findKey(xref.name());
   }

   ::timber::Pointer< ::xylon::xsdom::Element> XsdSchema::derefElement(const ElementXRef& xref) const throws()
   {
      if (!xref) {
         return Pointer< Element> ();
      }
      if (xref.isBound()) {
         return xref.target();
      }
      return findElement(xref.name());
   }

   bool XsdSchema::inSubstitutionGroup(const QName& group, const QName& qname) const throws( ::std::exception)
   {
      QName cur = qname;

      bool res = true;
      while (cur != group) {
         Pointer< Element> e = findElement(cur);
         if (!e) {
            throw ::std::runtime_error("Cannot determine group member ship of " + cur.string().string() + " yet");
         }

         ElementXRef s = e->substitutionGroup();
         if (!s) {
            LogEntry(logger()).debugging() << "inSubstitutionGroup(" << group << "," << qname << ") = false" << doLog;
            res = false;
            break;
         }

         cur = s.name();
      }

      return res;
   }

   QName XsdSchema::getSubstitutionGroupRoot(const QName& qname) const throws()
   {
      QName root = qname;
      while (true) {
         Reference< Element> e = findElement(root);
         ElementXRef s = e->substitutionGroup();
         if (s) {
            root = s.name();
         }
         else {
            break;
         }
      }
      return root;
   }

   XsdSchema::ElementType XsdSchema::getElementType(const QName& name) const throws (::std::exception)
   {
      Pointer< Element> e = findElement(name);
      if (e) {
         return getElementType(e);
      }
      else {
         throw ::std::runtime_error("Element " + name.string().string() + " not resolved");
      }
   }

   XsdSchema::ElementType XsdSchema::getElementType(const ::timber::Reference< Element>& e) const throws (::std::exception)
   {
      if (e->ref()) {
         return getElementType(e->ref().name());
      }
      Pointer< Type> t;
      if (e->type()) {
         t = findType(e->type().name());
         if (!t) {
            throw ::std::runtime_error("Type " + e->type().name().string().string() + " not resolved");
         }
      }
      else if (e->elementType()) {
         t = e->elementType();
      }
      else if (e->substitutionGroup()) {
         return getElementType(e->substitutionGroup().name());
      }
      else {
         return ANY_TYPE;
      }

      if (t.tryDynamicCast< SimpleType> ()) {
         return SIMPLE_TYPE;
      }

      assert(t.tryDynamicCast< ComplexType> ());
      return COMPLEX_TYPE;
   }

   XsdSchema::ContentModel XsdSchema::getContentModel(const QName& name) const throws (::std::exception)
   {
      Pointer< Element> e = findElement(name);
      if (e) {
         return getContentModel(e);
      }
      else {
         throw ::std::runtime_error("Element " + name.string().string() + " not resolved");
      }
   }

   XsdSchema::ContentModel XsdSchema::getContentModel(const ::timber::Reference< ::xylon::xsdom::Element>& e) const throws (::std::exception)
   {
      ElementType eType = getElementType(e);
      if (eType == NO_TYPE) {
         if (e->substitutionGroup()) {
            return getContentModel(e->substitutionGroup().name());
         }
         else {
            return NO_CONTENT;
         }
      }
      if (eType == SIMPLE_TYPE) {
         return SIMPLE_TYPE_CONTENT;
      }

      if (e->ref()) {
         return getContentModel(e->ref().name());
      }
      Pointer< ComplexType> t; // has simple type should have return true if the element was a simple type
      if (e->type()) {
         t = findType(e->type().name());
         // NOTE: we should not have to test this, because getElementType should have thrown an exception already
         if (!t) {
            throw ::std::runtime_error("Type " + e->type().name().string().string() + " not resolved");
         }
      }
      else if (e->elementType()) {
         t = e->elementType();
      }
      else if (e->substitutionGroup()) {
         return getContentModel(e->substitutionGroup().name());
      }
      else {
         return ANY_CONTENT;
      }

      if (!t->contentType()) {
         return MIXED_CONTENT;
      }
      if (t->contentType().tryDynamicCast< SimpleContent> ()) {
         return SIMPLE_CONTENT;
      }
      Reference< ComplexContent> c = t->contentType();
      if (c->isMixed()) {
         return MIXED_CONTENT;
      }
      else {
         return COMPLEX_CONTENT;
      }
   }

   bool XsdSchema::isAbstractElement(const QName& name) const throws (::std::exception)
   {
      Pointer< Element> e = findElement(name);
      if (e) {
         return isAbstractElement(e);
      }
      else {
         throw ::std::runtime_error("Element " + name.string().string() + " not resolved");
      }
   }

   bool XsdSchema::isAbstractElement(const ::timber::Reference< ::xylon::xsdom::Element>& e) const throws (::std::exception)
   {
      if (e->isAbstract()) {
         return true;
      }
      Pointer< ComplexType> t; // has simple type should have return true if the element was a simple type
      if (e->type()) {
         Pointer< Type> xt = findType(e->type().name());
         // NOTE: we should not have to test this, because getElementType should have thrown an exception already
         if (!xt) {
            throw ::std::runtime_error("Type " + e->type().name().string().string() + " not resolved");
         }
         t = xt.tryDynamicCast< ComplexType> ();
      }
      else if (e->elementType()) {
         t = e->elementType().tryDynamicCast< ComplexType> ();
      }
      else if (e->substitutionGroup()) {
         return isAbstractElement(e->substitutionGroup().name());
      }
      else {
         return false;
      }
      return t && t->isAbstract();
   }
   ::std::set< QName> XsdSchema::findSubtypesOf(const QName& typeName) const throws()
   {
      ::std::set< QName> res;

      // do a simple loop over all known types
      ::std::vector< ::xylon::xsdom::TypeXRef> topLevelTypes = types();
      for (size_t i = 0; i < topLevelTypes.size(); ++i) {
         if (topLevelTypes[i]->isSubtypeOf(typeName)) {
            res.insert(topLevelTypes[i].name());
            assert(topLevelTypes[i].name() != QName::xsQName("anyType")
                  && "anyType cannot be a subtype of any other type");
            assert(topLevelTypes[i].name() != typeName && "A type cannot be a subtype of itself");
         }
      }

      return res;
   }

}

