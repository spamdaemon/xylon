#include <xylon/ModelFactory.h>
#include <xylon/BindingProvider.h>
#include <xylon/XsdSchema.h>
#include <xylon/XsdSchemaModel.h>
#include <xylon/QName.h>
#include <xylon/model/TypeModel.h>
#include <xylon/xsdom/nodes.h>
#include <timber/logging.h>

#include <map>
#include <set>
#include <vector>

using namespace std;
using namespace timber;
using namespace timber::logging;
using namespace xylon::model;
using namespace xylon::xsdom;

namespace xylon {
   namespace {

      static Log logger()
      {
         return Log("xylon.ModelFactory");
      }

      typedef map< String, XsdSchemaModel::XRef> SchemaBindings;
      typedef ::std::map< GlobalName, ModelRef< TypeModel> > TypeModels;

      template<class T>
      static void createModelRefs(const ::std::vector< T>& v, const GlobalName::Type& type, TypeModels& models)
      {
         for (size_t i = 0; i < v.size(); ++i) {
            assert(v[i].isBound() && "Schema is not bound properly");
            ModelRef< TypeModel> ref(::timber::Pointer< TypeModel>(), true);

            LogEntry(logger()).debugging() << "Create model ref for " << GlobalName(v[i]->qname(), type) << doLog;
            models.insert(TypeModels::value_type(GlobalName(v[i]->qname(), type), ref));
         }
      }

      static vector< ModelRef< TypeModel> > collectModelRefs(const TypeModels& models, const GlobalName::Type& type)
      {
         vector< ModelRef< TypeModel> > res;
         for (TypeModels::const_iterator i = models.begin(); i != models.end(); ++i) {
            if (i->first.type() == type) {
               res.push_back(i->second);
            }
         }
         return res;
      }

      struct SimpleSchemaBinding : public XsdSchemaModel
      {
            SimpleSchemaBinding(const Reference< ::xylon::XsdSchema>& s) throws() :
               _schema(s)
            {
               ModelRef< TypeModel> anyType = TypeModel::createAnyType();
               _models.insert(TypeModels::value_type(anyType->globalName(), anyType));

               // create the type refs for each thing defined in the schema
               createModelRefs(s->attributes(), GlobalName::ATTRIBUTE, _models);
               createModelRefs(s->attributeGroups(), GlobalName::ATTRIBUTE_GROUP, _models);
               createModelRefs(s->elements(), GlobalName::ELEMENT, _models);
               createModelRefs(s->groups(), GlobalName::GROUP, _models);
               createModelRefs(s->types(), GlobalName::TYPE, _models);
            }

            ~SimpleSchemaBinding() throws()
            {
            }

            void accept(::xylon::model::ModelVisitor& v)
            {
               for (TypeModels::const_iterator i = _models.begin(); i != _models.end(); ++i) {
                  i->second->accept(v);
               }
            }

            /**
             * Get the schema that was being bound.
             * @return the bound schema
             */
         public:
            ::timber::Reference< ::xylon::XsdSchema> schema() const throws ()
            {
               return _schema;
            }

            /**
             * @defgroup Accessors Access the models for specific types of xs objects.
             * These methods are just short-cuts and all calls are forwarded to the appropriate SchemaModel method.
             * @{
             */
            /**
             * Get the type model for the specified element
             * @param qname the fully qualified name of an element
             * @return a global type model or null if the element was not bound
             */
         public:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> getElementModel(const QName& name) const throws (::std::exception)
            {
               return findModelRef(name, GlobalName::ELEMENT, false);
            }

            /**
             * Get the type model for the specified complex type or simple type.
             * @param qname the fully qualified name of a simple or complex type.
             * @return a global type model or null if the type was not bound
             */
         public:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> getTypeModel(const QName& name) const throws (::std::exception)
            {
               return findModelRef(name, GlobalName::TYPE, false);
            }

            /**
             * Get the type model for the specified group.
             * @param qname the fully qualified name of a group
             * @return a global type model or null if the group was not bound
             */
         public:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> getGroupModel(const QName& name) const throws (::std::exception)
            {
               return findModelRef(name, GlobalName::GROUP, false);
            }

            /**
             * Get the type model for the specified attribute.
             * @param qname the fully qualified name of an attribute
             * @return a global type model or null if the attribute was not bound
             */
         public:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> getAttributeModel(const QName& name) const throws (::std::exception)
            {
               return findModelRef(name, GlobalName::ATTRIBUTE, false);
            }

            /**
             * Get the type model for the specified attribute group.
             * @param qname the fully qualified name of an attribute group
             * @return a global type model or null if the attribute group was not bound
             */
         public:
            ::xylon::model::ModelRef< ::xylon::model::TypeModel> getAttributeGroupModel(const QName& name) const throws (::std::exception)
            {
               return findModelRef(name, GlobalName::ATTRIBUTE_GROUP, false);
            }

            /**
             * Find a type model by its qname and namespace type.
             * @param v the qualified name of the object to be found
             * @param type the namespace of the object
             * @param modifiable true, if the object should be modifiable
             * @return a model ref that may or may not be modifiable
             */
         private:
            ModelRef< TypeModel> findModelRef(const QName& v, const GlobalName::Type& type, bool modifiable) const
            {
               TypeModels::const_iterator i = _models.find(GlobalName(v, type));
               if (i == _models.end()) {
                  LogEntry(logger()).warn() << "Querying for an unknown object " << GlobalName(v, type) << doLog;
                  return ModelRef< TypeModel> ();
               }
               return ModelRef< TypeModel> (i->second, modifiable);
            }

            ::std::vector< ModelRef< TypeModel> > elementModels() const throws()
            {
               return collectModelRefs(_models, GlobalName::ELEMENT);
            }

            ::std::vector< ModelRef< TypeModel> > typeModels() const throws()
            {
               return collectModelRefs(_models, GlobalName::TYPE);
            }

            ::std::vector< ModelRef< TypeModel> > groupModels() const throws()
            {
               return collectModelRefs(_models, GlobalName::GROUP);
            }

            ::std::vector< ModelRef< TypeModel> > attributeModels() const throws()
            {
               return collectModelRefs(_models, GlobalName::ATTRIBUTE);
            }

            ::std::vector< ModelRef< TypeModel> > attributeGroupModels() const throws()
            {
               return collectModelRefs(_models, GlobalName::ATTRIBUTE_GROUP);
            }

         private:
            const Reference< ::xylon::XsdSchema> _schema;

            /** The models, indexed by their global names */
         public:
            TypeModels _models;

      };

      struct MyBindingManager : public ModelFactory
      {
            MyBindingManager() throws()
            {
            }
            ~MyBindingManager() throws()
            {
               while (!_providers.empty()) {
                  delete _providers.back();
                  _providers.pop_back();
               }
            }

         private:
            ::timber::Pointer< TypeModel> bindObject(const XsdSchemaModel& binding, const GlobalName& name,
                  BindingProvider& provider) throws (::std::exception)
            {
               ::timber::Pointer< TypeModel> boundModel;
               switch (name.type()) {
                  case GlobalName::ATTRIBUTE:
                     boundModel = provider.bindAttribute(binding, binding.schema()->findAttribute(name.name()));
                     break;
                  case GlobalName::ATTRIBUTE_GROUP:
                     boundModel = provider.bindAttributeGroup(binding,
                           binding.schema()->findAttributeGroup(name.name()));
                     break;
                  case GlobalName::ELEMENT:
                     boundModel = provider.bindElement(binding, binding.schema()->findElement(name.name()));
                     break;
                  case GlobalName::GROUP:
                     boundModel = provider.bindGroup(binding, binding.schema()->findGroup(name.name()));
                     break;
                  case GlobalName::TYPE: {
                     Reference< Type> t = binding.schema()->findType(name.name());
                     Pointer< SimpleType> st = t.tryDynamicCast< SimpleType> ();
                     if (st) {
                        boundModel = provider.bindSimpleType(binding, st);
                     }
                     else {
                        boundModel = provider.bindComplexType(binding, t);
                     }
                     break;
                  }
                  default:
                     LogEntry(logger()).warn() << "Unexpected global name  " << name << doLog;
                     throw ::std::runtime_error("Unexpected global name");
               }
               return boundModel;
            }

            Reference< XsdSchemaModel> createModel(const Reference< ::xylon::XsdSchema>& schema) throws (::std::exception)
            {
               typedef ::std::map< GlobalName, Reference< TypeModel> > BoundModels;
               BoundModels boundModels;

               Reference< SimpleSchemaBinding> B(new SimpleSchemaBinding(schema));

               for (TypeModels::iterator i = B->_models.begin(), end = B->_models.end(); i != end; ++i) {
                  // already bound? it's probably a built-in type such as xs:anyType
                  if (i->second.isBound()) {
                     continue;
                  }
                  Pointer< TypeModel> boundModel;
                  try {
                     for (size_t j = 0; !boundModel && j < _providers.size(); ++j) {
                        boundModel = bindObject(*B, i->first, *_providers[j]);
                     }
                     if (!boundModel && _defaultProvider.get() != 0) {
                        boundModel = bindObject(*B, i->first, *_defaultProvider);
                     }
                  }
                  catch (const ::std::exception& e) {
                     logger().caught("Could not map object to type model type", e);
                  }
                  if (boundModel) {
                     boundModels.insert(BoundModels::value_type(i->first, boundModel));
                  }
                  else {
                     LogEntry(logger()).warn() << "Object " << i->first << " was not bound to anything" << doLog;
                     throw ::std::runtime_error("Object not bound");
                  }

               }

               // now, bind the models
               for (TypeModels::iterator i = B->_models.begin(), end = B->_models.end(); i != end; ++i) {
                  if (!i->second.isBound()) {
                     i->second.bind(boundModels.find(i->first)->second);
                     LogEntry(logger()).debugging() << "Bound global " << i->first << doLog;
                  }
               }
               return B;
            }

            void registerBindingProvider(::std::unique_ptr< BindingProvider> gen) throws()
            {
               _providers.push_back(0);
               _providers.back() = gen.release();
            }

            bool setDefaultBindingProvider(::std::unique_ptr< BindingProvider> gen) throws()
            {
               if (_defaultProvider.get() == 0) {
                  _defaultProvider = ::std::move(gen);
                  return true;
               }
               // not set
               return false;
            }

            /** The bindings for a schema */
         private:
            SchemaBindings _bindings;

            /** A list of model providers */
         private:
            vector< BindingProvider*> _providers;

            /** The default model provider */
         private:
            unique_ptr< BindingProvider> _defaultProvider;
      };
   }

   ModelFactory::ModelFactory() throws()
   {
   }
   ModelFactory::~ModelFactory() throws()
   {
   }
   ::std::unique_ptr< ModelFactory> ModelFactory::create() throws()
   {
      return unique_ptr< ModelFactory> (new MyBindingManager());
   }
}
