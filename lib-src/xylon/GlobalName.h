#ifndef _XYLON_XSDOM_GLOBALNAME_H
#define _XYLON_XSDOM_GLOBALNAME_H

#ifndef _XYLON_XSDOM_QNAME_H
#include <xylon/QName.h>
#endif

#include <iosfwd>

namespace xylon {

   /**
    * A GlobalName can be used to index nodes by their qualified names. Unfortunately,
    * different types of nodes can have the same qualified names and so an extra node
    * type must be used  to distinguish such qualified names.
    */
   class GlobalName
   {
         /** The node types that can be bound to a class or attribute */
      public:
         enum Type
         {
            ELEMENT, GROUP, ATTRIBUTE, ATTRIBUTE_GROUP, TYPE, KEY, UNDEFINED
         };

         /**
          * Create an invalid global name.
          */
      public:
         GlobalName() throws();

         /**
          * Create a new node ref
          * @param n the node name
          * @param t the node type
          */
      public:
         GlobalName(const QName& n, Type t) throws();
         /** Copy constructor */
      public:
         GlobalName(const GlobalName& src) throws();

         /** Copy operator */
      public:
         GlobalName& operator=(const GlobalName& src) throws();

         /** Destructor */
      public:
         ~GlobalName() throws();

         /**
          * Get the qname
          * @return the qname
          */
      public:
         inline const QName& name() const throws()
         {
            return _name;
         }

         /**
          * Get the type of the node being referenced.
          * @return the node type
          */
      public:
         inline Type type() const throws()
         {
            return _type;
         }

         /**
          * Determine if this is a valid name.
          * @return true if this is a valid name
          */
      public:
         inline operator bool() const throws()
         {
            return _type != UNDEFINED;
         }

         /**
          * Compare two node refs
          * @param r a node ref
          * @return true if this node ref is the same as the specified node ref
          */
      public:
         bool operator==(const GlobalName& r) const throws();

         /**
          * Compute the ordering of two node refs
          * @param r a node ref
          * @return true if this node ref is less than the specified one
          */
      public:
         bool operator<(const GlobalName& r) const throws();

         /**
          * Get a unique string for this name
          * @return the string for this name that will also be emitted
          */
      public:
         String string() const throws();

         /** The qualified name of a node */
      private:
         QName _name;

         /** The node type */
      private:
         Type _type;
   };
}
::std::ostream& operator<<(::std::ostream& out, const ::xylon::GlobalName& name);

#endif
