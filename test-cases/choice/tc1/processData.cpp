#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->num_foo()==3);
   assert(data->num_anyElement()==2);
   assert(data->num_bar()==1);
   assert(data->num_group()==2);
   assert(data->get_group(0).get_name()==string("Key"));
   assert(data->get_group(0).get_value()==17);
   assert(data->get_group(1).get_name()==string("Key1"));
   assert(data->get_group(1).get_value()==34);
   assert(data->num_double()==1 && data->get_double(0) == 12.0);
   assert(data->num_int()==1 && data->get_int(0) == 123);


}

