#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->num_all() == 9);
   // 1,2,3
   for (int i = 0; i < 3; ++i) {
      const Envelope::all& g = data->get_all(i);
      assert(g.isSet_foo());
      assert(g.get_bar() == 1);
      assert(g.get_local().value() == 123);
   }
   // 4
   {
      const Envelope::all& g = data->get_all(3);
      assert(g.isSet_foo());
      assert(g.get_bar() == 1);
      assert(!g.isSet_local());
   }
   //5
   {
      const Envelope::all& g = data->get_all(4);
      assert(!g.isSet_foo());
      assert(g.get_bar() == 1);
      assert(g.get_local().value() == 123);
   }
   //6
   {
      const Envelope::all& g = data->get_all(5);
      assert(g.isSet_foo());
      assert(!g.isSet_bar());
      assert(g.get_local().value() == 123);
   }
   //7
   {
      const Envelope::all& g = data->get_all(6);
      assert(g.isSet_foo());
      assert(!g.isSet_bar() );
      assert(!g.isSet_local());
   }
   //8
   {
      const Envelope::all& g = data->get_all(7);
      assert(!g.isSet_foo());
      assert(g.get_bar() == 1);
      assert(!g.isSet_local());
   }
   //9
   {
      const Envelope::all& g = data->get_all(8);
      assert(!g.isSet_foo());
      assert(!g.isSet_bar() );
      assert(g.get_local().value() == 123);
   }

}
