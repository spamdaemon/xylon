#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->isSet_group());
   const group& g = data->get_group();
   assert(g.isSet_foo());
   assert(g.get_bar() == 1);
   assert(g.get_local().value() == 123);
}

