#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   ::std::cerr << " Value : " << data->get_Attributes().get_localInt()  << ::std::endl;
   assert(data->get_Attributes().get_localInt() == 1);

}
