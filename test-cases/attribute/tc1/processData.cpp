#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->get_Attributes(0).get_intAttr().get_value()==12);
   assert(data->get_Attributes(1).get_doubleAttr()==12.0);
   assert(data->get_Attributes(2).get_localAttr().get_items(0)==1);
   assert(data->get_Attributes(2).get_localAttr().get_items(1)==2);
   assert(data->get_Attributes(2).get_localAttr().get_items(2)==3);
   assert(data->get_Attributes(2).get_localAttr().get_items(3)==4);
   assert(data->get_Attributes(2).get_localAttr().get_items(4)==5);
   assert(data->get_Attributes(3).get_localInt()==1);

   assert(data->get_Attributes(4).get_intAttr().get_value()==117);
   assert(data->get_Attributes(4).get_doubleAttr()==118.0);
   assert(data->get_Attributes(4).get_localAttr().get_items(0)==1001);
   assert(data->get_Attributes(4).get_localInt()==1234);
}

