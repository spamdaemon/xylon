#include <timber/timber.h>
#include <iostream>
#include "Types.h"

using namespace ::std;
using namespace ::timber;
using namespace ::testNS;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   assert(data->num_required()==3);
   assert(data->num_optional()==2);
   assert(data->num_onlyOne()==1);
   assert(data->num_empty()==1);

   assert(data->get_required(0).get_a(0)==1);
   assert(data->get_required(0).get_b(0)==2);
   assert(data->get_required(0).get_c(0)==3);
   assert(data->get_required(0).get_d(0)==4);
   assert(data->get_required(0).get_e(0)==5);
   assert(data->get_required(0).get_f(0)==6);

   assert(data->get_required(1).get_a(0)==7);
   assert(data->get_required(1).get_d(0)==8);
   assert(data->get_required(1).get_e(0)==9);
   assert(data->get_required(1).get_f(0)==10);

   assert(data->get_required(2).get_d(0)==11);
   assert(data->get_required(2).get_d(1)==12);
   assert(data->get_required(2).get_d(2)==13);
   assert(data->get_required(2).get_d(3)==14);
   assert(data->get_required(2).get_e(0)==15);
   assert(data->get_required(2).get_e(1)==16);
   assert(data->get_required(2).get_e(2)==17);
   assert(data->get_required(2).get_e(3)==18);

   assert(data->get_optional(0).get_a(0)==19);
   assert(data->get_optional(0).get_b(0)==20);
   assert(data->get_optional(0).get_c(0)==21);
   assert(data->get_optional(0).get_d(0)==22);
   assert(data->get_optional(0).get_e(0)==23);
   assert(data->get_optional(0).get_f(0)==24);



}

