#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->isSet_repeated());
   assert(data->isSet_not_repeated());
   assert(data->get_not_repeated().num_foo()==1);
   assert(data->get_not_repeated().num_foo2()==1);
}

