#include <timber/timber.h>
#include <iostream>
#include "Types.h"

using namespace ::std;
using namespace ::timber;
using namespace ::testNS;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   assert(data->num_list() == 3);
   assert(data->get_list(0).num_items() == 2);
   assert(data->get_list(1).num_items() == 2);
   assert(data->get_list(2).num_items() == 2);

   assert(data->get_list(0).get_items(0) == 1);
   assert(data->get_list(0).get_items(1) == 2);
   assert(data->get_list(1).get_items(0) == 3);
   assert(data->get_list(1).get_items(1) == 4);
   assert(data->get_list(2).get_items(0) == 5);
   assert(data->get_list(2).get_items(1) == 6);

}

