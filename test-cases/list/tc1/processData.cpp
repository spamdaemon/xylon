#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   for (int i = 0; i < 4; ++i) {
      ::std::cerr << '[' << i << "] : ";
      ::std::cerr << data->get_list1(0).get_items(i) << " " << data->get_list3(0).get_value().get_items(i) << ' '
            << data->get_list3(0).get_list().get_items(i) << ::std::endl;
      assert(data->get_list1(0).get_items(i) == (i + 1));
      assert(data->get_list3(0).get_value().get_items(i) == -(i + 1));
      assert(data->get_list3(0).get_list().get_items(i) == 11 * (i + 1));
   }
   assert(data->get_list2(0).get_items(0).get_int() == 11);
   assert(data->get_list2(0).get_items(1).get_boolean() == true);
   assert(data->get_list2(0).get_items(2).get_int() == 33);
   assert(data->get_list2(0).get_items(3).get_boolean() == false);

}

