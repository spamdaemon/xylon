#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * This function should never be invoked, because the document cannot be parsed, if restrictions are supported.
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   throw runtime_error("Unexpected call to processData; document is invalid; getting here means we're not processing restrictions correctly");
}

