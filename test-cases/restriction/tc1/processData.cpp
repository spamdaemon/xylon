#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->get_restriction1(0) == 12);
   for (int i = 0; i < 4; ++i) {
      assert(data->get_restriction2(0).get_value().get_items(i) == i + 1);
      assert(data->get_restriction3(0).get_value().get_value().get_items(i) == 2*(i + 1));
      assert(data->get_restriction4(0).get_value().get_items(i) == -(i+1));
   }

}
