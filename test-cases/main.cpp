#include <timber/timber.h>
#include <timber/ios/URIInputStream.h>
#include <timber/w3c/xml/dom/DocumentParser.h>
#include <timber/w3c/xml/dom/Document.h>
#include <timber/w3c/xml/dom/Element.h>
#include <timber/w3c/xml/Parser.h>
#include <xylon/rt/StreamWriter.h>
#include <xylon/rt/ParseError.h>

#include <iostream>

#include "Types.h"
#include "Schema.h"

using namespace ::std;
using namespace ::timber;

void processData(unique_ptr< ::testNS::Envelope>);

static unique_ptr< ::xylon::rt::ElementCursor> loadDocument(istream& stream)
{
   using namespace ::timber::ios;
   using namespace ::timber::w3c::xml;

   unique_ptr< ::xylon::rt::ElementCursor> res;
   unique_ptr< Parser> parser = Parser::create();
   ::timber::w3c::xml::dom::DocumentParser docBuilder;
   parser->parseStream(stream, docBuilder);

   Reference< ::timber::w3c::xml::dom::Document> document = docBuilder.getDocument();
   Pointer< ::timber::w3c::xml::dom::Element> e = document->documentElement();
   if (e) {
      testNS::Schema mySchema;
      res = ::xylon::rt::ElementCursor::create(e);
   }
   return res;
}

static int verifyData(unique_ptr< ::xylon::rt::ElementCursor> cursor, ostream& buf)
{
   testNS::Schema mySchema;
   unique_ptr< ::testNS::Envelope> data;
   try {
      data = mySchema.parseElement< ::testNS::Envelope> (move(cursor));
   }
   catch (const ::xylon::rt::ParseError& e) {
      cerr << "ParseError  : " << e.what() << endl;
      return 1;
   }
   catch (const exception& e) {
      cerr << "Exception (2) : " << e.what() << endl;
      return 1;
   }

   if (data.get()) {
      try {
         // attempt to print the data
         ::xylon::rt::StreamWriter sw(buf);
         sw.setIndent("   ");
         sw.beginDocument();
         data->write(sw);
         sw.endDocument();
         buf << endl;

         processData(move(data));
         return 0;
      }
      catch (const ::xylon::rt::MarshalError& me) {
         cerr << "Marshaling error : " << me.what() << endl;
         return 3;
      }
      catch (const exception& e) {
         cerr << "Exception caught while processing data (1) : " << e.what() << endl;
         return 1;
      }
      catch (...) {
         cerr << "Unknown exception caught while processing data (1) : " << endl;
         return 1;
      }
   }
   return 1;
}

int main(int argc, char** argv)
{

   if (argc < 2) {
      cerr << "At least 1 input file is required (3)" << endl;
      return 3;
   }

   try {

      ::timber::ios::URIInputStream stream(argv[1]);

      unique_ptr< ::xylon::rt::ElementCursor> cursor = loadDocument(stream);
      if (!cursor.get()) {
         cerr << "No document found (3)" << endl;
         return 3;
      }

      {
         ostringstream buf;
         ::xylon::rt::StreamWriter sw(buf);
         sw.setIndent(" ");
         sw.beginDocument();
         cursor->writeElement(sw);
         sw.endDocument();
         buf << endl;
      }

      ostringstream buf;
      int verified = verifyData(move(cursor), buf);

      if (verified != 0) {
         return verified;
      }
      cerr << "Verified the original document" << endl;

      cerr << buf.str() << endl;
      // attempt to print the data
      istringstream ibuf(buf.str());
      cursor = loadDocument(ibuf);
      if (!cursor.get()) {
         cerr << "Could not reload document" << endl;
         return 3;
      }

      int res = verifyData(move(cursor), buf);
      if (res != 0) {
         return res;
      }
      cerr << "Reverified the document" << endl;
   }
   catch (const exception& e) {
      cerr << "Exception (2) : " << e.what() << endl;
      return 2;
   }
   catch (...) {
      cerr << "Unexpected exception " << endl;
      return 1;
   }

   return 0;
}
