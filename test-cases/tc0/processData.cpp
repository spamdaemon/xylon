#include <iostream>
#include <cassert>

#include "Types.h"

using namespace ::std;
using namespace ::testNS;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr<Envelope> data)
{
   assert("Template Process Data: processData not implemented"==0);
}

