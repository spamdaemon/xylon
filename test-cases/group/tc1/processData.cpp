#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   ::std::cerr << "Local : " << data->get_g(0).get_Group().get_local() << ::std::endl;
   assert(data->get_g(0).get_Group().isSet_bar());
   assert(data->get_g(0).get_Group().isSet_foo());
   assert(data->get_g(0).get_Group().get_local() == 1);

   assert(!data->get_g(1).get_Group().isSet_bar());
   assert(data->get_g(1).get_Group().isSet_foo());
   assert(!data->get_g(1).get_Group().isSet_local());

   assert(data->get_g(2).get_Group().isSet_bar());
   assert(!data->get_g(2).get_Group().isSet_foo());
   assert(!data->get_g(2).get_Group().isSet_local());

   assert(!data->get_g(3).get_Group().isSet_bar());
   assert(!data->get_g(3).get_Group().isSet_foo());
   assert(data->get_g(3).get_Group().get_local() == 1);

   assert(!data->get_g(4).isSet_Group());


}
