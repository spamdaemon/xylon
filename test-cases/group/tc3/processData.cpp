#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(!data->get_A().get_group().isSet_A());
   assert(data->get_A().get_group().get_B().get_group().isSet_A());
   assert(!data->get_B().isSet_group());
}
