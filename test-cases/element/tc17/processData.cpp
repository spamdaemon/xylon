#include <timber/timber.h>
#include <iostream>
#include "Types.h"

using namespace ::std;
using namespace ::timber;
using namespace ::testNS;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr<Envelope> data)
{
   bar& xbar = dynamic_cast<bar&>(data->get_foo());
   bar::Local& xobj = dynamic_cast<bar::Local&>(xbar.get_value());
   assert(xobj.get_foo()==1);
   assert(xobj.get_bar()==.25);
}

