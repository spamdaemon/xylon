#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   abstractType & e = data->get_Element();
   derivedType& d = dynamic_cast< derivedType&> (e);
   abstractType & rec = d.get_recursion(0);
   most_derivedType& most_derived = dynamic_cast< most_derivedType&> (rec);
   assert(most_derived.get_open() == 12);

}

