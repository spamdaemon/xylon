#include <timber/timber.h>
#include <iostream>
#include "Types.h"

using namespace ::std;
using namespace ::timber;
using namespace ::testNS;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< Envelope> data)
{
   bar* xbar = dynamic_cast<bar*>(&data->get_foo());
   assert(xbar!=0);
   deadbeefType* ximpl = dynamic_cast<deadbeefType*>(&xbar->get_value());
   cerr << "Type : " << typeid(xbar->get_value()).name() << endl;
   assert(ximpl!=0);

}

