#include <timber/timber.h>
#include <iostream>
#include "Types.h"

using namespace ::std;
using namespace ::timber;
using namespace ::testNS;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{

   assert(data->get_foo(0).get_value()==12);
   assert(data->get_foo(1).get_value()==13);
   assert(data->get_bar().get_value()==14);

}

