#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   if (data->isSet_abstract()) {
      concrete& obj = dynamic_cast< concrete&> (data->get_abstract());
      assert(obj.get_value() == 12);
   }

   if (data->isSet_localAbstract()) {
      localConcrete& obj = dynamic_cast< localConcrete&> (data->get_localAbstract());
      assert(obj.get_foo() == 13);
   }
}

