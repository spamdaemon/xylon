#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->get_extension().get_foo().get_bar(0)  == 123456);
   assert(data->get_extension().get_foo().get_bar(1)  == 654321);
   assert(data->get_extension().get_foo().get_deadbeef()  == 12);

   assert(data->get_restriction().get_foo().get_bar(0) == 1234);
}

