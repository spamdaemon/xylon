#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->get_group1().get_restriction() == 101);
   assert(data->get_group1().get_list().get_items(2) == 1200);
   assert(data->get_group1().get_union().get_double() == 12.0);
}

