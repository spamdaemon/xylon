#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   if (data->isSet_untyped()) {
      ::std::cerr << "Parsed a untyped element" << ::std::endl;
   }
   if (data->isSet_Integer()) {
      ::std::cerr << "Parsed a  Integer with value : " << data->get_Integer().get_value().value() << ::std::endl;

   }
   assert(data->get_Integer().get_value() == 17);
}

