#include <iostream>
#include <cassert>

#include "Types.h"

using namespace ::std;
using namespace ::testNS;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr<Envelope> data)
{
   derivedType& d = dynamic_cast<derivedType&>(data->get_content());

   baseType::foo_XList& bList = d.baseType::get_foo();
   derivedType::foo_XList& dList = d.get_foo();


   assert (bList.get_items(0) == 1.125);
   assert (bList.get_items(1) == 2.125);

   assert (dList.get_items(0) == 1);
   assert (dList.get_items(1) == 2);
   assert (dList.get_items(2) == 3);
   assert (dList.get_items(3) == 4);
}

