#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   assert(data->get_union1(0).isSet_boolean() && data->get_union1(0).get_boolean() == true);
   assert(!data->get_union1(0).isSet_int());
   assert(data->get_union1(1).isSet_int() && data->get_union1(1).get_int() == 23);
   assert(!data->get_union1(1).isSet_boolean());

   assert(data->get_union2(0).isSet_IItemType1() && data->get_union2(0).get_IItemType1() == false);
   assert(!data->get_union2(0).isSet_IItemType0());
   assert(data->get_union2(1).isSet_IItemType0() && data->get_union2(1).get_IItemType0() == 29);
   assert(!data->get_union2(1).isSet_IItemType1());

   assert(data->get_union3(0).isSet_boolean() && data->get_union3(0).get_boolean() == true);
   assert(!data->get_union3(0).isSet_IItemType0());
   assert(data->get_union3(1).isSet_IItemType0() && data->get_union3(1).get_IItemType0() == 11);
   assert(!data->get_union3(1).isSet_boolean());

}
