#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;


   assert(data->num_element()>=1 && "Expected at least 1 element");
   assert(data->get_element(0).get_GroupA().get_global().get_value()==string("2"));
   assert(data->get_element(0).get_GroupA().get_typed() == 3);
   assert(data->get_element(0).get_GroupA().isSet_GroupB());
   assert(data->get_element(0).get_GroupA().get_GroupB().isSet_local());
   ::std::cerr << "LOCAL : " << data->get_element(0).get_GroupA().get_GroupB().get_local() << ::std::endl;
   assert(data->get_element(0).get_GroupA().get_GroupB().get_local()==1);
}

