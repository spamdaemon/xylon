#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

/**
 * Process the data in the envelope. This is the function that should be filled in by the various test-cases
 * @param data
 */
void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

   // nothing really to verify here
   assert(data->isSet_strictAttribute());
   assert(data->isSet_laxAttribute());
   assert(data->isSet_skipAttribute());

}

