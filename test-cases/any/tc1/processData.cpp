#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

using namespace ::testNS;

static void printAnyType (::std::ostream& out, const ::xylon::rt::AnyElement& any)
{
  switch(any.type()) {
  case ::xylon::rt::AnyElement::PARSED:
    out << "Object : " << typeid(any.object()).name() << ::std::endl;
    break;
  case ::xylon::rt::AnyElement::UNPARSED:
    out << "Element: " << any.element().qname() << ::std::endl;
    break;
  default:
    out << "None: " << ::std::endl;
    break;
  }
}

void processData(::std::unique_ptr< ::testNS::Envelope> data)
{
   using namespace ::testNS;

}
