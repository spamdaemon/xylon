#include <timber/timber.h>
#include <iostream>

#include "Types.h"

using namespace ::std;
using namespace ::timber;

using namespace ::testNS;

static void printAnyType(::std::ostream& out, const ::xylon::rt::AnyElement& any)
{
   switch (any.type()) {
      case ::xylon::rt::AnyElement::PARSED:
         out << "Object : " << typeid(any.object()).name() << ::std::endl;
         break;
      case ::xylon::rt::AnyElement::UNPARSED:
         out << "Element: " << any.element().qname() << ::std::endl;
         break;
      default:
         out << "None: " << ::std::endl;
         break;
   }
}

void processData(::std::unique_ptr< ::testNS::Envelope> obj)
{
   for (size_t i = 0; i < obj->num_strict(); ++i) {
      ::std::cerr << "Parsed a STRICT any : ";
      printAnyType(::std::cerr, obj->get_strict(i).get_anyElement());
   }
   for (size_t i = 0; i < obj->num_lax(); ++i) {
      ::std::cerr << "Parsed a LAX any : ";
      printAnyType(::std::cerr, obj->get_lax(i).get_anyElement());
   }
   for (size_t i = 0; i < obj->num_skip(); ++i) {
      ::std::cerr << "Parsed a SKIP any : ";
      printAnyType(::std::cerr, obj->get_skip(i).get_anyElement());
   }
}
