#include <parser.h>

#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/dom/Document.h>
#include <timber/w3c/xml/dom/DocumentParser.h>
#include <timber/w3c/xml/dom/dom.h>

#include <string>
#include <fstream>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::xsd;


Reference< ::timber::w3c::xml::dom::Document> readFile (const string& file) throws (exception)
{
  ifstream in(file.c_str());
  unique_ptr< ::timber::w3c::xml::Parser> parser = ::timber::w3c::xml::Parser::create();
  ::timber::w3c::xml::dom::DocumentParser docBuilder(false,true);
  parser->parseStream(in,docBuilder);
  
  return docBuilder.getDocument();
}

int main(int argc, const char** argv)
{
  if (argc<2) {
    return 1;
  }
  try {
    Reference< ::timber::w3c::xml::dom::Document> doc = readFile(argv[1]);
    ::std::unique_ptr< ::Root> root = parseRoot(doc->documentElement());
    
    for (size_t i=0;i<root->getValues1()->getInteger().size();++i) {
      ::std::cerr << i << " : " << root->getValues1()->getInteger()[i] << ::std::endl;
    }
    for (size_t i=0;i<root->getValues2()->getInteger().size();++i) {
      ::std::cerr << i << " : " << root->getValues2()->getInteger()[i] << ::std::endl;
    }
    return 0;
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception : " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    return 1;
  }

}
