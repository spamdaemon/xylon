#include <KmlSchema.h>

#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/dom/Element.h>
#include <timber/w3c/xml/dom/Document.h>
#include <timber/w3c/xml/dom/DocumentParser.h>
#include <timber/w3c/xml/dom/dom.h>

#include <xylon/rt/Schema.h>
#include <xylon/rt/ElementCursor.h>

#include <string>
#include <fstream>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::xylon::rt;

Reference< ::timber::w3c::xml::dom::Document> readFile(const string& file) throws (exception)
{
   ifstream in(file.c_str());
   unique_ptr< ::timber::w3c::xml::Parser> parser = ::timber::w3c::xml::Parser::create();
   ::timber::w3c::xml::dom::DocumentParser docBuilder(false, true);
   parser->parseStream(in, docBuilder);

   return docBuilder.getDocument();
}

int main(int argc, const char** argv)
{
   if (argc < 2) {
      return 1;
   }
   try {
      Reference< ::timber::w3c::xml::dom::Document> doc = readFile(argv[1]);
      ::std::unique_ptr< ElementCursor> e = ElementCursor::create(doc->documentElement());
      ::std::unique_ptr< ::xylon::rt::Schema> schema(new kml::Schema());

      ::std::unique_ptr< ::xylon::rt::Object> obj = schema->parseElement(*e);
      ::std::cerr << "Element parsed : " << typeid(*obj).name() << ::std::endl;

      return 0;
   }
   catch (const ::std::exception& e) {
      ::std::cerr << "Exception : " << e.what() << ::std::endl;
      return 1;
   }
   catch (...) {
      return 1;
   }

}
