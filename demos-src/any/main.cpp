#include <parser.h>

#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/dom/Document.h>
#include <timber/w3c/xml/dom/DocumentParser.h>
#include <timber/w3c/xml/dom/dom.h>

#include <cassert>

#include <string>
#include <fstream>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::xsd;


Reference< ::timber::w3c::xml::dom::Document> readFile (const string& file) throws (exception)
{
  ifstream in(file.c_str());
  auto parser = ::timber::w3c::xml::Parser::create();
  ::timber::w3c::xml::dom::DocumentParser docBuilder(false,true);
  parser->parseStream(in,docBuilder);
  
  return docBuilder.getDocument();
}

int main(int argc, const char** argv)
{
  if (argc<2) {
    return 1;
  }
  try {
    auto doc = readFile(argv[1]);
    ::std::unique_ptr< ::Root> root = parseRoot(doc->documentElement());
    
    assert(root->getAny_strict()[0].getAnyElement()->object());  // "<empty/>
    assert(root->getAny_lax()[0].getAnyElement()->element());    // "<foo>"
    assert(root->getAny_lax()[1].getAnyElement()->object());     // "<empty/>"
    assert(root->getAny_skip()[0].getAnyElement()->element());   // "<empty/>
    return 0;
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception : " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    return 1;
  }

}
