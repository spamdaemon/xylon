#include <parser.h>

#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/dom/Document.h>
#include <timber/w3c/xml/dom/DocumentParser.h>
#include <timber/w3c/xml/dom/dom.h>

#include <string>
#include <fstream>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::xsd;

static void printItem (const ::types::Items::Item_T& item, ::std::ostream& out)
{
  if (item.getProductName()) {
    out << "Product    : " << *item.getProductName() << ::std::endl;
  }
  if (item.getPartNum()) {
    out << "SKU        : " << *item.getPartNum() << ::std::endl;
  }
  if (item.getQuantity()) {
    out << "Quantity   : " << *item.getQuantity() << ::std::endl;
  }
  if (item.getUSPrice()) {
    out << "Price    : " << *item.getUSPrice() << ::std::endl;
  }
  if (item.getShipDate()) {
    out << "Shipped  : " << *item.getShipDate() << ::std::endl;
  }
  
}

static void printAddress (const ::types::USAddress& addr, ::std::ostream& out)
{
  out << "Name    : " << *addr.getName() << ::std::endl
      << "Street  : " << *addr.getStreet() << ::std::endl
      << "City    : " << *addr.getCity()->getCity() << ::std::endl
      << "State   : " << *addr.getCity()->getState() << ::std::endl
      << "ZipCode : " << *addr.getZip()->getValue() << ::std::endl;
  if (addr.getCountry()) {
    out << "Country : " << *addr.getCountry() << ::std::endl;
  }
}

static void printOrder (const PurchaseOrder& order, ::std::ostream& out)
{
  if (!order.getValue()) {
    out << "No purchase" << endl;
    return;
  }
  if (order.getValue()->getItems()==0) {
    out << "No items" << endl;
  }
  for (size_t i = 0;i<order.getValue()->getItems()->getItem().size();++i) {
    printItem(order.getValue()->getItems()->getItem()[i],out);
  }
  
  const ::types::USAddress* addr = order.getValue()->getShipTo();
  if (addr) {
    out << "Ship To: " << endl;
    printAddress(*addr,out);
  }
  
  addr = order.getValue()->getBillTo();
  if (addr) {
    out << "Bill To : " << endl;
    printAddress(*addr,out);
  }
  
  if (order.getValue()->getOrderDate()->getOrderDate()) {
    out << "OrderDate : " << *order.getValue()->getOrderDate()->getOrderDate() << endl;
  }
  
}


static Reference< ::timber::w3c::xml::dom::Document> readFile (const string& file) throws (exception)
{
  ifstream in(file.c_str());
  unique_ptr< ::timber::w3c::xml::Parser> parser = ::timber::w3c::xml::Parser::create();
  ::timber::w3c::xml::dom::DocumentParser docBuilder(false,true);
  parser->parseStream(in,docBuilder);
  
  return docBuilder.getDocument();
}

int main(int argc, const char** argv)
{
  if (argc<2) {
    return 1;
  }
  try {
    Reference< ::timber::w3c::xml::dom::Document> doc = readFile(argv[1]);
    ::std::unique_ptr< ::PurchaseOrder> value = parsePurchaseOrder(doc->documentElement());
    if (value.get()) {
      printOrder(*value,::std::cout);
    }
    return 0;
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception : " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    return 1;
  }

}
