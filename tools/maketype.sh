#!/bin/sh



if [ "$1" = "list" ]; then
   echo lib-src/xsd/types.h
elif [ "$1" = "gen.h" ]; then
   mkdir -p gen-lib-src/xsd;
   cp tools/types.h.template gen-lib-src/xsd/types.h;
else
   mkdir -p gen-lib-src/xsd;
   cp tools/types.cpp.template gen-lib-src/xsd/types.cpp;
fi;

while [ 1 ]; do
   read NAME FILE TYPE REST;
   if [ $? -ne 0 ]; then
     break;
   fi;
  
  UPPER=`echo $FILE | tr [a-z] [A-Z]`
  if [ "$1" = "list" ]; then
      echo gen-lib-src/xsd/$FILE.h
      echo gen-lib-src/xsd/$FILE.cpp
  elif [ "$1" = "gen.h" ]; then 
      sed "s/@CLASS@/$UPPER/g" < tools/type.h.template  | sed "s/@class@/$FILE/g" | sed "s/@Type@/$TYPE/g">> gen-lib-src/xsd/types.h;
      echo >> gen-lib-src/xsd/types.h;
  else
      sed "s/@CLASS@/$UPPER/g" < tools/type.cpp.template  | sed "s/@class@/$FILE/g" | sed "s/@Type@/$TYPE/g" | sed "s/@qname@/ ::xylon::rt::QName::xsQName(\"$NAME\")/g" >> gen-lib-src/xsd/types.cpp;
      echo >> gen-lib-src/xsd/types.cpp;
  fi;
done;


if [ "$1" = "gen.h" ]; then
  echo "}" >> gen-lib-src/xsd/types.h;
  echo "#endif" >> gen-lib-src/xsd/types.h;
elif [ "$1" = "gen.cpp" ]; then
    echo "}" >> gen-lib-src/xsd/types.cpp;
fi;

