
mkdir lib-src/xylon/xsom;
rm -f lib-src/xylon/xsom/Visitor.h lib-src/xylon/xsom/Visitor.cpp;
for p in `cat tools/xs/list`; do
  VAR=`echo $p | tr [a-z] [A-Z]`;
  Var=$p;
  var=`tools/xs/tolowercase $p`;
  rm lib-src/xylon/xsom/${Var}.h lib-src/xylon/xsom/${Var}.cpp;
  sed "s/%VAR%/${VAR}/g" < tools/xs/t.h.templ   | sed "s/%Var%/${Var}/g" | sed "s/%var%/${var}/g" > lib-src/xylon/xsom/${Var}.h;
  sed "s/%VAR%/${VAR}/g" < tools/xs/t.cpp.templ | sed "s/%Var%/${Var}/g" | sed "s/%var%/${var}/g" > lib-src/xylon/xsom/${Var}.cpp;
#  echo "#include <xylon/xs/${Var}.h>" >> lib-src/xylon/xs/nodes
  echo "class ${Var};" >> lib-src/xylon/xsom/Visitor.h
done;

for p in `cat tools/xs/list`; do
  VAR=`echo $p | tr [a-z] [A-Z]`;
  Var=$p;
  var=`tools/xs/tolowercase $p`;
  echo "virtual void visit${Var}(const ::timber::Reference<${Var}>& obj);" >> lib-src/xylon/xsom/Visitor.h
  echo "void Visitor::visit${Var}(const ::timber::Reference<${Var}>& obj) {}" >> lib-src/xylon/xsom/Visitor.cpp
done;

