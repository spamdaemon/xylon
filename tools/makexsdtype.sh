#!/bin/sh

TYPES=`tools/listnames complexType test-data/w3c/XMLSchema.xsd`;
ELEMENTS=`tools/listnames element test-data/w3c/XMLSchema.xsd`;

#for TYPENAME in $TYPES; do
#
#  FILE=`basename $TYPENAME Type`Type;
#  TYPE=`capitalize $FILE`;
#  FILE=$TYPE
#  UPPER=`echo $FILE | tr [a-z] [A-Z]`
#
#  if [ $1 == "list" ]; then
#    echo src/xylon/schema/$FILE.h
#    echo src/xylon/schema/$FILE.cpp
#  else 
#   sed "s/@CLASS@/$UPPER/g" < tools/xsdtype.h.template  | sed "s/@class@/$FILE/g"  > src/xylon/schema/$FILE.h;
#   sed "s/@CLASS@/$UPPER/g" < tools/xsdtype.cpp.template  | sed "s/@class@/$FILE/g"  > src/xylon/schema/$FILE.cpp;
#  fi;
#done;

echo "" > src/xylon/schema/elements.h

for ELEMENTNAME in $ELEMENTS; do

  FILE=`basename $ELEMENTNAME`;
  ELEMENT=`capitalize $FILE`;
  FILE=$ELEMENT
  UPPER=`echo $FILE | tr [a-z] [A-Z]`

  if [ $1 == "list" ]; then
    echo src/xylon/schema/$FILE.h
    echo src/xylon/schema/$FILE.cpp
  else 
#   sed "s/@CLASS@/$UPPER/g" < tools/xsdtype.h.template  | sed "s/@class@/$FILE/g" > src/xylon/schema/$FILE.h;
#   sed "s/@CLASS@/$UPPER/g" < tools/xsdtype.cpp.template  | sed "s/@class@/$FILE/g" > src/xylon/schema/$FILE.cpp;
   echo "#include <xylon/schema/$FILE.h>" >>    src/xylon/schema/elements.h;
  fi;
done;

