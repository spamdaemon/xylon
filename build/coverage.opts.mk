# this fragment defines coverage options for compilation and linking

COMPILER_COVERAGE_OPTS := -fprofile-arcs -ftest-coverage
LINKER_COVERAGE_OPTS := -fprofile-arcs -ftest-coverage

# the coverage info is a file that contains the data
# gathering from the coverage tool;
COVERAGE_INFO := $(BASE_DIR)/$(BASE_NAME).lcov
